/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.utils;

import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.cassandra.config.entity.AreaConfigSchedule;
import com.uptime.cassandra.config.entity.AreaSite;
import com.uptime.cassandra.config.entity.AssetSiteArea;
import com.uptime.cassandra.config.entity.HwUnitToPoint;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.cassandra.config.entity.PointToHwUnit;
import com.uptime.cassandra.config.entity.Points;
import com.uptime.cassandra.config.entity.SiteContacts;
import com.uptime.cassandra.config.entity.SiteCustomer;
import com.uptime.cassandra.config.entity.SiteRegionCountry;
import com.uptime.cassandra.config.entity.Subscriptions;
import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import com.uptime.cassandra.monitor.entity.SiteDeviceBasestation;
import com.uptime.config.vo.ApAlSetVO;
import com.uptime.config.vo.AreaConfigScheduleVO;
import com.uptime.config.vo.AreaVO;
import com.uptime.config.vo.AssetVO;
import com.uptime.config.vo.DeviceStatusVO;
import com.uptime.config.vo.DeviceVO;
import com.uptime.config.vo.HwUnitVO;
import com.uptime.config.vo.PointLocationVO;
import com.uptime.config.vo.PointToHwUnitVO;
import com.uptime.config.vo.PointVO;
import com.uptime.config.vo.SiteBasestationDeviceVO;
import com.uptime.config.vo.SiteContactVO;
import com.uptime.config.vo.SiteVO;
import com.uptime.config.vo.SubscriptionsVO;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class DelegateUtil {
    
    /**
     * Convert AssetVO Object to List of PointLocations Object
     * @param assetVO, AssetVO Object
     * @return List of PointLocations Object
     */
    public static List<PointLocations> getPointLocationsList(AssetVO assetVO) {
        List<PointLocations> pointLocationsList = new ArrayList();
        
        if (assetVO != null && assetVO.getPointLocationList() != null && !assetVO.getPointLocationList().isEmpty()) {
            assetVO.getPointLocationList().stream().filter(pointLocationVO -> (pointLocationVO != null && pointLocationVO.getPointLocationId() == null)).forEachOrdered(pointLocationVO -> pointLocationVO.setPointLocationId(UUID.randomUUID()));
            assetVO.getPointLocationList().stream().filter(pointLocationVO -> pointLocationVO != null).forEachOrdered(pointLocationVO -> pointLocationVO.setAssetId(assetVO.getAssetId()));
            assetVO.getPointLocationList().stream().filter(pointLocationVO -> pointLocationVO != null).forEachOrdered(pointLocationVO -> pointLocationsList.add(getPointLocations(pointLocationVO)));
        }
        
        return pointLocationsList;
    }
    
    /**
     * Convert AssetVO Object to List of Points Object
     * @param assetVO, AssetVO Object
     * @return List of Points Object
     */
    public static List<Points> getPointsList(AssetVO assetVO) {
        List<Points> pointsList = new ArrayList();
        
        if (assetVO != null && assetVO.getPointLocationList() != null && !assetVO.getPointLocationList().isEmpty()) {
            assetVO.getPointLocationList().stream().filter(pointLocationVO -> pointLocationVO != null).forEachOrdered(pointLocationVO -> pointsList.addAll(getPointsList(pointLocationVO)));
        }
        
        return pointsList;
    }
    
    /**
     * Convert AssetVO Object to List of ApAlByPoint Object
     * @param assetVO, AssetVO Object
     * @return List of ApAlByPoint Object
     */
    public static List<ApAlByPoint> getApAlByPointList(AssetVO assetVO) {
        List<ApAlByPoint> apAlByPointList = new ArrayList();
        
        if (assetVO != null && assetVO.getPointLocationList() != null && !assetVO.getPointLocationList().isEmpty()) {
            assetVO.getPointLocationList().stream().filter(pointLocationVO -> pointLocationVO != null).forEachOrdered(pointLocationVO -> apAlByPointList.addAll(getApAlByPointList(pointLocationVO)));
        }
        
        return apAlByPointList;
    }
    
    /**
     * Convert PointLocationVO Object to PointLocations Object
     * @param pointLocationVO, PointLocationVO object
     * @return PointLocations Object
     */
    public static PointLocations getPointLocations(PointLocationVO pointLocationVO) {
        PointLocations entity = new PointLocations();
        
        if (pointLocationVO != null) {
            entity.setAreaId(pointLocationVO.getAreaId());
            entity.setAssetId(pointLocationVO.getAssetId());
            entity.setCustomerAccount(pointLocationVO.getCustomerAccount());
            entity.setDescription(pointLocationVO.getDescription());
            entity.setDeviceSerialNumber(pointLocationVO.getDeviceSerialNumber());
            entity.setFfSetIds(pointLocationVO.getFfSetIds());
            entity.setPointLocationName(pointLocationVO.getPointLocationName());
            entity.setPointLocationId(pointLocationVO.getPointLocationId());
            entity.setSampleInterval(pointLocationVO.getSampleInterval());
            entity.setBasestationPortNum(pointLocationVO.getBasestationPortNum());
            entity.setSiteId(pointLocationVO.getSiteId());
            entity.setSpeedRatio(pointLocationVO.getSpeedRatio());
            entity.setTachId(pointLocationVO.getTachId());
//            entity.setAlarmEnabled(pointLocationVO.isAlarmEnabled());
            entity.setRollDiameter(pointLocationVO.getRollDiameter());
            entity.setRollDiameterUnits(pointLocationVO.getRollDiameterUnits());
        }
        
        return entity;
    }
      
    /**
     * Convert PointLocationVO Object to List of Points Objects
     * @param pointLocationVO, PointLocationVO Object
     * @return List of Points Objects
     */
    public static List<Points> getPointsList(PointLocationVO pointLocationVO) {
        List<Points> pointsList = new ArrayList();
        
        if (pointLocationVO != null && pointLocationVO.getPointList() != null && !pointLocationVO.getPointList().isEmpty()) {
            pointLocationVO.getPointList().stream().filter(pointVO -> pointVO != null).forEachOrdered(pointVO -> pointVO.setAssetId(pointLocationVO.getAssetId()));
            pointLocationVO.getPointList().stream().filter(pointVO -> pointVO != null).forEachOrdered(pointVO -> pointVO.setPointLocationId(pointLocationVO.getPointLocationId()));
            pointLocationVO.getPointList().stream().filter(pointVO -> (pointVO != null && pointVO.getPointId() == null)).forEachOrdered(pointVO -> pointVO.setPointId(UUID.randomUUID()));
            pointLocationVO.getPointList().stream().filter(pointVO -> pointVO != null && pointVO.getApSetId() != null && !pointsList.stream().anyMatch(point -> point.getApSetId().equals(pointVO.getApSetId()))).forEachOrdered(pointVO -> pointsList.add(getPoints(pointVO)));
        }
        return pointsList;
    }
    
    /**
     * Convert List of Points Objects and PointLocationVO Object to List of Points Objects
     * @param dbPointList, List of Points Object
     * @param pointLocationVO, PointLocationVO Object
     * @return List of Points Objects
     */
    public static List<Points> getPointsList(List<Points> dbPointList, PointLocationVO pointLocationVO) {
        List<Points> pointsList = new ArrayList();
        
        if (dbPointList != null && !dbPointList.isEmpty()) {
            for (Points po : dbPointList) {
                Points entity = new Points();
                entity.setAlSetId(po.getAlSetId());
                entity.setApSetId(po.getApSetId());
                entity.setAreaId(po.getAreaId());
                entity.setAssetId(po.getAssetId());
                entity.setAutoAcknowledge(po.isAutoAcknowledge());
                entity.setCustomerAccount(po.getCustomerAccount());
                entity.setDisabled(po.isDisabled());
                entity.setPointLocationId(po.getPointLocationId());
                entity.setPointName(po.getPointName());
                entity.setPointId(po.getPointId());
                entity.setPointType(po.getPointType());
                entity.setSensorChannelNum(po.getSensorChannelNum());
                entity.setSensorOffset(po.getSensorOffset());
                entity.setSensorSensitivity(po.getSensorSensitivity());
                entity.setSensorType(po.getSensorType());
                entity.setSensorUnits(po.getSensorUnits());
                entity.setSiteId(po.getSiteId());
                entity.setAlarmEnabled(po.isAlarmEnabled());
                pointsList.add(entity);
            }
        }
        return pointsList;
    }
    
    /**
     * Convert PointLocationVO Object to List of ApAlByPoint Object
     * @param pointLocationVO, PointLocationVO Object
     * @return List of ApAlByPoint Object
     */
    public static List<ApAlByPoint> getApAlByPointList(PointLocationVO pointLocationVO) {
        List<ApAlByPoint> apAlByPointList = new ArrayList();
        
        if (pointLocationVO != null && pointLocationVO.getPointList() != null && !pointLocationVO.getPointList().isEmpty()) {
            pointLocationVO.getPointList().stream().filter(pointVO -> pointVO != null).forEachOrdered(pointVO -> apAlByPointList.add(getApAlByPoint(pointVO)));
        }
        return apAlByPointList;
    }
    
    /**
     * Convert PointVO Object to Points Object
     * @param pointVO, PointVO Object
     * @return Points Object
     */
    public static Points getPoints(PointVO pointVO) {
        Points entity = new Points();
        
        if (pointVO != null) {
            entity.setAlSetId(pointVO.getAlSetId());
            entity.setApSetId(pointVO.getApSetId());
            entity.setAreaId(pointVO.getAreaId());
            entity.setAssetId(pointVO.getAssetId());
            entity.setAutoAcknowledge(pointVO.isAutoAcknowledge());
            entity.setCustomerAccount(pointVO.getCustomerAccount());
            entity.setDisabled(pointVO.isDisabled());
            entity.setPointLocationId(pointVO.getPointLocationId());
            entity.setPointName(pointVO.getPointName());
            entity.setPointId(pointVO.getPointId());
            entity.setPointType(pointVO.getPointType());
            entity.setSensorChannelNum(pointVO.getSensorChannelNum());
            entity.setSensorOffset(pointVO.getSensorOffset());
            entity.setSensorSensitivity(pointVO.getSensorSensitivity());
            entity.setSensorType(pointVO.getSensorType());
            entity.setSensorUnits(pointVO.getSensorUnits());
            entity.setSensorSubType(pointVO.getSensorSubType());
            entity.setSensorLocalOrientation(pointVO.getSensorLocalOrientation());
            entity.setSiteId(pointVO.getSiteId());
            entity.setAlarmEnabled(pointVO.isAlarmEnabled());
        }
        
        return entity;
    }
    
    /**
     * Convert PointVO Object to ApAlByPoint Object
     * @param pointVO, PointVO Object
     * @return ApAlByPoint Object
     */
    public static ApAlByPoint getApAlByPoint(PointVO pointVO) {
        ApAlByPoint entity = new ApAlByPoint();
        
        if (pointVO != null) {
            entity.setAlSetId(pointVO.getAlSetId());
            entity.setApSetId(pointVO.getApSetId());
            entity.setSiteId(pointVO.getSiteId());
            entity.setAreaId(pointVO.getAreaId());
            entity.setAssetId(pointVO.getAssetId());
            entity.setCustomerAccount(pointVO.getCustomerAccount());
            entity.setPointLocationId(pointVO.getPointLocationId());
            entity.setPointId(pointVO.getPointId());
            entity.setParamName(pointVO.getParamName());
            entity.setSensorType(pointVO.getSensorType());
            entity.setCustomized(pointVO.isCustomized());
            entity.setDemodEnabled(pointVO.isDemodEnabled());
            entity.setDemodHighPass(pointVO.getDemodHighPass());
            entity.setDemodLowPass(pointVO.getDemodLowPass());
            entity.setDwell(pointVO.getDwell());
            entity.setFreqUnits(pointVO.getFreqUnits());
            entity.setHighAlert(pointVO.getHighAlert());
            entity.setHighAlertActive(pointVO.isHighAlertActive());
            entity.setHighFault(pointVO.getHighFault());
            entity.setHighFaultActive(pointVO.isHighFaultActive());
            entity.setLowAlert(pointVO.getLowAlert());
            entity.setLowAlertActive(pointVO.isLowAlertActive());
            entity.setLowFault(pointVO.getLowFault());
            entity.setLowFaultActive(pointVO.isLowFaultActive());
            entity.setMaxFreq(pointVO.getMaxFreq());
            entity.setMinFreq(pointVO.getMinFreq());
            entity.setParamAmpFactor(pointVO.getParamAmpFactor());
            entity.setParamType(pointVO.getParamType());
            entity.setParamUnits(pointVO.getParamUnits());
            entity.setPeriod(pointVO.getPeriod());
            entity.setResolution(pointVO.getResolution());
            entity.setSampleRate(pointVO.getSampleRate());
            entity.setfMax(pointVO.getfMax());
        }
        return entity;
    }
    
    /**
     * Convert PointVO and ApAlSetVO Objects to ApAlByPoint Object
     * @param pointVO, PointVO Object
     * @param apAlSetVO, ApAlSetVO Object
     * @return ApAlByPoint Object
     */
    public static ApAlByPoint getApAlByPoint(PointVO pointVO, ApAlSetVO apAlSetVO) {
        ApAlByPoint entity = new ApAlByPoint();
        
        if (apAlSetVO != null) {
            entity.setAlSetId(apAlSetVO.getAlSetId());
            entity.setApSetId(apAlSetVO.getApSetId());
            entity.setSiteId(pointVO.getSiteId());
            entity.setAreaId(pointVO.getAreaId());
            entity.setAssetId(pointVO.getAssetId());
            entity.setCustomerAccount(pointVO.getCustomerAccount());
            entity.setPointLocationId(pointVO.getPointLocationId());
            entity.setPointId(pointVO.getPointId());
            entity.setParamName(apAlSetVO.getParamName());
            entity.setSensorType(apAlSetVO.getSensorType());
            entity.setCustomized(pointVO.isCustomized());
            entity.setDemodEnabled(apAlSetVO.isDemod());
            entity.setDemodHighPass(apAlSetVO.getDemodHighPass());
            entity.setDemodLowPass(apAlSetVO.getDemodLowPass());
            entity.setDwell(apAlSetVO.getDwell());
            entity.setFreqUnits(apAlSetVO.getFrequencyUnits());
            entity.setHighAlert(apAlSetVO.getHighAlert());
            entity.setHighAlertActive(apAlSetVO.isHighAlertActive());
            entity.setHighFault(apAlSetVO.getHighFault());
            entity.setHighFaultActive(apAlSetVO.isHighFaultActive());
            entity.setLowAlert(apAlSetVO.getLowAlert());
            entity.setLowAlertActive(apAlSetVO.isLowAlertActive());
            entity.setLowFault(apAlSetVO.getLowFault());
            entity.setLowFaultActive(apAlSetVO.isLowFaultActive());
            entity.setMaxFreq(apAlSetVO.getMaxFrequency());
            entity.setMinFreq(apAlSetVO.getMinFrequency());
            entity.setParamAmpFactor(apAlSetVO.getParamAmpFactor());
            entity.setParamType(apAlSetVO.getParamType());
            entity.setParamUnits(apAlSetVO.getParamUnits());
            entity.setPeriod(apAlSetVO.getPeriod());
            entity.setResolution(apAlSetVO.getResolution());
            entity.setSampleRate(apAlSetVO.getSampleRate());
            entity.setfMax(apAlSetVO.getFmax());
        }
        return entity;
    }
    
    /**
     * Convert AssetVO Object to AssetSiteArea Object
     * @param assetVO, AssetVO Object
     * @return AssetSiteArea Object
     */
    public static AssetSiteArea getAssetSiteArea(AssetVO assetVO) {
        AssetSiteArea entity = new AssetSiteArea();
        
        if(assetVO != null) {
            entity.setAreaId(assetVO.getAreaId());
            entity.setAssetId(assetVO.getAssetId());
            entity.setAssetName(assetVO.getAssetName());
            entity.setAssetType(assetVO.getAssetType());
            entity.setCustomerAccount(assetVO.getCustomerAccount());
            entity.setDescription(assetVO.getDescription());
            entity.setSiteId(assetVO.getSiteId());
            entity.setSampleInterval(assetVO.getSampleInterval());
//            entity.setAlarmEnabled(assetVO.isAlarmEnabled());
        }
        
        return entity;
    }

    /**
     * Convert AreaVO Object to AreaSite Object
     * @param areaVO, AreaVO Object
     * @return AreaSite Object
     */
    public static AreaSite getAreaSite(AreaVO areaVO) {
        AreaSite entity = new AreaSite();
        
        if(areaVO != null) {
            entity.setCustomerAccount(areaVO.getCustomerAccount());
            entity.setSiteId(areaVO.getSiteId());
            entity.setAreaId(areaVO.getAreaId());
            entity.setAreaName(areaVO.getAreaName());
            entity.setClimateControlled(areaVO.isClimateControlled());
            entity.setDescription(areaVO.getDescription());            
            entity.setEnvironment(areaVO.getEnvironment());
        }
        return entity;
    }

    /**
     * Convert AreaConfigScheduleVO Object to AreaConfigSchedule Object
     * @param acsVO, AreaConfigScheduleVO Object
     * @return AreaConfigSchedule Object
     */
    public static AreaConfigSchedule getAreaConfigSchedule(AreaConfigScheduleVO acsVO) {
        AreaConfigSchedule entity = new AreaConfigSchedule();
        
        if(acsVO != null) {
            entity.setCustomerAccount(acsVO.getCustomerAccount());
            entity.setSiteId(acsVO.getSiteId());
            entity.setAreaId(acsVO.getAreaId());
            entity.setScheduleId(acsVO.getScheduleId());             
            entity.setDayOfWeek(acsVO.getDayOfWeek());    
            entity.setAreaName(acsVO.getAreaName());
            entity.setDescription(acsVO.getSchDescription());        
            entity.setHoursOfDay(acsVO.getHoursOfDay());
            entity.setContinuous(acsVO.isContinuous());
            entity.setScheduleName(acsVO.getScheduleName());
        }
        return entity;
    }
    
    /**
     * Convert siteContactVO Object to SiteContacts Object
     * @param siteContactVO, SiteContactVO Object
     * @return SiteContacts Object
     */
    public static SiteContacts getSiteContacts(SiteContactVO siteContactVO) {
        SiteContacts entity = new SiteContacts();
        
        if(siteContactVO != null) {
            entity.setSiteId(siteContactVO.getSiteId());
            entity.setContactEmail(siteContactVO.getContactEmail());
            entity.setContactName(siteContactVO.getContactName());
            entity.setContactPhone(siteContactVO.getContactPhone());
            entity.setContactTitle(siteContactVO.getContactTitle());
            entity.setCustomerAccount(siteContactVO.getCustomerAccount());
            entity.setRole(siteContactVO.getRole());
            entity.setSiteName(siteContactVO.getSiteName());
        }
        
        return entity;
    }
    
    /**
     * Convert SiteVO Object to SiteCustomer Object
     * @param siteVO, SiteVO Object
     * @return SiteCustomer Object
     */
    public static SiteCustomer getSiteCustomer(SiteVO siteVO) {
        SiteCustomer entity = new SiteCustomer();
        
        if(siteVO != null) {
            entity.setSiteId(siteVO.getSiteId());
            entity.setBillingAddress1(siteVO.getBillingAddress1());
            entity.setBillingAddress2(siteVO.getBillingAddress2());
            entity.setBillingCity(siteVO.getBillingCity());
            entity.setBillingPostalCode(siteVO.getBillingPostalCode());
            entity.setBillingStateProvince(siteVO.getBillingStateProvince());
            entity.setCountry(siteVO.getCountry());
            entity.setCustomerAccount(siteVO.getCustomerAccount());
            entity.setIndustry(siteVO.getIndustry());
            entity.setLatitude(siteVO.getLatitude());
            entity.setLongitude(siteVO.getLongitude());
            entity.setPhysicalAddress1(siteVO.getPhysicalAddress1());
            entity.setPhysicalAddress2(siteVO.getPhysicalAddress2());
            entity.setPhysicalCity(siteVO.getPhysicalCity());
            entity.setPhysicalPostalCode(siteVO.getPhysicalPostalCode());
            entity.setPhysicalStateProvince(siteVO.getPhysicalStateProvince());
            entity.setRegion(siteVO.getRegion());
            entity.setShipAddress1(siteVO.getShipAddress1());
            entity.setShipAddress2(siteVO.getShipAddress2());
            entity.setShipCity(siteVO.getShipCity());
            entity.setShipPostalCode(siteVO.getShipPostalCode());
            entity.setShipStateProvince(siteVO.getShipStateProvince());
            entity.setSiteName(siteVO.getSiteName());
            entity.setTimezone(siteVO.getTimezone());
        }
        
        return entity;
    }
    
    /**
     * Convert SiteVO Object to SiteRegionCountry Object
     * @param siteVO, SiteVO Object
     * @return SiteRegionCountry Object
     */
    public static SiteRegionCountry getSiteRegionCountry(SiteVO siteVO) {
        SiteRegionCountry entity = new SiteRegionCountry();
        
        if(siteVO != null) {
            entity.setSiteId(siteVO.getSiteId());
            entity.setBillingAddress1(siteVO.getBillingAddress1());
            entity.setBillingAddress2(siteVO.getBillingAddress2());
            entity.setBillingCity(siteVO.getBillingCity());
            entity.setBillingPostalCode(siteVO.getBillingPostalCode());
            entity.setBillingStateProvince(siteVO.getBillingStateProvince());
            entity.setCountry(siteVO.getCountry());
            entity.setCustomerAccount(siteVO.getCustomerAccount());
            entity.setIndustry(siteVO.getIndustry());
            entity.setLatitude(siteVO.getLatitude());
            entity.setLongitude(siteVO.getLongitude());
            entity.setPhysicalAddress1(siteVO.getPhysicalAddress1());
            entity.setPhysicalAddress2(siteVO.getPhysicalAddress2());
            entity.setPhysicalCity(siteVO.getPhysicalCity());
            entity.setPhysicalPostalCode(siteVO.getPhysicalPostalCode());
            entity.setPhysicalStateProvince(siteVO.getPhysicalStateProvince());
            entity.setRegion(siteVO.getRegion());
            entity.setShipAddress1(siteVO.getShipAddress1());
            entity.setShipAddress2(siteVO.getShipAddress2());
            entity.setShipCity(siteVO.getShipCity());
            entity.setShipPostalCode(siteVO.getShipPostalCode());
            entity.setShipStateProvince(siteVO.getShipStateProvince());
            entity.setSiteName(siteVO.getSiteName());
            entity.setTimezone(siteVO.getTimezone());
        }
        
        return entity;
    }

     /**
     * Convert HwUnitVO Object to HwUnitToPoint Object
     * @param hwUnitVO, SiteVO Object
     * @return HwUnitToPoint Object
     */
    public static HwUnitToPoint getHwUnitToPoint(HwUnitVO hwUnitVO) {
        HwUnitToPoint entity = new HwUnitToPoint();
        
        if(hwUnitVO != null) {
            entity.setSiteId(hwUnitVO.getSiteId());
            entity.setCustomerAcct(hwUnitVO.getCustomerAccount());
            entity.setDeviceId(hwUnitVO.getDeviceId());
            entity.setChannelType(hwUnitVO.getChannelType());
            entity.setChannelNum(hwUnitVO.getChannelNum());
            entity.setApSetId(hwUnitVO.getApSetId());
            entity.setAreaId(hwUnitVO.getAreaId());
            entity.setAssetId(hwUnitVO.getAssetId());
            entity.setPointLocationId(hwUnitVO.getPointLocationId());
            entity.setPointId(hwUnitVO.getPointId());
        }
        
        return entity;
    }

     /**
     * Convert Points Object and String Object to HwUnitToPoint Object
     * @param points, Points Object
     * @param deviceId, String Object
     * @return HwUnitToPoint Object
     */
    public static HwUnitToPoint getHwUnitToPoint(Points points, String deviceId) {
        HwUnitToPoint entity = new HwUnitToPoint();
        
        if(points != null && deviceId != null) {
            entity.setSiteId(points.getSiteId());
            entity.setCustomerAcct(points.getCustomerAccount());
            entity.setDeviceId(deviceId);
            entity.setChannelType(points.getPointType());
            entity.setChannelNum((byte) points.getSensorChannelNum());
            entity.setApSetId(points.getApSetId());
            entity.setAreaId(points.getAreaId());
            entity.setAssetId(points.getAssetId());
            entity.setPointLocationId(points.getPointLocationId());
            entity.setPointId(points.getPointId());
        }
        
        return entity;
    }
    
    /**
     * Convert PointToHwUnitVO Object to PointToHwUnit Object
     * @param pointToHwUnitVO, PointToHwUnitVO Object
     * @return PointToHwUnit Object
     */
    public static PointToHwUnit getPointToHwUnit(PointToHwUnitVO pointToHwUnitVO) {
        PointToHwUnit entity = new PointToHwUnit();
        
        if(pointToHwUnitVO != null) {
            entity.setDeviceId(pointToHwUnitVO.getDeviceId());
            entity.setChannelType(pointToHwUnitVO.getChannelType());
            entity.setChannelNum(pointToHwUnitVO.getChannelNum());
            entity.setPointId(pointToHwUnitVO.getPointId());
        }
        
        return entity;
    }
    
    /**
     * Convert Points Object and String Object to PointToHwUnit Object
     * @param points, Points Object
     * @param deviceId, String Object
     * @return PointToHwUnit Object
     */
    public static PointToHwUnit getPointToHwUnit(Points points, String deviceId) {
        PointToHwUnit entity = new PointToHwUnit();
        
        if(points != null && deviceId != null) {
            entity.setDeviceId(deviceId);
            entity.setChannelType(points.getPointType());
            entity.setChannelNum((byte) points.getSensorChannelNum());
            entity.setPointId(points.getPointId());
        }
        
        return entity;
    }
    
    /**
     * Convert HwUnitVO Object to Points Object
     * @param hwUnitVO, HwUnitVO Object
     * @return Points Object
     */
    public static Points getPoint(HwUnitVO hwUnitVO) {
        Points entity = new Points();
        
        if(hwUnitVO != null) {
            entity.setAlSetId(hwUnitVO.getAlSetId());
            entity.setApSetId(hwUnitVO.getApSetId());
            entity.setAreaId(hwUnitVO.getAreaId());
            entity.setAssetId(hwUnitVO.getAssetId());
            entity.setAutoAcknowledge(hwUnitVO.isAutoAcknowledge());
            entity.setCustomerAccount(hwUnitVO.getCustomerAccount());
            entity.setDisabled(hwUnitVO.isDisabled());
            entity.setPointLocationId(hwUnitVO.getPointLocationId());
            entity.setPointName(hwUnitVO.getPointName());
            entity.setPointId(hwUnitVO.getPointId());
            entity.setPointType(hwUnitVO.getPointType());
            entity.setSensorChannelNum(hwUnitVO.getSensorChannelNum());
            entity.setSensorOffset(hwUnitVO.getSensorOffset());
            entity.setSensorSensitivity(hwUnitVO.getSensorSensitivity());
            entity.setSensorType(hwUnitVO.getApAlSetVOs().get(0).getSensorType());
            entity.setSensorUnits(hwUnitVO.getSensorUnits());
            entity.setSiteId(hwUnitVO.getSiteId());
            entity.setAlarmEnabled(hwUnitVO.isAlarmEnabled());
        }
        
        return entity;
    }
    
    /**
     * Convert HwUnitVO Object to List of ApAlByPoint Objects
     * @param hwUnitVO, HwUnitVO Object
     * @return List of ApAlByPoint Objects
     */
    public static List<ApAlByPoint> getApAlByPointList(HwUnitVO hwUnitVO) {
        List<ApAlByPoint> list = new ArrayList();
        
        
        if(hwUnitVO != null && hwUnitVO.getApAlSetVOs() != null) {
            hwUnitVO.getApAlSetVOs().stream().forEachOrdered(apAlSetVO -> {
                ApAlByPoint entity = new ApAlByPoint();
                entity.setAlSetId(hwUnitVO.getAlSetId());
                entity.setApSetId(hwUnitVO.getApSetId());
                entity.setSiteId(hwUnitVO.getSiteId());
                entity.setAreaId(hwUnitVO.getAreaId());
                entity.setAssetId(hwUnitVO.getAssetId());
                entity.setCustomerAccount(hwUnitVO.getCustomerAccount());
                entity.setPointLocationId(hwUnitVO.getPointLocationId());
                entity.setPointId(hwUnitVO.getPointId());
                entity.setParamName(apAlSetVO.getParamName());
                entity.setParamType(apAlSetVO.getParamType());
                entity.setParamUnits(apAlSetVO.getParamUnits());
                entity.setDemodEnabled(apAlSetVO.isDemod());
                entity.setDemodHighPass(apAlSetVO.getDemodHighPass());
                entity.setDemodLowPass(apAlSetVO.getDemodLowPass());
                entity.setDwell(apAlSetVO.getDwell());
                entity.setfMax(apAlSetVO.getFmax());
                entity.setFreqUnits(apAlSetVO.getFrequencyUnits());
                entity.setHighAlert(apAlSetVO.getHighAlert());
                entity.setHighAlertActive(apAlSetVO.isHighAlertActive());
                entity.setSensorType(apAlSetVO.getSensorType());
                entity.setHighFault(apAlSetVO.getHighFault());
                entity.setHighFaultActive(apAlSetVO.isHighFaultActive());
                entity.setLowAlert(apAlSetVO.getLowAlert());
                entity.setLowAlertActive(apAlSetVO.isLowAlertActive());
                entity.setLowFault(apAlSetVO.getLowFault());
                entity.setLowFaultActive(apAlSetVO.isLowFaultActive());
                entity.setMaxFreq(apAlSetVO.getMaxFrequency());
                entity.setMinFreq(apAlSetVO.getMinFrequency());
                entity.setParamAmpFactor(apAlSetVO.getParamAmpFactor());
                entity.setPeriod(apAlSetVO.getPeriod());
                entity.setResolution(apAlSetVO.getResolution());
                entity.setSampleRate(apAlSetVO.getSampleRate());
                list.add(entity);
            });
        }
        
        return list;
    }

    /**
     * Convert PointVO Object to List of ApAlByPoint Objects
     * @param point, PointVO Object
     * @return List of ApAlByPoint Objects
     */
    public static List<ApAlByPoint> getApAlByPointList(PointVO point) {
        List<ApAlByPoint> list = new ArrayList();
       
        if (point != null && point.getApAlSetVOs() != null) {
            point.getApAlSetVOs().stream().forEachOrdered(apAlSetVO -> {
                ApAlByPoint entity = new ApAlByPoint();
                entity.setAlSetId(point.getAlSetId());
                entity.setApSetId(point.getApSetId());
                entity.setSiteId(point.getSiteId());
                entity.setAreaId(point.getAreaId());
                entity.setAssetId(point.getAssetId());
                entity.setCustomerAccount(point.getCustomerAccount());
                entity.setPointLocationId(point.getPointLocationId());
                entity.setPointId(point.getPointId());
                entity.setCustomized(point.isCustomized());
                entity.setParamName(apAlSetVO.getParamName());
                entity.setParamType(apAlSetVO.getParamType());
                entity.setParamUnits(apAlSetVO.getParamUnits());
                entity.setDemodHighPass(apAlSetVO.getDemodHighPass());
                entity.setDemodLowPass(apAlSetVO.getDemodLowPass());
                entity.setDwell(apAlSetVO.getDwell());
                entity.setfMax(apAlSetVO.getFmax());
                entity.setFreqUnits(apAlSetVO.getFrequencyUnits());
                entity.setHighAlert(apAlSetVO.getHighAlert());
                entity.setHighAlertActive(apAlSetVO.isHighAlertActive());
                entity.setSensorType(apAlSetVO.getSensorType());
                entity.setHighFault(apAlSetVO.getHighFault());
                entity.setHighFaultActive(apAlSetVO.isHighFaultActive());
                entity.setLowAlert(apAlSetVO.getLowAlert());
                entity.setLowAlertActive(apAlSetVO.isLowAlertActive());
                entity.setLowFault(apAlSetVO.getLowFault());
                entity.setLowFaultActive(apAlSetVO.isLowFaultActive());
                entity.setMaxFreq(apAlSetVO.getMaxFrequency());
                entity.setMinFreq(apAlSetVO.getMinFrequency());
                entity.setParamAmpFactor(apAlSetVO.getParamAmpFactor());
                entity.setPeriod(apAlSetVO.getPeriod());
                entity.setResolution(apAlSetVO.getResolution());
                entity.setSampleRate(apAlSetVO.getSampleRate());
                entity.setSensorType(apAlSetVO.getSensorType());
                list.add(entity);
            });
        }
        
        return list;
    }
    
    /**
     * Convert PointLocations Object,Points Object and ApAlByPoint Object to SiteBasestationDevice Object
     * @param pointLocations, PointLocations Object
     * @param point, Points Object
     * @param apAlByPoint, ApAlByPoint Object
     * @return SiteBasestationDevice Object
     */
    public static SiteBasestationDevice getSiteBasestationDevice(PointLocations pointLocations, Points point, ApAlByPoint apAlByPoint) {
        SiteBasestationDevice entity = new SiteBasestationDevice();
        
        if (pointLocations != null && point != null && apAlByPoint != null) {
            entity.setCustomerAccount(point.getCustomerAccount());
            entity.setSiteId(point.getSiteId());
            entity.setPointId(point.getPointId());
            entity.setDeviceId(pointLocations.getDeviceSerialNumber());
            entity.setBaseStationPort(pointLocations.getBasestationPortNum());
            entity.setChannelType(getChannelTypeBySensorType(point.getSensorType()));
            entity.setSensorChannelNum((byte)point.getSensorChannelNum());
            entity.setFmax(apAlByPoint.getfMax());
            entity.setResolution(apAlByPoint.getResolution());
            entity.setSampleInterval(pointLocations.getSampleInterval());
            entity.setLastSampled(null);
        }
        
        return entity;
    }
    
    /**
     * Convert SiteBasestationDeviceVO Object to SiteBasestationDevice Object
     * @param siteBasestationDeviceVO, SiteBasestationDeviceVO Object
     * @return SiteBasestationDevice Object
     */
    public static SiteBasestationDevice getSiteBasestationDevice(SiteBasestationDeviceVO siteBasestationDeviceVO) {
        SiteBasestationDevice entity = new SiteBasestationDevice();

        if (siteBasestationDeviceVO != null) {
            entity.setCustomerAccount(siteBasestationDeviceVO.getCustomerAccount());
            entity.setSiteId(siteBasestationDeviceVO.getSiteId());
            entity.setPointId(siteBasestationDeviceVO.getPointId());
            entity.setDeviceId(siteBasestationDeviceVO.getDeviceId());
            entity.setBaseStationPort(siteBasestationDeviceVO.getBaseStationPort());
            entity.setChannelType(siteBasestationDeviceVO.getChannelType());
            entity.setSensorChannelNum(siteBasestationDeviceVO.getSensorChannelNum());
            entity.setFmax(siteBasestationDeviceVO.getFmax());
            entity.setResolution(siteBasestationDeviceVO.getResolution());
            entity.setSampleInterval(siteBasestationDeviceVO.getSampleInterval());
            entity.setLastSampled(null);
        }

        return entity;
    }

    /**
     * Convert SiteBasestationDeviceVO Object to SiteDeviceBasestation Object
     * @param siteBasestationDevice, SiteBasestationDevice Object
     * @return SiteDeviceBasestation Object
     */
    public static SiteDeviceBasestation getSiteDeviceBasestation(SiteBasestationDevice siteBasestationDevice) {
        SiteDeviceBasestation entity = new SiteDeviceBasestation();

        if (siteBasestationDevice != null) {
            entity.setCustomerAccount(siteBasestationDevice.getCustomerAccount());
            entity.setSiteId(siteBasestationDevice.getSiteId());
            entity.setDeviceId(siteBasestationDevice.getDeviceId());
            entity.setBaseStationPort(siteBasestationDevice.getBaseStationPort());
        }

        return entity;
    }

    /**
     * Convert SubscriptionsVO Object to Subscriptions Object
     * @param subscriptionsVO, SubscriptionsVO Object
     * @return Subscriptions Object
     */
    public static Subscriptions getSubscriptions(SubscriptionsVO subscriptionsVO) {
        Subscriptions entity = new Subscriptions();
        
        if(subscriptionsVO != null) {
            entity.setCustomerAccount(subscriptionsVO.getCustomerAccount());
            entity.setSiteId(subscriptionsVO.getSiteId());
            entity.setInternal(subscriptionsVO.isInternal());
            entity.setMqClientId(subscriptionsVO.getMqClientId());
            entity.setMqConnectString(subscriptionsVO.getMqConnectString());
            entity.setMqProtocol(subscriptionsVO.getMqProtocol());
            entity.setMqPwd(subscriptionsVO.getMqPwd());
            entity.setMqQueueName(subscriptionsVO.getMqQueueName());
            entity.setMqUser(subscriptionsVO.getMqUser());
            entity.setSubscriptionType(subscriptionsVO.getSubscriptionType());
            entity.setWebhookUrl(subscriptionsVO.getWebhookUrl());
        }
        return entity;
    }
        
    /**
     * Convert DeviceVO Object to DeviceStatusVO Object
     * @param deviceVO, DeviceVO Object
     * @param deviceId, String Object
     * @return DeviceStatusVO Object
     */
    public static DeviceStatusVO getDeviceStatus(DeviceVO deviceVO, String deviceId) {
        DeviceStatusVO entity = new DeviceStatusVO();
        if(deviceVO != null && deviceId != null && !deviceId.isEmpty()) {
            entity.setDeviceId(deviceId);
            entity.setAreaName(deviceVO.getAreaName());
            entity.setAssetName(deviceVO.getAssetName());
            entity.setBatteryVoltage(deviceVO.getBatteryVoltage());
            entity.setCustomerAcct(deviceVO.getCustomerAccount());
            entity.setIsDisabled(deviceVO.isDisabled());
            entity.setLastSample(deviceVO.getLastSample());
            entity.setPointLocationName(deviceVO.getPointLocationName());
            entity.setSiteName(deviceVO.getSiteName());
        }
        return entity;
    }
        
    public static String getChannelTypeBySensorType(String sensorType) {
        String channelType = null;
        
        switch (sensorType)
        {
            case "Temperature" : return "DC";
            case "Pressure" : return "DC";
            case "Frequency" : return "DC";
            case "Voltage" : return "DC";
            case "Current" : return "DC";
            case "Phase" : return "DC";
            case "Acceleration" : return "AC";
            case "Velocity" : return "AC";
            case "Displacement" : return "AC";
            case "Ultrasonic" : return "AC";
        }
        return channelType;
    }
}
