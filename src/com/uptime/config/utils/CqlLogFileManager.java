/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author kpati
 */
public class CqlLogFileManager {

    private static int dayOfMonth = -1;	 // The current day of the month
    private Logger CQL_LOGGER;
    private FileHandler cqlFile;
    private String pattern;

    public CqlLogFileManager(Logger CQL_LOGGER, FileHandler cqlFile, String pattern) {
        this.CQL_LOGGER = CQL_LOGGER;
        this.cqlFile = cqlFile;
        this.pattern = pattern;
        long delay = 1;
        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(1);
        scheduledThreadPool.scheduleAtFixedRate(() -> manageCQLLogFiles(), 0, delay, TimeUnit.SECONDS);

    }

    private static boolean dayOfMonthHasChanged(Calendar calendar) {
        return calendar.get(Calendar.DAY_OF_MONTH) != dayOfMonth;
    }

    public static String getCurrentDirectory() {
        Path currentDirectoryPath = FileSystems.getDefault().getPath("");
        String currentDirectoryName = currentDirectoryPath.toAbsolutePath().toString();
        return currentDirectoryName;
    }

    public static String getCurrentDate() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        return df.format(date);
    }

    public static String getFileName() {
        String fileName = "";
        Calendar calendar = Calendar.getInstance();
        try {
            if (dayOfMonthHasChanged(calendar)) {

                StringBuilder pathBuilder = new StringBuilder();

                dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

                pathBuilder.append(getCurrentDirectory());
                pathBuilder.append(File.separator);
                pathBuilder.append(previousDateString(getCurrentDate(), 1));
                pathBuilder.append(".cql");
                pathBuilder.append(".txt");

                fileName = pathBuilder.toString();
            }
        } catch (Exception exp) {
            System.out.println("Exception occured while creating fileName: "+exp.getMessage());
        }

        return fileName;
    }

    public void manageCQLLogFiles() {
        try {
            //DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            String currentTime = dtf.format(now);
            //System.out.println("currentTime-->" + currentTime);
            if (currentTime.equals("00:00:01")){
                flush();
                deleteOldFiles();
                Thread.sleep(3000);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void flush() {
        try {
            CQLFileHandler handler = new CQLFileHandler(pattern);
            File currentFile = handler.getCurrentLogFile();
            cqlFile.close();
            currentFile.renameTo(new File(getFileName()));
//            FileHandler cqlFile = new FileHandler(pattern);
//            cqlFile.setFormatter(new SimpleFormatter());
//            CQL_LOGGER.setUseParentHandlers(false);
//            CQL_LOGGER.addHandler(cqlFile);
        } catch (Exception exp) {
            System.out.println("exp Occured --> " + exp.getMessage());
        }
        //close the file
    }

    private static File[] getAllExsistingCQLFiles() {
        File dir = new File(getCurrentDirectory());
        File[] files = dir.listFiles((dir1, name) -> name.contains("cql"));
        return files;
    }

    private static void deleteOldFiles() {

        try {
            File[] files = getAllExsistingCQLFiles();
            String oldFile = previousDateString(getCurrentDate(), 14);
            System.out.println("oldFilestr--> " + oldFile);
            for (File file : files) {

                String fileName = file.getName().substring(0, 10);
                //delete the 14 days old file
                if (fileName.contains(oldFile)) {
                    file.deleteOnExit();
                }

            }
        } catch (Exception exp) {
            System.out.println("Exception : " + exp);
        }
    }

    public static String previousDateString(String dateString, int dayesOld)
            throws ParseException {
        // Create a date formatter using your format string
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        // Parse the given date string into a Date object.
        // Note: This can throw a ParseException.
        Date myDate = dateFormat.parse(dateString);

        // Use the Calendar class to subtract 14 day
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(myDate);
        calendar.add(Calendar.DAY_OF_YEAR, -dayesOld);

        // Use the date formatter to produce a formatted date string
        Date previousDate = calendar.getTime();
        String result = dateFormat.format(previousDate);
        System.out.println("result-->" + result);
        return result;
    }

    public static void main(String[] arg) {
        System.out.println("Hiii--------------> " + getFileName());
    }
}
