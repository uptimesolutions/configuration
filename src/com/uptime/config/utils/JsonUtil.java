/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uptime.cassandra.config.entity.Subscriptions;
import com.uptime.config.ConfigService;
import com.uptime.config.vo.ApAlSetVO;
import com.uptime.config.vo.AreaConfigScheduleVO;
import com.uptime.config.vo.AreaVO;
import com.uptime.config.vo.AssetVO;
import com.uptime.config.vo.DeviceStatusVO;
import com.uptime.config.vo.HwUnitVO;
import com.uptime.config.vo.PointLocationVO;
import com.uptime.config.vo.PointVO;
import com.uptime.config.vo.DeviceVO;
import com.uptime.config.vo.SiteContactVO;
import com.uptime.config.vo.SiteVO;
import com.uptime.config.vo.SubscriptionsVO;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class JsonUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonUtil.class.getName());

    /**
     * Parse the given json String Object and return a AssetVO object
     *
     * @param content, String Object
     * @return AssetVO Object
     */
    public static AssetVO parser(String content) {
        List<PointLocationVO> pointLocationVOList;
        PointLocationVO pointLocationVO;
        AssetVO assetVO = null;
        JsonElement element;
        JsonObject jsonAsset, jsonPL, jsonPoint, jsonApAlSet;
        JsonArray jsonArray, jsonArray2, jsonArray3;
        List<PointVO> pointVOList;
        PointVO pointVO;
        List<ApAlSetVO> apAlByPointVOList;
        ApAlSetVO apAlSetVO;

        System.out.println("Json - " + content);

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonAsset = element.getAsJsonObject()) != null) {
            assetVO = new AssetVO();

            if (jsonAsset.has("assetId") && !jsonAsset.get("assetId").isJsonNull()) {
                try {
                    assetVO.setAssetId(UUID.fromString(jsonAsset.get("assetId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in assetId - " + ex.toString());
                }
            }

            if (jsonAsset.has("customerAccount")) {
                try {
                    assetVO.setCustomerAccount(jsonAsset.get("customerAccount").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in customerAccount - " + ex.toString());
                }
            }

            if (jsonAsset.has("siteId")) {
                try {
                    assetVO.setSiteId(UUID.fromString(jsonAsset.get("siteId").getAsString()));
                } catch (Exception ex) {
                    System.out.println("Exception in siteId - " + ex.toString());
                }
            }

            if (jsonAsset.has("siteName")) {
                try {
                    assetVO.setSiteName(jsonAsset.get("siteName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in siteName - " + ex.toString());
                }
            }

            if (jsonAsset.has("areaId")) {
                try {
                    assetVO.setAreaId(UUID.fromString(jsonAsset.get("areaId").getAsString()));
                } catch (Exception ex) {
                    System.out.println("Exception in areaId - " + ex.toString());
                }
            }

            if (jsonAsset.has("areaName")) {
                try {
                    assetVO.setAreaName(jsonAsset.get("areaName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in areaName - " + ex.toString());
                }
            }

            if (jsonAsset.has("assetName")) {
                try {
                    assetVO.setAssetName(jsonAsset.get("assetName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in assetName - " + ex.toString());
                }
            }

            if (jsonAsset.has("assetType")) {
                try {
                    assetVO.setAssetType(jsonAsset.get("assetType").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in assetType - " + ex.toString());
                }
            }

            if (jsonAsset.has("description")) {
                try {
                    assetVO.setDescription(jsonAsset.get("description").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in description - " + ex.toString());
                }
            }

            if (jsonAsset.has("sampleInterval")) {
                try {
                    assetVO.setSampleInterval(jsonAsset.get("sampleInterval").getAsInt());
                } catch (Exception ex) {
                    System.out.println("Exception in sampleInterval - " + ex.toString());
                }
            }

            if (jsonAsset.has("alarmEnabled")) {
                try {
                    assetVO.setAlarmEnabled(jsonAsset.get("alarmEnabled").getAsBoolean());
                } catch (Exception ex) {
                    System.out.println("Exception in alarmEnabled - " + ex.toString());
                }
            }

            if (jsonAsset.has("sensorTypes")) {
                try {
                    List<String> sensorTypeList = Arrays.asList(jsonAsset.get("sensorTypes").getAsString().split(","));
                    assetVO.setSensorList(sensorTypeList);
                } catch (Exception ex) {
                    System.out.println("Exception in assetVO sensorTypeList - " + ex.toString());
                }
            }
            if (jsonAsset.has("pointLocationList")) {
                try {
                    if ((jsonArray = jsonAsset.getAsJsonArray("pointLocationList")) != null && !jsonArray.isEmpty()) {
                        pointLocationVOList = new ArrayList();
                        for (int i = 0; i < jsonArray.size(); i++) {
                            jsonPL = jsonArray.get(i).getAsJsonObject();
                            pointLocationVO = new PointLocationVO();
                            pointLocationVO.setCustomerAccount(assetVO.getCustomerAccount());
                            pointLocationVO.setSiteId(assetVO.getSiteId());
                            pointLocationVO.setAreaId(assetVO.getAreaId());
                            pointLocationVO.setAssetId(assetVO.getAssetId());

                            if (jsonPL.has("pointLocationId") && !jsonPL.get("pointLocationId").isJsonNull()) {
                                try {
                                    pointLocationVO.setPointLocationId(UUID.fromString(jsonPL.get("pointLocationId").getAsString().trim()));
                                } catch (Exception ex) {
                                    System.out.println("Exception in pointLocationId - " + ex.toString());
                                }
                            }

                            if (jsonPL.has("pointLocationName")) {
                                try {
                                    pointLocationVO.setPointLocationName(jsonPL.get("pointLocationName").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in pointLocationName - " + ex.toString());
                                }
                            }

                            if (jsonPL.has("description")) {
                                try {
                                    pointLocationVO.setDescription(jsonPL.get("description").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in description - " + ex.toString());
                                }
                            }

                            if (jsonPL.has("deviceSerialNumber")) {
                                try {
                                    pointLocationVO.setDeviceSerialNumber(jsonPL.get("deviceSerialNumber").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in deviceSerialNumber - " + ex.toString());
                                }
                            }

                            if (jsonPL.has("ffSetIds")) {
                                try {
                                    pointLocationVO.setFfSetIds(jsonPL.get("ffSetIds").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in ffSetIds - " + ex.toString());
                                }
                            }

                            if (jsonPL.has("speedRatio")) {
                                try {
                                    pointLocationVO.setSpeedRatio(jsonPL.get("speedRatio").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in speedRatio - " + ex.toString());
                                }
                            }

                            if (jsonPL.has("sampleInterval")) {
                                try {
                                    pointLocationVO.setSampleInterval(jsonPL.get("sampleInterval").getAsShort());
                                } catch (Exception ex) {
                                    System.out.println("Exception in sampleInterval - " + ex.toString());
                                }
                            }

                            if (jsonPL.has("basestationPortNum")) {
                                try {
                                    pointLocationVO.setBasestationPortNum(jsonPL.get("basestationPortNum").getAsShort());
                                } catch (Exception ex) {
                                    System.out.println("Exception in basestationPortNum - " + ex.toString());
                                }
                            }

                            if (jsonPL.has("deviceSerialNumber")) {
                                try {
                                    pointLocationVO.setDeviceSerialNumber(jsonPL.get("deviceSerialNumber").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in deviceSerialNumber - " + ex.toString());
                                }
                            }

                            if (jsonPL.has("tachId")) {
                                try {
                                    pointLocationVO.setTachId(UUID.fromString(jsonPL.get("tachId").getAsString().trim()));
                                } catch (Exception ex) {
                                    System.out.println("Exception in tachId - " + ex.toString());
                                }
                            }

                            if (jsonPL.has("alarmEnabled")) {
                                try {
                                    pointLocationVO.setAlarmEnabled(jsonPL.get("alarmEnabled").getAsBoolean());
                                } catch (Exception ex) {
                                    System.out.println("Exception in pointLocationVO alarmEnabled - " + ex.toString());
                                }
                            }

                            if (jsonPL.has("rollDiameter")) {
                                try {
                                    pointLocationVO.setRollDiameter(jsonPL.get("rollDiameter").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in pointLocationVO rollDiameter - " + ex.toString());
                                }
                            }

                            if (jsonPL.has("rollDiameterUnits")) {
                                try {
                                    pointLocationVO.setRollDiameterUnits(jsonPL.get("rollDiameterUnits").getAsString());
                                } catch (Exception ex) {
                                    System.out.println("Exception in pointLocationVO rollDiameterUnits - " + ex.toString());
                                }
                            }

                            if (jsonPL.has("pointList")) {
                                try {
                                    if ((jsonArray2 = jsonPL.getAsJsonArray("pointList")) != null && !jsonArray2.isEmpty()) {
                                        pointVOList = new ArrayList();

                                        for (int j = 0; j < jsonArray2.size(); j++) {
                                            jsonPoint = jsonArray2.get(j).getAsJsonObject();
                                            pointVO = new PointVO();
                                            pointVO.setCustomerAccount(pointLocationVO.getCustomerAccount());
                                            pointVO.setSiteId(pointLocationVO.getSiteId());
                                            pointVO.setAreaId(pointLocationVO.getAreaId());
                                            pointVO.setAssetId(pointLocationVO.getAssetId());
                                            pointVO.setPointLocationId(pointLocationVO.getPointLocationId());

                                            if (jsonPoint.has("pointId") && !jsonPoint.get("pointId").isJsonNull()) {
                                                try {
                                                    pointVO.setPointId(UUID.fromString(jsonPoint.get("pointId").getAsString().trim()));
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in pointId - " + ex.toString());
                                                }
                                            }

                                            if (jsonPoint.has("pointLocationId")) {
                                                try {
                                                    pointVO.setPointLocationId(UUID.fromString(jsonPoint.get("pointLocationId").getAsString().trim()));
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in apSetId - " + ex.toString());
                                                }
                                            }

                                            if (jsonPoint.has("apSetId")) {
                                                try {
                                                    pointVO.setApSetId(UUID.fromString(jsonPoint.get("apSetId").getAsString().trim()));
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in alSetId - " + ex.toString());
                                                }
                                            }

                                            if (jsonPoint.has("alSetId")) {
                                                try {
                                                    pointVO.setAlSetId(UUID.fromString(jsonPoint.get("alSetId").getAsString().trim()));
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in alSetId - " + ex.toString());
                                                }
                                            }

                                            if (jsonPoint.has("pointName")) {
                                                try {
                                                    pointVO.setPointName(jsonPoint.get("pointName").getAsString().trim());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in pointName - " + ex.toString());
                                                }
                                            }

                                            if (jsonPoint.has("autoAcknowledge")) {
                                                try {
                                                    pointVO.setAutoAcknowledge(jsonPoint.get("autoAcknowledge").getAsBoolean());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in autoAcknowledge - " + ex.toString());
                                                }
                                            }

                                            if (jsonPoint.has("disabled")) {
                                                try {
                                                    pointVO.setDisabled(jsonPoint.get("disabled").getAsBoolean());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in isDisabled - " + ex.toString());
                                                }
                                            }

                                            if (jsonPoint.has("pointType")) {
                                                try {
                                                    pointVO.setPointType(jsonPoint.get("pointType").getAsString().trim());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in pointType - " + ex.toString());
                                                }
                                            }

                                            if (jsonPoint.has("sensorChannelNum")) {
                                                try {
                                                    pointVO.setSensorChannelNum(jsonPoint.get("sensorChannelNum").getAsInt());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in sensorChannelNum - " + ex.toString());
                                                }
                                            }

                                            if (jsonPoint.has("sensorOffset")) {
                                                try {
                                                    pointVO.setSensorOffset(jsonPoint.get("sensorOffset").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in sensorOffset - " + ex.toString());
                                                }
                                            }

                                            if (jsonPoint.has("sensorSensitivity")) {
                                                try {
                                                    pointVO.setSensorSensitivity(jsonPoint.get("sensorSensitivity").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in sensorSensitivity - " + ex.toString());
                                                }
                                            }

                                            if (jsonPoint.has("sensorType")) {
                                                try {
                                                    pointVO.setSensorType(jsonPoint.get("sensorType").getAsString().trim());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in sensorType - " + ex.toString());
                                                }
                                            }

                                            if (jsonPoint.has("sensorUnits")) {
                                                try {
                                                    pointVO.setSensorUnits(jsonPoint.get("sensorUnits").getAsString().trim());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in sensorUnits - " + ex.toString());
                                                }
                                            }

                                            if (jsonPoint.has("alarmEnabled")) {
                                                try {
                                                    pointVO.setAlarmEnabled(jsonPoint.get("alarmEnabled").getAsBoolean());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in pointVO alarmEnabled - " + ex.toString());
                                                }
                                            }
                                            
                                            if (jsonPoint.has("sensorSubType")) {
                                                try {
                                                    pointVO.setSensorSubType(jsonPoint.get("sensorSubType").getAsString().trim());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in pointVO sensorSubType - " + ex.toString());
                                                }
                                            }
                                            
                                            if (jsonPoint.has("sensorLocalOrientation")) {
                                                try {
                                                    pointVO.setSensorLocalOrientation(jsonPoint.get("sensorLocalOrientation").getAsString().trim());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in pointVO sensorLocalOrientation - " + ex.toString());
                                                }
                                            }

                                            if (jsonPoint.has("apAlSetVOs")) {
                                                try {
                                                    if ((jsonArray3 = jsonPoint.getAsJsonArray("apAlSetVOs")) != null && !jsonArray3.isEmpty()) {
                                                        apAlByPointVOList = new ArrayList();
                                                        for (int k = 0; k < jsonArray3.size(); k++) {
                                                            jsonApAlSet = jsonArray3.get(k).getAsJsonObject();
                                                            apAlSetVO = new ApAlSetVO();

                                                            if (jsonApAlSet.has("apSetId")) {
                                                                try {
                                                                    apAlSetVO.setApSetId(UUID.fromString(jsonApAlSet.get("apSetId").getAsString().trim()));
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in apSetId - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("alSetId")) {
                                                                try {
                                                                    apAlSetVO.setAlSetId(UUID.fromString(jsonApAlSet.get("alSetId").getAsString().trim()));
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in alSetId - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("paramName")) {
                                                                try {
                                                                    apAlSetVO.setParamName(jsonApAlSet.get("paramName").getAsString().trim());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in paramNamee - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("fmax")) {
                                                                try {
                                                                    apAlSetVO.setFmax(jsonApAlSet.get("fmax").getAsInt());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in fmax - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("resolution")) {
                                                                try {
                                                                    apAlSetVO.setResolution(jsonApAlSet.get("resolution").getAsInt());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in resolution - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("dwell")) {
                                                                try {
                                                                    apAlSetVO.setDwell(jsonApAlSet.get("dwell").getAsInt());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in dwell - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("period")) {
                                                                try {
                                                                    apAlSetVO.setPeriod(jsonApAlSet.get("period").getAsFloat());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in period - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("sampleRate")) {
                                                                try {
                                                                    apAlSetVO.setSampleRate(jsonApAlSet.get("sampleRate").getAsFloat());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in sampleRate - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("minFrequency")) {
                                                                try {
                                                                    apAlSetVO.setMinFrequency(jsonApAlSet.get("minFrequency").getAsFloat());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in minFreq - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("maxFrequency")) {
                                                                try {
                                                                    apAlSetVO.setMaxFrequency(jsonApAlSet.get("maxFrequency").getAsFloat());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in maxFrequency - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("lowFault")) {
                                                                try {
                                                                    apAlSetVO.setLowFault(jsonApAlSet.get("lowFault").getAsFloat());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in lowFault - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("lowAlert")) {
                                                                try {
                                                                    apAlSetVO.setLowAlert(jsonApAlSet.get("lowAlert").getAsFloat());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in lowAlert - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("highAlert")) {
                                                                try {
                                                                    apAlSetVO.setHighAlert(jsonApAlSet.get("highAlert").getAsFloat());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in highAlert - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("highFault")) {
                                                                try {
                                                                    apAlSetVO.setHighFault(jsonApAlSet.get("highFault").getAsFloat());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in highFault - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("demodHighPass")) {
                                                                try {
                                                                    apAlSetVO.setDemodHighPass(jsonApAlSet.get("demodHighPass").getAsFloat());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in demodHighPass - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("demodLowPass")) {
                                                                try {
                                                                    apAlSetVO.setDemodLowPass(jsonApAlSet.get("demodLowPass").getAsFloat());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in demodLowPass - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("lowFaultActive")) {
                                                                try {
                                                                    apAlSetVO.setLowFaultActive(jsonApAlSet.get("lowFaultActive").getAsBoolean());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in lowFaultActive - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("lowAlertActive")) {
                                                                try {
                                                                    apAlSetVO.setLowAlertActive(jsonApAlSet.get("lowAlertActive").getAsBoolean());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in lowAlertActive - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("highAlertActive")) {
                                                                try {
                                                                    apAlSetVO.setHighAlertActive(jsonApAlSet.get("highAlertActive").getAsBoolean());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in highAlertActive - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("highFaultActive")) {
                                                                try {
                                                                    apAlSetVO.setHighFaultActive(jsonApAlSet.get("highFaultActive").getAsBoolean());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in highFaultActive - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("demod")) {
                                                                try {
                                                                    apAlSetVO.setDemod(jsonApAlSet.get("demod").getAsBoolean());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in demod - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("paramType")) {
                                                                try {
                                                                    apAlSetVO.setParamType(jsonApAlSet.get("paramType").getAsString().trim());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in paramType - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("frequencyUnits")) {
                                                                try {
                                                                    apAlSetVO.setFrequencyUnits(jsonApAlSet.get("frequencyUnits").getAsString().trim());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in freqUnits - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("paramUnits")) {
                                                                try {
                                                                    apAlSetVO.setParamUnits(jsonApAlSet.get("paramUnits").getAsString().trim());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in paramUnits - " + ex.toString());
                                                                }
                                                            }

                                                            if (jsonApAlSet.has("paramAmpFactor")) {
                                                                try {
                                                                    apAlSetVO.setParamAmpFactor(jsonApAlSet.get("paramAmpFactor").getAsString().trim());
                                                                } catch (Exception ex) {
                                                                    System.out.println("Exception in paramAmpFactor - " + ex.toString());
                                                                }
                                                            }

                                                            apAlSetVO.setSensorType(pointVO.getSensorType());
                                                            apAlByPointVOList.add(apAlSetVO);
                                                        }
                                                        pointVO.setApAlSetVOs(apAlByPointVOList);
                                                    }
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in apAlSetVOs - " + ex.toString());
                                                }
                                            }
                                            pointVOList.add(pointVO);
                                        }
                                        pointLocationVO.setPointList(pointVOList);
                                    }
                                } catch (Exception ex) {
                                    System.out.println("Exception in pointList - " + ex.toString());
                                }
                            }
                            pointLocationVOList.add(pointLocationVO);
                        }
                        assetVO.setPointLocationList(pointLocationVOList);
                    }
                } catch (Exception ex) {
                    System.out.println("Exception in pointList - " + ex.toString());
                }
            }
        }
        return assetVO;
    }

    /**
     * Parse the given json String Object and return a List of AssetVO objects
     *
     * @param content, String Object
     * @return List of AssetVO Objects
     */
    public static List<AssetVO> parserList(String content) {
        List<AssetVO> assetVOList = new ArrayList<>();
        List<PointLocationVO> pointLocationVOList;
        PointLocationVO pointLocationVO;
        AssetVO assetVO;
        JsonElement element;
        JsonObject jsonAsset, jsonPL, jsonPoint, jsonApAlSet;
        JsonArray jsonAssetArray, jsonArray, jsonArray2, jsonArray3;
        List<PointVO> pointVOList;
        PointVO pointVO;
        List<ApAlSetVO> apAlByPointVOList;
        ApAlSetVO apAlSetVO;

        System.out.println("Json - " + content);
        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null) {
            if ((jsonAssetArray = element.getAsJsonArray()) != null && !jsonAssetArray.isEmpty()) {
                for (int i = 0; i < jsonAssetArray.size(); i++) {
                    jsonAsset = jsonAssetArray.get(i).getAsJsonObject();
                    assetVO = new AssetVO();

                    if (jsonAsset.has("assetId") && !jsonAsset.get("assetId").isJsonNull()) {
                        try {
                            assetVO.setAssetId(UUID.fromString(jsonAsset.get("assetId").getAsString().trim()));
                        } catch (Exception ex) {
                            System.out.println("Exception in assetId - " + ex.toString());
                        }
                    }

                    if (jsonAsset.has("customerAccount")) {
                        try {
                            assetVO.setCustomerAccount(jsonAsset.get("customerAccount").getAsString().trim());
                        } catch (Exception ex) {
                            System.out.println("Exception in customerAccount - " + ex.toString());
                        }
                    }

                    if (jsonAsset.has("siteId")) {
                        try {
                            assetVO.setSiteId(UUID.fromString(jsonAsset.get("siteId").getAsString()));
                        } catch (Exception ex) {
                            System.out.println("Exception in siteId - " + ex.toString());
                        }
                    }

                    if (jsonAsset.has("siteName")) {
                        try {
                            assetVO.setSiteName(jsonAsset.get("siteName").getAsString().trim());
                        } catch (Exception ex) {
                            System.out.println("Exception in siteName - " + ex.toString());
                        }
                    }

                    if (jsonAsset.has("areaId")) {
                        try {
                            assetVO.setAreaId(UUID.fromString(jsonAsset.get("areaId").getAsString()));
                        } catch (Exception ex) {
                            System.out.println("Exception in areaId - " + ex.toString());
                        }
                    }

                    if (jsonAsset.has("areaName")) {
                        try {
                            assetVO.setAreaName(jsonAsset.get("areaName").getAsString().trim());
                        } catch (Exception ex) {
                            System.out.println("Exception in areaName - " + ex.toString());
                        }
                    }

                    if (jsonAsset.has("assetName")) {
                        try {
                            assetVO.setAssetName(jsonAsset.get("assetName").getAsString().trim());
                        } catch (Exception ex) {
                            System.out.println("Exception in assetName - " + ex.toString());
                        }
                    }

                    if (jsonAsset.has("assetType")) {
                        try {
                            assetVO.setAssetType(jsonAsset.get("assetType").getAsString().trim());
                        } catch (Exception ex) {
                            System.out.println("Exception in assetType - " + ex.toString());
                        }
                    }

                    if (jsonAsset.has("description")) {
                        try {
                            assetVO.setDescription(jsonAsset.get("description").getAsString().trim());
                        } catch (Exception ex) {
                            System.out.println("Exception in description - " + ex.toString());
                        }
                    }

                    if (jsonAsset.has("sampleInterval")) {
                        try {
                            assetVO.setSampleInterval(jsonAsset.get("sampleInterval").getAsInt());
                        } catch (Exception ex) {
                            System.out.println("Exception in sampleInterval - " + ex.toString());
                        }
                    }

                    if (jsonAsset.has("alarmEnabled")) {
                        try {
                            assetVO.setAlarmEnabled(jsonAsset.get("alarmEnabled").getAsBoolean());
                        } catch (Exception ex) {
                            System.out.println("Exception in alarmEnabled - " + ex.toString());
                        }
                    }

                    if (jsonAsset.has("pointLocationList")) {
                        try {
                            if ((jsonArray = jsonAsset.getAsJsonArray("pointLocationList")) != null && !jsonArray.isEmpty()) {
                                pointLocationVOList = new ArrayList();
                                for (int l = 0; l < jsonArray.size(); l++) {
                                    jsonPL = jsonArray.get(l).getAsJsonObject();
                                    pointLocationVO = new PointLocationVO();
                                    pointLocationVO.setCustomerAccount(assetVO.getCustomerAccount());
                                    pointLocationVO.setSiteId(assetVO.getSiteId());
                                    pointLocationVO.setAreaId(assetVO.getAreaId());
                                    pointLocationVO.setAssetId(assetVO.getAssetId());

                                    if (jsonPL.has("pointLocationId") && !jsonPL.get("pointLocationId").isJsonNull()) {
                                        try {
                                            pointLocationVO.setPointLocationId(UUID.fromString(jsonPL.get("pointLocationId").getAsString().trim()));
                                        } catch (Exception ex) {
                                            System.out.println("Exception in pointLocationId - " + ex.toString());
                                        }
                                    }

                                    if (jsonPL.has("pointLocationName")) {
                                        try {
                                            pointLocationVO.setPointLocationName(jsonPL.get("pointLocationName").getAsString().trim());
                                        } catch (Exception ex) {
                                            System.out.println("Exception in pointLocationName - " + ex.toString());
                                        }
                                    }

                                    if (jsonPL.has("description")) {
                                        try {
                                            pointLocationVO.setDescription(jsonPL.get("description").getAsString().trim());
                                        } catch (Exception ex) {
                                            System.out.println("Exception in description - " + ex.toString());
                                        }
                                    }

                                    if (jsonPL.has("deviceSerialNumber")) {
                                        try {
                                            pointLocationVO.setDeviceSerialNumber(jsonPL.get("deviceSerialNumber").getAsString().trim());
                                        } catch (Exception ex) {
                                            System.out.println("Exception in deviceSerialNumber - " + ex.toString());
                                        }
                                    }

                                    if (jsonPL.has("ffSetIds")) {
                                        try {
                                            pointLocationVO.setFfSetIds(jsonPL.get("ffSetIds").getAsString().trim());
                                        } catch (Exception ex) {
                                            System.out.println("Exception in ffSetIds - " + ex.toString());
                                        }
                                    }

                                    if (jsonPL.has("speedRatio")) {
                                        try {
                                            pointLocationVO.setSpeedRatio(jsonPL.get("speedRatio").getAsFloat());
                                        } catch (Exception ex) {
                                            System.out.println("Exception in speedRatio - " + ex.toString());
                                        }
                                    }

                                    if (jsonPL.has("sampleInterval")) {
                                        try {
                                            pointLocationVO.setSampleInterval(jsonPL.get("sampleInterval").getAsShort());
                                        } catch (Exception ex) {
                                            System.out.println("Exception in sampleInterval - " + ex.toString());
                                        }
                                    }

                                    if (jsonPL.has("basestationPortNum")) {
                                        try {
                                            pointLocationVO.setBasestationPortNum(jsonPL.get("basestationPortNum").getAsShort());
                                        } catch (Exception ex) {
                                            System.out.println("Exception in basestationPortNum - " + ex.toString());
                                        }
                                    }

                                    if (jsonPL.has("deviceSerialNumber")) {
                                        try {
                                            pointLocationVO.setDeviceSerialNumber(jsonPL.get("deviceSerialNumber").getAsString().trim());
                                        } catch (Exception ex) {
                                            System.out.println("Exception in deviceSerialNumber - " + ex.toString());
                                        }
                                    }

                                    if (jsonPL.has("tachId")) {
                                        try {
                                            pointLocationVO.setTachId(UUID.fromString(jsonPL.get("tachId").getAsString().trim()));
                                        } catch (Exception ex) {
                                            System.out.println("Exception in tachId - " + ex.toString());
                                        }
                                    }

                                    if (jsonPL.has("alarmEnabled")) {
                                        try {
                                            pointLocationVO.setAlarmEnabled(jsonPL.get("alarmEnabled").getAsBoolean());
                                        } catch (Exception ex) {
                                            System.out.println("Exception in pointLocationVO alarmEnabled - " + ex.toString());
                                        }
                                    }

                                    if (jsonPL.has("rollDiameter")) {
                                        try {
                                            pointLocationVO.setRollDiameter(jsonPL.get("rollDiameter").getAsFloat());
                                        } catch (Exception ex) {
                                            System.out.println("Exception in pointLocationVO rollDiameter - " + ex.toString());
                                        }
                                    }

                                    if (jsonPL.has("rollDiameterUnits")) {
                                        try {
                                            pointLocationVO.setRollDiameterUnits(jsonPL.get("rollDiameterUnits").getAsString());
                                        } catch (Exception ex) {
                                            System.out.println("Exception in pointLocationVO rollDiameterUnits - " + ex.toString());
                                        }
                                    }

                                    if (jsonPL.has("pointList")) {
                                        try {
                                            if ((jsonArray2 = jsonPL.getAsJsonArray("pointList")) != null && !jsonArray2.isEmpty()) {
                                                pointVOList = new ArrayList();

                                                for (int j = 0; j < jsonArray2.size(); j++) {
                                                    jsonPoint = jsonArray2.get(j).getAsJsonObject();
                                                    pointVO = new PointVO();
                                                    pointVO.setCustomerAccount(pointLocationVO.getCustomerAccount());
                                                    pointVO.setSiteId(pointLocationVO.getSiteId());
                                                    pointVO.setAreaId(pointLocationVO.getAreaId());
                                                    pointVO.setAssetId(pointLocationVO.getAssetId());
                                                    pointVO.setPointLocationId(pointLocationVO.getPointLocationId());

                                                    if (jsonPoint.has("pointId") && !jsonPoint.get("pointId").isJsonNull()) {
                                                        try {
                                                            pointVO.setPointId(UUID.fromString(jsonPoint.get("pointId").getAsString().trim()));
                                                        } catch (Exception ex) {
                                                            System.out.println("Exception in pointId - " + ex.toString());
                                                        }
                                                    }

                                                    if (jsonPoint.has("pointLocationId")) {
                                                        try {
                                                            pointVO.setPointLocationId(UUID.fromString(jsonPoint.get("pointLocationId").getAsString().trim()));
                                                        } catch (Exception ex) {
                                                            System.out.println("Exception in apSetId - " + ex.toString());
                                                        }
                                                    }

                                                    if (jsonPoint.has("apSetId")) {
                                                        try {
                                                            pointVO.setApSetId(UUID.fromString(jsonPoint.get("apSetId").getAsString().trim()));
                                                        } catch (Exception ex) {
                                                            System.out.println("Exception in alSetId - " + ex.toString());
                                                        }
                                                    }

                                                    if (jsonPoint.has("alSetId")) {
                                                        try {
                                                            pointVO.setAlSetId(UUID.fromString(jsonPoint.get("alSetId").getAsString().trim()));
                                                        } catch (Exception ex) {
                                                            System.out.println("Exception in alSetId - " + ex.toString());
                                                        }
                                                    }

                                                    if (jsonPoint.has("pointName")) {
                                                        try {
                                                            pointVO.setPointName(jsonPoint.get("pointName").getAsString().trim());
                                                        } catch (Exception ex) {
                                                            System.out.println("Exception in pointName - " + ex.toString());
                                                        }
                                                    }

                                                    if (jsonPoint.has("autoAcknowledge")) {
                                                        try {
                                                            pointVO.setAutoAcknowledge(jsonPoint.get("autoAcknowledge").getAsBoolean());
                                                        } catch (Exception ex) {
                                                            System.out.println("Exception in autoAcknowledge - " + ex.toString());
                                                        }
                                                    }

                                                    if (jsonPoint.has("disabled")) {
                                                        try {
                                                            pointVO.setDisabled(jsonPoint.get("disabled").getAsBoolean());
                                                        } catch (Exception ex) {
                                                            System.out.println("Exception in isDisabled - " + ex.toString());
                                                        }
                                                    }

                                                    if (jsonPoint.has("pointType")) {
                                                        try {
                                                            pointVO.setPointType(jsonPoint.get("pointType").getAsString().trim());
                                                        } catch (Exception ex) {
                                                            System.out.println("Exception in pointType - " + ex.toString());
                                                        }
                                                    }

                                                    if (jsonPoint.has("sensorChannelNum")) {
                                                        try {
                                                            pointVO.setSensorChannelNum(jsonPoint.get("sensorChannelNum").getAsInt());
                                                        } catch (Exception ex) {
                                                            System.out.println("Exception in sensorChannelNum - " + ex.toString());
                                                        }
                                                    }

                                                    if (jsonPoint.has("sensorOffset")) {
                                                        try {
                                                            pointVO.setSensorOffset(jsonPoint.get("sensorOffset").getAsFloat());
                                                        } catch (Exception ex) {
                                                            System.out.println("Exception in sensorOffset - " + ex.toString());
                                                        }
                                                    }

                                                    if (jsonPoint.has("sensorSensitivity")) {
                                                        try {
                                                            pointVO.setSensorSensitivity(jsonPoint.get("sensorSensitivity").getAsFloat());
                                                        } catch (Exception ex) {
                                                            System.out.println("Exception in sensorSensitivity - " + ex.toString());
                                                        }
                                                    }

                                                    if (jsonPoint.has("sensorType")) {
                                                        try {
                                                            pointVO.setSensorType(jsonPoint.get("sensorType").getAsString().trim());
                                                        } catch (Exception ex) {
                                                            System.out.println("Exception in sensorType - " + ex.toString());
                                                        }
                                                    }

                                                    if (jsonPoint.has("sensorUnits")) {
                                                        try {
                                                            pointVO.setSensorUnits(jsonPoint.get("sensorUnits").getAsString().trim());
                                                        } catch (Exception ex) {
                                                            System.out.println("Exception in sensorUnits - " + ex.toString());
                                                        }
                                                    }

                                                    if (jsonPoint.has("alarmEnabled")) {
                                                        try {
                                                            pointVO.setAlarmEnabled(jsonPoint.get("alarmEnabled").getAsBoolean());
                                                        } catch (Exception ex) {
                                                            System.out.println("Exception in pointVO alarmEnabled - " + ex.toString());
                                                        }
                                                    }
                                                    
                                                    if (jsonPoint.has("sensorSubType")) {
                                                        try {
                                                            pointVO.setSensorSubType(jsonPoint.get("sensorSubType").getAsString().trim());
                                                        } catch (Exception ex) {
                                                            System.out.println("Exception in pointVO sensorSubType - " + ex.toString());
                                                        }
                                                    }

                                                    if (jsonPoint.has("sensorLocalOrientation")) {
                                                        try {
                                                            pointVO.setSensorLocalOrientation(jsonPoint.get("sensorLocalOrientation").getAsString().trim());
                                                        } catch (Exception ex) {
                                                            System.out.println("Exception in pointVO sensorLocalOrientation - " + ex.toString());
                                                        }
                                                    }

                                                    if (jsonPoint.has("apAlSetVOs")) {
                                                        try {
                                                            if ((jsonArray3 = jsonPoint.getAsJsonArray("apAlSetVOs")) != null && !jsonArray3.isEmpty()) {
                                                                apAlByPointVOList = new ArrayList();
                                                                for (int k = 0; k < jsonArray3.size(); k++) {
                                                                    jsonApAlSet = jsonArray3.get(k).getAsJsonObject();
                                                                    apAlSetVO = new ApAlSetVO();

                                                                    if (jsonApAlSet.has("apSetId")) {
                                                                        try {
                                                                            apAlSetVO.setApSetId(UUID.fromString(jsonApAlSet.get("apSetId").getAsString().trim()));
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in apSetId - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("alSetId")) {
                                                                        try {
                                                                            apAlSetVO.setAlSetId(UUID.fromString(jsonApAlSet.get("alSetId").getAsString().trim()));
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in alSetId - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("paramName")) {
                                                                        try {
                                                                            apAlSetVO.setParamName(jsonApAlSet.get("paramName").getAsString().trim());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in paramNamee - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("fmax")) {
                                                                        try {
                                                                            apAlSetVO.setFmax(jsonApAlSet.get("fmax").getAsInt());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in fmax - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("resolution")) {
                                                                        try {
                                                                            apAlSetVO.setResolution(jsonApAlSet.get("resolution").getAsInt());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in resolution - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("dwell")) {
                                                                        try {
                                                                            apAlSetVO.setDwell(jsonApAlSet.get("dwell").getAsInt());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in dwell - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("period")) {
                                                                        try {
                                                                            apAlSetVO.setPeriod(jsonApAlSet.get("period").getAsFloat());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in period - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("sampleRate")) {
                                                                        try {
                                                                            apAlSetVO.setSampleRate(jsonApAlSet.get("sampleRate").getAsFloat());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in sampleRate - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("minFrequency")) {
                                                                        try {
                                                                            apAlSetVO.setMinFrequency(jsonApAlSet.get("minFrequency").getAsFloat());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in minFreq - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("maxFrequency")) {
                                                                        try {
                                                                            apAlSetVO.setMaxFrequency(jsonApAlSet.get("maxFrequency").getAsFloat());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in maxFrequency - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("lowFault")) {
                                                                        try {
                                                                            apAlSetVO.setLowFault(jsonApAlSet.get("lowFault").getAsFloat());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in lowFault - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("lowAlert")) {
                                                                        try {
                                                                            apAlSetVO.setLowAlert(jsonApAlSet.get("lowAlert").getAsFloat());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in lowAlert - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("highAlert")) {
                                                                        try {
                                                                            apAlSetVO.setHighAlert(jsonApAlSet.get("highAlert").getAsFloat());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in highAlert - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("highFault")) {
                                                                        try {
                                                                            apAlSetVO.setHighFault(jsonApAlSet.get("highFault").getAsFloat());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in highFault - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("demodHighPass")) {
                                                                        try {
                                                                            apAlSetVO.setDemodHighPass(jsonApAlSet.get("demodHighPass").getAsFloat());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in demodHighPass - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("demodLowPass")) {
                                                                        try {
                                                                            apAlSetVO.setDemodLowPass(jsonApAlSet.get("demodLowPass").getAsFloat());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in demodLowPass - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("lowFaultActive")) {
                                                                        try {
                                                                            apAlSetVO.setLowFaultActive(jsonApAlSet.get("lowFaultActive").getAsBoolean());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in lowFaultActive - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("lowAlertActive")) {
                                                                        try {
                                                                            apAlSetVO.setLowAlertActive(jsonApAlSet.get("lowAlertActive").getAsBoolean());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in lowAlertActive - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("highAlertActive")) {
                                                                        try {
                                                                            apAlSetVO.setHighAlertActive(jsonApAlSet.get("highAlertActive").getAsBoolean());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in highAlertActive - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("highFaultActive")) {
                                                                        try {
                                                                            apAlSetVO.setHighFaultActive(jsonApAlSet.get("highFaultActive").getAsBoolean());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in highFaultActive - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("demod")) {
                                                                        try {
                                                                            apAlSetVO.setDemod(jsonApAlSet.get("demod").getAsBoolean());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in demod - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("paramType")) {
                                                                        try {
                                                                            apAlSetVO.setParamType(jsonApAlSet.get("paramType").getAsString().trim());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in paramType - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("frequencyUnits")) {
                                                                        try {
                                                                            apAlSetVO.setFrequencyUnits(jsonApAlSet.get("frequencyUnits").getAsString().trim());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in freqUnits - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("paramUnits")) {
                                                                        try {
                                                                            apAlSetVO.setParamUnits(jsonApAlSet.get("paramUnits").getAsString().trim());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in paramUnits - " + ex.toString());
                                                                        }
                                                                    }

                                                                    if (jsonApAlSet.has("paramAmpFactor")) {
                                                                        try {
                                                                            apAlSetVO.setParamAmpFactor(jsonApAlSet.get("paramAmpFactor").getAsString().trim());
                                                                        } catch (Exception ex) {
                                                                            System.out.println("Exception in paramAmpFactor - " + ex.toString());
                                                                        }
                                                                    }

                                                                    apAlSetVO.setSensorType(pointVO.getSensorType());
                                                                    apAlByPointVOList.add(apAlSetVO);
                                                                }
                                                                pointVO.setApAlSetVOs(apAlByPointVOList);
                                                            }
                                                        } catch (Exception ex) {
                                                            System.out.println("Exception in apAlSetVOs - " + ex.toString());
                                                        }
                                                    }
                                                    pointVOList.add(pointVO);
                                                }
                                                pointLocationVO.setPointList(pointVOList);
                                            }
                                        } catch (Exception ex) {
                                            System.out.println("Exception in pointList - " + ex.toString());
                                        }
                                    }
                                    pointLocationVOList.add(pointLocationVO);
                                }
                                assetVO.setPointLocationList(pointLocationVOList);
                            }
                        } catch (Exception ex) {
                            System.out.println("Exception in pointList - " + ex.toString());
                        }
                    }
                    assetVOList.add(assetVO);
                }
            }
        }
        return assetVOList;
    }
    
    /**
     * Parse the given json String Object and return a AreaVO object
     *
     * @param content, String Object
     * @return AreaVO Object
     */
    public static AreaVO areaParser(String content) {
        AreaVO areaVO = null;
        JsonElement element;
        JsonArray jsonArray;
        JsonObject jsonObject;
        AreaConfigScheduleVO areaConfigScheduleVO;
        List<AreaConfigScheduleVO> acsVOList;

        if (content != null && !content.isEmpty()) {
            areaVO = new AreaVO();
            element = JsonParser.parseString(content);
            jsonObject = element.getAsJsonObject();

            if (jsonObject.has("customerAccount")) {
                try {
                    areaVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in customerAccount - " + ex.toString());
                }
            }

            if (jsonObject.has("siteId")) {
                try {
                    areaVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in siteId - " + ex.toString());
                }
            }

            if (jsonObject.has("areaId")) {
                try {
                    areaVO.setAreaId(UUID.fromString(jsonObject.get("areaId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in areaId - " + ex.toString());
                }
            }

            if (jsonObject.has("areaName")) {
                try {
                    areaVO.setAreaName(jsonObject.get("areaName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in areaName - " + ex.toString());
                }
            }

            if (jsonObject.has("description")) {
                try {
                    areaVO.setDescription(jsonObject.get("description").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in description - " + ex.toString());
                }
            }

            if (jsonObject.has("environment")) {
                try {
                    areaVO.setEnvironment(jsonObject.get("environment").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in environment - " + ex.toString());
                }
            }

            if (jsonObject.has("climateControlled")) {
                try {
                    areaVO.setClimateControlled(jsonObject.get("climateControlled").getAsBoolean());
                } catch (Exception ex) {
                    System.out.println("Exception in climateControlled - " + ex.toString());
                }
            }

            if (jsonObject.has("scheduleVOList")) {
                try {
                    acsVOList = new ArrayList();
                    jsonArray = jsonObject.getAsJsonArray("scheduleVOList");

                    for (int i = 0; i < jsonArray.size(); i++) {
                        jsonObject = jsonArray.get(i).getAsJsonObject();
                        areaConfigScheduleVO = new AreaConfigScheduleVO();
                        areaConfigScheduleVO.setCustomerAccount(areaVO.getCustomerAccount());
                        areaConfigScheduleVO.setSiteId(areaVO.getSiteId());
                        areaConfigScheduleVO.setAreaId(areaVO.getAreaId());
                        areaConfigScheduleVO.setAreaName(areaVO.getAreaName());
                        if (jsonObject.has("schDescription")) {
                            try {
                                areaConfigScheduleVO.setSchDescription(jsonObject.get("schDescription").getAsString().trim());
                            } catch (Exception ex) {
                                System.out.println("Exception in schDescription - " + ex.toString());
                            }
                        }

                        if (jsonObject.has("scheduleId")) {
                            try {
                                areaConfigScheduleVO.setScheduleId(UUID.fromString(jsonObject.get("scheduleId").getAsString().trim()));
                            } catch (Exception ex) {
                                System.out.println("Exception in scheduleId - " + ex.toString());
                            }
                        }

                        if (jsonObject.has("scheduleName")) {
                            try {
                                areaConfigScheduleVO.setScheduleName(jsonObject.get("scheduleName").getAsString().trim());
                            } catch (Exception ex) {
                                System.out.println("Exception in scheduleName - " + ex.toString());
                            }
                        }

                        if (jsonObject.has("continuous")) {
                            try {
                                areaConfigScheduleVO.setContinuous(jsonObject.get("continuous").getAsBoolean());
                            } catch (Exception ex) {
                                System.out.println("Exception in continuous - " + ex.toString());
                            }
                        }

                        if (jsonObject.has("dayOfWeek")) {
                            try {
                                areaConfigScheduleVO.setDayOfWeek(jsonObject.get("dayOfWeek").getAsByte());
                            } catch (Exception ex) {
                                System.out.println("Exception in sampleInterval - " + ex.toString());
                            }
                        }

                        if (jsonObject.has("hoursOfDay")) {
                            try {
                                areaConfigScheduleVO.setHoursOfDay(jsonObject.get("hoursOfDay").getAsString().trim());
                            } catch (Exception ex) {
                                System.out.println("Exception in hoursOfDay - " + ex.toString());
                            }
                        }

                        if (jsonObject.has("type")) {
                            try {
                                areaConfigScheduleVO.setType(jsonObject.get("type").getAsString().trim());
                            } catch (Exception ex) {
                                System.out.println("Exception in type - " + ex.toString());
                            }
                        }
                        acsVOList.add(areaConfigScheduleVO);
                    }
                    areaVO.setScheduleVOList(acsVOList);
                } catch (Exception e) {
                    System.out.println("Exception in scheduleVOList - " + e.toString());
                }
            }

        }
        return areaVO;
    }

    /**
     * Parse the given json String Object and return a SiteVO object
     *
     * @param content, String Object
     * @return SiteVO Object
     */
    public static SiteVO siteParser(String content) {
        SiteVO siteVO = null;
        SiteContactVO siteContactVO;
        List<SiteContactVO> siteContactList;
        JsonElement element;
        JsonObject jsonObject;
        JsonArray jsonArray;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            siteVO = new SiteVO();

            if (jsonObject.has("customerAccount")) {
                try {
                    siteVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in customerAccount - " + ex.toString());
                }
            }

            if (jsonObject.has("siteId")) {
                try {
                    siteVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                } catch (Exception ex) {
                    System.out.println("Exception in siteId - " + ex.toString());
                }
            }

            if (jsonObject.has("siteName")) {
                try {
                    siteVO.setSiteName(jsonObject.get("siteName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in siteName - " + ex.toString());
                }
            }

            if (jsonObject.has("region")) {
                try {
                    siteVO.setRegion(jsonObject.get("region").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in region - " + ex.toString());
                }
            }

            if (jsonObject.has("country")) {
                try {
                    siteVO.setCountry(jsonObject.get("country").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in country - " + ex.toString());
                }
            }

            if (jsonObject.has("physicalAddress1")) {
                try {
                    siteVO.setPhysicalAddress1(jsonObject.get("physicalAddress1").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in physicalAddress1 - " + ex.toString());
                }
            }

            if (jsonObject.has("physicalAddress2")) {
                try {
                    siteVO.setPhysicalAddress2(jsonObject.get("physicalAddress2").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in physicalAddress2 - " + ex.toString());
                }
            }

            if (jsonObject.has("physicalCity")) {
                try {
                    siteVO.setPhysicalCity(jsonObject.get("physicalCity").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in physicalCity - " + ex.toString());
                }
            }

            if (jsonObject.has("physicalStateProvince")) {
                try {
                    siteVO.setPhysicalStateProvince(jsonObject.get("physicalStateProvince").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in physicalStateProvince - " + ex.toString());
                }
            }

            if (jsonObject.has("physicalPostalCode")) {
                try {
                    siteVO.setPhysicalPostalCode(jsonObject.get("physicalPostalCode").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in physicalPostalCode - " + ex.toString());
                }
            }

            if (jsonObject.has("latitude")) {
                try {
                    siteVO.setLatitude(jsonObject.get("latitude").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in latitude - " + ex.toString());
                }
            }

            if (jsonObject.has("longitude")) {
                try {
                    siteVO.setLongitude(jsonObject.get("longitude").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in longitude - " + ex.toString());
                }
            }

            if (jsonObject.has("timezone")) {
                try {
                    siteVO.setTimezone(jsonObject.get("timezone").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in timezone - " + ex.toString());
                }
            }

            if (jsonObject.has("industry")) {
                try {
                    siteVO.setIndustry(jsonObject.get("industry").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in industry - " + ex.toString());
                }
            }

            if (jsonObject.has("shipAddress1")) {
                try {
                    siteVO.setShipAddress1(jsonObject.get("shipAddress1").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in shipAddress1 - " + ex.toString());
                }
            }

            if (jsonObject.has("shipAddress2")) {
                try {
                    siteVO.setShipAddress2(jsonObject.get("shipAddress2").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in shipAddress2 - " + ex.toString());
                }
            }

            if (jsonObject.has("shipCity")) {
                try {
                    siteVO.setShipCity(jsonObject.get("shipCity").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in shipCity - " + ex.toString());
                }
            }

            if (jsonObject.has("shipStateProvince")) {
                try {
                    siteVO.setShipStateProvince(jsonObject.get("shipStateProvince").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in shipStateProvince - " + ex.toString());
                }
            }

            if (jsonObject.has("shipPostalCode")) {
                try {
                    siteVO.setShipPostalCode(jsonObject.get("shipPostalCode").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in shipPostalCode - " + ex.toString());
                }
            }

            if (jsonObject.has("billingAddress1")) {
                try {
                    siteVO.setBillingAddress1(jsonObject.get("billingAddress1").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in billingAddress1 - " + ex.toString());
                }
            }

            if (jsonObject.has("billingAddress2")) {
                try {
                    siteVO.setBillingAddress2(jsonObject.get("billingAddress2").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in billingAddress2 - " + ex.toString());
                }
            }

            if (jsonObject.has("billingCity")) {
                try {
                    siteVO.setBillingCity(jsonObject.get("billingCity").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in billingCity - " + ex.toString());
                }
            }

            if (jsonObject.has("billingStateProvince")) {
                try {
                    siteVO.setBillingStateProvince(jsonObject.get("billingStateProvince").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in billingStateProvince - " + ex.toString());
                }
            }

            if (jsonObject.has("billingPostalCode")) {
                try {
                    siteVO.setBillingPostalCode(jsonObject.get("billingPostalCode").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in billingPostalCode - " + ex.toString());
                }
            }

            if (jsonObject.has("siteContactList")) {
                try {
                    jsonArray = jsonObject.getAsJsonArray("siteContactList");
                    siteContactList = new ArrayList();

                    for (int i = 0; i < jsonArray.size(); i++) {
                        jsonObject = jsonArray.get(i).getAsJsonObject();
                        siteContactVO = new SiteContactVO();
                        siteContactVO.setCustomerAccount(siteVO.getCustomerAccount());
                        siteContactVO.setSiteName(siteVO.getSiteName());
                        siteContactVO.setSiteId(siteVO.getSiteId());

                        if (jsonObject.has("role")) {
                            try {
                                siteContactVO.setRole(jsonObject.get("role").getAsString().trim());
                            } catch (Exception ex) {
                                System.out.println("Exception in role - " + ex.toString());
                            }
                        }

                        if (jsonObject.has("contactName")) {
                            try {
                                siteContactVO.setContactName(jsonObject.get("contactName").getAsString().trim());
                            } catch (Exception ex) {
                                System.out.println("Exception in contactName - " + ex.toString());
                            }
                        }

                        if (jsonObject.has("contactTitle")) {
                            try {
                                siteContactVO.setContactTitle(jsonObject.get("contactTitle").getAsString().trim());
                            } catch (Exception ex) {
                                System.out.println("Exception in contactTitle - " + ex.toString());
                            }
                        }

                        if (jsonObject.has("contactPhone")) {
                            try {
                                siteContactVO.setContactPhone(jsonObject.get("contactPhone").getAsString().trim());
                            } catch (Exception ex) {
                                System.out.println("Exception in contactPhone - " + ex.toString());
                            }
                        }

                        if (jsonObject.has("contactEmail")) {
                            try {
                                siteContactVO.setContactEmail(jsonObject.get("contactEmail").getAsString().trim());
                            } catch (Exception ex) {
                                System.out.println("Exception in contactEmail - " + ex.toString());
                            }
                        }
                        siteContactList.add(siteContactVO);
                    }
                    siteVO.setSiteContactList(siteContactList);
                } catch (Exception e) {
                    System.out.println("Exception in siteContactList - " + e.toString());
                }
            }

        }
        return siteVO;
    }

    /**
     * Parse the given json String Object and return a PointLocationVO object
     *
     * @param content, String Object
     * @return PointLocationVO Object
     */
    public static PointLocationVO pointLocationParser(String content) {
        PointLocationVO pointLocationVO = null;
        List<PointVO> pointVOList;
        PointVO pointVO;
        List<ApAlSetVO> apAlByPointVOList;
        ApAlSetVO apAlSetVO;
        JsonElement element;
        JsonObject jsonObject;
        JsonArray jsonArray, jsonArray1;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            pointLocationVO = new PointLocationVO();
            if (jsonObject.has("request") && jsonObject.has("attributes")) {
                pointLocationVO.setRequest(jsonObject.get("request").getAsString());
                //Since jsonObject has elemints of 'request' and 'attributes' need to get the  jsonObject of 'attributes' first.
                jsonObject = jsonObject.getAsJsonObject("attributes");
            }
            if (jsonObject.has("customerAccount")) {
                try {
                    pointLocationVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in customerAccount - " + ex.toString());
                }
            }

            if (jsonObject.has("siteId")) {
                try {
                    pointLocationVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in siteId - " + ex.toString());
                }
            }

            if (jsonObject.has("siteName")) {
                try {
                    pointLocationVO.setSiteName(jsonObject.get("siteName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in siteName - " + ex.toString());
                }
            }

            if (jsonObject.has("areaId")) {
                try {
                    pointLocationVO.setAreaId(UUID.fromString(jsonObject.get("areaId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in areaId - " + ex.toString());
                }
            }

            if (jsonObject.has("areaName")) {
                try {
                    pointLocationVO.setAreaName(jsonObject.get("areaName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in areaName - " + ex.toString());
                }
            }

            if (jsonObject.has("assetId")) {
                try {
                    pointLocationVO.setAssetId(UUID.fromString(jsonObject.get("assetId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in assetId - " + ex.toString());
                }
            }

            if (jsonObject.has("assetName")) {
                try {
                    pointLocationVO.setAssetName(jsonObject.get("assetName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in assetName - " + ex.toString());
                }
            }

            if (jsonObject.has("pointLocationName")) {
                try {
                    pointLocationVO.setPointLocationName(jsonObject.get("pointLocationName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in pointLocationName - " + ex.toString());
                }
            }

            if (jsonObject.has("pointLocationId")) {
                try {
                    pointLocationVO.setPointLocationId(UUID.fromString(jsonObject.get("pointLocationId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in pointLocationId - " + ex.toString());
                }
            }

            if (jsonObject.has("description")) {
                try {
                    pointLocationVO.setDescription(jsonObject.get("description").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in description - " + ex.toString());
                }
            }

            if (jsonObject.has("deviceSerialNumber")) {
                try {
                    pointLocationVO.setDeviceSerialNumber(jsonObject.get("deviceSerialNumber").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in deviceSerialNumber - " + ex.toString());
                }
            }

            if (jsonObject.has("ffSetIds")) {
                try {
                    pointLocationVO.setFfSetIds(jsonObject.get("ffSetIds").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in ffSetIds - " + ex.toString());
                }
            }

            if (jsonObject.has("speedRatio")) {
                try {
                    pointLocationVO.setSpeedRatio(jsonObject.get("speedRatio").getAsFloat());
                } catch (Exception ex) {
                    System.out.println("Exception in speedRatio - " + ex.toString());
                }
            }

            if (jsonObject.has("sampleInterval")) {
                try {
                    pointLocationVO.setSampleInterval(jsonObject.get("sampleInterval").getAsShort());
                } catch (Exception ex) {
                    System.out.println("Exception in sampleInterval - " + ex.toString());
                }
            }
            if (jsonObject.has("basestationPortNum")) {
                try {
                    pointLocationVO.setBasestationPortNum(jsonObject.get("basestationPortNum").getAsShort());
                } catch (Exception ex) {
                    System.out.println("Exception in basestationPortNum - " + ex.toString());
                }
            }

            if (jsonObject.has("tachId")) {
                try {
                    pointLocationVO.setTachId(UUID.fromString(jsonObject.get("tachId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in tachId - " + ex.toString());
                }
            }

            if (jsonObject.has("alarmEnabled")) {
                try {
                    pointLocationVO.setAlarmEnabled(jsonObject.get("alarmEnabled").getAsBoolean());
                } catch (Exception ex) {
                    System.out.println("Exception in pointLocationVO alarmEnabled - " + ex.toString());
                }
            }

            if (jsonObject.has("rollDiameter")) {
                try {
                    pointLocationVO.setRollDiameter(jsonObject.get("rollDiameter").getAsFloat());
                } catch (Exception ex) {
                    System.out.println("Exception in pointLocationVO rollDiameter - " + ex.toString());
                }
            }

            if (jsonObject.has("rollDiameterUnits")) {
                try {
                    pointLocationVO.setRollDiameterUnits(jsonObject.get("rollDiameterUnits").getAsString());
                } catch (Exception ex) {
                    System.out.println("Exception in pointLocationVO rollDiameterUnits - " + ex.toString());
                }
            }

            if (jsonObject.has("sensorTypes")) {
                try {
                    List<String> sensorTypeList = Arrays.asList(jsonObject.get("sensorTypes").getAsString().split(","));
                    pointLocationVO.setSensorList(sensorTypeList);
                } catch (Exception ex) {
                    System.out.println("Exception in pointLocationVO sensorTypeList - " + ex.toString());
                }
            }

            if (jsonObject.has("pointList")) {
                try {
                    if ((jsonArray = jsonObject.getAsJsonArray("pointList")) != null && !jsonArray.isEmpty()) {
                        pointVOList = new ArrayList();
                        for (int j = 0; j < jsonArray.size(); j++) {
                            jsonObject = jsonArray.get(j).getAsJsonObject();
                            pointVO = new PointVO();
                            pointVO.setCustomerAccount(pointLocationVO.getCustomerAccount());
                            pointVO.setSiteId(pointLocationVO.getSiteId());
                            pointVO.setAreaId(pointLocationVO.getAreaId());
                            pointVO.setAssetId(pointLocationVO.getAssetId());
                            pointVO.setPointLocationId(pointLocationVO.getPointLocationId());

                            if (jsonObject.has("pointId")) {
                                try {
                                    pointVO.setPointId(UUID.fromString(jsonObject.get("pointId").getAsString().trim()));
                                } catch (Exception ex) {
                                    System.out.println("Exception in pointId - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("apSetId")) {
                                try {
                                    pointVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString().trim()));
                                } catch (Exception ex) {
                                    System.out.println("Exception in apSetId - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("alSetId")) {
                                try {
                                    pointVO.setAlSetId(UUID.fromString(jsonObject.get("alSetId").getAsString().trim()));
                                } catch (Exception ex) {
                                    System.out.println("Exception in alSetId - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("pointName")) {
                                try {
                                    pointVO.setPointName(jsonObject.get("pointName").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in pointName - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("autoAcknowledge")) {
                                try {
                                    pointVO.setAutoAcknowledge(jsonObject.get("autoAcknowledge").getAsBoolean());
                                } catch (Exception ex) {
                                    System.out.println("Exception in autoAcknowledge - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("disabled")) {
                                try {
                                    pointVO.setDisabled(jsonObject.get("disabled").getAsBoolean());
                                } catch (Exception ex) {
                                    System.out.println("Exception in isDisabled - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("pointType")) {
                                try {
                                    pointVO.setPointType(jsonObject.get("pointType").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in pointType - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("sensorChannelNum")) {
                                try {
                                    pointVO.setSensorChannelNum(jsonObject.get("sensorChannelNum").getAsInt());
                                } catch (Exception ex) {
                                    System.out.println("Exception in sensorChannelNum - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("sensorOffset")) {
                                try {
                                    pointVO.setSensorOffset(jsonObject.get("sensorOffset").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in sensorOffset - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("sensorSensitivity")) {
                                try {
                                    pointVO.setSensorSensitivity(jsonObject.get("sensorSensitivity").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in sensorSensitivity - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("sensorType")) {
                                try {
                                    pointVO.setSensorType(jsonObject.get("sensorType").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in sensorType - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("sensorUnits")) {
                                try {
                                    pointVO.setSensorUnits(jsonObject.get("sensorUnits").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in sensorUnits - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("alarmEnabled")) {
                                try {
                                    pointVO.setAlarmEnabled(jsonObject.get("alarmEnabled").getAsBoolean());
                                } catch (Exception ex) {
                                    System.out.println("Exception in pointVO alarmEnabled - " + ex.toString());
                                }
                            }
                            
                            if (jsonObject.has("sensorSubType")) {
                                try {
                                    pointVO.setSensorSubType(jsonObject.get("sensorSubType").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in pointVO sensorSubType - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("sensorLocalOrientation")) {
                                try {
                                    pointVO.setSensorLocalOrientation(jsonObject.get("sensorLocalOrientation").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in pointVO sensorLocalOrientation - " + ex.toString());
                                }
                            }

                                                    
                            if (jsonObject.has("apAlSetVOs")) {
                                try {
                                    if ((jsonArray1 = jsonObject.getAsJsonArray("apAlSetVOs")) != null && !jsonArray1.isEmpty()) {
                                        apAlByPointVOList = new ArrayList();
                                        for (int k = 0; k < jsonArray1.size(); k++) {
                                            jsonObject = jsonArray1.get(k).getAsJsonObject();
                                            apAlSetVO = new ApAlSetVO();

                                            if (jsonObject.has("apSetId")) {
                                                try {
                                                    apAlSetVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString().trim()));
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in apSetId - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("alSetId")) {
                                                try {
                                                    apAlSetVO.setAlSetId(UUID.fromString(jsonObject.get("alSetId").getAsString().trim()));
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in alSetId - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("paramName")) {
                                                try {
                                                    apAlSetVO.setParamName(jsonObject.get("paramName").getAsString().trim());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in paramNamee - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("fmax")) {
                                                try {
                                                    apAlSetVO.setFmax(jsonObject.get("fmax").getAsInt());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in fmax - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("resolution")) {
                                                try {
                                                    apAlSetVO.setResolution(jsonObject.get("resolution").getAsInt());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in resolution - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("dwell")) {
                                                try {
                                                    apAlSetVO.setDwell(jsonObject.get("dwell").getAsInt());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in dwell - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("period")) {
                                                try {
                                                    apAlSetVO.setPeriod(jsonObject.get("period").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in period - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("sampleRate")) {
                                                try {
                                                    apAlSetVO.setSampleRate(jsonObject.get("sampleRate").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in sampleRate - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("minFrequency")) {
                                                try {
                                                    apAlSetVO.setMinFrequency(jsonObject.get("minFrequency").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in minFreq - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("maxFrequency")) {
                                                try {
                                                    apAlSetVO.setMaxFrequency(jsonObject.get("maxFrequency").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in maxFrequency - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("lowFault")) {
                                                try {
                                                    apAlSetVO.setLowFault(jsonObject.get("lowFault").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in lowFault - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("lowAlert")) {
                                                try {
                                                    apAlSetVO.setLowAlert(jsonObject.get("lowAlert").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in lowAlert - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("highAlert")) {
                                                try {
                                                    apAlSetVO.setHighAlert(jsonObject.get("highAlert").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in highAlert - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("highFault")) {
                                                try {
                                                    apAlSetVO.setHighFault(jsonObject.get("highFault").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in highFault - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("demodHighPass")) {
                                                try {
                                                    apAlSetVO.setDemodHighPass(jsonObject.get("demodHighPass").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in demodHighPass - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("demodLowPass")) {
                                                try {
                                                    apAlSetVO.setDemodLowPass(jsonObject.get("demodLowPass").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in demodLowPass - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("lowFaultActive")) {
                                                try {
                                                    apAlSetVO.setLowFaultActive(jsonObject.get("lowFaultActive").getAsBoolean());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in lowFaultActive - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("lowAlertActive")) {
                                                try {
                                                    apAlSetVO.setLowAlertActive(jsonObject.get("lowAlertActive").getAsBoolean());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in lowAlertActive - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("highAlertActive")) {
                                                try {
                                                    apAlSetVO.setHighAlertActive(jsonObject.get("highAlertActive").getAsBoolean());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in highAlertActive - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("highFaultActive")) {
                                                try {
                                                    apAlSetVO.setHighFaultActive(jsonObject.get("highFaultActive").getAsBoolean());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in highFaultActive - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("demod")) {
                                                try {
                                                    apAlSetVO.setDemod(jsonObject.get("demod").getAsBoolean());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in demod - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("paramType")) {
                                                try {
                                                    apAlSetVO.setParamType(jsonObject.get("paramType").getAsString().trim());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in paramType - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("frequencyUnits")) {
                                                try {
                                                    apAlSetVO.setFrequencyUnits(jsonObject.get("frequencyUnits").getAsString().trim());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in freqUnits - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("paramUnits")) {
                                                try {
                                                    apAlSetVO.setParamUnits(jsonObject.get("paramUnits").getAsString().trim());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in paramUnits - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("paramAmpFactor")) {
                                                try {
                                                    apAlSetVO.setParamAmpFactor(jsonObject.get("paramAmpFactor").getAsString().trim());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in paramAmpFactor - " + ex.toString());
                                                }
                                            }

                                            apAlSetVO.setSensorType(pointVO.getSensorType());
                                            apAlByPointVOList.add(apAlSetVO);
                                        }
                                        pointVO.setApAlSetVOs(apAlByPointVOList);
                                    }
                                } catch (Exception ex) {
                                    System.out.println("Exception in apAlSetVOs - " + ex.toString());
                                }
                            }
                            pointVOList.add(pointVO);
                        }
                        pointLocationVO.setPointList(pointVOList);
                    }
                } catch (Exception ex) {
                    System.out.println("Exception in pointList - " + ex.toString());
                }
            }
        }

        return pointLocationVO;
    }

    /**
     * Parse the given json String Object and return a PointVO object
     *
     * @param content, String Object
     * @return List of PointVO Objects
     */
    public static List<PointVO> pointsParser(String content) {
        List<PointVO> pointVOList = new ArrayList();;
        JsonElement element;
        JsonObject jsonObject;
        JsonArray pointJsonArray, jsonArray;
        List<ApAlSetVO> apAlByPointVOList;
        ApAlSetVO apAlSetVO;
        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && element.isJsonArray() && (pointJsonArray = element.getAsJsonArray()) != null) {
            for (int i = 0; i < pointJsonArray.size(); i++) {
                jsonObject = pointJsonArray.get(i).getAsJsonObject();
                PointVO pointVO = new PointVO();

                if (jsonObject.has("customerAccount")) {
                    try {
                        pointVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString().trim());
                    } catch (Exception ex) {
                        System.out.println("Exception in customerAccount - " + ex.toString());
                    }
                }

                if (jsonObject.has("siteId")) {
                    try {
                        pointVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString().trim()));
                    } catch (Exception ex) {
                        System.out.println("Exception in siteId - " + ex.toString());
                    }
                }

                if (jsonObject.has("siteName")) {
                    try {
                        pointVO.setSiteName(jsonObject.get("siteName").getAsString().trim());
                    } catch (Exception ex) {
                        System.out.println("Exception in siteName - " + ex.toString());
                    }
                }

                if (jsonObject.has("areaId")) {
                    try {
                        pointVO.setAreaId(UUID.fromString(jsonObject.get("areaId").getAsString().trim()));
                    } catch (Exception ex) {
                        System.out.println("Exception in areaId - " + ex.toString());
                    }
                }

                if (jsonObject.has("areaName")) {
                    try {
                        pointVO.setAreaName(jsonObject.get("areaName").getAsString().trim());
                    } catch (Exception ex) {
                        System.out.println("Exception in areaName - " + ex.toString());
                    }
                }

                if (jsonObject.has("assetId")) {
                    try {
                        pointVO.setAssetId(UUID.fromString(jsonObject.get("assetId").getAsString().trim()));
                    } catch (Exception ex) {
                        System.out.println("Exception in assetId - " + ex.toString());
                    }
                }

                if (jsonObject.has("assetName")) {
                    try {
                        pointVO.setAssetName(jsonObject.get("assetName").getAsString().trim());
                    } catch (Exception ex) {
                        System.out.println("Exception in assetName - " + ex.toString());
                    }
                }

                if (jsonObject.has("pointLocationId")) {
                    try {
                        pointVO.setPointLocationId(UUID.fromString(jsonObject.get("pointLocationId").getAsString().trim()));
                    } catch (Exception ex) {
                        System.out.println("Exception in pointLocationName - " + ex.toString());
                    }
                }

                if (jsonObject.has("pointLocationName")) {
                    try {
                        pointVO.setPointLocationName(jsonObject.get("pointLocationName").getAsString().trim());
                    } catch (Exception ex) {
                        System.out.println("Exception in pointLocationName - " + ex.toString());
                    }
                }

                if (jsonObject.has("pointId")) {
                    try {
                        pointVO.setPointId(UUID.fromString(jsonObject.get("pointId").getAsString().trim()));
                    } catch (Exception ex) {
                        System.out.println("Exception in pointId - " + ex.toString());
                    }
                }

                if (jsonObject.has("apSetId")) {
                    try {
                        pointVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString().trim()));
                    } catch (Exception ex) {
                        System.out.println("Exception in apSetId - " + ex.toString());
                    }
                }

                if (jsonObject.has("alSetId")) {
                    try {
                        pointVO.setAlSetId(UUID.fromString(jsonObject.get("alSetId").getAsString().trim()));
                    } catch (Exception ex) {
                        System.out.println("Exception in alSetId - " + ex.toString());
                    }
                }

                if (jsonObject.has("pointName")) {
                    try {
                        pointVO.setPointName(jsonObject.get("pointName").getAsString().trim());
                    } catch (Exception ex) {
                        System.out.println("Exception in pointName - " + ex.toString());
                    }
                }

                if (jsonObject.has("autoAcknowledge")) {
                    try {
                        pointVO.setAutoAcknowledge(jsonObject.get("autoAcknowledge").getAsBoolean());
                    } catch (Exception ex) {
                        System.out.println("Exception in autoAcknowledge - " + ex.toString());
                    }
                }

                if (jsonObject.has("disabled")) {
                    try {
                        pointVO.setDisabled(jsonObject.get("disabled").getAsBoolean());
                    } catch (Exception ex) {
                        System.out.println("Exception in isDisabled - " + ex.toString());
                    }
                }

                if (jsonObject.has("pointType")) {
                    try {
                        pointVO.setPointType(jsonObject.get("pointType").getAsString().trim());
                    } catch (Exception ex) {
                        System.out.println("Exception in pointType - " + ex.toString());
                    }
                }

                if (jsonObject.has("sensorChannelNum")) {
                    try {
                        pointVO.setSensorChannelNum(jsonObject.get("sensorChannelNum").getAsInt());
                    } catch (Exception ex) {
                        System.out.println("Exception in sensorChannelNum - " + ex.toString());
                    }
                }

                if (jsonObject.has("sensorOffset")) {
                    try {
                        pointVO.setSensorOffset(jsonObject.get("sensorOffset").getAsFloat());
                    } catch (Exception ex) {
                        System.out.println("Exception in sensorOffset - " + ex.toString());
                    }
                }

                if (jsonObject.has("sensorSensitivity")) {
                    try {
                        pointVO.setSensorSensitivity(jsonObject.get("sensorSensitivity").getAsFloat());
                    } catch (Exception ex) {
                        System.out.println("Exception in sensorSensitivity - " + ex.toString());
                    }
                }

                if (jsonObject.has("sensorType")) {
                    try {
                        pointVO.setSensorType(jsonObject.get("sensorType").getAsString().trim());
                    } catch (Exception ex) {
                        System.out.println("Exception in sensorType - " + ex.toString());
                    }
                }

                if (jsonObject.has("sensorUnits")) {
                    try {
                        pointVO.setSensorUnits(jsonObject.get("sensorUnits").getAsString().trim());
                    } catch (Exception ex) {
                        System.out.println("Exception in sensorUnits - " + ex.toString());
                    }
                }

                if (jsonObject.has("alarmEnabled")) {
                    try {
                        pointVO.setAlarmEnabled(jsonObject.get("alarmEnabled").getAsBoolean());
                    } catch (Exception ex) {
                        System.out.println("Exception in pointVO alarmEnabled - " + ex.toString());
                    }
                }
                
                if (jsonObject.has("sensorSubType")) {
                    try {
                        pointVO.setSensorSubType(jsonObject.get("sensorSubType").getAsString().trim());
                    } catch (Exception ex) {
                        System.out.println("Exception in pointVO sensorSubType - " + ex.toString());
                    }
                }

                if (jsonObject.has("sensorLocalOrientation")) {
                    try {
                        pointVO.setSensorLocalOrientation(jsonObject.get("sensorLocalOrientation").getAsString().trim());
                    } catch (Exception ex) {
                        System.out.println("Exception in pointVO sensorLocalOrientation - " + ex.toString());
                    }
                }
                
                if (jsonObject.has("customized")) {
                    try {
                        pointVO.setCustomized(jsonObject.get("customized").getAsBoolean());
                    } catch (Exception ex) {
                        System.out.println("Exception in pointVO alarmEnabled - " + ex.toString());
                    }
                }

                if (jsonObject.has("apAlSetVOs")) {
                    try {
                        if ((jsonArray = jsonObject.getAsJsonArray("apAlSetVOs")) != null && !jsonArray.isEmpty()) {
                            apAlByPointVOList = new ArrayList();
                            for (int j = 0; j < jsonArray.size(); j++) {
                                jsonObject = jsonArray.get(j).getAsJsonObject();
                                apAlSetVO = new ApAlSetVO();

                                if (jsonObject.has("apSetId")) {
                                    try {
                                        apAlSetVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString().trim()));
                                    } catch (Exception ex) {
                                        System.out.println("Exception in apSetId - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("alSetId")) {
                                    try {
                                        apAlSetVO.setAlSetId(UUID.fromString(jsonObject.get("alSetId").getAsString().trim()));
                                    } catch (Exception ex) {
                                        System.out.println("Exception in alSetId - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("paramName")) {
                                    try {
                                        apAlSetVO.setParamName(jsonObject.get("paramName").getAsString().trim());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in paramNamee - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("fmax")) {
                                    try {
                                        apAlSetVO.setFmax(jsonObject.get("fmax").getAsInt());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in fmax - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("resolution")) {
                                    try {
                                        apAlSetVO.setResolution(jsonObject.get("resolution").getAsInt());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in resolution - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("dwell")) {
                                    try {
                                        apAlSetVO.setDwell(jsonObject.get("dwell").getAsInt());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in dwell - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("period")) {
                                    try {
                                        apAlSetVO.setPeriod(jsonObject.get("period").getAsFloat());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in period - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("sampleRate")) {
                                    try {
                                        apAlSetVO.setSampleRate(jsonObject.get("sampleRate").getAsFloat());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in sampleRate - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("minFrequency")) {
                                    try {
                                        apAlSetVO.setMinFrequency(jsonObject.get("minFrequency").getAsFloat());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in minFreq - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("maxFrequency")) {
                                    try {
                                        apAlSetVO.setMaxFrequency(jsonObject.get("maxFrequency").getAsFloat());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in maxFrequency - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("lowFault")) {
                                    try {
                                        apAlSetVO.setLowFault(jsonObject.get("lowFault").getAsFloat());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in lowFault - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("lowAlert")) {
                                    try {
                                        apAlSetVO.setLowAlert(jsonObject.get("lowAlert").getAsFloat());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in lowAlert - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("highAlert")) {
                                    try {
                                        apAlSetVO.setHighAlert(jsonObject.get("highAlert").getAsFloat());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in highAlert - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("highFault")) {
                                    try {
                                        apAlSetVO.setHighFault(jsonObject.get("highFault").getAsFloat());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in highFault - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("demodHighPass")) {
                                    try {
                                        apAlSetVO.setDemodHighPass(jsonObject.get("demodHighPass").getAsFloat());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in demodHighPass - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("demodLowPass")) {
                                    try {
                                        apAlSetVO.setDemodLowPass(jsonObject.get("demodLowPass").getAsFloat());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in demodLowPass - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("lowFaultActive")) {
                                    try {
                                        apAlSetVO.setLowFaultActive(jsonObject.get("lowFaultActive").getAsBoolean());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in lowFaultActive - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("lowAlertActive")) {
                                    try {
                                        apAlSetVO.setLowAlertActive(jsonObject.get("lowAlertActive").getAsBoolean());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in lowAlertActive - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("highAlertActive")) {
                                    try {
                                        apAlSetVO.setHighAlertActive(jsonObject.get("highAlertActive").getAsBoolean());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in highAlertActive - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("highFaultActive")) {
                                    try {
                                        apAlSetVO.setHighFaultActive(jsonObject.get("highFaultActive").getAsBoolean());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in highFaultActive - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("demod")) {
                                    try {
                                        apAlSetVO.setDemod(jsonObject.get("demod").getAsBoolean());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in demod - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("paramType")) {
                                    try {
                                        apAlSetVO.setParamType(jsonObject.get("paramType").getAsString().trim());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in paramType - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("frequencyUnits")) {
                                    try {
                                        apAlSetVO.setFrequencyUnits(jsonObject.get("frequencyUnits").getAsString().trim());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in freqUnits - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("paramUnits")) {
                                    try {
                                        apAlSetVO.setParamUnits(jsonObject.get("paramUnits").getAsString().trim());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in paramUnits - " + ex.toString());
                                    }
                                }

                                if (jsonObject.has("paramAmpFactor")) {
                                    try {
                                        apAlSetVO.setParamAmpFactor(jsonObject.get("paramAmpFactor").getAsString().trim());
                                    } catch (Exception ex) {
                                        System.out.println("Exception in paramAmpFactor - " + ex.toString());
                                    }
                                }

                                apAlSetVO.setSensorType(pointVO.getSensorType());
                                apAlByPointVOList.add(apAlSetVO);
                            }
                            pointVO.setApAlSetVOs(apAlByPointVOList);
                        }
                    } catch (Exception ex) {
                        System.out.println("Exception in apAlSetVOs - " + ex.toString());
                    }
                }
                pointVOList.add(pointVO);
            }
        }
        
        else if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && element.isJsonObject() && (jsonObject = element.getAsJsonObject()) != null) {
            PointVO pointVO = new PointVO();

            if (jsonObject.has("customerAccount")) {
                try {
                    pointVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in customerAccount - " + ex.toString());
                }
            }

            if (jsonObject.has("siteId")) {
                try {
                    pointVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in siteId - " + ex.toString());
                }
            }

            if (jsonObject.has("siteName")) {
                try {
                    pointVO.setSiteName(jsonObject.get("siteName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in siteName - " + ex.toString());
                }
            }

            if (jsonObject.has("areaId")) {
                try {
                    pointVO.setAreaId(UUID.fromString(jsonObject.get("areaId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in areaId - " + ex.toString());
                }
            }

            if (jsonObject.has("areaName")) {
                try {
                    pointVO.setAreaName(jsonObject.get("areaName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in areaName - " + ex.toString());
                }
            }

            if (jsonObject.has("assetId")) {
                try {
                    pointVO.setAssetId(UUID.fromString(jsonObject.get("assetId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in assetId - " + ex.toString());
                }
            }

            if (jsonObject.has("assetName")) {
                try {
                    pointVO.setAssetName(jsonObject.get("assetName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in assetName - " + ex.toString());
                }
            }

            if (jsonObject.has("pointLocationId")) {
                try {
                    pointVO.setPointLocationId(UUID.fromString(jsonObject.get("pointLocationId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in pointLocationName - " + ex.toString());
                }
            }

            if (jsonObject.has("pointLocationName")) {
                try {
                    pointVO.setPointLocationName(jsonObject.get("pointLocationName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in pointLocationName - " + ex.toString());
                }
            }

            if (jsonObject.has("pointId")) {
                try {
                    pointVO.setPointId(UUID.fromString(jsonObject.get("pointId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in pointId - " + ex.toString());
                }
            }

            if (jsonObject.has("apSetId")) {
                try {
                    pointVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in apSetId - " + ex.toString());
                }
            }

            if (jsonObject.has("alSetId")) {
                try {
                    pointVO.setAlSetId(UUID.fromString(jsonObject.get("alSetId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in alSetId - " + ex.toString());
                }
            }

            if (jsonObject.has("pointName")) {
                try {
                    pointVO.setPointName(jsonObject.get("pointName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in pointName - " + ex.toString());
                }
            }

            if (jsonObject.has("autoAcknowledge")) {
                try {
                    pointVO.setAutoAcknowledge(jsonObject.get("autoAcknowledge").getAsBoolean());
                } catch (Exception ex) {
                    System.out.println("Exception in autoAcknowledge - " + ex.toString());
                }
            }

            if (jsonObject.has("disabled")) {
                try {
                    pointVO.setDisabled(jsonObject.get("disabled").getAsBoolean());
                } catch (Exception ex) {
                    System.out.println("Exception in isDisabled - " + ex.toString());
                }
            }

            if (jsonObject.has("pointType")) {
                try {
                    pointVO.setPointType(jsonObject.get("pointType").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in pointType - " + ex.toString());
                }
            }

            if (jsonObject.has("sensorChannelNum")) {
                try {
                    pointVO.setSensorChannelNum(jsonObject.get("sensorChannelNum").getAsInt());
                } catch (Exception ex) {
                    System.out.println("Exception in sensorChannelNum - " + ex.toString());
                }
            }

            if (jsonObject.has("sensorOffset")) {
                try {
                    pointVO.setSensorOffset(jsonObject.get("sensorOffset").getAsFloat());
                } catch (Exception ex) {
                    System.out.println("Exception in sensorOffset - " + ex.toString());
                }
            }

            if (jsonObject.has("sensorSensitivity")) {
                try {
                    pointVO.setSensorSensitivity(jsonObject.get("sensorSensitivity").getAsFloat());
                } catch (Exception ex) {
                    System.out.println("Exception in sensorSensitivity - " + ex.toString());
                }
            }

            if (jsonObject.has("sensorType")) {
                try {
                    pointVO.setSensorType(jsonObject.get("sensorType").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in sensorType - " + ex.toString());
                }
            }

            if (jsonObject.has("sensorUnits")) {
                try {
                    pointVO.setSensorUnits(jsonObject.get("sensorUnits").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in sensorUnits - " + ex.toString());
                }
            }

            if (jsonObject.has("alarmEnabled")) {
                try {
                    pointVO.setAlarmEnabled(jsonObject.get("alarmEnabled").getAsBoolean());
                } catch (Exception ex) {
                    System.out.println("Exception in pointVO alarmEnabled - " + ex.toString());
                }
            }
            
            if (jsonObject.has("sensorSubType")) {
                    try {
                        pointVO.setSensorSubType(jsonObject.get("sensorSubType").getAsString().trim());
                    } catch (Exception ex) {
                        System.out.println("Exception in pointVO sensorSubType - " + ex.toString());
                    }
                }

            if (jsonObject.has("sensorLocalOrientation")) {
                    try {
                        pointVO.setSensorLocalOrientation(jsonObject.get("sensorLocalOrientation").getAsString().trim());
                    } catch (Exception ex) {
                        System.out.println("Exception in pointVO sensorLocalOrientation - " + ex.toString());
                    }
                }
            
            if (jsonObject.has("customized")) {
                try {
                    pointVO.setCustomized(jsonObject.get("customized").getAsBoolean());
                } catch (Exception ex) {
                    System.out.println("Exception in pointVO alarmEnabled - " + ex.toString());
                }
            }

            if (jsonObject.has("apAlSetVOs")) {
                try {
                    if ((jsonArray = jsonObject.getAsJsonArray("apAlSetVOs")) != null && !jsonArray.isEmpty()) {
                        apAlByPointVOList = new ArrayList();
                        for (int j = 0; j < jsonArray.size(); j++) {
                            jsonObject = jsonArray.get(j).getAsJsonObject();
                            apAlSetVO = new ApAlSetVO();

                            if (jsonObject.has("apSetId")) {
                                try {
                                    apAlSetVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString().trim()));
                                } catch (Exception ex) {
                                    System.out.println("Exception in apSetId - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("alSetId")) {
                                try {
                                    apAlSetVO.setAlSetId(UUID.fromString(jsonObject.get("alSetId").getAsString().trim()));
                                } catch (Exception ex) {
                                    System.out.println("Exception in alSetId - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("paramName")) {
                                try {
                                    apAlSetVO.setParamName(jsonObject.get("paramName").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in paramNamee - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("fmax")) {
                                try {
                                    apAlSetVO.setFmax(jsonObject.get("fmax").getAsInt());
                                } catch (Exception ex) {
                                    System.out.println("Exception in fmax - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("resolution")) {
                                try {
                                    apAlSetVO.setResolution(jsonObject.get("resolution").getAsInt());
                                } catch (Exception ex) {
                                    System.out.println("Exception in resolution - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("dwell")) {
                                try {
                                    apAlSetVO.setDwell(jsonObject.get("dwell").getAsInt());
                                } catch (Exception ex) {
                                    System.out.println("Exception in dwell - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("period")) {
                                try {
                                    apAlSetVO.setPeriod(jsonObject.get("period").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in period - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("sampleRate")) {
                                try {
                                    apAlSetVO.setSampleRate(jsonObject.get("sampleRate").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in sampleRate - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("minFrequency")) {
                                try {
                                    apAlSetVO.setMinFrequency(jsonObject.get("minFrequency").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in minFreq - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("maxFrequency")) {
                                try {
                                    apAlSetVO.setMaxFrequency(jsonObject.get("maxFrequency").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in maxFrequency - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("lowFault")) {
                                try {
                                    apAlSetVO.setLowFault(jsonObject.get("lowFault").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in lowFault - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("lowAlert")) {
                                try {
                                    apAlSetVO.setLowAlert(jsonObject.get("lowAlert").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in lowAlert - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("highAlert")) {
                                try {
                                    apAlSetVO.setHighAlert(jsonObject.get("highAlert").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in highAlert - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("highFault")) {
                                try {
                                    apAlSetVO.setHighFault(jsonObject.get("highFault").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in highFault - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("demodHighPass")) {
                                try {
                                    apAlSetVO.setDemodHighPass(jsonObject.get("demodHighPass").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in demodHighPass - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("demodLowPass")) {
                                try {
                                    apAlSetVO.setDemodLowPass(jsonObject.get("demodLowPass").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in demodLowPass - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("lowFaultActive")) {
                                try {
                                    apAlSetVO.setLowFaultActive(jsonObject.get("lowFaultActive").getAsBoolean());
                                } catch (Exception ex) {
                                    System.out.println("Exception in lowFaultActive - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("lowAlertActive")) {
                                try {
                                    apAlSetVO.setLowAlertActive(jsonObject.get("lowAlertActive").getAsBoolean());
                                } catch (Exception ex) {
                                    System.out.println("Exception in lowAlertActive - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("highAlertActive")) {
                                try {
                                    apAlSetVO.setHighAlertActive(jsonObject.get("highAlertActive").getAsBoolean());
                                } catch (Exception ex) {
                                    System.out.println("Exception in highAlertActive - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("highFaultActive")) {
                                try {
                                    apAlSetVO.setHighFaultActive(jsonObject.get("highFaultActive").getAsBoolean());
                                } catch (Exception ex) {
                                    System.out.println("Exception in highFaultActive - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("demod")) {
                                try {
                                    apAlSetVO.setDemod(jsonObject.get("demod").getAsBoolean());
                                } catch (Exception ex) {
                                    System.out.println("Exception in demod - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("paramType")) {
                                try {
                                    apAlSetVO.setParamType(jsonObject.get("paramType").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in paramType - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("frequencyUnits")) {
                                try {
                                    apAlSetVO.setFrequencyUnits(jsonObject.get("frequencyUnits").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in freqUnits - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("paramUnits")) {
                                try {
                                    apAlSetVO.setParamUnits(jsonObject.get("paramUnits").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in paramUnits - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("paramAmpFactor")) {
                                try {
                                    apAlSetVO.setParamAmpFactor(jsonObject.get("paramAmpFactor").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in paramAmpFactor - " + ex.toString());
                                }
                            }

                            apAlSetVO.setSensorType(pointVO.getSensorType());
                            apAlByPointVOList.add(apAlSetVO);
                        }
                        pointVO.setApAlSetVOs(apAlByPointVOList);
                    }
                } catch (Exception ex) {
                    System.out.println("Exception in apAlSetVOs - " + ex.toString());
                }
            }
            pointVOList.add(pointVO);
        }

        return pointVOList;
    }

    /**
     * Parse the given json String Object and return a PointVO object
     *
     * @param content, String Object
     * @return PointVO Object
     */
    public static HwUnitVO hwUnitToPointParser(String content) {
        HwUnitVO hwUnitVO = null;
        JsonElement element;
        JsonObject jsonObject;
        JsonArray jsonArray;
        List<ApAlSetVO> apAlByPointVOList;
        ApAlSetVO apAlSetVO;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            hwUnitVO = new HwUnitVO();
            if (jsonObject.has("deviceId")) {
                try {
                    hwUnitVO.setDeviceId(jsonObject.get("deviceId").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in deviceId - " + ex.toString());
                }
            }
            if (jsonObject.has("channelType")) {
                try {
                    hwUnitVO.setChannelType(jsonObject.get("channelType").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in channelType - " + ex.toString());
                }
            }
            if (jsonObject.has("channelNum")) {
                try {
                    hwUnitVO.setChannelNum(jsonObject.get("channelNum").getAsByte());
                } catch (Exception ex) {
                    System.out.println("Exception in channelNum - " + ex.toString());
                }
            }

            if (jsonObject.has("customerAccount")) {
                try {
                    hwUnitVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in customerAccount - " + ex.toString());
                }
            }

            if (jsonObject.has("siteId")) {
                try {
                    hwUnitVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in siteId - " + ex.toString());
                }
            }

            if (jsonObject.has("areaId")) {
                try {
                    hwUnitVO.setAreaId(UUID.fromString(jsonObject.get("areaId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in areaId - " + ex.toString());
                }
            }

            if (jsonObject.has("assetId")) {
                try {
                    hwUnitVO.setAssetId(UUID.fromString(jsonObject.get("assetId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in assetId - " + ex.toString());
                }
            }

            if (jsonObject.has("pointLocationId")) {
                try {
                    hwUnitVO.setPointLocationId(UUID.fromString(jsonObject.get("pointLocationId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in pointLocationName - " + ex.toString());
                }
            }

            if (jsonObject.has("pointId")) {
                try {
                    hwUnitVO.setPointId(UUID.fromString(jsonObject.get("pointId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in pointId - " + ex.toString());
                }
            }

            if (jsonObject.has("apSetId")) {
                try {
                    hwUnitVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in apSetId - " + ex.toString());
                }
            }

            if (jsonObject.has("alSetId")) {
                try {
                    hwUnitVO.setAlSetId(UUID.fromString(jsonObject.get("alSetId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in alSetId - " + ex.toString());
                }
            }

            if (jsonObject.has("pointName")) {
                try {
                    hwUnitVO.setPointName(jsonObject.get("pointName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in pointName - " + ex.toString());
                }
            }

            if (jsonObject.has("pointType")) {
                try {
                    hwUnitVO.setPointType(jsonObject.get("pointType").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in pointType - " + ex.toString());
                }
            }

            if (jsonObject.has("autoAcknowledge")) {
                try {
                    hwUnitVO.setAutoAcknowledge(jsonObject.get("autoAcknowledge").getAsBoolean());
                } catch (Exception ex) {
                    System.out.println("Exception in autoAcknowledge - " + ex.toString());
                }
            }

            if (jsonObject.has("disabled")) {
                try {
                    hwUnitVO.setDisabled(jsonObject.get("disabled").getAsBoolean());
                } catch (Exception ex) {
                    System.out.println("Exception in isDisabled - " + ex.toString());
                }
            }

            if (jsonObject.has("sampleInterval")) {
                try {
                    hwUnitVO.setSampleInterval(jsonObject.get("sampleInterval").getAsInt());
                } catch (Exception ex) {
                    System.out.println("Exception in sampleInterval - " + ex.toString());
                }
            }

            if (jsonObject.has("sensorChannelNum")) {
                try {
                    hwUnitVO.setSensorChannelNum(jsonObject.get("sensorChannelNum").getAsInt());
                } catch (Exception ex) {
                    System.out.println("Exception in sensorChannelNum - " + ex.toString());
                }
            }

            if (jsonObject.has("sensorOffset")) {
                try {
                    hwUnitVO.setSensorOffset(jsonObject.get("sensorOffset").getAsFloat());
                } catch (Exception ex) {
                    System.out.println("Exception in sensorOffset - " + ex.toString());
                }
            }
            if (jsonObject.has("sensorSensitivity")) {
                try {
                    hwUnitVO.setSensorSensitivity(jsonObject.get("sensorSensitivity").getAsFloat());
                } catch (Exception ex) {
                    System.out.println("Exception in sensorSensitivity - " + ex.toString());
                }
            }

            if (jsonObject.has("sensorUnits")) {
                try {
                    hwUnitVO.setSensorUnits(jsonObject.get("sensorUnits").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in sensorUnits - " + ex.toString());
                }
            }
            if (jsonObject.has("alarmEnabled")) {
                try {
                    hwUnitVO.setAlarmEnabled(jsonObject.get("alarmEnabled").getAsBoolean());
                } catch (Exception ex) {
                    System.out.println("Exception in hwUnitVO alarmEnabled - " + ex.toString());
                }
            }

            if (jsonObject.has("useCustomLimits")) {
                try {
                    hwUnitVO.setUseCustomLimits(jsonObject.get("useCustomLimits").getAsBoolean());
                } catch (Exception ex) {
                    System.out.println("Exception in useCustomLimits - " + ex.toString());
                }
            }

            if (jsonObject.has("apAlSetVOs")) {
                try {
                    if ((jsonArray = jsonObject.getAsJsonArray("apAlSetVOs")) != null && !jsonArray.isEmpty()) {
                        apAlByPointVOList = new ArrayList();
                        for (int j = 0; j < jsonArray.size(); j++) {
                            jsonObject = jsonArray.get(j).getAsJsonObject();
                            apAlSetVO = new ApAlSetVO();

                            if (jsonObject.has("apSetId")) {
                                try {
                                    apAlSetVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString().trim()));
                                } catch (Exception ex) {
                                    System.out.println("Exception in apSetId - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("alSetId")) {
                                try {
                                    apAlSetVO.setAlSetId(UUID.fromString(jsonObject.get("alSetId").getAsString().trim()));
                                } catch (Exception ex) {
                                    System.out.println("Exception in alSetId - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("paramName")) {
                                try {
                                    apAlSetVO.setParamName(jsonObject.get("paramName").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in paramNamee - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("fmax")) {
                                try {
                                    apAlSetVO.setFmax(jsonObject.get("fmax").getAsInt());
                                } catch (Exception ex) {
                                    System.out.println("Exception in fmax - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("resolution")) {
                                try {
                                    apAlSetVO.setResolution(jsonObject.get("resolution").getAsInt());
                                } catch (Exception ex) {
                                    System.out.println("Exception in resolution - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("dwell")) {
                                try {
                                    apAlSetVO.setDwell(jsonObject.get("dwell").getAsInt());
                                } catch (Exception ex) {
                                    System.out.println("Exception in dwell - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("period")) {
                                try {
                                    apAlSetVO.setPeriod(jsonObject.get("period").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in period - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("sampleRate")) {
                                try {
                                    apAlSetVO.setSampleRate(jsonObject.get("sampleRate").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in sampleRate - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("minFrequency")) {
                                try {
                                    apAlSetVO.setMinFrequency(jsonObject.get("minFrequency").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in minFreq - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("maxFrequency")) {
                                try {
                                    apAlSetVO.setMaxFrequency(jsonObject.get("maxFrequency").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in maxFrequency - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("lowFault")) {
                                try {
                                    apAlSetVO.setLowFault(jsonObject.get("lowFault").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in lowFault - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("lowAlert")) {
                                try {
                                    apAlSetVO.setLowAlert(jsonObject.get("lowAlert").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in lowAlert - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("highAlert")) {
                                try {
                                    apAlSetVO.setHighAlert(jsonObject.get("highAlert").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in highAlert - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("highFault")) {
                                try {
                                    apAlSetVO.setHighFault(jsonObject.get("highFault").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in highFault - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("demodHighPass")) {
                                try {
                                    apAlSetVO.setDemodHighPass(jsonObject.get("demodHighPass").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in demodHighPass - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("demodLowPass")) {
                                try {
                                    apAlSetVO.setDemodLowPass(jsonObject.get("demodLowPass").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in demodLowPass - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("lowFaultActive")) {
                                try {
                                    apAlSetVO.setLowFaultActive(jsonObject.get("lowFaultActive").getAsBoolean());
                                } catch (Exception ex) {
                                    System.out.println("Exception in lowFaultActive - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("lowAlertActive")) {
                                try {
                                    apAlSetVO.setLowAlertActive(jsonObject.get("lowAlertActive").getAsBoolean());
                                } catch (Exception ex) {
                                    System.out.println("Exception in lowAlertActive - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("highAlertActive")) {
                                try {
                                    apAlSetVO.setHighAlertActive(jsonObject.get("highAlertActive").getAsBoolean());
                                } catch (Exception ex) {
                                    System.out.println("Exception in highAlertActive - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("highFaultActive")) {
                                try {
                                    apAlSetVO.setHighFaultActive(jsonObject.get("highFaultActive").getAsBoolean());
                                } catch (Exception ex) {
                                    System.out.println("Exception in highFaultActive - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("demod")) {
                                try {
                                    apAlSetVO.setDemod(jsonObject.get("demod").getAsBoolean());
                                } catch (Exception ex) {
                                    System.out.println("Exception in demod - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("paramType")) {
                                try {
                                    apAlSetVO.setParamType(jsonObject.get("paramType").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in paramType - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("frequencyUnits")) {
                                try {
                                    apAlSetVO.setFrequencyUnits(jsonObject.get("frequencyUnits").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in freqUnits - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("paramUnits")) {
                                try {
                                    apAlSetVO.setParamUnits(jsonObject.get("paramUnits").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in paramUnits - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("paramAmpFactor")) {
                                try {
                                    apAlSetVO.setParamAmpFactor(jsonObject.get("paramAmpFactor").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in paramAmpFactor - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("sensorType")) {
                                try {
                                    apAlSetVO.setSensorType(jsonObject.get("sensorType").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in sensorType - " + ex.toString());
                                }
                            }
                            if (jsonObject.has("sampleRate")) {
                                try {
                                    apAlSetVO.setSampleRate(jsonObject.get("sampleRate").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in sampleRate - " + ex.toString());
                                }
                            }
                            apAlByPointVOList.add(apAlSetVO);
                        }
                        hwUnitVO.setApAlSetVOs(apAlByPointVOList);
                    }
                } catch (Exception ex) {
                    System.out.println("Exception in apAlSetVOs - " + ex.toString());
                }
            }

        }

        return hwUnitVO;
    }

    /**
     * Parse the given json String Object and return a ReplaceDeviceVO object
     *
     * @param content, String Object
     * @return ReplaceDeviceVO Object
     */
    public static DeviceVO manageDeviceParser(String content) {
        DeviceVO deviceVO = null;
        JsonElement element;
        JsonObject jsonObject;
        JsonArray jsonArray;
        JsonArray jsonApAlSetArray;
        List<ApAlSetVO> apAlByPointVOList;
        ApAlSetVO apAlSetVO;
        List<PointVO> pointVOList;
        PointVO pointVO;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            deviceVO = new DeviceVO();
            if (jsonObject.has("request") && jsonObject.has("attributes")) {
                deviceVO.setRequest(jsonObject.get("request").getAsString());
                //Since jsonObject has elements of 'request' and 'attributes' need to get the  jsonObject of 'attributes' first.
                jsonObject = jsonObject.getAsJsonObject("attributes");
            }
            if (jsonObject.has("existingDeviceId")) {
                try {
                    deviceVO.setExistingDeviceId(jsonObject.get("existingDeviceId").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in existingDeviceId - " + ex.toString());
                }
            }
            if (jsonObject.has("newDeviceId")) {
                try {
                    deviceVO.setNewDeviceId(jsonObject.get("newDeviceId").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in newDeviceId - " + ex.toString());
                }
            }
            if (jsonObject.has("dbpointListExists")) {
                try {
                    deviceVO.setDbpointListExists(jsonObject.get("dbpointListExists").getAsBoolean());
                } catch (Exception ex) {
                    System.out.println("Exception in dbpointListExists - " + ex.toString());
                }
            }
            if (jsonObject.has("channelType")) {
                try {
                    deviceVO.setChannelType(jsonObject.get("channelType").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in channelType - " + ex.toString());
                }
            }
            if (jsonObject.has("channelNum")) {
                try {
                    deviceVO.setChannelNum(jsonObject.get("channelNum").getAsByte());
                } catch (Exception ex) {
                    System.out.println("Exception in channelNum - " + ex.toString());
                }
            }
            if (jsonObject.has("channelNumLabel")) {
                try {
                    deviceVO.setChannelNumLabel(jsonObject.get("channelNumLabel").getAsString());
                } catch (Exception ex) {
                    System.out.println("Exception in channelNumLabel - " + ex.toString());
                }
            }
            if (jsonObject.has("restrictions")) {
                try {
                    deviceVO.setRestrictions(jsonObject.get("restrictions").getAsString());
                } catch (Exception ex) {
                    System.out.println("Exception in restrictions - " + ex.toString());
                }
            }

            if (jsonObject.has("customerAccount")) {
                try {
                    deviceVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in customerAccount - " + ex.toString());
                }
            }

            if (jsonObject.has("siteId")) {
                try {
                    deviceVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in siteId - " + ex.toString());
                }
            }

            if (jsonObject.has("areaId")) {
                try {
                    deviceVO.setAreaId(UUID.fromString(jsonObject.get("areaId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in areaId - " + ex.toString());
                }
            }

            if (jsonObject.has("assetId")) {
                try {
                    deviceVO.setAssetId(UUID.fromString(jsonObject.get("assetId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in assetId - " + ex.toString());
                }
            }

            if (jsonObject.has("pointLocationId")) {
                try {
                    deviceVO.setPointLocationId(UUID.fromString(jsonObject.get("pointLocationId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in pointLocationName - " + ex.toString());
                }
            }

            if (jsonObject.has("siteName")) {
                try {
                    deviceVO.setSiteName(jsonObject.get("siteName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in siteName - " + ex.toString());
                }
            }

            if (jsonObject.has("areaName")) {
                try {
                    deviceVO.setAreaName(jsonObject.get("areaName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in areaName - " + ex.toString());
                }
            }

            if (jsonObject.has("assetName")) {
                try {
                    deviceVO.setAssetName(jsonObject.get("assetName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in assetName - " + ex.toString());
                }
            }

            if (jsonObject.has("pointLocationName")) {
                try {
                    deviceVO.setPointLocationName(jsonObject.get("pointLocationName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in pointLocationName - " + ex.toString());
                }
            }
            
            if (jsonObject.has("deviceType")) {
                try {
                    deviceVO.setDeviceType(jsonObject.get("deviceType").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in deviceType - " + ex.toString());
                }
            }

            if (jsonObject.has("pointId")) {
                try {
                    deviceVO.setPointId(UUID.fromString(jsonObject.get("pointId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in pointId - " + ex.toString());
                }
            }

            if (jsonObject.has("apSetId")) {
                try {
                    deviceVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in apSetId - " + ex.toString());
                }
            }

            if (jsonObject.has("alSetId")) {
                try {
                    deviceVO.setAlSetId(UUID.fromString(jsonObject.get("alSetId").getAsString().trim()));
                } catch (Exception ex) {
                    System.out.println("Exception in alSetId - " + ex.toString());
                }
            }

            if (jsonObject.has("pointName")) {
                try {
                    deviceVO.setPointName(jsonObject.get("pointName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in pointName - " + ex.toString());
                }
            }

            if (jsonObject.has("pointType")) {
                try {
                    deviceVO.setPointType(jsonObject.get("pointType").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in pointType - " + ex.toString());
                }
            }

            if (jsonObject.has("autoAcknowledge")) {
                try {
                    deviceVO.setAutoAcknowledge(jsonObject.get("autoAcknowledge").getAsBoolean());
                } catch (Exception ex) {
                    System.out.println("Exception in autoAcknowledge - " + ex.toString());
                }
            }

            if (jsonObject.has("disabled")) {
                try {
                    deviceVO.setDisabled(jsonObject.get("disabled").getAsBoolean());
                } catch (Exception ex) {
                    System.out.println("Exception in isDisabled - " + ex.toString());
                }
            }

            if (jsonObject.has("sampleInterval")) {
                try {
                    deviceVO.setSampleInterval(jsonObject.get("sampleInterval").getAsInt());
                } catch (Exception ex) {
                    System.out.println("Exception in sampleInterval - " + ex.toString());
                }
            }

            if (jsonObject.has("sensorChannelNum")) {
                try {
                    deviceVO.setSensorChannelNum(jsonObject.get("sensorChannelNum").getAsInt());
                } catch (Exception ex) {
                    System.out.println("Exception in sensorChannelNum - " + ex.toString());
                }
            }

            if (jsonObject.has("sensorOffset")) {
                try {
                    deviceVO.setSensorOffset(jsonObject.get("sensorOffset").getAsFloat());
                } catch (Exception ex) {
                    System.out.println("Exception in sensorOffset - " + ex.toString());
                }
            }
            if (jsonObject.has("sensorSensitivity")) {
                try {
                    deviceVO.setSensorSensitivity(jsonObject.get("sensorSensitivity").getAsFloat());
                } catch (Exception ex) {
                    System.out.println("Exception in sensorSensitivity - " + ex.toString());
                }
            }

            if (jsonObject.has("sensorUnits")) {
                try {
                    deviceVO.setSensorUnits(jsonObject.get("sensorUnits").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in sensorUnits - " + ex.toString());
                }
            }
            if (jsonObject.has("sensorSubType")) {
                try {
                    deviceVO.setSensorSubType(jsonObject.get("sensorSubType").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in sensorSubType - " + ex.toString());
                }
            }
            if (jsonObject.has("sensorLocalOrientation")) {
                try {
                    deviceVO.setSensorLocalOrientation(jsonObject.get("sensorLocalOrientation").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in sensorLocalOrientation - " + ex.toString());
                }
            }
            if (jsonObject.has("alarmEnabled")) {
                try {
                    deviceVO.setAlarmEnabled(jsonObject.get("alarmEnabled").getAsBoolean());
                } catch (Exception ex) {
                    System.out.println("Exception in hwUnitVO alarmEnabled - " + ex.toString());
                }
            }

            if (jsonObject.has("useCustomLimits")) {
                try {
                    deviceVO.setUseCustomLimits(jsonObject.get("useCustomLimits").getAsBoolean());
                } catch (Exception ex) {
                    System.out.println("Exception in useCustomLimits - " + ex.toString());
                }
            }

            if (jsonObject.has("basestationPortNum")) {
                try {
                    deviceVO.setBasestationPortNum(jsonObject.get("basestationPortNum").getAsShort());
                } catch (Exception ex) {
                    System.out.println("Exception in basestationPortNum - " + ex.toString());
                }
            }

            if (jsonObject.has("batteryVoltage")) {
                try {
                    deviceVO.setBatteryVoltage(jsonObject.get("batteryVoltage").getAsFloat());
                } catch (Exception ex) {
                    System.out.println("Exception in batteryVoltage - " + ex.toString());
                }
            }

            if (jsonObject.has("lastSample")) {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    deviceVO.setLastSample(new Timestamp(sdf.parse(jsonObject.get("lastSample").getAsString()).getTime()));
                } catch (Exception ex) {
                    System.out.println("Exception in lastSample - " + ex.toString());
                }
            }

            if (jsonObject.has("pointListExists")) {
                try {
                    deviceVO.setDbpointListExists(jsonObject.get("pointListExists").getAsBoolean());
                } catch (Exception ex) {
                    System.out.println("Exception in pointListExists - " + ex.toString());
                }
            }

            if (jsonObject.has("pointList")) {
                try {
                    if ((jsonArray = jsonObject.getAsJsonArray("pointList")) != null && !jsonArray.isEmpty()) {
                        pointVOList = new ArrayList();
                        for (int j = 0; j < jsonArray.size(); j++) {
                            jsonObject = jsonArray.get(j).getAsJsonObject();
                            pointVO = new PointVO();
                            pointVO.setCustomerAccount(deviceVO.getCustomerAccount());
                            pointVO.setSiteId(deviceVO.getSiteId());
                            pointVO.setAreaId(deviceVO.getAreaId());
                            pointVO.setAssetId(deviceVO.getAssetId());
                            pointVO.setPointLocationId(deviceVO.getPointLocationId());

                            if (jsonObject.has("pointId")) {
                                try {
                                    pointVO.setPointId(UUID.fromString(jsonObject.get("pointId").getAsString().trim()));
                                } catch (Exception ex) {
                                    System.out.println("Exception in pointId - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("apSetId")) {
                                try {
                                    pointVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString().trim()));
                                } catch (Exception ex) {
                                    System.out.println("Exception in apSetId - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("alSetId")) {
                                try {
                                    pointVO.setAlSetId(UUID.fromString(jsonObject.get("alSetId").getAsString().trim()));
                                } catch (Exception ex) {
                                    System.out.println("Exception in alSetId - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("pointName")) {
                                try {
                                    pointVO.setPointName(jsonObject.get("pointName").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in pointName - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("autoAcknowledge")) {
                                try {
                                    pointVO.setAutoAcknowledge(jsonObject.get("autoAcknowledge").getAsBoolean());
                                } catch (Exception ex) {
                                    System.out.println("Exception in autoAcknowledge - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("disabled")) {
                                try {
                                    pointVO.setDisabled(jsonObject.get("disabled").getAsBoolean());
                                } catch (Exception ex) {
                                    System.out.println("Exception in isDisabled - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("pointType")) {
                                try {
                                    pointVO.setPointType(jsonObject.get("pointType").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in pointType - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("sensorChannelNum")) {
                                try {
                                    pointVO.setSensorChannelNum(jsonObject.get("sensorChannelNum").getAsInt());
                                } catch (Exception ex) {
                                    System.out.println("Exception in sensorChannelNum - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("sensorOffset")) {
                                try {
                                    pointVO.setSensorOffset(jsonObject.get("sensorOffset").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in sensorOffset - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("sensorSensitivity")) {
                                try {
                                    pointVO.setSensorSensitivity(jsonObject.get("sensorSensitivity").getAsFloat());
                                } catch (Exception ex) {
                                    System.out.println("Exception in sensorSensitivity - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("sensorType")) {
                                try {
                                    pointVO.setSensorType(jsonObject.get("sensorType").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in sensorType - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("sensorUnits")) {
                                try {
                                    pointVO.setSensorUnits(jsonObject.get("sensorUnits").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in sensorUnits - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("alarmEnabled")) {
                                try {
                                    pointVO.setAlarmEnabled(jsonObject.get("alarmEnabled").getAsBoolean());
                                } catch (Exception ex) {
                                    System.out.println("Exception in pointVO alarmEnabled - " + ex.toString());
                                }
                            }
                            
                            if (jsonObject.has("sensorSubType")) {
                                try {
                                    pointVO.setSensorSubType(jsonObject.get("sensorSubType").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in pointVO sensorSubType - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("sensorLocalOrientation")) {
                                try {
                                    pointVO.setSensorLocalOrientation(jsonObject.get("sensorLocalOrientation").getAsString().trim());
                                } catch (Exception ex) {
                                    System.out.println("Exception in pointVO sensorLocalOrientation - " + ex.toString());
                                }
                            }

                            if (jsonObject.has("apAlSetVOs")) {
                                try {
                                    if ((jsonApAlSetArray = jsonObject.getAsJsonArray("apAlSetVOs")) != null && !jsonApAlSetArray.isEmpty()) {
                                        apAlByPointVOList = new ArrayList();
                                        for (int k = 0; k < jsonApAlSetArray.size(); k++) {
                                            jsonObject = jsonApAlSetArray.get(k).getAsJsonObject();
                                            apAlSetVO = new ApAlSetVO();

                                            if (jsonObject.has("customerAccount")) {
                                                try {
                                                    apAlSetVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString().trim());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in customerAccount - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("siteId")) {
                                                try {
                                                    apAlSetVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString().trim()));
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in siteId - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("apSetId")) {
                                                try {
                                                    apAlSetVO.setApSetId(UUID.fromString(jsonObject.get("apSetId").getAsString().trim()));
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in apSetId - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("alSetId")) {
                                                try {
                                                    apAlSetVO.setAlSetId(UUID.fromString(jsonObject.get("alSetId").getAsString().trim()));
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in alSetId - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("paramName")) {
                                                try {
                                                    apAlSetVO.setParamName(jsonObject.get("paramName").getAsString().trim());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in paramNamee - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("fmax")) {
                                                try {
                                                    apAlSetVO.setFmax(jsonObject.get("fmax").getAsInt());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in fmax - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("resolution")) {
                                                try {
                                                    apAlSetVO.setResolution(jsonObject.get("resolution").getAsInt());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in resolution - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("dwell")) {
                                                try {
                                                    apAlSetVO.setDwell(jsonObject.get("dwell").getAsInt());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in dwell - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("period")) {
                                                try {
                                                    apAlSetVO.setPeriod(jsonObject.get("period").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in period - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("sampleRate")) {
                                                try {
                                                    apAlSetVO.setSampleRate(jsonObject.get("sampleRate").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in sampleRate - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("minFrequency")) {
                                                try {
                                                    apAlSetVO.setMinFrequency(jsonObject.get("minFrequency").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in minFreq - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("maxFrequency")) {
                                                try {
                                                    apAlSetVO.setMaxFrequency(jsonObject.get("maxFrequency").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in maxFrequency - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("lowFault")) {
                                                try {
                                                    apAlSetVO.setLowFault(jsonObject.get("lowFault").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in lowFault - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("lowAlert")) {
                                                try {
                                                    apAlSetVO.setLowAlert(jsonObject.get("lowAlert").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in lowAlert - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("highAlert")) {
                                                try {
                                                    apAlSetVO.setHighAlert(jsonObject.get("highAlert").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in highAlert - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("highFault")) {
                                                try {
                                                    apAlSetVO.setHighFault(jsonObject.get("highFault").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in highFault - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("demodHighPass")) {
                                                try {
                                                    apAlSetVO.setDemodHighPass(jsonObject.get("demodHighPass").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in demodHighPass - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("demodLowPass")) {
                                                try {
                                                    apAlSetVO.setDemodLowPass(jsonObject.get("demodLowPass").getAsFloat());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in demodLowPass - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("lowFaultActive")) {
                                                try {
                                                    apAlSetVO.setLowFaultActive(jsonObject.get("lowFaultActive").getAsBoolean());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in lowFaultActive - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("lowAlertActive")) {
                                                try {
                                                    apAlSetVO.setLowAlertActive(jsonObject.get("lowAlertActive").getAsBoolean());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in lowAlertActive - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("highAlertActive")) {
                                                try {
                                                    apAlSetVO.setHighAlertActive(jsonObject.get("highAlertActive").getAsBoolean());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in highAlertActive - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("highFaultActive")) {
                                                try {
                                                    apAlSetVO.setHighFaultActive(jsonObject.get("highFaultActive").getAsBoolean());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in highFaultActive - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("demod")) {
                                                try {
                                                    apAlSetVO.setDemod(jsonObject.get("demod").getAsBoolean());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in demod - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("paramType")) {
                                                try {
                                                    apAlSetVO.setParamType(jsonObject.get("paramType").getAsString().trim());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in paramType - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("frequencyUnits")) {
                                                try {
                                                    apAlSetVO.setFrequencyUnits(jsonObject.get("frequencyUnits").getAsString().trim());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in freqUnits - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("paramUnits")) {
                                                try {
                                                    apAlSetVO.setParamUnits(jsonObject.get("paramUnits").getAsString().trim());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in paramUnits - " + ex.toString());
                                                }
                                            }

                                            if (jsonObject.has("paramAmpFactor")) {
                                                try {
                                                    apAlSetVO.setParamAmpFactor(jsonObject.get("paramAmpFactor").getAsString().trim());
                                                } catch (Exception ex) {
                                                    System.out.println("Exception in paramAmpFactor - " + ex.toString());
                                                }
                                            }

                                            apAlSetVO.setSensorType(pointVO.getSensorType());
                                            apAlByPointVOList.add(apAlSetVO);
                                        }
                                        pointVO.setApAlSetVOs(apAlByPointVOList);
                                    }
                                } catch (Exception ex) {
                                    System.out.println("Exception in apAlSetVOs - " + ex.toString());
                                }
                            }
                            pointVOList.add(pointVO);
                        }
                        deviceVO.setPointList(pointVOList);
                    }
                } catch (Exception ex) {
                    System.out.println("Exception in pointList - " + ex.toString());
                }
            }

        }

        return deviceVO;
    }

    /**
     * Parse the given json String Object and return a DeviceStatusVO object
     *
     * @param content, String Object
     * @return DeviceStatusVO Object
     */
    public static DeviceStatusVO deviceStatusParser(String content) {
        DeviceStatusVO deviceStatusVO = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ"); 
        JsonElement element;
        JsonObject jsonObject;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            deviceStatusVO = new DeviceStatusVO();
           
            if (jsonObject.has("device_id")) {
                try {
                    deviceStatusVO.setDeviceId(jsonObject.get("device_id").getAsString().trim());
                } catch (Exception ex) {
                    LOGGER.error("\"Exception in device_id :{}", ex.toString());
                    System.out.println("Exception in device_id - " + ex.toString());
                }
            }

            if (jsonObject.has("battery_voltage")) {
                try {
                    deviceStatusVO.setBatteryVoltage(jsonObject.get("battery_voltage").getAsFloat());
                } catch (Exception ex) {
                    LOGGER.error("\"Exception in battery_voltage :{}", ex.toString());
                    System.out.println("Exception in battery_voltage - " + ex.toString());
                }
            } else {
                deviceStatusVO.setBatteryVoltage(-1);
            }

            if (jsonObject.has("last_sample")) {
                try {
                    deviceStatusVO.setLastSample(new Timestamp(sdf.parse(jsonObject.get("last_sample").getAsString()).getTime()));
                } catch (Exception ex) {
                    LOGGER.error("\"Exception in last_sample :{}", ex.toString());
                    System.out.println("Exception in last_sample - " + ex.toString());
                }
            }

        }

        return deviceStatusVO;
    }

    /**
     * Parse the given json String Object and return a SubscriptionsVO object
     *
     * @param content, String Object
     * @return SubscriptionsVO Object
     */
    public static SubscriptionsVO subscriptionsParser(String content) {

        SubscriptionsVO subscriptionsVO = null;
        JsonElement element;
        JsonObject jsonObject;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            subscriptionsVO = new SubscriptionsVO();

            if (jsonObject.has("requestType")) {
                try {
                    subscriptionsVO.setRequestType(jsonObject.get("requestType").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in requestType - " + ex.toString());
                }
            }
            if (jsonObject.has("customerAccount")) {
                try {
                    subscriptionsVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in customerAccount - " + ex.toString());
                }
            }

            if (jsonObject.has("siteId")) {
                try {
                    subscriptionsVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                } catch (Exception ex) {
                    System.out.println("Exception in siteId - " + ex.toString());
                }
            }

            if (jsonObject.has("subscriptionType")) {
                try {
                    subscriptionsVO.setSubscriptionType(jsonObject.get("subscriptionType").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in subscriptionType - " + ex.toString());
                }
            }

            if (jsonObject.has("mqProtocol")) {
                try {
                    subscriptionsVO.setMqProtocol(jsonObject.get("mqProtocol").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in mqProtocol - " + ex.toString());
                }
            }

            if (jsonObject.has("mqConnectString")) {
                try {
                    subscriptionsVO.setMqConnectString(jsonObject.get("mqConnectString").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in mqConnectString - " + ex.toString());
                }
            }

            if (jsonObject.has("mqUser")) {
                try {
                    subscriptionsVO.setMqUser(jsonObject.get("mqUser").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in mqUser - " + ex.toString());
                }
            }

            if (jsonObject.has("mqPwd")) {
                try {
                    subscriptionsVO.setMqPwd(jsonObject.get("mqPwd").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in mqPwd - " + ex.toString());
                }
            }

            if (jsonObject.has("mqQueueName")) {
                try {
                    subscriptionsVO.setMqQueueName(jsonObject.get("mqQueueName").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in mqQueueName - " + ex.toString());
                }
            }

            if (jsonObject.has("mqClientId")) {
                try {
                    subscriptionsVO.setMqClientId(jsonObject.get("mqClientId").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in mqClientId - " + ex.toString());
                }
            }

            if (jsonObject.has("webhookUrl")) {
                try {
                    subscriptionsVO.setWebhookUrl(jsonObject.get("webhookUrl").getAsString().trim());
                } catch (Exception ex) {
                    System.out.println("Exception in webhookUrl - " + ex.toString());
                }
            }

            if (jsonObject.has("internal")) {
                try {
                    subscriptionsVO.setInternal(jsonObject.get("internal").getAsBoolean());
                } catch (Exception ex) {
                    System.out.println("Exception in internal - " + ex.toString());
                }
            }
        }
        return subscriptionsVO;
    }
    
    /**
     * create JSON to POST to the KafkaProxy service
     *
     * @param hwUnitVO, HwUnitVO Object
     */
    public static void createAndSendKafkaMessageForDeleteHwUnit(HwUnitVO hwUnitVO) {
        StringBuilder json;

        try {
            json = new StringBuilder();
            json
                    .append("{\"device_id\":").append((hwUnitVO.getDeviceId() != null) ? "\"" + hwUnitVO.getDeviceId() + "\"" : null).append(",")
                    .append("\"channel_type\":").append((hwUnitVO.getChannelType() != null) ? "\"" + hwUnitVO.getChannelType() + "\"" : null).append(",")
                    .append("\"channel_num\":").append(hwUnitVO.getChannelNum()).append(",")
                    .append("\"delete\":\"true\"}");
            ConfigService.sendKafkaMessage(json.toString(), "Config");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * create JSON to POST to the KafkaProxy service
     *
     * @param deviceId, String Object
     * @param channelType, String Object
     * @param channelNum, int
     */
    public static void createAndSendDeleteMessageToKafka(String deviceId, String channelType, int channelNum) {
        StringBuilder json;

        try {
            json = new StringBuilder();
            json
                    .append("{\"device_id\":").append((deviceId != null) ? "\"" + deviceId + "\"" : null).append(",")
                    .append("\"channel_type\":").append((channelType != null) ? "\"" + channelType + "\"" : null).append(",")
                    .append("\"channel_num\":").append(channelNum).append(",")
                    .append("\"delete\":\"true\"}");
            ConfigService.sendKafkaMessage(json.toString(), "Config");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
    
    /**
     * create JSON to POST to the KafkaProxy service
     *
     * @param subscriptionsList, list of Subscriptions Objects
     */
    public static void createAndSendSubscriptionsKafkaMessage(List<Subscriptions> subscriptionsList) {
        StringBuilder json;
        try {
            json = new StringBuilder();
            json
                    .append("{\"customerId\":\"").append(subscriptionsList.get(0).getCustomerAccount()).append("\",")
                    .append("\"siteId\":\"").append(subscriptionsList.get(0).getSiteId()).append("\",")
                    .append("\"subscriptions\":[");
                    for (int i = 0; i < subscriptionsList.size(); i++) {
                        json.append("{");
                        json.append("\"subscriptionType\":\"").append(subscriptionsList.get(i).getSubscriptionType()).append("\",");
                        json.append("\"mqProtocol\":\"").append(subscriptionsList.get(i).getMqProtocol()).append("\",");
                        json.append("\"mqConnectString\":\"").append(subscriptionsList.get(i).getMqConnectString()).append("\",");
                        json.append("\"mqUser\":\"").append(subscriptionsList.get(i).getMqUser()).append("\",");
                        json.append("\"mqPwd\":\"").append(subscriptionsList.get(i).getMqPwd()).append("\",");
                        json.append("\"mqQueueName\":\"").append(subscriptionsList.get(i).getMqQueueName()).append("\",");
                        json.append("\"mqClientId\":\"").append(subscriptionsList.get(i).getMqClientId()).append("\",");
                        json.append("\"isInternal\":").append(subscriptionsList.get(i).isInternal());
                        json.append("}");
                        json.append(i < subscriptionsList.size() - 1 ? "," : "");
                    }
                    json.append("]}");
            ConfigService.sendKafkaMessage(json.toString(), "Subscriptions");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
    
    /**
     * create JSON to POST to the KafkaProxy service
     *
     * @param customerAccount, String Object
     * @param siteId, UUID object
     */
    public static void createAndSendSubscriptionsKafkaMessage(String customerAccount, UUID siteId) {
        StringBuilder json;
        try {
            json = new StringBuilder();
            json
                    .append("{\"customerId\":\"").append(customerAccount).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"delete\":true}");
            ConfigService.sendKafkaMessage(json.toString(), "Subscriptions");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
