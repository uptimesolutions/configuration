/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.utils;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.logging.FileHandler;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author kpati
 */
public class CQLFileHandler extends FileHandler {

    /**
     * *
     * In order to ensure the most recent log file is the file this one owns, we flush before checking the directory for most recent file.
     *
     * But we must keep other log handlers from flushing in between and making a NEW recent file.
     */
    private String pattern;

    public CQLFileHandler(String pattern) throws IOException,
            SecurityException {
        super(pattern);
        this.pattern = pattern;
    }

    /**
     * *
     * Finds the most recent log file matching the pattern. This is just a guess - if you have a complicated pattern format it may not work.
     *
     * IMPORTANT: This log file is still in use. You must removeHandler() on the logger first, .close() this handler, then add a NEW handler to your logger. THEN, you can read the file.
     *
     * Currently supported format strings: g, u
     *
     * @return A File of the current log file, or null on error.
     */
    public synchronized File getCurrentLogFile() {
        super.flush();
        Path currentDirectoryPath = FileSystems.getDefault().getPath("");
        String currentDirectoryName = currentDirectoryPath.toAbsolutePath().toString();
        File dir = new File(currentDirectoryName);
        File[] files = dir.listFiles((dir1, name) -> name.contains("cql"));
        return files[0];

//            final String patternRegex =
//                    // handle incremental number formats
//                    pattern.replaceAll("%[u]", "\\d*") +
//                    // handle default case where %g is appended to end
//                    "(\\.\\d*)?$";
//            final Pattern re = Pattern.compile(patternRegex);
//            final Matcher matcher = re.matcher("");
//            // check all files in the directory where this log would be
//            final File basedir = new File(pattern).getParentFile();
//            final File[] logs = basedir.listFiles(new FileFilter() {
//                
//                @Override
//                public boolean accept(final File pathname) {
//                    // only get files that are part of the pattern
//                    matcher.reset(pathname.getAbsolutePath());
//
//                    return matcher.find();
//                }
//
//            });
//
//            return findMostRecentLog(logs);
    }

    private File findMostRecentLog(File[] logs) {
        if (logs.length > 0) {
            long mostRecentDate = 0;
            int mostRecentIdx = 0;
            for (int i = 0; i < logs.length; i++) {
                final long d = logs[i].lastModified();

                if (d >= mostRecentDate) {
                    mostRecentDate = d;
                    mostRecentIdx = i;
                }

            }
            return logs[mostRecentIdx];
        } else {
            return null;
        }
    }

}
