/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.apalbypoint;

import com.uptime.cassandra.config.dao.ApAlByPointDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.PointLocationsDAO;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.cassandra.config.entity.Points;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.utils.DelegateUtil;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.PointVO;
import com.uptime.services.util.ServiceUtil;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class UpdateApAlByPointDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateApAlByPointDelegate.class.getName());
    private final ApAlByPointDAO apAlByPointDAO;
    private final PointLocationsDAO pointLocationsDAO;

    /**
     * Constructor
     */
    public UpdateApAlByPointDelegate() {
        apAlByPointDAO = ConfigMapperImpl.getInstance().apAlByPointDAO();
        pointLocationsDAO = ConfigMapperImpl.getInstance().pointLocationsDAO();
    }

    /**
     * Update Rows in Cassandra based on the given object
     *
     * @param pointVO, PointVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String updateApAlByPoint(PointVO pointVO) throws IllegalArgumentException, Exception {
        List<ApAlByPoint> updatedApAlByPoint;
        List<Points> pointsList;
        List<PointLocations> pointLocationsList;
        String errorMsg;
        
        // Insert updated entities into Cassandra
        try {
            updatedApAlByPoint = DelegateUtil.getApAlByPointList(pointVO);
            
            if ((errorMsg = ServiceUtil.validateObjects(updatedApAlByPoint, Boolean.TRUE)) == null) {
                apAlByPointDAO.update(updatedApAlByPoint);
                
                if ((pointLocationsList = pointLocationsDAO.findByPK(pointVO.getCustomerAccount(), pointVO.getSiteId(), pointVO.getAreaId(), pointVO.getAssetId(), pointVO.getPointLocationId())) != null && !pointLocationsList.isEmpty()) {
                    if (pointVO.getPointType() == null || pointVO.getPointType().length() == 0) {
                        if ((pointsList = ConfigMapperImpl.getInstance().pointsDAO().findByPK(pointVO.getCustomerAccount(), pointVO.getSiteId(), pointVO.getAreaId(), pointVO.getAssetId(), pointVO.getPointLocationId(), pointVO.getPointId() , pointVO.getApSetId())) != null && !pointsList.isEmpty()) {
                            pointVO.setPointType(pointsList.get(0).getPointType());
                            pointVO.setSensorChannelNum(pointsList.get(0).getSensorChannelNum());
                            System.out.println("set PointType -" + pointVO.getPointType());
                        }
                    }
                    if (!pointVO.isDisabled()) {
                        JsonUtil.createAndSendDeleteMessageToKafka(pointLocationsList.get(0).getDeviceSerialNumber(), pointVO.getPointType(), pointVO.getSensorChannelNum());
                    }
                }
                
                return "{\"outcome\":\"Updated ApAlByPoints successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to update ApAlByPoints.\"}";
    }

}
