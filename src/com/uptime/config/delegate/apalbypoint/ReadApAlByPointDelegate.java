/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.apalbypoint;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.dao.ApAlByPointDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.cassandra.presets.dao.CustomerApSetDAO;
import com.uptime.cassandra.presets.dao.GlobalApAlSetsByCustomerDAO;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteApAlSetsByCustomerDAO;
import com.uptime.cassandra.presets.entity.CustomerApSet;
import com.uptime.cassandra.presets.entity.GlobalApAlSetsByCustomer;
import com.uptime.cassandra.presets.entity.SiteApAlSetsByCustomer;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class ReadApAlByPointDelegate {
    private final ApAlByPointDAO apAlByPointDAO;
    private final GlobalApAlSetsByCustomerDAO globalApAlSetsByCustomerDAO;
    private final SiteApAlSetsByCustomerDAO siteApAlSetsByCustomerDAO;
    
    /**
     * Constructor
     */
    public ReadApAlByPointDelegate() {
        apAlByPointDAO = ConfigMapperImpl.getInstance().apAlByPointDAO();
        globalApAlSetsByCustomerDAO = PresetsMapperImpl.getInstance().globalApAlSetsByCustomerDAO();
        siteApAlSetsByCustomerDAO = PresetsMapperImpl.getInstance().siteApAlSetsByCustomerDAO();
    }

    /**
     * Return a List of ApAlByPoint Objects by the given params
     * 
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param locationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetId, UUID Object
     * @return List of ApAlByPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<ApAlByPoint> getByCustomerSiteAreaAssetLocationPointApSet(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID locationId, UUID pointId, UUID apSetId) throws IllegalArgumentException, ReadTimeoutException, UnavailableException {
        return apAlByPointDAO.findByCustomerSiteAreaAssetLocationPointApSet(customerAccount, siteId, areaId, assetId, locationId, pointId, apSetId);
    }
    
    /**
     * Return a List of ApAlByPoint Objects by the given params
     * 
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param locationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetIdList, List of UUID Objects
     * @return List of ApAlByPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<ApAlByPoint> getByCustomerSiteAreaAssetLocationPointApSets(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID locationId, UUID pointId, List<UUID> apSetIdList) throws IllegalArgumentException, ReadTimeoutException, UnavailableException {
        return apAlByPointDAO.findByCustomerSiteAreaAssetLocationPointApSets(customerAccount, siteId, areaId, assetId, locationId, pointId, apSetIdList);
    }
    
    /**
     * Return a List of ApAlByPoint Objects by the given params
     * 
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param locationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @return List of ApAlByPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<ApAlByPoint> getByCustomerSiteAreaAssetLocationPointApSetAlSet(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID locationId, UUID pointId, UUID apSetId, UUID alSetId) throws IllegalArgumentException, ReadTimeoutException, UnavailableException {
        return apAlByPointDAO.findByCustomerSiteAreaAssetLocationPointApSetAlSet(customerAccount, siteId, areaId, assetId, locationId, pointId, apSetId, alSetId);
    }
    
    /**
     * Return a List of ApAlByPoint Objects by the given params
     * 
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param locationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @param paramName, String Object
     * @return List of ApAlByPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<ApAlByPoint> getApAlByPointByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID locationId, UUID pointId, UUID apSetId, UUID alSetId, String paramName) throws IllegalArgumentException, ReadTimeoutException, UnavailableException {
        return apAlByPointDAO.findByPK(customerAccount, siteId, areaId, assetId, locationId, pointId, apSetId, alSetId, paramName);
    }
    
    /**
     * Return a List of GlobalApAlSetsByCustomer Objects from global_ap_al_sets_by_customer table by
     * the given params
     *
     * @param customerAccount, String Object
     * @param sensorType, String Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @return List of GlobalApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalApAlSetsByCustomer> findByGlobalApSetAlSet(String customerAccount, String sensorType, UUID apSetId, UUID alSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalApAlSetsByCustomerDAO.findByPK(customerAccount, sensorType, apSetId, alSetId);
    }
    
    /**
     * Return a List of SiteApAlSetsByCustomer Objects from site_ap_al_sets_by_customer table by
     * the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param sensorType, String Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @return List of SiteApAlSetsByCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteApAlSetsByCustomer> findBySiteApSetAlSet(String customerAccount, UUID siteId, String sensorType, UUID apSetId, UUID alSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteApAlSetsByCustomerDAO.findByPK(customerAccount, siteId, sensorType, apSetId, alSetId);
    }
}
