/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.apalbypoint;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.dao.ApAlByPointDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.PointLocationsDAO;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.cassandra.config.entity.Points;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.utils.DelegateUtil;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.PointVO;
import com.uptime.services.util.ServiceUtil;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class CreateApAlByPointDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateApAlByPointDelegate.class.getName());
    private final ApAlByPointDAO apAlByPointDAO;
    private final PointLocationsDAO pointLocationsDAO;

    /**
     * Constructor
     */
    public CreateApAlByPointDelegate() {
        apAlByPointDAO = ConfigMapperImpl.getInstance().apAlByPointDAO();
        pointLocationsDAO = ConfigMapperImpl.getInstance().pointLocationsDAO();
    }

    /**
     * Insert new Rows into Cassandra based on the given object
     *
     * @param pointVO, PointVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String createApAlByPoint(PointVO pointVO) throws IllegalArgumentException, Exception {
        List<PointLocations> pointLocationsList;
        List<ApAlByPoint> apAlByPoints = null;
        List<Points> pointsList;
        String errorMsg;

        if (pointVO != null) {
            try {
                pointLocationsList = pointLocationsDAO.findByPK(pointVO.getCustomerAccount(), pointVO.getSiteId(), pointVO.getAreaId(), pointVO.getAssetId(), pointVO.getPointLocationId());
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (ReadTimeoutException | UnavailableException e) {
                LOGGER.error(e.getMessage(), e);
                return "{\"outcome\":\"Error: Failed to validate ApAlByPoint.\"}";
            }

            if (pointVO.getApAlSetVOs() != null && !pointVO.getApAlSetVOs().isEmpty()) {
                apAlByPoints = DelegateUtil.getApAlByPointList(pointVO);
            }

            // Insert the entities into Cassandra
            try {
                if (apAlByPoints != null && !apAlByPoints.isEmpty()) {
                    if ((errorMsg = ServiceUtil.validateObjects(apAlByPoints, Boolean.TRUE)) == null) {
                        apAlByPointDAO.create(apAlByPoints);
                        
                        if (pointLocationsList != null && !pointLocationsList.isEmpty()) {
                            if (pointVO.getPointType() == null || pointVO.getPointType().length() == 0) {
                                if ((pointsList = ConfigMapperImpl.getInstance().pointsDAO().findByPK(pointVO.getCustomerAccount(), pointVO.getSiteId(), pointVO.getAreaId(), pointVO.getAssetId(), pointVO.getPointLocationId(), pointVO.getPointId(), pointVO.getApSetId())) != null && !pointsList.isEmpty()) {
                                    pointVO.setPointType(pointsList.get(0).getPointType());
                                    pointVO.setSensorChannelNum(pointsList.get(0).getSensorChannelNum());
                                    System.out.println("set PointType -" + pointVO.getPointType());
                                }
                            }
                            if (!pointVO.isDisabled()) {
                                JsonUtil.createAndSendDeleteMessageToKafka(pointLocationsList.get(0).getDeviceSerialNumber(), pointVO.getPointType(), pointVO.getSensorChannelNum());
                            }
                        }
                        
                    } else {
                        return errorMsg;
                    }
                }

                return "{\"outcome\":\"New ApAlByPoints created successfully.\"}";
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }
        return "{\"outcome\":\"Error: Failed to create new ApAlByPoints.\"}";
    }

}
