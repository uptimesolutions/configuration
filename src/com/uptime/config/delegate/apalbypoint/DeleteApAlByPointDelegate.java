/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.apalbypoint;

import com.uptime.cassandra.config.dao.ApAlByPointDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.vo.ApAlSetVO;
import com.uptime.config.vo.PointVO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class DeleteApAlByPointDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteApAlByPointDelegate.class.getName());
    private final ApAlByPointDAO apAlByPointDAO;

    /**
     * Constructor
     */
    public DeleteApAlByPointDelegate() {
        apAlByPointDAO = ConfigMapperImpl.getInstance().apAlByPointDAO();
    }

    /**
     * Delete Rows from Cassandra with containing the given values
     *
     * @param pointVO, PointVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deletePoints(PointVO pointVO) throws IllegalArgumentException {
        List<ApAlByPoint> dbApAlByPointList;
        ApAlByPoint dbApAlByPoint = null;

        // Find the entities based on the given values
        try {
            if (pointVO != null && pointVO.getApAlSetVOs() != null && !pointVO.getApAlSetVOs().isEmpty()) {
                for (ApAlSetVO apAlSetVO : pointVO.getApAlSetVOs()) {
                    dbApAlByPointList = apAlByPointDAO.findByPK(pointVO.getCustomerAccount(), pointVO.getSiteId(), pointVO.getAreaId(), pointVO.getAssetId(), pointVO.getPointLocationId(), pointVO.getPointId(), pointVO.getApSetId(), pointVO.getAlSetId(), apAlSetVO.getParamName());
                    if (dbApAlByPointList != null && !dbApAlByPointList.isEmpty()) {
                        dbApAlByPoint = dbApAlByPointList.get(0);
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (dbApAlByPoint != null) {
                apAlByPointDAO.delete(dbApAlByPoint);
                return "{\"outcome\":\"Deleted ApAlByPoints successfully.\"}";
            } else {
                return "{\"outcome\":\"No ApAlByPoints found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete ApAlByPoints.\"}";
    }
}
