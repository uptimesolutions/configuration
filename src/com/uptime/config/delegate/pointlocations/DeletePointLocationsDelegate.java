/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.pointlocations;

import com.uptime.cassandra.config.dao.ApAlByPointDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.PointLocationsDAO;
import com.uptime.cassandra.config.dao.PointsDAO;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.cassandra.config.entity.Points;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.vo.PointLocationVO;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class DeletePointLocationsDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeletePointLocationsDelegate.class.getName());
    private final PointLocationsDAO pointLocationsDAO;
    private final PointsDAO pointsDAO;
    private final ApAlByPointDAO apAlByPointDAO;

    /**
     * Constructor
     */
    public DeletePointLocationsDelegate() {
        pointLocationsDAO = ConfigMapperImpl.getInstance().pointLocationsDAO();
        pointsDAO = ConfigMapperImpl.getInstance().pointsDAO();
        apAlByPointDAO = ConfigMapperImpl.getInstance().apAlByPointDAO();
    }

    /**
     * Delete Rows from Cassandra with containing the given values
     *
     * @param pointLocationVO, PointLocationVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deletePointLocations(PointLocationVO pointLocationVO) throws IllegalArgumentException {
        List<PointLocations> pointLocationsList;
        PointLocations pointLocations = null;
        List<Points> pointsList = null;
        List<ApAlByPoint> apAlByPointList = new ArrayList(), tmpList;

        // Find the entities based on the given values
        try {
            if (pointLocationVO != null && (pointLocationsList = pointLocationsDAO.findByPK(pointLocationVO.getCustomerAccount(), pointLocationVO.getSiteId(), pointLocationVO.getAreaId(), pointLocationVO.getAssetId(), pointLocationVO.getPointLocationId())) != null && !pointLocationsList.isEmpty()) {
                pointLocations = pointLocationsList.get(0);
                if ((pointsList = pointsDAO.findByCustomerSiteAreaAssetLocation(pointLocations.getCustomerAccount(), pointLocations.getSiteId(), pointLocations.getAreaId(), pointLocations.getAssetId(), pointLocations.getPointLocationId())) != null && !pointsList.isEmpty()) {
                    for (Points points : pointsList) {
                        if ((tmpList = apAlByPointDAO.findByCustomerSiteAreaAssetLocationPointApSet(points.getCustomerAccount(), points.getSiteId(), points.getAreaId(), points.getAssetId(), points.getPointLocationId(), points.getPointId(), points.getApSetId())) != null && !tmpList.isEmpty()) {
                            apAlByPointList.addAll(tmpList);
                        }
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (pointLocationVO.getRequest() != null && pointLocationVO.getRequest().equals("deleteAllPoints")) {
                //delete only points from pointlocation.
                if (pointsList != null && !pointsList.isEmpty()) {
                    pointsDAO.delete(pointsList);
                }
                if (!apAlByPointList.isEmpty()) {
                    apAlByPointDAO.delete(apAlByPointList);
                }
                return "{\"outcome\":\"Deleted Points items from PointLocation successfully.\"}";
            } else {
                if (pointLocations != null) {
                    pointLocationsDAO.delete(pointLocations);
                    if (pointsList != null && !pointsList.isEmpty()) {
                        pointsDAO.delete(pointsList);
                    }
                    if (!apAlByPointList.isEmpty()) {
                        apAlByPointDAO.delete(apAlByPointList);
                    }
                    return "{\"outcome\":\"Deleted Point Locations items successfully.\"}";
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to delete Point Locations items.\"}";
        }
        return "{\"outcome\":\"No Point Locations items found to delete.\"}";
    }
}
