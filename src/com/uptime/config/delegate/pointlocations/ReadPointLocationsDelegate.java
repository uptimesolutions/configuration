/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.pointlocations;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.PointLocationsDAO;
import com.uptime.cassandra.config.dao.PointsDAO;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.cassandra.config.entity.Points;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
public class ReadPointLocationsDelegate {
    private final PointLocationsDAO pointLocationsDAO;
    private final PointsDAO pointsDAO;

    /**
     * Constructor
     */
    public ReadPointLocationsDelegate() {
        pointLocationsDAO = ConfigMapperImpl.getInstance().pointLocationsDAO();
        pointsDAO = ConfigMapperImpl.getInstance().pointsDAO();
    }

    /**
     * Return a List of PointLocations Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return List of PointLocations Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<PointLocations> getPointLocationsByCustomerSiteArea(String customerAccount, UUID siteId, UUID areaId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        return pointLocationsDAO.findByCustomerSiteArea(customerAccount, siteId, areaId);
    }

    /**
     * Return a List of PointLocations Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return List of PointLocations Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<PointLocations> getPointLocationsByCustomerSiteAreaAsset(String customerAccount, UUID siteId, UUID areaId, UUID assetId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        return pointLocationsDAO.findByCustomerSiteAreaAsset(customerAccount, siteId, areaId, assetId);
    }

    /**
     * Return a List of PointLocations Objects by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @return List of PointLocations Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<PointLocations> getPointLocationsByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId) throws UnavailableException, IllegalArgumentException, ReadTimeoutException {
        return pointLocationsDAO.findByPK(customerAccount, siteId, areaId, assetId, pointLocationId);
    }

    /**
     * Validate Al_set_id for the given pointLocation
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @return String Object
     */
    public String validateAlSetforPoints(String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId) {
        List<Points> pointsList = pointsDAO.findByCustomerSiteAreaAssetLocation(customer, siteId, areaId, assetId, pointLocationId);
        if (!pointsList.isEmpty()) {
            for (Points point : pointsList) {
                if (point.getAlSetId() == null) {
                    return "false";
                }
            }
        }
        return "true";
    }

}
