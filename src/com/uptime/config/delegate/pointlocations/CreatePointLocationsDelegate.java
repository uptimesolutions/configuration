/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.pointlocations;

import com.uptime.cassandra.config.dao.ApAlByPointDAO;
import com.uptime.cassandra.config.dao.PointLocationsDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.PointsDAO;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.cassandra.config.entity.Points;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.dao.SiteBasestationDeviceDAO;
import com.uptime.cassandra.monitor.dao.SiteDeviceBasestationDAO;
import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import com.uptime.cassandra.monitor.entity.SiteDeviceBasestation;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.utils.DelegateUtil;
import com.uptime.config.vo.PointLocationVO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class CreatePointLocationsDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreatePointLocationsDelegate.class.getName());
    private final PointLocationsDAO pointLocationsDAO;
    private final PointsDAO pointsDAO;
    private final ApAlByPointDAO apAlByPointDAO;
    private final SiteBasestationDeviceDAO siteBasestationDeviceDAO;
    private final SiteDeviceBasestationDAO siteDeviceBasestationDAO;
    
    /**
     * Constructor
     */
    public CreatePointLocationsDelegate() {
        pointLocationsDAO = ConfigMapperImpl.getInstance().pointLocationsDAO();
        pointsDAO = ConfigMapperImpl.getInstance().pointsDAO();
        apAlByPointDAO = ConfigMapperImpl.getInstance().apAlByPointDAO();
        siteBasestationDeviceDAO = BaseStationMapperImpl.getInstance().siteBasestationDeviceDAO();
        siteDeviceBasestationDAO = BaseStationMapperImpl.getInstance().siteDeviceBasestationDAO();
    }

    /**
     * Insert new Rows into Cassandra based on the given object
     * @param pointLocationVO, PointLocationVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String createPointLocations(PointLocationVO pointLocationVO) throws IllegalArgumentException, Exception {
        PointLocations pointLocations;
        List<Points> pointsList;
        List<ApAlByPoint> apAlByPointList;
        List<SiteBasestationDevice> siteBasestationDeviceList;
        List<SiteDeviceBasestation> siteDeviceBasestationList;
        SiteBasestationDevice siteBasestationDevice;
        ApAlByPoint baseApAlByPoint;
        String errorMsg;
        
        if (pointLocationVO.getPointLocationId() == null) {
            pointLocationVO.setPointLocationId(UUID.randomUUID());
        }
        
        pointLocations = DelegateUtil.getPointLocations(pointLocationVO);
        pointsList = DelegateUtil.getPointsList(pointLocationVO);
        apAlByPointList = DelegateUtil.getApAlByPointList(pointLocationVO);
        
        siteBasestationDeviceList = new ArrayList();
        siteDeviceBasestationList = new ArrayList();
        for (Points points : pointsList) {
            if (points.getPointLocationId() == pointLocations.getPointLocationId()) {
                baseApAlByPoint = null;
                for (ApAlByPoint apAlByPoint : apAlByPointList) {
                    if (apAlByPoint.getPointId() == points.getPointId()) {
                        baseApAlByPoint = apAlByPoint;
                        break;
                    }
                }
                siteBasestationDevice = DelegateUtil.getSiteBasestationDevice(pointLocations, points, baseApAlByPoint);
                siteBasestationDeviceList.add(siteBasestationDevice);
                siteDeviceBasestationList.add(DelegateUtil.getSiteDeviceBasestation(siteBasestationDevice));
            }
        }

        // Insert the entities into Cassandra
        try { 
            if (((errorMsg = ServiceUtil.validateObjectData(pointLocations)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(pointsList, Boolean.TRUE)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(apAlByPointList, Boolean.TRUE)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(siteBasestationDeviceList, Boolean.TRUE)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(siteDeviceBasestationList, Boolean.TRUE)) == null)) {
                pointLocationsDAO.create(pointLocations);
                if (!pointsList.isEmpty()) {
                    pointsDAO.create(pointsList);
                }
                if (!apAlByPointList.isEmpty()) {
                    apAlByPointDAO.create(apAlByPointList);
                }
                if (!siteBasestationDeviceList.isEmpty()) {
                    siteBasestationDeviceDAO.create(siteBasestationDeviceList);
                }
                if (!siteDeviceBasestationList.isEmpty()) {
                    siteDeviceBasestationList.forEach(siteDeviceBasestation -> siteDeviceBasestationDAO.create(siteDeviceBasestation));
                }
                return "{\"outcome\":\"New Point Location created successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception exp) {
            LOGGER.error(exp.getMessage(), exp);
            sendEvent(exp.getStackTrace());
        }    
        return "{\"outcome\":\"Error: Failed to create new Point Location.\"}";
    }
    
}
