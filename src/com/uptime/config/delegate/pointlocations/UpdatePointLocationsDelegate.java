/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.pointlocations;

import com.uptime.cassandra.config.dao.ApAlByPointDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.PointLocationsDAO;
import com.uptime.cassandra.config.dao.PointsDAO;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.cassandra.config.entity.Points;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.dao.SiteBasestationDeviceDAO;
import com.uptime.cassandra.monitor.dao.SiteDeviceBasestationDAO;
import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import com.uptime.cassandra.monitor.entity.SiteDeviceBasestation;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.utils.DelegateUtil;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.PointLocationVO;
import com.uptime.config.vo.PointVO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class UpdatePointLocationsDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdatePointLocationsDelegate.class.getName());
    private final PointLocationsDAO pointLocationsDAO;
    private final PointsDAO pointsDAO;
    private final ApAlByPointDAO apAlByPointDAO;
    
    private final SiteBasestationDeviceDAO siteBasestationDeviceDAO;
    private final SiteDeviceBasestationDAO siteDeviceBasestationDAO;

    
    /**
     * Constructor
     */
    public UpdatePointLocationsDelegate() {
        pointLocationsDAO = ConfigMapperImpl.getInstance().pointLocationsDAO();
        pointsDAO = ConfigMapperImpl.getInstance().pointsDAO();
        apAlByPointDAO = ConfigMapperImpl.getInstance().apAlByPointDAO();
        siteBasestationDeviceDAO = BaseStationMapperImpl.getInstance().siteBasestationDeviceDAO();
        siteDeviceBasestationDAO = BaseStationMapperImpl.getInstance().siteDeviceBasestationDAO();
    }

    /**
     * Update Rows in Cassandra based on the given object
     * @param pointLocationVO, PointLocationVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String updatePointLocation(PointLocationVO pointLocationVO) throws IllegalArgumentException, Exception {
        List<PointLocations> pointLocationsList;
        List<Points> tmpPointsList, originalPointsList = null, newPointsList = null;
        List<ApAlByPoint> tmpApAlByPointList, originalApAlByPointList = null, newApAlByPointList = null;
        List<SiteBasestationDevice> origSiteBasestationDeviceList = null, newSiteBasestationDeviceList = null, siteBasestationDeviceList;
        List<SiteDeviceBasestation> siteDeviceBasestationList;
        
        PointLocations  originalPointLocations = null, newPointLocations = null;
        SiteDeviceBasestation origSiteDeviceBasestation, newSiteDeviceBasestation = null;
        SiteBasestationDevice newSiteBasestationDevice;
        
        boolean sendMsg, updateSiteBasestationDevice;
        String errorMsg;
        
        // Find the entities based on the given values
        try {
            if (pointLocationVO != null) {
                if (pointLocationVO.getPointLocationId() != null && (pointLocationsList = pointLocationsDAO.findByPK(pointLocationVO.getCustomerAccount(), pointLocationVO.getSiteId(), pointLocationVO.getAreaId(), pointLocationVO.getAssetId(), pointLocationVO.getPointLocationId())) != null && !pointLocationsList.isEmpty()){
                    originalPointLocations = pointLocationsList.get(0);
                    originalPointsList = new ArrayList();
                    originalApAlByPointList = new ArrayList();
                    
                    if(pointLocationVO.getPointList() != null && !pointLocationVO.getPointList().isEmpty()) {
                        for (PointVO pointVO : pointLocationVO.getPointList()) {
                            if ((tmpPointsList = pointsDAO.findByPK(pointVO.getCustomerAccount(), pointVO.getSiteId(), pointVO. getAreaId(), pointVO.getAssetId(), pointVO.getPointLocationId(), pointVO.getPointId(), pointVO.getApSetId())) != null && !tmpPointsList.isEmpty()) {
                                originalPointsList.addAll(tmpPointsList);
                            }
                        
                            if ((tmpApAlByPointList = apAlByPointDAO.findByCustomerSiteAreaAssetLocationPointApSetAlSet(pointVO.getCustomerAccount(), pointVO.getSiteId(), pointVO.getAreaId(), pointVO.getAssetId(), pointVO.getPointLocationId(), pointVO.getPointId(), pointVO.getApSetId(), pointVO.getAlSetId())) != null && !tmpApAlByPointList.isEmpty()) { 
                                originalApAlByPointList.addAll(tmpApAlByPointList);
                            }
                        }
                    } else {
                        if ((tmpPointsList = pointsDAO.findByCustomerSiteAreaAssetLocation(pointLocationVO.getCustomerAccount(), pointLocationVO.getSiteId(), pointLocationVO. getAreaId(), pointLocationVO.getAssetId(), pointLocationVO.getPointLocationId())) != null && !tmpPointsList.isEmpty()) {
                            originalPointsList.addAll(tmpPointsList);
                        }
                        
                        for (Points points : originalPointsList) {
                            if ((tmpApAlByPointList = apAlByPointDAO.findByCustomerSiteAreaAssetLocationPointApSetAlSet(points.getCustomerAccount(), points.getSiteId(), points.getAreaId(), points.getAssetId(), points.getPointLocationId(), points.getPointId(), points.getApSetId(), points.getAlSetId())) != null && !tmpApAlByPointList.isEmpty()) { 
                                originalApAlByPointList.addAll(tmpApAlByPointList);
                            }
                        }
                    }
                }
                
                if (originalPointLocations != null) {
                    newPointLocations = DelegateUtil.getPointLocations(pointLocationVO);
                    if (originalPointsList != null && !originalPointsList.isEmpty()) {
                        newPointsList = DelegateUtil.getPointsList(originalPointsList, pointLocationVO);
                    }
//                    if (originalApAlByPointList != null && !originalApAlByPointList.isEmpty()) {
//                        newApAlByPointList = DelegateUtil.getApAlByPointList(pointLocationVO);
//                    }
                    }
                }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to update.\"}";
        }
        
        // Insert updated entities into Cassandra if original entities are found
        if(newPointLocations != null) {
            try { 
                if (((errorMsg = ServiceUtil.validateObjectData(newPointLocations)) == null) &&
                        ((errorMsg = ServiceUtil.validateObjects(newPointsList, Boolean.TRUE)) == null)) { 
//                       && ((errorMsg = ServiceUtil.validateObjects(newApAlByPointList, Boolean.TRUE)) == null)) {
                    sendMsg = false;
                    updateSiteBasestationDevice = false;

                    // Check if point_locations.basestation_port_num.
//                    if (originalPointLocations != null && (originalPointLocations.getBasestationPortNum() != newPointLocations.getBasestationPortNum())) {
//                        updateSiteBasestationDevice  = true;
//                    }
                    
                    // Check if point_locations is not null.
                    if (originalPointLocations != null) {
                        updateSiteBasestationDevice  = true;
                    }

                    // Check if point_locations.basestation_port_num, point_locations.sample_interval, point_locations.alarm_enabled,
                    // point_locations.speed_ratio, point_locations.roll_diameter, or point_locations.roll_diameter_units
                    // have modified for a given point_location. 
                    if (updateSiteBasestationDevice || (originalPointLocations != null && (originalPointLocations.getSampleInterval() != newPointLocations.getSampleInterval() ||
                            originalPointLocations.isAlarmEnabled() != newPointLocations.isAlarmEnabled() ||
                            originalPointLocations.getSpeedRatio() != newPointLocations.getSpeedRatio() ||
                            originalPointLocations.getRollDiameter() != newPointLocations.getRollDiameter() ||
                            (originalPointLocations.getRollDiameterUnits() != null && newPointLocations.getRollDiameterUnits() == null) ||
                            (originalPointLocations.getRollDiameterUnits() == null && newPointLocations.getRollDiameterUnits() != null) ||
                            (originalPointLocations.getRollDiameterUnits() != null && newPointLocations.getRollDiameterUnits() != null && !originalPointLocations.getRollDiameterUnits().equalsIgnoreCase(newPointLocations.getRollDiameterUnits()))))) {
                        sendMsg = true;
                    }

                    // Check if ap_al_by_point.fmax or ap_al_by_point.resolution are modified for a given point.
                    if ((!sendMsg || !updateSiteBasestationDevice) && originalApAlByPointList != null && !originalApAlByPointList.isEmpty()
                             && newApAlByPointList != null && !newApAlByPointList.isEmpty()) {
                        for (ApAlByPoint oApAlbyPoint : originalApAlByPointList) {
                            for (ApAlByPoint nApAlbyPoint : newApAlByPointList) {
                                if (!sendMsg && oApAlbyPoint.getPointId() == nApAlbyPoint.getPointId()
                                        && (oApAlbyPoint.getfMax() != nApAlbyPoint.getfMax()) 
                                        || oApAlbyPoint.getResolution() != nApAlbyPoint.getResolution()) {
                                    sendMsg = true;
                                    updateSiteBasestationDevice  = true;
                                    break;
                                }
                            }
                        }
                    }

                    // update the site_basestation_device table.
                    if (updateSiteBasestationDevice) {
                        origSiteBasestationDeviceList = new ArrayList();
                        newSiteBasestationDeviceList = new ArrayList();

                        if (originalPointsList != null && originalPointLocations != null && originalPointLocations.getDeviceSerialNumber() != null) {
                            for (Points points : originalPointsList) {
                                if ((siteBasestationDeviceList = siteBasestationDeviceDAO.findByPK(originalPointLocations.getCustomerAccount(), originalPointLocations.getSiteId(), originalPointLocations.getBasestationPortNum(), originalPointLocations.getDeviceSerialNumber(), points.getPointId())) != null && !siteBasestationDeviceList.isEmpty()){
                                    origSiteBasestationDeviceList.addAll(siteBasestationDeviceList);
                                }
                            }
                        }

                        if (!origSiteBasestationDeviceList.isEmpty()) {
                            for (SiteBasestationDevice origSiteBasestationDevice : origSiteBasestationDeviceList) {
                                newSiteBasestationDevice = new SiteBasestationDevice(origSiteBasestationDevice);
                                newSiteBasestationDevice.setBaseStationPort(newPointLocations.getBasestationPortNum());
                                newSiteBasestationDevice.setSampleInterval(newPointLocations.getSampleInterval());
                                if(newApAlByPointList != null && !newApAlByPointList.isEmpty()) {
                                    for (ApAlByPoint nApAlbyPoint : newApAlByPointList) {
                                        if (nApAlbyPoint.getPointId() == newSiteBasestationDevice.getPointId()) {
                                            newSiteBasestationDevice.setFmax(nApAlbyPoint.getfMax());
                                            newSiteBasestationDevice.setResolution(nApAlbyPoint.getResolution());
                                            break;
                                        }
                                    }
                                }
                                newSiteBasestationDeviceList.add(newSiteBasestationDevice);
                            }
                        }

                        if (originalPointLocations != null && originalPointLocations.getDeviceSerialNumber() != null && (siteDeviceBasestationList = siteDeviceBasestationDAO.findByPK(originalPointLocations.getCustomerAccount(), originalPointLocations.getSiteId(), originalPointLocations.getDeviceSerialNumber())) != null && !siteDeviceBasestationList.isEmpty()){
                            origSiteDeviceBasestation = siteDeviceBasestationList.get(0);
                            newSiteDeviceBasestation = new SiteDeviceBasestation(origSiteDeviceBasestation);
                            newSiteDeviceBasestation.setBaseStationPort(newPointLocations.getBasestationPortNum());
                        }
                    }
                    
                    // Update Cassandra
                    if ((newSiteDeviceBasestation == null || (errorMsg = ServiceUtil.validateObjectData(newSiteDeviceBasestation)) == null) &&
                            ((errorMsg = ServiceUtil.validateObjects(newSiteBasestationDeviceList, Boolean.TRUE)) == null)) {
                        pointLocationsDAO.update(newPointLocations);
                        if (newPointsList != null && !newPointsList.isEmpty()) {
                            pointsDAO.update(newPointsList);
                        }
                        if (newApAlByPointList != null && !newApAlByPointList.isEmpty()) {
                            apAlByPointDAO.update(newApAlByPointList);
                        }
                        
                        if (updateSiteBasestationDevice) {
                            if (origSiteBasestationDeviceList != null && !origSiteBasestationDeviceList.isEmpty()) {
                                siteBasestationDeviceDAO.delete(origSiteBasestationDeviceList);
                            }
                            if (newSiteBasestationDeviceList != null && !newSiteBasestationDeviceList.isEmpty()) {
                                siteBasestationDeviceDAO.create(newSiteBasestationDeviceList);
                            }
                            if (newSiteDeviceBasestation != null) {
                                siteDeviceBasestationDAO.update(newSiteDeviceBasestation);
                            }
                        }
                    } else {
                        return errorMsg;
                    }

                    // Send Kafka Message
                    if (sendMsg && newPointLocations.getDeviceSerialNumber() != null && !newPointLocations.getDeviceSerialNumber().isEmpty() 
                            && originalPointsList != null && !originalPointsList.isEmpty()) {

                        for (Points points : originalPointsList) {
                            if (!points.isDisabled()) {
                                JsonUtil.createAndSendDeleteMessageToKafka(newPointLocations.getDeviceSerialNumber(), points.getPointType(), points.getSensorChannelNum());
                            }
                        }
                    }
                    return "{\"outcome\":\"Updated Point Locations items successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            } 
            return "{\"outcome\":\"Error: Failed to update Point Locations items.\"}";
        }
        return "{\"outcome\":\"Point Locations items not found to update.\"}";
    }
    

    /**
     * Update Rows in Cassandra based on the given object
     * @param pointLocationVO, PointLocationVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String updatePointsAlarmEnabledForPointLocation(PointLocationVO pointLocationVO) throws IllegalArgumentException, Exception {
        List<PointLocations> pointLocationsList;
        List<Points> tmpPointsList, originalPointsList = null, newPointsList = null;
        PointLocations  originalPointLocations = null;
        String errorMsg;
        
        // Find the entities based on the given values
        try {
            if (pointLocationVO != null) {
                if (pointLocationVO.getPointLocationId() != null && (pointLocationsList = pointLocationsDAO.findByPK(pointLocationVO.getCustomerAccount(), pointLocationVO.getSiteId(), pointLocationVO.getAreaId(), pointLocationVO.getAssetId(), pointLocationVO.getPointLocationId())) != null && !pointLocationsList.isEmpty()){
                    originalPointLocations = pointLocationsList.get(0);
                    originalPointsList = new ArrayList();
                    if ((tmpPointsList = pointsDAO.findByCustomerSiteAreaAssetLocation(pointLocationVO.getCustomerAccount(), pointLocationVO.getSiteId(), pointLocationVO. getAreaId(), pointLocationVO.getAssetId(), pointLocationVO.getPointLocationId())) != null && !tmpPointsList.isEmpty()) {
                        originalPointsList.addAll(tmpPointsList);
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to update.\"}";
        }
        
        // Insert updated entities into Cassandra if original entities are found
        if(originalPointsList != null  && !originalPointsList.isEmpty() && pointLocationVO != null && pointLocationVO.getSensorList() != null) {
            try { 
                newPointsList = new ArrayList<>();
                for (Points points : originalPointsList) {
                    if (pointLocationVO.getSensorList().contains(points.getSensorType())) {
                        Points newPoints = new Points(points);
                        newPoints.setAlarmEnabled(pointLocationVO.isAlarmEnabled());
                        newPointsList.add(newPoints);
                    }
                }
                if ((errorMsg = ServiceUtil.validateObjects(newPointsList, Boolean.TRUE)) == null) { 
                   
                    // Update Cassandra
                    if (!newPointsList.isEmpty()) {
                        pointsDAO.update(newPointsList);
                    } else {
                        return errorMsg;
                    }

                    // Send Kafka Message
                    if (originalPointLocations != null && originalPointLocations.getDeviceSerialNumber() != null 
                            && !originalPointLocations.getDeviceSerialNumber().isEmpty() && !newPointsList.isEmpty()) {
                        for (Points points : newPointsList) {
                            if (!points.isDisabled()) {
                                JsonUtil.createAndSendDeleteMessageToKafka(originalPointLocations.getDeviceSerialNumber(), points.getPointType(), points.getSensorChannelNum());
                            }
                        }
                    }
                    return "{\"outcome\":\"Updated Point Locations alarm enabled successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            } 
            return "{\"outcome\":\"Error: Failed to update alarm enabled for Point Locations.\"}";
        }
        return "{\"outcome\":\"Point Locations items not found to update.\"}";
    }
    
}
