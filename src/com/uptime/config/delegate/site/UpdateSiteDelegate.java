/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.site;

import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.SiteContactsDAO;
import com.uptime.cassandra.config.dao.SiteCustomerDAO;
import com.uptime.cassandra.config.dao.SiteRegionCountryDAO;
import com.uptime.cassandra.config.entity.SiteContacts;
import com.uptime.cassandra.config.entity.SiteCustomer;
import com.uptime.cassandra.config.entity.SiteRegionCountry;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.utils.DelegateUtil;
import com.uptime.config.vo.SiteVO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class UpdateSiteDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateSiteDelegate.class.getName());
    private final SiteContactsDAO siteContactsDAO;
    private final SiteCustomerDAO siteCustomerDAO;
    private final SiteRegionCountryDAO siteRegionCountryDAO;

    /**
     * Constructor
     */
    public UpdateSiteDelegate() {
        siteContactsDAO = ConfigMapperImpl.getInstance().siteContactsDAO();
        siteCustomerDAO = ConfigMapperImpl.getInstance().siteCustomerDAO();
        siteRegionCountryDAO = ConfigMapperImpl.getInstance().siteRegionCountryDAO();
    }

    /**
     * Update rows in site_customer, site_region_country and site_contacts tables based on the given object
     *
     * @param siteVO, SiteVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String updateSite(SiteVO siteVO) throws IllegalArgumentException, Exception {
        List<SiteContacts> siteContactsList;
        List<SiteContacts> newSiteContacts, originalSiteContacts = null;
        SiteCustomer newSiteCustomer;
        SiteRegionCountry newSiteRegionCountry;
        String errorMsg;
        
        // Find the SiteContacts entities based on the given values
        try {
            originalSiteContacts = new ArrayList();
            if ((siteContactsList = siteContactsDAO.findByCustomerSiteId(siteVO.getCustomerAccount(), siteVO.getSiteId())) != null && !siteContactsList.isEmpty()) {
                originalSiteContacts.addAll(siteContactsList);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to update.\"}";
        }

        // Insert updated entities into Cassandra 
        newSiteContacts = new ArrayList();

        if (siteVO.getSiteContactList() != null && !siteVO.getSiteContactList().isEmpty()) {
            siteVO.getSiteContactList().forEach(contact -> {
                newSiteContacts.add(DelegateUtil.getSiteContacts(contact));
            });
        }

        newSiteCustomer = DelegateUtil.getSiteCustomer(siteVO);
        newSiteRegionCountry = DelegateUtil.getSiteRegionCountry(siteVO);

        try { 
            if (((errorMsg = ServiceUtil.validateObjectData(newSiteCustomer)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjectData(newSiteRegionCountry)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(newSiteContacts, Boolean.TRUE)) == null)) {

                siteCustomerDAO.update(newSiteCustomer);
                siteRegionCountryDAO.update(newSiteRegionCountry);
                if (!originalSiteContacts.isEmpty()) {
                    siteContactsDAO.delete(originalSiteContacts);
                }
                if (!newSiteContacts.isEmpty()) {
                    siteContactsDAO.create(newSiteContacts);
                }
                return "{\"outcome\":\"Updated Sites items successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to update Site items.\"}";

    }

}
