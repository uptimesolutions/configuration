/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.site;

import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.SiteContactsDAO;
import com.uptime.cassandra.config.dao.SiteCustomerDAO;
import com.uptime.cassandra.config.dao.SiteRegionCountryDAO;
import com.uptime.cassandra.config.entity.SiteContacts;
import com.uptime.cassandra.config.entity.SiteCustomer;
import com.uptime.cassandra.config.entity.SiteRegionCountry;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.vo.SiteContactVO;
import com.uptime.config.vo.SiteVO;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class DeleteSiteDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteSiteDelegate.class.getName());
    private final SiteContactsDAO siteContactsDAO;
    private final SiteCustomerDAO siteCustomerDAO;
    private final SiteRegionCountryDAO siteRegionCountryDAO;

    /**
     * Constructor
     */
    public DeleteSiteDelegate() {
        siteContactsDAO = ConfigMapperImpl.getInstance().siteContactsDAO();
        siteCustomerDAO = ConfigMapperImpl.getInstance().siteCustomerDAO();
        siteRegionCountryDAO = ConfigMapperImpl.getInstance().siteRegionCountryDAO();

    }

    /**
     * Delete R\rows from site_customer, site_region_country and site_contacts tables for the given values
     *
     * @param siteVO, SiteVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteSite(SiteVO siteVO) throws IllegalArgumentException {
        List<SiteContacts> siteContactsList;
        List<SiteCustomer> siteCustomerList;
        List<SiteRegionCountry> siteRegionCountryList;

        List<SiteContacts> siteContacts = null;
        SiteCustomer siteCustomer = null;
        SiteRegionCountry siteRegionCountry = null;

        // Find the entities based on the given values
        try {
            siteContacts = new ArrayList();
            for (SiteContactVO contact : siteVO.getSiteContactList()) {
                contact.setSiteId(siteVO.getSiteId());
                if ((siteContactsList = siteContactsDAO.findByPK(contact.getCustomerAccount(), contact.getSiteId(), contact.getRole())) != null&& !siteContactsList.isEmpty()) {
                    siteContacts.add(siteContactsList.get(0));
                    siteContactsList.clear();
                }
            }
            if ((siteCustomerList = siteCustomerDAO.findByPK(siteVO.getCustomerAccount(), siteVO.getSiteId())) != null && !siteCustomerList.isEmpty()) {
                siteCustomer = siteCustomerList.get(0);
            }
            if ((siteRegionCountryList = siteRegionCountryDAO.findByPK(siteVO.getCustomerAccount(), siteVO.getRegion(), siteVO.getCountry(), siteVO.getSiteId())) != null && !siteRegionCountryList.isEmpty()) {
                siteRegionCountry = siteRegionCountryList.get(0);
            }

        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (siteCustomer != null && siteRegionCountry != null) {
                siteCustomerDAO.delete(siteCustomer);
                siteRegionCountryDAO.delete(siteRegionCountry);
                if (!siteContacts.isEmpty())
                    siteContactsDAO.delete(siteContacts);
                return "{\"outcome\":\"Deleted Site items successfully.\"}";
            } else {
                return "{\"outcome\":\"No Site items found to delete.\"}";
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete Site items.\"}";
    }
}
