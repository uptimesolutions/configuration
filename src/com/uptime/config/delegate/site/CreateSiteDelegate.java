/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.site;

import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.SiteContactsDAO;
import com.uptime.cassandra.config.dao.SiteCustomerDAO;
import com.uptime.cassandra.config.dao.SiteRegionCountryDAO;
import com.uptime.cassandra.config.entity.SiteContacts;
import com.uptime.cassandra.config.entity.SiteCustomer;
import com.uptime.cassandra.config.entity.SiteRegionCountry;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.utils.DelegateUtil;
import com.uptime.config.vo.SiteVO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class CreateSiteDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateSiteDelegate.class.getName());
    private final SiteContactsDAO siteContactsDAO;
    private final SiteCustomerDAO siteCustomerDAO;
    private final SiteRegionCountryDAO siteRegionCountryDAO;

    /**
     * Constructor
     */
    public CreateSiteDelegate() {
        siteContactsDAO = ConfigMapperImpl.getInstance().siteContactsDAO();
        siteCustomerDAO = ConfigMapperImpl.getInstance().siteCustomerDAO();
        siteRegionCountryDAO = ConfigMapperImpl.getInstance().siteRegionCountryDAO();
    }

    /**
     * Insert new rows into site_customer, site_region_country and site_contacts tables based on the given object
     *
     * @param siteVO, siteVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String createSite(SiteVO siteVO) throws IllegalArgumentException, Exception {
        List<SiteContacts> siteContactList;
        SiteCustomer siteCustomer;
        SiteRegionCountry siteRegionCountry;
        String errorMsg;
        
        if (siteVO.getSiteId() == null) {
            siteVO.setSiteId(UUID.randomUUID());
        }
        
        siteContactList = new ArrayList();
        if (siteVO.getSiteContactList() != null && !siteVO.getSiteContactList().isEmpty()) {
            siteVO.getSiteContactList().forEach(sitecontact -> {
                sitecontact.setSiteId(siteVO.getSiteId());
                siteContactList.add(DelegateUtil.getSiteContacts(sitecontact));
            });
        }
        
        siteCustomer = DelegateUtil.getSiteCustomer(siteVO);
        siteRegionCountry = DelegateUtil.getSiteRegionCountry(siteVO);
        
        // Insert the entities into Cassandra
        try { 
            if (((errorMsg = ServiceUtil.validateObjectData(siteCustomer)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjectData(siteRegionCountry)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(siteContactList, Boolean.TRUE)) == null)) {
                
                siteCustomerDAO.create(siteCustomer);
                siteRegionCountryDAO.create(siteRegionCountry);
                if (!siteContactList.isEmpty()) {
                    siteContactsDAO.create(siteContactList);
                }
                return "{\"outcome\":\"New Site created successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to create new Site.\"}";
    }

}
