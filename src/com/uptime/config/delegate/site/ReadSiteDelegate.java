/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.site;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.SiteContactsDAO;
import com.uptime.cassandra.config.dao.SiteCustomerDAO;
import com.uptime.cassandra.config.dao.SiteRegionCountryDAO;
import com.uptime.cassandra.config.entity.SiteContacts;
import com.uptime.cassandra.config.entity.SiteCustomer;
import com.uptime.cassandra.config.entity.SiteRegionCountry;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class ReadSiteDelegate {
    private final SiteContactsDAO siteContactsDAO;
    private final SiteCustomerDAO siteCustomerDAO;
    private final SiteRegionCountryDAO siteRegionCountryDAO;

    /**
     * Constructor
     */
    public ReadSiteDelegate() {
        siteContactsDAO = ConfigMapperImpl.getInstance().siteContactsDAO();
        siteCustomerDAO = ConfigMapperImpl.getInstance().siteCustomerDAO();
        siteRegionCountryDAO = ConfigMapperImpl.getInstance().siteRegionCountryDAO();
    }
    
    /**
     * Return a List of SiteContacts Objects by the given params
     * @param customer, String Object
     * @param siteId, UUID Object
     * @return List of SiteContacts Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteContacts> getSiteContactsByCustomerSite(String customer, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteContactsDAO.findByCustomerSiteId(customer, siteId);
    } 
    
    /**
     * Return a List of SiteContacts Objects by the given params
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param role, String Object
     * @return List of SiteContacts Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteContacts> getSiteContactsByCustomerSiteRole(String customer, UUID siteId, String role) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteContactsDAO.findByPK(customer, siteId, role);
    } 
    
    /**
     * Return a List of SiteCustomer Objects by the given params
     * @param customer, String Object
     * @return List of SiteCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteCustomer> getSiteCustomerByCustomer(String customer) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteCustomerDAO.findByCustomer(customer);
    }
    
    /**
     * Return a List of SiteCustomer Objects by the given params
     * @param customer, String Object
     * @param siteId, UUID Object
     * @return List of SiteCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteCustomer> getSiteCustomerByCustomerSite(String customer, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteCustomerDAO.findByPK(customer, siteId);
    } 
    
    /**
     * Return a List of SiteRegionCountry Objects by the given params
     * @param customer, String Object
     * @return List of SiteRegionCountry Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteRegionCountry> getSiteRegionCountryByCustomer(String customer) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteRegionCountryDAO.findByCustomer(customer);
    }
    
    /**
     * Return a List of SiteRegionCountry Objects by the given params
     * @param customer, String Object
     * @param region, String Object
     * @return List of SiteRegionCountry Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteRegionCountry> getSiteRegionCountryByCustomerRegion(String customer, String region) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteRegionCountryDAO.findByCustomerRegion(customer, region);
    }
    
    /**
     * Return a List of SiteRegionCountry Objects by the given params
     * @param customer, String Object
     * @param region, String Object
     * @param country, String Object
     * @return List of SiteRegionCountry Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteRegionCountry> getSiteRegionCountryByCustomerRegionCountry(String customer, String region, String country) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteRegionCountryDAO.findByCustomerRegionCountry(customer, region, country);
    }
    
    /**
     * Return a List of SiteRegionCountry Objects by the given params
     * @param customer, String Object
     * @param region, String Object
     * @param country, String Object
     * @param siteId, UUID Object
     * @return List of SiteRegionCountry Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteRegionCountry> getSiteRegionCountryByCustomerRegionCountrySite(String customer, String region, String country, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteRegionCountryDAO.findByPK(customer, region, country, siteId);
    }
       
    
}
