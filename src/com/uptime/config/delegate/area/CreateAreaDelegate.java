/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.area;

import com.uptime.cassandra.config.dao.AreaConfigScheduleDAO;
import com.uptime.cassandra.config.dao.AreaSiteDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.entity.AreaConfigSchedule;
import com.uptime.cassandra.config.entity.AreaSite;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.utils.DelegateUtil;
import com.uptime.config.vo.AreaVO;
import com.uptime.config.vo.AreaConfigScheduleVO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gsingh
 */
public class CreateAreaDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateAreaDelegate.class.getName());
    private final AreaSiteDAO areaSiteDAO;
    private final AreaConfigScheduleDAO areaConfigScheduleDAO;

    /**
     * Constructor
     */
    public CreateAreaDelegate() {
        areaSiteDAO = ConfigMapperImpl.getInstance().areaSiteDAO();
        areaConfigScheduleDAO = ConfigMapperImpl.getInstance().areaConfigScheduleDAO();
    }

    /**
     * Insert new rows into area_site and area_config_schedule tables based on the given object
     *
     * @param areaVO, AreaVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String createAreaSite(AreaVO areaVO) throws IllegalArgumentException, Exception {
        List<AreaConfigSchedule>  areaConfigScheduleList = null;
        AreaSite areaSite;
        UUID schedId;
        String currSchedName;
        String errorMsg;
        
        if (areaVO.getAreaId() == null) {
            areaVO.setAreaId(UUID.randomUUID());
        }
        areaSite = DelegateUtil.getAreaSite(areaVO);
        
        if (areaVO.getScheduleVOList() != null) {
            currSchedName = null;
            schedId = UUID.randomUUID();
            
            areaConfigScheduleList = new ArrayList();
            for (AreaConfigScheduleVO acsVO : areaVO.getScheduleVOList()) {
                if (acsVO.getDayOfWeek() > 0) {
                    if (currSchedName == null) {
                        currSchedName = acsVO.getScheduleName();
                    }
                    
                    if (currSchedName.equals(acsVO.getScheduleName())) {
                        acsVO.setScheduleId(schedId);
                    } else {
                        currSchedName = acsVO.getScheduleName();
                        schedId = UUID.randomUUID();
                        acsVO.setScheduleId(schedId);
                    }
                    
                    acsVO.setAreaId(areaVO.getAreaId());
                    areaConfigScheduleList.add(DelegateUtil.getAreaConfigSchedule(acsVO));
                }
            }
        }
        
        // Insert the entities into Cassandra.
        try { 
            if (((errorMsg = ServiceUtil.validateObjectData(areaSite)) == null) &&
                    (errorMsg = ServiceUtil.validateObjects(areaConfigScheduleList, Boolean.TRUE)) == null) {
                areaSiteDAO.create(areaSite);
                if (areaConfigScheduleList != null && !areaConfigScheduleList.isEmpty()) {
                    areaConfigScheduleDAO.create(areaConfigScheduleList);
                }
                return "{\"outcome\":\"New Area created successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        
        return "{\"outcome\":\"Error: Failed to create new Area.\"}";
    }

    /**
     * Insert new rows into area_config_schedule table based on the given object
     *
     * @param acsVO, AreaConfigScheduleVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String createAreaConfigSchedule(AreaConfigScheduleVO acsVO) throws IllegalArgumentException, Exception {
        AreaConfigSchedule areaConfigSchedule;
        String errorMsg;
        
        if (acsVO.getScheduleId() == null) {
            acsVO.setScheduleId(UUID.randomUUID());
        }
        areaConfigSchedule = DelegateUtil.getAreaConfigSchedule(acsVO);
        
        // Insert the entities into Cassandra
        try {
            if ((errorMsg = ServiceUtil.validateObjectData(areaConfigSchedule)) == null) {
                areaConfigScheduleDAO.create(areaConfigSchedule);
                return "{\"outcome\":\"New AreaConfigSchedule created successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        
        return "{\"outcome\":\"Error: Failed to create new AreaConfigSchedule.\"}";
    }
}
