/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.area;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.dao.AreaConfigScheduleDAO;
import com.uptime.cassandra.config.dao.AreaSiteDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.entity.AreaSite;
import com.uptime.cassandra.config.entity.AreaConfigSchedule;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author gsingh
 */
public class ReadAreaDelegate {
    private final AreaSiteDAO areaSiteDAO;
    private final AreaConfigScheduleDAO areaConfigScheduleDAO;
    
    /**
     * Constructor
     */
    public ReadAreaDelegate() {
        areaSiteDAO = ConfigMapperImpl.getInstance().areaSiteDAO();
        areaConfigScheduleDAO = ConfigMapperImpl.getInstance().areaConfigScheduleDAO();
    }

    /**
     * Return a List of AreaSite Objects by the given params
     * @param customer, String Object
     * @param siteId
     * @return List of AreaSite Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AreaSite> getAreaSiteByCustomerSite(String customer, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return areaSiteDAO.findByCustomerSiteId(customer, siteId);
    }

    /**
     * Return a List of AreaSite Objects by the given params
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return List of AreaSite Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
        public List<AreaSite> getAreaSiteByPK(String customer, UUID siteId, UUID areaId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return areaSiteDAO.findByPK(customer, siteId, areaId);
    }

    /**
     * Return a List of AreaConfigSchedule Objects by the given params
     * @param customer, String Object
     * @param siteId
     * @param areaId
     * @return List of AreaConfigSchedule Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AreaConfigSchedule> getAreaConfigScheduleByCustomerSiteArea(String customer, UUID siteId, UUID areaId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return areaConfigScheduleDAO.findByCustomerSiteIdAreaId(customer, siteId, areaId);
    }

    /**
     * Return a List of AreaConfigSchedule Objects by the given params
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param scheduleId, UUID Object
     * @return List of AreaConfigSchedule Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AreaConfigSchedule> getAreaConfigScheduleByCustomerSiteAreaSchedule(String customer, UUID siteId, UUID areaId, UUID scheduleId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return areaConfigScheduleDAO.findByCustomerSiteIdAreaIdScheduleId(customer, siteId, areaId, scheduleId);
    }
    
    /**
     * Return a List of AreaConfigSchedule Objects by the given params
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param scheduleId, UUID Object
     * @param dayOfWeek, String Object
     * @return List of AreaConfigSchedule Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AreaConfigSchedule> getAreaConfigScheduleByPK(String customer, UUID siteId, UUID areaId, UUID scheduleId, byte dayOfWeek) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return areaConfigScheduleDAO.findByPK(customer, siteId, areaId, scheduleId, dayOfWeek);
    }    

}
