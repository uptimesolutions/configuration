/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.area;

import com.uptime.cassandra.config.dao.AreaConfigScheduleDAO;
import com.uptime.cassandra.config.dao.AreaSiteDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.entity.AreaConfigSchedule;
import com.uptime.cassandra.config.entity.AreaSite;
import com.uptime.cassandra.workorder.dao.OpenWorkOrdersDAO;
import com.uptime.cassandra.workorder.dao.WorkOrdersMapperImpl;
import com.uptime.cassandra.workorder.entity.OpenWorkOrders;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.utils.DelegateUtil;
import com.uptime.config.vo.AreaConfigScheduleVO;
import com.uptime.config.vo.AreaVO;
import com.uptime.services.util.ServiceUtil;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gsingh
 */
public class UpdateAreaDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateAreaDelegate.class.getName());
    private final AreaSiteDAO areaSiteDAO;
    private final AreaConfigScheduleDAO areaConfigScheduleDAO;
    private final OpenWorkOrdersDAO openWorkOrdersDAO;

    /**
     * Constructor
     */
    public UpdateAreaDelegate() {
        areaSiteDAO = ConfigMapperImpl.getInstance().areaSiteDAO();
        areaConfigScheduleDAO = ConfigMapperImpl.getInstance().areaConfigScheduleDAO();
        openWorkOrdersDAO = WorkOrdersMapperImpl.getInstance().openWorkOrdersDAO();
    }

    /**
     * Update Rows in Cassandra based on the given object
     *
     * @param areaVO, AreaVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateAreaSite(AreaVO areaVO) throws IllegalArgumentException{
        List<AreaSite> areaSiteList;
        List<OpenWorkOrders> openWorkOrdersList = null;
        AreaSite newAreaSite, originalAreaSite = null;
        String errorMsg;
        
        // Find the entities based on the given values
        try { 
            if ((areaSiteList = areaSiteDAO.findByPK(areaVO.getCustomerAccount(), areaVO.getSiteId(), areaVO.getAreaId())) != null && !areaSiteList.isEmpty()) {
                originalAreaSite = areaSiteList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to update.\"}";
        }
        
        // Insert updated entities into Cassandra if original entities are found
        if (originalAreaSite != null) {
            try { 
                newAreaSite = DelegateUtil.getAreaSite(areaVO);
                if ((errorMsg = ServiceUtil.validateObjectData(newAreaSite)) == null) {
                    if(!originalAreaSite.getAreaName().equals(areaVO.getAreaName())) {
                        if((openWorkOrdersList = openWorkOrdersDAO.findByCustomerSiteArea(originalAreaSite.getCustomerAccount(), originalAreaSite.getSiteId(), originalAreaSite.getAreaId())) != null && openWorkOrdersList.size() > 0) {
                            openWorkOrdersList.forEach(openWorkOrders -> openWorkOrders.setAreaName(newAreaSite.getAreaName()));
                        }
                    }
                    
                    if ((errorMsg = ServiceUtil.validateObjects(openWorkOrdersList, Boolean.TRUE)) == null) {
                        areaSiteDAO.update(newAreaSite);
                        if(openWorkOrdersList != null && !openWorkOrdersList.isEmpty()) {
                            openWorkOrdersDAO.update(openWorkOrdersList);
                        }
                    } else {
                        return errorMsg;
                    }
                    return "{\"outcome\":\"Updated Area items successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }    
            return "{\"outcome\":\"Error: Failed to update Area items.\"}";
        }
        return "{\"outcome\":\"Area items not found to update.\"}";
    }

    /**
     * Update Rows in Cassandra based on the given object
     *
     * @param acsVO, AreaConfigScheduleVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updateAreaConfigSchedule(AreaConfigScheduleVO acsVO) throws IllegalArgumentException {
        AreaConfigSchedule newAreaConfigSchedule;
        String errorMsg;
        
        // Insert updated entities into Cassandra 
        try {
            newAreaConfigSchedule = DelegateUtil.getAreaConfigSchedule(acsVO);
            
            if ((errorMsg = ServiceUtil.validateObjectData(newAreaConfigSchedule)) == null) {
                areaConfigScheduleDAO.update(newAreaConfigSchedule);
                return "{\"outcome\":\"Updated Area items successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to update Area items.\"}";
    }
}
