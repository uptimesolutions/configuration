/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.area;

import com.uptime.cassandra.config.dao.AreaConfigScheduleDAO;
import com.uptime.cassandra.config.dao.AreaSiteDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.entity.AreaConfigSchedule;
import com.uptime.cassandra.config.entity.AreaSite;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.vo.AreaConfigScheduleVO;
import com.uptime.config.vo.AreaVO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gsingh
 */
public class DeleteAreaDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteAreaDelegate.class.getName());
    private final AreaSiteDAO areaSiteDAO;
    private final AreaConfigScheduleDAO areaConfigScheduleDAO;

    /**
     * Constructor
     */
    public DeleteAreaDelegate() {
        areaSiteDAO = ConfigMapperImpl.getInstance().areaSiteDAO();
        areaConfigScheduleDAO = ConfigMapperImpl.getInstance().areaConfigScheduleDAO();
    }

    /**
     * Delete Rows from Cassandra with containing the given values
     *
     * @param areaVO, AreaVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteAreaSite(AreaVO areaVO) throws IllegalArgumentException {
        List<AreaSite> areaSiteList;
        AreaSite areaSite = null;
        
        // Find the entities based on the given values
        try { 
            if ((areaSiteList = areaSiteDAO.findByPK(areaVO.getCustomerAccount(), areaVO.getSiteId(), areaVO.getAreaId())) != null && !areaSiteList.isEmpty()) {
                areaSite = areaSiteList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to delete.\"}";
        }
        
        // Delete the entities from Cassandra
        try { 
            if (areaSite != null) {
                areaSiteDAO.delete(areaSite);
                return "{\"outcome\":\"Deleted Area items successfully.\"}";
            } else {
                return "{\"outcome\":\"No Area items found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        
        return "{\"outcome\":\"Error: Failed to delete Area items.\"}";
    }

    /**
     * Delete Rows from Cassandra with containing the given values
     *
     * @param acsVO, AreaConfigScheduleVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteAreaConfigSchedule(AreaConfigScheduleVO acsVO) throws IllegalArgumentException {
        List<AreaConfigSchedule> areaConfigScheduleList;
        AreaConfigSchedule areaConfigSchedule = null;
        
        // Find the entities based on the given values
        try { 
            if ((areaConfigScheduleList = areaConfigScheduleDAO.findByPK(acsVO.getCustomerAccount(), acsVO.getSiteId(), acsVO.getAreaId(), acsVO.getScheduleId(), acsVO.getDayOfWeek())) != null && !areaConfigScheduleList.isEmpty()) {
                areaConfigSchedule = areaConfigScheduleList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to delete.\"}";
        }
        
        // Delete the entities from Cassandra
        try { 
            if (areaConfigSchedule != null) {
                areaConfigScheduleDAO.delete(areaConfigSchedule);
                return "{\"outcome\":\"Deleted Area items successfully.\"}";
            } else {
                return "{\"outcome\":\"No Area items found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        
        return "{\"outcome\":\"Error: Failed to delete Area items.\"}";
    }
}
