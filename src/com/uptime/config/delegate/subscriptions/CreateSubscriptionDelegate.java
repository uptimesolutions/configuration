/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.subscriptions;

import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.SubscriptionsDAO;
import com.uptime.cassandra.config.entity.Subscriptions;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.utils.DelegateUtil;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.SubscriptionsVO;
import com.uptime.services.util.ServiceUtil;
import java.util.List;

/**
 *
 * @author madhavi
 */
public class CreateSubscriptionDelegate {
    
    private final SubscriptionsDAO subscriptionsDAO;

    /**
     * Constructor
     */
    public CreateSubscriptionDelegate() {
        subscriptionsDAO = ConfigMapperImpl.getInstance().subscriptionsDAO();
    }

    /**
     * Insert new rows into subscriptions table based on the given object
     *
     * @param subscriptionsVO, SubscriptionsVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String createSubscriptions(SubscriptionsVO subscriptionsVO) throws IllegalArgumentException, Exception {
        
        List<Subscriptions> subscriptionsList;
        Subscriptions subscriptions;
        String errorMsg;
        
        subscriptions = DelegateUtil.getSubscriptions(subscriptionsVO);
        
        // Insert the entities into Cassandra
        try { 
            if (((errorMsg = ServiceUtil.validateObjectData(subscriptions)) == null)){
                
                subscriptionsDAO.create(subscriptions);
                if ((subscriptionsList = subscriptionsDAO.findByCustomerSiteId(subscriptions.getCustomerAccount(), subscriptions.getSiteId())) != null && !subscriptionsList.isEmpty()) {
                    JsonUtil.createAndSendSubscriptionsKafkaMessage(subscriptionsList);
                }
                return "{\"outcome\":\"New Subscriptions created successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to create new Subscriptions.\"}";
    }

}
