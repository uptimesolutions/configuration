/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.subscriptions;

import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.SubscriptionsDAO;
import com.uptime.cassandra.config.entity.Subscriptions;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.SubscriptionsVO;
import java.util.List;

/**
 *
 * @author madhavi
 */
public class DeleteSubscriptionDelegate {
    
    private final SubscriptionsDAO subscriptionsDAO;

    /**
     * Constructor
     */
    public DeleteSubscriptionDelegate() {
        subscriptionsDAO = ConfigMapperImpl.getInstance().subscriptionsDAO();
    }

    /**
     * Delete rows from subscriptions table for the given values
     *
     * @param subscriptionsVO, SubscriptionsVO Object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String deleteSubscription(SubscriptionsVO subscriptionsVO) throws IllegalArgumentException, Exception {
        
        List<Subscriptions> subscriptionsList, updsubscriptionsList;
        Subscriptions subscriptions = null;

        // Find the entities based on the given values
        try {
            if ((subscriptionsList = subscriptionsDAO.findByPK(subscriptionsVO.getCustomerAccount(), subscriptionsVO.getSiteId(), subscriptionsVO.getSubscriptionType())) != null && !subscriptionsList.isEmpty()) {
                subscriptions = subscriptionsList.get(0);
            }

        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find Subscriptions to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (subscriptions != null) {
                subscriptionsDAO.delete(subscriptions);
                if ((updsubscriptionsList = subscriptionsDAO.findByCustomerSiteId(subscriptions.getCustomerAccount(), subscriptions.getSiteId())) != null && !subscriptionsList.isEmpty()) {
                    JsonUtil.createAndSendSubscriptionsKafkaMessage(updsubscriptionsList);
                }return "{\"outcome\":\"Deleted Subscriptions successfully.\"}";
            } else {
                return "{\"outcome\":\"No Subscriptions found to cancel.\"}";
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to cancel Subscriptions.\"}";
    }

    /**
     * Delete rows from subscriptions table for the given values
     *
     * @param subscriptionsVO, SubscriptionsVO Object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String cancelAllSubscription(SubscriptionsVO subscriptionsVO) throws IllegalArgumentException, Exception {
        
        List<Subscriptions> subscriptionsList;
        Subscriptions subscriptions = null;

        // Find the entities based on the given values
        try {
            if ((subscriptionsList = subscriptionsDAO.findByCustomerSiteId(subscriptionsVO.getCustomerAccount(), subscriptionsVO.getSiteId())) != null && !subscriptionsList.isEmpty()) {
                subscriptions = subscriptionsList.get(0);
            }

        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find Subscriptions to cancel.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (subscriptions != null) {
                subscriptionsDAO.deleteByCustomerSite(subscriptions.getCustomerAccount(), subscriptions.getSiteId());
                JsonUtil.createAndSendSubscriptionsKafkaMessage(subscriptionsVO.getCustomerAccount(), subscriptionsVO.getSiteId());
                return "{\"outcome\":\"Subscriptions Cancelled successfully.\"}";
            } else {
                return "{\"outcome\":\"No Subscriptions found to cancel.\"}";
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to cancel Subscriptions.\"}";
    }


}
