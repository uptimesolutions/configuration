/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.subscriptions;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.SubscriptionsDAO;
import com.uptime.cassandra.config.entity.Subscriptions;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class ReadSubscriptionsDelegate {
    
    private final SubscriptionsDAO subscriptionsDAO;

    /**
     * Constructor
     */
    public ReadSubscriptionsDelegate() {
        subscriptionsDAO = ConfigMapperImpl.getInstance().subscriptionsDAO();
    }

    /**
     * Return a List of Subscriptions Objects by the given params
     * @param customer, String Object
     * @param siteId, UUID Object
     * @return List of Subscriptions Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<Subscriptions> getSubscriptionsByCustomerSite(String customer, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return subscriptionsDAO.findByCustomerSiteId(customer, siteId);
    } 

    /**
     * Return a List of Subscriptions Objects by the given params
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param subscriptionType, String Object
     
     * @return List of Subscriptions Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<Subscriptions> getSubscriptionsByPK(String customer, UUID siteId, String subscriptionType) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return subscriptionsDAO.findByPK(customer, siteId, subscriptionType);
    }
       
    
}
