/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.subscriptions;

import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.SubscriptionsDAO;
import com.uptime.cassandra.config.entity.Subscriptions;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.utils.DelegateUtil;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.SubscriptionsVO;
import com.uptime.services.util.ServiceUtil;
import java.util.List;

/**
 *
 * @author madhavi
 */
public class UpdateSubscriptionDelegate {
    
     private final SubscriptionsDAO subscriptionsDAO;

    /**
     * Constructor
     */
    public UpdateSubscriptionDelegate() {
        subscriptionsDAO = ConfigMapperImpl.getInstance().subscriptionsDAO();
    }

    /**
     * Update rows in subscriptions table based on the given object
     *
     * @param subscriptionsVO, SubscriptionsVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String updateSite(SubscriptionsVO subscriptionsVO) throws IllegalArgumentException, Exception {
        List<Subscriptions> subscriptionsList;
        Subscriptions newSubscriptions;
        String errorMsg;
        
        // Insert updated entities into Cassandra 
        newSubscriptions = DelegateUtil.getSubscriptions(subscriptionsVO);

        try { 
            if (((errorMsg = ServiceUtil.validateObjectData(newSubscriptions)) == null)) {

                subscriptionsDAO.update(newSubscriptions);
                if ((subscriptionsList = subscriptionsDAO.findByCustomerSiteId(newSubscriptions.getCustomerAccount(), newSubscriptions.getSiteId())) != null && !subscriptionsList.isEmpty()) {
                    JsonUtil.createAndSendSubscriptionsKafkaMessage(subscriptionsList);
                }
                return "{\"outcome\":\"Updated Subscriptions successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to update Subscriptions.\"}";

    }

}
