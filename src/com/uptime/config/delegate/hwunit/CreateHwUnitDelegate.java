/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.hwunit;
import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.dao.ApAlByPointDAO;
import com.uptime.cassandra.config.dao.HwUnitToPointDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.PointLocationsDAO;
import com.uptime.cassandra.config.dao.PointsDAO;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.cassandra.config.entity.HwUnitToPoint;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.cassandra.config.entity.Points;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.dao.SiteBasestationDeviceDAO;
import com.uptime.cassandra.monitor.dao.SiteDeviceBasestationDAO;
import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import com.uptime.cassandra.monitor.entity.SiteDeviceBasestation;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.utils.DelegateUtil;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.HwUnitVO;
import com.uptime.services.util.ServiceUtil;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author madhavi
 */
public class CreateHwUnitDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateHwUnitDelegate.class.getName());
    private final HwUnitToPointDAO hwUnitToPointDAO;
    private final PointsDAO pointsDAO;
    private final ApAlByPointDAO apAlByPointDAO;
    private final PointLocationsDAO pointLocationsDAO;
    private final SiteBasestationDeviceDAO siteBasestationDeviceDAO;
    private final SiteDeviceBasestationDAO siteDeviceBasestationDAO;
    /**
     * Constructor
     */
    public CreateHwUnitDelegate() {
        hwUnitToPointDAO = ConfigMapperImpl.getInstance().hwUnitToPointDAO();
        pointsDAO = ConfigMapperImpl.getInstance().pointsDAO();
        apAlByPointDAO = ConfigMapperImpl.getInstance().apAlByPointDAO();
        pointLocationsDAO = ConfigMapperImpl.getInstance().pointLocationsDAO();
        siteBasestationDeviceDAO = BaseStationMapperImpl.getInstance().siteBasestationDeviceDAO();
        siteDeviceBasestationDAO = BaseStationMapperImpl.getInstance().siteDeviceBasestationDAO();
    }
    /**
     * Insert new Rows into Cassandra based on the given object
     *
     * @param hwUnitVO, HwUnitVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String createHwUnitToPoint(HwUnitVO hwUnitVO) throws IllegalArgumentException {
        List<PointLocations> pointLocationsList;
        HwUnitToPoint hwUnitToPoint;
        Points points;
        List<ApAlByPoint> apAlByPointList;
        String errorMsg;
        SiteBasestationDevice siteBasestationDevice = null;
        SiteDeviceBasestation siteDeviceBasestation = null;
        
        try {
            pointLocationsList = pointLocationsDAO.findByPK(hwUnitVO.getCustomerAccount(), hwUnitVO.getSiteId(), hwUnitVO.getAreaId(), hwUnitVO.getAssetId(), hwUnitVO.getPointLocationId());
        } catch (IllegalArgumentException ex) {
            return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        } catch (ReadTimeoutException | UnavailableException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return "{\"outcome\":\"Error: Failed to validate HwUnitToPoint.\"}";
        }
        hwUnitVO.setPointId(UUID.randomUUID());
        hwUnitToPoint = DelegateUtil.getHwUnitToPoint(hwUnitVO);
        points = DelegateUtil.getPoint(hwUnitVO);
        apAlByPointList = DelegateUtil.getApAlByPointList(hwUnitVO);
        
        if (apAlByPointList != null && !apAlByPointList.isEmpty() && pointLocationsList != null && !pointLocationsList.isEmpty()) {
            siteBasestationDevice = DelegateUtil.getSiteBasestationDevice(pointLocationsList.get(0), points, apAlByPointList.get(0));
            siteDeviceBasestation = DelegateUtil.getSiteDeviceBasestation(siteBasestationDevice);
        }
        
        // Insert the entities into Cassandra
        try {
            if (((errorMsg = ServiceUtil.validateObjectData(points)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjectData(hwUnitToPoint)) == null) &&
                    (siteBasestationDevice == null || (errorMsg = ServiceUtil.validateObjectData(siteBasestationDevice)) == null) &&
                    (siteDeviceBasestation == null || (errorMsg = ServiceUtil.validateObjectData(siteDeviceBasestation)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(apAlByPointList, Boolean.TRUE)) == null)) {
                pointsDAO.create(points);
                apAlByPointDAO.create(apAlByPointList);
                hwUnitToPointDAO.create(hwUnitToPoint);
                if (siteBasestationDevice != null){
                    siteBasestationDeviceDAO.create(siteBasestationDevice);
                }
                if (siteDeviceBasestation != null) {
                    siteDeviceBasestationDAO.create(siteDeviceBasestation);
                }
                if (pointLocationsList != null && !pointLocationsList.isEmpty()) {
                    JsonUtil.createAndSendDeleteMessageToKafka(pointLocationsList.get(0).getDeviceSerialNumber(), hwUnitVO.getChannelType(), hwUnitVO.getChannelNum());
                }
                return "{\"outcome\":\"New HwUnitToPoint created successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to create new HwUnitToPoint.\"}";
    }
}