/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.hwunit;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.HwUnitToPointDAO;
import com.uptime.cassandra.config.entity.HwUnitToPoint;
import java.util.List;

/**
 *
 * @author madhavi
 */
public class ReadHwUnitDelegate {
    private final HwUnitToPointDAO hwUnitToPointDAO;
    
    /**
     * Constructor
     */
    public ReadHwUnitDelegate() {
        hwUnitToPointDAO = ConfigMapperImpl.getInstance().hwUnitToPointDAO();
    }

    /**
     * Return a List of HwUnitToPoint Objects by the given params
     * @param deviceId, String Object
     * @return List of HwUnitToPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<HwUnitToPoint> getHwUnitToPointByDevice(String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return hwUnitToPointDAO.findByDevice(deviceId);
    }
    
    /**
     * Return a List of HwUnitToPoint Objects by the given params
     * @param deviceId, String Object
     * @param channelType, String Object
     * @param channelNum, byte
     * @return List of HwUnitToPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<HwUnitToPoint> getHwUnitToPointByPK(String deviceId, String channelType, byte channelNum) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return hwUnitToPointDAO.findByPK(deviceId, channelType, channelNum);
    }
}
