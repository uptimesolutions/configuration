/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.device;

import com.uptime.cassandra.config.dao.ApAlByPointDAO;
import com.uptime.cassandra.config.dao.AreaSiteDAO;
import com.uptime.cassandra.config.dao.AssetSiteAreaDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.HwUnitToPointDAO;
import com.uptime.cassandra.config.dao.PointLocationsDAO;
import com.uptime.cassandra.config.dao.PointToHwUnitDAO;
import com.uptime.cassandra.config.dao.PointsDAO;
import com.uptime.cassandra.config.dao.SiteCustomerDAO;
import com.uptime.cassandra.config.dao.SubscriptionsDAO;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.cassandra.config.entity.AreaSite;
import com.uptime.cassandra.config.entity.AssetSiteArea;
import com.uptime.cassandra.config.entity.HwUnitToPoint;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.cassandra.config.entity.PointToHwUnit;
import com.uptime.cassandra.config.entity.Points;
import com.uptime.cassandra.config.entity.SiteCustomer;
import com.uptime.cassandra.config.entity.Subscriptions;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.dao.SiteBasestationDeviceDAO;
import com.uptime.cassandra.monitor.dao.SiteDeviceBasestationDAO;
import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import com.uptime.cassandra.monitor.entity.SiteDeviceBasestation;
import com.uptime.cassandra.presets.dao.GlobalApAlSetsDAO;
import com.uptime.cassandra.presets.dao.GlobalPtLocationNamesDAO;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteApAlSetsDAO;
import com.uptime.cassandra.presets.dao.SitePtLocationNamesDAO;
import com.uptime.cassandra.presets.entity.GlobalApAlSets;
import com.uptime.cassandra.presets.entity.GlobalPtLocationNames;
import com.uptime.cassandra.presets.entity.SiteApAlSets;
import com.uptime.cassandra.presets.entity.SitePtLocationNames;
import com.uptime.config.ConfigService;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.dao.DeviceStatusDAO;
import com.uptime.config.utils.DelegateUtil;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.ApAlSetVO;
import com.uptime.config.vo.AssetVO;
import com.uptime.config.vo.DeviceVO;
import com.uptime.config.vo.HwUnitVO;
import com.uptime.config.vo.MQVO;
import com.uptime.config.vo.PointLocationVO;
import com.uptime.config.vo.PointVO;
import com.uptime.config.vo.SiteBasestationDeviceVO;
import com.uptime.config.vo.DeviceStatusVO;
import com.uptime.services.ServiceConstants;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class CreateDeviceDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateDeviceDelegate.class.getName());
    private final SiteBasestationDeviceDAO siteBasestationDeviceDAO;
    private final SiteDeviceBasestationDAO siteDeviceBasestationDAO;
    private final PointToHwUnitDAO pointToHwUnitDAO;
    private final HwUnitToPointDAO hwUnitToPointDAO;
    private final PointLocationsDAO pointLocationsDAO;
    private final GlobalApAlSetsDAO globalApAlSetsDAO;
    private final SiteApAlSetsDAO siteApAlSetsDAO;
    private final PointsDAO pointsDAO;
    private final ApAlByPointDAO apAlByPointDAO;
    private final AssetSiteAreaDAO assetSiteAreaDAO;
    private final GlobalPtLocationNamesDAO globalPtLocationNamesDAO;
    private final SitePtLocationNamesDAO sitePtLocationNamesDAO;
    private final SubscriptionsDAO subscriptionsDAO;
    private final SiteCustomerDAO siteCustomerDAO;
    private final AreaSiteDAO areaSiteDAO;
    private final DeviceStatusDAO deviceStatusDAO;

    public CreateDeviceDelegate() {
        siteBasestationDeviceDAO = BaseStationMapperImpl.getInstance().siteBasestationDeviceDAO();
        hwUnitToPointDAO = ConfigMapperImpl.getInstance().hwUnitToPointDAO();
        siteDeviceBasestationDAO = BaseStationMapperImpl.getInstance().siteDeviceBasestationDAO();
        pointsDAO = ConfigMapperImpl.getInstance().pointsDAO();
        apAlByPointDAO = ConfigMapperImpl.getInstance().apAlByPointDAO();
        pointLocationsDAO = ConfigMapperImpl.getInstance().pointLocationsDAO();
        pointToHwUnitDAO = ConfigMapperImpl.getInstance().pointToHwUnitDAO();
        globalApAlSetsDAO = PresetsMapperImpl.getInstance().globalApAlSetsDAO();
        siteApAlSetsDAO = PresetsMapperImpl.getInstance().siteApAlSetsDAO();
        assetSiteAreaDAO = ConfigMapperImpl.getInstance().assetSiteAreaDAO();
        globalPtLocationNamesDAO = PresetsMapperImpl.getInstance().globalPtLocationNamesDAO();
        sitePtLocationNamesDAO = PresetsMapperImpl.getInstance().sitePtLocationNamesDAO();
        subscriptionsDAO = ConfigMapperImpl.getInstance().subscriptionsDAO();
        siteCustomerDAO = ConfigMapperImpl.getInstance().siteCustomerDAO();
        areaSiteDAO = ConfigMapperImpl.getInstance().areaSiteDAO();
        deviceStatusDAO = new DeviceStatusDAO();
    }

    /**
     * Insert new Rows into Cassandra based on the given object
     *
     * @param hwUnitVO, HwUnitVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String createHwUnitToPoint(HwUnitVO hwUnitVO) throws IllegalArgumentException, Exception {
        HwUnitToPoint hwUnitToPoint;
        String errorMsg;
        
        if (hwUnitVO != null) {
            if (hwUnitVO.getPointId() == null) {
                hwUnitVO.setPointId(UUID.randomUUID());
            }
            hwUnitToPoint = DelegateUtil.getHwUnitToPoint(hwUnitVO);

            // Insert the entities into Cassandra
            try {
                if ((errorMsg = ServiceUtil.validateObjectData(hwUnitToPoint)) == null) {
                    hwUnitToPointDAO.create(hwUnitToPoint);
                } else {
                    return errorMsg;
                }
                return "{\"outcome\":\"New HwUnitToPoint created successfully.\"}";
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }

        return "{\"outcome\":\"Error: Failed to create new HwUnitToPoint.\"}";
    }
    
    /**
     * Insert new Rows into Cassandra based on the given object
     *
     * @param siteApAlSets, SiteApAlSets object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String createSiteApAlSet(SiteApAlSets siteApAlSets) throws IllegalArgumentException, Exception {
        String errorMsg;
        
        // Insert the entities into Cassandra
        try {
            if ((errorMsg = ServiceUtil.validateObjectData(siteApAlSets)) == null) {
                siteApAlSetsDAO.create(siteApAlSets);
            } else {
                return errorMsg;
            }
            return "{\"outcome\":\"New SiteApAlSets created successfully.\"}";
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }

        return "{\"outcome\":\"Error: Failed to create new SiteApAlSets.\"}";
    }
    
    /**
     * Insert Rows in Cassandra based on the given object
     *
     * @param siteBasestationDeviceVO, SiteBasestationDeviceVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String createSiteBasestationDevice(SiteBasestationDeviceVO siteBasestationDeviceVO) throws IllegalArgumentException, Exception {
        SiteBasestationDevice newSiteBasestationDevice;
        String errorMsg;
        
        newSiteBasestationDevice = DelegateUtil.getSiteBasestationDevice(siteBasestationDeviceVO);
        
        // Insert the entities into Cassandra
        try {
            if ((errorMsg = ServiceUtil.validateObjectData(newSiteBasestationDevice)) == null) {
                siteBasestationDeviceDAO.create(newSiteBasestationDevice);
            } else {
                return errorMsg;
            }
            return "{\"outcome\":\"Created / updated SiteBasestationDevice successfully.\"}";
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to create SiteBasestationDevice.\"}";
    }

    /**
     * Insert Rows in Cassandra based on the given object
     *
     * @param deviceVO, DeviceVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String installDeviceWithNewPoints(DeviceVO deviceVO) throws IllegalArgumentException {
        SiteBasestationDevice siteBasestationDevice;
        SiteDeviceBasestation siteDeviceBasestation;
        List<PointLocations> pointLocationsList;
        List<ApAlByPoint> apAlByPointList;
        List<SiteApAlSets> siteApAlSetsList;
        List<GlobalApAlSets> globalApAlSetsList;
        List<Subscriptions> subscriptionsList;
        List<SiteCustomer> siteList;
        List<AreaSite> areaList;
        List<AssetSiteArea> assetList;
        PointLocations pointLocations = null;
        boolean updatePointLocation = true;
        PointToHwUnit pointToHwUnit;
        HwUnitToPoint hwUnitToPoint;
        DeviceStatusVO deviceStatusVO;
        Points points;
        String errorMsg;
        MQVO mqVO;
        
        if (deviceVO != null && deviceVO.getPointList() != null && !deviceVO.getPointList().isEmpty() && deviceVO.getNewDeviceId() != null) {
            
            // Find original pointLocations and info for kafka messages from DB
            try {
                if ((pointLocationsList = pointLocationsDAO.findByPK(deviceVO.getCustomerAccount(), deviceVO.getSiteId(), deviceVO.getAreaId(), deviceVO.getAssetId(), deviceVO.getPointLocationId())) != null && !pointLocationsList.isEmpty()) {
                    pointLocations = pointLocationsList.get(0);
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
                return "{\"outcome\":\"Error: Failed to find items to install device.\"}";
            }
            
            if (pointLocations != null) {
                
                // Update pointLocations entity
                pointLocations.setDeviceSerialNumber(deviceVO.getNewDeviceId());
                
                for (PointVO pointVO : deviceVO.getPointList()) {
                    
                    // Update pointVO
                    pointVO.setSiteName(deviceVO.getSiteName());
                    pointVO.setAreaName(deviceVO.getAreaName());                                                                                 
                    pointVO.setAssetName(deviceVO.getAssetName());
                    pointVO.setPointLocationName(pointLocations.getPointLocationName());
                    if (pointVO.getPointId() == null) {
                        pointVO.setPointId(UUID.randomUUID());
                    }
                    
                    // Set apAlSetVOs
                    try {
                        pointVO.setApAlSetVOs(new ArrayList());
                        if ((globalApAlSetsList = globalApAlSetsDAO.findByCustomerApSetAlSet(ServiceConstants.DEFAULT_UPTIME_ACCOUNT, pointVO.getApSetId(), pointVO.getAlSetId())) != null && !globalApAlSetsList.isEmpty()) {
                            for (GlobalApAlSets globalApAlSets : globalApAlSetsList) {
                                pointVO.getApAlSetVOs().add(new ApAlSetVO(globalApAlSets, pointVO.getSiteId()));
                            }
                        } else if ((globalApAlSetsList = globalApAlSetsDAO.findByCustomerApSetAlSet(pointVO.getCustomerAccount(), pointVO.getApSetId(), pointVO.getAlSetId())) != null && !globalApAlSetsList.isEmpty()) {
                            for (GlobalApAlSets globalApAlSets : globalApAlSetsList) {
                                pointVO.getApAlSetVOs().add(new ApAlSetVO(globalApAlSets, pointVO.getSiteId()));
                            }
                        } else if ((siteApAlSetsList = siteApAlSetsDAO.findByCustomerSiteApSetAlSet(pointVO.getCustomerAccount(), pointVO.getSiteId(), pointVO.getApSetId(), pointVO.getAlSetId())) != null && !siteApAlSetsList.isEmpty()) {
                            for (SiteApAlSets siteApAlSets : siteApAlSetsList) {
                                pointVO.getApAlSetVOs().add(new ApAlSetVO(siteApAlSets));
                            }
                        }
                    } catch (IllegalArgumentException e) {
                        throw e;
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        sendEvent(e.getStackTrace());
                        return "{\"outcome\":\"Error: Failed to find items to install device.\"}";
                    }
                    
                    // Set Points
                    points = DelegateUtil.getPoints(pointVO);
                    
                    // Set ApAlByPoint List
                    apAlByPointList = DelegateUtil.getApAlByPointList(pointVO);
                    
                    // Set siteBasestationDevice and siteDeviceBasestation
                    siteBasestationDevice = DelegateUtil.getSiteBasestationDevice(pointLocations, points, apAlByPointList.get(0));
                    siteDeviceBasestation = DelegateUtil.getSiteDeviceBasestation(siteBasestationDevice);
                    
                    // Set pointToHwUnit and hwUnitToPoint
                    pointToHwUnit = DelegateUtil.getPointToHwUnit(points, deviceVO.getNewDeviceId());
                    hwUnitToPoint = DelegateUtil.getHwUnitToPoint(points, deviceVO.getNewDeviceId());
                    
                    // Insert the entities into Cassandra
                    try {
                        if (updatePointLocation) {
                            if ((errorMsg = ServiceUtil.validateObjectData(pointLocations)) == null) {
                                pointLocationsDAO.update(pointLocations);
                                updatePointLocation = false;
                            } else {
                                return errorMsg;
                            }
                        }
                        
                        if ((errorMsg = ServiceUtil.validateObjectData(points)) == null &&
                                (errorMsg = ServiceUtil.validateObjectData(siteBasestationDevice)) == null &&
                                (errorMsg = ServiceUtil.validateObjectData(siteDeviceBasestation)) == null &&
                                (errorMsg = ServiceUtil.validateObjects(apAlByPointList, Boolean.FALSE)) == null &&
                                (errorMsg = ServiceUtil.validateObjectData(pointToHwUnit)) == null &&
                                (errorMsg = ServiceUtil.validateObjectData(hwUnitToPoint)) == null) {

                            pointsDAO.create(points);
                            apAlByPointDAO.create(apAlByPointList);
                            siteBasestationDeviceDAO.create(siteBasestationDevice);
                            siteDeviceBasestationDAO.create(siteDeviceBasestation);
                            pointToHwUnitDAO.create(pointToHwUnit);
                            hwUnitToPointDAO.create(hwUnitToPoint);
                            
                            if (!pointVO.isDisabled()) {
                                JsonUtil.createAndSendDeleteMessageToKafka(pointLocations.getDeviceSerialNumber(), pointVO.getPointType(), pointVO.getSensorChannelNum());
                            }
                            
                            siteList = siteCustomerDAO.findByPK(pointLocations.getCustomerAccount(), pointLocations.getSiteId());
                            areaList = areaSiteDAO.findByPK(pointLocations.getCustomerAccount(), pointLocations.getSiteId(), pointLocations.getAreaId());
                            assetList = assetSiteAreaDAO.findByPK(pointLocations.getCustomerAccount(), pointLocations.getSiteId(), pointLocations.getAreaId(), pointLocations.getAssetId());
                                    
                            // Send MQ Message
                            // Fetch Subscriptions 
                            if ((subscriptionsList = subscriptionsDAO.findByPK(pointLocations.getCustomerAccount(), pointLocations.getSiteId(), "DEVICE")) != null && !subscriptionsList.isEmpty() &&
                                    siteList != null && !siteList.isEmpty() &&
                                    areaList != null && !areaList.isEmpty() &&
                                    assetList != null && !assetList.isEmpty()) {
                                
                                // Populate MQ VO with the details to be posted to MQ
                                mqVO = new MQVO();
                                mqVO.setCustomer_id(pointLocations.getCustomerAccount());
                                mqVO.setSite_id(pointLocations.getSiteId().toString());
                                mqVO.setSite_name(siteList.get(0).getSiteName());
                                mqVO.setArea(areaList.get(0).getAreaName());
                                mqVO.setAsset(assetList.get(0).getAssetName());
                                mqVO.setPoint_location(pointLocations.getPointLocationName());
                                mqVO.setDevice_id(pointLocations.getDeviceSerialNumber());
                                mqVO.setDevice_action("add");
                                mqVO.setUse_internal(subscriptionsList.get(0).isInternal());
                                mqVO.setMq_queue_name(subscriptionsList.get(0).getMqQueueName());
                                
                                if (!subscriptionsList.get(0).isInternal()) {
                                    mqVO.setMq_connect_string((subscriptionsList.get(0).getMqConnectString()));
                                    mqVO.setMq_user((subscriptionsList.get(0).getMqUser()));
                                    mqVO.setMq_pwd((subscriptionsList.get(0).getMqPwd()));
                                    mqVO.setMq_client_id((subscriptionsList.get(0).getMqClientId()));
                                }
                                
                                ConfigService.sendMQMessage(JsonConverterUtil.toJSON(mqVO));
                            } 
                            if ((deviceVO.getSiteName() == null || deviceVO.getSiteName().isEmpty())
                                && siteList != null && !siteList.isEmpty())
                                deviceVO.setSiteName(siteList.get(0).getSiteName());

                            if ((deviceVO.getAreaName() == null || deviceVO.getAreaName().isEmpty())
                                    && areaList != null && !areaList.isEmpty())
                                deviceVO.setAreaName(areaList.get(0).getAreaName());

                            if ((deviceVO.getAssetName() == null || deviceVO.getAssetName().isEmpty())
                                    && assetList != null && !assetList.isEmpty())
                                deviceVO.setAssetName(assetList.get(0).getAssetName());
                            deviceVO.setPointLocationName(pointLocations.getPointLocationName());
                            //Update MySQL device_status
                            deviceStatusVO = DelegateUtil.getDeviceStatus(deviceVO, deviceVO.getNewDeviceId());
                            deviceStatusDAO.upsert(deviceStatusVO);
                        } else {
                            return errorMsg;
                        }
                    } catch (IllegalArgumentException e) {
                        throw e;
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        sendEvent(e.getStackTrace());
                        break;
                    }
                }
                return "{\"outcome\":\"Device installed successfully.\"}";
            } else {
                return "{\"outcome\":\"Error: Failed to find items to install device.\"}";
            }
        }
        return "{\"outcome\":\"Error: Failed to install device.\"}";
    }

    /**
     * Insert Rows in Cassandra based on the given object
     *
     * @param deviceVO, DeviceVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String installDeviceWithExistingPoints(DeviceVO deviceVO) throws IllegalArgumentException {
        SiteBasestationDevice siteBasestationDevice;
        SiteDeviceBasestation siteDeviceBasestation;
        List<PointLocations> pointLocationsList;
        PointLocations pointLocations = null;
        List<ApAlByPoint> apAlByPointList = null;
        List<Points> pointsList;
        Points points = null;
        boolean updatePointLocation = true;
        PointToHwUnit pointToHwUnit;
        HwUnitToPoint hwUnitToPoint;
        DeviceStatusVO deviceStatusVO;
        List<Subscriptions> subscriptionsList;
        List<SiteCustomer> siteList;
        List<AreaSite> areaList;
        List<AssetSiteArea> assetList;
        String errorMsg;
        MQVO mqVO;
        
        if (deviceVO != null && deviceVO.getPointList() != null && !deviceVO.getPointList().isEmpty() && deviceVO.getNewDeviceId() != null) {
            
            // Find original pointLocations and info for kafka messages from DB
            try {
                if ((pointLocationsList = pointLocationsDAO.findByPK(deviceVO.getCustomerAccount(), deviceVO.getSiteId(), deviceVO.getAreaId(), deviceVO.getAssetId(), deviceVO.getPointLocationId()))!= null && !pointLocationsList.isEmpty()) {
                    pointLocations = pointLocationsList.get(0);
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
                return "{\"outcome\":\"Error: Failed to find items to install device.\"}";
            }
            
            if (pointLocations != null) {

                // Update pointLocations entity
                pointLocations.setDeviceSerialNumber(deviceVO.getNewDeviceId());
                
                for (PointVO pointVO : deviceVO.getPointList()) {
                
                    // Find original entities from DB per point
                    try {
                        if ((pointsList = pointsDAO.findByPK(pointVO.getCustomerAccount(), pointVO.getSiteId(), pointVO.getAreaId(), pointVO.getAssetId(), pointVO.getPointLocationId(), pointVO.getPointId(), pointVO.getApSetId())) != null && !pointsList.isEmpty()) {
                            points = pointsList.get(0);
                            apAlByPointList = apAlByPointDAO.findByCustomerSiteAreaAssetLocationPointApSetAlSet(points.getCustomerAccount(), points.getSiteId(), points.getAreaId(), points.getAssetId(), points.getPointLocationId(), points.getPointId(), points.getApSetId(), points.getAlSetId());
                        }
                    } catch (IllegalArgumentException e) {
                        throw e;
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        sendEvent(e.getStackTrace());
                        return "{\"outcome\":\"Error: Failed to find items to installed device.\"}";
                    }

                    if (points != null && apAlByPointList != null && !apAlByPointList.isEmpty()) {
                    
                        // Update pointVO
//                        pointVO.setSiteName(deviceVO.getSiteName());
//                        pointVO.setAreaName(deviceVO.getAreaName());
//                        pointVO.setAssetName(deviceVO.getChannelType());
//                        pointVO.setPointType(deviceVO.getAssetName());
//                        pointVO.setPointLocationName(pointLocations.getPointLocationName());
//                        pointVO.setApAlSetVOs(new ArrayList());
//                        for (ApAlByPoint apAlByPoint : apAlByPointList) {
//                            pointVO.getApAlSetVOs().add(new ApAlSetVO(apAlByPoint));
//                        }

                        // Update points entity
                        points.setSensorChannelNum(pointVO.getSensorChannelNum());
                        points.setSensorSensitivity(pointVO.getSensorSensitivity());
                        points.setSensorOffset(pointVO.getSensorOffset());
                        points.setSensorUnits(pointVO.getSensorUnits());
                        points.setAutoAcknowledge(pointVO.isAutoAcknowledge());
                        points.setAlarmEnabled(pointVO.isAlarmEnabled());
                        points.setDisabled(pointVO.isDisabled());
                        
                        //StormX deviceType specific fields
                        points.setSensorSubType(pointVO.getSensorSubType());
                        points.setSensorLocalOrientation(pointVO.getSensorLocalOrientation());
                        
                        // set siteBasestationDevice and siteDeviceBasestation
                        siteBasestationDevice = DelegateUtil.getSiteBasestationDevice(pointLocations, points, apAlByPointList.get(0));
                        siteDeviceBasestation = DelegateUtil.getSiteDeviceBasestation(siteBasestationDevice);

                        // set pointToHwUnit and hwUnitToPoint
                        pointToHwUnit = DelegateUtil.getPointToHwUnit(points, deviceVO.getNewDeviceId());
                        hwUnitToPoint = DelegateUtil.getHwUnitToPoint(points, deviceVO.getNewDeviceId());

                        // Insert the entities into Cassandra
                        try {
                            if (updatePointLocation) {
                                if ((errorMsg = ServiceUtil.validateObjectData(pointLocations)) == null) {
                                    pointLocationsDAO.update(pointLocations);
                                    updatePointLocation = false;
                                } else {
                                    return errorMsg;
                                }
                            }

                            if ((errorMsg = ServiceUtil.validateObjectData(points)) == null &&
                                    (errorMsg = ServiceUtil.validateObjectData(siteBasestationDevice)) == null &&
                                    (errorMsg = ServiceUtil.validateObjectData(siteDeviceBasestation)) == null &&
                                    (errorMsg = ServiceUtil.validateObjectData(pointToHwUnit)) == null &&
                                    (errorMsg = ServiceUtil.validateObjectData(hwUnitToPoint)) == null) {

                                pointsDAO.update(points);
                                siteBasestationDeviceDAO.create(siteBasestationDevice);
                                siteDeviceBasestationDAO.create(siteDeviceBasestation);
                                pointToHwUnitDAO.create(pointToHwUnit);
                                hwUnitToPointDAO.create(hwUnitToPoint);
                            
                                if (!pointVO.isDisabled()) {
                                    JsonUtil.createAndSendDeleteMessageToKafka(pointLocations.getDeviceSerialNumber(), points.getPointType(), points.getSensorChannelNum());
                                }
                                
                                siteList = siteCustomerDAO.findByPK(pointLocations.getCustomerAccount(), pointLocations.getSiteId());
                                areaList = areaSiteDAO.findByPK(pointLocations.getCustomerAccount(), pointLocations.getSiteId(), pointLocations.getAreaId());
                                assetList = assetSiteAreaDAO.findByPK(pointLocations.getCustomerAccount(), pointLocations.getSiteId(), pointLocations.getAreaId(), pointLocations.getAssetId());
                               
                                // Send MQ Message
                                // Fetch Subscriptions 
                                if ((subscriptionsList = subscriptionsDAO.findByPK(pointLocations.getCustomerAccount(), pointLocations.getSiteId(), "DEVICE")) != null && !subscriptionsList.isEmpty() &&
                                        siteList != null && !siteList.isEmpty() &&
                                        areaList != null && !areaList.isEmpty() &&
                                        assetList != null && !assetList.isEmpty()) {
                                    
                                    // Populate MQ VO with the details to be posted to MQ
                                    mqVO = new MQVO();
                                    mqVO.setCustomer_id(pointLocations.getCustomerAccount());
                                    mqVO.setSite_id(pointLocations.getSiteId().toString());
                                    mqVO.setSite_name(siteList.get(0).getSiteName());
                                    mqVO.setArea(areaList.get(0).getAreaName());
                                    mqVO.setAsset(assetList.get(0).getAssetName());
                                    mqVO.setPoint_location(pointLocations.getPointLocationName());
                                    mqVO.setDevice_id(pointLocations.getDeviceSerialNumber());
                                    mqVO.setDevice_action("add");
                                    mqVO.setUse_internal(subscriptionsList.get(0).isInternal());
                                    mqVO.setMq_queue_name(subscriptionsList.get(0).getMqQueueName());
                                    
                                    if (!subscriptionsList.get(0).isInternal()) {
                                        mqVO.setMq_connect_string((subscriptionsList.get(0).getMqConnectString()));
                                        mqVO.setMq_user((subscriptionsList.get(0).getMqUser()));
                                        mqVO.setMq_pwd((subscriptionsList.get(0).getMqPwd()));
                                        mqVO.setMq_client_id((subscriptionsList.get(0).getMqClientId()));
                                    }
                                    
                                    ConfigService.sendMQMessage(JsonConverterUtil.toJSON(mqVO));
                                } 
                                if ((deviceVO.getSiteName() == null || deviceVO.getSiteName().isEmpty())
                                    && siteList != null && !siteList.isEmpty())
                                    deviceVO.setSiteName(siteList.get(0).getSiteName());
                            
                                if ((deviceVO.getAreaName() == null || deviceVO.getAreaName().isEmpty())
                                        && areaList != null && !areaList.isEmpty())
                                    deviceVO.setAreaName(areaList.get(0).getAreaName());

                                if ((deviceVO.getAssetName() == null || deviceVO.getAssetName().isEmpty())
                                        && assetList != null && !assetList.isEmpty())
                                    deviceVO.setAssetName(assetList.get(0).getAssetName());
                                deviceVO.setPointLocationName(pointLocations.getPointLocationName());
                                //Update MySQL device_status
                                deviceStatusVO = DelegateUtil.getDeviceStatus(deviceVO, deviceVO.getNewDeviceId());
                                deviceStatusDAO.upsert(deviceStatusVO);
                            } else {
                                return errorMsg;
                            }
                        } catch (IllegalArgumentException e) {
                            throw e;
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                            sendEvent(e.getStackTrace());
                            break;
                        }
                    } else {
                        return "{\"outcome\":\"Error: Failed to find items to installed device.\"}";
                    }
                }
                return "{\"outcome\":\"Device installed successfully.\"}";
            } else {
                return "{\"outcome\":\"Error: Failed to find items to installed device.\"}";
            }
        }
        return "{\"outcome\":\"Error: Failed to installed device.\"}";
    }
    
    /**
     * Insert new Rows into Cassandra based on the given object
     *
     * @param assetVO, AssetVO Object
     * @param siteName, String object
     * @param areaName, String object
     * @param hwDisabled, boolean
     * @param modified, boolean
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String installOrModifyTS1Channel(AssetVO assetVO, String siteName, String areaName, boolean hwDisabled, boolean modified) throws IllegalArgumentException, Exception {
        AssetSiteArea assetSiteArea;
        List<PointLocations> pointLocationsList;
        List<Points> pointsList;
        List<ApAlByPoint> apAlByPointList;
        List<SiteBasestationDevice> siteBasestationDeviceList, dbSiteBasestationDeviceList, delSiteBasestationDeviceList;
        List<SiteDeviceBasestation> siteDeviceBasestationList, dbSiteDeviceBasestationList;
        SiteBasestationDevice siteBasestationDevice;
        ApAlByPoint baseApAlByPoint;
        PointToHwUnit pointToHwUnit;
        HwUnitToPoint hwUnitToPoint;
        
        List<PointToHwUnit> pointToHwUnitList;
        List<HwUnitToPoint> hwUnitToPointList;
        String errorMsg;
        SitePtLocationNames sitePtLocationNames;
        List<SitePtLocationNames> dbSitePtLocationNamesList, newSitePtLocationNamesList;
        List<GlobalPtLocationNames> dbGlobalPtLocationNamesList;
        List<Subscriptions> subscriptionsList;

        PointLocations pl;
        MQVO mqVO;
        
        if (assetVO.getAssetId() == null) {
            assetVO.setAssetId(UUID.randomUUID());
        }
        
        assetSiteArea = DelegateUtil.getAssetSiteArea(assetVO);
        pointLocationsList = DelegateUtil.getPointLocationsList(assetVO);
        pointsList = DelegateUtil.getPointsList(assetVO);
        apAlByPointList = new ArrayList();
        
        if (assetVO.getPointLocationList() != null && !assetVO.getPointLocationList().isEmpty()) {
            for (PointLocationVO pointLocationsVO : assetVO.getPointLocationList()) {
                if (pointLocationsVO.getPointList()!= null && !pointLocationsVO.getPointList().isEmpty()) {
                    for (PointVO points : pointLocationsVO.getPointList()) {
                        if ( points.getApAlSetVOs() != null && ! points.getApAlSetVOs().isEmpty()) {
                            for (ApAlSetVO apAlSetVO : points.getApAlSetVOs()) {
                                apAlByPointList.add(DelegateUtil.getApAlByPoint(points, apAlSetVO));
                            }
                        }
                    }
                }
            }
        }
        siteBasestationDeviceList = new ArrayList();
        siteDeviceBasestationList = new ArrayList();
        delSiteBasestationDeviceList = new ArrayList();
        
        pointToHwUnitList = new ArrayList();
        hwUnitToPointList = new ArrayList();
        
        newSitePtLocationNamesList = new ArrayList<>();
        
        if (pointLocationsList != null && !pointLocationsList.isEmpty()) {
            for (PointLocations pointLocations : pointLocationsList) {
                dbSitePtLocationNamesList = sitePtLocationNamesDAO.findByPK(pointLocations.getCustomerAccount(), pointLocations.getSiteId(), pointLocations.getPointLocationName());
                if (dbSitePtLocationNamesList == null || dbSitePtLocationNamesList.isEmpty()) {
                    dbGlobalPtLocationNamesList = globalPtLocationNamesDAO.findByPK(pointLocations.getCustomerAccount(), pointLocations.getPointLocationName());
                    if (dbGlobalPtLocationNamesList == null || dbGlobalPtLocationNamesList.isEmpty()) {
                        sitePtLocationNames = new SitePtLocationNames();
                        sitePtLocationNames.setCustomerAccount(pointLocations.getCustomerAccount());
                        sitePtLocationNames.setSiteId(pointLocations.getSiteId());
                        sitePtLocationNames.setPointLocationName(pointLocations.getPointLocationName());
                        sitePtLocationNames.setDescription(null);
                        if (!newSitePtLocationNamesList.contains(sitePtLocationNames)) {
                            newSitePtLocationNamesList.add(sitePtLocationNames);
                        }
                    }
                }
                for (Points points : pointsList) {
                    if (points.getPointLocationId() == pointLocations.getPointLocationId()) {
                        baseApAlByPoint = null;
                        for (ApAlByPoint apAlByPoint : apAlByPointList) {
                            if (apAlByPoint.getPointId() == points.getPointId()) {
                                baseApAlByPoint = apAlByPoint;
                                break;
                            }
                        }
                        siteBasestationDevice = DelegateUtil.getSiteBasestationDevice(pointLocations, points, baseApAlByPoint);
                        siteBasestationDeviceList.add(siteBasestationDevice);
                        siteBasestationDeviceList.forEach(l -> l.setDisabled(hwDisabled));
                        siteDeviceBasestationList.add(DelegateUtil.getSiteDeviceBasestation(siteBasestationDevice));
                        hwUnitToPoint = DelegateUtil.getHwUnitToPoint(points, pointLocations.getDeviceSerialNumber());
                        hwUnitToPointList.add(hwUnitToPoint);
                        pointToHwUnit = DelegateUtil.getPointToHwUnit(points, pointLocations.getDeviceSerialNumber());
                        pointToHwUnitList.add(pointToHwUnit);
                        
                        //Delete the siteBasestationDevice and siteDeviceBasestation records for the old base port number
                        try { 
                            if ((dbSiteDeviceBasestationList = siteDeviceBasestationDAO.findByPK(pointLocations.getCustomerAccount(), pointLocations.getSiteId(), pointLocations.getDeviceSerialNumber())) != null && !dbSiteDeviceBasestationList.isEmpty()) {
                                if (dbSiteDeviceBasestationList.get(0).getBaseStationPort() != pointLocations.getBasestationPortNum()) {
                                    dbSiteBasestationDeviceList = siteBasestationDeviceDAO.findByPK(pointLocations.getCustomerAccount(), pointLocations.getSiteId(), dbSiteDeviceBasestationList.get(0).getBaseStationPort(), pointLocations.getDeviceSerialNumber(), points.getPointId());
                                    delSiteBasestationDeviceList.addAll(dbSiteBasestationDeviceList);
                                }
                            }
                        } catch (IllegalArgumentException e) {
                            throw e;
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                            sendEvent(e.getStackTrace());
                            return "{\"outcome\":\"Error: Failed lookups.\"}";
                        }
                    }
                }
            }
        }

        // Insert the entities into Cassandra
        try { 
            if (((errorMsg = ServiceUtil.validateObjectData(assetSiteArea)) == null) && 
                    ((errorMsg = ServiceUtil.validateObjects(pointLocationsList, Boolean.TRUE)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(pointsList, Boolean.TRUE)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(apAlByPointList, Boolean.TRUE)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(hwUnitToPointList, Boolean.TRUE)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(pointToHwUnitList, Boolean.TRUE)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(siteBasestationDeviceList, Boolean.TRUE)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(newSitePtLocationNamesList, Boolean.TRUE)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(siteDeviceBasestationList, Boolean.TRUE)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(delSiteBasestationDeviceList, Boolean.TRUE)) == null)) {
                
                assetSiteAreaDAO.create(assetSiteArea);
                
                if (modified) {
                    if (pointLocationsList != null && !pointLocationsList.isEmpty()) {
                        pointLocationsDAO.update(pointLocationsList);
                    }
                }
                else {
                    if (pointLocationsList != null && !pointLocationsList.isEmpty()) {
                        pointLocationsDAO.create(pointLocationsList);
                    }
                }
                
                if (!newSitePtLocationNamesList.isEmpty()) {
                    newSitePtLocationNamesList.forEach(sitePtLocationName -> sitePtLocationNamesDAO.create(sitePtLocationName));
                }
                if (pointsList != null && !pointsList.isEmpty()) {
                    pointsDAO.create(pointsList);
                }
                if (!apAlByPointList.isEmpty()) {
                    apAlByPointDAO.create(apAlByPointList);
                }
                if (!siteBasestationDeviceList.isEmpty()) {
                    siteBasestationDeviceDAO.create(siteBasestationDeviceList);
                }
                if (!siteDeviceBasestationList.isEmpty()) {
                    siteDeviceBasestationList.forEach(siteDeviceBasestation -> siteDeviceBasestationDAO.create(siteDeviceBasestation));
                }
                if (!hwUnitToPointList.isEmpty()) {
                    hwUnitToPointList.forEach(hwUnit -> hwUnitToPointDAO.create(hwUnit));
                }
                if (!pointToHwUnitList.isEmpty()) {
                    pointToHwUnitList.forEach(pointToHU -> pointToHwUnitDAO.create(pointToHU));
                }
                if (!delSiteBasestationDeviceList.isEmpty()) {
                    siteBasestationDeviceDAO.delete(delSiteBasestationDeviceList);
                }
                
                // Send Kafka Message
                if (pointsList != null && !pointsList.isEmpty() &&
                        pointLocationsList != null && !pointLocationsList.isEmpty()) {
                    for (Points points : pointsList) {
                        if (!points.isDisabled() && pointLocationsList.stream().anyMatch(pLoc -> points.getPointLocationId().equals(pLoc.getPointLocationId()))) {
                            pl = pointLocationsList.stream().filter(pLoc -> points.getPointLocationId().equals(pLoc.getPointLocationId())).findFirst().get();
                            JsonUtil.createAndSendDeleteMessageToKafka(pl.getDeviceSerialNumber(), points.getPointType(), points.getSensorChannelNum());
                        }
                    }
                } 
                
                // Send MQ Message
                // Fetch Subscriptions 
                if (pointLocationsList != null && !pointLocationsList.isEmpty() &&
                        (subscriptionsList = subscriptionsDAO.findByPK(assetSiteArea.getCustomerAccount(), assetSiteArea.getSiteId(), "DEVICE")) != null && !subscriptionsList.isEmpty()) {
                    
                    // Populate MQVO with the details to be posted to MQ
                    mqVO = new MQVO();
                    mqVO.setCustomer_id(assetSiteArea.getCustomerAccount());
                    mqVO.setSite_id(assetSiteArea.getSiteId().toString());
                    mqVO.setSite_name(siteName);
                    mqVO.setArea(areaName);
                    mqVO.setAsset(assetSiteArea.getAssetName());
                    mqVO.setPoint_location(pointLocationsList.get(0).getPointLocationName());
                    mqVO.setDevice_id(pointLocationsList.get(0).getDeviceSerialNumber());
                    mqVO.setDevice_action("add");
                    mqVO.setUse_internal(subscriptionsList.get(0).isInternal());
                    mqVO.setMq_queue_name(subscriptionsList.get(0).getMqQueueName());

                    if (!subscriptionsList.get(0).isInternal()) {
                        mqVO.setMq_connect_string((subscriptionsList.get(0).getMqConnectString()));
                        mqVO.setMq_user((subscriptionsList.get(0).getMqUser()));
                        mqVO.setMq_pwd((subscriptionsList.get(0).getMqPwd()));
                        mqVO.setMq_client_id((subscriptionsList.get(0).getMqClientId()));
                    }
                    
                    ConfigService.sendMQMessage(JsonConverterUtil.toJSON(mqVO));
                }
                
               
                return "{\"outcome\":\"Install/modify TS1 Channel successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to install/modify TS1 Channel.\"}";
    }
}
