/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.device;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.dao.SiteBasestationDeviceDAO;
import com.uptime.cassandra.monitor.dao.SiteDeviceBasestationDAO;
import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import com.uptime.cassandra.monitor.entity.SiteDeviceBasestation;
import com.uptime.cassandra.presets.dao.GlobalTachometersDAO;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteTachometersDAO;
import com.uptime.cassandra.presets.entity.GlobalTachometers;
import com.uptime.cassandra.presets.entity.SiteTachometers;
import com.uptime.config.dao.DeviceStatusDAO;
import com.uptime.config.vo.DeviceStatusVO;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class ReadDeviceDelegate {
    private final SiteBasestationDeviceDAO siteBasestationDeviceDAO;
    private final SiteDeviceBasestationDAO siteDeviceBasestationDAO;
    private final GlobalTachometersDAO globalTachometersDAO;
    private final SiteTachometersDAO siteTachometersDAO;
    private final DeviceStatusDAO deviceStatusDAO;
    
    public ReadDeviceDelegate(){
        siteBasestationDeviceDAO = BaseStationMapperImpl.getInstance().siteBasestationDeviceDAO();
        siteDeviceBasestationDAO = BaseStationMapperImpl.getInstance().siteDeviceBasestationDAO();
        globalTachometersDAO = PresetsMapperImpl.getInstance().globalTachometersDAO();
        siteTachometersDAO = PresetsMapperImpl.getInstance().siteTachometersDAO();
        deviceStatusDAO = new DeviceStatusDAO();
    }
    
     
    /**
     * Return a List of SiteBasestationDevice Objects by the given params
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param baseStationPort, short
     * @param deviceId, String Object
     * @param pointId, UUID Object
     * @return List of SiteBasestationDevice Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteBasestationDevice> getSiteBasestationDeviceByPK(String customerAccount, UUID siteId, short baseStationPort, String deviceId, UUID pointId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteBasestationDeviceDAO.findByPK(customerAccount, siteId, baseStationPort, deviceId, pointId);
    }
    
    /**
     * Return a List of SiteBasestationDevice Objects by the given params
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param baseStationPort, short
     * @param deviceId, String Object
     * @return List of SiteBasestationDevice Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteBasestationDevice> getSiteBasestationDeviceByCustomerSiteBasestationDevice(String customerAccount, UUID siteId, short baseStationPort, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteBasestationDeviceDAO.findByCustomerSiteBasestationDevice(customerAccount, siteId, baseStationPort, deviceId);
    }
    
    /**
     * Return a List of SiteDeviceBasestation Objects by the given params
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param deviceId, String Object
     * @return List of SiteDeviceBasestation Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteDeviceBasestation> getSiteDeviceBasestationByPK(String customerAccount, UUID siteId, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteDeviceBasestationDAO.queryByPK(customerAccount, siteId, deviceId).all();
    }

    /**
     * Return a List of DeviceStatusVO Objects by the given params
     *
     * @param customer, String Object
     * @param siteName, String Object
     * @return List of DeviceStatusVO Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public List<DeviceStatusVO> getDeviceStatusByCustomerSiteName(String customer, String siteName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        return deviceStatusDAO.findByCustomerSite(customer, siteName);
    }
    
    /**
     * Return a List of Tachometers Objects from global_tachometers table by the given params
     *
     * @param customerAccount, String Object
     * @param tachId, UUID Object
     * @return List of Tachometers Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<GlobalTachometers> findByPK(String customerAccount, UUID tachId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return globalTachometersDAO.findByPK(customerAccount,tachId);
    }

    /**
     * Return a List of SiteTachometers Objects from site_tachometers table by the given params
     *
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param tachId, UUID Object
     * @return List of SiteTachometers Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteTachometers> findByPK(String customerAccount, UUID siteId, UUID tachId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteTachometersDAO.findByPK(customerAccount,siteId, tachId);
    }
}
