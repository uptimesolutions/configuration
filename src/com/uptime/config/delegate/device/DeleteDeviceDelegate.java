/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.device;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.dao.AreaSiteDAO;
import com.uptime.cassandra.config.dao.AssetSiteAreaDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.HwUnitToPointDAO;
import com.uptime.cassandra.config.dao.PointLocationHwUnitHistoryDAO;
import com.uptime.cassandra.config.dao.PointLocationsDAO;
import com.uptime.cassandra.config.dao.PointToHwUnitDAO;
import com.uptime.cassandra.config.dao.SiteCustomerDAO;
import com.uptime.cassandra.config.dao.SubscriptionsDAO;
import com.uptime.cassandra.config.entity.AreaSite;
import com.uptime.cassandra.config.entity.AssetSiteArea;
import com.uptime.cassandra.config.entity.HwUnitToPoint;
import com.uptime.cassandra.config.entity.PointLocationHwUnitHistory;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.cassandra.config.entity.PointToHwUnit;
import com.uptime.cassandra.config.entity.SiteCustomer;
import com.uptime.cassandra.config.entity.Subscriptions;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.dao.SiteBasestationDeviceDAO;
import com.uptime.cassandra.monitor.dao.SiteDeviceBasestationDAO;
import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import com.uptime.cassandra.monitor.entity.SiteDeviceBasestation;
import com.uptime.cassandra.presets.dao.PresetsMapperImpl;
import com.uptime.cassandra.presets.dao.SiteApAlSetsDAO;
import com.uptime.cassandra.presets.entity.SiteApAlSets;
import com.uptime.config.ConfigService;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.dao.DeviceStatusDAO;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.DeviceVO;
import com.uptime.config.vo.HwUnitVO;
import com.uptime.config.vo.MQVO;
import com.uptime.config.vo.SiteBasestationDeviceVO;
import com.uptime.config.vo.SiteDeviceBasestationVO;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.services.util.ServiceUtil;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class DeleteDeviceDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteDeviceDelegate.class.getName());
    private final SiteBasestationDeviceDAO siteBasestationDeviceDAO;
    private final SiteDeviceBasestationDAO siteDeviceBasestationDAO;
    private final PointToHwUnitDAO pointToHwUnitDAO;
    private final HwUnitToPointDAO hwUnitToPointDAO;
    private final SiteApAlSetsDAO siteApAlSetsDAO;
    private final PointLocationHwUnitHistoryDAO pointLocationHwUnitHistoryDAO;
    private final PointLocationsDAO pointLocationsDAO;
    private final SubscriptionsDAO subscriptionsDAO;
    private final SiteCustomerDAO siteCustomerDAO;
    private final AreaSiteDAO areaSiteDAO;
    private final AssetSiteAreaDAO assetSiteAreaDAO;
    private final DeviceStatusDAO deviceStatusDAO;
    
    public DeleteDeviceDelegate(){
        siteBasestationDeviceDAO = BaseStationMapperImpl.getInstance().siteBasestationDeviceDAO();
        siteDeviceBasestationDAO = BaseStationMapperImpl.getInstance().siteDeviceBasestationDAO();
        pointToHwUnitDAO = ConfigMapperImpl.getInstance().pointToHwUnitDAO();
        hwUnitToPointDAO = ConfigMapperImpl.getInstance().hwUnitToPointDAO();
        siteApAlSetsDAO = PresetsMapperImpl.getInstance().siteApAlSetsDAO();
        pointLocationHwUnitHistoryDAO = ConfigMapperImpl.getInstance().pointLocationHwUnitHistoryDAO();
        pointLocationsDAO = ConfigMapperImpl.getInstance().pointLocationsDAO();
        subscriptionsDAO = ConfigMapperImpl.getInstance().subscriptionsDAO();
        siteCustomerDAO = ConfigMapperImpl.getInstance().siteCustomerDAO();
        areaSiteDAO = ConfigMapperImpl.getInstance().areaSiteDAO();
        assetSiteAreaDAO = ConfigMapperImpl.getInstance().assetSiteAreaDAO();
        deviceStatusDAO = new DeviceStatusDAO();
    }
    
    /**
     * Delete Rows in the hwunit_to_point, point_to_hwunit, site_basestation_device and 
     * site_device_basestation tables based on the given object
     *
     * @param deviceVO, DeviceVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String deleteDevice(DeviceVO deviceVO) throws IllegalArgumentException, Exception {
        List<HwUnitToPoint> dbDeviceHwToPointList, delDeviceHwToPointList;
        List<PointToHwUnit> dbPointToHwUnitList, delPointToHwUnitList;
        List<SiteBasestationDevice> dbSiteBaseStationDeviceList, delSiteBaseStationDeviceList;
        List<SiteDeviceBasestation> dbDeviceSiteBaseStationList, delSiteDeviceBaseStationList;
        List<PointLocations> dbPointLocationsList;
        HashMap<UUID, PointLocations> plMap;
        PointLocations dbPointLocations = null;
        PointLocationHwUnitHistory insertNewPLHwUnitHistory = null;
        List<Subscriptions> subscriptionsList;
        List<SiteCustomer> siteList;
        List<AreaSite> areaList;
        List<AssetSiteArea> assetList;
        List<PointLocations> plList;
        String errorMsg;
        MQVO mqVO;

        try {
            // get hwToPointList by device Serial #
            dbDeviceHwToPointList = hwUnitToPointDAO.findByDevice(deviceVO.getExistingDeviceId());
            
            if (dbDeviceHwToPointList != null && !dbDeviceHwToPointList.isEmpty()) {
                delSiteBaseStationDeviceList = new ArrayList();
                delSiteDeviceBaseStationList = new ArrayList();
                delDeviceHwToPointList = new ArrayList();
                delPointToHwUnitList = new ArrayList();       
                plMap = new HashMap();
                errorMsg = null;
                
                delDeviceHwToPointList.addAll(dbDeviceHwToPointList);
                for (HwUnitToPoint hwPointVO : dbDeviceHwToPointList) {
                    dbPointLocations = null;

                    if (plMap.isEmpty() || (plMap.get(hwPointVO.getPointLocationId()) == null)) {
                        if ((dbPointLocationsList = pointLocationsDAO.findByPK(hwPointVO.getCustomerAcct(), hwPointVO.getSiteId(), hwPointVO.getAreaId(), hwPointVO.getAssetId(), hwPointVO.getPointLocationId())) != null && !dbPointLocationsList.isEmpty()) {
                            dbPointLocations = dbPointLocationsList.get(0);
                        }
                    } else {
                        if (!plMap.isEmpty()) {
                            dbPointLocations = plMap.get(hwPointVO.getPointLocationId());
                        }
                    }
                    
                    if (dbPointLocations != null) {
                        if ((dbSiteBaseStationDeviceList = siteBasestationDeviceDAO.findByPK(hwPointVO.getCustomerAcct(), hwPointVO.getSiteId(), dbPointLocations.getBasestationPortNum(), hwPointVO.getDeviceId(), hwPointVO.getPointId())) != null && !dbSiteBaseStationDeviceList.isEmpty()) {
                            delSiteBaseStationDeviceList.addAll(dbSiteBaseStationDeviceList);
                        }
                        if ((dbDeviceSiteBaseStationList = siteDeviceBasestationDAO.findByPK(hwPointVO.getCustomerAcct(), hwPointVO.getSiteId(), hwPointVO.getDeviceId())) != null && !dbDeviceSiteBaseStationList.isEmpty()) {
                            delSiteDeviceBaseStationList.addAll(dbDeviceSiteBaseStationList);
                        }
                    }
                    
                    if (hwPointVO.getPointId() != null && (dbPointToHwUnitList = pointToHwUnitDAO.findByPK(hwPointVO.getPointId())) != null && !dbPointToHwUnitList.isEmpty()) {
                        delPointToHwUnitList.addAll(dbPointToHwUnitList);
                    }
                }

                if (dbPointLocations != null) {
                    dbPointLocations.setDeviceSerialNumber(null);
                    insertNewPLHwUnitHistory = new PointLocationHwUnitHistory();
                    insertNewPLHwUnitHistory.setPointLocationId(dbPointLocations.getPointLocationId());
                    insertNewPLHwUnitHistory.setCustomerAccount(dbPointLocations.getCustomerAccount());
                    insertNewPLHwUnitHistory.setDeviceId(deviceVO.getExistingDeviceId());
                    insertNewPLHwUnitHistory.setDateFrom(LocalDateTime.now().atOffset(ZoneOffset.UTC).toInstant());
                }
                
                // Delete entities from Cassandra if original entities are found
                try {
                    if (((dbPointLocations != null && (errorMsg = ServiceUtil.validateObjectData(dbPointLocations)) == null)
                            && ((errorMsg = ServiceUtil.validateObjects(delDeviceHwToPointList, Boolean.FALSE)) == null)
                            && ((errorMsg = ServiceUtil.validateObjects(delPointToHwUnitList, Boolean.FALSE)) == null) 
                            && ((errorMsg = ServiceUtil.validateObjects(delSiteBaseStationDeviceList, Boolean.TRUE)) == null) 
                            && ((errorMsg = ServiceUtil.validateObjects(delSiteDeviceBaseStationList, Boolean.TRUE)) == null))
                            && ((errorMsg = ServiceUtil.validateObjectData(insertNewPLHwUnitHistory)) == null)) {
                        
                        pointLocationsDAO.update(dbPointLocations);
                        if (!delDeviceHwToPointList.isEmpty()) {
                            hwUnitToPointDAO.delete(delDeviceHwToPointList);
                        }
                        if (!delPointToHwUnitList.isEmpty()) {
                            pointToHwUnitDAO.delete(delPointToHwUnitList);
                        }
                        if (!delSiteBaseStationDeviceList.isEmpty()) {
                            siteBasestationDeviceDAO.delete(delSiteBaseStationDeviceList);
                        }
                        if (!delSiteDeviceBaseStationList.isEmpty()) {
                            delSiteDeviceBaseStationList.forEach(siteDeviceBasestation -> siteDeviceBasestationDAO.delete(siteDeviceBasestation));
                        }
                        pointLocationHwUnitHistoryDAO.create(insertNewPLHwUnitHistory);

                    } else {
                        return errorMsg;
                    }

                    dbDeviceHwToPointList.forEach(exiHwToPoint -> JsonUtil.createAndSendKafkaMessageForDeleteHwUnit(new HwUnitVO(exiHwToPoint)));
                    
                    // Send MQ Message
                    // Fetch Subscriptions 
                    if ((siteList = siteCustomerDAO.findByPK(dbDeviceHwToPointList.get(0).getCustomerAcct(), dbDeviceHwToPointList.get(0).getSiteId())) != null && !siteList.isEmpty() &&
                            (areaList = areaSiteDAO.findByPK(dbDeviceHwToPointList.get(0).getCustomerAcct(), dbDeviceHwToPointList.get(0).getSiteId(), dbDeviceHwToPointList.get(0).getAreaId())) != null && !areaList.isEmpty() &&
                            (assetList = assetSiteAreaDAO.findByPK(dbDeviceHwToPointList.get(0).getCustomerAcct(), dbDeviceHwToPointList.get(0).getSiteId(), dbDeviceHwToPointList.get(0).getAreaId(), dbDeviceHwToPointList.get(0).getAssetId())) != null && !assetList.isEmpty() &&
                            (plList = pointLocationsDAO.findByPK(dbDeviceHwToPointList.get(0).getCustomerAcct(), dbDeviceHwToPointList.get(0).getSiteId(), dbDeviceHwToPointList.get(0).getAreaId(), dbDeviceHwToPointList.get(0).getAssetId(), dbDeviceHwToPointList.get(0).getPointLocationId())) != null && !plList.isEmpty() &&
                            (subscriptionsList = subscriptionsDAO.findByPK(dbDeviceHwToPointList.get(0).getCustomerAcct(), dbDeviceHwToPointList.get(0).getSiteId(), "DEVICE")) != null && !subscriptionsList.isEmpty()) {
                        
                        // Populate MQ VO with the details to be posted to MQ
                        mqVO = new MQVO();
                        mqVO.setCustomer_id(dbDeviceHwToPointList.get(0).getCustomerAcct());
                        mqVO.setSite_id(dbDeviceHwToPointList.get(0).getSiteId().toString());
                        mqVO.setSite_name(siteList.get(0).getSiteName());
                        mqVO.setArea(areaList.get(0).getAreaName());
                        mqVO.setAsset(assetList.get(0).getAssetName());
                        mqVO.setPoint_location(plList.get(0).getPointLocationName());
                        mqVO.setDevice_id(dbDeviceHwToPointList.get(0).getDeviceId());
                        mqVO.setDevice_action("remove");
                        mqVO.setUse_internal(subscriptionsList.get(0).isInternal());
                        mqVO.setMq_queue_name(subscriptionsList.get(0).getMqQueueName());
                        
                        if (!subscriptionsList.get(0).isInternal()) {
                            mqVO.setMq_connect_string((subscriptionsList.get(0).getMqConnectString()));
                            mqVO.setMq_user((subscriptionsList.get(0).getMqUser()));
                            mqVO.setMq_pwd((subscriptionsList.get(0).getMqPwd()));
                            mqVO.setMq_client_id((subscriptionsList.get(0).getMqClientId()));
                        }
                        
                        ConfigService.sendMQMessage(JsonConverterUtil.toJSON(mqVO));
                    }

                    //Update MySQL device_status
                    deviceStatusDAO.clearDevice(deviceVO.getExistingDeviceId());
                    
                } catch (IllegalArgumentException e) {
                    throw e;
                } catch (Exception e) {
                    sendEvent(e.getStackTrace());
                    return "{\"outcome\":\"Error: issue with delete device.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient data found.\"}";
            }

            return "{\"outcome\":\"Device deleted successfully.\"}";

        } catch (ReadTimeoutException | UnavailableException | IllegalArgumentException e) {
            return "{\"outcome\":\"Error: Failed to delete device.\"}";
        }
    }
        
    /**
     * Delete Rows in Cassandra based on the given object
     *
     * @param siteBasestationDeviceVO, SiteBasestationDeviceVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteSiteBasestationDevice(SiteBasestationDeviceVO siteBasestationDeviceVO) throws IllegalArgumentException {
        List<SiteBasestationDevice> dbSiteBasestationDeviceList;
        SiteBasestationDevice dbSiteBasestationDevice = null;
        try {
        // Find the entities based on the given values
            if (siteBasestationDeviceVO != null) {
                if (siteBasestationDeviceVO.getPointId() != null &&
                    siteBasestationDeviceVO.getCustomerAccount() != null &&
                    siteBasestationDeviceVO.getSiteId() != null  && 
                    siteBasestationDeviceVO.getBaseStationPort() != 0 &&
                    siteBasestationDeviceVO.getDeviceId() != null &&
                   (dbSiteBasestationDeviceList = siteBasestationDeviceDAO.findByPK(siteBasestationDeviceVO.getCustomerAccount(), siteBasestationDeviceVO.getSiteId(), siteBasestationDeviceVO.getBaseStationPort(),siteBasestationDeviceVO.getDeviceId(),siteBasestationDeviceVO.getPointId())) != null && !dbSiteBasestationDeviceList.isEmpty()) {
                    
                    dbSiteBasestationDevice = dbSiteBasestationDeviceList.get(0);
                }
            }
            else {
                return "{\"outcome\":\"Error: Invaid input.\"}";
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to delete.\"}";
        }

        // Delete entities from Cassandra if original entities are found
        if (dbSiteBasestationDevice != null) {
            try {
                siteBasestationDeviceDAO.delete(dbSiteBasestationDevice);
                return "{\"outcome\":\"Deleted SiteBasestationDevice items successfully.\"}";
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
            return "{\"outcome\":\"Error: Failed to delete SiteBasestationDevice items.\"}";
        }

        return "{\"outcome\":\"SiteBasestationDevice items not found to delete.\"}";
    }
    
    /**
     * Delete Rows in Cassandra based on the given object
     *
     * @param hwUnitVO, HwUnitVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteHwUnitToPoint(HwUnitVO hwUnitVO) throws IllegalArgumentException {
        List<HwUnitToPoint> dbHwUnitToPointList;
        HwUnitToPoint dbHwUnitToPoint = null;

        // Find the entities based on the given values
        try {
            if (hwUnitVO != null) {
                if (hwUnitVO.getDeviceId() != null && (dbHwUnitToPointList = hwUnitToPointDAO.findByPK(hwUnitVO.getDeviceId(), hwUnitVO.getChannelType(), hwUnitVO.getChannelNum())) != null && !dbHwUnitToPointList.isEmpty()) {
                    dbHwUnitToPoint = dbHwUnitToPointList.get(0);
                }
            }
            else {
                return "{\"outcome\":\"Error: Invaid input.\"}";
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to delete.\"}";
        }

        // Delete from Cassandra if original entities are found
        if (dbHwUnitToPoint != null) {
            try {
                hwUnitToPointDAO.delete(dbHwUnitToPoint);
                return "{\"outcome\":\"Deleted HwUnitToPoint items successfully.\"}";
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
            return "{\"outcome\":\"Error: Failed to delete HwUnitToPoint items.\"}";
        }

        return "{\"outcome\":\"HwUnitToPoint items not found to delete.\"}";
    }
    
    /**
     * Delete Rows in Cassandra based on the given object
     *
     * @param siteDeviceBasestationVO, SiteDeviceBasestationVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String deleteSiteDeviceBasestation(SiteDeviceBasestationVO siteDeviceBasestationVO) throws IllegalArgumentException, Exception {
        List<SiteBasestationDevice> dbSiteBasestationDeviceList;
        SiteBasestationDevice dbSiteBasestationDevice = null;
        
        try {
            // Find the entities based on the given values
            if (siteDeviceBasestationVO != null) {
                if (
                    siteDeviceBasestationVO.getCustomerAccount() != null &&
                    siteDeviceBasestationVO.getSiteId() != null  && 
                    siteDeviceBasestationVO.getDeviceId() != null &&
                   (dbSiteBasestationDeviceList = siteBasestationDeviceDAO.findByPK(siteDeviceBasestationVO.getCustomerAccount(), siteDeviceBasestationVO.getSiteId(), siteDeviceBasestationVO.getBaseStationPort(),siteDeviceBasestationVO.getDeviceId(),siteDeviceBasestationVO.getPointId())) != null && !dbSiteBasestationDeviceList.isEmpty()) {
                    
                    dbSiteBasestationDevice = dbSiteBasestationDeviceList.get(0);
                }
            }
            else {
                return "{\"outcome\":\"Error: Invaid input.\"}";
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to delete.\"}";
        }

        // Delete entities from Cassandra if original entities are found
        if (dbSiteBasestationDevice != null) {
            try {
                siteBasestationDeviceDAO.delete(dbSiteBasestationDevice);
                return "{\"outcome\":\"Deleted SiteBasestationDevice items successfully.\"}";
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
            return "{\"outcome\":\"Error: Failed to delete SiteBasestationDevice items.\"}";
        }

        return "{\"outcome\":\"SiteBasestationDevice items not found to delete.\"}";
    }
    
    /**
     * Insert new Rows into Cassandra based on the given object
     *
     * @param siteApAlSets, SiteApAlSets object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String deleteSiteApAlSet(SiteApAlSets siteApAlSets) throws IllegalArgumentException, Exception {
        String errorMsg;
        
        // Delete the entities from Cassandra
        try {
            if ((errorMsg = ServiceUtil.validateObjectData(siteApAlSets)) == null) {
                siteApAlSetsDAO.delete(siteApAlSets);
            } else {
                return errorMsg;
            }
            return "{\"outcome\":\"Deleted SiteApAlSets successfully.\"}";
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }

        return "{\"outcome\":\"Error: Failed to create new SiteApAlSets.\"}";
    } 
}
