/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.device;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.dao.AreaSiteDAO;
import com.uptime.cassandra.config.dao.AssetSiteAreaDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.HwUnitToPointDAO;
import com.uptime.cassandra.config.dao.PointLocationHwUnitHistoryDAO;
import com.uptime.cassandra.config.dao.PointLocationsDAO;
import com.uptime.cassandra.config.dao.PointToHwUnitDAO;
import com.uptime.cassandra.config.dao.PointsDAO;
import com.uptime.cassandra.config.dao.SiteCustomerDAO;
import com.uptime.cassandra.config.dao.SubscriptionsDAO;
import com.uptime.cassandra.config.entity.AreaSite;
import com.uptime.cassandra.config.entity.AssetSiteArea;
import com.uptime.cassandra.config.entity.HwUnitToPoint;
import com.uptime.cassandra.config.entity.PointLocationHwUnitHistory;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.cassandra.config.entity.PointToHwUnit;
import com.uptime.cassandra.config.entity.Points;
import com.uptime.cassandra.config.entity.SiteCustomer;
import com.uptime.cassandra.config.entity.Subscriptions;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.dao.SiteBasestationDeviceDAO;
import com.uptime.cassandra.monitor.dao.SiteDeviceBasestationDAO;
import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import com.uptime.cassandra.monitor.entity.SiteDeviceBasestation;
import com.uptime.config.ConfigService;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.dao.DeviceStatusDAO;
import com.uptime.config.utils.DelegateUtil;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.DeviceStatusVO;
import com.uptime.config.vo.DeviceVO;
import com.uptime.config.vo.HwUnitVO;
import com.uptime.config.vo.MQVO;
import com.uptime.config.vo.PointVO;
import com.uptime.config.vo.SiteBasestationDeviceVO;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.services.util.ServiceUtil;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class UpdateDeviceDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateDeviceDelegate.class.getName());
    private final SiteBasestationDeviceDAO siteBasestationDeviceDAO;
    private final SiteDeviceBasestationDAO siteDeviceBasestationDAO;
    private final HwUnitToPointDAO hwUnitToPointDAO;
    private final PointToHwUnitDAO pointToHwUnitDAO;
    private final PointLocationHwUnitHistoryDAO pointLocationHwUnitHistoryDAO;
    private final PointLocationsDAO pointLocationsDAO;
    private final PointsDAO pointsDAO;
    private final SubscriptionsDAO subscriptionsDAO;
    private final SiteCustomerDAO siteCustomerDAO;
    private final AreaSiteDAO areaSiteDAO;
    private final AssetSiteAreaDAO assetSiteAreaDAO;
    private final DeviceStatusDAO deviceStatusDAO;

    public UpdateDeviceDelegate() {
        siteBasestationDeviceDAO = BaseStationMapperImpl.getInstance().siteBasestationDeviceDAO();
        siteDeviceBasestationDAO = BaseStationMapperImpl.getInstance().siteDeviceBasestationDAO();
        hwUnitToPointDAO = ConfigMapperImpl.getInstance().hwUnitToPointDAO();
        pointToHwUnitDAO = ConfigMapperImpl.getInstance().pointToHwUnitDAO();
        pointLocationHwUnitHistoryDAO = ConfigMapperImpl.getInstance().pointLocationHwUnitHistoryDAO();
        pointLocationsDAO = ConfigMapperImpl.getInstance().pointLocationsDAO();
        pointsDAO = ConfigMapperImpl.getInstance().pointsDAO();
        subscriptionsDAO = ConfigMapperImpl.getInstance().subscriptionsDAO();
        siteCustomerDAO = ConfigMapperImpl.getInstance().siteCustomerDAO();
        areaSiteDAO = ConfigMapperImpl.getInstance().areaSiteDAO();
        assetSiteAreaDAO = ConfigMapperImpl.getInstance().assetSiteAreaDAO();
        deviceStatusDAO = new DeviceStatusDAO();
    }

    /**
     * Update Rows in Cassandra based on the given object
     *
     * @param siteBasestationDeviceVO, SiteBasestationDeviceVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String updateSiteBasestationDevice(SiteBasestationDeviceVO siteBasestationDeviceVO) throws IllegalArgumentException, Exception {
        List<SiteBasestationDevice> dbSiteBasestationDeviceList;
        SiteBasestationDevice siteBasestationDevice = null;
        String errorMsg;

        // Find the entities based on the given values
        if (siteBasestationDeviceVO != null) {
            try {
                if ((dbSiteBasestationDeviceList = siteBasestationDeviceDAO.findByPK(siteBasestationDeviceVO.getCustomerAccount(), siteBasestationDeviceVO.getSiteId(), siteBasestationDeviceVO.getBaseStationPort(), siteBasestationDeviceVO.getDeviceId(), siteBasestationDeviceVO.getPointId())) != null && !dbSiteBasestationDeviceList.isEmpty()) {
                    siteBasestationDevice = dbSiteBasestationDeviceList.get(0);
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
                return "{\"outcome\":\"Error: Failed to find items to update.\"}";
            }

            // Insert updated entities into Cassandra if original entities are found
            if (siteBasestationDevice != null) {
                siteBasestationDevice.setLastSampled(siteBasestationDeviceVO.getLastSampled());

                try {
                    if ((errorMsg = ServiceUtil.validateObjectData(siteBasestationDevice)) == null) {
                        siteBasestationDeviceDAO.update(siteBasestationDevice);
                    } else {
                        return errorMsg;
                    }
                    return "{\"outcome\":\"Updated SiteBasestationDevice successfully.\"}";
                } catch (IllegalArgumentException e) {
                    throw e;
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    sendEvent(e.getStackTrace());
                }
                return "{\"outcome\":\"Error: Failed to update SiteBasestationDevice.\"}";
            }
            return "{\"outcome\":\"SiteBasestationDevice not found to update.\"}";
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update Rows in Cassandra based on the given object
     *
     * @param hwUnitVO, HwUnitVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String updateHwUnitToPoint(HwUnitVO hwUnitVO) throws IllegalArgumentException, Exception {
        List<HwUnitToPoint> dbHwUnitToPointList;
        HwUnitToPoint dbHwUnitToPoint = null, newHwUnitToPoint;
        String errorMsg;

        // Find the entities based on the given values
        try {
            if (hwUnitVO != null) {
                if (hwUnitVO.getDeviceId() != null && (dbHwUnitToPointList = hwUnitToPointDAO.findByPK(hwUnitVO.getDeviceId(), hwUnitVO.getChannelType(), hwUnitVO.getChannelNum())) != null && !dbHwUnitToPointList.isEmpty()) {
                    dbHwUnitToPoint = dbHwUnitToPointList.get(0);
                }
            } else {
                return "{\"outcome\":\"Error: Invaid input.\"}";
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to update.\"}";
        }

        // Insert updated entities into Cassandra if original entities are found
        if (dbHwUnitToPoint != null) {
            newHwUnitToPoint = DelegateUtil.getHwUnitToPoint(hwUnitVO);

            try {
                if ((errorMsg = ServiceUtil.validateObjectData(newHwUnitToPoint)) == null) {
                    hwUnitToPointDAO.update(newHwUnitToPoint);
                } else {
                    return errorMsg;
                }
                return "{\"outcome\":\"Updated HwUnitToPoint items successfully.\"}";
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
            return "{\"outcome\":\"Error: Failed to update HwUnitToPoint items.\"}";
        }

        return "{\"outcome\":\"HwUnitToPoint items not found to update.\"}";
    }

    /**
     * Update Rows in Cassandra based on the given object
     *
     * @param replaceDeviceVO, DeviceVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String replaceDevice(DeviceVO replaceDeviceVO) throws IllegalArgumentException, Exception {
        final List<PointToHwUnit> upsertPointToHwUnitList, deleteNewDeviceOriginalPointToHwUnitList;
        List<HwUnitToPoint> existingDeviceHwToPointList, newDeviceHwToPointList, upsertHwToPointList, deleteExistDeviceOriginalHwToPointList;
        List<SiteBasestationDevice> deleteExistDeviceOriginalSiteBaseStationDeviceList, deleteNewDeviceOriginalSiteBaseStationDeviceList, createExistDeviceSiteBaseStationDeviceList;
        List<PointLocations> pointLocationsList;
        List<PointVO> pointVOList;
        List<Points> upsertPointsList;
        PointLocations pointLocations, newDevicePointLocations, extDevicePointLocations;
        PointLocationHwUnitHistory insertNewPLHwUnitHistory, insertReplacePLHwUnitHistory;
        List<SiteBasestationDevice> siteBaseStationDeviceList, dbSiteBaseStationDeviceList;
        SiteBasestationDevice siteBasestationDevice;
        List<SiteDeviceBasestation> siteDeviceBaseStationList = null, delSiteDeviceBaseStationList, createSiteDeviceBaseStationList;
        SiteDeviceBasestation siteDeviceBasestation;
        DeviceStatusVO deviceStatusVO;
        List<Subscriptions> subscriptionsList;
        List<SiteCustomer> siteList;
        List<AreaSite> areaList;
        List<AssetSiteArea> assetList;
        List<PointLocations> plList;
        String errorMsg, newDeviceId;
        MQVO mqVO;
        
        try {
            //get the new device Serial #
            newDeviceId = replaceDeviceVO.getNewDeviceId();

            //Step:  update point list, using create method for upserting records.
            pointVOList = replaceDeviceVO.getPointList();

            //STEP 1: get hwToPointList by existing device Serial #
            existingDeviceHwToPointList = hwUnitToPointDAO.findByDevice(replaceDeviceVO.getExistingDeviceId());

            //STEP 2: get hwToPointList by new device Serial #
            newDeviceHwToPointList = hwUnitToPointDAO.findByDevice(newDeviceId);

            if (newDeviceId != null && !newDeviceId.isEmpty()
                    && pointVOList != null && !pointVOList.isEmpty()
                    && existingDeviceHwToPointList != null && !existingDeviceHwToPointList.isEmpty()) {

                deleteExistDeviceOriginalSiteBaseStationDeviceList = new ArrayList();
                deleteNewDeviceOriginalSiteBaseStationDeviceList = new ArrayList();
                createExistDeviceSiteBaseStationDeviceList = new ArrayList();
                deleteNewDeviceOriginalPointToHwUnitList = new ArrayList();
                deleteExistDeviceOriginalHwToPointList = new ArrayList();
                delSiteDeviceBaseStationList = new ArrayList<>();
                createSiteDeviceBaseStationList = new ArrayList<>();
                upsertPointToHwUnitList = new ArrayList();
                upsertHwToPointList = new ArrayList();
                upsertPointsList = new ArrayList();
                newDevicePointLocations = null;
                extDevicePointLocations = null;
                pointLocations = null;
                errorMsg = null;

                //STEP 3: find PointToHwUnit for each pointId exists in existingHwToPointList
                existingDeviceHwToPointList.stream().map(hwUnitToPoint -> {
                    PointToHwUnit pointToHwUnit = new PointToHwUnit();
                    pointToHwUnit.setPointId(hwUnitToPoint.getPointId());
                    pointToHwUnit.setChannelNum(hwUnitToPoint.getChannelNum());
                    pointToHwUnit.setChannelType(hwUnitToPoint.getChannelType());
                    return pointToHwUnit;
                }).map(pointToHwUnit -> {
                    pointToHwUnit.setDeviceId(newDeviceId);
                    return pointToHwUnit;
                }).forEachOrdered(pointToHwUnit -> {
                    upsertPointToHwUnitList.add(pointToHwUnit);
                });

                if (pointLocations == null && (pointLocationsList = pointLocationsDAO.findByPK(pointVOList.get(0).getCustomerAccount(), pointVOList.get(0).getSiteId(), pointVOList.get(0).getAreaId(), pointVOList.get(0).getAssetId(), pointVOList.get(0).getPointLocationId())) != null && !pointLocationsList.isEmpty()) {
                    pointLocations = pointLocationsList.get(0);
                }

                //Step:  update point list, using create method for upserting records.
                //Only upserting points data as there is no scope of changing any other value other than alarm_enabled and auto_acknowledge
                pointVOList.stream().forEachOrdered(pointVO -> {
                    Points points;

                    if (pointVO.getPointId() == null) {
                        pointVO.setPointId(UUID.randomUUID());
                    }

                    points = DelegateUtil.getPoints(pointVO);
                    upsertPointsList.add(points);
                });

                //STEP 4: upsert existingHwToPointList with new deviceSerial.
                existingDeviceHwToPointList.stream().map(hwUnitToPoint -> {
                    HwUnitToPoint newHwUnitToPoint;
                    HwUnitVO hwUnitVO;
                    
                    hwUnitVO = new HwUnitVO(hwUnitToPoint);
                    hwUnitVO.setDeviceId(newDeviceId);
                    
                    //set the new device # to the existing hwUnitToPoint
                    newHwUnitToPoint = new HwUnitToPoint();
                    newHwUnitToPoint.setApSetId(hwUnitToPoint.getApSetId());
                    newHwUnitToPoint.setAreaId(hwUnitToPoint.getAreaId());
                    newHwUnitToPoint.setAssetId(hwUnitToPoint.getAssetId());
                    newHwUnitToPoint.setChannelNum(hwUnitToPoint.getChannelNum());
                    newHwUnitToPoint.setChannelType(hwUnitToPoint.getChannelType());
                    newHwUnitToPoint.setCustomerAcct(hwUnitToPoint.getCustomerAcct());
                    newHwUnitToPoint.setPointId(hwUnitToPoint.getPointId());
                    newHwUnitToPoint.setPointLocationId(hwUnitToPoint.getPointLocationId());
                    newHwUnitToPoint.setSiteId(hwUnitToPoint.getSiteId());
                    return newHwUnitToPoint;
                }).map(newHwUnitToPoint -> {
                    newHwUnitToPoint.setDeviceId(newDeviceId);
                    return newHwUnitToPoint;
                }).forEachOrdered(newHwUnitToPoint -> {
                    upsertHwToPointList.add(newHwUnitToPoint);
                });

                //STEP 5: delete existingHwToPointList. NO:4
                existingDeviceHwToPointList.stream().map(hwUnitToPoint -> {
                    List<HwUnitToPoint> dbHwUnitToPointList;
                    HwUnitToPoint dbHwUnitToPoint = null;
                    
                    if (hwUnitToPoint.getDeviceId() != null && (dbHwUnitToPointList = hwUnitToPointDAO.findByPK(hwUnitToPoint.getDeviceId(), hwUnitToPoint.getChannelType(), hwUnitToPoint.getChannelNum())) != null && !dbHwUnitToPointList.isEmpty()) {
                        dbHwUnitToPoint = dbHwUnitToPointList.get(0);
                    }
                    return dbHwUnitToPoint;
                }).filter(dbHwUnitToPoint -> (dbHwUnitToPoint != null)).forEachOrdered(dbHwUnitToPoint -> {
                    deleteExistDeviceOriginalHwToPointList.add(dbHwUnitToPoint);
                });

                //STEP 6: delete PointToHwUnit, get from newHwToPointList(For new Device Sl #).
                newDeviceHwToPointList.forEach(hwUnitToPoint -> {
                    List<PointToHwUnit> PointToHwUnitList;
                    
                    if ((PointToHwUnitList = pointToHwUnitDAO.findByPK(hwUnitToPoint.getPointId())) != null && !PointToHwUnitList.isEmpty()) {
                        deleteNewDeviceOriginalPointToHwUnitList.add(PointToHwUnitList.get(0));
                    }
                });

                //STEP 7, 8, 9: find extDevicePointLocations from existingHwToPointList, find the SiteBasestationDevice by extDevicePointLocations base_station_port
                for (HwUnitToPoint exHwToPoint : existingDeviceHwToPointList) {
                    
                    
                    if (extDevicePointLocations == null) {
                        if ((pointLocationsList = pointLocationsDAO.findByPK(exHwToPoint.getCustomerAcct(), exHwToPoint.getSiteId(), exHwToPoint.getAreaId(), exHwToPoint.getAssetId(), exHwToPoint.getPointLocationId())) != null && !pointLocationsList.isEmpty()) {
                            if ((extDevicePointLocations = pointLocationsList.get(0)) != null) {
                                extDevicePointLocations.setDeviceSerialNumber(newDeviceId);
                            }
                        }
                    }
                    if (extDevicePointLocations != null &&(dbSiteBaseStationDeviceList = siteBasestationDeviceDAO.findByPK(extDevicePointLocations.getCustomerAccount(), extDevicePointLocations.getSiteId(), extDevicePointLocations.getBasestationPortNum(), exHwToPoint.getDeviceId(), exHwToPoint.getPointId())) != null && !dbSiteBaseStationDeviceList.isEmpty()) {
                        if ((siteBasestationDevice = dbSiteBaseStationDeviceList.get(0)) != null) {
                            deleteExistDeviceOriginalSiteBaseStationDeviceList.add(siteBasestationDevice);
                        }
                    }
                }

                //STEP 10,11 : find newDevicePointLocations from newDeviceHwToPointList, find the SiteBasestationDevice by PointLocations base_station_port
                for (HwUnitToPoint newHwToPoint : newDeviceHwToPointList) {
                    if (newDevicePointLocations == null) {
                        if ((pointLocationsList = pointLocationsDAO.findByPK(newHwToPoint.getCustomerAcct(), newHwToPoint.getSiteId(), newHwToPoint.getAreaId(), newHwToPoint.getAssetId(), newHwToPoint.getPointLocationId())) != null && !pointLocationsList.isEmpty()) {
                            if ((newDevicePointLocations = pointLocationsList.get(0)) != null) {
                                newDevicePointLocations.setDeviceSerialNumber(null);
                            }
                        }
                    }
                    // Find the siteBaseStationDevice and add to the list for deletion.
                    if (newDevicePointLocations != null && (siteBaseStationDeviceList = siteBasestationDeviceDAO.findByPK(newDevicePointLocations.getCustomerAccount(), newDevicePointLocations.getSiteId(), newDevicePointLocations.getBasestationPortNum(), newHwToPoint.getDeviceId(), newHwToPoint.getPointId())) != null && !siteBaseStationDeviceList.isEmpty()) {
                        if ((siteBasestationDevice = siteBaseStationDeviceList.get(0)) != null) {
                            deleteNewDeviceOriginalSiteBaseStationDeviceList.add(siteBasestationDevice);
                        }
                    }
                }

                //SETP 12: find the SiteBasestationDevice by extDevicePointLocations base_station_port
                if (extDevicePointLocations != null) {
                    for (HwUnitToPoint exHwToPoint : existingDeviceHwToPointList) {
                        if ((siteBaseStationDeviceList = siteBasestationDeviceDAO.findByPK(extDevicePointLocations.getCustomerAccount(), extDevicePointLocations.getSiteId(), extDevicePointLocations.getBasestationPortNum(), exHwToPoint.getDeviceId(), exHwToPoint.getPointId())) != null && !siteBaseStationDeviceList.isEmpty()) {
                            siteBasestationDevice = siteBaseStationDeviceList.get(0);
                            siteBasestationDevice.setDeviceId(newDeviceId); //repointLocationsace the existing deviceId with the newDeviceId.
                            createExistDeviceSiteBaseStationDeviceList.add(siteBasestationDevice);
                        } else if (pointLocations != null) {
                            siteBasestationDevice = new SiteBasestationDevice();
                            siteBasestationDevice.setCustomerAccount(pointLocations.getCustomerAccount());
                            siteBasestationDevice.setSiteId(pointLocations.getSiteId());
                            siteBasestationDevice.setBaseStationPort(pointLocations.getBasestationPortNum());
                            siteBasestationDevice.setDeviceId(newDeviceId);
                            siteBasestationDevice.setPointId(exHwToPoint.getPointId());
                            siteBasestationDevice.setChannelType(exHwToPoint.getChannelType());
                            siteBasestationDevice.setSampleInterval(pointLocations.getSampleInterval());

                            //TO DO: 
                            //siteBasestationDevice.setFmax();
                            //siteBasestationDevice.setLastSampled();
                            //siteBasestationDevice.setResolution();
                            //siteBasestationDevice.setSensorChannelNum();
                            createExistDeviceSiteBaseStationDeviceList.add(siteBasestationDevice);
                        }

                        if ((siteDeviceBaseStationList = siteDeviceBasestationDAO.findByPK(extDevicePointLocations.getCustomerAccount(), extDevicePointLocations.getSiteId(), exHwToPoint.getDeviceId())) != null && !siteDeviceBaseStationList.isEmpty()) {
                            if (!delSiteDeviceBaseStationList.contains(siteDeviceBaseStationList.get(0))) {
                                delSiteDeviceBaseStationList.add(siteDeviceBaseStationList.get(0));
                            }
                        } 
                        
                        if (pointLocations != null) {
                            siteDeviceBasestation = new SiteDeviceBasestation();
                            siteDeviceBasestation.setCustomerAccount(pointLocations.getCustomerAccount());
                            siteDeviceBasestation.setSiteId(pointLocations.getSiteId());
                            siteDeviceBasestation.setBaseStationPort(pointLocations.getBasestationPortNum());
                            siteDeviceBasestation.setDeviceId(newDeviceId);
                            if (siteDeviceBaseStationList != null && !siteDeviceBaseStationList.isEmpty()) {
                                siteDeviceBasestation.setDisabled(siteDeviceBaseStationList.get(0).isDisabled());
                            }
                            if (!createSiteDeviceBaseStationList.contains(siteDeviceBasestation)) {
                                createSiteDeviceBaseStationList.add(siteDeviceBasestation);
                            }
                        }
                    }
                }
                
                // upsert, delete operation to the cassandra
                try {
                    //Step:  update point list, using create method for upserting records.
                    if ((!upsertPointsList.isEmpty() && ((errorMsg = ServiceUtil.validateObjects(upsertPointsList, Boolean.TRUE)) == null))) {
                        pointsDAO.create(upsertPointsList);
                    } else {
                        return errorMsg;
                    }
                    //(from STEP 3) upsert upsertPointToHwUnitList in to the cassandra
                    if ((errorMsg = ServiceUtil.validateObjects(upsertPointToHwUnitList, Boolean.TRUE)) == null) {
                        pointToHwUnitDAO.create(upsertPointToHwUnitList);
                    } else {
                        LOGGER.error("Error occured while creating pointToHwUnit : {}", errorMsg);
                        return errorMsg;
                    }
                    //(from STEP 4) upsert upsertHwUnitToPointList in to the cassandra
                    if ((errorMsg = ServiceUtil.validateObjects(upsertHwToPointList, Boolean.TRUE)) == null) {
                        hwUnitToPointDAO.create(upsertHwToPointList);
                    } else {
                        LOGGER.error("Error occured while creating HwToPointLis : {}", errorMsg);
                        return errorMsg;
                    }
                    //(from STEP 5) Delete list of HwUnitToPoint from Cassandra if original entities are found
                    if (!deleteExistDeviceOriginalHwToPointList.isEmpty()) {
                        hwUnitToPointDAO.delete(deleteExistDeviceOriginalHwToPointList);
                    }
                    //(from STEP 6) Delete from Cassandra if original entities are found
                    if (!deleteNewDeviceOriginalPointToHwUnitList.isEmpty()) {
                        pointToHwUnitDAO.delete(deleteNewDeviceOriginalPointToHwUnitList);
                    }
                    //(from STEP 7, 8, 9) Delete siteBaseStationDevice list from Cassandra if original entities are found
                    if (!deleteExistDeviceOriginalSiteBaseStationDeviceList.isEmpty()) {
                        siteBasestationDeviceDAO.delete(deleteExistDeviceOriginalSiteBaseStationDeviceList);
                    }
                    //(from STEP 10, 11) Update newDevicePointLocations in Cassandra using update method for NUll saving.
                    if (newDevicePointLocations != null) {
                        pointLocationsDAO.update(newDevicePointLocations);
                    }
                    //(from STEP 10, 11) Delete siteBaseStationDevice list  from Cassandra if original entities are found.
                    if (!deleteNewDeviceOriginalSiteBaseStationDeviceList.isEmpty()) {
                        siteBasestationDeviceDAO.delete(deleteNewDeviceOriginalSiteBaseStationDeviceList);
                    }
                    
                    if ((errorMsg = ServiceUtil.validateObjects(delSiteDeviceBaseStationList, false)) == null) {
                        delSiteDeviceBaseStationList.forEach(sbsDevice -> siteDeviceBasestationDAO.delete(sbsDevice));
                    } else {
                        LOGGER.error("Error occured while deleting siteDeviceBasestation : {}", errorMsg);
                        return errorMsg;
                    }
                    
                    if ((errorMsg = ServiceUtil.validateObjects(createSiteDeviceBaseStationList, false)) == null) {
                        siteDeviceBasestationDAO.create(createSiteDeviceBaseStationList);
                    } else {
                        LOGGER.error("Error occured while creating siteDeviceBasestation : {}", errorMsg);
                        return errorMsg;
                    }
                    
                    // (from STEP: 12) Update extDevicePointLocations in Cassandra using create method
                    if (extDevicePointLocations != null) {
                        if ((errorMsg = ServiceUtil.validateObjectData(extDevicePointLocations)) == null) {
                            pointLocationsDAO.update(extDevicePointLocations);
                        } else {
                            LOGGER.error("Error occured while creating extDevicePointLocations : {}", errorMsg);
                            return errorMsg;
                        }
                    }

                    //(from STEP: 12) Insert the list of SiteBasestationDevice.
                    if (!createExistDeviceSiteBaseStationDeviceList.isEmpty()) {
                        if ((errorMsg = ServiceUtil.validateObjects(createExistDeviceSiteBaseStationDeviceList, Boolean.TRUE)) == null) {
                            siteBasestationDeviceDAO.create(createExistDeviceSiteBaseStationDeviceList);
                        } else {
                            LOGGER.error("Error occured while upserting createExistDeviceSiteBaseStationDeviceList : {}", errorMsg);
                            return errorMsg;
                        }
                    }

                    //STEP 13: Insert a row into the point_location_hwunit_history table
                    if (pointLocations != null) {
                        insertNewPLHwUnitHistory = new PointLocationHwUnitHistory();
                        insertNewPLHwUnitHistory.setPointLocationId(pointLocations.getPointLocationId());
                        insertNewPLHwUnitHistory.setCustomerAccount(pointLocations.getCustomerAccount());
                        insertNewPLHwUnitHistory.setDeviceId(replaceDeviceVO.getExistingDeviceId());
                        insertNewPLHwUnitHistory.setDateFrom(Instant.now().atZone(ZoneOffset.UTC).toInstant().truncatedTo(ChronoUnit.SECONDS));

                        if ((errorMsg = ServiceUtil.validateObjectData(insertNewPLHwUnitHistory)) == null) {
                            pointLocationHwUnitHistoryDAO.create(insertNewPLHwUnitHistory);
                        } else {
                            LOGGER.info("Error occured while creating PointLocationHwUnitHistory : {}", errorMsg);
                            return errorMsg;
                        }
                    }

                    if (newDevicePointLocations != null) {
                        insertReplacePLHwUnitHistory = new PointLocationHwUnitHistory();
                        insertReplacePLHwUnitHistory.setPointLocationId(newDevicePointLocations.getPointLocationId());
                        insertReplacePLHwUnitHistory.setCustomerAccount(newDevicePointLocations.getCustomerAccount());
                        insertReplacePLHwUnitHistory.setDeviceId(replaceDeviceVO.getNewDeviceId());
                        insertReplacePLHwUnitHistory.setDateFrom(Instant.now().atZone(ZoneOffset.UTC).toInstant().truncatedTo(ChronoUnit.SECONDS));

                        if ((errorMsg = ServiceUtil.validateObjectData(insertReplacePLHwUnitHistory)) == null) {
                            pointLocationHwUnitHistoryDAO.create(insertReplacePLHwUnitHistory);
                        } else {
                            LOGGER.info("Error occured while creating insertReplacePLHwUnitHistory : {}", errorMsg);
                            return errorMsg;
                        }
                        
                        newDeviceHwToPointList.forEach(existHwToPoint -> JsonUtil.createAndSendKafkaMessageForDeleteHwUnit(new HwUnitVO(existHwToPoint)));
                    }
                    //STEP 14: For each row from step #4, Produce a respective dc-point-config or ac-point-config Kafka message
//                    for (HwUnitVO hwUnitVO : exisHwToPoint_withNewDeviceId_list) {
//                        for (PointVO pointVO : pointVOList) {
//                            if (hwUnitVO.getPointId().compareTo(pointVO.getPointId()) == 0) {
//                                if (pointLocations != null && !pointVO.isDisabled()) {
//                                    JsonUtil.createAndSendDeleteMessageToKafka(pointLocations.getDeviceSerialNumber(), pointVO.getPointType(), pointVO.getSensorChannelNum());
//                                }
//                            }
//                        }
//                    }

                    //SETP 14: For each row of step #1, send a delete JSON to the Kafka Proxy Service “/pointconfig” (TT-902) using the below JSON format:
                    existingDeviceHwToPointList.forEach(exiHwToPoint -> JsonUtil.createAndSendKafkaMessageForDeleteHwUnit(new HwUnitVO(exiHwToPoint)));
                    
                    siteList = siteCustomerDAO.findByPK(existingDeviceHwToPointList.get(0).getCustomerAcct(), existingDeviceHwToPointList.get(0).getSiteId());
                    areaList = areaSiteDAO.findByPK(existingDeviceHwToPointList.get(0).getCustomerAcct(), existingDeviceHwToPointList.get(0).getSiteId(), existingDeviceHwToPointList.get(0).getAreaId());
                    assetList = assetSiteAreaDAO.findByPK(existingDeviceHwToPointList.get(0).getCustomerAcct(), existingDeviceHwToPointList.get(0).getSiteId(), existingDeviceHwToPointList.get(0).getAreaId(), existingDeviceHwToPointList.get(0).getAssetId());
                    plList = pointLocationsDAO.findByPK(existingDeviceHwToPointList.get(0).getCustomerAcct(), existingDeviceHwToPointList.get(0).getSiteId(), existingDeviceHwToPointList.get(0).getAreaId(), existingDeviceHwToPointList.get(0).getAssetId(), existingDeviceHwToPointList.get(0).getPointLocationId());
                    // Send MQ Message
                    // Fetch Subscriptions 
                    if ((subscriptionsList = subscriptionsDAO.findByPK(existingDeviceHwToPointList.get(0).getCustomerAcct(), existingDeviceHwToPointList.get(0).getSiteId(), "DEVICE")) != null && !subscriptionsList.isEmpty() &&
                            siteList != null && !siteList.isEmpty() &&
                            areaList != null && !areaList.isEmpty() &&
                            assetList != null && !assetList.isEmpty() &&
                            plList != null && !plList.isEmpty()) {
                        
                        // Populate MQ VO with the details to be posted to MQ for the device being replaced
                        mqVO = new MQVO();
                        mqVO.setCustomer_id(existingDeviceHwToPointList.get(0).getCustomerAcct());
                        mqVO.setSite_id(existingDeviceHwToPointList.get(0).getSiteId().toString());
                        mqVO.setSite_name(siteList.get(0).getSiteName());
                        mqVO.setArea(areaList.get(0).getAreaName());
                        mqVO.setAsset(assetList.get(0).getAssetName());
                        mqVO.setPoint_location(plList.get(0).getPointLocationName());
                        mqVO.setDevice_action("replace");
                        mqVO.setUse_internal(subscriptionsList.get(0).isInternal());
                        mqVO.setMq_queue_name(subscriptionsList.get(0).getMqQueueName());
                        mqVO.setNew_device_id(newDeviceId);
                        mqVO.setOld_device_id(replaceDeviceVO.getExistingDeviceId());
                        
                        if (!subscriptionsList.get(0).isInternal()) {
                            mqVO.setMq_connect_string((subscriptionsList.get(0).getMqConnectString()));
                            mqVO.setMq_user((subscriptionsList.get(0).getMqUser()));
                            mqVO.setMq_pwd((subscriptionsList.get(0).getMqPwd()));
                            mqVO.setMq_client_id((subscriptionsList.get(0).getMqClientId()));
                        }
                        
                        ConfigService.sendMQMessage(JsonConverterUtil.toJSON(mqVO));                                               
                    }
                    
                    if (siteList != null && !siteList.isEmpty())
                        replaceDeviceVO.setSiteName(siteList.get(0).getSiteName());

                    if (areaList != null && !areaList.isEmpty())
                        replaceDeviceVO.setAreaName(areaList.get(0).getAreaName());

                    if (assetList != null && !assetList.isEmpty())
                        replaceDeviceVO.setAssetName(assetList.get(0).getAssetName());
                    
                    if (plList != null && !plList.isEmpty())
                        replaceDeviceVO.setPointLocationName(plList.get(0).getPointLocationName());
                    
                    //Update MySQL device_status
                    deviceStatusVO = DelegateUtil.getDeviceStatus(replaceDeviceVO, replaceDeviceVO.getNewDeviceId());
                    if (siteDeviceBaseStationList != null && !siteDeviceBaseStationList.isEmpty()) {
                        deviceStatusVO.setIsDisabled(siteDeviceBaseStationList.get(0).isDisabled());
                    }
                    deviceStatusDAO.upsert(deviceStatusVO);
                    // Fetch Subscriptions 
                    if (!newDeviceHwToPointList.isEmpty() &&
                            (subscriptionsList = subscriptionsDAO.findByPK(newDeviceHwToPointList.get(0).getCustomerAcct(), newDeviceHwToPointList.get(0).getSiteId(), "DEVICE")) != null && !subscriptionsList.isEmpty() &&
                            (siteList = siteCustomerDAO.findByPK(newDeviceHwToPointList.get(0).getCustomerAcct(), newDeviceHwToPointList.get(0).getSiteId())) != null && !siteList.isEmpty() &&
                            (areaList = areaSiteDAO.findByPK(newDeviceHwToPointList.get(0).getCustomerAcct(), newDeviceHwToPointList.get(0).getSiteId(), newDeviceHwToPointList.get(0).getAreaId())) != null && !areaList.isEmpty() &&
                            (assetList = assetSiteAreaDAO.findByPK(newDeviceHwToPointList.get(0).getCustomerAcct(), newDeviceHwToPointList.get(0).getSiteId(), newDeviceHwToPointList.get(0).getAreaId(), newDeviceHwToPointList.get(0).getAssetId())) != null && !assetList.isEmpty() &&
                            (plList = pointLocationsDAO.findByPK(newDeviceHwToPointList.get(0).getCustomerAcct(), newDeviceHwToPointList.get(0).getSiteId(), newDeviceHwToPointList.get(0).getAreaId(), newDeviceHwToPointList.get(0).getAssetId(), newDeviceHwToPointList.get(0).getPointLocationId())) != null && !plList.isEmpty()) {
                        
                        // Populate MQ VO with the details to be posted to MQ for the new device which was removed from another point
                        mqVO = new MQVO();
                        mqVO.setCustomer_id(newDeviceHwToPointList.get(0).getCustomerAcct());
                        mqVO.setSite_id(newDeviceHwToPointList.get(0).getSiteId().toString());
                        mqVO.setSite_name(siteList.get(0).getSiteName());
                        mqVO.setArea(areaList.get(0).getAreaName());
                        mqVO.setAsset(assetList.get(0).getAssetName());
                        mqVO.setPoint_location(plList.get(0).getPointLocationName());
                        mqVO.setDevice_action("remove");
                        mqVO.setUse_internal(subscriptionsList.get(0).isInternal());
                        mqVO.setMq_queue_name(subscriptionsList.get(0).getMqQueueName());
                        mqVO.setDevice_id(newDeviceId);
                        
                        if (!subscriptionsList.get(0).isInternal()) {
                            mqVO.setMq_connect_string((subscriptionsList.get(0).getMqConnectString()));
                            mqVO.setMq_user((subscriptionsList.get(0).getMqUser()));
                            mqVO.setMq_pwd((subscriptionsList.get(0).getMqPwd()));
                            mqVO.setMq_client_id((subscriptionsList.get(0).getMqClientId()));
                        }
                        
                        ConfigService.sendMQMessage(JsonConverterUtil.toJSON(mqVO));
                    }
                    //Update MySQL device_status
                    deviceStatusDAO.clearDevice(replaceDeviceVO.getExistingDeviceId());
                } catch (IllegalArgumentException e) {
                    throw e;
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    sendEvent(e.getStackTrace());
                    return "{\"outcome\":\"issue with replace device.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient data found.\"}";
            }

            return "{\"outcome\":\"Device replaced successfully.\"}";

        } catch (ReadTimeoutException | UnavailableException | IllegalArgumentException e) {
            LOGGER.error(e.getMessage(), e);
            return "{\"outcome\":\"Error: Failed to replace device.\"}";
        }
    }

    /**
     * Update Rows in Cassandra based on the given object
     *
     * @param enableDeviceVO, DeviceVO object
     * @return String object
     */
    public String enableDevice(DeviceVO enableDeviceVO) {
        List<HwUnitToPoint> existingDeviceHwToPointList;
        List<SiteBasestationDevice> dbSiteBaseStationDeviceList, updSiteBaseStationDeviceList;
        List<SiteDeviceBasestation> dbDeviceSiteBaseStationList, updSiteDeviceBaseStationList;
        List<PointLocations> dbPointLocationsList;
        HashMap<UUID, PointLocations> plMap;
        PointLocations dbPointLocations = null;
        DeviceStatusVO deviceStatusVO;
        SiteBasestationDevice siteBasestationDevice;
        SiteDeviceBasestation siteDeviceBasestation;
        List<Subscriptions> subscriptionsList;
        List<SiteCustomer> siteList;
        List<AreaSite> areaList;
        List<AssetSiteArea> assetList;
        String errorMsg;
        MQVO mqVO;

        try {
            // get hwToPointList by device Serial #
            existingDeviceHwToPointList = hwUnitToPointDAO.findByDevice(enableDeviceVO.getExistingDeviceId());

            if (existingDeviceHwToPointList != null && !existingDeviceHwToPointList.isEmpty()) {
                updSiteBaseStationDeviceList = new ArrayList();
                updSiteDeviceBaseStationList = new ArrayList();
                plMap = new HashMap<>();

                for (HwUnitToPoint hwPointVO : existingDeviceHwToPointList) {
                    dbPointLocations = null;

                    if (plMap.isEmpty() || (plMap.get(hwPointVO.getPointLocationId()) == null)) {
                        if ((dbPointLocationsList = pointLocationsDAO.findByPK(hwPointVO.getCustomerAcct(), hwPointVO.getSiteId(), hwPointVO.getAreaId(), hwPointVO.getAssetId(), hwPointVO.getPointLocationId())) != null && !dbPointLocationsList.isEmpty()) {
                            dbPointLocations = dbPointLocationsList.get(0);
                        }
                    } else {
                        if (!plMap.isEmpty()) {
                            dbPointLocations = plMap.get(hwPointVO.getPointLocationId());
                        }
                    }

                    if (dbPointLocations != null) {
                        dbSiteBaseStationDeviceList = siteBasestationDeviceDAO.findByPK(hwPointVO.getCustomerAcct(), hwPointVO.getSiteId(), dbPointLocations.getBasestationPortNum(), hwPointVO.getDeviceId(), hwPointVO.getPointId());
                        if (dbSiteBaseStationDeviceList != null && !dbSiteBaseStationDeviceList.isEmpty()) {
                            siteBasestationDevice = new SiteBasestationDevice(dbSiteBaseStationDeviceList.get(0));
                            siteBasestationDevice.setDisabled(false);
                            updSiteBaseStationDeviceList.add(siteBasestationDevice);
                        }
                        dbDeviceSiteBaseStationList = siteDeviceBasestationDAO.findByPK(hwPointVO.getCustomerAcct(), hwPointVO.getSiteId(), hwPointVO.getDeviceId());
                        if (dbDeviceSiteBaseStationList != null && !dbDeviceSiteBaseStationList.isEmpty()) {
                            siteDeviceBasestation = new SiteDeviceBasestation(dbDeviceSiteBaseStationList.get(0));
                            siteDeviceBasestation.setDisabled(false);
                            if (!updSiteDeviceBaseStationList.contains(siteDeviceBasestation))
                                updSiteDeviceBaseStationList.add(siteDeviceBasestation);
                        }
                    }
                }

                // upsert operation to the cassandra
                try {
                    if (((errorMsg = ServiceUtil.validateObjects(updSiteBaseStationDeviceList, Boolean.FALSE)) == null) 
                            && ((errorMsg = ServiceUtil.validateObjects(updSiteDeviceBaseStationList, Boolean.FALSE)) == null)) {
                        if (!updSiteBaseStationDeviceList.isEmpty()) {
                            siteBasestationDeviceDAO.create(updSiteBaseStationDeviceList);
                        }
                        if (!updSiteDeviceBaseStationList.isEmpty()) {
                            siteDeviceBasestationDAO.create(updSiteDeviceBaseStationList);
                        }

                    } else {
                        return errorMsg;
                    }

                    existingDeviceHwToPointList.forEach(exiHwToPoint -> JsonUtil.createAndSendKafkaMessageForDeleteHwUnit(new HwUnitVO(exiHwToPoint)));
                    
                    siteList = siteCustomerDAO.findByPK(existingDeviceHwToPointList.get(0).getCustomerAcct(), existingDeviceHwToPointList.get(0).getSiteId());
                    areaList = areaSiteDAO.findByPK(existingDeviceHwToPointList.get(0).getCustomerAcct(), existingDeviceHwToPointList.get(0).getSiteId(), existingDeviceHwToPointList.get(0).getAreaId());
                    assetList = assetSiteAreaDAO.findByPK(existingDeviceHwToPointList.get(0).getCustomerAcct(), existingDeviceHwToPointList.get(0).getSiteId(), existingDeviceHwToPointList.get(0).getAreaId(), existingDeviceHwToPointList.get(0).getAssetId());

                    // Send MQ Message
                    // Fetch Subscriptions 
                    if (dbPointLocations != null &&
                            siteList != null && !siteList.isEmpty() &&
                            areaList != null && !areaList.isEmpty() &&
                            assetList != null && !assetList.isEmpty() &&
                            (subscriptionsList = subscriptionsDAO.findByPK(existingDeviceHwToPointList.get(0).getCustomerAcct(), existingDeviceHwToPointList.get(0).getSiteId(), "DEVICE")) != null && !subscriptionsList.isEmpty()) {
                        
                        // Populate MQ VO with the details to be posted to MQ
                        mqVO = new MQVO();
                        mqVO.setCustomer_id(existingDeviceHwToPointList.get(0).getCustomerAcct());
                        mqVO.setSite_id(existingDeviceHwToPointList.get(0).getSiteId().toString());
                        mqVO.setSite_name(siteList.get(0).getSiteName());
                        mqVO.setArea(areaList.get(0).getAreaName());
                        mqVO.setAsset(assetList.get(0).getAssetName());
                        mqVO.setPoint_location(dbPointLocations.getPointLocationName());
                        mqVO.setDevice_id(existingDeviceHwToPointList.get(0).getDeviceId());
                        mqVO.setDevice_action("enable");
                        mqVO.setUse_internal(subscriptionsList.get(0).isInternal());
                        mqVO.setMq_queue_name(subscriptionsList.get(0).getMqQueueName());

                        if (!subscriptionsList.get(0).isInternal()) {
                            mqVO.setMq_connect_string((subscriptionsList.get(0).getMqConnectString()));
                            mqVO.setMq_user((subscriptionsList.get(0).getMqUser()));
                            mqVO.setMq_pwd((subscriptionsList.get(0).getMqPwd()));
                            mqVO.setMq_client_id((subscriptionsList.get(0).getMqClientId()));
                        }
                        
                        ConfigService.sendMQMessage(JsonConverterUtil.toJSON(mqVO));
                    }
                    if (enableDeviceVO.getCustomerAccount() == null || enableDeviceVO.getCustomerAccount().isEmpty()) 
                        enableDeviceVO.setCustomerAccount(existingDeviceHwToPointList.get(0).getCustomerAcct());
                    if ((enableDeviceVO.getSiteName() == null || enableDeviceVO.getSiteName().isEmpty())
                        && siteList != null && !siteList.isEmpty())
                        enableDeviceVO.setSiteName(siteList.get(0).getSiteName());

                    if ((enableDeviceVO.getAreaName() == null || enableDeviceVO.getAreaName().isEmpty())
                            && areaList != null && !areaList.isEmpty())
                        enableDeviceVO.setAreaName(areaList.get(0).getAreaName());

                    if ((enableDeviceVO.getAssetName() == null || enableDeviceVO.getAssetName().isEmpty())
                            && assetList != null && !assetList.isEmpty())
                        enableDeviceVO.setAssetName(assetList.get(0).getAssetName());
                    if (dbPointLocations != null)
                        enableDeviceVO.setPointLocationName(dbPointLocations.getPointLocationName());
                    //Update MySQL device_status
                    deviceStatusVO = DelegateUtil.getDeviceStatus(enableDeviceVO, enableDeviceVO.getExistingDeviceId());
                    deviceStatusVO.setIsDisabled(false);
                    deviceStatusDAO.upsert(deviceStatusVO);
                } catch (IllegalArgumentException e) {
                    throw e;
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    sendEvent(e.getStackTrace());
                    return "{\"outcome\":\"Error: issue with enable device.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient data found.\"}";
            }

            return "{\"outcome\":\"Device enabled successfully.\"}";

        } catch (ReadTimeoutException | UnavailableException | IllegalArgumentException e) {
            LOGGER.error(e.getMessage(), e);
            return "{\"outcome\":\"Error: Failed to enable device.\"}";
        }
    }

    /**
     * Update Rows in Cassandra based on the given object
     *
     * @param disableDeviceVO, DeviceVO object
     * @return String object
     */
    public String disableDevice(DeviceVO disableDeviceVO) {
        List<HwUnitToPoint> existingDeviceHwToPointList;
        List<SiteBasestationDevice> updSiteBaseStationDeviceList, dbSiteBaseStationDeviceList;
        List<SiteDeviceBasestation> dbDeviceSiteBaseStationList, updSiteDeviceBaseStationList;
        List<PointLocations> dbPointLocationsList;
        PointLocations dbPointLocations = null;
        DeviceStatusVO deviceStatusVO;
        SiteBasestationDevice siteBasestationDevice;
        SiteDeviceBasestation siteDeviceBasestation;
        List<Subscriptions> subscriptionsList;
        List<SiteCustomer> siteList;
        List<AreaSite> areaList;
        List<AssetSiteArea> assetList;
        String errorMsg;
        MQVO mqVO;

        try {
            //STEP 1: get hwToPointList by device Serial #
            existingDeviceHwToPointList = hwUnitToPointDAO.findByDevice(disableDeviceVO.getExistingDeviceId());

            if (existingDeviceHwToPointList != null && !existingDeviceHwToPointList.isEmpty()) {
                updSiteBaseStationDeviceList = new ArrayList();
                updSiteDeviceBaseStationList = new ArrayList();

                for (HwUnitToPoint hwPointVO : existingDeviceHwToPointList) {
                    dbPointLocations = null;

                    if ((dbPointLocationsList = pointLocationsDAO.findByPK(hwPointVO.getCustomerAcct(), hwPointVO.getSiteId(), hwPointVO.getAreaId(), hwPointVO.getAssetId(), hwPointVO.getPointLocationId())) != null && !dbPointLocationsList.isEmpty()) {
                        dbPointLocations = dbPointLocationsList.get(0);
                    }
                    
                    if (dbPointLocations != null) {
                        dbSiteBaseStationDeviceList = siteBasestationDeviceDAO.findByPK(hwPointVO.getCustomerAcct(), hwPointVO.getSiteId(), dbPointLocations.getBasestationPortNum(), hwPointVO.getDeviceId(), hwPointVO.getPointId());
                        if (dbSiteBaseStationDeviceList != null && !dbSiteBaseStationDeviceList.isEmpty()) {
                            siteBasestationDevice = new SiteBasestationDevice(dbSiteBaseStationDeviceList.get(0));
                            siteBasestationDevice.setDisabled(true);
                            updSiteBaseStationDeviceList.add(siteBasestationDevice);
                        }
                        dbDeviceSiteBaseStationList = siteDeviceBasestationDAO.findByPK(hwPointVO.getCustomerAcct(), hwPointVO.getSiteId(), hwPointVO.getDeviceId());
                        if (dbDeviceSiteBaseStationList != null && !dbDeviceSiteBaseStationList.isEmpty()) {
                            siteDeviceBasestation = new SiteDeviceBasestation(dbDeviceSiteBaseStationList.get(0));
                            siteDeviceBasestation.setDisabled(true);
                            if (!updSiteDeviceBaseStationList.contains(siteDeviceBasestation))
                                updSiteDeviceBaseStationList.add(siteDeviceBasestation);
                        }
                    }
                }

                // upsert operation to the cassandra
                try {
                    if (((errorMsg = ServiceUtil.validateObjects(updSiteBaseStationDeviceList, Boolean.TRUE)) == null)
                            && ((errorMsg = ServiceUtil.validateObjects(updSiteDeviceBaseStationList, Boolean.TRUE)) == null)) {
                        if (!updSiteBaseStationDeviceList.isEmpty()) {
                            siteBasestationDeviceDAO.create(updSiteBaseStationDeviceList);
                        }
                        
                        if (!updSiteDeviceBaseStationList.isEmpty()) {
                            siteDeviceBasestationDAO.create(updSiteDeviceBaseStationList);
                        }
                    } else {
                        return errorMsg;
                    }

                    existingDeviceHwToPointList.forEach(exiHwToPoint -> JsonUtil.createAndSendKafkaMessageForDeleteHwUnit(new HwUnitVO(exiHwToPoint)));
                    
                    siteList = siteCustomerDAO.findByPK(existingDeviceHwToPointList.get(0).getCustomerAcct(), existingDeviceHwToPointList.get(0).getSiteId());
                    areaList = areaSiteDAO.findByPK(existingDeviceHwToPointList.get(0).getCustomerAcct(), existingDeviceHwToPointList.get(0).getSiteId(), existingDeviceHwToPointList.get(0).getAreaId());
                    assetList = assetSiteAreaDAO.findByPK(existingDeviceHwToPointList.get(0).getCustomerAcct(), existingDeviceHwToPointList.get(0).getSiteId(), existingDeviceHwToPointList.get(0).getAreaId(), existingDeviceHwToPointList.get(0).getAssetId());

                    // Send MQ Message
                    // Fetch Subscriptions 
                    if (dbPointLocations != null &&
                            siteList != null && !siteList.isEmpty() &&
                            areaList != null && !areaList.isEmpty() &&
                            assetList != null && !assetList.isEmpty() &&
                            (subscriptionsList = subscriptionsDAO.findByPK(existingDeviceHwToPointList.get(0).getCustomerAcct(), existingDeviceHwToPointList.get(0).getSiteId(), "DEVICE")) != null && !subscriptionsList.isEmpty()) {
                        
                        // Populate MQ VO with the details to be posted to MQ
                        mqVO = new MQVO();
                        mqVO.setCustomer_id(existingDeviceHwToPointList.get(0).getCustomerAcct());
                        mqVO.setSite_id(existingDeviceHwToPointList.get(0).getSiteId().toString());
                        mqVO.setSite_name(siteList.get(0).getSiteName());
                        mqVO.setArea(areaList.get(0).getAreaName());
                        mqVO.setAsset(assetList.get(0).getAssetName());
                        mqVO.setPoint_location(dbPointLocations.getPointLocationName());
                        mqVO.setDevice_id(existingDeviceHwToPointList.get(0).getDeviceId());
                        mqVO.setDevice_action("disable");
                        mqVO.setUse_internal(subscriptionsList.get(0).isInternal());
                        mqVO.setMq_queue_name(subscriptionsList.get(0).getMqQueueName());
                        
                        if (!subscriptionsList.get(0).isInternal()) {
                            mqVO.setMq_connect_string((subscriptionsList.get(0).getMqConnectString()));
                            mqVO.setMq_user((subscriptionsList.get(0).getMqUser()));
                            mqVO.setMq_pwd((subscriptionsList.get(0).getMqPwd()));
                            mqVO.setMq_client_id((subscriptionsList.get(0).getMqClientId()));
                        }
                            
                        ConfigService.sendMQMessage(JsonConverterUtil.toJSON(mqVO));
                    }        
                    if (disableDeviceVO.getCustomerAccount() == null || disableDeviceVO.getCustomerAccount().isEmpty()) 
                        disableDeviceVO.setCustomerAccount(existingDeviceHwToPointList.get(0).getCustomerAcct());
                    if ((disableDeviceVO.getSiteName() == null || disableDeviceVO.getSiteName().isEmpty())
                        && siteList != null && !siteList.isEmpty())
                        disableDeviceVO.setSiteName(siteList.get(0).getSiteName());

                    if ((disableDeviceVO.getAreaName() == null || disableDeviceVO.getAreaName().isEmpty())
                            && areaList != null && !areaList.isEmpty())
                        disableDeviceVO.setAreaName(areaList.get(0).getAreaName());

                    if ((disableDeviceVO.getAssetName() == null || disableDeviceVO.getAssetName().isEmpty())
                            && assetList != null && !assetList.isEmpty())
                        disableDeviceVO.setAssetName(assetList.get(0).getAssetName());
                    if (dbPointLocations != null)
                        disableDeviceVO.setPointLocationName(dbPointLocations.getPointLocationName());
                    //Update MySQL device_status
                    deviceStatusVO = DelegateUtil.getDeviceStatus(disableDeviceVO, disableDeviceVO.getExistingDeviceId());
                    deviceStatusVO.setIsDisabled(true);
                    deviceStatusDAO.upsert(deviceStatusVO);
                } catch (IllegalArgumentException e) {
                    throw e;
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    sendEvent(e.getStackTrace());
                    return "{\"outcome\":\"Error: issue with disable device.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient data found.\"}";
            }

            return "{\"outcome\":\"Device disabled successfully.\"}";

        } catch (ReadTimeoutException | UnavailableException | IllegalArgumentException e) {
            LOGGER.error(e.getMessage(), e);
            return "{\"outcome\":\"Error: Failed to disable device.\"}";
        }
    }

    /**
     * Update Rows in Cassandra based on the given object
     *
     * @param deviceStatusVO, DeviceStatusVO object
     * @return String object
     */
    public String updateDeviceStatus(DeviceStatusVO deviceStatusVO) {

        try {
            deviceStatusDAO.updateLastSample(deviceStatusVO);
            return "{\"outcome\":\"Device status updated successfully.\"}";

        } catch (ReadTimeoutException | UnavailableException | IllegalArgumentException e) {
            LOGGER.error(e.getMessage(), e);
            return "{\"outcome\":\"Error: Failed to update device status.\"}";
        }
    }

}
