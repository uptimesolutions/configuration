/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.pointtohwunit;

import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.PointToHwUnitDAO;
import com.uptime.cassandra.config.entity.PointToHwUnit;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.utils.DelegateUtil;
import com.uptime.config.vo.PointToHwUnitVO;
import com.uptime.services.util.ServiceUtil;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class CreatePointToHwUnitDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreatePointToHwUnitDelegate.class.getName());
    private final PointToHwUnitDAO pointToHwUnitDAO;

    public CreatePointToHwUnitDelegate() {
        pointToHwUnitDAO = ConfigMapperImpl.getInstance().pointToHwUnitDAO();
    }

    /**
     * Insert new Rows into Cassandra based on the given object
     *
     * @param pointToHwUnitVO, PointToHwUnitVO object
     * @return String object
     * @throws java.lang.IllegalArgumentException
     */
    public String createPointToHwUnit(PointToHwUnitVO pointToHwUnitVO) throws IllegalArgumentException {
        PointToHwUnit pointToHwUnit;
        String errorMsg;
        
        if (pointToHwUnitVO != null) {
            if (pointToHwUnitVO.getPointId() == null) {
                pointToHwUnitVO.setPointId(UUID.randomUUID());
            }
            pointToHwUnit = DelegateUtil.getPointToHwUnit(pointToHwUnitVO);

            // Insert the entities into Cassandra
            try {
                if ((errorMsg = ServiceUtil.validateObjectData(pointToHwUnit)) == null) {
                    pointToHwUnitDAO.create(pointToHwUnit);
                    return "{\"outcome\":\"New PointToHwUnit created / updated successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }

        return "{\"outcome\":\"Error: Failed to create new PointToHwUnit.\"}";
    }

}
