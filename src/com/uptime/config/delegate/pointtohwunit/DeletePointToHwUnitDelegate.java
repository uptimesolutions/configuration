/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.pointtohwunit;

import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.PointToHwUnitDAO;
import com.uptime.cassandra.config.entity.PointToHwUnit;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.vo.PointToHwUnitVO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class DeletePointToHwUnitDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeletePointToHwUnitDelegate.class.getName());
    private final PointToHwUnitDAO pointToHwUnitDAO;
    
    public DeletePointToHwUnitDelegate(){
        pointToHwUnitDAO = ConfigMapperImpl.getInstance().pointToHwUnitDAO();
    }
    
    /**
     * Delete Rows in Cassandra based on the given object
     *
     * @param pointToHwUnitVO, HwUnitVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deletePointToHwUnit(PointToHwUnitVO pointToHwUnitVO) throws IllegalArgumentException {
        List<PointToHwUnit> dbPointToHwUnitList;
        PointToHwUnit dbPointToHwUnit = null;

        // Find the entities based on the given values
        try {
            if (pointToHwUnitVO != null) {
                if (pointToHwUnitVO.getPointId() != null && (dbPointToHwUnitList = pointToHwUnitDAO.findByPK(pointToHwUnitVO.getPointId())) != null && !dbPointToHwUnitList.isEmpty()) {
                    dbPointToHwUnit = dbPointToHwUnitList.get(0);
                }
            }
            else {
                return "{\"outcome\":\"Error: Invaid input.\"}";
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to delete.\"}";
        }

        // Delete entities from Cassandra if original entities are found
        if (dbPointToHwUnit != null) {
            try {
                pointToHwUnitDAO.delete(dbPointToHwUnit);
                return "{\"outcome\":\"Deleted PointToHwUnit items successfully.\"}";
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
            return "{\"outcome\":\"Error: Failed to delete PointToHwUnit items.\"}";
        }

        return "{\"outcome\":\"PointToHwUnit items not found to delete.\"}";
    }

    
}
