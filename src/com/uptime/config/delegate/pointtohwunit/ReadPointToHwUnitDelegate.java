/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.pointtohwunit;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.PointToHwUnitDAO;
import com.uptime.cassandra.config.entity.PointToHwUnit;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class ReadPointToHwUnitDelegate {
    
    private final PointToHwUnitDAO pointToHwUnitDAO;
    
    public ReadPointToHwUnitDelegate(){
        pointToHwUnitDAO = ConfigMapperImpl.getInstance().pointToHwUnitDAO();
    }
    
    /**
     * Return a List of PointToHwUnit Objects by the given params
     * @param pointId, UUID Object
     * @return List of PointToHwUnit Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<PointToHwUnit> getPointToHwUnitByPoint(UUID pointId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return pointToHwUnitDAO.findByPK(pointId);
    }
    
}
