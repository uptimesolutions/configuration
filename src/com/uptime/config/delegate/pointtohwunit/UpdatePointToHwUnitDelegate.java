/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.pointtohwunit;

import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.PointToHwUnitDAO;
import com.uptime.cassandra.config.entity.PointToHwUnit;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.utils.DelegateUtil;
import com.uptime.config.vo.PointToHwUnitVO;
import com.uptime.services.util.ServiceUtil;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class UpdatePointToHwUnitDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdatePointToHwUnitDelegate.class.getName());
    private final PointToHwUnitDAO pointToHwUnitDAO;
    
    public UpdatePointToHwUnitDelegate(){
        pointToHwUnitDAO = ConfigMapperImpl.getInstance().pointToHwUnitDAO();
    }
    
     /**
     * Update Rows in Cassandra based on the given object
     *
     * @param pointToHwUnitVO, PointToHwUnitVO object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updatePointToHwUnit(PointToHwUnitVO pointToHwUnitVO) throws IllegalArgumentException {
        String errorMsg;
        PointToHwUnit newPointToHwUnit;

        // Insert updated entities into Cassandra 
        newPointToHwUnit = DelegateUtil.getPointToHwUnit(pointToHwUnitVO);

        try { 
            if ((errorMsg = ServiceUtil.validateObjectData(newPointToHwUnit)) == null) {
                pointToHwUnitDAO.update(newPointToHwUnit);
                return "{\"outcome\":\"Updated PointToHwUnit items successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }  
        return "{\"outcome\":\"Error: Failed to update PointToHwUnit items.\"}";
    }
    
}
