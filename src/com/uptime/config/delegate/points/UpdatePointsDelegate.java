/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.points;

import com.uptime.cassandra.config.dao.ApAlByPointDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.HwUnitToPointDAO;
import com.uptime.cassandra.config.dao.PointLocationsDAO;
import com.uptime.cassandra.config.dao.PointToHwUnitDAO;
import com.uptime.cassandra.config.dao.PointsDAO;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.cassandra.config.entity.HwUnitToPoint;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.cassandra.config.entity.PointToHwUnit;
import com.uptime.cassandra.config.entity.Points;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.dao.SiteBasestationDeviceDAO;
import com.uptime.cassandra.monitor.dao.SiteDeviceBasestationDAO;
import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import com.uptime.cassandra.monitor.entity.SiteDeviceBasestation;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.utils.DelegateUtil;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.PointVO;
import com.uptime.services.util.ServiceUtil;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class UpdatePointsDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdatePointsDelegate.class.getName());
    private final PointsDAO pointsDAO;
    private final ApAlByPointDAO apAlByPointDAO;
    private final PointLocationsDAO pointLocationsDAO;
    private final HwUnitToPointDAO hwUnitToPointDAO;
    private final PointToHwUnitDAO pointToHwUnitDAO;
    private final SiteDeviceBasestationDAO siteDeviceBasestationDAO;
    private final SiteBasestationDeviceDAO siteBasestationDeviceDAO;
    
    /**
     * Constructor
     */
    public UpdatePointsDelegate() {
        pointsDAO = ConfigMapperImpl.getInstance().pointsDAO();
        apAlByPointDAO = ConfigMapperImpl.getInstance().apAlByPointDAO();
        pointLocationsDAO = ConfigMapperImpl.getInstance().pointLocationsDAO();
        hwUnitToPointDAO = ConfigMapperImpl.getInstance().hwUnitToPointDAO();
        pointToHwUnitDAO = ConfigMapperImpl.getInstance().pointToHwUnitDAO();
        siteDeviceBasestationDAO = BaseStationMapperImpl.getInstance().siteDeviceBasestationDAO();
        siteBasestationDeviceDAO = BaseStationMapperImpl.getInstance().siteBasestationDeviceDAO();
    }

    /**
     * Update Rows in Cassandra based on the given object
     *
     * @param pointVO, PointVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    private String updatePoints(PointVO pointVO) throws IllegalArgumentException, Exception {
        List<ApAlByPoint> updatedApAlByPoint = null;
        List<PointLocations> pointLocationsList;
        List<PointToHwUnit> pointToHwUnitList;
        List<HwUnitToPoint> dbHwUnitToPointList;
        HwUnitToPoint dbHwUnitToPoint = null;
        List<SiteDeviceBasestation> dbDeviceSiteBaseStationList;
        List<SiteBasestationDevice> dbSiteBaseStationDeviceList;
        SiteBasestationDevice siteBasestationDevice = null;
        Points newPoints;
        String errorMsg;
        
        if (pointVO != null) {

            // Insert updated entities into Cassandra 
            newPoints = DelegateUtil.getPoints(pointVO);

            if (pointVO.getApAlSetVOs() != null && !pointVO.getApAlSetVOs().isEmpty()) {
                updatedApAlByPoint = DelegateUtil.getApAlByPointList(pointVO);
            }
                  
            if (!pointVO.isDisabled()) {
                if ((pointToHwUnitList = pointToHwUnitDAO.findByPK(pointVO.getPointId())) != null && !pointToHwUnitList.isEmpty()) {
                    if (pointToHwUnitList.get(0).getDeviceId() != null && (dbHwUnitToPointList = hwUnitToPointDAO.findByPK(pointToHwUnitList.get(0).getDeviceId(), pointToHwUnitList.get(0).getChannelType(), pointToHwUnitList.get(0).getChannelNum())) != null && !dbHwUnitToPointList.isEmpty()) {
                        dbHwUnitToPoint = dbHwUnitToPointList.get(0);
                        dbHwUnitToPoint.setApSetId(pointVO.getApSetId());
                    }
                    if (pointVO.getApAlSetVOs() != null && !pointVO.getApAlSetVOs().isEmpty()) {
                        if ((dbDeviceSiteBaseStationList = siteDeviceBasestationDAO.findByPK(pointVO.getCustomerAccount(), pointVO.getSiteId(), pointToHwUnitList.get(0).getDeviceId())) != null && !dbDeviceSiteBaseStationList.isEmpty()) {
                            if ((dbSiteBaseStationDeviceList = siteBasestationDeviceDAO.findByPK(pointVO.getCustomerAccount(), pointVO.getSiteId(), dbDeviceSiteBaseStationList.get(0).getBaseStationPort(), dbDeviceSiteBaseStationList.get(0).getDeviceId(), pointVO.getPointId())) != null && !dbSiteBaseStationDeviceList.isEmpty()) {
                                siteBasestationDevice = dbSiteBaseStationDeviceList.get(0);
                                siteBasestationDevice.setFmax(pointVO.getApAlSetVOs().get(0).getFmax());
                                siteBasestationDevice.setResolution(pointVO.getApAlSetVOs().get(0).getResolution());
                            }
                        }
                    }
                }
                
                
            }
            
            try { 
                if (((errorMsg = ServiceUtil.validateObjectData(newPoints)) == null) &&
                        ((errorMsg = ServiceUtil.validateObjects(updatedApAlByPoint, Boolean.TRUE)) == null) &&
                        (dbHwUnitToPoint == null || (errorMsg = ServiceUtil.validateObjectData(dbHwUnitToPoint)) == null) &&
                        (siteBasestationDevice == null || (errorMsg = ServiceUtil.validateObjectData(siteBasestationDevice)) == null)) {
                    pointsDAO.update(newPoints);

                    if (updatedApAlByPoint != null && !updatedApAlByPoint.isEmpty()) {
                        apAlByPointDAO.update(updatedApAlByPoint);
                    }

                    pointLocationsList = pointLocationsDAO.findByPK(pointVO.getCustomerAccount(), pointVO.getSiteId(), pointVO.getAreaId(), pointVO.getAssetId(), pointVO.getPointLocationId());
                    
                    if (dbHwUnitToPoint != null){
                        hwUnitToPointDAO.update(dbHwUnitToPoint);
                    }
                    
                    if (siteBasestationDevice != null){
                        siteBasestationDeviceDAO.update(siteBasestationDevice);
                    }
                    
                    if (pointLocationsList != null && !pointLocationsList.isEmpty() && !pointVO.isDisabled()) {
                        JsonUtil.createAndSendDeleteMessageToKafka(pointLocationsList.get(0).getDeviceSerialNumber(), pointVO.getPointType(), pointVO.getSensorChannelNum());
                    }
                    return "{\"outcome\":\"Updated Points successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            } 
            return "{\"outcome\":\"Error: Failed to update Points.\"}";
        }
        return "{\"outcome\":\"Error: Invaid input.\"}";
    }

    /**
     * Update Rows in Cassandra based on the given object
     *
     * @param pointVOList, List of PointVO objects
     * @return String object
     * @throws IllegalArgumentException
     */
    public String updatePoints(List<PointVO> pointVOList) throws IllegalArgumentException {
        try {
            for (PointVO pointVO : pointVOList) {
                // Update Cassandra
                updatePoints(pointVO);
            }
            return "{\"outcome\":\"Updated Points successfully.\"}";
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to update Points.\"}";
    }
}
