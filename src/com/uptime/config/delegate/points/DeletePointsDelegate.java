/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.points;

import com.uptime.cassandra.config.dao.ApAlByPointDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.PointsDAO;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.cassandra.config.entity.Points;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.vo.ApAlSetVO;
import com.uptime.config.vo.PointVO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class DeletePointsDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeletePointsDelegate.class.getName());
    private final PointsDAO pointsDAO;
    private final ApAlByPointDAO apAlByPointDAO;

    /**
     * Constructor
     */
    public DeletePointsDelegate() {
        pointsDAO = ConfigMapperImpl.getInstance().pointsDAO();
        apAlByPointDAO = ConfigMapperImpl.getInstance().apAlByPointDAO();
    }

    /**
     * Delete Rows from Cassandra with containing the given values
     *
     * @param pointVO, PointVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deletePoints(PointVO pointVO) throws IllegalArgumentException {
        List<Points> dbPointsList;
        Points dbPoints = null;
        List<ApAlByPoint> dbApAlByPointList;
        ApAlByPoint dbApAlByPoint = null;

        // Find the entities based on the given values
        try {
            if (pointVO != null) {
                if ((dbPointsList = pointsDAO.findByPK(pointVO.getCustomerAccount(), pointVO.getSiteId(), pointVO.getAreaId(), pointVO.getAssetId(), pointVO.getPointLocationId(), pointVO.getPointId(), pointVO.getApSetId())) != null && !dbPointsList.isEmpty()) {
                    dbPoints = dbPointsList.get(0);
                }
                if (pointVO.getApAlSetVOs() != null && !pointVO.getApAlSetVOs().isEmpty()) {
                    for (ApAlSetVO apAlSetVO : pointVO.getApAlSetVOs()) {
                        if ((dbApAlByPointList = apAlByPointDAO.findByPK(pointVO.getCustomerAccount(), pointVO.getSiteId(), pointVO.getAreaId(), pointVO.getAssetId(), pointVO.getPointLocationId(), pointVO.getPointId(), pointVO.getApSetId(), pointVO.getAlSetId(), apAlSetVO.getParamName())) != null && !dbApAlByPointList.isEmpty()) {
                            dbApAlByPoint = dbApAlByPointList.get(0);
                        }
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (dbPoints != null) {
                pointsDAO.delete(dbPoints);

                if (dbApAlByPoint != null) {
                    apAlByPointDAO.delete(dbApAlByPoint);
                }

                return "{\"outcome\":\"Deleted Points successfully.\"}";
            } else {
                return "{\"outcome\":\"No Points found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete Points.\"}";
    }
    
    /**
     * Delete Rows from Cassandra with containing the given values
     *
     * @param pointVOList, List of PointVO Objects
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deletePoints(List<PointVO> pointVOList) throws IllegalArgumentException {
        try {
            for (PointVO pointVO : pointVOList) {
                // Insert into Cassandra
                deletePoints(pointVO);
            }
            return "{\"outcome\":\"Deleted Points successfully.\"}";
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete Points.\"}";
    }
}
