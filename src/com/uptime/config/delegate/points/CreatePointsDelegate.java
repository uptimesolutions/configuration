/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.points;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.dao.ApAlByPointDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.PointLocationsDAO;
import com.uptime.cassandra.config.dao.PointsDAO;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.cassandra.config.entity.Points;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.dao.SiteBasestationDeviceDAO;
import com.uptime.cassandra.monitor.dao.SiteDeviceBasestationDAO;
import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import com.uptime.cassandra.monitor.entity.SiteDeviceBasestation;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.utils.DelegateUtil;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.PointVO;
import com.uptime.services.util.ServiceUtil;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author madhavi
 */
public class CreatePointsDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreatePointsDelegate.class.getName());
    private final PointsDAO pointsDAO;
    private final ApAlByPointDAO apAlByPointDAO;
    private final PointLocationsDAO pointLocationsDAO;
    private final SiteBasestationDeviceDAO siteBasestationDeviceDAO;
    private final SiteDeviceBasestationDAO siteDeviceBasestationDAO;

    /**
     * Constructor
     */
    public CreatePointsDelegate() {
        pointsDAO = ConfigMapperImpl.getInstance().pointsDAO();
        apAlByPointDAO = ConfigMapperImpl.getInstance().apAlByPointDAO();
        pointLocationsDAO = ConfigMapperImpl.getInstance().pointLocationsDAO();
        siteBasestationDeviceDAO = BaseStationMapperImpl.getInstance().siteBasestationDeviceDAO();
        siteDeviceBasestationDAO = BaseStationMapperImpl.getInstance().siteDeviceBasestationDAO();
    }

    /**
     * Insert new Rows into Cassandra based on the given object
     *
     * @param pointVO, PointVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String createPoints(PointVO pointVO) throws IllegalArgumentException, Exception {
        SiteBasestationDevice siteBasestationDevice = null;
        SiteDeviceBasestation siteDeviceBasestation = null;
        List<PointLocations> pointLocationsList;
        List<ApAlByPoint> apAlByPoints = null;
        PointLocations pointLocations = null;
        Points points;
        String errorMsg; 
        
        try {
            if ((pointLocationsList = pointLocationsDAO.findByPK(pointVO.getCustomerAccount(), pointVO.getSiteId(), pointVO.getAreaId(), pointVO.getAssetId(), pointVO.getPointLocationId())) != null && !pointLocationsList.isEmpty()) {
                pointLocations = pointLocationsList.get(0);
            }
        } catch (IllegalArgumentException ex) {
            return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        } catch (ReadTimeoutException | UnavailableException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return "{\"outcome\":\"Error: Failed to validate Points.\"}";
        }

        if (pointVO.getPointId() == null) {
            pointVO.setPointId(UUID.randomUUID());
        }
        pointVO.setSensorChannelNum(-1);
        points = DelegateUtil.getPoints(pointVO);

        if (pointVO.getApAlSetVOs() != null && !pointVO.getApAlSetVOs().isEmpty()) {
            apAlByPoints = DelegateUtil.getApAlByPointList(pointVO);
        }

        if (apAlByPoints != null && !apAlByPoints.isEmpty() && pointLocations != null && pointLocations.getDeviceSerialNumber() != null && !pointLocations.getDeviceSerialNumber().isEmpty()) {
            siteBasestationDevice = DelegateUtil.getSiteBasestationDevice(pointLocations, points, apAlByPoints.get(0));
            siteDeviceBasestation = DelegateUtil.getSiteDeviceBasestation(siteBasestationDevice);
        }

        // Insert the entities into Cassandra
        try {
            if (((errorMsg = ServiceUtil.validateObjectData(points)) == null) &&
                    (siteBasestationDevice == null || (errorMsg = ServiceUtil.validateObjectData(siteBasestationDevice)) == null) &&
                    (siteDeviceBasestation == null || (errorMsg = ServiceUtil.validateObjectData(siteDeviceBasestation)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(apAlByPoints, Boolean.TRUE)) == null)) {
                
                pointsDAO.create(points);
                if (apAlByPoints != null && !apAlByPoints.isEmpty()) {
                    apAlByPointDAO.create(apAlByPoints);
                }
                if (siteBasestationDevice != null) {
                    siteBasestationDeviceDAO.create(siteBasestationDevice);
                }
                if (siteBasestationDevice != null) {
                    siteDeviceBasestationDAO.create(siteDeviceBasestation);
                }
                
                if (pointLocations != null && !pointVO.isDisabled()) {
                    JsonUtil.createAndSendDeleteMessageToKafka(pointLocations.getDeviceSerialNumber(), pointVO.getPointType(), pointVO.getSensorChannelNum());
                }

                return "{\"outcome\":\"New Points created / updated successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to create new Points.\"}";
    }

    /**
     * Insert new Rows into Cassandra based on the given object
     *
     * @param pointVOList, List of PointVO objects
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String createPoints(List<PointVO> pointVOList) throws IllegalArgumentException, Exception {
        try {
            // Insert into Cassandra
            for (PointVO pointVO : pointVOList) {
                createPoints(pointVO);
            }
            return "{\"outcome\":\"New Points created / updated successfully.\"}";
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to create new Points.\"}";
    }
}
