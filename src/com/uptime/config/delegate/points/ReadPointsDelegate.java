/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.points;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.dao.ApAlByPointDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.cassandra.config.entity.Points;
import com.uptime.cassandra.config.dao.PointsDAO;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
public class ReadPointsDelegate {
    private final PointsDAO  pointsDAO;
    private final ApAlByPointDAO apAlByPointDAO;
    
    /**
     * Constructor
     */
    public ReadPointsDelegate() {
        pointsDAO = ConfigMapperImpl.getInstance().pointsDAO();
        apAlByPointDAO = ConfigMapperImpl.getInstance().apAlByPointDAO();
    }
    
    /**
     * Return a List of Points Objects by the given params
     * 
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return List of Points Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<Points> getPointsByCustomerSiteAreaAsset(String customerAccount, UUID siteId, UUID areaId, UUID assetId) throws IllegalArgumentException, ReadTimeoutException, UnavailableException {
        return pointsDAO.findByCustomerSiteAreaAsset(customerAccount, siteId, areaId, assetId);
    }
    
    /**
     * Return a List of Points Objects by the given params
     * 
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param locationId, UUID Object
     * @return List of Points Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    
    public List<Points> getPointsByCustomerSiteAreaAssetLocation(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID locationId) throws IllegalArgumentException, ReadTimeoutException, UnavailableException {
        return pointsDAO.findByCustomerSiteAreaAssetLocation(customerAccount, siteId, areaId, assetId, locationId);
    }
    
    /**
     * Return a List of Points Objects by the given params
     * 
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param locationId, UUID Object
     * @param pointId, UUID Object
     * @return List of Points Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<Points> getPointsByCustomerSiteAreaAssetLocationPoint(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID locationId, UUID pointId) throws IllegalArgumentException, ReadTimeoutException, UnavailableException {
        return pointsDAO.findByCustomerSiteAreaAssetLocationPoint(customerAccount, siteId, areaId, assetId, locationId, pointId);
    }
    
    /**
     * Return a List of Points Objects by the given params
     * 
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param locationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetId, UUID Object
     * @return List of Points Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
   
    public List<Points> getPointsByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID locationId, UUID pointId, UUID apSetId) throws IllegalArgumentException, ReadTimeoutException, UnavailableException {
        return pointsDAO.findByPK(customerAccount, siteId, areaId, assetId, locationId, pointId, apSetId);
    }
    
    /**
     * Return a List of ApAlByPoint Objects by the given params
     * 
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param locationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetId, UUID Object
     * @return List of ApAlByPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
 
    public List<ApAlByPoint> getByCustomerSiteAreaAssetLocationPointApSet(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID locationId, UUID pointId, UUID apSetId) throws IllegalArgumentException, ReadTimeoutException, UnavailableException {
        return apAlByPointDAO.findByCustomerSiteAreaAssetLocationPointApSet(customerAccount, siteId, areaId, assetId, locationId, pointId, apSetId);
    }
    
    /**
     * Return a List of ApAlByPoint Objects by the given params
     * 
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param locationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @return List of ApAlByPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<ApAlByPoint> getByCustomerSiteAreaAssetLocationPointApSetAlSet(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID locationId, UUID pointId, UUID apSetId, UUID alSetId) throws IllegalArgumentException, ReadTimeoutException, UnavailableException {
        return apAlByPointDAO.findByCustomerSiteAreaAssetLocationPointApSetAlSet(customerAccount, siteId, areaId, assetId, locationId, pointId, apSetId, alSetId);
    }
    
    /**
     * Return a List of ApAlByPoint Objects by the given params
     * 
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param locationId, UUID Object
     * @param pointId, UUID Object
     * @param apSetId, UUID Object
     * @param alSetId, UUID Object
     * @param paramName, String Object
     * @return List of ApAlByPoint Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<ApAlByPoint> getApAlByPointByPK(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID locationId, UUID pointId, UUID apSetId, UUID alSetId, String paramName) throws IllegalArgumentException, ReadTimeoutException, UnavailableException {
        return apAlByPointDAO.findByPK(customerAccount, siteId, areaId, assetId, locationId, pointId, apSetId, alSetId, paramName);
    }
}
