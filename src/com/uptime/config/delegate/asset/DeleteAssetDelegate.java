/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.asset;

import com.uptime.cassandra.config.dao.ApAlByPointDAO;
import com.uptime.cassandra.config.dao.AssetSiteAreaDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.PointLocationsDAO;
import com.uptime.cassandra.config.dao.PointsDAO;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.cassandra.config.entity.AssetSiteArea;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.cassandra.config.entity.Points;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.vo.AssetVO;
import com.uptime.config.vo.PointLocationVO;
import com.uptime.config.vo.PointVO;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class DeleteAssetDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteAssetDelegate.class.getName());
    private final AssetSiteAreaDAO assetSiteAreaDAO;
    private final PointLocationsDAO pointLocationsDAO;
    private final PointsDAO pointsDAO;
    private final ApAlByPointDAO apAlByPointDAO;
    
    /**
     * Constructor
     */
    public DeleteAssetDelegate() {
        assetSiteAreaDAO = ConfigMapperImpl.getInstance().assetSiteAreaDAO();
        pointLocationsDAO = ConfigMapperImpl.getInstance().pointLocationsDAO();
        pointsDAO = ConfigMapperImpl.getInstance().pointsDAO();
        apAlByPointDAO = ConfigMapperImpl.getInstance().apAlByPointDAO();
    }

    /**
     * Delete Rows from Cassandra with containing the given values
     * @param assetVO, AssetVO Object
     * @return String object
     * @throws IllegalArgumentException
     */
    public String deleteAsset(AssetVO assetVO) throws IllegalArgumentException {
        List<AssetSiteArea> assetSiteAreaList;
        List<PointLocations> pointLocationsList, dbPointLocationsList;
        List<Points> pointsList, dbPointsList;
        List<ApAlByPoint> apAlByPointList, dbApAlByPointList;
        
        AssetSiteArea assetSiteArea = null;
        dbPointLocationsList = new ArrayList<>();
        dbPointsList = new ArrayList<>();
        dbApAlByPointList = new ArrayList<>();
        
        PointLocationVO plvo;
        PointVO pvo;
        
        // Find the entities based on the given values
        try {
            if((assetSiteAreaList = assetSiteAreaDAO.findByPK(assetVO.getCustomerAccount(), assetVO.getSiteId(), assetVO.getAreaId(), assetVO.getAssetId())) != null && !assetSiteAreaList.isEmpty()) {
                assetSiteArea = assetSiteAreaList.get(0);
            }
            
            if (assetVO.getPointLocationList() != null && !assetVO.getPointLocationList().isEmpty()){
                for (int i = 0; i < assetVO.getPointLocationList().size(); i++) {
                    if ((plvo = assetVO.getPointLocationList().get(i)) != null){
                        pointLocationsList = pointLocationsDAO.findByPK(plvo.getCustomerAccount(), plvo.getSiteId(), plvo.getAreaId(), plvo.getAssetId(), plvo.getPointLocationId());
                        dbPointLocationsList.addAll(pointLocationsList);
                        if (plvo.getPointList() != null && !plvo.getPointList().isEmpty()) {
                            for (int j=0; j < plvo.getPointList().size(); j++) {
                                pvo = plvo.getPointList().get(j);
                                pointsList = pointsDAO.findByPK(pvo.getCustomerAccount(), pvo.getSiteId(), pvo.getAreaId(), pvo.getAssetId(), pvo.getPointLocationId(), pvo.getPointId(), pvo.getApSetId());
                                if (pointsList != null && !pointsList.isEmpty()) {
                                    dbPointsList.addAll(pointsList);
                                    apAlByPointList = apAlByPointDAO.findByCustomerSiteAreaAssetLocationPointApSetAlSet(pvo.getCustomerAccount(), pvo.getSiteId(), pvo.getAreaId(), pvo.getAssetId(), pvo.getPointLocationId(), pvo.getPointId(), pvo.getApSetId(), pvo.getAlSetId());
                                    dbApAlByPointList.addAll(apAlByPointList);
                                }
                            }
                        }
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to delete.\"}";
        }
        
        // Delete the entities from Cassandra
        try {
            if (assetSiteArea != null) {
                assetSiteAreaDAO.delete(assetSiteArea);
                
                if (!dbPointLocationsList.isEmpty()) {
                    pointLocationsDAO.delete(dbPointLocationsList);
                }

                if (!dbPointsList.isEmpty()) {
                    pointsDAO.delete(dbPointsList);
                }
                
                if (!dbApAlByPointList.isEmpty()) {
                    apAlByPointDAO.delete(dbApAlByPointList);
                }
                
                return "{\"outcome\":\"Deleted Asset items successfully.\"}";
            } else {
                return "{\"outcome\":\"No Asset items found to delete.\"}";
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete Asset items.\"}";
    }
}
