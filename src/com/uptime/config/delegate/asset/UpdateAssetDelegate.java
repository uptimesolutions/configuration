/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.asset;

import com.uptime.cassandra.config.dao.AssetSiteAreaDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.PointLocationsDAO;
import com.uptime.cassandra.config.dao.PointsDAO;
import com.uptime.cassandra.config.entity.AssetSiteArea;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.cassandra.config.entity.Points;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.dao.SiteBasestationDeviceDAO;
import com.uptime.cassandra.workorder.dao.OpenWorkOrdersDAO;
import com.uptime.cassandra.workorder.dao.WorkOrdersMapperImpl;
import com.uptime.cassandra.workorder.entity.OpenWorkOrders;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.utils.DelegateUtil;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.AssetVO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class UpdateAssetDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateAssetDelegate.class.getName());
    private final AssetSiteAreaDAO assetSiteAreaDAO;
    private final PointLocationsDAO pointLocationsDAO;
    private final PointsDAO pointsDAO;
    private final SiteBasestationDeviceDAO siteBasestationDeviceDAO;
    private final OpenWorkOrdersDAO openWorkOrdersDAO;
    private List<PointLocations> pointLocationsList;
    private HashMap<UUID, List<Points>> plPointsListMap;

    /**
     * Constructor
     */
    public UpdateAssetDelegate() {

        assetSiteAreaDAO = ConfigMapperImpl.getInstance().assetSiteAreaDAO();
        pointLocationsDAO = ConfigMapperImpl.getInstance().pointLocationsDAO();
        pointsDAO = ConfigMapperImpl.getInstance().pointsDAO();
        
        siteBasestationDeviceDAO = BaseStationMapperImpl.getInstance().siteBasestationDeviceDAO();
        openWorkOrdersDAO = WorkOrdersMapperImpl.getInstance().openWorkOrdersDAO();
    }

    /**
     * Update Rows in Cassandra based on the given object
     *
     * @param assetVO, AssetVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String updateAsset(AssetVO assetVO) throws IllegalArgumentException, Exception {
        List<OpenWorkOrders> openWorkOrdersList = null;
        List<AssetSiteArea> assetSiteAreaList;
        AssetSiteArea newAssetSiteArea, originalAssetSiteArea = null;
        List<Points> pointsList;
        String errorMsg;

        // Find the entities based on the given values
        try {
            if ((assetSiteAreaList = assetSiteAreaDAO.findByPK(assetVO.getCustomerAccount(), assetVO.getSiteId(), assetVO.getAreaId(), assetVO.getAssetId())) != null && !assetSiteAreaList.isEmpty()) {
                originalAssetSiteArea = assetSiteAreaList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to update.\"}";
        }

        // Insert updated entities into Cassandra if original entities are found
        // commented the originalAssetSiteType and originalAssetTypeSite checks as all migrated assets would not have the asset type yet
        if (originalAssetSiteArea != null) {
            newAssetSiteArea = DelegateUtil.getAssetSiteArea(assetVO);
            if (!originalAssetSiteArea.getAssetName().equals(newAssetSiteArea.getAssetName())) {
                if ((openWorkOrdersList = openWorkOrdersDAO.findByCustomerSiteAreaAsset(originalAssetSiteArea.getCustomerAccount(), originalAssetSiteArea.getSiteId(), originalAssetSiteArea.getAreaId(), originalAssetSiteArea.getAssetId())) != null && !openWorkOrdersList.isEmpty()) {
                    openWorkOrdersList.forEach(openWorkOrders -> openWorkOrders.setAssetName(newAssetSiteArea.getAssetName()));
                }
            }
            
            try {
                if (((errorMsg = ServiceUtil.validateObjectData(newAssetSiteArea)) == null)) {
                    // Update Cassandra
                    if (((errorMsg = ServiceUtil.validateObjects(openWorkOrdersList, Boolean.TRUE)) == null)) {
                        assetSiteAreaDAO.update(newAssetSiteArea);
                        if (openWorkOrdersList != null) {
                            openWorkOrdersList.forEach(openWorkOrders -> openWorkOrdersDAO.update(openWorkOrders));
                        }
                    } else {
                        return errorMsg;
                    }

                    // Here if alarm_enabled or asset_name is modified, update point And, point_locations tables.
                     if ((originalAssetSiteArea.isAlarmEnabled() != newAssetSiteArea.isAlarmEnabled()) ||
                            (!originalAssetSiteArea.getAssetName().equals(newAssetSiteArea.getAssetName()))) {
                        pointLocationsList = pointLocationsDAO.findByCustomerSiteAreaAsset(newAssetSiteArea.getCustomerAccount(), newAssetSiteArea.getSiteId(), newAssetSiteArea.getAreaId(), newAssetSiteArea.getAssetId());
                        plPointsListMap = new HashMap<>();
//                        if (originalAssetSiteArea.isAlarmEnabled() != newAssetSiteArea.isAlarmEnabled()) 
//                            updateAlarmEnabled(newAssetSiteArea);
                    }

                    // Here if sample_interval is modified, update all point_locations for that asset.
                    if (originalAssetSiteArea.getSampleInterval() != newAssetSiteArea.getSampleInterval()) {
                        if (pointLocationsList == null) {
                            pointLocationsList = pointLocationsDAO.findByCustomerSiteAreaAsset(newAssetSiteArea.getCustomerAccount(), newAssetSiteArea.getSiteId(), newAssetSiteArea.getAreaId(), newAssetSiteArea.getAssetId());
                        }
                        updateSampleTime(newAssetSiteArea);
                    }

                    //Send message to Kafka if alarmEnabled or asset name has changed
                    if ((originalAssetSiteArea.isAlarmEnabled() != newAssetSiteArea.isAlarmEnabled()) ||
                            (!originalAssetSiteArea.getAssetName().equals(newAssetSiteArea.getAssetName()))) {
                        if (pointLocationsList != null && !pointLocationsList.isEmpty()) {
                            if (plPointsListMap == null || plPointsListMap.isEmpty()) {
                                for (PointLocations pvo : pointLocationsList) {
                                    if ((pointsList = pointsDAO.findByCustomerSiteAreaAssetLocation(pvo.getCustomerAccount(), pvo.getSiteId(), pvo.getAreaId(), pvo.getAssetId(), pvo.getPointLocationId())) != null && !pointsList.isEmpty()) {
                                        plPointsListMap.put(pvo.getPointLocationId(), pointsList);
                                    }
                                }
                            }
                            
                            for (PointLocations pvo : pointLocationsList) {
                                if ((pointsList = plPointsListMap.get(pvo.getPointLocationId())) != null && !pointsList.isEmpty()) {
                                    for (Points points : pointsList) {
                                        if (!points.isDisabled()) {
                                            JsonUtil.createAndSendDeleteMessageToKafka(pvo.getDeviceSerialNumber(), points.getPointType(), points.getSensorChannelNum());
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return "{\"outcome\":\"Updated Asset items successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
            return "{\"outcome\":\"Error: Failed to update Asset items.\"}";
        }

        return "{\"outcome\":\"Asset items not found to update.\"}";
    }

    /**
     * Update Rows in Cassandra based on the given object
     * @param assetVO, AssetVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String updatePointsAlarmEnabledForAsset(AssetVO assetVO) throws IllegalArgumentException, Exception {
        List<AssetSiteArea> assetSiteAreaList;
        List<PointLocations> origPointLocationsList = null;

        List<Points> tmpPointsList, originalPointsList = null, newPointsList = null;
        AssetSiteArea assetSiteArea = null;
        String errorMsg;
        
        // Find the entities based on the given values
        try {
            if (assetVO != null) {

                if((assetSiteAreaList = assetSiteAreaDAO.findByPK(assetVO.getCustomerAccount(), assetVO.getSiteId(), assetVO.getAreaId(), assetVO.getAssetId())) != null && !assetSiteAreaList.isEmpty()) {
                    assetSiteArea = assetSiteAreaList.get(0);
                }
                if (assetSiteArea != null && (origPointLocationsList = pointLocationsDAO.findByCustomerSiteAreaAsset(assetSiteArea.getCustomerAccount(), assetSiteArea.getSiteId(), assetSiteArea.getAreaId(), assetSiteArea.getAssetId())) != null && !origPointLocationsList.isEmpty()){
                    originalPointsList = new ArrayList();
                    for (PointLocations pl : origPointLocationsList) {
                        if ((tmpPointsList = pointsDAO.findByCustomerSiteAreaAssetLocation(pl.getCustomerAccount(), pl.getSiteId(), pl. getAreaId(), pl.getAssetId(), pl.getPointLocationId())) != null && !tmpPointsList.isEmpty()) {
                            originalPointsList.addAll(tmpPointsList);
                        }
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find items to update.\"}";
        }
        
        // Insert updated entities into Cassandra if original entities are found
        if(originalPointsList != null && origPointLocationsList != null && assetVO != null && assetVO.getSensorList() != null) {
            try { 
                newPointsList = new ArrayList<>();
                for (Points points : originalPointsList) {
                    if (assetVO.getSensorList().contains(points.getSensorType())) {
                        Points newPoints = new Points(points);
                        newPoints.setAlarmEnabled(assetVO.isAlarmEnabled());
                        newPointsList.add(newPoints);
                    }
                }
                if ((errorMsg = ServiceUtil.validateObjects(newPointsList, Boolean.TRUE)) == null) { 
                   
                    // Update Cassandra
                    if (!newPointsList.isEmpty()) {
                        pointsDAO.update(newPointsList);
                    } else {
                        return errorMsg;
                    }

                    // Send Kafka Message
                    for (PointLocations pvo : origPointLocationsList) {
                        if (pvo.getDeviceSerialNumber() != null && !pvo.getDeviceSerialNumber().isEmpty()) {
                            for (Points points : newPointsList) {
                                if (pvo.getPointLocationId().equals(points.getPointLocationId()) && !points.isDisabled()) {
                                    JsonUtil.createAndSendDeleteMessageToKafka(pvo.getDeviceSerialNumber(), points.getPointType(), points.getSensorChannelNum());
                                }
                            }
                        }
                    }
                    return "{\"outcome\":\"Updated Asset alarm enabled successfully.\"}";
                } else {
                    return errorMsg;
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            } 
            return "{\"outcome\":\"Error: Failed to update alarm enabled for Asset.\"}";
        }
        return "{\"outcome\":\"Asset items not found to update.\"}";
    }

    /**
     * Here if sample_interval modified, update point_locations,
     * site_basestation_device records to the same sample_interval.
     *
     * @param newAssetSiteArea, AssetSiteArea Object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    private void updateSampleTime(AssetSiteArea newAssetSiteArea) throws IllegalArgumentException, Exception {
        List<String> pointLocationIdList;
        List<Points> pointsList;
        StringBuilder query, sb;

        if (pointLocationsList != null && !pointLocationsList.isEmpty()) {
            pointLocationIdList = new ArrayList();
            pointLocationsList.stream().forEach(pointLocation -> pointLocationIdList.add(pointLocation.getPointLocationId().toString()));

            if (!pointLocationIdList.isEmpty()) {
                query = new StringBuilder();
                query.append("update point_locations set sample_interval=");
                query.append(newAssetSiteArea.getSampleInterval());
                query.append(" where customer_acct= '");
                query.append(newAssetSiteArea.getCustomerAccount());
                query.append("' and site_id= ");
                query.append(newAssetSiteArea.getSiteId());
                query.append(" and area_id= ");
                query.append(newAssetSiteArea.getAreaId());
                query.append(" and asset_id= ");
                query.append(newAssetSiteArea.getAssetId());
                query.append(" and point_location_id in (");
                query.append(String.join(",", pointLocationIdList));
                query.append(");");
                pointLocationsDAO.updateWithCustomQuery(query.toString());
            }

            for (PointLocations pvo : pointLocationsList) {
                if ((pointsList = pointsDAO.findByCustomerSiteAreaAssetLocation(pvo.getCustomerAccount(), pvo.getSiteId(), pvo.getAreaId(), pvo.getAssetId(), pvo.getPointLocationId())) != null
                        && !pointsList.isEmpty() && pvo.getDeviceSerialNumber() != null && !pvo.getDeviceSerialNumber().isEmpty()) {
                    query = new StringBuilder();
                    query.append("BEGIN BATCH ");
                    for (Points point : pointsList) {
                        sb = new StringBuilder();
                        sb.append("update site_basestation_device set sample_interval=");
                        sb.append(newAssetSiteArea.getSampleInterval());
                        sb.append(" where customer_acct= '");
                        sb.append(point.getCustomerAccount());
                        sb.append("' and site_id= ");
                        sb.append(point.getSiteId());
                        sb.append(" and base_station_port= ");
                        sb.append(pvo.getBasestationPortNum());
                        sb.append(" and device_id= '");
                        sb.append(pvo.getDeviceSerialNumber());
                        sb.append("' and point_id = ");
                        sb.append(point.getPointId());
                        sb.append("; ");
                        query.append(sb);
                    }
                    query.append(" APPLY BATCH;");
                    siteBasestationDeviceDAO.updateSampleIntervalField(query.toString());
                }
            }
        }
    }

}
