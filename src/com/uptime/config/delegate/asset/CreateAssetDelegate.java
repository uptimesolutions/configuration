/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.asset;

import com.uptime.cassandra.config.dao.ApAlByPointDAO;
import com.uptime.cassandra.config.dao.AssetSiteAreaDAO;
import com.uptime.cassandra.config.dao.PointLocationsDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.HwUnitToPointDAO;
import com.uptime.cassandra.config.dao.PointToHwUnitDAO;
import com.uptime.cassandra.config.dao.PointsDAO;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.cassandra.config.entity.AssetSiteArea;
import com.uptime.cassandra.config.entity.HwUnitToPoint;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.cassandra.config.entity.PointToHwUnit;
import com.uptime.cassandra.config.entity.Points;
import com.uptime.cassandra.monitor.dao.BaseStationMapperImpl;
import com.uptime.cassandra.monitor.dao.SiteBasestationDeviceDAO;
import com.uptime.cassandra.monitor.dao.SiteDeviceBasestationDAO;
import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import com.uptime.cassandra.monitor.entity.SiteDeviceBasestation;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.utils.DelegateUtil;
import com.uptime.config.vo.ApAlSetVO;
import com.uptime.config.vo.AssetVO;
import com.uptime.config.vo.PointLocationVO;
import com.uptime.config.vo.PointVO;
import com.uptime.services.util.ServiceUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class CreateAssetDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateAssetDelegate.class.getName());
    private final AssetSiteAreaDAO assetSiteAreaDAO;
    private final PointLocationsDAO pointLocationsDAO;
    private final PointsDAO pointsDAO;
    private final ApAlByPointDAO apAlByPointDAO;
    private final SiteBasestationDeviceDAO siteBasestationDeviceDAO;
    private final SiteDeviceBasestationDAO siteDeviceBasestationDAO;
    private final HwUnitToPointDAO hwUnitToPointDAO;
    private final PointToHwUnitDAO pointToHwUnitDAO;

    /**
     * Constructor
     */
    public CreateAssetDelegate() {
        assetSiteAreaDAO = ConfigMapperImpl.getInstance().assetSiteAreaDAO();
        pointLocationsDAO = ConfigMapperImpl.getInstance().pointLocationsDAO();
        pointsDAO = ConfigMapperImpl.getInstance().pointsDAO();
        apAlByPointDAO = ConfigMapperImpl.getInstance().apAlByPointDAO();
        siteBasestationDeviceDAO = BaseStationMapperImpl.getInstance().siteBasestationDeviceDAO();
        siteDeviceBasestationDAO = BaseStationMapperImpl.getInstance().siteDeviceBasestationDAO();
        hwUnitToPointDAO = ConfigMapperImpl.getInstance().hwUnitToPointDAO();
        pointToHwUnitDAO = ConfigMapperImpl.getInstance().pointToHwUnitDAO();
    }

    /**
     * Insert new Rows into Cassandra based on the given object
     *
     * @param assetVO, AssetVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String createAsset(AssetVO assetVO) throws IllegalArgumentException, Exception {
        AssetSiteArea assetSiteArea;
        List<PointLocations> pointLocationsList;
        List<Points> pointsList;
        List<ApAlByPoint> apAlByPointList;
        List<SiteBasestationDevice> siteBasestationDeviceList, dbSiteBasestationDeviceList, delSiteBasestationDeviceList;
        List<SiteDeviceBasestation> siteDeviceBasestationList, dbSiteDeviceBasestationList;
        SiteBasestationDevice siteBasestationDevice;
        ApAlByPoint baseApAlByPoint;
        PointToHwUnit pointToHwUnit;
        HwUnitToPoint hwUnitToPoint;
        List<PointToHwUnit> pointToHwUnitList;
        List<HwUnitToPoint> hwUnitToPointList;
        String errorMsg;
        
        if (assetVO.getAssetId() == null) {
            assetVO.setAssetId(UUID.randomUUID());
        }
        
        assetSiteArea = DelegateUtil.getAssetSiteArea(assetVO);
        pointLocationsList = DelegateUtil.getPointLocationsList(assetVO);
        pointsList = DelegateUtil.getPointsList(assetVO);
        apAlByPointList = new ArrayList();
        
        if (assetVO.getPointLocationList() != null && !assetVO.getPointLocationList().isEmpty()) {
            for (PointLocationVO pointLocationsVO : assetVO.getPointLocationList()) {
                if (pointLocationsVO.getPointList()!= null && !pointLocationsVO.getPointList().isEmpty()) {
                    for (PointVO points : pointLocationsVO.getPointList()) {
                        if ( points.getApAlSetVOs() != null && ! points.getApAlSetVOs().isEmpty()) {
                            for (ApAlSetVO apAlSetVO : points.getApAlSetVOs()) {
                                ApAlByPoint apAlByPoint = DelegateUtil.getApAlByPoint(points, apAlSetVO);
                                apAlByPointList.add(apAlByPoint);
                            }
                        }
                    }
                }
            }
        }
        siteBasestationDeviceList = new ArrayList();
        siteDeviceBasestationList = new ArrayList();
        delSiteBasestationDeviceList = new ArrayList();
        
        pointToHwUnitList = new ArrayList();
        hwUnitToPointList = new ArrayList();
        
        if (pointLocationsList != null && !pointLocationsList.isEmpty()) {
            for (PointLocations pointLocations : pointLocationsList) {
                for (Points points : pointsList) {
                    if (points.getPointLocationId() == pointLocations.getPointLocationId()) {
                        baseApAlByPoint = null;
                        for (ApAlByPoint apAlByPoint : apAlByPointList) {
                            if (apAlByPoint.getPointId() == points.getPointId()) {
                                baseApAlByPoint = apAlByPoint;
                                break;
                            }
                        }
                        if (pointLocations.getDeviceSerialNumber() != null && pointLocations.getDeviceSerialNumber().length() > 0) {
                            siteBasestationDevice = DelegateUtil.getSiteBasestationDevice(pointLocations, points, baseApAlByPoint);
                            siteBasestationDeviceList.add(siteBasestationDevice);
                            siteDeviceBasestationList.add(DelegateUtil.getSiteDeviceBasestation(siteBasestationDevice));
                            hwUnitToPoint = DelegateUtil.getHwUnitToPoint(points, pointLocations.getDeviceSerialNumber());
                            hwUnitToPointList.add(hwUnitToPoint);
                            pointToHwUnit = DelegateUtil.getPointToHwUnit(points, pointLocations.getDeviceSerialNumber());
                            pointToHwUnitList.add(pointToHwUnit);

                            //Delete the siteBasestationDevice and siteDeviceBasestation records for the old base port number
                            if ((dbSiteDeviceBasestationList = siteDeviceBasestationDAO.findByPK(pointLocations.getCustomerAccount(), pointLocations.getSiteId(), pointLocations.getDeviceSerialNumber())) != null && !dbSiteDeviceBasestationList.isEmpty()) {
                                if (dbSiteDeviceBasestationList.get(0).getBaseStationPort() != pointLocations.getBasestationPortNum()) {
                                    dbSiteBasestationDeviceList = siteBasestationDeviceDAO.findByPK(pointLocations.getCustomerAccount(), pointLocations.getSiteId(), dbSiteDeviceBasestationList.get(0).getBaseStationPort(), pointLocations.getDeviceSerialNumber(), points.getPointId());
                                    delSiteBasestationDeviceList.addAll(dbSiteBasestationDeviceList);
                                }
                            }
                        }
                    }
                }
            }
        }

        // Insert the entities into Cassandra
        try { 
            if (((errorMsg = ServiceUtil.validateObjectData(assetSiteArea)) == null) && 
                    ((errorMsg = ServiceUtil.validateObjects(pointLocationsList, Boolean.TRUE)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(pointsList, Boolean.TRUE)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(apAlByPointList, Boolean.TRUE)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(siteBasestationDeviceList, Boolean.TRUE)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(siteDeviceBasestationList, Boolean.TRUE)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(delSiteBasestationDeviceList, Boolean.TRUE)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(hwUnitToPointList, Boolean.TRUE)) == null) &&
                    ((errorMsg = ServiceUtil.validateObjects(pointToHwUnitList, Boolean.TRUE)) == null)) {
                assetSiteAreaDAO.create(assetSiteArea);
                
                if (pointLocationsList != null && !pointLocationsList.isEmpty()) {
                    pointLocationsDAO.create(pointLocationsList);
                }
                if (pointsList != null && !pointsList.isEmpty()) {
                    pointsDAO.create(pointsList);
                }
                
                if (!apAlByPointList.isEmpty()) {
                    apAlByPointDAO.create(apAlByPointList);
                }
                if (!siteBasestationDeviceList.isEmpty()) {
                    siteBasestationDeviceDAO.create(siteBasestationDeviceList);
                }
                if (!siteDeviceBasestationList.isEmpty()) {
                    siteDeviceBasestationList.forEach(siteDeviceBasestation -> siteDeviceBasestationDAO.create(siteDeviceBasestation));
                }
                if (!hwUnitToPointList.isEmpty()) {
                    hwUnitToPointList.forEach(hwUnit -> hwUnitToPointDAO.create(hwUnit));
                }
                if (!pointToHwUnitList.isEmpty()) {
                    pointToHwUnitList.forEach(pointToHU -> pointToHwUnitDAO.create(pointToHU));
                }
                if (!delSiteBasestationDeviceList.isEmpty()) {
                    siteBasestationDeviceDAO.delete(delSiteBasestationDeviceList);
                }
                
                return "{\"outcome\":\"New Asset created successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to create new Asset.\"}";
    }

    public String createAssets(List<AssetVO> assetVOList) throws IllegalArgumentException {
        try {
            for (AssetVO assetVO : assetVOList) {
                if (assetVO != null
                    && assetVO.getCustomerAccount() != null && !assetVO.getCustomerAccount().isEmpty()
                    && assetVO.getAssetName() != null && !assetVO.getAssetName().isEmpty()
                    && assetVO.getSiteId() != null
                    && assetVO.getAreaId() != null) {
                    // Insert into Cassandra
                    createAsset(assetVO);
                }
            }
            return "{\"outcome\":\"Created new TS1 Device successfully.\"}";
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to create new TS1 Device.\"}";
    }
}
