/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.delegate.asset;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.AssetSiteAreaDAO;
import com.uptime.cassandra.config.dao.PointLocationsDAO;
import com.uptime.cassandra.config.dao.PointsDAO;
import com.uptime.cassandra.config.entity.AssetSiteArea;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.cassandra.config.entity.Points;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class ReadAssetDelegate {
    private final AssetSiteAreaDAO assetSiteAreaDAO;
    private final PointLocationsDAO pointLocationsDAO;
    private final PointsDAO pointsDAO;

    /**
     * Constructor
     */
    public ReadAssetDelegate() {
        assetSiteAreaDAO = ConfigMapperImpl.getInstance().assetSiteAreaDAO();
        pointLocationsDAO = ConfigMapperImpl.getInstance().pointLocationsDAO();
        pointsDAO = ConfigMapperImpl.getInstance().pointsDAO();
    }

    /**
     * Return a List of AssetSiteArea Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @return List of AssetSiteArea Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AssetSiteArea> getAssetSiteAreaByCustomerSite(String customer, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return assetSiteAreaDAO.findByCustomerSiteId(customer, siteId);
    }

    /**
     * Return a List of AssetSiteArea Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @return List of AssetSiteArea Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AssetSiteArea> getAssetSiteAreaByCustomerSiteArea(String customer, UUID siteId, UUID areaId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return assetSiteAreaDAO.findByCustomerSiteArea(customer, siteId, areaId);
    }

    /**
     * Return a List of AssetSiteArea Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return List of AssetSiteArea Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AssetSiteArea> getAssetSiteAreaByPK(String customer, UUID siteId, UUID areaId, UUID assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return assetSiteAreaDAO.findByPK(customer, siteId, areaId, assetId);
    }

    /**
     * Validate Al_set_id for the given asset
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @return String Object
     */
    public String validateAlSetforPoints(String customer, UUID siteId, UUID areaId, UUID assetId) {
        List<PointLocations> pointLocationsList;
        pointLocationsList = pointLocationsDAO.findByCustomerSiteAreaAsset(customer, siteId, areaId, assetId);
        if (pointLocationsList != null && !pointLocationsList.isEmpty()) {
            for (PointLocations pvo : pointLocationsList) {
                List<Points> pointsList = pointsDAO.findByCustomerSiteAreaAssetLocation(pvo.getCustomerAccount(), pvo.getSiteId(), pvo.getAreaId(), pvo.getAssetId(), pvo.getPointLocationId());
                if (!pointsList.isEmpty()) {
                    for (Points point : pointsList) {
                        if (point.getAlSetId() == null) {
                            return "false";
                        }
                    }
                }
            }
        }
        return "true";
    }

}
