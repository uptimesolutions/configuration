/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.dao;

import com.uptime.config.ConfigService;
import com.uptime.config.vo.DeviceStatusVO;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ksimmons
 */
public class DeviceStatusDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceStatusDAO.class.getName());
    private static final HikariConfig config = new HikariConfig();
    private static final HikariDataSource ds;

    // create a connection pool
    static {
        config.setJdbcUrl(ConfigService.APP_PROPERTIES.getProperty("mqsql.jdbc.url"));
        config.setUsername(ConfigService.APP_PROPERTIES.getProperty("mqsql.jdbc.username"));
        config.setPassword(ConfigService.APP_PROPERTIES.getProperty("mqsql.jdbc.password"));
        config.setDriverClassName(ConfigService.APP_PROPERTIES.getProperty("mqsql.jdbc.driver"));
        config.addDataSourceProperty( "cachePrepStmts" , ConfigService.APP_PROPERTIES.getProperty("hikari.cachePrepStmts") );
        config.addDataSourceProperty( "prepStmtCacheSize" , ConfigService.APP_PROPERTIES.getProperty("hikari.prepStmtCacheSize") );
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , ConfigService.APP_PROPERTIES.getProperty("hikari.prepStmtCacheSqlLimit") );
        ds = new HikariDataSource( config );
    }
    
    private static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
    
    /**
     * Use this method when a device is added or updated in UpCast. The method will automatically check if the device ID already exists in which case an update is performed.
     * @param vo
     * @return 1 if the row was create or updated successfully. 0 if now rows were created or updated.
     */
    public int upsert(DeviceStatusVO vo) throws SQLException{
        Connection conn = null;
        PreparedStatement INSERT_STMT = null;
        try{
            conn = getConnection();
            INSERT_STMT = conn.prepareStatement("INSERT INTO device_status (device_id,customer_acct,site_name,area_name,asset_name,point_location_name,is_disabled) "
                    + "VALUES (?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE customer_acct=?,site_name=?,area_name=?,asset_name=?,point_location_name=?,"
                    + "is_disabled=?");
            INSERT_STMT.setString(1, vo.getDeviceId());
            INSERT_STMT.setString(2, vo.getCustomerAcct());
            INSERT_STMT.setString(3, vo.getSiteName());
            INSERT_STMT.setString(4, vo.getAreaName());
            INSERT_STMT.setString(5, vo.getAssetName());
            INSERT_STMT.setString(6, vo.getPointLocationName());
            INSERT_STMT.setBoolean(7, vo.isIsDisabled());
            INSERT_STMT.setString(8, vo.getCustomerAcct());
            INSERT_STMT.setString(9, vo.getSiteName());
            INSERT_STMT.setString(10, vo.getAreaName());
            INSERT_STMT.setString(11, vo.getAssetName());
            INSERT_STMT.setString(12, vo.getPointLocationName());
            INSERT_STMT.setBoolean(13, vo.isIsDisabled());
//LOGGER.info("INSERT_STMT - " + INSERT_STMT.toString());
            int row = INSERT_STMT.executeUpdate();
            LOGGER.info("upsert row - " + row);
            return row;
        }
        catch(SQLException e){
            LOGGER.error(e.getMessage(), e);
            ConfigService.sendEvent(e.getStackTrace());
        }
        finally{
            if(conn != null){
                try{
                    conn.close();
                }
                catch(SQLException e){
                    LOGGER.error(e.getMessage(), e);
                    ConfigService.sendEvent(e.getStackTrace());
                }
            }
        }
        return -1;
    }
    
    /**
     * Gets the devices for a given customer and site.
     * @param customerId
     * @param siteName
     * @return 
     */
    public List<DeviceStatusVO> findByCustomerSite(String customerId, String siteName){
        Connection conn = null;
        PreparedStatement FIND_BY_CUSTOMER_SITE_STMT = null;
        ArrayList<DeviceStatusVO> voList = new ArrayList();
        try{
            conn = getConnection();
            FIND_BY_CUSTOMER_SITE_STMT = conn.prepareStatement("SELECT * FROM device_status WHERE customer_acct=? AND site_name=?");
            FIND_BY_CUSTOMER_SITE_STMT.setString(1, customerId);
            FIND_BY_CUSTOMER_SITE_STMT.setString(2, siteName);
            ResultSet rs = FIND_BY_CUSTOMER_SITE_STMT.executeQuery();
            while(rs.next()){
                DeviceStatusVO vo = new DeviceStatusVO();
                vo.setAreaName(rs.getString("area_name"));
                vo.setAssetName(rs.getString("asset_name"));
                vo.setBatteryVoltage(rs.getFloat("battery_voltage"));
                vo.setCustomerAcct(rs.getString("customer_acct"));
                vo.setDeviceId(rs.getString("device_id"));
                vo.setIsDisabled(rs.getBoolean("is_disabled"));
                vo.setLastSample(rs.getTimestamp("last_sample"));
                vo.setPointLocationName(rs.getString("point_location_name"));
                vo.setSiteName(rs.getString("site_name"));
                voList.add(vo);
            }
            rs.close();
            return voList;
        }
        catch(SQLException e){
            LOGGER.error(e.getMessage(), e);
            ConfigService.sendEvent(e.getStackTrace());
        }
        finally{
            if(conn != null){
                try{
                    conn.close();
                }
                catch(SQLException e){
                    LOGGER.error(e.getMessage(), e);
                    ConfigService.sendEvent(e.getStackTrace());
                }
            }
        }
        return null;
    }

    /**
     * Use this method when a device is removed from UpCast. The device ID remains but everything else is unassigned.
     * @param deviceId
     * @return 1 if the rows was update successfully, 0 if unsuccessful, -1 if an error occurs
     */
    public int clearDevice(String deviceId){
        Connection conn = null;
        PreparedStatement CLEAR_STMT = null;
        try{
            conn = getConnection();
//LOGGER.info("deviceId - " + deviceId);
            CLEAR_STMT = conn.prepareStatement("UPDATE device_status SET customer_acct=NULL, site_name=NULL, area_name=NULL, asset_name=NULL,"
                    + " point_location_name=NULL, is_disabled=1 WHERE device_id=?");
            CLEAR_STMT.setString(1, deviceId);
            int row = CLEAR_STMT.executeUpdate();
//LOGGER.info("clearDevice row - " + row);
            return row;
        }
        catch(SQLException e){
            LOGGER.error(e.getMessage(), e);
            ConfigService.sendEvent(e.getStackTrace());
        }
        finally{
            if(conn != null){
                try{
                    conn.close();
                }
                catch(SQLException e){
                    LOGGER.error(e.getMessage(), e);
                    ConfigService.sendEvent(e.getStackTrace());
                }
            }
        }
        return -1;
    }
    
    /**
     * Use this method to update the latest voltage and/or last sample time for a given device. To update just the last_sample set the battery_voltage = -1
     * @param vo 
     * @return 1 if the row was updated successfully, otherwise 0, -1 if an error occurs
     */
    public int updateLastSample(DeviceStatusVO vo){
        Connection conn = null;
        PreparedStatement UPDATE_LAST_SAMPLE_STMT = null;
        PreparedStatement UPDATE_VOLTAGE_STMT = null;
        try{
            conn = getConnection();
            // if there is no battery voltage just update the last_sample
            if(vo.getBatteryVoltage() == -1){
                UPDATE_LAST_SAMPLE_STMT = conn.prepareStatement("UPDATE device_status SET last_sample=? WHERE device_id=?");
                UPDATE_LAST_SAMPLE_STMT.setTimestamp(1, vo.getLastSample());
                UPDATE_LAST_SAMPLE_STMT.setString(2, vo.getDeviceId());
                int row = UPDATE_LAST_SAMPLE_STMT.executeUpdate();
                return row;
            }
            // if there is a battery voltage update the voltage and last_sample
            else{
                UPDATE_VOLTAGE_STMT = conn.prepareStatement("UPDATE device_status SET last_sample=?, battery_voltage=? WHERE device_id=?");
                UPDATE_VOLTAGE_STMT.setTimestamp(1, vo.getLastSample());
                UPDATE_VOLTAGE_STMT.setFloat(2, vo.getBatteryVoltage());
                UPDATE_VOLTAGE_STMT.setString(3, vo.getDeviceId());
                int row = UPDATE_VOLTAGE_STMT.executeUpdate();
                return row;
            }
        }
        catch(SQLException e){
            LOGGER.error(e.getMessage(), e);
            ConfigService.sendEvent(e.getStackTrace());
        }
        finally{
            if(conn != null){
                try{
                    conn.close();
                }
                catch(SQLException e){
                    LOGGER.error(e.getMessage(), e);
                    ConfigService.sendEvent(e.getStackTrace());
                }
            }
        }
        return -1;
    }
    
    public static void main(String[] args){
        DeviceStatusDAO dao = new DeviceStatusDAO();
        //DeviceStatusVO vo = new DeviceStatusVO();
        //vo.setAreaName("Area A");
        //vo.setAssetName("Asset A");
        //vo.setBatteryVoltage(3.77f);
        //vo.setCustomerAcct("90000");
        //vo.setDeviceId("BBE041E9");
        //vo.setIsDisabled(false);
        //Timestamp ts = new Timestamp(System.currentTimeMillis());
        //vo.setLastSample(ts);
        //vo.setPointLocationName("Location A");
        //vo.setSiteName("OP");
        //dao.create(vo);
        
        List<DeviceStatusVO> l = dao.findByCustomerSite("90000", "OP");
        for(DeviceStatusVO dev : l){
            System.out.println(dev.getDeviceId());
        }
    }
}
