/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.config;

import com.uptime.config.controller.AreaController;
import com.uptime.config.controller.AssetController;
import com.uptime.config.controller.SiteController;

/**
 *
 * @author Joseph
 */
public class ConfigImport {

    public static void main(String[] args) {
        SiteController sc = new SiteController();
        AreaController areaController;
        AssetController assetController;

        String customer = "77777";
        String siteName = "TEST SITE 001";

        String siteContent = "{"
                + "\"customerAccount\" : \"" + customer + "\", "
                + "\"siteName\" : \"" + siteName + "\", "
                + "\"role\" : \"user\", "
                + "\"contactName\" : \"sam.spade\", "
                + "\"contactTitle\" : \"sam\", "
                + "\"contactPhone\" : \"9999999999\", "
                + "\"contactEmail\" : \"sam.spade@uptime-solutions.us\", "
                + "\"region\" : \"Test region1\", "
                + "\"country\" : \"Test country1\", "
                + "\"physicalAddress1\" : \"Test physicalAddress1\", "
                + "\"physicalAddress2\" : \"Test physicalAddress2\", "
                + "\"physicalCity\" : \"Test physicalCity\", "
                + "\"physicalStateProvince\" : \"Test physicalStateProvince\", "
                + "\"physicalPostalCode\" : \"32567\", "
                + "\"latitude\" : \"Test 11122\", "
                + "\"longitude\" : \"Test 222111\", "
                + "\"timezone\" : \"Test EST\", "
                + "\"industry\" : \"Test industry\", "
                + "\"shipAddress1\" : \"Test shipAddress1\", "
                + "\"shipAddress2\" : \"Test shipAddress2\", "
                + "\"shipCity\" : \"Test shipCity\", "
                + "\"shipStateProvince\" : \"Test shipStateProvince\", "
                + "\"shipPostalCode\" : \"34678\", "
                + "\"billingAddress1\" : \"Test billingAddress1\", "
                + "\"billingAddress2\" : \"Test billingAddress2\", "
                + "\"billingCity\" : \"Test billingCity\", "
                + "\"billingStateProvince\" : \"Test billingStateProvince\", "
                + "\"billingPostalCode\" : \"32890\""
                + "}\n";

        if (sc.createSite(siteContent).equals("{\"outcome\":\"New Site created successfully.\"}")) {

            for (int i = 1; i <= 1; i++) {
                areaController = new AreaController();
                String area = "Test Area " + i;
                
                String areacontent = "{\n"
                        + "\"customerAccount\" : \"" + customer + "\", "
                        + "\"siteName\" : \"" + siteName + "\", "
                        + "\"areaName\" : \"" + area + "\", "
                        + "\"climateControlled\": true,\n"
                        + "\"description\": \"desc11\",\n"
                        + "\"environment\": \"true,false\",\n"
                        + "\"scheduleVOList\": [\n"
                        + "{\n"
                         + "\"customerAccount\" : \"" + customer + "\", "
                        + "\"siteName\" : \"" + siteName + "\", "
                        + "\"areaName\" : \"" + area + "\", "
                        + "\"scheduleName\": \"area51 sc1\",\n"
                        + "\"dayOfWeek\": 2,\n"
                        + "\"schDescription\": \"area51 sch1 desc\",\n"
                        + "\"hoursOfDay\": \"01:00-2:00\",\n"
                        + "\"continuous\": false\n"
                        + "}\n"
                        + "]\n"
                        + "}";
                if (areaController.createArea(areacontent).equals("{\"outcome\":\"New Area created successfully.\"}")) {

                    for (int j = 1; j <= 1; j++) {
                        assetController = new AssetController();
                        String content = "{"
                                + "\"customerAccount\" : \"" + customer + "\", "
                                + "\"siteName\" : \"" + siteName + "\", "
                                + "\"areaName\" : \"" + area + "\", "
                                + "\"assetName\" : \"Asset " + i + j + "\", "
                                + "\"assetType\" : \"Asset Type " + i + j + "\", "
                                + "\"assetId\" : \"Asset Id " + i + j + "\", "
                                + "\"description\" : \"Test Description " + j + "\", "
                                + "\"sampleInterval\" : \"10\""
                                + "}\n";

                        assetController.createAsset(content);
                    }

                }
            }
        }
        System.exit(0);
    }
}
