/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.vo;

import java.util.List;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class SiteVO {
    
    private String customerAccount;
    private UUID siteId;
    private String siteName;
    private String region;
    private String country;
    private String physicalAddress1;
    private String physicalAddress2;
    private String physicalCity;
    private String physicalStateProvince;
    private String physicalPostalCode;
    private String latitude;
    private String longitude;
    private String timezone;
    private String industry;
    private String shipAddress1;
    private String shipAddress2;
    private String shipCity;
    private String shipStateProvince;
    private String shipPostalCode;
    private String billingAddress1;
    private String billingAddress2;
    private String billingCity;
    private String billingStateProvince;
    private String billingPostalCode;
    private List<SiteContactVO> siteContactList;

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhysicalAddress1() {
        return physicalAddress1;
    }

    public void setPhysicalAddress1(String physicalAddress1) {
        this.physicalAddress1 = physicalAddress1;
    }

    public String getPhysicalAddress2() {
        return physicalAddress2;
    }

    public void setPhysicalAddress2(String physicalAddress2) {
        this.physicalAddress2 = physicalAddress2;
    }

    public String getPhysicalCity() {
        return physicalCity;
    }

    public void setPhysicalCity(String physicalCity) {
        this.physicalCity = physicalCity;
    }

    public String getPhysicalStateProvince() {
        return physicalStateProvince;
    }

    public void setPhysicalStateProvince(String physicalStateProvince) {
        this.physicalStateProvince = physicalStateProvince;
    }

    public String getPhysicalPostalCode() {
        return physicalPostalCode;
    }

    public void setPhysicalPostalCode(String physicalPostalCode) {
        this.physicalPostalCode = physicalPostalCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getShipAddress1() {
        return shipAddress1;
    }

    public void setShipAddress1(String shipAddress1) {
        this.shipAddress1 = shipAddress1;
    }

    public String getShipAddress2() {
        return shipAddress2;
    }

    public void setShipAddress2(String shipAddress2) {
        this.shipAddress2 = shipAddress2;
    }

    public String getShipCity() {
        return shipCity;
    }

    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    public String getShipStateProvince() {
        return shipStateProvince;
    }

    public void setShipStateProvince(String shipStateProvince) {
        this.shipStateProvince = shipStateProvince;
    }

    public String getShipPostalCode() {
        return shipPostalCode;
    }

    public void setShipPostalCode(String shipPostalCode) {
        this.shipPostalCode = shipPostalCode;
    }

    public String getBillingAddress1() {
        return billingAddress1;
    }

    public void setBillingAddress1(String billingAddress1) {
        this.billingAddress1 = billingAddress1;
    }

    public String getBillingAddress2() {
        return billingAddress2;
    }

    public void setBillingAddress2(String billingAddress2) {
        this.billingAddress2 = billingAddress2;
    }

    public String getBillingCity() {
        return billingCity;
    }

    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }

    public String getBillingStateProvince() {
        return billingStateProvince;
    }

    public void setBillingStateProvince(String billingStateProvince) {
        this.billingStateProvince = billingStateProvince;
    }

    public String getBillingPostalCode() {
        return billingPostalCode;
    }

    public void setBillingPostalCode(String billingPostalCode) {
        this.billingPostalCode = billingPostalCode;
    }

    public List<SiteContactVO> getSiteContactList() {
        return siteContactList;
    }

    public void setSiteContactList(List<SiteContactVO> siteContactList) {
        this.siteContactList = siteContactList;
    }

    @Override
    public String toString() {
        return "SiteVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", siteName=" + siteName + ", region=" + region + ", country=" + country + ", physicalAddress1=" + physicalAddress1 + ", physicalAddress2=" + physicalAddress2 + ", physicalCity=" + physicalCity + ", physicalStateProvince=" + physicalStateProvince + ", physicalPostalCode=" + physicalPostalCode + ", latitude=" + latitude + ", longitude=" + longitude + ", timezone=" + timezone + ", industry=" + industry + ", shipAddress1=" + shipAddress1 + ", shipAddress2=" + shipAddress2 + ", shipCity=" + shipCity + ", shipStateProvince=" + shipStateProvince + ", shipPostalCode=" + shipPostalCode + ", billingAddress1=" + billingAddress1 + ", billingAddress2=" + billingAddress2 + ", billingCity=" + billingCity + ", billingStateProvince=" + billingStateProvince + ", billingPostalCode=" + billingPostalCode + ", siteContactList=" + siteContactList + '}';
    }

}
