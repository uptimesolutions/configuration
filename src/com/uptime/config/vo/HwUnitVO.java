/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.vo;

import com.uptime.cassandra.config.entity.HwUnitToPoint;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class HwUnitVO {
    private String deviceId;
    private String channelType;
    private byte channelNum;
    private String customerAccount;
    private UUID siteId;
    private UUID areaId;
    private UUID assetId;
    private UUID pointLocationId;
    private UUID pointId;
    private UUID alSetId;
    private UUID apSetId;
    private String pointName;
    private boolean autoAcknowledge;
    private boolean disabled;
    private String pointType;
    private int sampleInterval;
    private int sensorChannelNum;
    private float sensorOffset;
    private float sensorSensitivity;
    private String sensorUnits;
    private boolean useCustomLimits;
    private boolean alarmEnabled;
    private List<ApAlSetVO> apAlSetVOs;
    
    //Added these fields to hold the values for creating the JSON to be sent to Kafka
    private String siteName;
    private String areaName;
    private String assetName;
    private String pointLocationName;
    
    public HwUnitVO(){
        
    }
    
    public HwUnitVO(String deviceId, String channelType, byte channelNum, String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, UUID pointId, UUID apSetId){
        this.deviceId = deviceId;
        this.channelType = channelType;
        this.channelNum = channelNum;
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.areaId = areaId;
        this.assetId = assetId;
        this.pointLocationId = pointLocationId;
        this.pointId = pointId;
        this.apSetId = apSetId;
    }
    
    public HwUnitVO(HwUnitToPoint hwToPoint){
        deviceId = hwToPoint.getDeviceId();
        channelType = hwToPoint.getChannelType();
        channelNum = hwToPoint.getChannelNum();
        customerAccount = hwToPoint.getCustomerAcct();
        siteId = hwToPoint.getSiteId();
        areaId = hwToPoint.getAreaId();
        assetId = hwToPoint.getAssetId();
        pointLocationId = hwToPoint.getPointLocationId();
        pointId = hwToPoint.getPointId();
        apSetId = hwToPoint.getApSetId();
    }
    
    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public byte getChannelNum() {
        return channelNum;
    }

    public void setChannelNum(byte channelNum) {
        this.channelNum = channelNum;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public boolean isAutoAcknowledge() {
        return autoAcknowledge;
    }

    public void setAutoAcknowledge(boolean autoAcknowledge) {
        this.autoAcknowledge = autoAcknowledge;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public int getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(int sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public int getSensorChannelNum() {
        return sensorChannelNum;
    }

    public void setSensorChannelNum(int sensorChannelNum) {
        this.sensorChannelNum = sensorChannelNum;
    }

    public float getSensorOffset() {
        return sensorOffset;
    }

    public void setSensorOffset(float sensorOffset) {
        this.sensorOffset = sensorOffset;
    }

    public float getSensorSensitivity() {
        return sensorSensitivity;
    }

    public void setSensorSensitivity(float sensorSensitivity) {
        this.sensorSensitivity = sensorSensitivity;
    }

    public String getSensorUnits() {
        return sensorUnits;
    }

    public void setSensorUnits(String sensorUnits) {
        this.sensorUnits = sensorUnits;
    }

    public boolean isUseCustomLimits() {
        return useCustomLimits;
    }

    public void setUseCustomLimits(boolean useCustomLimits) {
        this.useCustomLimits = useCustomLimits;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }

    public List<ApAlSetVO> getApAlSetVOs() {
        return apAlSetVOs;
    }

    public void setApAlSetVOs(List<ApAlSetVO> apAlSetVOs) {
        this.apAlSetVOs = apAlSetVOs;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.deviceId);
        hash = 97 * hash + Objects.hashCode(this.channelType);
        hash = 97 * hash + this.channelNum;
        hash = 97 * hash + Objects.hashCode(this.customerAccount);
        hash = 97 * hash + Objects.hashCode(this.siteId);
        hash = 97 * hash + Objects.hashCode(this.areaId);
        hash = 97 * hash + Objects.hashCode(this.assetId);
        hash = 97 * hash + Objects.hashCode(this.pointLocationId);
        hash = 97 * hash + Objects.hashCode(this.pointId);
        hash = 97 * hash + Objects.hashCode(this.alSetId);
        hash = 97 * hash + Objects.hashCode(this.apSetId);
        hash = 97 * hash + Objects.hashCode(this.pointName);
        hash = 97 * hash + (this.autoAcknowledge ? 1 : 0);
        hash = 97 * hash + (this.disabled ? 1 : 0);
        hash = 97 * hash + Objects.hashCode(this.pointType);
        hash = 97 * hash + this.sampleInterval;
        hash = 97 * hash + this.sensorChannelNum;
        hash = 97 * hash + Float.floatToIntBits(this.sensorOffset);
        hash = 97 * hash + Float.floatToIntBits(this.sensorSensitivity);
        hash = 97 * hash + Objects.hashCode(this.sensorUnits);
        hash = 97 * hash + (this.useCustomLimits ? 1 : 0);
        hash = 97 * hash + (this.alarmEnabled ? 1 : 0);
        hash = 97 * hash + Objects.hashCode(this.apAlSetVOs);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HwUnitVO other = (HwUnitVO) obj;
        if (this.channelNum != other.channelNum) {
            return false;
        }
        if (this.autoAcknowledge != other.autoAcknowledge) {
            return false;
        }
        if (this.disabled != other.disabled) {
            return false;
        }
        if (this.sampleInterval != other.sampleInterval) {
            return false;
        }
        if (this.sensorChannelNum != other.sensorChannelNum) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorOffset) != Float.floatToIntBits(other.sensorOffset)) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorSensitivity) != Float.floatToIntBits(other.sensorSensitivity)) {
            return false;
        }
        if (this.useCustomLimits != other.useCustomLimits) {
            return false;
        }
        if (this.alarmEnabled != other.alarmEnabled) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.channelType, other.channelType)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.pointName, other.pointName)) {
            return false;
        }
        if (!Objects.equals(this.pointType, other.pointType)) {
            return false;
        }
        if (!Objects.equals(this.sensorUnits, other.sensorUnits)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.apAlSetVOs, other.apAlSetVOs)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "HwUnitVO{" + "deviceId=" + deviceId + ", channelType=" + channelType + ", channelNum=" + channelNum + ", customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", alSetId=" + alSetId + ", apSetId=" + apSetId + ", pointName=" + pointName + ", autoAcknowledge=" + autoAcknowledge + ", disabled=" + disabled + ", pointType=" + pointType + ", sampleInterval=" + sampleInterval + ", sensorChannelNum=" + sensorChannelNum + ", sensorOffset=" + sensorOffset + ", sensorSensitivity=" + sensorSensitivity + ", sensorUnits=" + sensorUnits + ", useCustomLimits=" + useCustomLimits + ", alarmEnabled=" + alarmEnabled + ", apAlSetVOs=" + apAlSetVOs + "}";
    }

}
