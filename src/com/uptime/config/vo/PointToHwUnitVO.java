/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.vo;

import com.uptime.cassandra.config.entity.PointToHwUnit;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class PointToHwUnitVO {
    
    private String deviceId;
    private String channelType;
    private byte channelNum;
    private UUID pointId;
    private String request;
    
    public PointToHwUnitVO(){
        
    }
    
    public PointToHwUnitVO(PointToHwUnit pointToHwUnit){
        this.deviceId = pointToHwUnit.getDeviceId();
        this.channelType = pointToHwUnit.getChannelType();
        this.channelNum = pointToHwUnit.getChannelNum();
        this.pointId = pointToHwUnit.getPointId();
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public byte getChannelNum() {
        return channelNum;
    }

    public void setChannelNum(byte channelNum) {
        this.channelNum = channelNum;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    @Override
    public String toString() {
        return "PointToHwUnitVO{" + "deviceId=" + deviceId + ", channelType=" + channelType + ", channelNum=" + channelNum + ", pointId=" + pointId + ", request=" + request + '}';
    }
    
}
