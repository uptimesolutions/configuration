/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.vo;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class AssetVO {
    private String customerAccount;
    private UUID siteId;
    private UUID areaId;
    private UUID assetId;
    private String assetName;
    private String assetType;
    private String description;
    private int sampleInterval;
    private boolean alarmEnabled;
    private List<PointLocationVO> pointLocationList;
    private List<String> sensorList;

    //Used for Kafka
    private String siteName;
    private String areaName;
    
    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(int sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }

    public List<PointLocationVO> getPointLocationList() {
        return pointLocationList;
    }

    public void setPointLocationList(List<PointLocationVO> pointLocationList) {
        this.pointLocationList = pointLocationList;
    }

    public List<String> getSensorList() {
        return sensorList;
    }

    public void setSensorList(List<String> sensorList) {
        this.sensorList = sensorList;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.customerAccount);
        hash = 89 * hash + Objects.hashCode(this.siteId);
        hash = 89 * hash + Objects.hashCode(this.areaId);
        hash = 89 * hash + Objects.hashCode(this.assetId);
        hash = 89 * hash + Objects.hashCode(this.pointLocationList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AssetVO other = (AssetVO) obj;
        if (this.sampleInterval != other.sampleInterval) {
            return false;
        }
        if (this.alarmEnabled != other.alarmEnabled) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetType, other.assetType)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationList, other.pointLocationList)) {
            return false;
        }
        if (!Objects.equals(this.sensorList, other.sensorList)) {
            return false;
        }
        if (!Objects.equals(this.sensorList, other.sensorList)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AssetVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", assetName=" + assetName + ", assetType=" + assetType + ", description=" + description + ", sampleInterval=" + sampleInterval + ", alarmEnabled=" + alarmEnabled + ", pointLocationList=" + pointLocationList + ", sensorList=" + sensorList + ", siteName=" + siteName + ", areaName=" + areaName + "}";
    }
}
