/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.vo;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class DeviceVO {
    
    private String existingDeviceId;
    private String newDeviceId;
    private String channelType;
    private byte channelNum;
    private String channelNumLabel;
    private String customerAccount;
    private UUID siteId;
    private UUID areaId;
    private UUID assetId;
    private UUID pointLocationId;
    private UUID pointId;
    private UUID alSetId;
    private UUID apSetId;
    private String pointName;
    private boolean autoAcknowledge;
    private boolean disabled;
    private String pointType;
    private int sampleInterval;
    private int sensorChannelNum;
    private float sensorOffset;
    private float sensorSensitivity;
    private String deviceType;
    private String sensorUnits;
    private String sensorSubType;
    private String sensorLocalOrientation;
    private boolean useCustomLimits;
    private boolean alarmEnabled;
    private List<ApAlSetVO> apAlSetVOs;
    private String restrictions;
    private List<PointVO> pointList;
    private boolean dbpointListExists;
    private short basestationPortNum;
    
    //Added these fields to hold the values for DeviceStatus
    private Timestamp  lastSample;
    private float batteryVoltage;
    
    //Added these fields to hold the values for creating the JSON to be sent to Kafka
    private String siteName;
    private String areaName;
    private String assetName;
    private String pointLocationName;
    private String request;
    
    public DeviceVO(){
        
    }

    public String getExistingDeviceId() {
        return existingDeviceId;
    }

    public void setExistingDeviceId(String existingDeviceId) {
        this.existingDeviceId = existingDeviceId;
    }

    public String getNewDeviceId() {
        return newDeviceId;
    }

    public void setNewDeviceId(String newDeviceId) {
        this.newDeviceId = newDeviceId;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public byte getChannelNum() {
        return channelNum;
    }

    public void setChannelNum(byte channelNum) {
        this.channelNum = channelNum;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public boolean isAutoAcknowledge() {
        return autoAcknowledge;
    }

    public void setAutoAcknowledge(boolean autoAcknowledge) {
        this.autoAcknowledge = autoAcknowledge;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public int getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(int sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public int getSensorChannelNum() {
        return sensorChannelNum;
    }

    public void setSensorChannelNum(int sensorChannelNum) {
        this.sensorChannelNum = sensorChannelNum;
    }

    public float getSensorOffset() {
        return sensorOffset;
    }

    public void setSensorOffset(float sensorOffset) {
        this.sensorOffset = sensorOffset;
    }

    public float getSensorSensitivity() {
        return sensorSensitivity;
    }

    public void setSensorSensitivity(float sensorSensitivity) {
        this.sensorSensitivity = sensorSensitivity;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
    
    public String getSensorUnits() {
        return sensorUnits;
    }

    public void setSensorUnits(String sensorUnits) {
        this.sensorUnits = sensorUnits;
    }

    public boolean isUseCustomLimits() {
        return useCustomLimits;
    }

    public void setUseCustomLimits(boolean useCustomLimits) {
        this.useCustomLimits = useCustomLimits;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }

    public List<ApAlSetVO> getApAlSetVOs() {
        return apAlSetVOs;
    }

    public void setApAlSetVOs(List<ApAlSetVO> apAlSetVOs) {
        this.apAlSetVOs = apAlSetVOs;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public String getChannelNumLabel() {
        return channelNumLabel;
    }

    public void setChannelNumLabel(String channelNumLabel) {
        this.channelNumLabel = channelNumLabel;
    }

    public String getRestrictions() {
        return restrictions;
    }

    public void setRestrictions(String restrictions) {
        this.restrictions = restrictions;
    }

    public List<PointVO> getPointList() {
        return pointList;
    }

    public void setPointList(List<PointVO> pointList) {
        this.pointList = pointList;
    }

    public boolean isDbpointListExists() {
        return dbpointListExists;
    }

    public void setDbpointListExists(boolean dbpointListExists) {
        this.dbpointListExists = dbpointListExists;
    }

    public short getBasestationPortNum() {
        return basestationPortNum;
    }

    public void setBasestationPortNum(short basestationPortNum) {
        this.basestationPortNum = basestationPortNum;
    }

    public Timestamp getLastSample() {
        return lastSample;
    }

    public void setLastSample(Timestamp lastSample) {
        this.lastSample = lastSample;
    }

    public float getBatteryVoltage() {
        return batteryVoltage;
    }

    public void setBatteryVoltage(float batteryVoltage) {
        this.batteryVoltage = batteryVoltage;
    }

    public String getSensorSubType() {
        return sensorSubType;
    }

    public void setSensorSubType(String sensorSubType) {
        this.sensorSubType = sensorSubType;
    }

    public String getSensorLocalOrientation() {
        return sensorLocalOrientation;
    }

    public void setSensorLocalOrientation(String sensorLocalOrientation) {
        this.sensorLocalOrientation = sensorLocalOrientation;
    }

    @Override
    public String toString() {
        return "DeviceVO{" + "existingDeviceId=" + existingDeviceId + ", newDeviceId=" + newDeviceId + ", channelType=" + channelType + ", channelNum=" + channelNum + ", channelNumLabel=" + channelNumLabel + ", customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointId=" + pointId + ", alSetId=" + alSetId + ", apSetId=" + apSetId + ", pointName=" + pointName + ", autoAcknowledge=" + autoAcknowledge + ", disabled=" + disabled + ", pointType=" + pointType + ", sampleInterval=" + sampleInterval + ", sensorChannelNum=" + sensorChannelNum + ", sensorOffset=" + sensorOffset + ", sensorSensitivity=" + sensorSensitivity + ", sensorUnits=" + sensorUnits + ", sensorSubType=" + sensorSubType + ", sensorLocalOrientation=" + sensorLocalOrientation + ", useCustomLimits=" + useCustomLimits + ", alarmEnabled=" + alarmEnabled + ", apAlSetVOs=" + apAlSetVOs + ", restrictions=" + restrictions + ", pointList=" + pointList + ", dbpointListExists=" + dbpointListExists + ", basestationPortNum=" + basestationPortNum + ", lastSample=" + lastSample + ", batteryVoltage=" + batteryVoltage + ", siteName=" + siteName + ", areaName=" + areaName + ", assetName=" + assetName + ", pointLocationName=" + pointLocationName + ", request=" + request + '}';
    }
    
}
