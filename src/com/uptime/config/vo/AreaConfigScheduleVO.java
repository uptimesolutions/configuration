/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.vo;

import java.io.Serializable;
import java.util.UUID;

/**
 *
 * @author gsingh
 */
public class AreaConfigScheduleVO implements Serializable {

    String customerAccount;
    UUID siteId;
    UUID areaId;
    UUID scheduleId;
    String areaName;
    String scheduleName;
    byte dayOfWeek;
    String schDescription;
    String hoursOfDay;
    boolean continuous;
    String type;

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    public byte getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(byte dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getSchDescription() {
        return schDescription;
    }

    public void setSchDescription(String schDescription) {
        this.schDescription = schDescription;
    }

    public String getHoursOfDay() {
        return hoursOfDay;
    }

    public void setHoursOfDay(String hoursOfDay) {
        this.hoursOfDay = hoursOfDay;
    }

    public boolean isContinuous() {
        return continuous;
    }

    public void setContinuous(boolean continuous) {
        this.continuous = continuous;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(UUID scheduleId) {
        this.scheduleId = scheduleId;
    }

    @Override
    public String toString() {
        return "AreaConfigScheduleVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", scheduleId=" + scheduleId + ", areaName=" + areaName + ", scheduleName=" + scheduleName + ", dayOfWeek=" + dayOfWeek + ", schDescription=" + schDescription + ", hoursOfDay=" + hoursOfDay + ", continuous=" + continuous + ", type=" + type + '}';
    }

}
