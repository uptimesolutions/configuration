/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.config.vo;

import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.cassandra.presets.entity.GlobalApAlSets;
import com.uptime.cassandra.presets.entity.SiteApAlSets;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author Joseph
 */
public class ApAlSetVO implements Serializable {
    private String customerAccount;
    private UUID siteId;
    private UUID apSetId;
    private UUID alSetId;
    private String paramName;
    private String sensorType;
    private int fmax;
    private int resolution;
    private float period;
    private float sampleRate;
    private boolean demod;
    private float demodHighPass;
    private float demodLowPass;
    private String paramType;
    private String frequencyUnits;
    private String paramUnits;
    private String paramAmpFactor;
    private float minFrequency;
    private float maxFrequency;
    private int dwell;
    private boolean lowFaultActive;
    private boolean lowAlertActive;
    private boolean highAlertActive;
    private boolean highFaultActive;
    private float lowFault;
    private float lowAlert;
    private float highAlert;
    private float highFault;
    private String apSetName;
    private String alSetName;
    
    public ApAlSetVO() {
    }

    public ApAlSetVO(String customerAccount, UUID siteId, UUID apSetId, UUID alSetId, String paramName, String sensorType, int fmax, int resolution, float period, float sampleRate, boolean demod, float demodHighPass, float demodLowPass, String paramType, String frequencyUnits, String paramUnits, String paramAmpFactor, float minFrequency, float maxFrequency, int dwell, boolean lowFaultActive, boolean lowAlertActive, boolean highAlertActive, boolean highFaultActive, float lowFault, float lowAlert, float highAlert, float highFault, String alsetName, String apSetName) {
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.apSetId = apSetId;
        this.alSetId = alSetId;
        this.paramName = paramName;
        this.sensorType = sensorType;
        this.fmax = fmax;
        this.resolution = resolution;
        this.period = period;
        this.sampleRate = sampleRate;
        this.demod = demod;
        this.demodHighPass = demodHighPass;
        this.demodLowPass = demodLowPass;
        this.paramType = paramType;
        this.frequencyUnits = frequencyUnits;
        this.paramUnits = paramUnits;
        this.paramAmpFactor = paramAmpFactor;
        this.minFrequency = minFrequency;
        this.maxFrequency = maxFrequency;
        this.dwell = dwell;
        this.lowFaultActive = lowFaultActive;
        this.lowAlertActive = lowAlertActive;
        this.highAlertActive = highAlertActive;
        this.highFaultActive = highFaultActive;
        this.lowFault = lowFault;
        this.lowAlert = lowAlert;
        this.highAlert = highAlert;
        this.highFault = highFault;
        this.alSetName = alsetName;
        this.apSetName = apSetName;
    }

    public ApAlSetVO(ApAlSetVO apAlSetVO) {
        customerAccount = apAlSetVO.getCustomerAccount();
        siteId = apAlSetVO.getSiteId();
        apSetId = apAlSetVO.getApSetId();
        alSetId = apAlSetVO.getAlSetId();
        paramName = apAlSetVO.getParamName();
        sensorType = apAlSetVO.getSensorType();
        fmax = apAlSetVO.getFmax();
        resolution = apAlSetVO.getResolution();
        period = apAlSetVO.getPeriod();
        sampleRate = apAlSetVO.getSampleRate();
        demod = apAlSetVO.isDemod();
        demodHighPass = apAlSetVO.getDemodHighPass();
        demodLowPass = apAlSetVO.getDemodLowPass();
        paramType = apAlSetVO.getParamType();
        frequencyUnits = apAlSetVO.getFrequencyUnits();
        paramUnits = apAlSetVO.getParamUnits();
        paramAmpFactor = apAlSetVO.getParamAmpFactor();
        minFrequency = apAlSetVO.getMinFrequency();
        maxFrequency = apAlSetVO.getMaxFrequency();
        dwell = apAlSetVO.getDwell();
        lowFaultActive = apAlSetVO.isLowFaultActive();
        lowAlertActive = apAlSetVO.isLowAlertActive();
        highAlertActive = apAlSetVO.isHighAlertActive();
        highFaultActive = apAlSetVO.isHighFaultActive();
        lowFault = apAlSetVO.getLowFault();
        lowAlert = apAlSetVO.getLowAlert();
        highAlert = apAlSetVO.getHighAlert();
        highFault = apAlSetVO.getHighFault();
        alSetName = apAlSetVO.getAlSetName();
        apSetName = apAlSetVO.getApSetName();
    }

    public ApAlSetVO(GlobalApAlSets globalApAlSets) {
        customerAccount = globalApAlSets.getCustomerAccount();
        apSetId = globalApAlSets.getApSetId();
        alSetId = globalApAlSets.getAlSetId();
        paramName = globalApAlSets.getParamName();
        sensorType = globalApAlSets.getSensorType();
        fmax = globalApAlSets.getFmax();
        resolution = globalApAlSets.getResolution();
        period = globalApAlSets.getPeriod();
        sampleRate = globalApAlSets.getSampleRate();
        demod = globalApAlSets.isDemod();
        demodHighPass = globalApAlSets.getDemodHighPass();
        demodLowPass = globalApAlSets.getDemodLowPass();
        paramType = globalApAlSets.getParamType();
        frequencyUnits = globalApAlSets.getFrequencyUnits();
        paramUnits = globalApAlSets.getParamUnits();
        paramAmpFactor = globalApAlSets.getParamAmpFactor();
        minFrequency = globalApAlSets.getMinFrequency();
        maxFrequency = globalApAlSets.getMaxFrequency();
        dwell = globalApAlSets.getDwell();
        lowFaultActive = globalApAlSets.isLowFaultActive();
        lowAlertActive = globalApAlSets.isLowAlertActive();
        highAlertActive = globalApAlSets.isHighAlertActive();
        highFaultActive = globalApAlSets.isHighFaultActive();
        lowFault = globalApAlSets.getLowFault();
        lowAlert = globalApAlSets.getLowAlert();
        highAlert = globalApAlSets.getHighAlert();
        highFault = globalApAlSets.getHighFault();
        alSetName = globalApAlSets.getAlSetName();
        apSetName = globalApAlSets.getApSetName();
    }

    public ApAlSetVO(GlobalApAlSets globalApAlSets, UUID siteId) {
        customerAccount = globalApAlSets.getCustomerAccount();
        this.siteId = siteId;
        apSetId = globalApAlSets.getApSetId();
        alSetId = globalApAlSets.getAlSetId();
        paramName = globalApAlSets.getParamName();
        sensorType = globalApAlSets.getSensorType();
        fmax = globalApAlSets.getFmax();
        resolution = globalApAlSets.getResolution();
        period = globalApAlSets.getPeriod();
        sampleRate = globalApAlSets.getSampleRate();
        demod = globalApAlSets.isDemod();
        demodHighPass = globalApAlSets.getDemodHighPass();
        demodLowPass = globalApAlSets.getDemodLowPass();
        paramType = globalApAlSets.getParamType();
        frequencyUnits = globalApAlSets.getFrequencyUnits();
        paramUnits = globalApAlSets.getParamUnits();
        paramAmpFactor = globalApAlSets.getParamAmpFactor();
        minFrequency = globalApAlSets.getMinFrequency();
        maxFrequency = globalApAlSets.getMaxFrequency();
        dwell = globalApAlSets.getDwell();
        lowFaultActive = globalApAlSets.isLowFaultActive();
        lowAlertActive = globalApAlSets.isLowAlertActive();
        highAlertActive = globalApAlSets.isHighAlertActive();
        highFaultActive = globalApAlSets.isHighFaultActive();
        lowFault = globalApAlSets.getLowFault();
        lowAlert = globalApAlSets.getLowAlert();
        highAlert = globalApAlSets.getHighAlert();
        highFault = globalApAlSets.getHighFault();
        alSetName = globalApAlSets.getAlSetName();
        apSetName = globalApAlSets.getApSetName();
    }

    public ApAlSetVO(SiteApAlSets siteApAlSets) {
        customerAccount = siteApAlSets.getCustomerAccount();
        siteId = siteApAlSets.getSiteId();
        apSetId = siteApAlSets.getApSetId();
        alSetId = siteApAlSets.getAlSetId();
        paramName = siteApAlSets.getParamName();
        sensorType = siteApAlSets.getSensorType();
        fmax = siteApAlSets.getFmax();
        resolution = siteApAlSets.getResolution();
        period = siteApAlSets.getPeriod();
        sampleRate = siteApAlSets.getSampleRate();
        demod = siteApAlSets.isDemod();
        demodHighPass = siteApAlSets.getDemodHighPass();
        demodLowPass = siteApAlSets.getDemodLowPass();
        paramType = siteApAlSets.getParamType();
        frequencyUnits = siteApAlSets.getFrequencyUnits();
        paramUnits = siteApAlSets.getParamUnits();
        paramAmpFactor = siteApAlSets.getParamAmpFactor();
        minFrequency = siteApAlSets.getMinFrequency();
        maxFrequency = siteApAlSets.getMaxFrequency();
        dwell = siteApAlSets.getDwell();
        lowFaultActive = siteApAlSets.isLowFaultActive();
        lowAlertActive = siteApAlSets.isLowAlertActive();
        highAlertActive = siteApAlSets.isHighAlertActive();
        highFaultActive = siteApAlSets.isHighFaultActive();
        lowFault = siteApAlSets.getLowFault();
        lowAlert = siteApAlSets.getLowAlert();
        highAlert = siteApAlSets.getHighAlert();
        highFault = siteApAlSets.getHighFault();
        alSetName = siteApAlSets.getAlSetName();
        apSetName = siteApAlSets.getApSetName();
    }

    public ApAlSetVO(ApAlByPoint apAlByPoint) {
        customerAccount = apAlByPoint.getCustomerAccount();
        siteId = apAlByPoint.getSiteId();
        apSetId = apAlByPoint.getApSetId();
        alSetId = apAlByPoint.getAlSetId();
        paramName = apAlByPoint.getParamName();
        sensorType = apAlByPoint.getSensorType();
        fmax = apAlByPoint.getfMax();
        resolution = apAlByPoint.getResolution();
        period = apAlByPoint.getPeriod();
        sampleRate = apAlByPoint.getSampleRate();
        demod = apAlByPoint.isDemodEnabled();
        demodHighPass = apAlByPoint.getDemodHighPass();
        demodLowPass = apAlByPoint.getDemodLowPass();
        paramType = apAlByPoint.getParamType();
        frequencyUnits = apAlByPoint.getFreqUnits();
        paramUnits = apAlByPoint.getParamUnits();
        paramAmpFactor = apAlByPoint.getParamAmpFactor();
        minFrequency = apAlByPoint.getMinFreq();
        maxFrequency = apAlByPoint.getMaxFreq();
        dwell = apAlByPoint.getDwell();
        lowFaultActive = apAlByPoint.isLowFaultActive();
        lowAlertActive = apAlByPoint.isLowAlertActive();
        highAlertActive = apAlByPoint.isHighAlertActive();
        highFaultActive = apAlByPoint.isHighFaultActive();
        lowFault = apAlByPoint.getLowFault();
        lowAlert = apAlByPoint.getLowAlert();
        highAlert = apAlByPoint.getHighAlert();
        highFault = apAlByPoint.getHighFault();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public int getFmax() {
        return fmax;
    }

    public void setFmax(int fmax) {
        this.fmax = fmax;
    }

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

    public float getPeriod() {
        return period;
    }

    public void setPeriod(float period) {
        this.period = period;
    }

    public float getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(float sampleRate) {
        this.sampleRate = sampleRate;
    }

    public boolean isDemod() {
        return demod;
    }

    public void setDemod(boolean demod) {
        this.demod = demod;
    }

    public float getDemodHighPass() {
        return demodHighPass;
    }

    public void setDemodHighPass(float demodHighPass) {
        this.demodHighPass = demodHighPass;
    }

    public float getDemodLowPass() {
        return demodLowPass;
    }

    public void setDemodLowPass(float demodLowPass) {
        this.demodLowPass = demodLowPass;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public String getFrequencyUnits() {
        return frequencyUnits;
    }

    public void setFrequencyUnits(String frequencyUnits) {
        this.frequencyUnits = frequencyUnits;
    }

    public String getParamUnits() {
        return paramUnits;
    }

    public void setParamUnits(String paramUnits) {
        this.paramUnits = paramUnits;
    }

    public String getParamAmpFactor() {
        return paramAmpFactor;
    }

    public void setParamAmpFactor(String paramAmpFactor) {
        this.paramAmpFactor = paramAmpFactor;
    }

    public float getMinFrequency() {
        return minFrequency;
    }

    public void setMinFrequency(float minFrequency) {
        this.minFrequency = minFrequency;
    }

    public float getMaxFrequency() {
        return maxFrequency;
    }

    public void setMaxFrequency(float maxFrequency) {
        this.maxFrequency = maxFrequency;
    }

    public int getDwell() {
        return dwell;
    }

    public void setDwell(int dwell) {
        this.dwell = dwell;
    }

    public boolean isLowFaultActive() {
        return lowFaultActive;
    }

    public void setLowFaultActive(boolean lowFaultActive) {
        this.lowFaultActive = lowFaultActive;
    }

    public boolean isLowAlertActive() {
        return lowAlertActive;
    }

    public void setLowAlertActive(boolean lowAlertActive) {
        this.lowAlertActive = lowAlertActive;
    }

    public boolean isHighAlertActive() {
        return highAlertActive;
    }

    public void setHighAlertActive(boolean highAlertActive) {
        this.highAlertActive = highAlertActive;
    }

    public boolean isHighFaultActive() {
        return highFaultActive;
    }

    public void setHighFaultActive(boolean highFaultActive) {
        this.highFaultActive = highFaultActive;
    }

    public float getLowFault() {
        return lowFault;
    }

    public void setLowFault(float lowFault) {
        this.lowFault = lowFault;
    }

    public float getLowAlert() {
        return lowAlert;
    }

    public void setLowAlert(float lowAlert) {
        this.lowAlert = lowAlert;
    }

    public float getHighAlert() {
        return highAlert;
    }

    public void setHighAlert(float highAlert) {
        this.highAlert = highAlert;
    }

    public float getHighFault() {
        return highFault;
    }

    public void setHighFault(float highFault) {
        this.highFault = highFault;
    }

    public String getApSetName() {
        return apSetName;
    }

    public void setApSetName(String apSetName) {
        this.apSetName = apSetName;
    }

    public String getAlSetName() {
        return alSetName;
    }

    public void setAlSetName(String alSetName) {
        this.alSetName = alSetName;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.customerAccount);
        hash = 29 * hash + Objects.hashCode(this.siteId);
        hash = 29 * hash + Objects.hashCode(this.apSetId);
        hash = 29 * hash + Objects.hashCode(this.alSetId);
        hash = 29 * hash + Objects.hashCode(this.paramName);
        hash = 29 * hash + Objects.hashCode(this.sensorType);
        hash = 29 * hash + this.fmax;
        hash = 29 * hash + this.resolution;
        hash = 29 * hash + Float.floatToIntBits(this.period);
        hash = 29 * hash + Float.floatToIntBits(this.sampleRate);
        hash = 29 * hash + (this.demod ? 1 : 0);
        hash = 29 * hash + Float.floatToIntBits(this.demodHighPass);
        hash = 29 * hash + Float.floatToIntBits(this.demodLowPass);
        hash = 29 * hash + Objects.hashCode(this.paramType);
        hash = 29 * hash + Objects.hashCode(this.frequencyUnits);
        hash = 29 * hash + Objects.hashCode(this.paramUnits);
        hash = 29 * hash + Objects.hashCode(this.paramAmpFactor);
        hash = 29 * hash + Float.floatToIntBits(this.minFrequency);
        hash = 29 * hash + Float.floatToIntBits(this.maxFrequency);
        hash = 29 * hash + this.dwell;
        hash = 29 * hash + (this.lowFaultActive ? 1 : 0);
        hash = 29 * hash + (this.lowAlertActive ? 1 : 0);
        hash = 29 * hash + (this.highAlertActive ? 1 : 0);
        hash = 29 * hash + (this.highFaultActive ? 1 : 0);
        hash = 29 * hash + Float.floatToIntBits(this.lowFault);
        hash = 29 * hash + Float.floatToIntBits(this.lowAlert);
        hash = 29 * hash + Float.floatToIntBits(this.highAlert);
        hash = 29 * hash + Float.floatToIntBits(this.highFault);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApAlSetVO other = (ApAlSetVO) obj;
        if (this.fmax != other.fmax) {
            return false;
        }
        if (this.resolution != other.resolution) {
            return false;
        }
        if (Float.floatToIntBits(this.period) != Float.floatToIntBits(other.period)) {
            return false;
        }
        if (Float.floatToIntBits(this.sampleRate) != Float.floatToIntBits(other.sampleRate)) {
            return false;
        }
        if (this.demod != other.demod) {
            return false;
        }
        if (Float.floatToIntBits(this.demodHighPass) != Float.floatToIntBits(other.demodHighPass)) {
            return false;
        }
        if (Float.floatToIntBits(this.demodLowPass) != Float.floatToIntBits(other.demodLowPass)) {
            return false;
        }
        if (Float.floatToIntBits(this.minFrequency) != Float.floatToIntBits(other.minFrequency)) {
            return false;
        }
        if (Float.floatToIntBits(this.maxFrequency) != Float.floatToIntBits(other.maxFrequency)) {
            return false;
        }
        if (this.dwell != other.dwell) {
            return false;
        }
        if (this.lowFaultActive != other.lowFaultActive) {
            return false;
        }
        if (this.lowAlertActive != other.lowAlertActive) {
            return false;
        }
        if (this.highAlertActive != other.highAlertActive) {
            return false;
        }
        if (this.highFaultActive != other.highFaultActive) {
            return false;
        }
        if (Float.floatToIntBits(this.lowFault) != Float.floatToIntBits(other.lowFault)) {
            return false;
        }
        if (Float.floatToIntBits(this.lowAlert) != Float.floatToIntBits(other.lowAlert)) {
            return false;
        }
        if (Float.floatToIntBits(this.highAlert) != Float.floatToIntBits(other.highAlert)) {
            return false;
        }
        if (Float.floatToIntBits(this.highFault) != Float.floatToIntBits(other.highFault)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.paramType, other.paramType)) {
            return false;
        }
        if (!Objects.equals(this.frequencyUnits, other.frequencyUnits)) {
            return false;
        }
        if (!Objects.equals(this.paramUnits, other.paramUnits)) {
            return false;
        }
        if (!Objects.equals(this.paramAmpFactor, other.paramAmpFactor)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ApAlSetVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", apSetId=" + apSetId + ", alSetId=" + alSetId + ", paramName=" + paramName + ", sensorType=" + sensorType + ", fmax=" + fmax + ", resolution=" + resolution + ", period=" + period + ", sampleRate=" + sampleRate + ", demod=" + demod + ", demodHighPass=" + demodHighPass + ", demodLowPass=" + demodLowPass + ", paramType=" + paramType + ", frequencyUnits=" + frequencyUnits + ", paramUnits=" + paramUnits + ", paramAmpFactor=" + paramAmpFactor + ", minFrequency=" + minFrequency + ", maxFrequency=" + maxFrequency + ", dwell=" + dwell + ", lowFaultActive=" + lowFaultActive + ", lowAlertActive=" + lowAlertActive + ", highAlertActive=" + highAlertActive + ", highFaultActive=" + highFaultActive + ", lowFault=" + lowFault + ", lowAlert=" + lowAlert + ", highAlert=" + highAlert + ", highFault=" + highFault + ", apSetName=" + apSetName + ", alSetName=" + alSetName + "}";
    }
}
