/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class ChannelVO implements Serializable {
    private String customerAccount;
    private String deviceId;
    private UUID siteId;
    private UUID areaId;
    private String areaName;
    private UUID assetId;
    private String assetName;
    private UUID pointLocationId;
    private String pointLocationName;
    private String pointLocationDesc;
    private UUID pointId;
    private String pointName;
    private UUID apSetId;
    private String apSetName;
    private UUID alSetId;
    private String alSetName;
    private String pointType;
    private short basestationPortNum;
    private int sensorChannelNum;
    private float sensorOffset;
    private float sensorSensitivity;
    private String sensorType;
    private String sensorUnits;
    private String ffSetIds;
    private UUID tachId;
    private String tachReferenceUnits;
    private float rollDiameter;
    private String rollDiameterUnits;
    private float speedRatio;
    private int fMax;
    private int resolution;
    private float period;
    private float sampleRate;
    private short sampleInterval;
    private boolean alarmEnabled;
    private List<ApAlSetVO> apAlSetVOs;

    public ChannelVO() {
    }

    public ChannelVO(ChannelVO channelVO) {
        customerAccount = channelVO.getCustomerAccount();
        siteId = channelVO.getSiteId();
        areaId = channelVO.getAreaId();
        assetId = channelVO.getAssetId();
        pointLocationId = channelVO.getPointLocationId();
        pointId = channelVO.getPointId();
        apSetId = channelVO.getApSetId();
        alSetId = channelVO.getAlSetId();
        pointName = channelVO.getPointName();
        pointType = channelVO.getPointType();
        sensorChannelNum = channelVO.getSensorChannelNum();
        sensorOffset = channelVO.getSensorOffset();
        sensorSensitivity = channelVO.getSensorSensitivity();
        sensorType = channelVO.getSensorType();
        sensorUnits = channelVO.getSensorUnits();
        fMax = channelVO.getfMax();
        areaName = channelVO.getAreaName();
        assetName = channelVO.getAssetName();
        pointLocationName = channelVO.getPointLocationName();
        pointLocationDesc = channelVO.getPointLocationDesc();
        apSetName = channelVO.getApSetName();
        alSetName = channelVO.getAlSetName();
        alarmEnabled = channelVO.isAlarmEnabled();
        period = channelVO.getPeriod();
        resolution = channelVO.getResolution();
        apAlSetVOs = channelVO.getApAlSetVOs();
        ffSetIds = channelVO.getFfSetIds();
        speedRatio = channelVO.getSpeedRatio();
        sampleInterval = channelVO.getSampleInterval();
        tachId = channelVO.getTachId();
        alarmEnabled = channelVO.isAlarmEnabled();
        basestationPortNum = channelVO.getBasestationPortNum();
        rollDiameter = channelVO.getRollDiameter();
        rollDiameterUnits = channelVO.getRollDiameterUnits();
        tachReferenceUnits = channelVO.getTachReferenceUnits();
        deviceId = channelVO.getDeviceId();
        sampleRate = channelVO.getSampleRate();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public String getPointLocationDesc() {
        return pointLocationDesc;
    }

    public void setPointLocationDesc(String pointLocationDesc) {
        this.pointLocationDesc = pointLocationDesc;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public String getApSetName() {
        return apSetName;
    }

    public void setApSetName(String apSetName) {
        this.apSetName = apSetName;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public String getAlSetName() {
        return alSetName;
    }

    public void setAlSetName(String alSetName) {
        this.alSetName = alSetName;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public short getBasestationPortNum() {
        return basestationPortNum;
    }

    public void setBasestationPortNum(short basestationPortNum) {
        this.basestationPortNum = basestationPortNum;
    }

    public int getSensorChannelNum() {
        return sensorChannelNum;
    }

    public void setSensorChannelNum(int sensorChannelNum) {
        this.sensorChannelNum = sensorChannelNum;
    }

    public float getSensorOffset() {
        return sensorOffset;
    }

    public void setSensorOffset(float sensorOffset) {
        this.sensorOffset = sensorOffset;
    }

    public float getSensorSensitivity() {
        return sensorSensitivity;
    }

    public void setSensorSensitivity(float sensorSensitivity) {
        this.sensorSensitivity = sensorSensitivity;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public String getSensorUnits() {
        return sensorUnits;
    }

    public void setSensorUnits(String sensorUnits) {
        this.sensorUnits = sensorUnits;
    }

    public String getFfSetIds() {
        return ffSetIds;
    }

    public void setFfSetIds(String ffSetIds) {
        this.ffSetIds = ffSetIds;
    }

    public UUID getTachId() {
        return tachId;
    }

    public void setTachId(UUID tachId) {
        this.tachId = tachId;
    }

    public String getTachReferenceUnits() {
        return tachReferenceUnits;
    }

    public void setTachReferenceUnits(String tachReferenceUnits) {
        this.tachReferenceUnits = tachReferenceUnits;
    }

    public float getRollDiameter() {
        return rollDiameter;
    }

    public void setRollDiameter(float rollDiameter) {
        this.rollDiameter = rollDiameter;
    }

    public String getRollDiameterUnits() {
        return rollDiameterUnits;
    }

    public void setRollDiameterUnits(String rollDiameterUnits) {
        this.rollDiameterUnits = rollDiameterUnits;
    }

    public float getSpeedRatio() {
        return speedRatio;
    }

    public void setSpeedRatio(float speedRatio) {
        this.speedRatio = speedRatio;
    }

    public int getfMax() {
        return fMax;
    }

    public void setfMax(int fMax) {
        this.fMax = fMax;
    }

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

    public float getPeriod() {
        return period;
    }

    public void setPeriod(float period) {
        this.period = period;
    }

    public float getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(float sampleRate) {
        this.sampleRate = sampleRate;
    }

    public short getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(short sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }

    public List<ApAlSetVO> getApAlSetVOs() {
        return apAlSetVOs;
    }

    public void setApAlSetVOs(List<ApAlSetVO> apAlSetVOs) {
        this.apAlSetVOs = apAlSetVOs;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.customerAccount);
        hash = 47 * hash + Objects.hashCode(this.deviceId);
        hash = 47 * hash + Objects.hashCode(this.siteId);
        hash = 47 * hash + Objects.hashCode(this.areaId);
        hash = 47 * hash + Objects.hashCode(this.areaName);
        hash = 47 * hash + Objects.hashCode(this.assetId);
        hash = 47 * hash + Objects.hashCode(this.assetName);
        hash = 47 * hash + Objects.hashCode(this.pointLocationId);
        hash = 47 * hash + Objects.hashCode(this.pointLocationName);
        hash = 47 * hash + Objects.hashCode(this.pointLocationDesc);
        hash = 47 * hash + Objects.hashCode(this.pointId);
        hash = 47 * hash + Objects.hashCode(this.pointName);
        hash = 47 * hash + Objects.hashCode(this.apSetId);
        hash = 47 * hash + Objects.hashCode(this.apSetName);
        hash = 47 * hash + Objects.hashCode(this.alSetId);
        hash = 47 * hash + Objects.hashCode(this.alSetName);
        hash = 47 * hash + Objects.hashCode(this.pointType);
        hash = 47 * hash + this.basestationPortNum;
        hash = 47 * hash + this.sensorChannelNum;
        hash = 47 * hash + Float.floatToIntBits(this.sensorOffset);
        hash = 47 * hash + Float.floatToIntBits(this.sensorSensitivity);
        hash = 47 * hash + Objects.hashCode(this.sensorType);
        hash = 47 * hash + Objects.hashCode(this.sensorUnits);
        hash = 47 * hash + Objects.hashCode(this.ffSetIds);
        hash = 47 * hash + Objects.hashCode(this.tachId);
        hash = 47 * hash + Objects.hashCode(this.tachReferenceUnits);
        hash = 47 * hash + Float.floatToIntBits(this.rollDiameter);
        hash = 47 * hash + Objects.hashCode(this.rollDiameterUnits);
        hash = 47 * hash + Float.floatToIntBits(this.speedRatio);
        hash = 47 * hash + this.fMax;
        hash = 47 * hash + this.resolution;
        hash = 47 * hash + Float.floatToIntBits(this.period);
        hash = 47 * hash + Float.floatToIntBits(this.sampleRate);
        hash = 47 * hash + this.sampleInterval;
        hash = 47 * hash + (this.alarmEnabled ? 1 : 0);
        hash = 47 * hash + Objects.hashCode(this.apAlSetVOs);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ChannelVO other = (ChannelVO) obj;
        if (this.basestationPortNum != other.basestationPortNum) {
            return false;
        }
        if (this.sensorChannelNum != other.sensorChannelNum) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorOffset) != Float.floatToIntBits(other.sensorOffset)) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorSensitivity) != Float.floatToIntBits(other.sensorSensitivity)) {
            return false;
        }
        if (Float.floatToIntBits(this.rollDiameter) != Float.floatToIntBits(other.rollDiameter)) {
            return false;
        }
        if (Float.floatToIntBits(this.speedRatio) != Float.floatToIntBits(other.speedRatio)) {
            return false;
        }
        if (this.fMax != other.fMax) {
            return false;
        }
        if (this.resolution != other.resolution) {
            return false;
        }
        if (Float.floatToIntBits(this.period) != Float.floatToIntBits(other.period)) {
            return false;
        }
        if (Float.floatToIntBits(this.sampleRate) != Float.floatToIntBits(other.sampleRate)) {
            return false;
        }
        if (this.sampleInterval != other.sampleInterval) {
            return false;
        }
        if (this.alarmEnabled != other.alarmEnabled) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationDesc, other.pointLocationDesc)) {
            return false;
        }
        if (!Objects.equals(this.pointName, other.pointName)) {
            return false;
        }
        if (!Objects.equals(this.apSetName, other.apSetName)) {
            return false;
        }
        if (!Objects.equals(this.alSetName, other.alSetName)) {
            return false;
        }
        if (!Objects.equals(this.pointType, other.pointType)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.sensorUnits, other.sensorUnits)) {
            return false;
        }
        if (!Objects.equals(this.tachReferenceUnits, other.tachReferenceUnits)) {
            return false;
        }
        if (!Objects.equals(this.rollDiameterUnits, other.rollDiameterUnits)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        if (!Objects.equals(this.ffSetIds, other.ffSetIds)) {
            return false;
        }
        if (!Objects.equals(this.tachId, other.tachId)) {
            return false;
        }
        if (!Objects.equals(this.apAlSetVOs, other.apAlSetVOs)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ChannelVO{" + "customerAccount=" + customerAccount + ", deviceId=" + deviceId + ", siteId=" + siteId + ", areaId=" + areaId + ", areaName=" + areaName + ", assetId=" + assetId + ", assetName=" + assetName + ", pointLocationId=" + pointLocationId + ", pointLocationName=" + pointLocationName+ ", pointLocationDesc=" + pointLocationDesc + ", pointId=" + pointId + ", pointName=" + pointName + ", apSetId=" + apSetId + ", apSetName=" + apSetName + ", alSetId=" + alSetId + ", alSetName=" + alSetName + ", pointType=" + pointType + ", basestationPortNum=" + basestationPortNum + ", sensorChannelNum=" + sensorChannelNum + ", sensorOffset=" + sensorOffset + ", sensorSensitivity=" + sensorSensitivity + ", sensorType=" + sensorType + ", sensorUnits=" + sensorUnits + ", ffSetIds=" + ffSetIds + ", tachId=" + tachId + ", tachReferenceUnits=" + tachReferenceUnits + ", rollDiameter=" + rollDiameter + ", rollDiameterUnits=" + rollDiameterUnits + ", speedRatio=" + speedRatio + ", fMax=" + fMax + ", resolution=" + resolution + ", period=" + period + ", sampleRate=" + sampleRate + ", sampleInterval=" + sampleInterval + ", alarmEnabled=" + alarmEnabled + ", apAlSetVOs=" + apAlSetVOs + '}';
    }
}
