/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.vo;

import com.uptime.cassandra.config.entity.Points;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class PointVO {

    private String customerAccount;
    private UUID siteId;
    private UUID areaId;
    private UUID assetId;
    private UUID pointId;
    private UUID pointLocationId;
    private UUID alSetId;
    private UUID apSetId;
    private String pointName;
    private boolean autoAcknowledge;
    private boolean disabled;
    private String pointType;
    private int sensorChannelNum;
    private float sensorOffset;
    private float sensorSensitivity;
    private String sensorType;
    private String sensorUnits;
    private boolean alarmEnabled;
    private int sqlChannelId;
    private List<ApAlSetVO> apAlSetVOs;
    //Used for ApAlSetVO
    private String paramName;
    private boolean customized;
    private boolean demodEnabled;
    private float demodHighPass;
    private float demodLowPass;
    private int dwell;
    private int fMax;
    private String freqUnits;
    private float highAlert;
    private boolean highAlertActive;
    private float highFault;
    private boolean highFaultActive;
    private float lowAlert;
    private boolean lowAlertActive;
    private float lowFault;
    private boolean lowFaultActive;
    private float maxFreq;
    private float minFreq;
    private String paramAmpFactor;
    private String paramType;
    private String paramUnits;
    private float period;
    private int resolution;
    private float sampleRate;
    private String sensorSubType;
    private String sensorLocalOrientation;

    //Used for Kafka
    private String siteName;
    private String areaName;
    private String assetName;
    private String pointLocationName;

    public PointVO() {

    }

    public PointVO(Points entity) {
        this.customerAccount = entity.getCustomerAccount();
        this.siteId = entity.getSiteId();
        this.areaId = entity.getAreaId();
        this.assetId = entity.getAssetId();
        this.pointLocationId = entity.getPointLocationId();
        this.pointId = entity.getPointId();
        this.apSetId = entity.getApSetId();
        this.alSetId = entity.getAlSetId();
        this.autoAcknowledge = entity.isAutoAcknowledge();
        this.disabled = entity.isDisabled();
        this.pointName = entity.getPointName();
        this.pointType = entity.getPointType();
        this.sensorChannelNum = entity.getSensorChannelNum();
        this.sensorOffset = entity.getSensorOffset();
        this.sensorSensitivity = entity.getSensorSensitivity();
        this.sensorType = entity.getSensorType();
        this.sensorUnits = entity.getSensorUnits();
        this.alarmEnabled = entity.isAlarmEnabled();
        this.sensorSubType=entity.getSensorSubType();
        this.sensorLocalOrientation=entity.getSensorLocalOrientation();
    }

    public PointVO(PointVO pointVO) {
        this.customerAccount = pointVO.getCustomerAccount();
        this.siteId = pointVO.getSiteId();
        this.areaId = pointVO.getAreaId();
        this.assetId = pointVO.getAssetId();
        this.pointLocationId = pointVO.getPointLocationId();
        this.pointId = pointVO.getPointId();
        this.apSetId = pointVO.getApSetId();
        this.alSetId = pointVO.getAlSetId();
        this.autoAcknowledge = pointVO.isAutoAcknowledge();
        this.disabled = pointVO.isDisabled();
        this.pointName = pointVO.getPointName();
        this.pointType = pointVO.getPointType();
        this.sensorChannelNum = pointVO.getSensorChannelNum();
        this.sensorOffset = pointVO.getSensorOffset();
        this.sensorSensitivity = pointVO.getSensorSensitivity();
        this.sensorType = pointVO.getSensorType();
        this.sensorUnits = pointVO.getSensorUnits();
        this.alarmEnabled = pointVO.isAlarmEnabled();
        this.sqlChannelId = pointVO.getSqlChannelId();
        this.apAlSetVOs = pointVO.getApAlSetVOs();
        this.siteName = pointVO.getSiteName();
        this.areaName = pointVO.getAreaName();
        this.assetName = pointVO.getAssetName();
        this.pointLocationName = pointVO.getPointLocationName();
        this.paramName = pointVO.getParamName();
        this.sensorSubType=pointVO.getSensorSubType();
        this.sensorLocalOrientation=pointVO.getSensorLocalOrientation();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public UUID getAlSetId() {
        return alSetId;
    }

    public void setAlSetId(UUID alSetId) {
        this.alSetId = alSetId;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public boolean isAutoAcknowledge() {
        return autoAcknowledge;
    }

    public void setAutoAcknowledge(boolean autoAcknowledge) {
        this.autoAcknowledge = autoAcknowledge;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType;
    }

    public int getSensorChannelNum() {
        return sensorChannelNum;
    }

    public void setSensorChannelNum(int sensorChannelNum) {
        this.sensorChannelNum = sensorChannelNum;
    }

    public float getSensorOffset() {
        return sensorOffset;
    }

    public void setSensorOffset(float sensorOffset) {
        this.sensorOffset = sensorOffset;
    }

    public float getSensorSensitivity() {
        return sensorSensitivity;
    }

    public void setSensorSensitivity(float sensorSensitivity) {
        this.sensorSensitivity = sensorSensitivity;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public String getSensorUnits() {
        return sensorUnits;
    }

    public void setSensorUnits(String sensorUnits) {
        this.sensorUnits = sensorUnits;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }

    public int getSqlChannelId() {
        return sqlChannelId;
    }

    public void setSqlChannelId(int sqlChannelId) {
        this.sqlChannelId = sqlChannelId;
    }

    public List<ApAlSetVO> getApAlSetVOs() {
        return apAlSetVOs;
    }

    public void setApAlSetVOs(List<ApAlSetVO> apAlSetVOs) {
        this.apAlSetVOs = apAlSetVOs;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public boolean isCustomized() {
        return customized;
    }

    public void setCustomized(boolean customized) {
        this.customized = customized;
    }

    public boolean isDemodEnabled() {
        return demodEnabled;
    }

    public void setDemodEnabled(boolean demodEnabled) {
        this.demodEnabled = demodEnabled;
    }

    public float getDemodHighPass() {
        return demodHighPass;
    }

    public void setDemodHighPass(float demodHighPass) {
        this.demodHighPass = demodHighPass;
    }

    public float getDemodLowPass() {
        return demodLowPass;
    }

    public void setDemodLowPass(float demodLowPass) {
        this.demodLowPass = demodLowPass;
    }

    public int getDwell() {
        return dwell;
    }

    public void setDwell(int dwell) {
        this.dwell = dwell;
    }

    public int getfMax() {
        return fMax;
    }

    public void setfMax(int fMax) {
        this.fMax = fMax;
    }

    public String getFreqUnits() {
        return freqUnits;
    }

    public void setFreqUnits(String freqUnits) {
        this.freqUnits = freqUnits;
    }

    public float getHighAlert() {
        return highAlert;
    }

    public void setHighAlert(float highAlert) {
        this.highAlert = highAlert;
    }

    public boolean isHighAlertActive() {
        return highAlertActive;
    }

    public void setHighAlertActive(boolean highAlertActive) {
        this.highAlertActive = highAlertActive;
    }

    public float getHighFault() {
        return highFault;
    }

    public void setHighFault(float highFault) {
        this.highFault = highFault;
    }

    public boolean isHighFaultActive() {
        return highFaultActive;
    }

    public void setHighFaultActive(boolean highFaultActive) {
        this.highFaultActive = highFaultActive;
    }

    public float getLowAlert() {
        return lowAlert;
    }

    public void setLowAlert(float lowAlert) {
        this.lowAlert = lowAlert;
    }

    public boolean isLowAlertActive() {
        return lowAlertActive;
    }

    public void setLowAlertActive(boolean lowAlertActive) {
        this.lowAlertActive = lowAlertActive;
    }

    public float getLowFault() {
        return lowFault;
    }

    public void setLowFault(float lowFault) {
        this.lowFault = lowFault;
    }

    public boolean isLowFaultActive() {
        return lowFaultActive;
    }

    public void setLowFaultActive(boolean lowFaultActive) {
        this.lowFaultActive = lowFaultActive;
    }

    public float getMaxFreq() {
        return maxFreq;
    }

    public void setMaxFreq(float maxFreq) {
        this.maxFreq = maxFreq;
    }

    public float getMinFreq() {
        return minFreq;
    }

    public void setMinFreq(float minFreq) {
        this.minFreq = minFreq;
    }

    public String getParamAmpFactor() {
        return paramAmpFactor;
    }

    public void setParamAmpFactor(String paramAmpFactor) {
        this.paramAmpFactor = paramAmpFactor;
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    public String getParamUnits() {
        return paramUnits;
    }

    public void setParamUnits(String paramUnits) {
        this.paramUnits = paramUnits;
    }

    public float getPeriod() {
        return period;
    }

    public void setPeriod(float period) {
        this.period = period;
    }

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

    public float getSampleRate() {
        return sampleRate;
    }

    public void setSampleRate(float sampleRate) {
        this.sampleRate = sampleRate;
    }

    public String getSensorSubType() {
        return sensorSubType;
    }

    public void setSensorSubType(String sensorSubType) {
        this.sensorSubType = sensorSubType;
    }

    public String getSensorLocalOrientation() {
        return sensorLocalOrientation;
    }

    public void setSensorLocalOrientation(String sensorLocalOrientation) {
        this.sensorLocalOrientation = sensorLocalOrientation;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.customerAccount);
        hash = 89 * hash + Objects.hashCode(this.siteId);
        hash = 89 * hash + Objects.hashCode(this.areaId);
        hash = 89 * hash + Objects.hashCode(this.assetId);
        hash = 89 * hash + Objects.hashCode(this.pointId);
        hash = 89 * hash + Objects.hashCode(this.pointLocationId);
        hash = 89 * hash + Objects.hashCode(this.alSetId);
        hash = 89 * hash + Objects.hashCode(this.apSetId);
        hash = 89 * hash + Objects.hashCode(this.pointName);
        hash = 89 * hash + (this.autoAcknowledge ? 1 : 0);
        hash = 89 * hash + (this.disabled ? 1 : 0);
        hash = 89 * hash + Objects.hashCode(this.pointType);
        hash = 89 * hash + this.sensorChannelNum;
        hash = 89 * hash + Float.floatToIntBits(this.sensorOffset);
        hash = 89 * hash + Float.floatToIntBits(this.sensorSensitivity);
        hash = 89 * hash + Objects.hashCode(this.sensorType);
        hash = 89 * hash + Objects.hashCode(this.sensorUnits);
        hash = 89 * hash + (this.alarmEnabled ? 1 : 0);
        hash = 89 * hash + this.sqlChannelId;
        hash = 89 * hash + Objects.hashCode(this.apAlSetVOs);
        hash = 89 * hash + Objects.hashCode(this.paramName);
        hash = 89 * hash + (this.customized ? 1 : 0);
        hash = 89 * hash + (this.demodEnabled ? 1 : 0);
        hash = 89 * hash + Float.floatToIntBits(this.demodHighPass);
        hash = 89 * hash + Float.floatToIntBits(this.demodLowPass);
        hash = 89 * hash + this.dwell;
        hash = 89 * hash + this.fMax;
        hash = 89 * hash + Objects.hashCode(this.freqUnits);
        hash = 89 * hash + Float.floatToIntBits(this.highAlert);
        hash = 89 * hash + (this.highAlertActive ? 1 : 0);
        hash = 89 * hash + Float.floatToIntBits(this.highFault);
        hash = 89 * hash + (this.highFaultActive ? 1 : 0);
        hash = 89 * hash + Float.floatToIntBits(this.lowAlert);
        hash = 89 * hash + (this.lowAlertActive ? 1 : 0);
        hash = 89 * hash + Float.floatToIntBits(this.lowFault);
        hash = 89 * hash + (this.lowFaultActive ? 1 : 0);
        hash = 89 * hash + Float.floatToIntBits(this.maxFreq);
        hash = 89 * hash + Float.floatToIntBits(this.minFreq);
        hash = 89 * hash + Objects.hashCode(this.paramAmpFactor);
        hash = 89 * hash + Objects.hashCode(this.paramType);
        hash = 89 * hash + Objects.hashCode(this.paramUnits);
        hash = 89 * hash + Float.floatToIntBits(this.period);
        hash = 89 * hash + this.resolution;
        hash = 89 * hash + Float.floatToIntBits(this.sampleRate);
        hash = 89 * hash + Objects.hashCode(this.sensorSubType);
        hash = 89 * hash + Objects.hashCode(this.sensorLocalOrientation);
        hash = 89 * hash + Objects.hashCode(this.siteName);
        hash = 89 * hash + Objects.hashCode(this.areaName);
        hash = 89 * hash + Objects.hashCode(this.assetName);
        hash = 89 * hash + Objects.hashCode(this.pointLocationName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PointVO other = (PointVO) obj;
        if (this.autoAcknowledge != other.autoAcknowledge) {
            return false;
        }
        if (this.disabled != other.disabled) {
            return false;
        }
        if (this.sensorChannelNum != other.sensorChannelNum) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorOffset) != Float.floatToIntBits(other.sensorOffset)) {
            return false;
        }
        if (Float.floatToIntBits(this.sensorSensitivity) != Float.floatToIntBits(other.sensorSensitivity)) {
            return false;
        }
        if (this.alarmEnabled != other.alarmEnabled) {
            return false;
        }
        if (this.sqlChannelId != other.sqlChannelId) {
            return false;
        }
        if (this.customized != other.customized) {
            return false;
        }
        if (this.demodEnabled != other.demodEnabled) {
            return false;
        }
        if (Float.floatToIntBits(this.demodHighPass) != Float.floatToIntBits(other.demodHighPass)) {
            return false;
        }
        if (Float.floatToIntBits(this.demodLowPass) != Float.floatToIntBits(other.demodLowPass)) {
            return false;
        }
        if (this.dwell != other.dwell) {
            return false;
        }
        if (this.fMax != other.fMax) {
            return false;
        }
        if (Float.floatToIntBits(this.highAlert) != Float.floatToIntBits(other.highAlert)) {
            return false;
        }
        if (this.highAlertActive != other.highAlertActive) {
            return false;
        }
        if (Float.floatToIntBits(this.highFault) != Float.floatToIntBits(other.highFault)) {
            return false;
        }
        if (this.highFaultActive != other.highFaultActive) {
            return false;
        }
        if (Float.floatToIntBits(this.lowAlert) != Float.floatToIntBits(other.lowAlert)) {
            return false;
        }
        if (this.lowAlertActive != other.lowAlertActive) {
            return false;
        }
        if (Float.floatToIntBits(this.lowFault) != Float.floatToIntBits(other.lowFault)) {
            return false;
        }
        if (this.lowFaultActive != other.lowFaultActive) {
            return false;
        }
        if (Float.floatToIntBits(this.maxFreq) != Float.floatToIntBits(other.maxFreq)) {
            return false;
        }
        if (Float.floatToIntBits(this.minFreq) != Float.floatToIntBits(other.minFreq)) {
            return false;
        }
        if (Float.floatToIntBits(this.period) != Float.floatToIntBits(other.period)) {
            return false;
        }
        if (this.resolution != other.resolution) {
            return false;
        }
        if (Float.floatToIntBits(this.sampleRate) != Float.floatToIntBits(other.sampleRate)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.pointName, other.pointName)) {
            return false;
        }
        if (!Objects.equals(this.pointType, other.pointType)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.sensorUnits, other.sensorUnits)) {
            return false;
        }
        if (!Objects.equals(this.paramName, other.paramName)) {
            return false;
        }
        if (!Objects.equals(this.freqUnits, other.freqUnits)) {
            return false;
        }
        if (!Objects.equals(this.paramAmpFactor, other.paramAmpFactor)) {
            return false;
        }
        if (!Objects.equals(this.paramType, other.paramType)) {
            return false;
        }
        if (!Objects.equals(this.paramUnits, other.paramUnits)) {
            return false;
        }
        if (!Objects.equals(this.sensorSubType, other.sensorSubType)) {
            return false;
        }
        if (!Objects.equals(this.sensorLocalOrientation, other.sensorLocalOrientation)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.alSetId, other.alSetId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        return Objects.equals(this.apAlSetVOs, other.apAlSetVOs);
    }

    @Override
    public String toString() {
        return "PointVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointId=" + pointId + ", pointLocationId=" + pointLocationId + ", alSetId=" + alSetId + ", apSetId=" + apSetId + ", pointName=" + pointName + ", autoAcknowledge=" + autoAcknowledge + ", disabled=" + disabled + ", pointType=" + pointType + ", sensorChannelNum=" + sensorChannelNum + ", sensorOffset=" + sensorOffset + ", sensorSensitivity=" + sensorSensitivity + ", sensorType=" + sensorType + ", sensorUnits=" + sensorUnits + ", alarmEnabled=" + alarmEnabled + ", sqlChannelId=" + sqlChannelId + ", apAlSetVOs=" + apAlSetVOs + ", paramName=" + paramName + ", customized=" + customized + ", demodEnabled=" + demodEnabled + ", demodHighPass=" + demodHighPass + ", demodLowPass=" + demodLowPass + ", dwell=" + dwell + ", fMax=" + fMax + ", freqUnits=" + freqUnits + ", highAlert=" + highAlert + ", highAlertActive=" + highAlertActive + ", highFault=" + highFault + ", highFaultActive=" + highFaultActive + ", lowAlert=" + lowAlert + ", lowAlertActive=" + lowAlertActive + ", lowFault=" + lowFault + ", lowFaultActive=" + lowFaultActive + ", maxFreq=" + maxFreq + ", minFreq=" + minFreq + ", paramAmpFactor=" + paramAmpFactor + ", paramType=" + paramType + ", paramUnits=" + paramUnits + ", period=" + period + ", resolution=" + resolution + ", sampleRate=" + sampleRate + ", sensorSubType=" + sensorSubType + ", sensorLocalOrientation=" + sensorLocalOrientation + ", siteName=" + siteName + ", areaName=" + areaName + ", assetName=" + assetName + ", pointLocationName=" + pointLocationName + '}';
    }
    
    
    
    
}
