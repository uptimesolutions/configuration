/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.vo;

import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import java.time.Instant;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class SiteBasestationDeviceVO {
    
    private String customerAccount;
    private UUID siteId;
    private short baseStationPort;
    private String deviceId;
    private UUID pointId;
    private String channelType;
    private int fmax;
    private Instant lastSampled;
    private int resolution;
    private short sampleInterval ;
    private byte sensorChannelNum;
    
    public SiteBasestationDeviceVO(){
        
    }
    
    public SiteBasestationDeviceVO(SiteBasestationDevice siteBasestationDevice){
        this.customerAccount = siteBasestationDevice.getCustomerAccount();
        this.baseStationPort = siteBasestationDevice.getBaseStationPort();
        this.channelType = siteBasestationDevice.getChannelType();
        this.deviceId = siteBasestationDevice.getDeviceId();
        this.fmax = siteBasestationDevice.getFmax();
        this.lastSampled = siteBasestationDevice.getLastSampled();
        this.pointId = siteBasestationDevice.getPointId();
        this.resolution = siteBasestationDevice.getResolution();
        this.sampleInterval = siteBasestationDevice.getSampleInterval();
        this.sensorChannelNum = siteBasestationDevice.getSensorChannelNum();
        this.siteId = siteBasestationDevice.getSiteId();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public short getBaseStationPort() {
        return baseStationPort;
    }

    public void setBaseStationPort(short baseStationPort) {
        this.baseStationPort = baseStationPort;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public int getFmax() {
        return fmax;
    }

    public void setFmax(int fmax) {
        this.fmax = fmax;
    }

    public Instant getLastSampled() {
        return lastSampled;
    }

    public void setLastSampled(Instant lastSampled) {
        this.lastSampled = lastSampled;
    }

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

    public short getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(short sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public byte getSensorChannelNum() {
        return sensorChannelNum;
    }

    public void setSensorChannelNum(byte sensorChannelNum) {
        this.sensorChannelNum = sensorChannelNum;
    }
    
    
}
