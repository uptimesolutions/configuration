/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.vo;

import com.uptime.cassandra.config.entity.HwUnitToPoint;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
public class DeviceManagementRowVO implements Serializable {
    private String deviceId;
    private String customerAcct;
    private UUID siteId;
    private UUID areaId; 
    private String areaName;
    private UUID assetId;
    private String assetName;
    private UUID pointLocationId;
    private String pointLocationName;
    private UUID pointId;
    private String pointName;
    private UUID apSetId;
    private boolean alarmEnabled;
    private String channelType;
    private byte channelNum;
    private String sensorType;
    private int fmax;
    private int resolution;
    private short basestationPortNum;

    public DeviceManagementRowVO() {
    }

    public DeviceManagementRowVO(DeviceManagementRowVO deviceManagementRowVO) {
        deviceId = deviceManagementRowVO.getDeviceId();
        customerAcct = deviceManagementRowVO.getCustomerAcct();
        siteId = deviceManagementRowVO.getSiteId();
        areaId = deviceManagementRowVO.getAreaId(); 
        areaName = deviceManagementRowVO.getAreaName();
        assetId = deviceManagementRowVO.getAssetId();
        assetName = deviceManagementRowVO.getAssetName();
        pointLocationId = deviceManagementRowVO.getPointLocationId();
        pointLocationName = deviceManagementRowVO.getPointLocationName();
        pointId = deviceManagementRowVO.getPointId();
        pointName = deviceManagementRowVO.getPointName();
        apSetId = deviceManagementRowVO.getApSetId();
        alarmEnabled = deviceManagementRowVO.isAlarmEnabled();
        channelType = deviceManagementRowVO.getChannelType();
        channelNum = deviceManagementRowVO.getChannelNum();
        sensorType = deviceManagementRowVO.getSensorType();
        fmax = deviceManagementRowVO.getFmax();
        resolution = deviceManagementRowVO.getResolution();
        basestationPortNum = deviceManagementRowVO.getBasestationPortNum();
    }

    public DeviceManagementRowVO(HwUnitToPoint entity) {
        deviceId = entity.getDeviceId();
        channelType = entity.getChannelType();
        channelNum = entity.getChannelNum();
        customerAcct = entity.getCustomerAcct();
        siteId = entity.getSiteId();
        areaId = entity.getAreaId();
        assetId = entity.getAssetId();
        pointLocationId = entity.getPointLocationId();
        pointId = entity.getPointId();
        apSetId = entity.getApSetId();
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getCustomerAcct() {
        return customerAcct;
    }

    public void setCustomerAcct(String customerAcct) {
        this.customerAcct = customerAcct;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public UUID getPointId() {
        return pointId;
    }

    public void setPointId(UUID pointId) {
        this.pointId = pointId;
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName;
    }

    public UUID getApSetId() {
        return apSetId;
    }

    public void setApSetId(UUID apSetId) {
        this.apSetId = apSetId;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public byte getChannelNum() {
        return channelNum;
    }

    public void setChannelNum(byte channelNum) {
        this.channelNum = channelNum;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public int getFmax() {
        return fmax;
    }

    public void setFmax(int fmax) {
        this.fmax = fmax;
    }

    public int getResolution() {
        return resolution;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

    public short getBasestationPortNum() {
        return basestationPortNum;
    }

    public void setBasestationPortNum(short basestationPortNum) {
        this.basestationPortNum = basestationPortNum;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.deviceId);
        hash = 89 * hash + Objects.hashCode(this.customerAcct);
        hash = 89 * hash + Objects.hashCode(this.siteId);
        hash = 89 * hash + Objects.hashCode(this.areaId);
        hash = 89 * hash + Objects.hashCode(this.areaName);
        hash = 89 * hash + Objects.hashCode(this.assetId);
        hash = 89 * hash + Objects.hashCode(this.assetName);
        hash = 89 * hash + Objects.hashCode(this.pointLocationId);
        hash = 89 * hash + Objects.hashCode(this.pointLocationName);
        hash = 89 * hash + Objects.hashCode(this.pointId);
        hash = 89 * hash + Objects.hashCode(this.pointName);
        hash = 89 * hash + Objects.hashCode(this.apSetId);
        hash = 89 * hash + (this.alarmEnabled ? 1 : 0);
        hash = 89 * hash + Objects.hashCode(this.channelType);
        hash = 89 * hash + this.channelNum;
        hash = 89 * hash + Objects.hashCode(this.sensorType);
        hash = 89 * hash + this.fmax;
        hash = 89 * hash + this.resolution;
        hash = 89 * hash + this.basestationPortNum;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DeviceManagementRowVO other = (DeviceManagementRowVO) obj;
        if (this.alarmEnabled != other.alarmEnabled) {
            return false;
        }
        if (this.channelNum != other.channelNum) {
            return false;
        }
        if (this.fmax != other.fmax) {
            return false;
        }
        if (this.resolution != other.resolution) {
            return false;
        }
        if (this.basestationPortNum != other.basestationPortNum) {
            return false;
        }
        if (!Objects.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (!Objects.equals(this.customerAcct, other.customerAcct)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.pointName, other.pointName)) {
            return false;
        }
        if (!Objects.equals(this.channelType, other.channelType)) {
            return false;
        }
        if (!Objects.equals(this.sensorType, other.sensorType)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.pointId, other.pointId)) {
            return false;
        }
        if (!Objects.equals(this.apSetId, other.apSetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DeviceManagementRowVO{" + "deviceId=" + deviceId + ", customerAcct=" + customerAcct + ", siteId=" + siteId + ", areaId=" + areaId + ", areaName=" + areaName + ", assetId=" + assetId + ", assetName=" + assetName + ", pointLocationId=" + pointLocationId + ", pointLocationName=" + pointLocationName + ", pointId=" + pointId + ", pointName=" + pointName + ", apSetId=" + apSetId + ", alarmEnabled=" + alarmEnabled + ", channelType=" + channelType + ", channelNum=" + channelNum + ", sensorType=" + sensorType + ", fmax=" + fmax + ", resolution=" + resolution + ", basestationPortNum=" + basestationPortNum + '}';
    }
}
