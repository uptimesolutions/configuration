/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.vo;

import java.util.List;
import java.util.UUID;

/**
 *
 * @author gsingh
 */
public class AreaVO {
    String customerAccount;
    UUID siteId;
    UUID areaId;
    String areaName;
    boolean climateControlled;
    String description;
    String environment;

    List<AreaConfigScheduleVO> scheduleVOList;

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public boolean isClimateControlled() {
        return climateControlled;
    }

    public void setClimateControlled(boolean climateControlled) {
        this.climateControlled = climateControlled;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public List<AreaConfigScheduleVO> getScheduleVOList() {
        return scheduleVOList;
    }

    public void setScheduleVOList(List<AreaConfigScheduleVO> scheduleVOList) {
        this.scheduleVOList = scheduleVOList;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    @Override
    public String toString() {
        return "AreaVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", areaName=" + areaName + ", climateControlled=" + climateControlled + ", description=" + description + ", environment=" + environment + ", scheduleVOList=" + scheduleVOList + '}';
    }

}
