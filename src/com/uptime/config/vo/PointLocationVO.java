/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.vo;

import com.uptime.cassandra.config.entity.PointLocations;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class PointLocationVO {
    private String customerAccount;
    private UUID siteId;
    private UUID areaId;
    private UUID assetId;
    private UUID pointLocationId;
    private String pointLocationName;
    private String description;
    private String deviceSerialNumber;
    private String ffSetIds;
    private float speedRatio;
    private short sampleInterval;
    private short basestationPortNum;
    private UUID tachId;
    private boolean alarmEnabled;
    private float rollDiameter;
    private String rollDiameterUnits;
    private List<PointVO> pointList;
    private List<String> sensorList;
    private String request;
    
    //Used for Kafka
    private String siteName;
    private String areaName;
    private String assetName;

    public PointLocationVO() {
    }

    public PointLocationVO(String customerAccount, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId, String pointLocationName, String description, String deviceSerialNumber, String ffSetIds, float speedRatio, float fpm, float rollDiameter, short sampleInterval, UUID tachId, boolean alarmEnabled, String rollDiameterUnits, List<PointVO> pointList, List<String> sensorList) {
        this.customerAccount = customerAccount;
        this.siteId = siteId;
        this.areaId = areaId;
        this.assetId = assetId;
        this.pointLocationId = pointLocationId;
        this.pointLocationName = pointLocationName;
        this.description = description;
        this.deviceSerialNumber = deviceSerialNumber;
        this.ffSetIds = ffSetIds;
        this.speedRatio = speedRatio;
        this.sampleInterval = sampleInterval;
        this.tachId = tachId;
        this.alarmEnabled = alarmEnabled;
        this.rollDiameter = rollDiameter;
        this.rollDiameterUnits = rollDiameterUnits;
        this.pointList = pointList;
        this.sensorList = sensorList;
    }
    
    public PointLocationVO(PointLocationVO pointLocationVO) {
        customerAccount = pointLocationVO.getCustomerAccount();
        siteId = pointLocationVO.getSiteId();
        areaId = pointLocationVO.getAreaId();
        assetId = pointLocationVO.getAssetId();
        pointLocationId = pointLocationVO.getPointLocationId();
        pointLocationName = pointLocationVO.getPointLocationName();
        description = pointLocationVO.getDescription();
        deviceSerialNumber = pointLocationVO.getDeviceSerialNumber();
        ffSetIds = pointLocationVO.getFfSetIds();
        speedRatio = pointLocationVO.getSpeedRatio();
        sampleInterval = pointLocationVO.getSampleInterval();
        basestationPortNum = pointLocationVO.getBasestationPortNum();
        tachId = pointLocationVO.getTachId();
        alarmEnabled = pointLocationVO.isAlarmEnabled();
        rollDiameter = pointLocationVO.getRollDiameter();
        rollDiameterUnits = pointLocationVO.getRollDiameterUnits();
        pointList = pointLocationVO.getPointList();
    }
    
    public PointLocationVO(PointLocations pointLocation) {
        customerAccount = pointLocation.getCustomerAccount();
        siteId = pointLocation.getSiteId();
        areaId = pointLocation.getAreaId();
        assetId = pointLocation.getAssetId();
        pointLocationId = pointLocation.getPointLocationId();
        pointLocationName = pointLocation.getPointLocationName();
        description = pointLocation.getDescription();
        deviceSerialNumber = pointLocation.getDeviceSerialNumber();
        ffSetIds = pointLocation.getFfSetIds();
        speedRatio = pointLocation.getSpeedRatio();
        sampleInterval = pointLocation.getSampleInterval();
        basestationPortNum = pointLocation.getBasestationPortNum();
        tachId = pointLocation.getTachId();
        alarmEnabled = pointLocation.isAlarmEnabled();
        rollDiameter = pointLocation.getRollDiameter();
        rollDiameterUnits = pointLocation.getRollDiameterUnits();
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAreaId() {
        return areaId;
    }

    public void setAreaId(UUID areaId) {
        this.areaId = areaId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public UUID getPointLocationId() {
        return pointLocationId;
    }

    public void setPointLocationId(UUID pointLocationId) {
        this.pointLocationId = pointLocationId;
    }

    public String getPointLocationName() {
        return pointLocationName;
    }

    public void setPointLocationName(String pointLocationName) {
        this.pointLocationName = pointLocationName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDeviceSerialNumber() {
        return deviceSerialNumber;
    }

    public void setDeviceSerialNumber(String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }

    public String getFfSetIds() {
        return ffSetIds;
    }

    public void setFfSetIds(String ffSetIds) {
        this.ffSetIds = ffSetIds;
    }

    public float getSpeedRatio() {
        return speedRatio;
    }

    public void setSpeedRatio(float speedRatio) {
        this.speedRatio = speedRatio;
    }

    public short getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(short sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public short getBasestationPortNum() {
        return basestationPortNum;
    }

    public void setBasestationPortNum(short basestationPortNum) {
        this.basestationPortNum = basestationPortNum;
    }
    
    public UUID getTachId() {
        return tachId;
    }

    public void setTachId(UUID tachId) {
        this.tachId = tachId;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean alarmEnabled) {
        this.alarmEnabled = alarmEnabled;
    }

    public List<PointVO> getPointList() {
        return pointList;
    }

    public void setPointList(List<PointVO> pointList) {
        this.pointList = pointList;
    }

    public List<String> getSensorList() {
        return sensorList;
    }

    public void setSensorList(List<String> sensorList) {
        this.sensorList = sensorList;
    }

    public float getRollDiameter() {
        return rollDiameter;
    }

    public void setRollDiameter(float rollDiameter) {
        this.rollDiameter = rollDiameter;
    }

    public String getRollDiameterUnits() {
        return rollDiameterUnits;
    }

    public void setRollDiameterUnits(String rollDiameterUnits) {
        this.rollDiameterUnits = rollDiameterUnits;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.customerAccount);
        hash = 29 * hash + Objects.hashCode(this.siteId);
        hash = 29 * hash + Objects.hashCode(this.areaId);
        hash = 29 * hash + Objects.hashCode(this.assetId);
        hash = 29 * hash + Objects.hashCode(this.pointLocationId);
        hash = 29 * hash + Objects.hashCode(this.pointLocationName);
        hash = 29 * hash + Objects.hashCode(this.siteName);
        hash = 29 * hash + Objects.hashCode(this.areaName);
        hash = 29 * hash + Objects.hashCode(this.assetName);
        hash = 29 * hash + Objects.hashCode(this.description);
        hash = 29 * hash + Objects.hashCode(this.deviceSerialNumber);
        hash = 29 * hash + Objects.hashCode(this.ffSetIds);
        hash = 29 * hash + Float.floatToIntBits(this.speedRatio);
        hash = 29 * hash + this.sampleInterval;
        hash = 29 * hash + this.basestationPortNum;
        hash = 29 * hash + Objects.hashCode(this.tachId);
        hash = 29 * hash + (this.alarmEnabled ? 1 : 0);
        hash = 29 * hash + Float.floatToIntBits(this.rollDiameter);
        hash = 29 * hash + Objects.hashCode(this.rollDiameterUnits);
        hash = 29 * hash + Objects.hashCode(this.pointList);
        hash = 29 * hash + Objects.hashCode(this.sensorList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PointLocationVO other = (PointLocationVO) obj;
        if (Float.floatToIntBits(this.speedRatio) != Float.floatToIntBits(other.speedRatio)) {
            return false;
        }
        if (this.sampleInterval != other.sampleInterval) {
            return false;
        }
        if (this.basestationPortNum != other.basestationPortNum) {
            return false;
        }
        if (this.alarmEnabled != other.alarmEnabled) {
            return false;
        }
        if (Float.floatToIntBits(this.rollDiameter) != Float.floatToIntBits(other.rollDiameter)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationName, other.pointLocationName)) {
            return false;
        }
        if (!Objects.equals(this.siteName, other.siteName)) {
            return false;
        }
        if (!Objects.equals(this.areaName, other.areaName)) {
            return false;
        }
        if (!Objects.equals(this.assetName, other.assetName)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.deviceSerialNumber, other.deviceSerialNumber)) {
            return false;
        }
        if (!Objects.equals(this.ffSetIds, other.ffSetIds)) {
            return false;
        }
        if (!Objects.equals(this.rollDiameterUnits, other.rollDiameterUnits)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.areaId, other.areaId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        if (!Objects.equals(this.pointLocationId, other.pointLocationId)) {
            return false;
        }
        if (!Objects.equals(this.tachId, other.tachId)) {
            return false;
        }
        if (!Objects.equals(this.pointList, other.pointList)) {
            return false;
        }
        if (!Objects.equals(this.sensorList, other.sensorList)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PointLocationVO{" + "customerAccount=" + customerAccount + ", siteId=" + siteId + ", areaId=" + areaId + ", assetId=" + assetId + ", pointLocationId=" + pointLocationId + ", pointLocationName=" + pointLocationName + ", description=" + description + ", deviceSerialNumber=" + deviceSerialNumber + ", ffSetIds=" + ffSetIds + ", speedRatio=" + speedRatio + ", sampleInterval=" + sampleInterval + ", basestationPortNum=" + basestationPortNum + ", tachId=" + tachId + ", alarmEnabled=" + alarmEnabled + ", rollDiameter=" + rollDiameter + ", rollDiameterUnits=" + rollDiameterUnits + ", pointList=" + pointList + ", request=" + request + ", siteName=" + siteName + ", areaName=" + areaName + ", assetName=" + assetName + ", sensorList=" + sensorList + "}";
    }
}
