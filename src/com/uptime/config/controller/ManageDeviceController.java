
package com.uptime.config.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.dao.AreaSiteDAO;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.SiteCustomerDAO;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.cassandra.config.entity.AreaSite;
import com.uptime.cassandra.config.entity.AssetSiteArea;
import com.uptime.cassandra.config.entity.HwUnitToPoint;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.cassandra.config.entity.PointToHwUnit;
import com.uptime.cassandra.config.entity.Points;
import com.uptime.cassandra.config.entity.SiteCustomer;
import com.uptime.cassandra.monitor.entity.SiteBasestationDevice;
import com.uptime.cassandra.presets.entity.GlobalApAlSetsByCustomer;
import com.uptime.cassandra.presets.entity.GlobalTachometers;
import com.uptime.cassandra.presets.entity.SiteApAlSetsByCustomer;
import com.uptime.cassandra.presets.entity.SiteTachometers;
import com.uptime.config.dao.DeviceStatusDAO;
import com.uptime.config.delegate.apalbypoint.ReadApAlByPointDelegate;
import com.uptime.config.delegate.area.ReadAreaDelegate;
import com.uptime.config.delegate.asset.ReadAssetDelegate;
import com.uptime.config.delegate.device.CreateDeviceDelegate;
import com.uptime.config.delegate.device.DeleteDeviceDelegate;
import com.uptime.config.delegate.device.ReadDeviceDelegate;
import com.uptime.config.delegate.device.UpdateDeviceDelegate;
import com.uptime.config.delegate.hwunit.ReadHwUnitDelegate;
import com.uptime.config.delegate.pointlocations.ReadPointLocationsDelegate;
import com.uptime.config.delegate.pointlocations.UpdatePointLocationsDelegate;
import com.uptime.config.delegate.points.CreatePointsDelegate;
import com.uptime.config.delegate.points.ReadPointsDelegate;
import com.uptime.config.delegate.pointtohwunit.CreatePointToHwUnitDelegate;
import com.uptime.config.delegate.pointtohwunit.DeletePointToHwUnitDelegate;
import com.uptime.config.delegate.pointtohwunit.ReadPointToHwUnitDelegate;
import com.uptime.config.delegate.pointtohwunit.UpdatePointToHwUnitDelegate;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.ApAlSetVO;
import com.uptime.config.vo.AssetVO;
import com.uptime.config.vo.ChannelVO;
import com.uptime.config.vo.DeviceManagementRowVO;
import com.uptime.config.vo.DeviceStatusVO;
import com.uptime.config.vo.DeviceVO;
import com.uptime.services.ServiceConstants;
import com.uptime.services.util.JsonConverterUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class ManageDeviceController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ManageDeviceController.class.getName());
    
    CreatePointToHwUnitDelegate createPointToHwUnitDelegate;
    UpdatePointToHwUnitDelegate updatePointToHwUnitDelegate;
    DeletePointToHwUnitDelegate deletePointToHwUnitDelegate;
    ReadPointToHwUnitDelegate readPointToHwUnitDelegate;
    ReadAreaDelegate readAreaDelegate;
    ReadAssetDelegate readAssetDelegate;
    ReadPointLocationsDelegate readPointLocationsDelegate;
    ReadPointsDelegate readPointDelegate;
    ReadApAlByPointDelegate readApAlByPointDelegate;
    ReadHwUnitDelegate readHwUnitDelegate;
    ReadDeviceDelegate readDeviceDelegate;
    CreateDeviceDelegate createDeviceDelegate;
    UpdateDeviceDelegate updateDeviceDelegate;
    UpdatePointLocationsDelegate updatePointLocationDelegate;
    CreatePointsDelegate createPointsDelegate;
    DeleteDeviceDelegate deleteDeviceDelegate;
    
    public ManageDeviceController() {
        createPointToHwUnitDelegate = new CreatePointToHwUnitDelegate();
        updatePointToHwUnitDelegate = new UpdatePointToHwUnitDelegate();
        readPointToHwUnitDelegate = new ReadPointToHwUnitDelegate();
        readAreaDelegate = new ReadAreaDelegate();
        readAssetDelegate = new ReadAssetDelegate();
        readPointDelegate = new ReadPointsDelegate();
        readApAlByPointDelegate = new ReadApAlByPointDelegate();
        readHwUnitDelegate = new ReadHwUnitDelegate();
        readPointLocationsDelegate = new ReadPointLocationsDelegate();
        readDeviceDelegate = new ReadDeviceDelegate();
        deletePointToHwUnitDelegate = new DeletePointToHwUnitDelegate();
        createDeviceDelegate = new CreateDeviceDelegate();
        updateDeviceDelegate = new UpdateDeviceDelegate();
        createPointsDelegate = new CreatePointsDelegate();
        deleteDeviceDelegate = new DeleteDeviceDelegate();
    }

    /**
     * Return a json of a list of ChannelVO Objects by the given params
     * 
     * @param customer, String Object
     * @param siteId, String Object
     * @param deviceId, String Object
     * @return String Object
     * @throws UnavailableException
     * @throws ReadTimeoutException
     * @throws IllegalArgumentException
     * @throws Exception 
     */
    public String getVerboseDeviceManagementRowsByCustomerSiteIdDevice(String customer, UUID siteId, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteBasestationDevice> siteBasestationDeviceList;
        List<HwUnitToPoint> hwUnitToPointList;
        List<ChannelVO> channelVOList;
        StringBuilder json;
        boolean disabled = false;

        if (customer != null && !customer.isEmpty() &&  deviceId != null && !deviceId.isEmpty() && siteId != null ) {
            if ((hwUnitToPointList = readHwUnitDelegate.getHwUnitToPointByDevice(deviceId.trim())) != null && !hwUnitToPointList.isEmpty()) {
                channelVOList = new ArrayList();
                
                // Add to the list channelVOList
                hwUnitToPointList.stream().forEachOrdered(entity -> {
                    ChannelVO channelVO;
                    ApAlSetVO apAlVO;
                    List<AreaSite> areaSiteList;
                    List<AssetSiteArea> assetSiteAreaList;
                    List<PointLocations> pointLocationsList;
                    List<Points> pointsList;
                    List<ApAlByPoint> apAlByPointList;
                    List<ApAlSetVO> apAlSetVOList;
                    List<GlobalApAlSetsByCustomer> globalApAlSetsByCustomerList;
                    List<SiteApAlSetsByCustomer> siteApAlSetsByCustomerList;
                    List<GlobalTachometers> globalTachometersList;
                    List<SiteTachometers> siteTachometersList;
                    
                    if (entity.getCustomerAcct() != null && !entity.getCustomerAcct().isEmpty() && customer.equals(entity.getCustomerAcct()) &&
                            entity.getSiteId() != null && entity.getSiteId().equals(siteId) &&
                            entity.getAreaId() != null &&
                            entity.getAssetId() != null &&
                            entity.getPointLocationId() != null &&
                            entity.getPointId() != null &&
                            entity.getApSetId() != null) {
                        
                        // Set channelVO Object
                        channelVO = new ChannelVO();
                        channelVO.setCustomerAccount(entity.getCustomerAcct());
                        channelVO.setSiteId(entity.getSiteId());
                        channelVO.setAreaId(entity.getAreaId());
                        channelVO.setAssetId(entity.getAssetId());
                        channelVO.setPointLocationId(entity.getPointLocationId());
                        channelVO.setPointId(entity.getPointId());
                        channelVO.setApSetId(entity.getApSetId());
                        
                        // Set fields from area_site table
                        if(!channelVOList.isEmpty()) {
                            if (channelVOList.stream().anyMatch(vo -> vo.getAreaId().equals(channelVO.getAreaId()))) {
                                for (ChannelVO vo : channelVOList) {
                                    if (vo.getAreaId().equals(channelVO.getAreaId())) {
                                        channelVO.setAreaName(vo.getAreaName());
                                        break;
                                    }
                                }
                            } else if ((areaSiteList = readAreaDelegate.getAreaSiteByPK(channelVO.getCustomerAccount(), channelVO.getSiteId(), channelVO.getAreaId())) != null && !areaSiteList.isEmpty()) {
                                channelVO.setAreaName(areaSiteList.get(0).getAreaName());
                            }
                        } else if ((areaSiteList = readAreaDelegate.getAreaSiteByPK(channelVO.getCustomerAccount(), channelVO.getSiteId(), channelVO.getAreaId())) != null && !areaSiteList.isEmpty()) {
                            channelVO.setAreaName(areaSiteList.get(0).getAreaName());
                        }
                        
                        // Set fields from asset_site_area table
                        if(!channelVOList.isEmpty()) {
                            if (channelVOList.stream().anyMatch(vo -> vo.getAssetId().equals(channelVO.getAssetId()))) {
                                for (ChannelVO vo : channelVOList) {
                                    if (vo.getAssetId().equals(channelVO.getAssetId())) {
                                        channelVO.setAssetName(vo.getAssetName());
                                        break;
                                    }
                                }
                            } else if ((assetSiteAreaList = readAssetDelegate.getAssetSiteAreaByPK(channelVO.getCustomerAccount(), channelVO.getSiteId(), channelVO.getAreaId(), channelVO.getAssetId())) != null && !assetSiteAreaList.isEmpty()) {
                                channelVO.setAssetName(assetSiteAreaList.get(0).getAssetName());
                            }
                        } else if ((assetSiteAreaList = readAssetDelegate.getAssetSiteAreaByPK(channelVO.getCustomerAccount(), channelVO.getSiteId(), channelVO.getAreaId(), channelVO.getAssetId())) != null && !assetSiteAreaList.isEmpty()) {
                            channelVO.setAssetName(assetSiteAreaList.get(0).getAssetName());
                        }
                        
                        // Set fields from point_locations table
                        if(!channelVOList.isEmpty()) {
                            if (channelVOList.stream().anyMatch(vo -> vo.getPointLocationId().equals(channelVO.getPointLocationId()))) {
                                for (ChannelVO vo : channelVOList) {
                                    if (vo.getPointLocationId().equals(channelVO.getPointLocationId())) {
                                        channelVO.setPointLocationName(vo.getPointLocationName());
                                        channelVO.setPointLocationDesc(vo.getPointLocationDesc());
                                        channelVO.setDeviceId(vo.getDeviceId());
                                        channelVO.setBasestationPortNum(vo.getBasestationPortNum());
                                        channelVO.setTachId(vo.getTachId());
                                        channelVO.setRollDiameter(vo.getRollDiameter());
                                        channelVO.setRollDiameterUnits(vo.getRollDiameterUnits());
                                        channelVO.setTachReferenceUnits(vo.getTachReferenceUnits());
                                        channelVO.setSampleInterval(vo.getSampleInterval());
                                        channelVO.setSpeedRatio(vo.getSpeedRatio());
                                        channelVO.setFfSetIds(vo.getFfSetIds());
                                        break;
                                    }
                                }
                            } else if ((pointLocationsList = readPointLocationsDelegate.getPointLocationsByPK(channelVO.getCustomerAccount(), channelVO.getSiteId(), channelVO.getAreaId(), channelVO.getAssetId(), channelVO.getPointLocationId())) != null && !pointLocationsList.isEmpty()) {
                                channelVO.setPointLocationName(pointLocationsList.get(0).getPointLocationName());
                                channelVO.setPointLocationDesc(pointLocationsList.get(0).getDescription());
                                channelVO.setDeviceId(pointLocationsList.get(0).getDeviceSerialNumber());
                                channelVO.setBasestationPortNum(pointLocationsList.get(0).getBasestationPortNum());
                                channelVO.setTachId(pointLocationsList.get(0).getTachId());
                                channelVO.setRollDiameter(pointLocationsList.get(0).getRollDiameter());
                                channelVO.setRollDiameterUnits(pointLocationsList.get(0).getRollDiameterUnits());
                                channelVO.setSampleInterval(pointLocationsList.get(0).getSampleInterval());
                                channelVO.setSpeedRatio(pointLocationsList.get(0).getSpeedRatio());

                                if (pointLocationsList.get(0).getFfSetIds() != null && !pointLocationsList.get(0).getFfSetIds().isEmpty()) {
                                    channelVO.setFfSetIds(pointLocationsList.get(0).getFfSetIds());
                                }
                                
                                if (channelVO.getTachId() != null) {
                                    if ((globalTachometersList = readDeviceDelegate.findByPK(channelVO.getCustomerAccount(), channelVO.getTachId())) != null && !globalTachometersList.isEmpty()) {
                                        channelVO.setTachReferenceUnits(globalTachometersList.get(0).getTachReferenceUnits());
                                    } else if ((siteTachometersList = readDeviceDelegate.findByPK(channelVO.getCustomerAccount(), channelVO.getSiteId(), channelVO.getTachId())) != null && !siteTachometersList.isEmpty()) {
                                        channelVO.setTachReferenceUnits(siteTachometersList.get(0).getTachReferenceUnits());
                                    } 
                                    else if ((globalTachometersList = readDeviceDelegate.findByPK(ServiceConstants.DEFAULT_UPTIME_ACCOUNT, channelVO.getTachId())) != null && !globalTachometersList.isEmpty()) {
                                        channelVO.setTachReferenceUnits(globalTachometersList.get(0).getTachReferenceUnits());
                                    }
                                }
                            }
                        } else if ((pointLocationsList = readPointLocationsDelegate.getPointLocationsByPK(channelVO.getCustomerAccount(), channelVO.getSiteId(), channelVO.getAreaId(), channelVO.getAssetId(), channelVO.getPointLocationId())) != null && !pointLocationsList.isEmpty()) {
                            channelVO.setPointLocationName(pointLocationsList.get(0).getPointLocationName());
                            channelVO.setPointLocationDesc(pointLocationsList.get(0).getDescription());
                            channelVO.setDeviceId(pointLocationsList.get(0).getDeviceSerialNumber());
                            channelVO.setBasestationPortNum(pointLocationsList.get(0).getBasestationPortNum());
                            channelVO.setTachId(pointLocationsList.get(0).getTachId());
                            channelVO.setRollDiameter(pointLocationsList.get(0).getRollDiameter());
                            channelVO.setRollDiameterUnits(pointLocationsList.get(0).getRollDiameterUnits());
                            channelVO.setSampleInterval(pointLocationsList.get(0).getSampleInterval());
                            channelVO.setSpeedRatio(pointLocationsList.get(0).getSpeedRatio());

                            if (pointLocationsList.get(0).getFfSetIds() != null && !pointLocationsList.get(0).getFfSetIds().isEmpty()) {
                                channelVO.setFfSetIds(pointLocationsList.get(0).getFfSetIds());
                            }
                            
                            if (channelVO.getTachId() != null) {
                                if ((globalTachometersList = readDeviceDelegate.findByPK(channelVO.getCustomerAccount(), channelVO.getTachId())) != null && !globalTachometersList.isEmpty()) {
                                    channelVO.setTachReferenceUnits(globalTachometersList.get(0).getTachReferenceUnits());
                                } else if ((siteTachometersList = readDeviceDelegate.findByPK(channelVO.getCustomerAccount(), channelVO.getSiteId(), channelVO.getTachId())) != null && !siteTachometersList.isEmpty()) {
                                    channelVO.setTachReferenceUnits(siteTachometersList.get(0).getTachReferenceUnits());
                                } else if ((globalTachometersList = readDeviceDelegate.findByPK(ServiceConstants.DEFAULT_UPTIME_ACCOUNT, channelVO.getTachId())) != null && !globalTachometersList.isEmpty()) {
                                    channelVO.setTachReferenceUnits(globalTachometersList.get(0).getTachReferenceUnits());
                                }
                            }
                        }
                        
                        // Set fields from Points table
                        if ((pointsList = readPointDelegate.getPointsByPK(channelVO.getCustomerAccount(), channelVO.getSiteId(), channelVO.getAreaId(), channelVO.getAssetId(), channelVO.getPointLocationId(), channelVO.getPointId(), channelVO.getApSetId())) != null && !pointsList.isEmpty()) {
                            channelVO.setPointName(pointsList.get(0).getPointName());
                            channelVO.setAlarmEnabled(pointsList.get(0).isAlarmEnabled());
                            channelVO.setAlSetId(pointsList.get(0).getAlSetId());
                            channelVO.setPointType(pointsList.get(0).getPointType());
                            channelVO.setSensorChannelNum(pointsList.get(0).getSensorChannelNum());
                            channelVO.setSensorType(pointsList.get(0).getSensorType());
                            channelVO.setSensorOffset(pointsList.get(0).getSensorOffset());
                            channelVO.setSensorSensitivity(pointsList.get(0).getSensorSensitivity());
                            channelVO.setSensorUnits(pointsList.get(0).getSensorUnits());
                            
                            // Set fields from ap_al_by_point table
                            if ((apAlByPointList = readPointDelegate.getByCustomerSiteAreaAssetLocationPointApSetAlSet(channelVO.getCustomerAccount(), channelVO.getSiteId(), channelVO.getAreaId(), channelVO.getAssetId(), channelVO.getPointLocationId(), channelVO.getPointId(), channelVO.getApSetId(), channelVO.getAlSetId())) != null && !apAlByPointList.isEmpty()) {
                                channelVO.setResolution(apAlByPointList.get(0).getResolution());
                                channelVO.setfMax(apAlByPointList.get(0).getfMax());
                                channelVO.setPeriod(apAlByPointList.get(0).getPeriod());
                                channelVO.setSampleRate(apAlByPointList.get(0).getSampleRate());
                                
                                if(!channelVOList.isEmpty()) {
                                    if (channelVOList.stream().anyMatch(vo -> vo.getApSetId().equals(channelVO.getApSetId()) && vo.getAlSetId().equals(channelVO.getAlSetId()))) {
                                        for (ChannelVO vo : channelVOList) {
                                            if (vo.getApSetId().equals(channelVO.getApSetId()) && vo.getAlSetId().equals(channelVO.getAlSetId())) {
                                                channelVO.setApSetName(vo.getApSetName());
                                                channelVO.setAlSetName(vo.getAlSetName());
                                                break;
                                            }
                                        }
                                    } else {
                                        if ((globalApAlSetsByCustomerList = readApAlByPointDelegate.findByGlobalApSetAlSet(channelVO.getCustomerAccount(), channelVO.getSensorType(), channelVO.getApSetId(), channelVO.getAlSetId())) != null && !globalApAlSetsByCustomerList.isEmpty()) {
                                            channelVO.setApSetName(globalApAlSetsByCustomerList.get(0).getApSetName());
                                            channelVO.setAlSetName(globalApAlSetsByCustomerList.get(0).getAlSetName());
                                        } else if ((siteApAlSetsByCustomerList = readApAlByPointDelegate.findBySiteApSetAlSet(channelVO.getCustomerAccount(), channelVO.getSiteId(), channelVO.getSensorType(), channelVO.getApSetId(), channelVO.getAlSetId())) != null && !siteApAlSetsByCustomerList.isEmpty()) {
                                            channelVO.setApSetName(siteApAlSetsByCustomerList.get(0).getApSetName());
                                            channelVO.setAlSetName(siteApAlSetsByCustomerList.get(0).getAlSetName());
                                        } 
                                        else if ((globalApAlSetsByCustomerList = readApAlByPointDelegate.findByGlobalApSetAlSet(ServiceConstants.DEFAULT_UPTIME_ACCOUNT, channelVO.getSensorType(), channelVO.getApSetId(), channelVO.getAlSetId())) != null && !globalApAlSetsByCustomerList.isEmpty()) {
                                            channelVO.setApSetName(globalApAlSetsByCustomerList.get(0).getApSetName());
                                            channelVO.setAlSetName(globalApAlSetsByCustomerList.get(0).getAlSetName());
                                        }
                                    }
                                } else {
                                    if ((globalApAlSetsByCustomerList = readApAlByPointDelegate.findByGlobalApSetAlSet(channelVO.getCustomerAccount(), channelVO.getSensorType(), channelVO.getApSetId(), channelVO.getAlSetId())) != null && !globalApAlSetsByCustomerList.isEmpty()) {
                                        channelVO.setApSetName(globalApAlSetsByCustomerList.get(0).getApSetName());
                                        channelVO.setAlSetName(globalApAlSetsByCustomerList.get(0).getAlSetName());
                                    } else if ((siteApAlSetsByCustomerList = readApAlByPointDelegate.findBySiteApSetAlSet(channelVO.getCustomerAccount(), channelVO.getSiteId(), channelVO.getSensorType(), channelVO.getApSetId(), channelVO.getAlSetId())) != null && !siteApAlSetsByCustomerList.isEmpty()) {
                                        channelVO.setApSetName(siteApAlSetsByCustomerList.get(0).getApSetName());
                                        channelVO.setAlSetName(siteApAlSetsByCustomerList.get(0).getAlSetName());
                                    } 
                                    else if ((globalApAlSetsByCustomerList = readApAlByPointDelegate.findByGlobalApSetAlSet(ServiceConstants.DEFAULT_UPTIME_ACCOUNT, channelVO.getSensorType(), channelVO.getApSetId(), channelVO.getAlSetId())) != null && !globalApAlSetsByCustomerList.isEmpty()) {
                                        channelVO.setApSetName(globalApAlSetsByCustomerList.get(0).getApSetName());
                                        channelVO.setAlSetName(globalApAlSetsByCustomerList.get(0).getAlSetName());
                                    }
                                }

                                apAlSetVOList = new ArrayList();
                                for (ApAlByPoint apAlByPoint : apAlByPointList) {
                                    apAlVO = new ApAlSetVO();
                                    apAlVO.setAlSetId(apAlByPoint.getAlSetId());
                                    apAlVO.setApSetId(apAlByPoint.getApSetId());
                                    apAlVO.setCustomerAccount(apAlByPoint.getCustomerAccount());
                                    apAlVO.setDemod(apAlByPoint.isDemodEnabled());
                                    apAlVO.setDemodHighPass(apAlByPoint.getDemodHighPass());
                                    apAlVO.setDemodLowPass(apAlByPoint.getDemodLowPass());
                                    apAlVO.setDwell(apAlByPoint.getDwell());
                                    apAlVO.setFmax(apAlByPoint.getfMax());
                                    apAlVO.setFrequencyUnits(apAlByPoint.getFreqUnits());
                                    apAlVO.setHighAlert(apAlByPoint.getHighAlert());
                                    apAlVO.setHighAlertActive(apAlByPoint.isHighAlertActive());
                                    apAlVO.setHighFault(apAlByPoint.getHighFault());
                                    apAlVO.setHighFaultActive(apAlByPoint.isHighFaultActive());
                                    apAlVO.setLowAlert(apAlByPoint.getLowAlert());
                                    apAlVO.setLowAlertActive(apAlByPoint.isLowAlertActive());
                                    apAlVO.setLowFault(apAlByPoint.getLowFault());
                                    apAlVO.setLowFaultActive(apAlByPoint.isLowFaultActive());
                                    apAlVO.setMaxFrequency(apAlByPoint.getMaxFreq());
                                    apAlVO.setMinFrequency(apAlByPoint.getMinFreq());
                                    apAlVO.setParamAmpFactor(apAlByPoint.getParamAmpFactor());
                                    apAlVO.setParamName(apAlByPoint.getParamName());
                                    apAlVO.setParamType(apAlByPoint.getParamType());
                                    apAlVO.setParamUnits(apAlByPoint.getParamUnits());
                                    apAlVO.setPeriod(apAlByPoint.getPeriod());
                                    apAlVO.setResolution(apAlByPoint.getResolution());
                                    apAlVO.setSampleRate(apAlByPoint.getSampleRate());
                                    apAlVO.setSensorType(apAlByPoint.getSensorType());
                                    apAlVO.setSiteId(apAlByPoint.getSiteId());
                                    apAlVO.setApSetName(channelVO.getApSetName());
                                    apAlVO.setAlSetName(channelVO.getAlSetName());
                                    apAlSetVOList.add(apAlVO);
                                }

                                channelVO.setApAlSetVOs(apAlSetVOList);
                            }
                        }
                        
                        // Add to List
                        channelVOList.add(channelVO);
                    }
                });
                
                // Send Json based on the field deviceManagementRowVOList
                if (!channelVOList.isEmpty()) {
                    
                    // Set the disabled field
                    if ((siteBasestationDeviceList = readDeviceDelegate.getSiteBasestationDeviceByCustomerSiteBasestationDevice(customer, siteId, channelVOList.get(0).getBasestationPortNum(), deviceId)) != null && !siteBasestationDeviceList.isEmpty()) {
                        disabled = siteBasestationDeviceList.get(0).isDisabled();
                    }
                    
                    json = new StringBuilder();
                    json    
                            .append("{\"customer\":\"").append(customer).append("\",")
                            .append("\"siteId\":\"").append(siteId.toString()).append("\",")
                            .append("\"deviceId\":\"").append(deviceId).append("\",")
                            .append("\"disabled\":").append(disabled).append(",")
                            .append("\"channelVOs\":[");
                    for (int i = 0; i < channelVOList.size(); i++) {
                        json.append(JsonConverterUtil.toJSON(channelVOList.get(i))).append(i < channelVOList.size() - 1 ? "," : "");
                    }
                    json.append("],\"outcome\":\"GET worked successfully.\"}");
                    return json.toString();
                } else {
                    return "{\"outcome\":\"Item/Items not found.\"}";
                }
                
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a list of DeviceManagementRowVO Objects by the given params
     * 
     * @param customer, String Object
     * @param siteId, String Object
     * @param deviceId, String Object
     * @return String Object
     * @throws UnavailableException
     * @throws ReadTimeoutException
     * @throws IllegalArgumentException
     * @throws Exception 
     */
    public String getDeviceManagementRowsByCustomerSiteIdDevice(String customer, UUID siteId, String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<HwUnitToPoint> hwUnitToPointList;
        List<DeviceManagementRowVO> deviceManagementRowVOList;
        List<SiteBasestationDevice> siteBasestationDeviceList;
        StringBuilder json;
        boolean disabled = false;

        if (customer != null && !customer.isEmpty() &&  deviceId != null && !deviceId.isEmpty() && siteId != null ) {
            if ((hwUnitToPointList = readHwUnitDelegate.getHwUnitToPointByDevice(deviceId.trim())) != null && !hwUnitToPointList.isEmpty()) {
                deviceManagementRowVOList = new ArrayList();
                
                // Add to the list deviceManagementRowVOList
                hwUnitToPointList.stream().forEachOrdered(entity -> {
                    DeviceManagementRowVO deviceManagementRowVO;
                    List<AreaSite> areaSiteList;
                    List<AssetSiteArea> assetSiteAreaList;
                    List<PointLocations> pointLocationsList;
                    List<Points> pointsList;
                    List<ApAlByPoint> apAlByPointList;
                    
                    if (entity.getCustomerAcct() != null && !entity.getCustomerAcct().isEmpty() && customer.equals(entity.getCustomerAcct()) &&
                            entity.getSiteId() != null && entity.getSiteId().equals(siteId) &&
                            entity.getAreaId() != null &&
                            entity.getAssetId() != null &&
                            entity.getPointLocationId() != null &&
                            entity.getPointId() != null &&
                            entity.getApSetId() != null) {
                        
                        // Set channelVO Object
                        deviceManagementRowVO = new DeviceManagementRowVO(entity);
                        
                        // Set fields from Points table
                        if ((pointsList = readPointDelegate.getPointsByPK(entity.getCustomerAcct(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getApSetId())) != null && !pointsList.isEmpty()) {
                            deviceManagementRowVO.setPointName(pointsList.get(0).getPointName());
                            deviceManagementRowVO.setAlarmEnabled(pointsList.get(0).isAlarmEnabled());
                        }
                        
                        // Set fields from ap_al_by_point table
                        if ((apAlByPointList = readPointDelegate.getByCustomerSiteAreaAssetLocationPointApSet(entity.getCustomerAcct(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId(), entity.getPointId(), entity.getApSetId())) != null && !apAlByPointList.isEmpty()) {
                            deviceManagementRowVO.setSensorType(apAlByPointList.get(0).getSensorType());
                            deviceManagementRowVO.setResolution(apAlByPointList.get(0).getResolution());
                            deviceManagementRowVO.setFmax(apAlByPointList.get(0).getfMax());
                        }
                        
                        // Set fields from point_locations table
                        if(!deviceManagementRowVOList.isEmpty()) {
                            if (deviceManagementRowVOList.stream().anyMatch(vo -> vo.getPointLocationId().equals(entity.getPointLocationId()))) {
                                for (DeviceManagementRowVO vo : deviceManagementRowVOList) {
                                    if (vo.getPointLocationId().equals(entity.getPointLocationId())) {
                                        deviceManagementRowVO.setPointLocationName(vo.getPointLocationName());
                                        deviceManagementRowVO.setBasestationPortNum(vo.getBasestationPortNum());
                                        break;
                                    }
                                }
                            } else if ((pointLocationsList = readPointLocationsDelegate.getPointLocationsByPK(entity.getCustomerAcct(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId())) != null && !pointLocationsList.isEmpty()) {
                                deviceManagementRowVO.setPointLocationName(pointLocationsList.get(0).getPointLocationName());
                                deviceManagementRowVO.setBasestationPortNum(pointLocationsList.get(0).getBasestationPortNum());
                            }
                        } else if ((pointLocationsList = readPointLocationsDelegate.getPointLocationsByPK(entity.getCustomerAcct(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId(), entity.getPointLocationId())) != null && !pointLocationsList.isEmpty()) {
                            deviceManagementRowVO.setPointLocationName(pointLocationsList.get(0).getPointLocationName());
                            deviceManagementRowVO.setBasestationPortNum(pointLocationsList.get(0).getBasestationPortNum());
                        }
                        
                        // Set fields from asset_site_area table
                        if(!deviceManagementRowVOList.isEmpty()) {
                            if (deviceManagementRowVOList.stream().anyMatch(vo -> vo.getAssetId().equals(entity.getAssetId()))) {
                                for (DeviceManagementRowVO vo : deviceManagementRowVOList) {
                                    if (vo.getAssetId().equals(entity.getAssetId())) {
                                        deviceManagementRowVO.setAssetName(vo.getAssetName());
                                        break;
                                    }
                                }
                            } else if ((assetSiteAreaList = readAssetDelegate.getAssetSiteAreaByPK(entity.getCustomerAcct(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId())) != null && !assetSiteAreaList.isEmpty()) {
                                deviceManagementRowVO.setAssetName(assetSiteAreaList.get(0).getAssetName());
                            }
                        } else if ((assetSiteAreaList = readAssetDelegate.getAssetSiteAreaByPK(entity.getCustomerAcct(), entity.getSiteId(), entity.getAreaId(), entity.getAssetId())) != null && !assetSiteAreaList.isEmpty()) {
                            deviceManagementRowVO.setAssetName(assetSiteAreaList.get(0).getAssetName());
                        }
                        
                        // Set fields from area_site table
                        if(!deviceManagementRowVOList.isEmpty()) {
                            if (deviceManagementRowVOList.stream().anyMatch(vo -> vo.getAreaId().equals(entity.getAreaId()))) {
                                for (DeviceManagementRowVO vo : deviceManagementRowVOList) {
                                    if (vo.getAreaId().equals(entity.getAreaId())) {
                                        deviceManagementRowVO.setAreaName(vo.getAreaName());
                                        break;
                                    }
                                }
                            } else if ((areaSiteList = readAreaDelegate.getAreaSiteByPK(entity.getCustomerAcct(), entity.getSiteId(), entity.getAreaId())) != null && !areaSiteList.isEmpty()) {
                                deviceManagementRowVO.setAreaName(areaSiteList.get(0).getAreaName());
                            }
                        } else if ((areaSiteList = readAreaDelegate.getAreaSiteByPK(entity.getCustomerAcct(), entity.getSiteId(), entity.getAreaId())) != null && !areaSiteList.isEmpty()) {
                            deviceManagementRowVO.setAreaName(areaSiteList.get(0).getAreaName());
                        }
                        
                        // Add to List
                        deviceManagementRowVOList.add(deviceManagementRowVO);
                    }
                });
                
                // Send Json based on the field deviceManagementRowVOList
                if (!deviceManagementRowVOList.isEmpty()) {
                    
                    // Set the disabled field
                    if ((siteBasestationDeviceList = readDeviceDelegate.getSiteBasestationDeviceByCustomerSiteBasestationDevice(customer, siteId, deviceManagementRowVOList.get(0).getBasestationPortNum(), deviceId)) != null && !siteBasestationDeviceList.isEmpty()) {
                        disabled = siteBasestationDeviceList.get(0).isDisabled();
                    }
                    
                    json = new StringBuilder();
                    json    
                            .append("{\"customer\":\"").append(customer).append("\",")
                            .append("\"siteId\":\"").append(siteId.toString()).append("\",")
                            .append("\"deviceId\":\"").append(deviceId).append("\",")
                            .append("\"disabled\":").append(disabled).append(",")
                            .append("\"deviceManagementRows\":[");
                    for (int i = 0; i < deviceManagementRowVOList.size(); i++) {
                        json.append(JsonConverterUtil.toJSON(deviceManagementRowVOList.get(i))).append(i < deviceManagementRowVOList.size() - 1 ? "," : "");
                    }
                    json.append("],\"outcome\":\"GET worked successfully.\"}");
                    return json.toString();
                } else {
                    return "{\"outcome\":\"Item/Items not found.\"}";
                }
                
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of PointToHwUnit Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, UUID Object
     * @param areaId, UUID Object
     * @param assetId, UUID Object
     * @param pointLocationId, UUID Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getPointToHwUnitByCustomerSiteAreaAssetLocation(String customer, UUID siteId, UUID areaId, UUID assetId, UUID pointLocationId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<Points> point_result;
        StringBuilder json;

        if ((point_result = readPointDelegate.getPointsByCustomerSiteAreaAssetLocation(customer.trim(), siteId, areaId, assetId, pointLocationId)) != null) {
            if (!point_result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"customer\":\"").append(customer).append("\",")
                        .append("\"siteId\":\"").append(siteId.toString()).append("\",")
                        .append("\"areaId\":\"").append(areaId.toString()).append("\",")
                        .append("\"assetId\":\"").append(assetId.toString()).append("\",")
                        .append("\"pointLocationId\":\"").append(pointLocationId.toString()).append("\",")
                        .append("\"PointToHwUnit\":[");

                point_result.forEach(point -> {
                    List<PointToHwUnit> result;
                    if ((result = readPointToHwUnitDelegate.getPointToHwUnitByPoint(point.getPointId())) != null) {
                        try {
                            for (int i = 0; i < result.size(); i++) {
                                json.append(JsonConverterUtil.toJSON(result.get(i))).append(",");
                            }
                        } catch (Exception exp) {
                            LOGGER.error(exp.getMessage(), exp);
                            System.out.println("Exception occured: " + exp.getMessage());
                        }
                    }
                });
                //remove the last comma inside the array
                json.deleteCharAt(json.length() - 1);
                json.append("],\"outcome\":\"GET worked successfully.\"}");

                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of device Objects by the given params
     *
     * @param customer, String Object
     * @param siteName, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getDeviceStatusByCustomerSiteName(String customer, String siteName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<DeviceStatusVO> result;
        StringBuilder json;

        if ((result = readDeviceDelegate.getDeviceStatusByCustomerSiteName(customer.trim(), siteName.trim())) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer_acct\":\"").append(customer).append("\",")
                    .append("\"site_name\":\"").append(siteName).append("\",")
                    .append("\"devices\":[");

                try {
                    for (int i = 0; i < result.size(); i++) {
                        json
                            .append("{\"device_id\":").append(result.get(i).getDeviceId() != null ? "\"" : "").append(result.get(i).getDeviceId()).append(result.get(i).getDeviceId() != null ? "\"," : ",")
                            .append("\"area_name\":").append(result.get(i).getAreaName() != null ? "\"" : "").append(result.get(i).getAreaName()).append(result.get(i).getAreaName() != null ? "\"," : ",")
                            .append("\"asset_name\":").append(result.get(i).getAssetName() != null ? "\"" : "").append(result.get(i).getAssetName()).append(result.get(i).getAssetName() != null ? "\"," : ",")
                            .append("\"point_location_name\":").append(result.get(i).getPointLocationName() != null ? "\"" : "").append(result.get(i).getPointLocationName()).append(result.get(i).getPointLocationName() != null ? "\"," : ",")
                            .append("\"battery_voltage\":").append(result.get(i).getBatteryVoltage()).append(",")
                            .append("\"is_disabled\":").append(result.get(i).isIsDisabled()).append(",")
                            .append("\"last_sample\":").append(result.get(i).getLastSample() != null ? "\"" : "").append(result.get(i).getLastSample()).append(result.get(i).getLastSample() != null ? "\"" : "").append(i < result.size() - 1 ? "}," : "}");
                    }
                } catch (Exception exp) {
                    LOGGER.error(exp.getMessage(), exp);
                    System.out.println("Exception occured: " + exp.getMessage());
                }
                //remove the last comma inside the array
                json.append("]}");

                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Install new Device
     *
     * @param content, String Object
     * @return String Object
     */
    public String installNewDevice(String content) {
        DeviceVO installDeviceVO;

        if ((installDeviceVO = JsonUtil.manageDeviceParser(content)) != null) {
            if (installDeviceVO != null
                    && installDeviceVO.getNewDeviceId() != null && !installDeviceVO.getNewDeviceId().isEmpty()
                    && installDeviceVO.getCustomerAccount() != null && !installDeviceVO.getCustomerAccount().isEmpty()
                    && installDeviceVO.getSiteId() != null && installDeviceVO.getAreaId() != null
                    && installDeviceVO.getAssetId() != null && installDeviceVO.getPointLocationId() != null
                    && installDeviceVO.getPointList() != null && !installDeviceVO.getPointList().isEmpty()
                    && installDeviceVO.getPointList().stream().allMatch(p -> p.getPointName() != null && !p.getPointName().isEmpty())) {

                try {
                    // Install Device with New Points
                    if (!installDeviceVO.isDbpointListExists()) {
                        return createDeviceDelegate.installDeviceWithNewPoints(installDeviceVO);
                    } else {// Install Device with Existing Points
                        return createDeviceDelegate.installDeviceWithExistingPoints(installDeviceVO);
                    }
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to installed device.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
    
    /**
     * Update the given device
     *
     * @param content, String Object
     * @return String Object
     */
    public String updateDevice(String content) {
        DeviceVO deviceVO;

        if ((deviceVO = JsonUtil.manageDeviceParser(content)) != null) {
            if (deviceVO.getRequest() != null && !deviceVO.getRequest().isEmpty()) {
                switch (deviceVO.getRequest().toLowerCase()) {
                    case "replacedevice":
                        return replaceDevice(deviceVO);
                    case "enabledevice":
                        return enableDevice(deviceVO);
                    case "disabledevice":
                        return disableDevice(deviceVO);
                    default:
                        return "{\"outcome\":\"Invalid Request type in json\"}";
                }
            } else {
                return "{\"outcome\":\"Request type missing in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Install Or modify a TS1 Device
     *
     * @param content, String Object
     * @param hwDisabled, boolean
     * @param modified, boolean
     * @return String Object
     */
    public String installOrModifyTS1Device(String content, boolean hwDisabled, boolean modified) {
        List<AssetVO> assetVOList;
        StringBuilder areaStr = new StringBuilder(), assetStr = new StringBuilder(), plStr = new StringBuilder();
        DeviceStatusVO deviceStatusVO;
        DeviceStatusDAO deviceStatusDAO;
        SiteCustomerDAO siteCustomerDAO;
        AreaSiteDAO areaSiteDAO;
        List<SiteCustomer> siteList;
        List<AreaSite> areaList;
        String siteName = null, areaName = null;
        
        if ((assetVOList = JsonUtil.parserList(content)) != null && !assetVOList.isEmpty()) {
            for (AssetVO assetVO : assetVOList) {
                if (!(assetVO != null
                    && assetVO.getCustomerAccount() != null && !assetVO.getCustomerAccount().isEmpty()
                    && assetVO.getAssetName() != null && !assetVO.getAssetName().isEmpty()
                    && assetVO.getSiteId() != null
                    && assetVO.getAreaId() != null)) {
                    
                    return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
                }
            }
            
            // Insert into Cassandra
            try {
                //Query the site and area tables to fetch the names. 
                if (assetVOList.get(0).getSiteName() == null ||
                        assetVOList.get(0).getSiteName().isEmpty()) {
                    siteCustomerDAO = ConfigMapperImpl.getInstance().siteCustomerDAO();
                    if ((siteList = siteCustomerDAO.findByPK(assetVOList.get(0).getCustomerAccount(), assetVOList.get(0).getSiteId())) != null && !siteList.isEmpty()) {
                        siteName = siteList.get(0).getSiteName();
                    }
                }
                else {
                    siteName = assetVOList.get(0).getSiteName();
                }

                areaSiteDAO = ConfigMapperImpl.getInstance().areaSiteDAO();
                for (AssetVO assetVO : assetVOList) {
                    if (assetVO.getAreaName() == null || assetVO.getAreaName().isEmpty()) {
                        if ((areaList = areaSiteDAO.findByPK(assetVO.getCustomerAccount(), assetVO.getSiteId(), assetVO.getAreaId())) != null && !areaList.isEmpty()) {
                            areaName = areaList.get(0).getAreaName();
                            if (!areaStr.toString().contains(areaList.get(0).getAreaName()))
                                areaStr.append(areaList.get(0).getAreaName()).append(", ");
                        }
                    }
                    else {
                        areaName = assetVO.getAreaName();
                        if (!areaStr.toString().contains(assetVO.getAreaName()))
                            areaStr.append(assetVO.getAreaName()).append(", ");
                    }

                    if (!assetStr.toString().contains(assetVO.getAssetName()))
                            assetStr.append(assetVO.getAssetName()).append(", ");
                    if (assetVO.getPointLocationList() != null && !assetVO.getPointLocationList().isEmpty()
                        && assetVO.getPointLocationList().get(0).getPointLocationName() != null && !assetVO.getPointLocationList().get(0).getPointLocationName().isEmpty()
                        && !plStr.toString().contains(assetVO.getPointLocationList().get(0).getPointLocationName()))
                        plStr.append(assetVO.getPointLocationList().get(0).getPointLocationName()).append(", ");
                    if (!createDeviceDelegate.installOrModifyTS1Channel(assetVO, siteName, areaName, hwDisabled, modified).equalsIgnoreCase("{\"outcome\":\"Install/modify TS1 Channel successfully.\"}")) {
                        return "{\"outcome\":\"Error: Failed to install/modify the complete TS1 Device. A partial install/modify is possible.\"}";
                    }
                }
                
                //Update Device Status for TS1
                deviceStatusVO = new DeviceStatusVO();
                deviceStatusDAO = new DeviceStatusDAO();
                deviceStatusVO.setCustomerAcct(assetVOList.get(0).getCustomerAccount());
                deviceStatusVO.setSiteName(siteName);
                deviceStatusVO.setAreaName(areaStr.substring(0, areaStr.length() - 2));
                deviceStatusVO.setAssetName(assetStr.substring(0, assetStr.length() - 2));
                deviceStatusVO.setPointLocationName(plStr.substring(0, plStr.length() - 2));
                deviceStatusVO.setIsDisabled(hwDisabled);
                deviceStatusVO.setDeviceId(assetVOList.get(0).getPointLocationList().get(0).getDeviceSerialNumber());
                
                //Update MySQL device_status
                deviceStatusDAO.upsert(deviceStatusVO);
                return "{\"outcome\":\"Install/modify TS1 Device successfully.\"}";
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                return "{\"outcome\":\"Error: Failed to install/modify the complete TS1 Device. A partial install/modify is possible.\"}";
            }
        } else {
            return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        }
    }

    /**
     * Replace device with existing device id
     *
     * @param replaceDeviceVO, DeviceVO Object
     * @return String Object
     */
    private String replaceDevice(DeviceVO replaceDeviceVO) {
        if (replaceDeviceVO.getExistingDeviceId() != null && !replaceDeviceVO.getExistingDeviceId().isEmpty()
                && replaceDeviceVO.getPointList() != null && !replaceDeviceVO.getPointList().isEmpty()
                && replaceDeviceVO.getNewDeviceId() != null && !replaceDeviceVO.getNewDeviceId().isEmpty()
                && replaceDeviceVO.getCustomerAccount() != null && !replaceDeviceVO.getCustomerAccount().isEmpty()
                && replaceDeviceVO.getSiteId() != null && replaceDeviceVO.getAreaId() != null
                && replaceDeviceVO.getAssetId() != null && replaceDeviceVO.getPointLocationId() != null) {
            try {
                return updateDeviceDelegate.replaceDevice(replaceDeviceVO);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                return "{\"outcome\":\"Error: Failed to replace device.\"}";
            }

        } else {
            return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        }
    }

    /**
     * Enable a device 
     *
     * @param deviceVO, DeviceVO Object
     * @return String Object
     */
    private String enableDevice(DeviceVO deviceVO) {
        if (deviceVO.getExistingDeviceId() != null) {
            return updateDeviceDelegate.enableDevice(deviceVO);
        } else {
            return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        }
    }
    
    /**
     * Disable a device 
     *
     * @param deviceVO, DeviceVO Object
     * @return String Object
     */
    private String disableDevice(DeviceVO deviceVO) {
        if (deviceVO != null && deviceVO.getExistingDeviceId() != null && !deviceVO.getExistingDeviceId().isEmpty()) {
            return updateDeviceDelegate.disableDevice(deviceVO);
        } else {
            return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        }
    }

    /**
     * Delete a device 
     *
     * @param deviceVO, DeviceVO Object
     * @return String Object
     */
    public String deleteDevice(DeviceVO deviceVO) {
        if (deviceVO.getExistingDeviceId() != null) {
            try {
                return deleteDeviceDelegate.deleteDevice(deviceVO);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                return "{\"outcome\":\"Error: Failed to delete device.\"}";
            }
        } else {
            return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        }
    }
    
    /**
     * Update the given device status in MySQL DB
     *
     * @param content, String Object
     * @return String Object
     */
    public String updateDeviceStatus(String content) {
        DeviceStatusVO deviceStatusVO;

        if ((deviceStatusVO = JsonUtil.deviceStatusParser(content)) != null) {
            if (deviceStatusVO.getDeviceId() != null && !deviceStatusVO.getDeviceId().isEmpty()
                    && deviceStatusVO.getLastSample() != null) {
                return updateDeviceDelegate.updateDeviceStatus(deviceStatusVO);
            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    public static void main(String arg[]) {
        System.out.println("Hellooo");
        ManageDeviceController controller = new ManageDeviceController();
        try {
            DeviceVO replaceDeviceVO = new DeviceVO();
            replaceDeviceVO.setExistingDeviceId("BB001928");
            replaceDeviceVO.setNewDeviceId("BB390000");
//            String installDeviceJson = "{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"3ff9a86e-292e-4d64-85f3-271148276904\",\"assetId\":\"c9a6140b-6b35-4b9e-bc8e-6c1c29c6728e\",\"pointLocationId\":\"ac61dd8b-fdc4-4e8d-bf28-29e61a5e341d\",\"channelNum\":0,\"pointList\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"3ff9a86e-292e-4d64-85f3-271148276904\",\"assetId\":\"c9a6140b-6b35-4b9e-bc8e-6c1c29c6728e\",\"pointLocationId\":\"ac61dd8b-fdc4-4e8d-bf28-29e61a5e341d\",\"apSetId\":\"8ed39428-92cd-4eb0-9e47-8dbc70aafbfd\",\"alSetId\":\"000a743d-6494-480a-b319-4de14eb1cc27\",\"pointName\":\"997 ULTRA\",\"autoAcknowledge\":true,\"disabled\":false,\"pointType\":\"AC\",\"sampleInterval\":0,\"sensorChannelNum\":0,\"sensorOffset\":0.0,\"sensorSensitivity\":50.0,\"sensorType\":\"Ultrasonic\",\"sensorUnits\":\"Pa\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":0,\"fMax\":0,\"demodInterval\":0,\"alarmEnabled\":false,\"highAlert\":0.0,\"highAlertActive\":false,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":false,\"lowAlert\":0.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"period\":0.0,\"resolution\":0,\"sampleRate\":0.0,\"apAlSetVOs\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"apSetName\":\"AUTO_ULTRASONIC\",\"apSetId\":\"8ed39428-92cd-4eb0-9e47-8dbc70aafbfd\",\"alSetName\":\"2560 ASSET 317_ULTRA\",\"alSetId\":\"000a743d-6494-480a-b319-4de14eb1cc27\",\"sensorType\":\"Ultrasonic\",\"fmax\":0,\"resolution\":0,\"period\":0.0,\"sampleRate\":0.0,\"demod\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"minFrequency\":0.0,\"maxFrequency\":0.0,\"dwell\":0,\"lowFaultActive\":false,\"lowAlertActive\":false,\"highAlertActive\":false,\"highFaultActive\":false,\"lowFault\":0.0,\"lowAlert\":0.0,\"highAlert\":0.0,\"highFault\":0.0}]},{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"3ff9a86e-292e-4d64-85f3-271148276904\",\"assetId\":\"c9a6140b-6b35-4b9e-bc8e-6c1c29c6728e\",\"pointLocationId\":\"ac61dd8b-fdc4-4e8d-bf28-29e61a5e341d\",\"apSetId\":\"110a3388-51be-4005-9fe0-f7b8b319720c\",\"alSetId\":\"212aca52-38c7-445d-98ac-daf07c6c9177\",\"pointName\":\"997 VIBE\",\"autoAcknowledge\":false,\"disabled\":false,\"pointType\":\"AC\",\"sampleInterval\":0,\"sensorChannelNum\":1,\"sensorOffset\":0.0,\"sensorSensitivity\":50.0,\"sensorType\":\"Acceleration\",\"sensorUnits\":\"Gs\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":0,\"fMax\":0,\"demodInterval\":0,\"alarmEnabled\":true,\"highAlert\":0.0,\"highAlertActive\":false,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":false,\"lowAlert\":0.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"period\":0.0,\"resolution\":0,\"sampleRate\":0.0,\"apAlSetVOs\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"apSetName\":\"*****Global Ap Set 2\",\"apSetId\":\"110a3388-51be-4005-9fe0-f7b8b319720c\",\"alSetName\":\"Al Set 1\",\"alSetId\":\"212aca52-38c7-445d-98ac-daf07c6c9177\",\"sensorType\":\"Acceleration\",\"fmax\":0,\"resolution\":0,\"period\":0.0,\"sampleRate\":0.0,\"demod\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"minFrequency\":0.0,\"maxFrequency\":0.0,\"dwell\":0,\"lowFaultActive\":false,\"lowAlertActive\":false,\"highAlertActive\":false,\"highFaultActive\":false,\"lowFault\":0.0,\"lowAlert\":0.0,\"highAlert\":0.0,\"highFault\":0.0}]},{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"3ff9a86e-292e-4d64-85f3-271148276904\",\"assetId\":\"c9a6140b-6b35-4b9e-bc8e-6c1c29c6728e\",\"pointLocationId\":\"ac61dd8b-fdc4-4e8d-bf28-29e61a5e341d\",\"apSetId\":\"af0bbdf9-c9dd-4b96-afb9-8cd8007d6d99\",\"alSetId\":\"0009e57e-31b0-4473-8ec4-bfb358e96916\",\"pointName\":\"997 TEMP\",\"autoAcknowledge\":true,\"disabled\":false,\"pointType\":\"DC\",\"sampleInterval\":0,\"sensorChannelNum\":0,\"sensorOffset\":0.0,\"sensorSensitivity\":1000.0,\"sensorType\":\"Temperature\",\"sensorUnits\":\"F\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":0,\"fMax\":0,\"demodInterval\":0,\"alarmEnabled\":true,\"highAlert\":0.0,\"highAlertActive\":false,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":false,\"lowAlert\":0.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"period\":0.0,\"resolution\":0,\"sampleRate\":0.0,\"apAlSetVOs\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"apSetName\":\"AUTO_TEMP\",\"apSetId\":\"af0bbdf9-c9dd-4b96-afb9-8cd8007d6d99\",\"alSetName\":\"1743 ASSET 613_TEMP\",\"alSetId\":\"0009e57e-31b0-4473-8ec4-bfb358e96916\",\"sensorType\":\"Temperature\",\"fmax\":0,\"resolution\":0,\"period\":0.0,\"sampleRate\":0.0,\"demod\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"minFrequency\":0.0,\"maxFrequency\":0.0,\"dwell\":0,\"lowFaultActive\":false,\"lowAlertActive\":false,\"highAlertActive\":false,\"highFaultActive\":false,\"lowFault\":0.0,\"lowAlert\":0.0,\"highAlert\":0.0,\"highFault\":0.0}]},{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"3ff9a86e-292e-4d64-85f3-271148276904\",\"assetId\":\"c9a6140b-6b35-4b9e-bc8e-6c1c29c6728e\",\"pointLocationId\":\"ac61dd8b-fdc4-4e8d-bf28-29e61a5e341d\",\"apSetId\":\"2b2c8e66-d5d5-41b2-b601-2157e4623c4c\",\"alSetId\":\"001ce525-4d11-4e27-a493-e336086d6dd9\",\"pointName\":\"997 BATT\",\"autoAcknowledge\":false,\"disabled\":false,\"pointType\":\"DC\",\"sampleInterval\":0,\"sensorChannelNum\":1,\"sensorOffset\":0.0,\"sensorSensitivity\":1000.0,\"sensorType\":\"Voltage\",\"sensorUnits\":\"V\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":0,\"fMax\":0,\"demodInterval\":0,\"alarmEnabled\":true,\"highAlert\":0.0,\"highAlertActive\":false,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":false,\"lowAlert\":0.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"period\":0.0,\"resolution\":0,\"sampleRate\":0.0,\"apAlSetVOs\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"apSetName\":\"AUTO_BATTERY\",\"apSetId\":\"2b2c8e66-d5d5-41b2-b601-2157e4623c4c\",\"alSetName\":\"47 ASSET 30_VOLT\",\"alSetId\":\"001ce525-4d11-4e27-a493-e336086d6dd9\",\"sensorType\":\"Voltage\",\"fmax\":0,\"resolution\":0,\"period\":0.0,\"sampleRate\":0.0,\"demod\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"minFrequency\":0.0,\"maxFrequency\":0.0,\"dwell\":0,\"lowFaultActive\":false,\"lowAlertActive\":false,\"highAlertActive\":false,\"highFaultActive\":false,\"lowFault\":0.0,\"lowAlert\":0.0,\"highAlert\":0.0,\"highFault\":0.0}]}],\"newDeviceId\":\"BB003004\",\"siteName\":\"MEMPHIS\",\"areaName\":\"14\",\"assetName\":\"Asset 815\",\"pointLocationName\":\"997\",\"dbpointListExists\":false,\"basestationPortNum\":11}";
//            String installDeviceWithPoint = "{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"3ff9a86e-292e-4d64-85f3-271148276904\",\"assetId\":\"c9a6140b-6b35-4b9e-bc8e-6c1c29c6728e\",\"pointLocationId\":\"83638649-45d9-4b10-b7b4-4cdc7c2bcb83\",\"channelNum\":0,\"pointList\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"3ff9a86e-292e-4d64-85f3-271148276904\",\"assetId\":\"c9a6140b-6b35-4b9e-bc8e-6c1c29c6728e\",\"pointLocationId\":\"83638649-45d9-4b10-b7b4-4cdc7c2bcb83\",\"pointId\":\"10d69fc1-44df-4bb5-af97-ba267972ffb0\",\"apSetId\":\"af0bbdf9-c9dd-4b96-afb9-8cd8007d6d99\",\"alSetId\":\"0009e57e-31b0-4473-8ec4-bfb358e96916\",\"pointName\":\"61 TEMP\",\"autoAcknowledge\":true,\"disabled\":false,\"pointType\":\"DC\",\"sampleInterval\":720,\"sensorChannelNum\":0,\"sensorOffset\":0.0,\"sensorSensitivity\":1000.0,\"sensorType\":\"Temperature\",\"sensorUnits\":\"F\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":0,\"fMax\":0,\"apSetName\":\"AUTO_TEMP\",\"alSetName\":\"1743 ASSET 613_TEMP\",\"demodInterval\":0,\"alarmEnabled\":true,\"highAlert\":0.0,\"highAlertActive\":false,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":false,\"lowAlert\":0.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"period\":0.0,\"resolution\":0,\"sampleRate\":0.0},{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"3ff9a86e-292e-4d64-85f3-271148276904\",\"assetId\":\"c9a6140b-6b35-4b9e-bc8e-6c1c29c6728e\",\"pointLocationId\":\"83638649-45d9-4b10-b7b4-4cdc7c2bcb83\",\"pointId\":\"755bdeca-dfeb-4df5-a96e-6c29207833b5\",\"apSetId\":\"2b2c8e66-d5d5-41b2-b601-2157e4623c4c\",\"alSetId\":\"001ce525-4d11-4e27-a493-e336086d6dd9\",\"pointName\":\"61 BATT\",\"autoAcknowledge\":false,\"disabled\":false,\"pointType\":\"DC\",\"sampleInterval\":721,\"sensorChannelNum\":1,\"sensorOffset\":0.0,\"sensorSensitivity\":1000.0,\"sensorType\":\"Voltage\",\"sensorUnits\":\"V\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":0,\"fMax\":0,\"apSetName\":\"AUTO_BATTERY\",\"alSetName\":\"47 ASSET 30_VOLT\",\"demodInterval\":0,\"alarmEnabled\":true,\"highAlert\":0.0,\"highAlertActive\":false,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":false,\"lowAlert\":0.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"period\":0.0,\"resolution\":0,\"sampleRate\":0.0},{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"3ff9a86e-292e-4d64-85f3-271148276904\",\"assetId\":\"c9a6140b-6b35-4b9e-bc8e-6c1c29c6728e\",\"pointLocationId\":\"83638649-45d9-4b10-b7b4-4cdc7c2bcb83\",\"pointId\":\"9552e720-3c13-4c10-a310-b7a26e6ea75c\",\"apSetId\":\"8ed39428-92cd-4eb0-9e47-8dbc70aafbfd\",\"alSetId\":\"000a743d-6494-480a-b319-4de14eb1cc27\",\"pointName\":\"61 ULTRA\",\"autoAcknowledge\":true,\"disabled\":false,\"pointType\":\"AC\",\"sampleInterval\":722,\"sensorChannelNum\":0,\"sensorOffset\":0.0,\"sensorSensitivity\":50.0,\"sensorType\":\"Ultrasonic\",\"sensorUnits\":\"Pa\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":0,\"fMax\":0,\"apSetName\":\"AUTO_ULTRASONIC\",\"alSetName\":\"2560 ASSET 317_ULTRA\",\"demodInterval\":0,\"alarmEnabled\":false,\"highAlert\":0.0,\"highAlertActive\":false,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":false,\"lowAlert\":0.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"period\":0.0,\"resolution\":0,\"sampleRate\":0.0},{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"3ff9a86e-292e-4d64-85f3-271148276904\",\"assetId\":\"c9a6140b-6b35-4b9e-bc8e-6c1c29c6728e\",\"pointLocationId\":\"83638649-45d9-4b10-b7b4-4cdc7c2bcb83\",\"pointId\":\"b2d05d08-20ec-460b-9a0a-438de089898f\",\"apSetId\":\"110a3388-51be-4005-9fe0-f7b8b319720c\",\"alSetId\":\"212aca52-38c7-445d-98ac-daf07c6c9177\",\"pointName\":\"61 VIBE\",\"autoAcknowledge\":false,\"disabled\":false,\"pointType\":\"AC\",\"sampleInterval\":723,\"sensorChannelNum\":1,\"sensorOffset\":0.0,\"sensorSensitivity\":50.0,\"sensorType\":\"Acceleration\",\"sensorUnits\":\"Gs\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":0,\"fMax\":0,\"apSetName\":\"*****Global Ap Set 2\",\"alSetName\":\"Al Set 1\",\"demodInterval\":0,\"alarmEnabled\":true,\"highAlert\":0.0,\"highAlertActive\":false,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":false,\"lowAlert\":0.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"period\":0.0,\"resolution\":0,\"sampleRate\":0.0}],\"newDeviceId\":\"BB777777\",\"siteName\":\"MEMPHIS\",\"areaName\":\"14\",\"assetName\":\"Asset 815\",\"pointLocationName\":\"61\",\"dbpointListExists\":true,\"basestationPortNum\":20}";
            String replaceDeviceJson = "{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"e469f742-6f11-46dc-ac1c-61ef5ec36182\",\"assetId\":\"d3882ea9-da3a-4423-9f43-052bb6c1ca50\",\"pointLocationId\":\"d65645e4-da34-482b-83da-a4fadde32be9\",\"channelNum\":0,\"pointList\":"
                    + "[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"e469f742-6f11-46dc-ac1c-61ef5ec36182\",\"assetId\":\"d3882ea9-da3a-4423-9f43-052bb6c1ca50\",\"pointLocationId\":\"d65645e4-da34-482b-83da-a4fadde32be9\",\"pointId\":\"77c209ec-d83f-4147-9b30-2fe94ffdb01b\",\"apSetId\":\"11505795-91bd-4b16-b75b-5c141fb12f8d\",\"alSetId\":\"bfcc5fb5-f2a0-4bb7-bb99-b707f29e7374\",\"pointName\":\"998 TEMP\",\"autoAcknowledge\":false,\"disabled\":false,\"pointType\":\"DC\",\"sampleInterval\":720,\"sensorChannelNum\":0,\"sensorOffset\":0.0,\"sensorSensitivity\":1000.0,\"sensorType\":\"Temperature\",\"sensorUnits\":\"F\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":0,\"fMax\":0,\"apSetName\":\"*****Global Ap Set 4 - DC\",\"alSetName\":\"Al Set 1\",\"demodInterval\":0,\"alarmEnabled\":false,\"highAlert\":0.0,\"highAlertActive\":false,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":false,\"lowAlert\":0.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"period\":0.0,\"resolution\":0,\"sampleRate\":0.0},"
                    + "{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"e469f742-6f11-46dc-ac1c-61ef5ec36182\",\"assetId\":\"d3882ea9-da3a-4423-9f43-052bb6c1ca50\",\"pointLocationId\":\"d65645e4-da34-482b-83da-a4fadde32be9\",\"pointId\":\"679bb2f9-c32d-4845-bd6c-143423dfacdf\",\"apSetId\":\"8ed39428-92cd-4eb0-9e47-8dbc70aafbfd\",\"alSetId\":\"000a743d-6494-480a-b319-4de14eb1cc27\",\"pointName\":\"998 ULTRA\",\"autoAcknowledge\":false,\"disabled\":false,\"pointType\":\"AC\",\"sampleInterval\":720,\"sensorChannelNum\":0,\"sensorOffset\":0.0,\"sensorSensitivity\":50.0,\"sensorType\":\"Ultrasonic\",\"sensorUnits\":\"Pa\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":0,\"fMax\":0,\"apSetName\":\"AUTO_ULTRASONIC\",\"alSetName\":\"2560 ASSET 317_ULTRA\",\"demodInterval\":0,\"alarmEnabled\":false,\"highAlert\":0.0,\"highAlertActive\":false,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":false,\"lowAlert\":0.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"period\":0.0,\"resolution\":0,\"sampleRate\":0.0},"
                    + "{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"e469f742-6f11-46dc-ac1c-61ef5ec36182\",\"assetId\":\"d3882ea9-da3a-4423-9f43-052bb6c1ca50\",\"pointLocationId\":\"d65645e4-da34-482b-83da-a4fadde32be9\",\"pointId\":\"54dbfbaf-f43d-49b0-bbe9-95abd184151c\",\"apSetId\":\"2e784174-ab82-4376-a95c-a175d7916a95\",\"alSetId\":\"da1d2d87-21c5-4336-bc0a-504e1313a13a\",\"pointName\":\"998 VIBE\",\"autoAcknowledge\":false,\"disabled\":false,\"pointType\":\"AC\",\"sampleInterval\":720,\"sensorChannelNum\":1,\"sensorOffset\":0.0,\"sensorSensitivity\":50.0,\"sensorType\":\"Acceleration\",\"sensorUnits\":\"Gs\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":0,\"fMax\":0,\"apSetName\":\"Global Accel AP Set\",\"alSetName\":\"AL Set 1\",\"demodInterval\":0,\"alarmEnabled\":false,\"highAlert\":0.0,\"highAlertActive\":false,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":false,\"lowAlert\":0.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"period\":0.0,\"resolution\":0,\"sampleRate\":0.0},"
                    + "{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"e469f742-6f11-46dc-ac1c-61ef5ec36182\",\"assetId\":\"d3882ea9-da3a-4423-9f43-052bb6c1ca50\",\"pointLocationId\":\"d65645e4-da34-482b-83da-a4fadde32be9\",\"pointId\":\"4b24296c-43f0-49a9-b23d-c36c8684d0b5\",\"apSetId\":\"7df5e892-e20a-4972-87ac-6382dbc0a14f\",\"alSetId\":\"80de5583-482b-48a2-ae8d-c97b80d84927\",\"pointName\":\"998 BATT\",\"autoAcknowledge\":false,\"disabled\":false,\"pointType\":\"DC\",\"sampleInterval\":720,\"sensorChannelNum\":1,\"sensorOffset\":0.0,\"sensorSensitivity\":1000.0,\"sensorType\":\"Voltage\",\"sensorUnits\":\"V\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":0,\"fMax\":0,\"apSetName\":\"Voltage 02-22-23\",\"alSetName\":\"Voltage AL Set\",\"demodInterval\":0,\"alarmEnabled\":false,\"highAlert\":0.0,\"highAlertActive\":false,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":false,\"lowAlert\":0.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"period\":0.0,\"resolution\":0,\"sampleRate\":0.0}],\"existingDeviceId\":\"BB003004\",\"newDeviceId\":\"BB009004\",\"siteName\":\"MEMPHIS\",\"areaName\":\"14\",\"assetName\":\"Asset 815\",\"pointLocationName\":\"998\",\"dbpointListExists\":false,\"basestationPortNum\":0}";
            //System.out.println("installDeviceJson-->" + installDeviceJson);
            String msg = controller.replaceDevice(JsonUtil.manageDeviceParser(replaceDeviceJson));
            System.out.println("msg-->"+msg);
//            DeviceVO enableDeviceVO = new DeviceVO();
//            enableDeviceVO.setExistingDeviceId("00181711");
//            String msg = controller.enableDevice(enableDeviceVO);
//            System.out.println("enableDevice-->" + msg);
//            
//            DeviceVO disableDeviceVO = new DeviceVO();
//            disableDeviceVO.setExistingDeviceId("00181711");
//            msg = controller.disableDevice(disableDeviceVO);
//            System.out.println("enableDevice-->" + msg);
            
            
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            System.out.println("Exception occured in main : " + e.getMessage());
        } finally {
            System.exit(0);
        }
    }

}
