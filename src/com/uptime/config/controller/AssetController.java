/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.uptime.cassandra.config.entity.AssetSiteArea;
import com.uptime.config.delegate.asset.CreateAssetDelegate;
import com.uptime.config.delegate.asset.DeleteAssetDelegate;
import com.uptime.config.delegate.asset.ReadAssetDelegate;
import com.uptime.config.delegate.asset.UpdateAssetDelegate;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.AssetVO;
import com.uptime.services.util.JsonConverterUtil;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class AssetController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AssetController.class.getName());
    ReadAssetDelegate readDelegate;
    
    public AssetController() {
        readDelegate = new ReadAssetDelegate();
    }

    /**
     * Return a List of AssetSiteArea Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAssetSiteAreaByCustomerSite(String customer, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AssetSiteArea> result;
        StringBuilder json;

        if ((result = readDelegate.getAssetSiteAreaByCustomerSite(customer.trim(), UUID.fromString(siteId.trim()))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                .append("{\"customer\":\"").append(customer).append("\",")
                .append("\"siteId\":\"").append(siteId).append("\",")
                .append("\"AssetSiteAreas\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of AssetSiteArea Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAssetSiteAreaByCustomerSiteArea(String customer, String siteId, String areaId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AssetSiteArea> result;
        StringBuilder json;

        if ((result = readDelegate.getAssetSiteAreaByCustomerSiteArea(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                .append("{\"customer\":\"").append(customer).append("\",")
                .append("\"siteId\":\"").append(siteId).append("\",")
                .append("\"areaId\":\"").append(areaId).append("\",")
                .append("\"AssetSiteAreas\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of AssetSiteArea Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAssetSiteAreaByPK(String customer, String siteId, String areaId, String assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AssetSiteArea> result;
        StringBuilder json;

        if ((result = readDelegate.getAssetSiteAreaByPK(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()), UUID.fromString(assetId.trim()))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                .append("{\"customer\":\"").append(customer).append("\",")
                .append("\"siteId\":\"").append(siteId).append("\",")
                .append("\"areaId\":\"").append(areaId).append("\",")
                .append("\"assetId\":\"").append(assetId).append("\",")
                .append("\"AssetSiteAreas\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new Asset by inserting into multiple tables
     *
     * @param content, String Object
     * @return String Object
     */
    public String createAsset(String content) {
        CreateAssetDelegate createDelegate;
        AssetVO assetVO;
        List<AssetVO> assetVOList;
        JsonElement jsonElement;
        
        if (content != null && (jsonElement = JsonParser.parseString(content)).isJsonObject()) {
            assetVO = JsonUtil.parser(content);
            if (assetVO != null
                    && assetVO.getCustomerAccount() != null && !assetVO.getCustomerAccount().isEmpty()
                    && assetVO.getAssetType() != null && !assetVO.getAssetType().isEmpty()
                    && assetVO.getAssetName() != null && !assetVO.getAssetName().isEmpty()
                    && assetVO.getSiteId() != null
                    && assetVO.getAreaId() != null) {
                // Insert into Cassandra
                try {
                    createDelegate = new CreateAssetDelegate();
                    return createDelegate.createAsset(assetVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to create new Asset.\"}";
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to create new Asset.\"}";
                }
            } else {
                return "{\"outcome\":\"Json is invalid\"}";
            }
        } else if (content != null && (jsonElement = JsonParser.parseString(content)).isJsonArray() && (assetVOList = JsonUtil.parserList(content)) != null) {
            // Insert into Cassandra
            try {
                createDelegate = new CreateAssetDelegate();
                return createDelegate.createAssets(assetVOList);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                return "{\"outcome\":\"Error: Failed to create new Assets.\"}";
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                return "{\"outcome\":\"Error: Failed to create new Assets.\"}";
            }
        } else {
            return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        }
    }

    /**
     * Update tables dealing with the given Asset
     *
     * @param content, String Object
     * @return String Object
     * @throws java.lang.Exception
     */
    public String updateAsset(String content)throws IllegalArgumentException, Exception {
        UpdateAssetDelegate updateDelegate;
        AssetVO assetVO;

        if (content != null && (assetVO = JsonUtil.parser(content)) != null) {
            if (assetVO != null
                    && assetVO.getCustomerAccount() != null && !assetVO.getCustomerAccount().isEmpty()
                    && assetVO.getAssetId() != null
                    && assetVO.getAssetName() != null && !assetVO.getAssetName().isEmpty()
                    && assetVO.getSiteId() != null
                    && assetVO.getAreaId() != null) {
                if (assetVO.getSensorList() != null && !assetVO.getSensorList().isEmpty()) {
                    try {
                            updateDelegate = new UpdateAssetDelegate();
                            return updateDelegate.updatePointsAlarmEnabledForAsset(assetVO);
                    } catch (IllegalArgumentException e) {
                        return "{\"outcome\":\"Error: Failed to update Asset points alarm enabled.\"}";
                    }
                } 
                else {  
                    // Update in Cassandra
                    try {
                        updateDelegate = new UpdateAssetDelegate();
                        return updateDelegate.updateAsset(assetVO);
                    } catch (IllegalArgumentException e) {
                        return "{\"outcome\":\"Error: Failed to update Asset items.\"}";
                    }
                }
            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete tables dealing with the given Asset
     *
     * @param content, String Object
     * @return String Object
     */
    public String deleteAsset(String content) throws IllegalArgumentException {
        DeleteAssetDelegate deleteDelegate;
        AssetVO assetVO;

        if ((assetVO = JsonUtil.parser(content)) != null) {
            if (assetVO != null
                    && assetVO.getCustomerAccount() != null && !assetVO.getCustomerAccount().isEmpty()
                    && assetVO.getAssetId() != null
                    && assetVO.getSiteId() != null
                    && assetVO.getAreaId() != null) {

                // Delete from Cassandra
                try {
                    deleteDelegate = new DeleteAssetDelegate();
                    return deleteDelegate.deleteAsset(assetVO);
                } catch (IllegalArgumentException e) {
                    return "{\"outcome\":\"Error: Failed to delete Asset items.\"}";
                }

            } else {
                return "{\"outcome\":\"Insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Validate Al_set_id for the given asset
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @return String Object
     */
    public String validateAlSetforPoints(String customer, String siteId, String areaId, String assetId) {
        return readDelegate.validateAlSetforPoints(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()), UUID.fromString(assetId.trim()));
    }
}
