/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.entity.Subscriptions;
import com.uptime.config.delegate.subscriptions.CreateSubscriptionDelegate;
import com.uptime.config.delegate.subscriptions.DeleteSubscriptionDelegate;
import com.uptime.config.delegate.subscriptions.ReadSubscriptionsDelegate;
import com.uptime.config.delegate.subscriptions.UpdateSubscriptionDelegate;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.SubscriptionsVO;
import com.uptime.services.util.JsonConverterUtil;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class SubscriptionsController {
    ReadSubscriptionsDelegate readDelegate;
    private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionsController.class.getName());

    public SubscriptionsController() {
        readDelegate = new ReadSubscriptionsDelegate();
    }
    
    /**
     * Return a List of Subscriptions Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getSubscriptionsByCustomerSite(String customer, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<Subscriptions> result;
        StringBuilder json;

        if ((result = readDelegate.getSubscriptionsByCustomerSite(customer.trim(), UUID.fromString(siteId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"Subscriptions\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Error: Subscriptions not found in database.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of Subscriptions Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param subscriptionType, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getSubscriptionsByPK(String customer, String siteId, String subscriptionType) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<Subscriptions> result;
        StringBuilder json;

        if ((result = readDelegate.getSubscriptionsByPK(customer.trim(), UUID.fromString(siteId), subscriptionType)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"subscriptionType\":\"").append(subscriptionType).append("\",")
                    .append("\"Subscriptions\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Error: Subscriptions not found in database.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new Subscription by inserting into subscriptions table
     *
     * @param content, String Object
     * @return String Object
     */
    public String createSubscriptions(String content) {
        CreateSubscriptionDelegate createDelegate;
        SubscriptionsVO subscriptionsVO;

        if ((subscriptionsVO = JsonUtil.subscriptionsParser(content)) != null) {
            if (subscriptionsVO != null
                    && subscriptionsVO.getCustomerAccount() != null && !subscriptionsVO.getCustomerAccount().isEmpty()
                    && subscriptionsVO.getSiteId() != null
                    && subscriptionsVO.getSubscriptionType() != null && !subscriptionsVO.getSubscriptionType().isEmpty()) {
                
                // Insert into Cassandra
                try {
                    createDelegate = new CreateSubscriptionDelegate();
                    return createDelegate.createSubscriptions(subscriptionsVO);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to create new subscriptions.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update tables dealing with the given Subscriptions
     *
     * @param content, String Object
     * @return String Object
     */
    public String updateSubscriptions(String content) {
        UpdateSubscriptionDelegate updateDelegate;
        SubscriptionsVO subscriptionsVO;

        if ((subscriptionsVO = JsonUtil.subscriptionsParser(content)) != null) {
            if (subscriptionsVO != null
                    && subscriptionsVO.getCustomerAccount() != null && !subscriptionsVO.getCustomerAccount().isEmpty()
                    && subscriptionsVO.getSiteId() != null
                    && subscriptionsVO.getSubscriptionType() != null && !subscriptionsVO.getSubscriptionType().isEmpty()) {

                // Update in Cassandra
                try {
                    updateDelegate = new UpdateSubscriptionDelegate();
                    return updateDelegate.updateSite(subscriptionsVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to update subscriptions.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete tables dealing with the given Subscriptions
     *
     * @param content, String Object
     * @return String Object
     */
    public String deleteSubscriptions(String content) throws IllegalArgumentException {
        DeleteSubscriptionDelegate deleteDelegate;
        SubscriptionsVO subscriptionsVO;

        if ((subscriptionsVO = JsonUtil.subscriptionsParser(content)) != null) {
            if (subscriptionsVO != null
                    && subscriptionsVO.getCustomerAccount() != null && !subscriptionsVO.getCustomerAccount().isEmpty()
                    && subscriptionsVO.getSiteId() != null
                    && subscriptionsVO.getRequestType() != null && !subscriptionsVO.getRequestType().isEmpty()) {
                // Delete from Cassandra
                try {
                    deleteDelegate = new DeleteSubscriptionDelegate();
                    if (subscriptionsVO.getRequestType().equalsIgnoreCase("Delete")
                            && subscriptionsVO.getSubscriptionType() != null && !subscriptionsVO.getSubscriptionType().isEmpty())
                        return deleteDelegate.deleteSubscription(subscriptionsVO);
                    else if (subscriptionsVO.getRequestType().equalsIgnoreCase("Cancel"))
                        return deleteDelegate.cancelAllSubscription(subscriptionsVO);
                    else 
                        return "{\"outcome\":\"Insufficient data given in json\"}";
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    throw e;
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to delete subscriptions.\"}";
                }
                
            } else {
                return "{\"outcome\":\"Insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
