/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.cassandra.config.entity.Points;
import com.uptime.config.delegate.points.CreatePointsDelegate;
import com.uptime.config.delegate.points.DeletePointsDelegate;
import com.uptime.config.delegate.points.ReadPointsDelegate;
import com.uptime.config.delegate.points.UpdatePointsDelegate;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.PointVO;
import com.uptime.services.util.JsonConverterUtil;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class PointsController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PointsController.class.getName());
    ReadPointsDelegate readDelegate;

    public PointsController() {
        readDelegate = new ReadPointsDelegate();
    }

    /**
     * Return a List of Points Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getPointsByCustomerSiteAreaAsset(String customer, String siteId, String areaId, String assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<Points> result;
        StringBuilder json;

        if ((result = readDelegate.getPointsByCustomerSiteAreaAsset(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()), UUID.fromString(assetId.trim()))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"Points\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of Points Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getPointsByCustomerSiteAreaAssetLocation(String customer, String siteId, String areaId, String assetId, String pointLocationId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<Points> result;
        StringBuilder json;

        if ((result = readDelegate.getPointsByCustomerSiteAreaAssetLocation(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()), UUID.fromString(assetId.trim()), UUID.fromString(pointLocationId.trim()))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"Points\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of Points Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getPointsByCustomerSiteAreaAssetLocationPoint(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<Points> result;
        StringBuilder json;

        if ((result = readDelegate.getPointsByCustomerSiteAreaAssetLocationPoint(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()), UUID.fromString(assetId.trim()), UUID.fromString(pointLocationId.trim()), UUID.fromString(pointId.trim()))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"Points\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a list of Points Objects with respective list of ApAlByPoint Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getPointsWithApAlPointByCustomerSiteAreaAssetLocationPoint(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<Points> result;
        StringBuilder json;

        if ((result = readDelegate.getPointsByCustomerSiteAreaAssetLocationPoint(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()), UUID.fromString(assetId.trim()), UUID.fromString(pointLocationId.trim()), UUID.fromString(pointId.trim()))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                .append("{\"customer\":\"").append(customer).append("\",")
                .append("\"siteId\":\"").append(siteId).append("\",")
                .append("\"areaId\":\"").append(areaId).append("\",")
                .append("\"assetId\":\"").append(assetId).append("\",")
                .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                .append("\"pointId\":\"").append(pointId).append("\",")
                .append("\"Points\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i)));
                List<ApAlByPoint> apAlResult;
                StringBuilder apAljson;
                if ((apAlResult = readDelegate.getByCustomerSiteAreaAssetLocationPointApSet(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()), UUID.fromString(assetId.trim()), UUID.fromString(pointLocationId.trim()), UUID.fromString(pointId.trim()), result.get(i).getApSetId())) != null) {
                    if (!apAlResult.isEmpty()) {
                        json.replace(json.length() - 1, json.length(), "");
                        json.append(",")
                            .append("\"ApAlByPoints\":[");
                        apAljson = new StringBuilder();
                        for (int j = 0; j < apAlResult.size(); j++) {
                            apAljson.append(JsonConverterUtil.toJSON(apAlResult.get(j))).append(j < apAlResult.size() - 1 ? "," : "");
                        }
                        json.append(apAljson).append("]}");
                    }
                }
                json.append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of Points Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getPointsByPK(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<Points> result;
        StringBuilder json;

        if ((result = readDelegate.getPointsByPK(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()), UUID.fromString(assetId.trim()), UUID.fromString(pointLocationId.trim()), UUID.fromString(pointId.trim()), UUID.fromString(apSetId.trim()))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"apSetId\":\"").append(apSetId).append("\",")
                    .append("\"Points\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of ApAlByPoint Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getApAlByPointByCustomerSiteAreaAssetLocationPointApSet(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<ApAlByPoint> result;
        StringBuilder json;

        if ((result = readDelegate.getByCustomerSiteAreaAssetLocationPointApSet(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()), UUID.fromString(assetId.trim()), UUID.fromString(pointLocationId.trim()), UUID.fromString(pointId.trim()), UUID.fromString(apSetId.trim()))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"apSetId\":\"").append(apSetId).append("\",")
                    .append("\"ApAlByPoints\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of ApAlByPoint Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetId, String Object
     * @param alSetId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetId, String alSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<ApAlByPoint> result;
        StringBuilder json;

        if ((result = readDelegate.getByCustomerSiteAreaAssetLocationPointApSetAlSet(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()), UUID.fromString(assetId.trim()), UUID.fromString(pointLocationId.trim()), UUID.fromString(pointId.trim()), UUID.fromString(apSetId.trim()), UUID.fromString(alSetId.trim()))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"apSetId\":\"").append(apSetId).append("\",")
                    .append("\"alSetId\":\"").append(alSetId).append("\",")
                    .append("\"ApAlByPoints\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of ApAlByPoint Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetId, String Object
     * @param alSetId, String Object
     * @param paramName, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getApAlByPointByPK(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetId, String alSetId, String paramName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<ApAlByPoint> result;
        StringBuilder json;

        if ((result = readDelegate.getApAlByPointByPK(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()), UUID.fromString(assetId.trim()), UUID.fromString(pointLocationId.trim()), UUID.fromString(pointId.trim()), UUID.fromString(apSetId.trim()), UUID.fromString(alSetId.trim()), paramName)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"apSetId\":\"").append(apSetId).append("\",")
                    .append("\"alSetId\":\"").append(alSetId).append("\",")
                    .append("\"paramName\":\"").append(paramName).append("\",")
                    .append("\"ApAlByPoints\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new point by inserting into points table
     *
     * @param content, String Object
     * @return String Object
     */
    public String createPoints(String content) {
        CreatePointsDelegate createDelegate;
        List<PointVO> pointVOList;
        boolean foundActiveAPSet = false;

        UUID pointId = UUID.randomUUID();
        String currentPoint;
        if ((pointVOList = JsonUtil.pointsParser(content)) != null && !pointVOList.isEmpty()) {
            
            for (PointVO pointVO : pointVOList) {   
                if (pointVO.getCustomerAccount() == null || pointVO.getCustomerAccount().isEmpty()
                || pointVO.getSiteId() == null
                || pointVO.getAreaId() == null
                || pointVO.getAssetId() == null
                || pointVO.getPointLocationId() == null
                || pointVO.getPointName() == null || pointVO.getPointName().isEmpty()
                || pointVO.getApSetId() == null
                || pointVO.getAlSetId() == null) {
                    return "{\"outcome\":\"insufficient data given in json\"}";
                }
            }
            
            pointVOList.sort(Comparator.comparing(PointVO::getPointName));
            currentPoint = pointVOList.get(0).getPointName();
            for (PointVO pointVO : pointVOList) {      
                if (currentPoint.equalsIgnoreCase(pointVO.getPointName()) 
                        && !pointVO.isDisabled()) {
                    if (!foundActiveAPSet)
                       foundActiveAPSet = true;
                    else
                        return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
                    //Set Same pointId for all points with different AP Set IDs
                    if (pointVO.getPointId() == null)
                        pointVO.setPointId(pointId);
                }
                else { //different point name
                    currentPoint = pointVO.getPointName();
                    pointId = UUID.randomUUID();
                    foundActiveAPSet = false;
                    if (pointVO.getPointId() == null)
                        pointVO.setPointId(pointId);
                    
                    if (!pointVO.isDisabled()) {
                        foundActiveAPSet = true;
                    }
                }
            }
            
            // Insert into Cassandra
            try {
                createDelegate = new CreatePointsDelegate();
                return createDelegate.createPoints(pointVOList);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                return "{\"outcome\":\"Error: Failed to create new Points.\"}";
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                return "{\"outcome\":\"Error: Failed to create new Points.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update tables dealing with the given Point
     *
     * @param content, String Object
     * @return String Object
     */
    public String updatePoints(String content) {
        UpdatePointsDelegate updateDelegate;

        List<PointVO> pointVOList;
        boolean foundActiveAPSet = false;

        if ((pointVOList = JsonUtil.pointsParser(content)) != null && !pointVOList.isEmpty()) {
            for (PointVO pointVO : pointVOList) {   
                if (pointVO.getCustomerAccount() == null || pointVO.getCustomerAccount().isEmpty()
                || pointVO.getSiteId() == null
                || pointVO.getAreaId() == null
                || pointVO.getAssetId() == null
                || pointVO.getPointLocationId() == null
                || (pointVO.getPointId() != null && (pointVO.getPointName() == null || pointVO.getPointName().isEmpty()))
                || ((pointVO.getPointName() != null && !pointVO.getPointName().isEmpty()) && pointVO.getPointId() == null)
                || pointVO.getApSetId() == null
                || pointVO.getAlSetId() == null) {
                    return "{\"outcome\":\"insufficient data given in json\"}";
                }
                if (!pointVO.isDisabled()) {
                    if (!foundActiveAPSet)
                       foundActiveAPSet = true;
                    else
                        return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
                }
            }
            // Update in Cassandra
            try {
                updateDelegate = new UpdatePointsDelegate();
                return updateDelegate.updatePoints(pointVOList);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                return "{\"outcome\":\"Error: Failed to update Point items.\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete tables dealing with the given Point
     *
     * @param content, String Object
     * @return String Object
     */
    public String deletePoints(String content) {
        DeletePointsDelegate deleteDelegate;
        List<PointVO> pointVOList;
        boolean foundActiveAPSet = false;

        if ((pointVOList = JsonUtil.pointsParser(content)) != null && !pointVOList.isEmpty()) {
            for (PointVO pointVO : pointVOList) {   
                if (pointVO.getCustomerAccount() == null || pointVO.getCustomerAccount().isEmpty()
                || pointVO.getSiteId() == null
                || pointVO.getAreaId() == null
                || pointVO.getAssetId() == null
                || pointVO.getPointLocationId() == null
                || (pointVO.getPointId() != null && (pointVO.getPointName() == null || pointVO.getPointName().isEmpty()))
                || ((pointVO.getPointName() != null && !pointVO.getPointName().isEmpty()) && pointVO.getPointId() == null)
                || pointVO.getApSetId() == null
                || pointVO.getAlSetId() == null) {
                    return "{\"outcome\":\"insufficient data given in json\"}";
                }
                if (!pointVO.isDisabled()) {
                    if (!foundActiveAPSet)
                       foundActiveAPSet = true;
                    else
                        return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
                }
            }
            // Delete from Cassandra
            try {
                deleteDelegate = new DeletePointsDelegate();
                return deleteDelegate.deletePoints(pointVOList);
            } catch (IllegalArgumentException e) {
                LOGGER.error(e.getMessage(), e);
                return "{\"outcome\":\"Error: Failed to delete Point items.\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

}
