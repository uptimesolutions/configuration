/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.cassandra.config.entity.AreaSite;
import com.uptime.cassandra.config.entity.AssetSiteArea;
import com.uptime.cassandra.config.entity.HwUnitToPoint;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.cassandra.config.entity.Points;
import com.uptime.cassandra.config.entity.SiteCustomer;
import com.uptime.config.delegate.hwunit.CreateHwUnitDelegate;
import com.uptime.config.delegate.hwunit.ReadHwUnitDelegate;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.HwUnitVO;
import com.uptime.services.util.JsonConverterUtil;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author madhavi
 */
public class HwUnitController {
    ReadHwUnitDelegate readDelegate;
    
    public HwUnitController() {
        readDelegate = new ReadHwUnitDelegate();
    }

    /**
     * Return a List of HwUnitToPoint Objects by the given params
     *
     * @param deviceId, String Object
     *
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getHwUnitToPointByDevice(String deviceId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<HwUnitToPoint> result;
        StringBuilder json;

        if ((result = readDelegate.getHwUnitToPointByDevice(deviceId.trim())) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"deviceId\":\"").append(deviceId).append("\",")
                        .append("\"HwUnitToPoints\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of HwUnitToPoint Objects by the given params
     *
     * @param deviceId, String Object
     * @param channelType, String Object
     * @param channelNum, byte
     *
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getHwUnitToPointByPK(String deviceId, String channelType, byte channelNum) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<HwUnitToPoint> result;
        StringBuilder json;

        if ((result = readDelegate.getHwUnitToPointByPK(deviceId.trim(), channelType.trim(), channelNum)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"deviceId\":\"").append(deviceId).append("\",")
                        .append("\"channelType\":\"").append(channelType).append("\",")
                        .append("\"channelNum\":\"").append(channelNum).append("\",")
                        .append("\"HwUnitToPoints\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of HwUnitToPoint Objects by the given params
     *
     * @param serialNum, String Object
     * @param channelType, String Object
     * @param channelNum, byte
     *
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getLookupPointConfigByHardware(String serialNum, String channelType, byte channelNum) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<HwUnitToPoint> result;
        List<ApAlByPoint> apAlByPoints;
        List<SiteCustomer> siteList;
        List<AreaSite> areaList;
        List<AssetSiteArea> assetList;
        List<PointLocations> pointLocations;
        List<Points> pointsList;
        HwUnitToPoint hwUnitToPoint;
        Points points;
        UUID tachId = null;
        float speedRatio = 0f;
        float rollDiameter = 0f;
        String rollDiameterUnits = null;
        int baseStationPort = 0;
        String siteName = null;
        String areaName = null;
        String assetName = null;
        String pointLocationName = null;
        StringBuilder json;

        if ((result = readDelegate.getHwUnitToPointByPK(serialNum.trim(), channelType.trim(), channelNum)) != null) {
            if (!result.isEmpty()) {
                hwUnitToPoint = result.get(0);
                
                // Check the points table 
                if ((pointsList = ConfigMapperImpl.getInstance().pointsDAO().findByPK(hwUnitToPoint.getCustomerAcct(), hwUnitToPoint.getSiteId(), hwUnitToPoint.getAreaId(), hwUnitToPoint.getAssetId(), hwUnitToPoint.getPointLocationId(), hwUnitToPoint.getPointId(), hwUnitToPoint.getApSetId())) != null && !pointsList.isEmpty()) {
                    points = pointsList.get(0);
                    
                    // Check the ap_al_by_point table 
                    if ((apAlByPoints = ConfigMapperImpl.getInstance().apAlByPointDAO().findByCustomerSiteAreaAssetLocationPointApSet(hwUnitToPoint.getCustomerAcct(), hwUnitToPoint.getSiteId(), hwUnitToPoint.getAreaId(), hwUnitToPoint.getAssetId(), hwUnitToPoint.getPointLocationId(), hwUnitToPoint.getPointId(), hwUnitToPoint.getApSetId())) != null && !apAlByPoints.isEmpty()) {
                        
                        // Get tachId, speedRatio, roll_diameter, roll_diameter_units, baseStationPort, and pointLocationName from point_locations table
                        if ((pointLocations = ConfigMapperImpl.getInstance().pointLocationsDAO().findByPK(hwUnitToPoint.getCustomerAcct(), hwUnitToPoint.getSiteId(), hwUnitToPoint.getAreaId(), hwUnitToPoint.getAssetId(), hwUnitToPoint.getPointLocationId())) != null && !pointLocations.isEmpty()) {
                            tachId = pointLocations.get(0).getTachId();
                            speedRatio = pointLocations.get(0).getSpeedRatio();
                            rollDiameter = pointLocations.get(0).getRollDiameter();
                            rollDiameterUnits = pointLocations.get(0).getRollDiameterUnits();
                            baseStationPort = pointLocations.get(0).getBasestationPortNum();
                            pointLocationName = pointLocations.get(0).getPointLocationName();
                        }
                        
                        // Get siteName from site_customer table
                        if ((siteList = ConfigMapperImpl.getInstance().siteCustomerDAO().findByPK(hwUnitToPoint.getCustomerAcct(), hwUnitToPoint.getSiteId())) != null && !siteList.isEmpty()) {
                            siteName = siteList.get(0).getSiteName();
                        }
                        
                        // Get areaName from area_site table
                        if ((areaList = ConfigMapperImpl.getInstance().areaSiteDAO().findByPK(hwUnitToPoint.getCustomerAcct(), hwUnitToPoint.getSiteId(), hwUnitToPoint.getAreaId())) != null && !areaList.isEmpty()) {
                            areaName = areaList.get(0).getAreaName();
                        }
                        
                        // Get assetName from asset_site_area table
                        if ((assetList = ConfigMapperImpl.getInstance().assetSiteAreaDAO().findByPK(hwUnitToPoint.getCustomerAcct(), hwUnitToPoint.getSiteId(), hwUnitToPoint.getAreaId(), hwUnitToPoint.getAssetId())) != null && !assetList.isEmpty()) {
                            assetName = assetList.get(0).getAssetName();
                        }
                        
                        json = new StringBuilder();
                        json.append("{\"customerId").append("\":\"").append(points.getCustomerAccount()).append("\",");
                        json.append("\"deviceId").append("\":\"").append(serialNum).append("\",");
                        json.append("\"channelType").append("\":\"").append(channelType).append("\",");
                        json.append("\"channelNumber").append("\":").append(channelNum).append(",");
                        json.append("\"siteId").append("\":\"").append(points.getSiteId()).append("\",");
                        json.append("\"siteName").append("\":\"").append(siteName).append("\",");
                        json.append("\"areaId").append("\":\"").append(points.getAreaId()).append("\",");
                        json.append("\"areaName").append("\":\"").append(areaName).append("\",");
                        json.append("\"assetId").append("\":\"").append(points.getAssetId()).append("\",");
                        json.append("\"assetName").append("\":\"").append(assetName).append("\",");
                        json.append("\"pointLocationId").append("\":\"").append(points.getPointLocationId()).append("\",");
                        json.append("\"pointLocationName").append("\":\"").append(pointLocationName).append("\",");
                        json.append("\"speedRatio").append("\":").append(speedRatio).append(",");
                        json.append("\"rollDiameter").append("\":").append(rollDiameter).append(",");
                        json.append("\"rollDiameterUnits").append("\":\"").append(rollDiameterUnits).append("\",");
                        json.append("\"pointId").append("\":\"").append(points.getPointId()).append("\",");
                        json.append("\"pointName").append("\":\"").append(points.getPointName()).append("\",");
                        json.append("\"tachId").append("\":").append(tachId != null ? "\"" + tachId + "\"" : null).append(",");
                        json.append("\"baseStationPort").append("\":").append(baseStationPort).append(",");
                        json.append("\"sensorType").append("\":\"").append(points.getSensorType()).append("\",");
                        json.append("\"sensitivity").append("\":").append(points.getSensorSensitivity()).append(",");
                        json.append("\"sensitivityUnits").append("\":\"").append(points.getSensorUnits()).append("\",");
                        json.append("\"offset").append("\":").append(points.getSensorOffset()).append(",");
                        
                        // If the give channelType field is "DC"
                        if (channelType.equalsIgnoreCase("DC")) {
                            json.append("\"apSetId").append("\":\"").append(points.getApSetId()).append("\",");
                            json.append("\"alSetId").append("\":\"").append(points.getAlSetId()).append("\",");
                            json.append("\"dwell").append("\":").append(apAlByPoints.get(0).getDwell()).append(",");
                            json.append("\"lowFaultActive").append("\":").append(apAlByPoints.get(0).isLowFaultActive()).append(",");
                            json.append("\"lowAlertActive").append("\":").append(apAlByPoints.get(0).isLowAlertActive()).append(",");
                            json.append("\"highAlertActive").append("\":").append(apAlByPoints.get(0).isHighAlertActive()).append(",");
                            json.append("\"highFaultActive").append("\":").append(apAlByPoints.get(0).isHighFaultActive()).append(",");
                            json.append("\"lowFault").append("\":").append(apAlByPoints.get(0).getLowFault()).append(",");
                            json.append("\"lowAlert").append("\":").append(apAlByPoints.get(0).getLowAlert()).append(",");
                            json.append("\"highAlert").append("\":").append(apAlByPoints.get(0).getHighAlert()).append(",");
                            json.append("\"highFault").append("\":").append(apAlByPoints.get(0).getHighFault()).append(",");
                            json.append("\"alarmenabled").append("\":").append(points.isAlarmEnabled());
                        } 
                        
                        // If the give channelType field is "AC"
                        else if (channelType.equalsIgnoreCase("AC")) {
                            json.append("\"alarmenabled").append("\":").append(points.isAlarmEnabled()).append(",");
                            json.append("\"apSet\":[");
                            
                            boolean isFirst = true;
                            for (ApAlByPoint apAlByPoint1 : apAlByPoints) {
                                if(isFirst){
                                    json.append("{");
                                    json.append("\"apSetId").append("\":\"").append(apAlByPoint1.getApSetId()).append("\",");
                                    json.append("\"fmax").append("\":").append(apAlByPoint1.getfMax()).append(",");
                                    json.append("\"resolution").append("\":").append(apAlByPoint1.getResolution()).append(",");
                                    json.append("\"period").append("\":").append(apAlByPoint1.getPeriod()).append(",");
                                    json.append("\"sampleRate").append("\":").append(apAlByPoint1.getSampleRate()).append(",");
                                    json.append("\"demodEnabled").append("\":").append(apAlByPoint1.isDemodEnabled()).append(",");
                                    json.append("\"demodHighPass").append("\":").append(apAlByPoint1.getDemodHighPass()).append(",");
                                    json.append("\"demodLowPass").append("\":").append(apAlByPoint1.getDemodLowPass()).append(",");
                                    json.append("\"isRTD").append("\":null").append(",");
                                    json.append("\"params\":[");
                                    isFirst = false;
                                }
                                
                                json.append("{");
                                json.append("\"paramName").append("\":\"").append(apAlByPoint1.getParamName()).append("\",");
                                json.append("\"paramType").append("\":\"").append(apAlByPoint1.getParamType()).append("\",");
                                json.append("\"freqUnits").append("\":\"").append(apAlByPoint1.getFreqUnits()).append("\",");
                                json.append("\"paramUnits").append("\":\"").append(apAlByPoint1.getParamUnits()).append("\",");
                                json.append("\"paramAmpFactor").append("\":\"").append(apAlByPoint1.getParamAmpFactor()).append("\",");
                                json.append("\"minFreq").append("\":").append(apAlByPoint1.getMinFreq()).append(",");
                                json.append("\"maxFreq").append("\":").append(apAlByPoint1.getMaxFreq()).append(",");
                                json.append("\"alSet\":[{");
                                json.append("\"alSetId").append("\":\"").append(points.getAlSetId()).append("\",");
                                json.append("\"dwell").append("\":").append(apAlByPoint1.getDwell()).append(",");
                                json.append("\"lowFaultActive").append("\":").append(apAlByPoint1.isLowFaultActive()).append(",");
                                json.append("\"lowAlertActive").append("\":").append(apAlByPoint1.isLowAlertActive()).append(",");
                                json.append("\"highAlertActive").append("\":").append(apAlByPoint1.isHighAlertActive()).append(",");
                                json.append("\"highFaultActive").append("\":").append(apAlByPoint1.isHighFaultActive()).append(",");
                                json.append("\"lowFault").append("\":").append(apAlByPoint1.getLowFault()).append(",");
                                json.append("\"lowAlert").append("\":").append(apAlByPoint1.getLowAlert()).append(",");
                                json.append("\"highAlert").append("\":").append(apAlByPoint1.getHighAlert()).append(",");
                                json.append("\"highFault").append("\":").append(apAlByPoint1.getHighFault()).append("}]},");
                            }
                            json.deleteCharAt(json.length()-1);
                            json.append("] }");
                            json.append("]");
                        }
                        
                        json.append("}");

                        return json.toString();
                    } else {
                        return "{\"outcome\":\"Item/Items not found.\"}";
                    }
                } else {
                    return "{\"outcome\":\"Item/Items not found.\"}";
                }
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

/**
     * Create a new HwUnitToPoint by inserting into hwunit_to_point, points and ap_al_by_point tables
     *
     * @param content, String Object
     * @return String Object
     */
    public String createHwUnitToPoint(String content) {
        CreateHwUnitDelegate createDelegate;
        HwUnitVO hwUnitVO;
        
        if ((hwUnitVO = JsonUtil.hwUnitToPointParser(content)) != null) {
            if (hwUnitVO != null
                    && hwUnitVO.getDeviceId() != null && !hwUnitVO.getDeviceId().isEmpty()
                    && hwUnitVO.getChannelType() != null && !hwUnitVO.getChannelType().isEmpty()
                    && hwUnitVO.getCustomerAccount() != null && !hwUnitVO.getCustomerAccount().isEmpty()
                    && hwUnitVO.getSiteId() != null
                    && hwUnitVO.getAreaId() != null
                    && hwUnitVO.getAssetId() != null
                    && hwUnitVO.getPointLocationId() != null
                    && hwUnitVO.getPointName() != null && !hwUnitVO.getPointName().isEmpty()
                    && hwUnitVO.getApSetId() != null
                    && hwUnitVO.getAlSetId() != null
                    && hwUnitVO.getApAlSetVOs() != null && !hwUnitVO.getApAlSetVOs().isEmpty()) {

                // Insert into Cassandra
                try {
                    createDelegate = new CreateHwUnitDelegate();
                    return createDelegate.createHwUnitToPoint(hwUnitVO);
                } catch (IllegalArgumentException e) {
                    return "{\"outcome\":\"Error: Failed to create new HwUnitToPoint.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

}