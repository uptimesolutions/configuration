/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.entity.AreaSite;
import com.uptime.cassandra.config.entity.AreaConfigSchedule;
import com.uptime.config.delegate.area.CreateAreaDelegate;
import com.uptime.config.delegate.area.DeleteAreaDelegate;
import com.uptime.config.delegate.area.ReadAreaDelegate;
import com.uptime.config.delegate.area.UpdateAreaDelegate;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.AreaVO;
import com.uptime.config.vo.AreaConfigScheduleVO;
import com.uptime.services.util.JsonConverterUtil;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gsingh
 */
public class AreaController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AreaController.class.getName());
    ReadAreaDelegate readDelegate;
    
    public AreaController() {
        readDelegate = new ReadAreaDelegate();
    }

    /**
     * Return a json of a list of AreaSite Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAreaSiteByCustomerSite(String customer, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AreaSite> result;
        StringBuilder json;

        if ((result = readDelegate.getAreaSiteByCustomerSite(customer.trim(), UUID.fromString(siteId.trim()))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                .append("{\"customerAccount\":\"").append(customer).append("\",")
                .append("\"siteId\":\"").append(siteId).append("\",")
                .append("\"areaSites\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a list of AreaSite Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAreaSiteByPK(String customer, String siteId, String areaId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AreaSite> result;
        StringBuilder json;

        if ((result = readDelegate.getAreaSiteByPK(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                .append("{\"customerAccount\":\"").append(customer).append("\",")
                .append("\"siteId\":\"").append(siteId).append("\",")
                .append("\"areaId\":\"").append(areaId).append("\",")
                .append("\"areaSites\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of AreaConfigSchedule Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAreaConfigScheduleByCustomerSiteArea(String customer, String siteId, String areaId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AreaConfigSchedule> result;
        StringBuilder json;

        if ((result = readDelegate.getAreaConfigScheduleByCustomerSiteArea(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                .append("{\"customerAccount\":\"").append(customer).append("\",")
                .append("\"siteId\":\"").append(siteId).append("\",")
                .append("\"areaId\":\"").append(areaId).append("\",")
                .append("\"areaConfigSchedules\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of AreaSite & AreaConfigSchedule Objects by the given
     * params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAreaListByCustomerSiteArea(String customer, String siteId, String areaId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AreaSite> areaSiteResult;
        List<AreaConfigSchedule> areaConfigScheduleResult;
        StringBuilder json;
        
        if ((areaSiteResult = readDelegate.getAreaSiteByPK(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()))) != null && !areaSiteResult.isEmpty()) {
            json = new StringBuilder();
            json
                .append("{\"customerAccount\":\"").append(areaSiteResult.get(0).getCustomerAccount()).append("\",")
                .append("\"siteId\":\"").append(areaSiteResult.get(0).getSiteId()).append("\",")
                .append("\"areaId\":\"").append(areaSiteResult.get(0).getAreaId()).append("\",")
                .append("\"areaName\":\"").append(areaSiteResult.get(0).getAreaName()).append("\",")
                .append("\"description\":\"").append(areaSiteResult.get(0).getDescription()).append("\",")
                .append("\"environment\":\"").append(areaSiteResult.get(0).getEnvironment()).append("\",")
                .append("\"climateControlled\":").append(areaSiteResult.get(0).isClimateControlled()).append(",")
                .append("\"areaConfigSchedules\":[");
            if ((areaConfigScheduleResult = readDelegate.getAreaConfigScheduleByCustomerSiteArea(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()))) != null && !areaConfigScheduleResult.isEmpty()) {
                for (int i = 0; i < areaConfigScheduleResult.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(areaConfigScheduleResult.get(i))).append(i < areaConfigScheduleResult.size() - 1 ? "," : "");
                }
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of AreaConfigSchedule Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param scheduleId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAreaConfigScheduleByCustomerSiteAreaSchedule(String customer, String siteId, String areaId, String scheduleId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AreaConfigSchedule> result;
        StringBuilder json;

        if ((result = readDelegate.getAreaConfigScheduleByCustomerSiteAreaSchedule(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()), UUID.fromString(scheduleId.trim()))) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                .append("{\"customerAccount\":\"").append(customer).append("\",")
                .append("\"siteId\":\"").append(siteId).append("\",")
                .append("\"areaId\":\"").append(areaId).append("\",")
                .append("\"scheduleId\":\"").append(scheduleId).append("\",")
                .append("\"areaConfigSchedules\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of AreaConfigSchedule Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param scheduleId, String Object
     * @param dayOfWeek, byte Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAreaConfigScheduleByPK(String customer, String siteId, String areaId, String scheduleId, byte dayOfWeek) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AreaConfigSchedule> result;
        StringBuilder json;

        if ((result = readDelegate.getAreaConfigScheduleByPK(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()), UUID.fromString(scheduleId.trim()), dayOfWeek)) != null && !result.isEmpty()) {
            json = new StringBuilder();
            json
                .append("{\"customerAccount\":\"").append(customer).append("\",")
                .append("\"siteId\":\"").append(siteId).append("\",")
                .append("\"areaId\":\"").append(areaId).append("\",")
                .append("\"scheduleId\":\"").append(scheduleId).append("\",")
                .append("\"dayOfWeek\":\"").append(dayOfWeek).append("\",")
                .append("\"areaConfigSchedules\":[");
            for (int i = 0; i < result.size(); i++) {
                json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
            }
            json.append("],\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new Area by inserting into multiple tables
     *
     * @param content, String Object
     * @return String Object
     */
    public String createArea(String content) {
        CreateAreaDelegate createDelegate;
        AreaVO areaVO;
        
        if ((areaVO = JsonUtil.areaParser(content)) != null) {
            if (areaVO.getCustomerAccount() != null && !areaVO.getCustomerAccount().isEmpty()
                    && areaVO.getAreaName() != null && !areaVO.getAreaName().isEmpty()
                    && areaVO.getSiteId() != null) {

                // Insert into Cassandra
                try {
                    createDelegate = new CreateAreaDelegate();
                    return createDelegate.createAreaSite(areaVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to create new Area.\"}";
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to create new Area.\"}";
                }
                
            } else {
                return "{\"outcome\":\"Insufficient and/or invalid data given in json\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update tables dealing with the given Area
     *
     * @param content, String Object
     * @return String Object
     */
    public String updateArea(String content) {
        CreateAreaDelegate createAreaDelegate;
        DeleteAreaDelegate deleteAreaDelegate;
        UpdateAreaDelegate updateAreaDelegate;
        AreaVO areaVO;
        String resp;
        
        if ((areaVO = JsonUtil.areaParser(content)) != null) {
            if (areaVO.getCustomerAccount() != null && !areaVO.getCustomerAccount().isEmpty()
                    && areaVO.getSiteId() != null
                    && areaVO.getAreaId() != null) {
                
                // Update in Cassandra
                try {   
                    createAreaDelegate = new CreateAreaDelegate();
                    deleteAreaDelegate = new DeleteAreaDelegate();
                    updateAreaDelegate = new UpdateAreaDelegate();
                    
                    resp = updateAreaDelegate.updateAreaSite(areaVO);
                    for (AreaConfigScheduleVO acsVO : areaVO.getScheduleVOList()) {
                        if (acsVO.getScheduleId() != null && acsVO.getDayOfWeek() > 0 && acsVO.getType() != null && !acsVO.getType().equalsIgnoreCase("DB")) {
                            if (acsVO.getType().equalsIgnoreCase("New")) {
                                resp = createAreaDelegate.createAreaConfigSchedule(acsVO);
                            } else if (acsVO.getType().equalsIgnoreCase("delete")) {
                                resp = deleteAreaDelegate.deleteAreaConfigSchedule(acsVO);
                            } else {
                                resp = updateAreaDelegate.updateAreaConfigSchedule(acsVO);
                            }
                        }
                    }
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to update Area items.\"}";
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to update Area items.\"}";
                }
                
            } else {
                return "{\"outcome\":\"Insufficient and/or invalid data given in json\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
        return resp;
    }

    /**
     * Delete tables dealing with the given Area
     *
     * @param content, String Object
     * @return String Object
     */
    public String deleteArea(String content) {
        DeleteAreaDelegate deleteAreaDelegate;
        AreaVO areaVO;
        String resp;
        
        if ((areaVO = JsonUtil.areaParser(content)) != null) {
            if (areaVO.getCustomerAccount() != null && !areaVO.getCustomerAccount().isEmpty()
                    && areaVO.getSiteId() != null
                    && areaVO.getAreaId() != null) {
                
                // Delete from Cassandra
                try {   
                    deleteAreaDelegate = new DeleteAreaDelegate();
                    
                    resp = deleteAreaDelegate.deleteAreaSite(areaVO);
                    for (AreaConfigScheduleVO acsVO : areaVO.getScheduleVOList()) {
                        if (acsVO.getScheduleId() != null && acsVO.getDayOfWeek() > 0) {
                            resp = deleteAreaDelegate.deleteAreaConfigSchedule(acsVO);
                        }
                    }
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to delete Area items.\"}";
                }

            } else {
                return "{\"outcome\":\"Insufficient and/or invalid data given in json\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
        return resp;
    }

}
