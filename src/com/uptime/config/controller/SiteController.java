/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.entity.SiteContacts;
import com.uptime.cassandra.config.entity.SiteCustomer;
import com.uptime.cassandra.config.entity.SiteRegionCountry;
import com.uptime.config.delegate.site.CreateSiteDelegate;
import com.uptime.config.delegate.site.DeleteSiteDelegate;
import com.uptime.config.delegate.site.ReadSiteDelegate;
import com.uptime.config.delegate.site.UpdateSiteDelegate;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.SiteVO;
import com.uptime.services.util.JsonConverterUtil;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class SiteController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SiteController.class.getName());
    ReadSiteDelegate readDelegate;

    public SiteController() {
        readDelegate = new ReadSiteDelegate();
    }

    /**
     * Return Lists of SiteContacts, SiteCustomer, SiteRegionCountry Objects by
     * the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param region, String Object
     * @param country, String Object
     * @param returnSiteContacts, boolean
     * @param returnSiteCustomer, boolean
     * @param returnSiteRegionCountry, boolean
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getAllByCustomerSiteRegionCountry(String customer, String region, String country, String siteId, boolean returnSiteContacts, boolean returnSiteCustomer, boolean returnSiteRegionCountry) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        StringBuilder json;
        List<SiteContacts> resultSiteContacts;
        List<SiteCustomer> resultSiteCustomer;
        List<SiteRegionCountry> resultSiteRegionCountry;
        boolean found;
        
        json = new StringBuilder();
        json
            .append("{\"customer\":\"").append(customer).append("\",")
            .append("\"siteId\":\"").append(siteId).append("\",")
            .append("\"region\":\"").append(region).append("\",")
            .append("\"country\":\"").append(country).append("\",");
        
        found = false;
        if (returnSiteContacts) {
            if ((resultSiteContacts = readDelegate.getSiteContactsByCustomerSite(customer.trim(), UUID.fromString(siteId))) != null) {
                if (!resultSiteContacts.isEmpty()) {
                    json.append("\"SiteContacts\":[");
                    for (int i = 0; i < resultSiteContacts.size(); i++) {
                        json.append(JsonConverterUtil.toJSON(resultSiteContacts.get(i))).append(i < resultSiteContacts.size() - 1 ? "," : "");
                    }
                    json.append("],");
                    found = true;
                }
            }
        }
        
        if (returnSiteCustomer) {
            if ((resultSiteCustomer = readDelegate.getSiteCustomerByCustomerSite(customer.trim(), UUID.fromString(siteId))) != null) {
                if (!resultSiteCustomer.isEmpty()) {
                    json.append("\"SiteCustomers\":[");
                    for (int i = 0; i < resultSiteCustomer.size(); i++) {
                        json.append(JsonConverterUtil.toJSON(resultSiteCustomer.get(i))).append(i < resultSiteCustomer.size() - 1 ? "," : "");
                    }
                    json.append("],");
                    found = true;
                } else {
                    return "{\"outcome\":\"Error: SiteCustomer Data not found in database.\"}";
                }
            }
        }
        
        if (returnSiteRegionCountry) {
            if ((resultSiteRegionCountry = readDelegate.getSiteRegionCountryByCustomerRegionCountrySite(customer.trim(), region, country, UUID.fromString(siteId))) != null) {
                if (!resultSiteRegionCountry.isEmpty()) {
                    json.append("\"SiteRegionCountry\":[");
                    for (int i = 0; i < resultSiteRegionCountry.size(); i++) {
                        json.append(JsonConverterUtil.toJSON(resultSiteRegionCountry.get(i))).append(i < resultSiteRegionCountry.size() - 1 ? "," : "");
                    }
                    json.append("],");
                    found = true;
                } else {
                    return "{\"outcome\":\"Error: SiteRegionCountry Data not found in database.\"}";
                }
            }
        }

        if (found) {
            json.append("\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return Lists of SiteContacts or SiteCustomer Objects by
     * the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param returnSiteContacts, boolean
     * @param returnSiteCustomer, boolean
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getSiteContactsOrSiteCustomerByCustomerSite(String customer, String siteId, boolean returnSiteContacts, boolean returnSiteCustomer) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        StringBuilder json;
        List<SiteContacts> resultSiteContacts;
        List<SiteCustomer> resultSiteCustomer;
        boolean found;
        
        json = new StringBuilder();
        json
            .append("{\"customer\":\"").append(customer).append("\",")
            .append("\"siteId\":\"").append(siteId).append("\",");
        
        found = false;
        if (returnSiteContacts) {
            if ((resultSiteContacts = readDelegate.getSiteContactsByCustomerSite(customer.trim(), UUID.fromString(siteId))) != null) {
                if (!resultSiteContacts.isEmpty()) {
                    json.append("\"SiteContacts\":[");
                    for (int i = 0; i < resultSiteContacts.size(); i++) {
                        json.append(JsonConverterUtil.toJSON(resultSiteContacts.get(i))).append(i < resultSiteContacts.size() - 1 ? "," : "");
                    }
                    json.append("],");
                    found = true;
                } else {
                    return "{\"outcome\":\"Error: SiteContacts Data not found in database.\"}";
                }
            }
        }
        
        if (returnSiteCustomer) {
            if ((resultSiteCustomer = readDelegate.getSiteCustomerByCustomerSite(customer.trim(), UUID.fromString(siteId))) != null) {
                if (!resultSiteCustomer.isEmpty()) {
                    json.append("\"SiteCustomers\":[");
                    for (int i = 0; i < resultSiteCustomer.size(); i++) {
                        json.append(JsonConverterUtil.toJSON(resultSiteCustomer.get(i))).append(i < resultSiteCustomer.size() - 1 ? "," : "");
                    }
                    json.append("],");
                    found = true;
                } else {
                    return "{\"outcome\":\"Error: SiteCustomer Data not found in database.\"}";
                }
            }
        }

        if (found) {
            json.append("\"outcome\":\"GET worked successfully.\"}");
            return json.toString();
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of SiteContacts Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getSiteContactsByCustomerSite(String customer, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteContacts> result;
        StringBuilder json;

        if ((result = readDelegate.getSiteContactsByCustomerSite(customer.trim(), UUID.fromString(siteId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"SiteContacts\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Error: SiteContacts Data not found in database.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of SiteContacts Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param role, Siring Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getSiteContactsByPK(String customer, String siteId, String role) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteContacts> result;
        StringBuilder json;

        if ((result = readDelegate.getSiteContactsByCustomerSiteRole(customer.trim(), UUID.fromString(siteId), role.trim())) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"role\":\"").append(role).append("\",")
                    .append("\"SiteContacts\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Error: SiteContacts Data not found in database.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of SiteCustomer Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getSiteCustomerByPK(String customer, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteCustomer> result;
        StringBuilder json;

        if ((result = readDelegate.getSiteCustomerByCustomerSite(customer.trim(), UUID.fromString(siteId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"SiteCustomers\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Error: SiteCustomer Data not found in database.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of SiteRegionCountry Objects by the given params
     *
     * @param customer, String Object
     * @param region, String Object
     * @param country, String Object
     * @param siteId String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getSiteRegionCountryByPK(String customer, String region, String country, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteRegionCountry> result;
        StringBuilder json;

        if ((result = readDelegate.getSiteRegionCountryByCustomerRegionCountrySite(customer.trim(), region.trim(), country.trim(), UUID.fromString(siteId))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"region\":\"").append(region).append("\",")
                    .append("\"country\":\"").append(country).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"SiteRegionCountry\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Error: SiteRegionCountry Data not found in database.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of SiteRegionCountry Objects by the given params
     *
     * @param customer, String Object
     * @param region, String Object
     * @param country, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getSiteRegionCountryByCustomerRegionCountry(String customer, String region, String country) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteRegionCountry> result;
        StringBuilder json;

        if ((result = readDelegate.getSiteRegionCountryByCustomerRegionCountry(customer.trim(), region.trim(), country.trim())) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"region\":\"").append(region).append("\",")
                    .append("\"country\":\"").append(country).append("\",")
                    .append("\"SiteRegionCountry\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Error: SiteRegionCountry Data not found in database.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of SiteRegionCountry Objects by the given params
     *
     * @param customer, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getSiteRegionCountryByCustomer(String customer) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteRegionCountry> result;
        StringBuilder json;

        if ((result = readDelegate.getSiteRegionCountryByCustomer(customer.trim())) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"SiteRegionCountry\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Error: SiteRegionCountry Data not found in database.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of SiteCustomer Objects by the given params
     *
     * @param customer, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getSiteCustomerByCustomer(String customer) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteCustomer> result;
        StringBuilder json;

        if ((result = readDelegate.getSiteCustomerByCustomer(customer.trim())) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"SiteCustomers\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Error: SiteCustomer Data not found in database.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new Site by inserting into site_customer, site_region_country and/or site_contacts tables
     *
     * @param content, String Object
     * @return String Object
     */
    public String createSite(String content) {
        CreateSiteDelegate createDelegate;
        boolean isSiteContactPKExist;
        SiteVO siteVO;

        if ((siteVO = JsonUtil.siteParser(content)) != null) {
            if (siteVO != null
                    && siteVO.getCustomerAccount() != null && !siteVO.getCustomerAccount().isEmpty()
                    && siteVO.getSiteName() != null && !siteVO.getSiteName().isEmpty()
                    && siteVO.getRegion() != null && !siteVO.getRegion().isEmpty()
                    && siteVO.getCountry() != null && !siteVO.getCountry().isEmpty()) {
                
                // Insert into Cassandra
                try {
                    if (siteVO.getSiteContactList() != null && !siteVO.getSiteContactList().isEmpty()) {
                        isSiteContactPKExist = siteVO.getSiteContactList().stream()
                                .allMatch(c -> c.getCustomerAccount() != null && !c.getCustomerAccount().isEmpty()
                                && c.getSiteName() != null && !c.getSiteName().isEmpty()
                                && c.getContactName() != null && !c.getContactName().isEmpty()
                                && c.getRole() != null && !c.getRole().isEmpty());
                        if (!isSiteContactPKExist) {
                            return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
                        }
                    }
                    createDelegate = new CreateSiteDelegate();
                    return createDelegate.createSite(siteVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to create new Site.\"}";
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to create new Site.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update tables dealing with the given Site
     *
     * @param content, String Object
     * @return String Object
     */
    public String updateSite(String content) {
        UpdateSiteDelegate updateDelegate;
        SiteVO siteVO;

        if ((siteVO = JsonUtil.siteParser(content)) != null) {
            if (siteVO != null
                    && siteVO.getCustomerAccount() != null && !siteVO.getCustomerAccount().isEmpty()
                    && siteVO.getSiteId() != null 
                    && siteVO.getRegion() != null && !siteVO.getRegion().isEmpty()
                    && siteVO.getCountry() != null && !siteVO.getCountry().isEmpty()) {

                // Update in Cassandra
                try {
                    updateDelegate = new UpdateSiteDelegate();
                    return updateDelegate.updateSite(siteVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to update Site items.\"}";
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to update Site items.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete tables dealing with the given Site
     *
     * @param content, String Object
     * @return String Object
     */
    public String deleteSite(String content) {
        DeleteSiteDelegate deleteDelegate;
        SiteVO siteVO;

        if ((siteVO = JsonUtil.siteParser(content)) != null) {
            if (siteVO != null
                    && siteVO.getCustomerAccount() != null && !siteVO.getCustomerAccount().isEmpty()
                    && siteVO.getSiteId() != null 
                    && siteVO.getRegion() != null && !siteVO.getRegion().isEmpty()
                    && siteVO.getCountry() != null && !siteVO.getCountry().isEmpty()) {
                
                // Delete from Cassandra
                try {
                    deleteDelegate = new DeleteSiteDelegate();
                    return deleteDelegate.deleteSite(siteVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to delete Site items.\"}";
                }
                
            } else {
                return "{\"outcome\":\"Insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
