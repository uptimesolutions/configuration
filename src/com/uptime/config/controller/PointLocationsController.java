/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.entity.PointLocations;
import com.uptime.config.delegate.pointlocations.CreatePointLocationsDelegate;
import com.uptime.config.delegate.pointlocations.DeletePointLocationsDelegate;
import com.uptime.config.delegate.pointlocations.ReadPointLocationsDelegate;
import com.uptime.config.delegate.pointlocations.UpdatePointLocationsDelegate;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.PointLocationVO;
import com.uptime.services.util.JsonConverterUtil;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author madhavi
 */
public class PointLocationsController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PointLocationsController.class.getName());
    ReadPointLocationsDelegate readDelegate;

    public PointLocationsController() {
        readDelegate = new ReadPointLocationsDelegate();
    }

    /**
     * Return a List of PointLocations Objects by the given params
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getPointLocationsByCustomerSiteArea(String customer, String siteId, String areaId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<PointLocations> result;
        StringBuilder json;

        if ((result = readDelegate.getPointLocationsByCustomerSiteArea(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"pointLocations\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }
    
    /**
     * Return a List of PointLocations Objects by the given params
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getPointLocationsByCustomerSiteAreaAsset(String customer, String siteId, String areaId, String assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<PointLocations> result;
        StringBuilder json;

        if ((result = readDelegate.getPointLocationsByCustomerSiteAreaAsset(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()), UUID.fromString(assetId.trim()))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocations\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }
    
    /**
     * Return a List of PointLocations Objects by the given params
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    
    public String getPointLocationsByPK(String customer, String siteId, String areaId, String assetId, String pointLocationId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<PointLocations> result;
        StringBuilder json;

        if ((result = readDelegate.getPointLocationsByPK(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()), UUID.fromString(assetId.trim()), UUID.fromString(pointLocationId.trim()))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointLocations\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new pointLocation by inserting into point_location table
     * @param content, String Object
     * @return String Object
     */
    public String createPointLocation(String content) {
        CreatePointLocationsDelegate createDelegate;
        PointLocationVO pointLocationVO;
        boolean validPoints;
        
        if ((pointLocationVO = JsonUtil.pointLocationParser(content)) != null) {
            if(pointLocationVO != null && 
                    pointLocationVO.getCustomerAccount() != null && !pointLocationVO.getCustomerAccount().isEmpty() &&
                    pointLocationVO.getSiteId()!= null &&
                    pointLocationVO.getAreaId()!= null &&
                    pointLocationVO.getAssetId()!= null && 
                    pointLocationVO.getBasestationPortNum() != 0 && 
                    pointLocationVO.getPointLocationName() != null && !pointLocationVO.getPointLocationName().isEmpty()) {
                
                validPoints = false;
                if (pointLocationVO.getPointList() != null && !pointLocationVO.getPointList().isEmpty()) {
                    if (pointLocationVO.getPointList().stream().allMatch(point -> 
                            point.getPointName() != null && !point.getPointName().isEmpty() &&
                            point.getApSetId()!= null && 
                            point.getAlSetId()!= null)) {
                        validPoints = true;
                    } 
                } else {
                    validPoints = true;
                }
                
                if (validPoints) {
                    
                    // Insert into Cassandra
                    try {
                        createDelegate = new CreatePointLocationsDelegate();
                        return createDelegate.createPointLocations(pointLocationVO);
                    } catch (IllegalArgumentException e) {
                        LOGGER.error(e.getMessage(), e);
                        return "{\"outcome\":\"Error: Failed to create new PointLocations.\"}";
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        return "{\"outcome\":\"Error: Failed to create new PointLocations.\"}";
                    }
                    
                } else {
                    return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
                }
            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
    
    /**
     * Update tables dealing with the given PointLocation
     * @param content, String Object
     * @return String Object
     * @throws java.lang.Exception
     */
    public String updatePointLocation(String content) throws IllegalArgumentException, Exception {
        UpdatePointLocationsDelegate updateDelegate;
        PointLocationVO pointLocationVO;
        boolean validPoints;
        
        if ((pointLocationVO = JsonUtil.pointLocationParser(content)) != null) {
            if(pointLocationVO != null && 
                    pointLocationVO.getCustomerAccount() != null && !pointLocationVO.getCustomerAccount().isEmpty() &&
                    pointLocationVO.getSiteId()!= null && 
                    pointLocationVO.getAreaId()!= null && 
                    pointLocationVO.getAssetId()!= null && 
                    pointLocationVO.getBasestationPortNum() != 0 && 
                    pointLocationVO.getPointLocationId()!= null) {
                if (pointLocationVO.getSensorList() != null && !pointLocationVO.getSensorList().isEmpty()) {
                    // Update in Cassandra
                    try {
                        updateDelegate = new UpdatePointLocationsDelegate();
                        return updateDelegate.updatePointsAlarmEnabledForPointLocation(pointLocationVO);
                    } catch (IllegalArgumentException e) {
                        LOGGER.error(e.getMessage(), e);
                        return "{\"outcome\":\"Error: Failed to update Point Location items.\"}";
                    }
                }
                else {
                    validPoints = false;
                    if (pointLocationVO.getPointList() != null && !pointLocationVO.getPointList().isEmpty()) {
                        if (pointLocationVO.getPointList().stream().allMatch(point -> point.getPointName() != null && !point.getPointName().isEmpty() &&
                                point.getApSetId()!= null && 
                                point.getAlSetId()!= null)) {
                            validPoints = true;
                        }
                    } else { 
                        validPoints = true;
                    }

                    if (validPoints) {

                        // Update in Cassandra
                        try {
                            updateDelegate = new UpdatePointLocationsDelegate();
                            return updateDelegate.updatePointLocation(pointLocationVO);
                        } catch (IllegalArgumentException e) {
                            LOGGER.error(e.getMessage(), e);
                            return "{\"outcome\":\"Error: Failed to update Point Location items.\"}";
                        }

                    } else {
                        return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
                    }
                }
            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
                }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete tables dealing with the given PointLocation
     * @param content, String Object
     * @return String Object
     */
    public String deletePointLocation(String content) {
        DeletePointLocationsDelegate deleteDelegate;
        PointLocationVO pointLocationVO;
        
        if ((pointLocationVO = JsonUtil.pointLocationParser(content)) != null) {
            if(pointLocationVO != null && 
                    pointLocationVO.getCustomerAccount() != null && !pointLocationVO.getCustomerAccount().isEmpty() &&
                    pointLocationVO.getSiteId()!= null &&
                    pointLocationVO.getAreaId()!= null &&
                    pointLocationVO.getAssetId()!= null && 
                    pointLocationVO.getBasestationPortNum() != 0 && 
                    pointLocationVO.getPointLocationId()!= null) {
                    
                // Delete from Cassandra
                try {
                    deleteDelegate = new DeletePointLocationsDelegate();
                    return deleteDelegate.deletePointLocations(pointLocationVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to delete Point Locations items.\"}";
                }
                
            } else {
                return "{\"outcome\":\"Insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
    
    /**
     * Validate Al_set_id for the given pointLocation
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @return String Object
     */
    public String validateAlSetforPoints(String customer, String siteId, String areaId, String assetId, String pointLocationId) {
        return readDelegate.validateAlSetforPoints(customer, UUID.fromString(siteId), UUID.fromString(areaId), UUID.fromString(assetId), UUID.fromString(pointLocationId));
    }

}
