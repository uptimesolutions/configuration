/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.entity.ApAlByPoint;
import com.uptime.config.delegate.apalbypoint.CreateApAlByPointDelegate;
import com.uptime.config.delegate.apalbypoint.DeleteApAlByPointDelegate;
import com.uptime.config.delegate.apalbypoint.ReadApAlByPointDelegate;
import com.uptime.config.delegate.apalbypoint.UpdateApAlByPointDelegate;
import com.uptime.config.utils.JsonUtil;
import com.uptime.config.vo.PointVO;
import com.uptime.services.util.JsonConverterUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author madhavi
 */
public class ApAlByPointController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ApAlByPointController.class.getName());
    ReadApAlByPointDelegate readDelegate;
    

    public ApAlByPointController() {
        readDelegate = new ReadApAlByPointDelegate();
    }

    /**
     * Return a List of ApAlByPoint Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getApAlByPointByCustomerSiteAreaAssetLocationPointApSet(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<ApAlByPoint> result;
        StringBuilder json;

        if ((result = readDelegate.getByCustomerSiteAreaAssetLocationPointApSet(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()), UUID.fromString(assetId.trim()), UUID.fromString(pointLocationId.trim()), UUID.fromString(pointId.trim()), UUID.fromString(apSetId.trim()))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"apSetId\":\"").append(apSetId).append("\",")
                    .append("\"ApAlByPoints\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of ApAlByPoint Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetIds, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getApAlByPointByCustomerSiteAreaAssetLocationPointApSets(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetIds) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<ApAlByPoint> result;
        StringBuilder json;
        List<UUID> apSetIdList = new ArrayList<>();
        String[] apSetIdArr = apSetIds.split(",");
        
        for (String apSetId : apSetIdArr) {
            apSetIdList.add(UUID.fromString(apSetId.trim()));
        }
        if ((result = readDelegate.getByCustomerSiteAreaAssetLocationPointApSets(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()), UUID.fromString(assetId.trim()), UUID.fromString(pointLocationId.trim()), UUID.fromString(pointId.trim()), apSetIdList)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"apSetId\":\"").append(apSetIds).append("\",")
                    .append("\"ApAlByPoints\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of ApAlByPoint Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetId, String Object
     * @param alSetId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetId, String alSetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<ApAlByPoint> result;
        StringBuilder json;

        if ((result = readDelegate.getByCustomerSiteAreaAssetLocationPointApSetAlSet(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()), UUID.fromString(assetId.trim()), UUID.fromString(pointLocationId.trim()), UUID.fromString(pointId.trim()), UUID.fromString(apSetId.trim()), UUID.fromString(alSetId.trim()))) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"apSetId\":\"").append(apSetId).append("\",")
                    .append("\"alSetId\":\"").append(alSetId).append("\",")
                    .append("\"ApAlByPoints\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of ApAlByPoint Objects by the given params
     *
     * @param customer, String Object
     * @param siteId, String Object
     * @param areaId, String Object
     * @param assetId, String Object
     * @param pointLocationId, String Object
     * @param pointId, String Object
     * @param apSetId, String Object
     * @param alSetId, String Object
     * @param paramName, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getApAlByPointByPK(String customer, String siteId, String areaId, String assetId, String pointLocationId, String pointId, String apSetId, String alSetId, String paramName) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<ApAlByPoint> result;
        StringBuilder json;

        if ((result = readDelegate.getApAlByPointByPK(customer.trim(), UUID.fromString(siteId.trim()), UUID.fromString(areaId.trim()), UUID.fromString(assetId.trim()), UUID.fromString(pointLocationId.trim()), UUID.fromString(pointId.trim()), UUID.fromString(apSetId.trim()), UUID.fromString(alSetId.trim()), paramName)) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"customer\":\"").append(customer).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"areaId\":\"").append(areaId).append("\",")
                    .append("\"assetId\":\"").append(assetId).append("\",")
                    .append("\"pointLocationId\":\"").append(pointLocationId).append("\",")
                    .append("\"pointId\":\"").append(pointId).append("\",")
                    .append("\"apSetId\":\"").append(apSetId).append("\",")
                    .append("\"alSetId\":\"").append(alSetId).append("\",")
                    .append("\"paramName\":\"").append(paramName).append("\",")
                    .append("\"ApAlByPoints\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Item/Items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new ApAlByPoint by inserting into ApAlByPoint table
     *
     * @param content, String Object
     * @return String Object
     * @throws java.lang.Exception
     * @throws IllegalArgumentException
     */
    public String createApAlByPoints(String content) throws IllegalArgumentException, Exception {
        CreateApAlByPointDelegate createDelegate;
        List<PointVO> pointVOList;
        PointVO pointVO;
        
        if ((pointVOList = JsonUtil.pointsParser(content)) != null && !pointVOList.isEmpty()) {
            if ((pointVO = pointVOList.get(0)) != null
                    && pointVO.getCustomerAccount() != null && !pointVO.getCustomerAccount().isEmpty()
                    && pointVO.getSiteId() != null
                    && pointVO.getAreaId() != null
                    && pointVO.getAssetId() != null
                    && pointVO.getPointLocationId() != null
                    && pointVO.getPointId() != null
                    && pointVO.getApSetId() != null
                    && pointVO.getAlSetId() != null
                    && pointVO.getApAlSetVOs() != null && !pointVO.getApAlSetVOs().isEmpty()) {

                // Insert into Cassandra
                try {
                    createDelegate = new CreateApAlByPointDelegate();
                    return createDelegate.createApAlByPoint(pointVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to create ApAlByPoints.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update tables dealing with the given ApAlByPoint
     *
     * @param content, String Object
     * @return String Object
     * @throws java.lang.Exception
     * @throws IllegalArgumentException
     */
    public String updateApAlByPoints(String content) throws IllegalArgumentException, Exception {
        UpdateApAlByPointDelegate updateDelegate;
        List<PointVO> pointVOList;
        PointVO pointVO;
        
        if ((pointVOList = JsonUtil.pointsParser(content)) != null && !pointVOList.isEmpty()) {
            if ((pointVO = pointVOList.get(0)) != null
                    && pointVO.getCustomerAccount() != null && !pointVO.getCustomerAccount().isEmpty()
                    && pointVO.getSiteId() != null
                    && pointVO.getAreaId() != null
                    && pointVO.getAssetId() != null
                    && pointVO.getPointLocationId() != null
                    && pointVO.getPointId() != null
                    && pointVO.getApSetId() != null
                    && pointVO.getAlSetId() != null
                    && pointVO.getApAlSetVOs() != null && !pointVO.getApAlSetVOs().isEmpty()) {

                // Update in Cassandra
                try {
                    updateDelegate = new UpdateApAlByPointDelegate();
                    return updateDelegate.updateApAlByPoint(pointVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to update ApAlByPoints.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete tables dealing with the given ApAlByPoint
     *
     * @param content, String Object
     * @return String Object
     */
    public String deleteApAlByPoints(String content) {
        DeleteApAlByPointDelegate deleteDelegate;
        PointVO pointVO;
        List<PointVO> pointVOList;
        
        if ((pointVOList = JsonUtil.pointsParser(content)) != null && !pointVOList.isEmpty()) {
            if ((pointVO = pointVOList.get(0)) != null
                    && pointVO.getCustomerAccount() != null && !pointVO.getCustomerAccount().isEmpty()
                    && pointVO.getSiteId() != null
                    && pointVO.getAreaId() != null
                    && pointVO.getAssetId() != null
                    && pointVO.getPointLocationId() != null
                    && pointVO.getPointId() != null
                    && pointVO.getApSetId() != null
                    && pointVO.getApAlSetVOs() != null && !pointVO.getApAlSetVOs().isEmpty()) {

                // Delete from Cassandra
                try {
                    deleteDelegate = new DeleteApAlByPointDelegate();
                    return deleteDelegate.deletePoints(pointVO);
                } catch (IllegalArgumentException e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to delete ApAlByPoints.\"}";
                }

            } else {
                return "{\"outcome\":\"Insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

}
