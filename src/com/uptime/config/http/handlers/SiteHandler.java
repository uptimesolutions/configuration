/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.http.handlers;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import static com.uptime.config.ConfigService.running;
import static com.uptime.config.ConfigService.sendEvent;
import com.uptime.config.controller.SiteController;
import com.uptime.services.http.handler.AbstractGenericHandlerNew;
import static com.uptime.services.util.HttpUtils.parseQuery;
import static com.uptime.services.util.ServiceUtil.streamReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kpati
 */
public class SiteHandler extends AbstractGenericHandlerNew {
    private static final Logger LOGGER = LoggerFactory.getLogger(SiteHandler.class.getName());
    SiteController controller;
    
    public SiteHandler(){
        controller = new SiteController();
    }
    
    /**
     * HTTP GET handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doGet(HttpExchange he) throws IOException { 
        boolean returnSiteContacts = false;
        boolean returnSiteCustomer = false;
        boolean returnSiteRegionCountry = false;
        StackTraceElement[] stackTrace = null;
        String resp;
        Map<String, Object> params;
        byte[] response;
        OutputStream os;
        Headers respHeaders;
        Object object;

        try {
            if ((params = parseQuery(he.getRequestURI().getRawQuery())).isEmpty()) {
                resp = "{\"outcome\":\"Hello World!\"}";
            } else {
                LOGGER.info("Query: {}", new Object[]{he.getRequestURI().getQuery()});
                for (String key : params.keySet()) {
                    if((object = params.get(key)) != null) {
                        LOGGER.info("{}: {}", new Object[]{key, object.toString()});
                    } else {
                        LOGGER.info("{}: null", new Object[]{key});
                    }
                }
                if (params.containsKey("SiteContacts") || params.containsKey("SiteCustomer") || params.containsKey("SiteRegionCountry")) {
                    if (params.containsKey("SiteContacts") && params.get("SiteContacts").toString().trim().equalsIgnoreCase("True")) {
                        returnSiteContacts = true;
                    }
                    if (params.containsKey("SiteCustomer") && params.get("SiteCustomer").toString().trim().equalsIgnoreCase("True")) {
                        returnSiteCustomer = true;
                    }
                    if (params.containsKey("SiteRegionCountry") && params.get("SiteRegionCountry").toString().trim().equalsIgnoreCase("True")) {
                        returnSiteRegionCountry = true;
                    }
                    if (returnSiteContacts || returnSiteCustomer || returnSiteRegionCountry) {
                        if (params.containsKey("customer") && params.containsKey("region") && params.containsKey("country") && params.containsKey("siteId") ) {
                            if(returnSiteContacts && returnSiteCustomer && returnSiteRegionCountry){
                                try {
                                    resp = controller.getAllByCustomerSiteRegionCountry(params.get("customer").toString().trim(), params.get("region").toString().trim(), params.get("country").toString().trim(), params.get("siteId").toString().trim(), returnSiteContacts, returnSiteCustomer, returnSiteRegionCountry);
                                } catch (NumberFormatException | NullPointerException e) {
                                    resp = "{\"outcome\":\"A valid customer, region, country and siteId are required.\"}";
                                } 
                            } else if(returnSiteRegionCountry) {
                                try {
                                    resp = controller.getSiteRegionCountryByPK(params.get("customer").toString().trim(), params.get("region").toString().trim(), params.get("country").toString().trim(), params.get("siteId").toString().trim());
                                } catch (NumberFormatException | NullPointerException e) {
                                    resp = "{\"outcome\":\"A valid customer, region, country and siteId are required.\"}";
                                } 
                            } else {
                                resp = "{\"outcome\":\"The request parameter returnSiteRegionCountry Or returnSiteRegionCountry, returnSiteCustomer and returnSiteRegionCountry should be set to true when searching for SiteRegionCountry ojects Or SiteContacts, SiteCustomer and SiteRegionCountryfor given customer, siteId, region and country.\"}";
                            }
                        } else if (params.containsKey("customer") && params.containsKey("region") && params.containsKey("country")) {
                            if(returnSiteRegionCountry){
                                try {
                                    resp = controller.getSiteRegionCountryByCustomerRegionCountry(params.get("customer").toString().trim(), params.get("region").toString().trim(), params.get("country").toString().trim());
                                } catch (NumberFormatException | NullPointerException e) {
                                    resp = "{\"outcome\":\"A valid customer, region and country are required.\"}";
                                } 
                            } else {
                                resp = "{\"outcome\":\"The request parameter returnSiteRegionCountry should be set to true when searching for SiteRegionCountry ojects for the given customer, region, and country.\"}";
                            }
                        } else if (params.containsKey("customer") && params.containsKey("siteId")) {
                            if (returnSiteContacts || returnSiteCustomer) {
                                try {
                                    resp = controller.getSiteContactsOrSiteCustomerByCustomerSite(params.get("customer").toString().trim(), params.get("siteId").toString().trim(), returnSiteContacts, returnSiteCustomer);
                                } catch (NumberFormatException | NullPointerException e) {
                                    resp = "{\"outcome\":\"A valid customer and siteId are required.\"}";
                                } 
                            } else {
                                resp = "{\"outcome\":\"The request parameter returnSiteContacts or returnSiteCustomer should be set to true when searching for SiteContacts or SiteCustomer ojects for given customer and siteId.\"}";
                            }
                        } else if (params.containsKey("customer")) {
                            if (returnSiteRegionCountry || returnSiteCustomer) {
                                try {
                                    resp = returnSiteRegionCountry ? controller.getSiteRegionCountryByCustomer(params.get("customer").toString().trim()) : controller.getSiteCustomerByCustomer(params.get("customer").toString().trim());
                                } catch (NumberFormatException | NullPointerException e) {
                                    resp = "{\"outcome\":\"A valid customer is required.\"}";
                                } 
                            } else {
                                resp = "{\"outcome\":\"The request parameter returnSiteRegionCountry or returnSiteCustomer should be set to true when searching for SiteRegionCountry or SiteCustomer ojects for given customer.\"}";
                            }
                        } else {
                            resp = "{\"outcome\":\"Unknown Params\"}";
                        }
                    } else {
                        resp = "{\"outcome\":\"No valid return object specified\"}";
                    }
                } else {
                    resp = "{\"outcome\":\"No valid return object specified\"}";
                }
            }
            
            response = resp.getBytes();
            if (resp.contains("Unknown Params") || (resp.contains("A valid") && resp.contains("required."))) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
            } else if (resp.contains("Null value received")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
            } else if (resp.contains("Data not found in")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_FOUND, response.length);
            } else {
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            }
        } catch (IllegalArgumentException e) {
            LOGGER.error(e.getMessage(), e);
            response = "{\"outcome\":\"IllegalArgumentException\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
        } catch (ReadTimeoutException e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"ReadTimeoutException\"}".getBytes()), he);
        } catch (UnavailableException e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"UnavailableException\"}".getBytes()), he);
        } catch (UnsupportedEncodingException e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"UnsupportedEncodingException\"}".getBytes()), he);
        } catch (Exception e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"Exception\"}".getBytes()), he);
        }
        
        respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();

        // Send Event
        if (stackTrace != null) {
            sendEvent(stackTrace);
        }
    }
    
    /**
     * HTTP POST handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doPost(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        byte[] response;
        OutputStream os;
        Headers respHeaders;
        String resp;
        String content;

        try {
            if (he.getRequestBody() == null) {
                resp = "{\"outcome\":\"RequestBody is invalid\"}";
            } else {
                if ((content = streamReader(he.getRequestBody())) != null) {
                    LOGGER.info("*********JSON VALUE:{}", content);
                    resp = controller.createSite(content);
                } else {
                    resp = "{\"outcome\":\"RequestBody is invalid\"}";
                }
            }
            response = resp.getBytes();
            if (resp.contains(" successfully.")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            } else if (resp.contains("Error: ")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
            } else {
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
            }
        } catch (Exception e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"Exception\"}".getBytes()), he);
        }
        
        respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();

        if (stackTrace != null) {
            sendEvent(stackTrace);
        }
    }
    
 /**
     * HTTP PUT handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doPut(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        byte[] response;
        OutputStream os;
        Headers respHeaders;
        String resp;
        String content;

        try {
            if (he.getRequestBody() == null) {
                resp = "{\"outcome\":\"RequestBody is invalid\"}";
            } else {
                if ((content = streamReader(he.getRequestBody())) != null) {
                    LOGGER.info("*********JSON VALUE:{}", content);
                    resp = controller.updateSite(content);
                } else {
                    resp = "{\"outcome\":\"RequestBody is invalid\"}";
                }
            }
            response = resp.getBytes();
            if (resp.contains(" successfully.")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            } else if (resp.contains("Failed to find items")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_FOUND, response.length);
            } else if (resp.contains("Error: ")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
            } else {
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
            }
        } catch (Exception e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"Exception\"}".getBytes()), he);
        }
        
        respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();

        if (stackTrace != null) {
            sendEvent(stackTrace);
        }
    }
    
    /**
     * HTTP DELETE handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doDelete(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        byte[] response;
        OutputStream os;
        Headers respHeaders;
        String resp;
        String content;

        try {
            if (he.getRequestBody() == null) {
                resp = "{\"outcome\":\"RequestBody is invalid\"}";
            } else {
                if ((content = streamReader(he.getRequestBody())) != null) {
                    LOGGER.info("*********JSON VALUE:{}", content);
                    resp = controller.deleteSite(content);
                } else {
                    resp = "{\"outcome\":\"RequestBody is invalid\"}";
                }
            }
            response = resp.getBytes();
            if (resp.contains(" successfully.") || resp.contains("No Asset items found")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            } else if (resp.contains("Failed to find items")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_FOUND, response.length);
            } else if (resp.contains("Error: ")) {
                he.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
            } else {
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, response.length);
            }
        } catch (Exception e) {
            stackTrace = setStackTrace(e, (response = "{\"outcome\":\"Exception\"}".getBytes()), he);
        }
        
        respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();

        if (stackTrace != null) {
            sendEvent(stackTrace);
        }
    }
    
    /**
     * Returns the field 'running'
     * @return boolean
     */
    @Override
    public boolean getRunning() {
        return running;
    }

    /**
     * Returns the field 'LOGGER'
     * @return Logger Object
     */
    @Override
    public Logger getLogger() {
        return LOGGER;
    }

    /**
     * Set the HttpExchange response header and return a StackTraceElement Array Object
     * @param exception, Exception Object
     * @param response, byte Array Object
     * @param httpExchange, HttpExchange Object
     * @return StackTraceElement Array Object
     * @throws IOException 
     */
    private StackTraceElement[] setStackTrace(final Exception exception, byte[] response, HttpExchange httpExchange) throws IOException {
        LOGGER.error(exception.getMessage(), exception);
        httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_UNAVAILABLE, response.length);
        return exception.getStackTrace();
    }
    
}
