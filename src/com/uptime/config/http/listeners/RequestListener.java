/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.http.listeners;

import com.sun.net.httpserver.HttpServer;
import com.uptime.config.http.handlers.ApAlByPointHandler;
import com.uptime.config.http.handlers.AreaHandler;
import com.uptime.config.http.handlers.AssetHandler;
import com.uptime.config.http.handlers.DeviceStatusHandler;
import com.uptime.config.http.handlers.HwUnitToPointHandler;
import com.uptime.config.http.handlers.ManageDeviceHandler;
import com.uptime.config.http.handlers.PointLocationsHandler;
import com.uptime.config.http.handlers.PointsHandler;
import com.uptime.config.http.handlers.SiteHandler;
import com.uptime.config.http.handlers.SubscriptionHandler;
import com.uptime.config.http.handlers.SystemHandler;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gsingh
 */
public class RequestListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestListener.class.getName());
    private int port = 0;
    HttpServer server;
    
    /**
     * Parameterized Constructor
     * @param port, int 
     */
    public RequestListener(int port) {
        this.port = port;
    }
    
    /**
     * Stop the Http server
     */
    public void stop() {
        LOGGER.info("Stopping request listener...");
        server.stop(10);
    }
    
    /**
     * Start the Http Server with the needed handlers
     * @throws Exception 
     */
    public void start() throws Exception {
        server = HttpServer.create(new InetSocketAddress(port), 2);
        server.createContext("/system", new SystemHandler());
        server.createContext("/config/asset", new AssetHandler());
        server.createContext("/config/pointLocation", new PointLocationsHandler());
        server.createContext("/config/point", new PointsHandler());
        server.createContext("/config/apalbypoint", new ApAlByPointHandler());
        server.createContext("/config/area", new AreaHandler());
        server.createContext("/config/site", new SiteHandler());
        server.createContext("/config/hwUnit", new HwUnitToPointHandler());
        server.createContext("/config/manageDevice", new ManageDeviceHandler());
        server.createContext("/config/subscriptions", new SubscriptionHandler());
        server.createContext("/device-status", new DeviceStatusHandler());
        server.setExecutor(Executors.newCachedThreadPool());
        
        //start server
        LOGGER.info("Starting request listener...");
        server.start();
    }
}
