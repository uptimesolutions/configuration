/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.controller;

import com.uptime.config.vo.PointLocationVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.json.Json;
import javax.json.stream.JsonParser;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.junit.ClassRule;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PointLocationsControllerTest {

    static PointLocationsController instance;

    public PointLocationsControllerTest() {
    }

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//config.cql", true, true, "worldview_dev1"));

    private static String content = "";
    private static final PointLocationVO pVo = new PointLocationVO();

    @BeforeClass
    public static void setUpClass() {
        instance = new PointLocationsController();
        content = "{\n"
                + "    \"customerAccount\": \"FEDEX\",  \n"
                + "    \"siteId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120001\", \n"
                + "    \"areaId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                + "    \"assetId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120003\", \n"
                + "    \"pointLocationName\": \"Location\", \n"
                + "    \"description\": \"Test Point Location Description 319\",  \n"
                + "    \"deviceSerialNumber\": \"BB004567\",  \n"
                + "    \"ffSetIds\":[ \"ddb973d4-c9ef-11ec-9d64-0242ac120007\"],  \n"
                + "    \"speedRatio\": 3.19,  \n"
                + "    \"basestationPortNum\": 2345,  \n"
                + "    \"sampleInterval\": 30,  \n"
                + "    \"rollDiameter\": 3.19,  \n"
                + "    \"rollDiameterUnits\": \"Millimeters\",  \n"
                + "    \"alarmEnabled\": true,  \n"
                + "    \"tachId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120008\"\n"
                + "}";
    }

    /**
     * Test of createPointLocation method, of class PointLocationsController.
     *
     * @throws java.lang.Exception
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test21_CreatePointLocationwithoutPoints() throws Exception {
        System.out.println("test21_CreatePointLocationwithoutPoints");
        String expResult = "{\"outcome\":\"New Point Location created successfully.\"}";
        String result = instance.createPointLocation(content);
        assertEquals(expResult, result);
        String customer = "FEDEX";
        String siteId = "ddb973d4-c9ef-11ec-9d64-0242ac120001";
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        String assetId = "ddb973d4-c9ef-11ec-9d64-0242ac120003";
        String result1 = instance.getPointLocationsByCustomerSiteAreaAsset(customer, siteId, areaId, assetId);
        List<PointLocationVO> list = new ArrayList();
        String keyName;
        JsonParser parser;
        JsonParser.Event event;
        PointLocationVO pointLocationVO;
        boolean inArray;

        try {
            parser = Json.createParser(new StringReader(result1));
            keyName = null;
            pointLocationVO = null;
            inArray = false;

            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        pointLocationVO = null;
                        break;
                    case START_OBJECT:
                        pointLocationVO = inArray ? new PointLocationVO() : null;
                        break;
                    case END_OBJECT:
                        if (pointLocationVO != null && inArray) {
                            list.add(pointLocationVO);
                        }
                        pointLocationVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "customerAccount":
                                keyName = "customerAccount";
                                break;
                            case "siteId":
                                keyName = "siteId";
                                break;
                            case "areaId":
                                keyName = "areaId";
                                break;
                            case "assetId":
                                keyName = "assetId";
                                break;
                            case "pointLocationId":
                                keyName = "pointLocationId";
                                break;
                            case "pointLocationName":
                                keyName = "pointLocationName";
                                break;
                            case "description":
                                keyName = "description";
                                break;
                            case "deviceSerialNumber":
                                keyName = "deviceSerialNumber";
                                break;
                            case "ffSetIds":
                                keyName = "ffSetIds";
                                break;
                            case "speedRatio":
                                keyName = "speedRatio";
                                break;
                            case "sampleInterval":
                                keyName = "sampleInterval";
                                break;
                            case "tachId":
                                keyName = "tachId";
                                break;
                            case "rollDiameter":
                                keyName = "rollDiameter";
                                break;
                            case "rollDiameterUnits":
                                keyName = "rollDiameterUnits";
                                break;
                            default:
                                keyName = null;
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (pointLocationVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "customerAccount":
                                    pointLocationVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteId":
                                    pointLocationVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                                case "areaId":
                                    pointLocationVO.setAreaId(UUID.fromString(parser.getString()));
                                    break;
                                case "assetId":
                                    pointLocationVO.setAssetId(UUID.fromString(parser.getString()));
                                    break;
                                case "pointLocationId":
                                    pointLocationVO.setPointLocationId(UUID.fromString(parser.getString()));
                                    break;
                                case "pointLocationName":
                                    pointLocationVO.setPointLocationName(parser.getString());
                                    break;
                                case "description":
                                    pointLocationVO.setDescription(parser.getString());
                                    break;
                                case "deviceSerialNumber":
                                    pointLocationVO.setDeviceSerialNumber(parser.getString());
                                    break;
                                case "ffsetIds":
                                    pointLocationVO.setFfSetIds(parser.getString());
                                    break;
                                case "tachId":
                                    pointLocationVO.setTachId(UUID.fromString(parser.getString()));
                                    break;
                                case "rollDiameterUnits":
                                    pointLocationVO.setRollDiameterUnits(parser.getString());
                                    break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if (pointLocationVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "speedRatio":
                                    pointLocationVO.setSpeedRatio(parser.getBigDecimal().floatValue());
                                    break;
                                case "sampleInterval":
                                    pointLocationVO.setSampleInterval(parser.getBigDecimal().shortValue());
                                case "rollDiameter":
                                    pointLocationVO.setRollDiameter(parser.getBigDecimal().floatValue());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            System.out.println("Exception - " + e);
            list = new ArrayList();
        }
        pVo.setPointLocationId(list.get(0).getPointLocationId());
        System.out.println("created PointLocationId - " + pVo.getPointLocationId());

    }

    /**
     * Test of createPointLocation method, of class PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Missing required field siteId in input json.
     */
    @Test
    public void test22_CreatePointLocationwithoutPoints() throws Exception {
        System.out.println("test22_CreatePointLocationwithoutPoints");
        content = "{\n"
                + "    \"customerAccount\": \"FEDEX\",  \n"
                + "    \"areaId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                + "    \"assetId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120003\", \n"
                + "    \"pointLocationName\": \"Location\", \n"
                + "    \"description\": \"Test Point Location Description 319\",  \n"
                + "    \"deviceSerialNumber\": \"BB004567\",  \n"
                + "    \"ffSetIds\": [\"ddb973d4-c9ef-11ec-9d64-0242ac120007\"],  \n"
                + "    \"speedRatio\": 3.19,  \n"
                + "    \"fpm\": 3.19,  \n"
                + "    \"rollDiameter\": 3.19,  \n"
                + "    \"rollDiameterUnits\": \"Millimeters\",  \n"
                + "    \"sampleInterval\": 30,  \n"
                + "    \"alarmEnabled\": true,  \n"
                + "    \"tachId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120008\"\n"
                + "}";
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createPointLocation(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of createPointLocation method, of class PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Empty input json.
     */
    @Test
    public void test23_CreatePointLocationwithoutPoints() throws Exception {
        System.out.println("test23_CreatePointLocationwithoutPoints");
        content = "";
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createPointLocation(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of createPointLocation method, of class PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: null input json.
     */
    @Test
    public void test24_CreatePointLocationwithoutPoints() throws Exception {
        System.out.println("test24_CreatePointLocationwithoutPoints");
        content = null;
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createPointLocation(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPointLocationsByCustomerSiteAreaAsset method, of class
     * PointLocationsController.
     *
     * @throws java.lang.Exception
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test31_GetPointLocationsByCustomerSiteArea() throws Exception {
        System.out.println("test31_GetPointLocationsByCustomerSiteArea");
        String customer = "FEDEX";
        String siteId = "ddb973d4-c9ef-11ec-9d64-0242ac120001";
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        String expResult = "{\"customer\":\"FEDEX\",\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120001\",\"areaId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\"pointLocations\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120001\",\"areaId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\"assetId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120003\",\"pointLocationId\":\"" + pVo.getPointLocationId() + "\",\"description\":\"Test Point Location Description 319\",\"deviceSerialNumber\":\"BB004567\",\"ffSetIds\":\"ddb973d4-c9ef-11ec-9d64-0242ac120007\",\"pointLocationName\":\"Location\",\"sampleInterval\":30,\"basestationPortNum\":2345,\"speedRatio\":3.19,\"tachId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120008\",\"alarmEnabled\":false,\"rollDiameter\":3.19,\"rollDiameterUnits\":\"Millimeters\"}],\"outcome\":\"GET worked successfully.\"}";
        System.out.println("expResult-->"+expResult);
        String result = instance.getPointLocationsByCustomerSiteArea(customer, siteId, areaId);
        System.out.println("result-->"+result);
        assertEquals(expResult, result);

    }

    /**
     * Test of getPointLocationsByCustomerSiteAreaAsset method, of class
     * PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: DAO throws IllegalArgumentException when passed empty customer Id. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test32_GetPointLocationsByCustomerSiteArea() throws Exception {
        System.out.println("test32_GetPointLocationsByCustomerSiteArea");
        String customer = "";
        String siteId = "ddb973d4-c9ef-11ec-9d64-0242ac120001";
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        fail(instance.getPointLocationsByCustomerSiteArea(customer, siteId, areaId));
    }

    /**
     * Test of getPointLocationsByCustomerSiteAreaAsset method, of class
     * PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when customer is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test33_GetPointLocationsByCustomerSiteArea() throws Exception {
        System.out.println("test33_GetPointLocationsByCustomerSiteArea");
        String customer = null;
        String siteId = "ddb973d4-c9ef-11ec-9d64-0242ac120001";
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        fail(instance.getPointLocationsByCustomerSiteArea(customer, siteId, areaId));
    }

    /**
     * Test of getPointLocationsByCustomerSiteAreaAsset method, of class
     * PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws IllegalArgumentException when passing invalid siteId. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test34_GetPointLocationsByCustomerSiteArea() throws Exception {
        System.out.println("test34_GetPointLocationsByCustomerSiteArea");
        String customer = "FEDEX";
        String siteId = "ddb973d4-c9ef-11ec-9d64";
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        fail(instance.getPointLocationsByCustomerSiteArea(customer, siteId, areaId));
    }

    /**
     * Test of getPointLocationsByCustomerSiteAreaAsset method, of class
     * PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when siteId is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test35_GetPointLocationsByCustomerSiteArea() throws Exception {
        System.out.println("test35_GetPointLocationsByCustomerSiteArea");
        String customer = "FEDEX";
        String siteId = null;
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        fail(instance.getPointLocationsByCustomerSiteArea(customer, siteId, areaId));
    }

    /**
     * Test of getPointLocationsByCustomerSiteAreaAsset method, of class
     * PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Item not found if passing invalid siteId. 
     */
    @Test
    public void test36_GetPointLocationsByCustomerSiteArea() throws Exception {
        System.out.println("test36_GetPointLocationsByCustomerSiteArea");
        String customer = "FEDEX";
        String siteId = "ddb973d4-c9ef-11ec-9d64-0242ac120001";
        String areaId = "ddb983d4-c9ef-11ec-9d64-0242ac120002";
        String expResult = "{\"outcome\":\"Item/Items not found.\"}";
        String result = instance.getPointLocationsByCustomerSiteArea(customer, siteId, areaId);
        assertEquals(expResult, result);

    }

    /**
     * Test of getPointLocationsByCustomerSiteAreaAsset method, of class
     * PointLocationsController.
     *
     * @throws java.lang.Exception
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test41_GetPointLocationsByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test41_GetPointLocationsByCustomerSiteAreaAsset");
        String customer = "FEDEX";
        String siteId = "ddb973d4-c9ef-11ec-9d64-0242ac120001";
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        String assetId = "ddb973d4-c9ef-11ec-9d64-0242ac120003";
        String expResult = "{\"customer\":\"FEDEX\",\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120001\",\"areaId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\"assetId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120003\",\"pointLocations\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120001\",\"areaId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\"assetId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120003\",\"pointLocationId\":\"" + pVo.getPointLocationId() + "\",\"description\":\"Test Point Location Description 319\",\"deviceSerialNumber\":\"BB004567\",\"ffSetIds\":\"ddb973d4-c9ef-11ec-9d64-0242ac120007\",\"pointLocationName\":\"Location\",\"sampleInterval\":30,\"basestationPortNum\":2345,\"speedRatio\":3.19,\"tachId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120008\",\"alarmEnabled\":false,\"rollDiameter\":3.19,\"rollDiameterUnits\":\"Millimeters\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getPointLocationsByCustomerSiteAreaAsset(customer, siteId, areaId, assetId);
        System.out.println("result 41 -->"+result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPointLocationsByCustomerSiteAreaAsset method, of class
     * PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: DAO throws IllegalArgumentException when passed empty customer Id. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test42_GetPointLocationsByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test42_GetPointLocationsByCustomerSiteAreaAsset");
        String customer = "";
        String siteId = "ddb973d4-c9ef-11ec-9d64-0242ac120001";
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        String assetId = "ddb973d4-c9ef-11ec-9d64-0242ac120003";
        fail(instance.getPointLocationsByCustomerSiteAreaAsset(customer, siteId, areaId, assetId));
    }

    /**
     * Test of getPointLocationsByCustomerSiteAreaAsset method, of class
     * PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when customer is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test43_GetPointLocationsByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test43_GetPointLocationsByCustomerSiteAreaAsset");
        String customer = null;
        String siteId = "ddb973d4-c9ef-11ec-9d64-0242ac120001";
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        String assetId = "ddb973d4-c9ef-11ec-9d64-0242ac120003";
        fail(instance.getPointLocationsByCustomerSiteAreaAsset(customer, siteId, areaId, assetId));
    }

    /**
     * Test of getPointLocationsByCustomerSiteAreaAsset method, of class
     * PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws IllegalArgumentException when passing invalid siteId. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test44_GetPointLocationsByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test44_GetPointLocationsByCustomerSiteAreaAsset");
        String customer = "FEDEX";
        String siteId = "ddb973d4-c9ef-11ec-9d64";
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        String assetId = "ddb973d4-c9ef-11ec-9d64-0242ac120003";
        fail(instance.getPointLocationsByCustomerSiteAreaAsset(customer, siteId, areaId, assetId));
    }

    /**
     * Test of getPointLocationsByCustomerSiteAreaAsset method, of class
     * PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when siteId is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test45_GetPointLocationsByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test45_GetPointLocationsByCustomerSiteAreaAsset");
        String customer = "FEDEX";
        String siteId = null;
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        String assetId = "ddb973d4-c9ef-11ec-9d64-0242ac120003";
        fail(instance.getPointLocationsByCustomerSiteAreaAsset(customer, siteId, areaId, assetId));
    }

    /**
     * Test of getPointLocationsByCustomerSiteAreaAsset method, of class
     * PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Item not found if passing invalid siteId. 
     */
    @Test
    public void test46_GetPointLocationsByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test46_GetPointLocationsByCustomerSiteAreaAsset");
        String customer = "FEDEX";
        String siteId = "ddb973d4-c9ef-11ec-9d64-0242ac120001";
        String areaId = "ddb983d4-c9ef-11ec-9d64-0242ac120002";
        String assetId = "ddb973d4-c9ef-11ec-9d64-0242ac120003";
        String expResult = "{\"outcome\":\"Item/Items not found.\"}";
        String result = instance.getPointLocationsByCustomerSiteAreaAsset(customer, siteId, areaId, assetId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPointLocationsByPK method, of class PointLocationsController.
     *
     * @throws java.lang.Exception
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test51_GetPointLocationsByPK() throws Exception {
        System.out.println("test51_GetPointLocationsByPK");
        String customer = "FEDEX";
        String siteId = "ddb973d4-c9ef-11ec-9d64-0242ac120001";
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        String assetId = "ddb973d4-c9ef-11ec-9d64-0242ac120003";
        String pointLocationId = pVo.getPointLocationId().toString();
        String expResult = "{\"customer\":\"FEDEX\",\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120001\",\"areaId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\"assetId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120003\",\"pointLocationId\":\"" + pointLocationId + "\",\"pointLocations\":[{\"customerAccount\":\"FEDEX\",\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120001\",\"areaId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\"assetId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120003\",\"pointLocationId\":\"" + pVo.getPointLocationId() + "\",\"description\":\"Test Point Location Description 319\",\"deviceSerialNumber\":\"BB004567\",\"ffSetIds\":\"ddb973d4-c9ef-11ec-9d64-0242ac120007\",\"pointLocationName\":\"Location\",\"sampleInterval\":30,\"basestationPortNum\":2345,\"speedRatio\":3.19,\"tachId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120008\",\"alarmEnabled\":false,\"rollDiameter\":3.19,\"rollDiameterUnits\":\"Millimeters\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getPointLocationsByPK(customer, siteId, areaId, assetId, pointLocationId);
        System.out.println("result 51-->"+result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPointLocationsByPK method, of class PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: DAO throws IllegalArgumentException when passed empty customer Id. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test52_GetPointLocationsByPK() throws Exception {
        System.out.println("test52_GetPointLocationsByPK");
        String customer = "";
        String siteId = "ddb973d4-c9ef-11ec-9d64-0242ac120001";
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        String assetId = "ddb973d4-c9ef-11ec-9d64-0242ac120003";
        String pointLocationId = pVo.getPointLocationId().toString();
        fail(instance.getPointLocationsByPK(customer, siteId, areaId, assetId, pointLocationId));
    }

    /**
     * Test of getPointLocationsByPK method, of class PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when customer is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test53_GetPointLocationsByPK() throws Exception {
        System.out.println("test53_GetPointLocationsByPK");
        String customer = null;
        String siteId = "ddb973d4-c9ef-11ec-9d64-0242ac120001";
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        String assetId = "ddb973d4-c9ef-11ec-9d64-0242ac120003";
        String pointLocationId = pVo.getPointLocationId().toString();
        fail(instance.getPointLocationsByPK(customer, siteId, areaId, assetId, pointLocationId));
    }

    /**
     * Test of getPointLocationsByPK method, of class PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws IllegalArgumentException when passing invalid siteId. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test54_GetPointLocationsByPK() throws Exception {
        System.out.println("test54_GetPointLocationsByPK");
        String customer = "FEDEX";
        String siteId = "ddb973d4-c9ef-11ec-9d64";
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        String assetId = "ddb973d4-c9ef-11ec-9d64-0242ac120003";
        String pointLocationId = pVo.getPointLocationId().toString();
        fail(instance.getPointLocationsByPK(customer, siteId, areaId, assetId, pointLocationId));
    }

    /**
     * Test of getPointLocationsByPK method, of class PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when siteId is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test55_GetPointLocationsByPK() throws Exception {
        System.out.println("test55_GetPointLocationsByPK");
        String customer = "FEDEX";
        String siteId = null;
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        String assetId = "ddb973d4-c9ef-11ec-9d64-0242ac120003";
        String pointLocationId = pVo.getPointLocationId().toString();
        fail(instance.getPointLocationsByPK(customer, siteId, areaId, assetId, pointLocationId));
    }

    /**
     * Test of getPointLocationsByPK method, of class PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Item not found if passing invalid siteId. 
     */
    @Test
    public void test56_GetPointLocationsByPK() throws Exception {
        System.out.println("test56_GetPointLocationsByPK");
        String customer = "FEDEX";
        String siteId = "ddb973d4-c9ef-11ec-9d64-0242ac120001";
        String areaId = "ddb983d4-c9ef-11ec-9d64-0242ac120002";
        String assetId = "ddb973d4-c9ef-11ec-9d64-0242ac120003";
        String pointLocationId = pVo.getPointLocationId().toString();
        String expResult = "{\"outcome\":\"Item/Items not found.\"}";
        String result = instance.getPointLocationsByPK(customer, siteId, areaId, assetId, pointLocationId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPointLocationsByPK method, of class PointLocationsController.
     *
     * @throws java.lang.Exception
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test61_ValidateAlSetforPoints() throws Exception {
        System.out.println("test61_ValidateAlSetforPoints");
        String customer = "FEDEX";
        String siteId = "ddb973d4-c9ef-11ec-9d64-0242ac120001";
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        String assetId = "ddb973d4-c9ef-11ec-9d64-0242ac120003";
        String pointLocationId = pVo.getPointLocationId().toString();
        String expResult = "true";
        String result = instance.validateAlSetforPoints(customer, siteId, areaId, assetId, pointLocationId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPointLocationsByPK method, of class PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: DAO throws IllegalArgumentException when passed empty customer Id. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test62_ValidateAlSetforPoints() throws Exception {
        System.out.println("test62_ValidateAlSetforPoints");
        String customer = "";
        String siteId = "ddb973d4-c9ef-11ec-9d64-0242ac120001";
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        String assetId = "ddb973d4-c9ef-11ec-9d64-0242ac120003";
        String pointLocationId = pVo.getPointLocationId().toString();
        fail(instance.validateAlSetforPoints(customer, siteId, areaId, assetId, pointLocationId));
    }

    /**
     * Test of getPointLocationsByPK method, of class PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when customer is passed as null. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test63_ValidateAlSetforPoints() throws Exception {
        System.out.println("test63_ValidateAlSetforPoints");
        String customer = null;
        String siteId = "ddb973d4-c9ef-11ec-9d64-0242ac120001";
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        String assetId = "ddb973d4-c9ef-11ec-9d64-0242ac120003";
        String pointLocationId = pVo.getPointLocationId().toString();
        fail(instance.validateAlSetforPoints(customer, siteId, areaId, assetId, pointLocationId));
    }

    /**
     * Test of getPointLocationsByPK method, of class PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws IllegalArgumentException when passing invalid siteId. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test64_ValidateAlSetforPoints() throws Exception {
        System.out.println("test64_ValidateAlSetforPoints");
        String customer = "FEDEX";
        String siteId = "ddb973d4-c9ef-11ec-9d64";
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        String assetId = "ddb973d4-c9ef-11ec-9d64-0242ac120003";
        String pointLocationId = pVo.getPointLocationId().toString();
        fail(instance.validateAlSetforPoints(customer, siteId, areaId, assetId, pointLocationId));
    }

    /**
     * Test of getPointLocationsByPK method, of class PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when siteId is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test65_ValidateAlSetforPoints() throws Exception {
        System.out.println("test65_ValidateAlSetforPoints");
        String customer = "FEDEX";
        String siteId = null;
        String areaId = "ddb973d4-c9ef-11ec-9d64-0242ac120002";
        String assetId = "ddb973d4-c9ef-11ec-9d64-0242ac120003";
        String pointLocationId = pVo.getPointLocationId().toString();
        fail(instance.validateAlSetforPoints(customer, siteId, areaId, assetId, pointLocationId));
    }

    /**
     * Test of getPointLocationsByPK method, of class PointLocationsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Item not found if passing invalid siteId. 
     */
    @Test
    public void test66_ValidateAlSetforPoints() throws Exception {
        System.out.println("test66_ValidateAlSetforPoints");
        String customer = "FEDEX";
        String siteId = "ddb973d4-c9ef-11ec-9d64-0242ac120001";
        String areaId = "ddb983d4-c9ef-11ec-9d64-0242ac120002";
        String assetId = "ddb973d4-c9ef-11ec-9d64-0242ac120003";
        String pointLocationId = pVo.getPointLocationId().toString();
        String expResult = "true";
        String result = instance.validateAlSetforPoints(customer, siteId, areaId, assetId, pointLocationId);
        assertEquals(expResult, result);
    }

    /**
     * Test of updatePointLocation method, of class PointLocationsController.
     * Happy path: Passing input correctly and achieving desired output. 
     * @throws java.lang.Exception
     */
    @Test
    public void test71_UpdatePointLocationwithoutPoints() throws Exception {
        System.out.println("test71_UpdatePointLocationwithoutPoints - " + pVo.getPointLocationId());
        content = "{\n"
                + "    \"customerAccount\": \"FEDEX\",  \n"
                + "    \"siteId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120001\", \n"
                + "    \"areaId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                + "    \"assetId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120003\", \n"
                + "    \"pointLocationId\": \"" + pVo.getPointLocationId() + "\", \n "
                + "    \"pointLocationName\": \"Location\", \n"
                + "    \"description\": \"Test Point Location Description 319\",  \n"
                + "    \"deviceSerialNumber\": \"BB004567\",  \n"
                + "    \"ffSetIds\": [\"ddb973d4-c9ef-11ec-9d64-0242ac120007\"],  \n"
                + "    \"speedRatio\": 3.19,  \n"
                + "    \"basestationPortNum\": 2355,  \n"
                + "    \"fpm\": 3.19,  \n"
                + "    \"rollDiameter\": 3.19,  \n"
                + "    \"rollDiameterUnits\": \"Millimeters\",  \n"
                + "    \"sampleInterval\": 30,  \n"
                + "    \"alarmEnabled\": true,  \n"
                + "    \"tachId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120008\"\n"
                + "}";
        String expResult = "{\"outcome\":\"Updated Point Locations items successfully.\"}";
        String result = instance.updatePointLocation(content);
        System.out.println("test71_UpdatePointLocationwithoutPoints result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of updatePointLocation method, of class PointLocationsController.
     * Unhappy path: Missing required field siteId in input json.
     * @throws java.lang.Exception
     */
    @Test
    public void test72_UpdatePointLocationwithoutPoints() throws Exception {
        System.out.println("test72_UpdatePointLocationwithoutPoints - " + pVo.getPointLocationId());
        content = "{\n"
                + "    \"customerAccount\": \"FEDEX\",  \n"
                + "    \"areaId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                + "    \"assetId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120003\", \n"
                + "\"pointLocationId\": \"" + pVo.getPointLocationId() + "\", \n "
                + "    \"pointLocationName\": \"Location\", \n"
                + "    \"description\": \"Test Point Location Description 319\",  \n"
                + "    \"deviceSerialNumber\": \"BB004567\",  \n"
                + "    \"ffSetIds\": [\"ddb973d4-c9ef-11ec-9d64-0242ac120007\"],  \n"
                + "    \"speedRatio\": 3.19,  \n"
                + "    \"fpm\": 3.19,  \n"
                + "    \"rollDiameter\": 3.19,  \n"
                + "    \"rollDiameterUnits\": \"Millimeters\",  \n"
                + "    \"sampleInterval\": 30,  \n"
                + "    \"alarmEnabled\": true,  \n"
                + "    \"tachId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120008\"\n"
                + "}";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updatePointLocation(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updatePointLocation method, of class PointLocationsController.
     * Unhappy path: Empty input json.
     * @throws java.lang.Exception
     */
    @Test
    public void test73_UpdatePointLocationwithoutPoints() throws Exception {
        System.out.println("test73_UpdatePointLocationwithoutPoints - " + pVo.getPointLocationId());
        content = "";
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updatePointLocation(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updatePointLocation method, of class PointLocationsController.
     * Unhappy path: null input json.
     * @throws java.lang.Exception
     */
    @Test
    public void test74_UpdatePointLocationwithoutPoints() throws Exception {
        System.out.println("test74_UpdatePointLocationwithoutPoints - " + pVo.getPointLocationId());
        content = null;
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updatePointLocation(content);
        assertEquals(expResult, result);
    }


    /**
     * Test of updatePointLocation method, of class PointLocationsController.
     * Happy path: Passing input correctly and achieving desired output. 
     * @throws java.lang.Exception
     */
    @Test
    public void test75_UpdatePointLocationwithoutPoints() throws Exception {
        System.out.println("test75_UpdatePointLocationwithoutPoints - " + pVo.getPointLocationId());
        content = "{\n"
                + "    \"customerAccount\": \"FEDEX\",  \n"
                + "    \"siteId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120001\", \n"
                + "    \"areaId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                + "    \"assetId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120003\", \n"
                + "    \"pointLocationId\": \"" + pVo.getPointLocationId() + "\", \n "
                + "    \"pointLocationName\": \"Location\", \n"
                + "    \"description\": \"Test Point Location Description 319\",  \n"
                + "    \"deviceSerialNumber\": \"BB004567\",  \n"
                + "    \"ffSetIds\": [\"ddb973d4-c9ef-11ec-9d64-0242ac120007\"],  \n"
                + "    \"speedRatio\": 3.19,  \n"
                + "    \"basestationPortNum\": 2355,  \n"
                + "    \"fpm\": 3.19,  \n"
                + "    \"rollDiameter\": 3.19,  \n"
                + "    \"rollDiameterUnits\": \"Millimeters\",  \n"
                + "    \"sampleInterval\": 30,  \n"
                + "    \"sensorTypes\" : \"Temperature,Acceleration\", \n"
                + "    \"alarmEnabled\": true,  \n"
                + "    \"tachId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120008\"\n"
                + "}";
        String expResult = "{\"outcome\":\"Point Locations items not found to update.\"}";
        String result = instance.updatePointLocation(content);
        System.out.println("test75_UpdatePointLocationwithoutPoints result - " + result);
        assertEquals(expResult, result);
    }
    /**
     * Test of deletePointLocation method, of class PointLocationsController.
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test81_DeletePointLocationWithoutPoints() {
        System.out.println("test81_DeletePointLocationWithoutPoints");
        String content = "{\n"
                + "    \"customerAccount\": \"FEDEX\",  \n"
                + "    \"siteId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120001\", \n"
                + "    \"areaId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                + "    \"assetId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120003\", \n"
                + "\"pointLocationId\": \"" + pVo.getPointLocationId() + "\", \n "
                + "    \"pointLocationName\": \"Location\", \n"
                + "    \"description\": \"Test Point Location Description 319\",  \n"
                + "    \"deviceSerialNumber\": \"BB004567\",  \n"
                + "    \"ffSetIds\": [\"ddb973d4-c9ef-11ec-9d64-0242ac120007\"],  \n"
                + "    \"speedRatio\": 3.19,  \n"
                + "    \"basestationPortNum\": 2345,  \n"
                + "    \"fpm\": 3.19,  \n"
                + "    \"rollDiameter\": 3.19,  \n"
                + "    \"rollDiameterUnits\": \"Millimeters\",  \n"
                + "    \"sampleInterval\": 30,  \n"
                + "    \"alarmEnabled\": true,  \n"
                + "    \"tachId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120008\"\n"
                + "}";
        String expResult = "{\"outcome\":\"Deleted Point Locations items successfully.\"}";
        String result = instance.deletePointLocation(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deletePointLocation method, of class PointLocationsController.
     * Unhappy path: Missing required field siteId in input json.
     */
    @Test
    public void test82_DeletePointLocationWithoutPoints() {
        System.out.println("test82_DeletePointLocationWithoutPoints");
        String content = "{\n"
                + "    \"customerAccount\": \"FEDEX\",  \n"
                + "    \"areaId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                + "    \"assetId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120003\", \n"
                + "\"pointLocationId\": \"" + pVo.getPointLocationId() + "\", \n "
                + "    \"pointLocationName\": \"Location\", \n"
                + "    \"description\": \"Test Point Location Description 319\",  \n"
                + "    \"deviceSerialNumber\": \"BB004567\",  \n"
                + "    \"ffSetIds\": [\"ddb973d4-c9ef-11ec-9d64-0242ac120007\"],  \n"
                + "    \"speedRatio\": 3.19,  \n"
                + "    \"fpm\": 3.19,  \n"
                + "    \"rollDiameter\": 3.19,  \n"
                + "    \"rollDiameterUnits\": \"Millimeters\",  \n"
                + "    \"sampleInterval\": 30,  \n"
                + "    \"alarmEnabled\": true,  \n"
                + "    \"tachId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120008\"\n"
                + "}";
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deletePointLocation(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deletePointLocation method, of class PointLocationsController.
     * Unhappy path: Empty input json.
     */
    @Test
    public void test83_DeletePointLocationWithoutPoints() {
        System.out.println("test83_DeletePointLocationWithoutPoints");
        String content = "";
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.deletePointLocation(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deletePointLocation method, of class PointLocationsController.
     * Unhappy path: null input json.
     */
    @Test
    public void test84_DeletePointLocationWithoutPoints() {
        System.out.println("test84_DeletePointLocationWithoutPoints");
        String content = null;
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.deletePointLocation(content);
        assertEquals(expResult, result);
    }

}
