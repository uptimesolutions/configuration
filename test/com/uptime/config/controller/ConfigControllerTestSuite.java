/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.config.controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author gopal
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    ApAlByPointControllerTest.class,
    AreaControllerTest.class,
    AssetControllerTest.class,
    HwUnitControllerTest.class,
    ManageDeviceControllerTest.class,
    PointLocationsControllerTest.class,
    PointsControllerTest.class,
    SiteControllerTest.class,
    SubscriptionsControllerTest.class,
    // Add more test classes here
})
public class ConfigControllerTestSuite {
    // This class doesn't need to have any code
}
