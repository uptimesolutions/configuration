/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.controller;

import com.uptime.cassandra.workorder.dao.OpenWorkOrdersDAO;
import com.uptime.cassandra.workorder.dao.WorkOrdersMapperImpl;
import com.uptime.cassandra.workorder.entity.OpenWorkOrders;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AssetControllerTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit  =
            new CassandraCQLUnit(new ClassPathCQLDataSet("resources//config.cql", true, true,"worldview_dev1"));

    static AssetController instance;
    static UUID newAssetId = UUID.fromString("88c4a233-5e07-4d89-947b-7fead5950e84");
    static OpenWorkOrdersDAO instance1;
    static OpenWorkOrders entity;
    private static final String DATE_TIME_OFFSET_STRING = "yyyy-MM-dd HH:mm:ssZ";
    public AssetControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = new AssetController();
        instance1 = WorkOrdersMapperImpl.getInstance().openWorkOrdersDAO();
        entity = new OpenWorkOrders();
        entity.setAction("");
        entity.setAreaId(UUID.fromString("57e923d1-15ed-4993-8068-72a01f91a6d9"));
        entity.setAreaName("Test Area 1");
        entity.setAssetComponent("XXX Pump");
        entity.setAssetId(newAssetId);
        entity.setAssetName("Test Asset 1");
        entity.setCreatedDate(LocalDateTime.parse("2022-10-19 01:00:00+0000", DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_STRING)).atZone(ZoneOffset.UTC).toInstant());
        entity.setCustomerAccount("77777");
        entity.setDescription("Client Asset Desc 1");
        entity.setIssue("Test issue");
        entity.setNotes("Test Note");
        entity.setPriority((byte)2);
        entity.setSiteId(UUID.fromString("e4c66964-5a8c-451d-ae2e-9d04d83c8241"));
        entity.setUser("Uptime");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Test of createAsset method, of class AssetController.
     */
    @Test
    public void test10_CreateAsset() {
        System.out.println("createAsset");
        String content = "{"
                + "\"customerAccount\" : \"77777\", "
                + "\"siteId\" : \"e4c66964-5a8c-451d-ae2e-9d04d83c8241\", "
                + "\"areaId\" : \"57e923d1-15ed-4993-8068-72a01f91a6d9\", "
                + "\"assetName\" : \"Asset 111\", "
                + "\"assetType\" : \"Asset Type 111\", "
                + "\"assetId\" : \""+ newAssetId + "\", "
                + "\"description\" : \"Test Description 111\", "
                + "\"alarmEnabled\" : \"true\", "
                + "\"sampleInterval\" : \"10\""
                + "}\n";
        System.out.println("content\n " + content);
        String expResult = "{\"outcome\":\"New Asset created successfully.\"}";
        String result = instance.createAsset(content);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of createAsset method, of class AssetController.
     * Provided a json without assetName
     */
    @Test
    public void test12_CreateAsset() {
        System.out.println("createAsset");
        String content = "{"
                + "\"customerAccount\" : \"77777\", "
                + "\"siteId\" : \"e4c66964-5a8c-451d-ae2e-9d04d83c8241\", "
                + "\"areaId\" : \"57e923d1-15ed-4993-8068-72a01f91a6d9\", "
                //+ "\"assetName\" : \"Asset 111\", "
                + "\"assetType\" : \"Asset Type 111\", "
                + "\"assetId\" : \""+ newAssetId + "\", "
                + "\"description\" : \"Test Description 111\", "
                + "\"alarmEnabled\" : \"true\", "
                + "\"sampleInterval\" : \"10\""
                + "}\n";
        System.out.println("content\n " + content);
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createAsset(content);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of createAsset method, of class AssetController.
     * Provided null Json
     */
    @Test
    public void test13_CreateAsset() {
        System.out.println("createAsset");
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createAsset(null);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAssetSiteAreaByCustomerSite method, of class AssetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test43_GetAssetSiteAreaByCustomerSite() throws Exception {
        System.out.println("getAssetSiteAreaByCustomerSite");
        String customer = "77777";
        String siteName = "e4c66964-5a8c-451d-ae2e-9d04d83c8241";
        
        String expResult = "{\"customer\":\"77777\",\"siteId\":\"e4c66964-5a8c-451d-ae2e-9d04d83c8241\",\"AssetSiteAreas\":[{\"customerAccount\":\"77777\",\"siteId\":\"e4c66964-5a8c-451d-ae2e-9d04d83c8241\",\"areaId\":\"57e923d1-15ed-4993-8068-72a01f91a6d9\",\"assetId\":\"88c4a233-5e07-4d89-947b-7fead5950e84\",\"assetName\":\"Asset 111\",\"assetType\":\"Asset Type 111\",\"description\":\"Test Description 111\",\"assetTypeId\":\"53a1bc71-94c5-4ab9-acfa-07304e74c4df\",\"sampleInterval\":10,\"alarmEnabled\":false}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAssetSiteAreaByCustomerSite(customer, siteName);
        System.out.println(" Result 43 JSON - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAssetSiteAreaByCustomerSiteArea method, of class AssetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test44_GetAssetSiteAreaByCustomerSiteArea() throws Exception {
        System.out.println("getAssetSiteAreaByCustomerSiteArea");
        String customer = "77777";
        String siteName = "e4c66964-5a8c-451d-ae2e-9d04d83c8241";
        String areaName = "57e923d1-15ed-4993-8068-72a01f91a6d9";
      
        String expResult = "{\"customer\":\"77777\",\"siteId\":\"e4c66964-5a8c-451d-ae2e-9d04d83c8241\",\"areaId\":\"57e923d1-15ed-4993-8068-72a01f91a6d9\",\"AssetSiteAreas\":[{\"customerAccount\":\"77777\",\"siteId\":\"e4c66964-5a8c-451d-ae2e-9d04d83c8241\",\"areaId\":\"57e923d1-15ed-4993-8068-72a01f91a6d9\",\"assetId\":\"88c4a233-5e07-4d89-947b-7fead5950e84\",\"assetName\":\"Asset 111\",\"assetType\":\"Asset Type 111\",\"description\":\"Test Description 111\",\"assetTypeId\":\"53a1bc71-94c5-4ab9-acfa-07304e74c4df\",\"sampleInterval\":10,\"alarmEnabled\":false}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAssetSiteAreaByCustomerSiteArea(customer, siteName, areaName);
        System.out.println(" Result 44 JSON - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAssetSiteAreaByPK method, of class AssetController.
     * @throws java.lang.Exception
     */
    @Test
    public void test45_GetAssetSiteAreaByPK() throws Exception {
        System.out.println("getAssetSiteAreaByPK");
        String customer = "77777";
        String siteName = "e4c66964-5a8c-451d-ae2e-9d04d83c8241";
        String areaName = "57e923d1-15ed-4993-8068-72a01f91a6d9";
        String assetId = AssetControllerTest.newAssetId.toString();        
        String expResult = "{\"customer\":\"77777\",\"siteId\":\"e4c66964-5a8c-451d-ae2e-9d04d83c8241\",\"areaId\":\"57e923d1-15ed-4993-8068-72a01f91a6d9\",\"assetId\":\"88c4a233-5e07-4d89-947b-7fead5950e84\",\"AssetSiteAreas\":[{\"customerAccount\":\"77777\",\"siteId\":\"e4c66964-5a8c-451d-ae2e-9d04d83c8241\",\"areaId\":\"57e923d1-15ed-4993-8068-72a01f91a6d9\",\"assetId\":\"88c4a233-5e07-4d89-947b-7fead5950e84\",\"assetName\":\"Asset 111\",\"assetType\":\"Asset Type 111\",\"description\":\"Test Description 111\",\"assetTypeId\":\"53a1bc71-94c5-4ab9-acfa-07304e74c4df\",\"sampleInterval\":10,\"alarmEnabled\":false}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAssetSiteAreaByPK(customer, siteName, areaName, assetId);
        System.out.println(" Result 45 JSON - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of updateAsset method, of class AssetController.
     */
    @Test
    public void test91_UpdateAsset() throws Exception{
        System.out.println("updateAsset");
        String content = "{"
                + "\"customerAccount\" : \"77777\", "
                + "\"siteId\" : \"e4c66964-5a8c-451d-ae2e-9d04d83c8241\", "
                + "\"areaId\" : \"57e923d1-15ed-4993-8068-72a01f91a6d9\", "
                + "\"assetName\" : \"Asset 222\", "
                + "\"assetType\" : \"Asset Type 111\", "
                + "\"assetId\" : \""+ newAssetId + "\", "
                + "\"description\" : \"Test Description 111\", "
                + "\"alarmEnabled\" : \"true\", "
                + "\"sampleInterval\" : \"20\""
                + "}\n";
        
        String expResult = "{\"outcome\":\"Updated Asset items successfully.\"}";
        instance1.create(entity);
        String result = instance.updateAsset(content);
        assertEquals(expResult, result);
        List<OpenWorkOrders> resultWorkOrders =  instance1.findByCustomerSiteAreaAsset(entity.getCustomerAccount(), entity.getSiteId(), 
                entity.getAreaId(), entity.getAssetId());
        assertEquals(1, resultWorkOrders.size());
        assertEquals(resultWorkOrders.get(0).getAssetName(), "Asset 222");
    }
    
    /**
     * Test of updateAsset method, of class AssetController.
     */
    @Test
    public void test92_UpdateAsset() throws Exception{
        System.out.println("updateAsset");
        String content = "{"
                + "\"customerAccount\" : \"77777\", "
                + "\"siteId\" : \"e4c66964-5a8c-451d-ae2e-9d04d83c8241\", "
                + "\"areaId\" : \"57e923d1-15ed-4993-8068-72a01f91a6d9\", "
                + "\"assetName\" : \"Asset 222\", "
                + "\"assetType\" : \"Asset Type 111\", "
                + "\"assetId\" : \""+ newAssetId + "\", "
                + "\"description\" : \"Test Description 111\", "
                + "\"alarmEnabled\" : \"true\", "
                + "\"sampleInterval\" : \"20\""
                + "}\n";
        
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateAsset(content);
        System.out.println("test92_UpdateAsset result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of updateAsset method, of class AssetController.
     */
    @Test
    public void test93_UpdateAsset() throws Exception{
        System.out.println("updateAsset");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updateAsset(null);
        System.out.println("test93_UpdateAsset result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of updateAsset method, of class AssetController.
     */
    @Test
    public void test94_UpdateAsset() throws Exception{
        System.out.println("updateAsset");
        String content = "{"
                + "\"customerAccount\" : \"77777\", "
                + "\"siteId\" : \"e4c66964-5a8c-451d-ae2e-9d04d83c8241\", "
                + "\"areaId\" : \"57e923d1-15ed-4993-8068-72a01f91a6d9\", "
                + "\"assetName\" : \"Asset 222\", "
                + "\"assetType\" : \"Asset Type 111\", "
                + "\"assetId\" : \""+ newAssetId + "\", "
                + "\"description\" : \"Test Description 111\", "
                + "\"sensorTypes\" : \"Temperature,Acceleration\", "
                + "\"alarmEnabled\" : \"true\", "
                + "\"sampleInterval\" : \"20\""
                + "}\n";
         
        String expResult = "{\"outcome\":\"Asset items not found to update.\"}";
        String result = instance.updateAsset(content);
        System.out.println("test94_UpdateAsset result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteAsset method, of class AssetController.
     */
    @Test
    public void test99_DeleteAsset() {
        System.out.println("deleteAsset");
        String content = "{"
                + "\"customerAccount\" : \"77777\", "
                + "\"siteId\" : \"e4c66964-5a8c-451d-ae2e-9d04d83c8241\", "
                + "\"areaId\" : \"57e923d1-15ed-4993-8068-72a01f91a6d9\", "
                + "\"assetName\" : \"Asset 111\", "
                + "\"assetType\" : \"ASSET TYPE 111\", "
                + "\"assetId\" : \""+ newAssetId + "\", "
                + "\"alarmEnabled\" : \"true\", "
                + "\"description\" : \"Test Description 111\" "
                + "}\n";
        
        String expResult = "{\"outcome\":\"Deleted Asset items successfully.\"}";
        String result = instance.deleteAsset(content);
        assertEquals(expResult, result);
    }

}
