/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HwUnitControllerTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit  =
           new CassandraCQLUnit(new ClassPathCQLDataSet("resources//config.cql", true, true,"worldview_dev1"));
    static HwUnitController instance;
    
    private static String content = "";
    private static String contentDC = "";
    private static String pointId;
    
    public HwUnitControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = new HwUnitController();
        content = "{\n" +
"    \"deviceId\": \"BB003003\",\n" +
"    \"channelType\": \"AC\",\n" +
"    \"channelNum\": 1,\n" +
"    \"customerAccount\": \"77777\",\n" +
"    \"siteId\": \"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
"    \"areaId\": \"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\n" +
"    \"assetId\": \"4d288160-04cf-4e84-8f9f-4dc0f22442c0\",\n" +
"    \"pointLocationId\": \"4e74c569-530d-46ce-ac0b-5d4a94f811c2\",\n" +
"    \"pointName\": \"Test Point A11\",\n" +
"    \"apSetId\": \"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\n" +
"    \"alSetId\": \"668e4093-b882-4c6a-b9f5-a4af8b8fa47f\",\n" +
"    \"alarmEnabled\": true,\n" +
"    \"autoAcknowledge\": true,\n" +
"    \"isDisabled\": false,\n" +
"    \"pointType\": \"AC\",\n" +
"    \"sampleInterval\": 1191,\n" +
"    \"sensorChannelNum\": 1191,\n" +
"    \"sensorOffset\": 0.1191,\n" +
"    \"sensorSensitivity\": 1.1191,\n" +
"    \"sensorUnits\": \"Celcius\",\n" +
"    \"useCustomLimits\": true,\n" +
"    \"apAlSetVOs\": [\n" +
"        {\n" +
"            \"paramName\": \"Overall\",\n" +
"            \"fmax\": \"977\",\n" +
"            \"resolution\": \"1600\",\n" +
"            \"dwell\": \"3\",\n" +
"            \"period\": \"1.638\",\n" +
"            \"sampleRate\": \"2500\",\n" +
"            \"minFrequency\": \"0\",\n" +
"            \"maxFrequency\": \"1\",\n" +
"            \"lowFault\": \"0\",\n" +
"            \"lowAlert\": \"1\",\n" +
"            \"highAlert\": \"2\",\n" +
"            \"highFault\": \"3\",\n" +
"            \"demodHighPass\": \"0\",\n" +
"            \"demodLowPass\": \"0\",\n" +
"            \"lowFaultActive\": \"true\",\n" +
"            \"lowAlertActive\": \"true\",\n" +
"            \"highAlertActive\": \"true\",\n" +
"            \"highFaultActive\": \"true\",\n" +
"            \"demod\": \"false\",\n" +
"            \"paramType\": \"Total Spectral Energy\",\n" +
"            \"frequencyUnits\": \"Hz\",\n" +
"            \"paramUnits\": \"Gs\",\n" +
"            \"paramAmpFactor\": \"Peak\",\n" +
"            \"sensorType\": \"Acceleration\"\n" +
"        },\n" +
"        {\n" +
"            \"paramName\": \"Total Non Sync\",\n" +
"            \"fmax\": \"977\",\n" +
"            \"resolution\": \"1600\",\n" +
"            \"dwell\": \"3\",\n" +
"            \"period\": \"1.638\",\n" +
"            \"sampleRate\": \"2500\",\n" +
"            \"minFrequency\": \"0\",\n" +
"            \"maxFrequency\": \"1\",\n" +
"            \"lowFault\": \"0\",\n" +
"            \"lowAlert\": \"1\",\n" +
"            \"highAlert\": \"2\",\n" +
"            \"highFault\": \"3\",\n" +
"            \"demodHighPass\": \"0\",\n" +
"            \"demodLowPass\": \"0\",\n" +
"            \"lowFaultActive\": \"true\",\n" +
"            \"lowAlertActive\": \"true\",\n" +
"            \"highAlertActive\": \"true\",\n" +
"            \"highFaultActive\": \"true\",\n" +
"            \"demod\": \"false\",\n" +
"            \"paramType\": \"Total Non Sync Energy\",\n" +
"            \"frequencyUnits\": \"RPM\",\n" +
"            \"paramUnits\": \"mils\",\n" +
"            \"paramAmpFactor\": \"RMS\",\n" +
"            \"sensorType\": \"Acceleration\"\n" +
"        },\n" +
"        {\n" +
"            \"paramName\": \"Total Sub Sync\",\n" +
"            \"fmax\": \"977\",\n" +
"            \"resolution\": \"1600\",\n" +
"            \"dwell\": \"3\",\n" +
"            \"period\": \"1.638\",\n" +
"            \"sampleRate\": \"2500\",\n" +
"            \"minFrequency\": \"0\",\n" +
"            \"maxFrequency\": \"1\",\n" +
"            \"lowFault\": \"0\",\n" +
"            \"lowAlert\": \"1\",\n" +
"            \"highAlert\": \"2\",\n" +
"            \"highFault\": \"3\",\n" +
"            \"demodHighPass\": \"0\",\n" +
"            \"demodLowPass\": \"0\",\n" +
"            \"lowFaultActive\": \"true\",\n" +
"            \"lowAlertActive\": \"true\",\n" +
"            \"highAlertActive\": \"true\",\n" +
"            \"highFaultActive\": \"true\",\n" +
"            \"demod\": \"false\",\n" +
"            \"paramType\": \"Total Sub Sync Energy\",\n" +
"            \"frequencyUnits\": \"RPM\",\n" +
"            \"paramUnits\": \"Pascals\",\n" +
"            \"paramAmpFactor\": \"RMS\",\n" +
"            \"sensorType\": \"Acceleration\"\n" +
"        }\n" +
"    ]\n" +
"}";
        
        contentDC = "{\n" +
"    \"deviceId\": \"BB003004\",\n" +
"    \"channelType\": \"DC\",\n" +
"    \"channelNum\": 1,\n" +
"    \"customerAccount\": \"77777\",\n" +
"    \"siteId\": \"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\n" +
"    \"areaId\": \"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\n" +
"    \"assetId\": \"4d288160-04cf-4e84-8f9f-4dc0f22442c0\",\n" +
"    \"pointLocationId\": \"4e74c569-530d-46ce-ac0b-5d4a94f811c2\",\n" +
"    \"pointName\": \"Test Point A11\",\n" +
"    \"apSetId\": \"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\n" +
"    \"alSetId\": \"668e4093-b882-4c6a-b9f5-a4af8b8fa47f\",\n" +
"    \"alarmEnabled\": true,\n" +
"    \"autoAcknowledge\": true,\n" +
"    \"isDisabled\": false,\n" +
"    \"pointType\": \"DC\",\n" +
"    \"sampleInterval\": 1191,\n" +
"    \"sensorChannelNum\": 1191,\n" +
"    \"sensorOffset\": 0.1191,\n" +
"    \"sensorSensitivity\": 1.1191,\n" +
"    \"sensorUnits\": \"Celcius\",\n" +
"    \"useCustomLimits\": true,\n" +
"    \"apAlSetVOs\": [\n" +
"        {\n" +
"            \"paramName\": \"DC PARAM\",\n" +
"            \"fmax\": \"977\",\n" +
"            \"resolution\": \"1600\",\n" +
"            \"dwell\": \"3\",\n" +
"            \"period\": \"1.638\",\n" +
"            \"sampleRate\": \"2500\",\n" +
"            \"minFrequency\": \"0\",\n" +
"            \"maxFrequency\": \"1\",\n" +
"            \"lowFault\": \"0\",\n" +
"            \"lowAlert\": \"1\",\n" +
"            \"highAlert\": \"2\",\n" +
"            \"highFault\": \"3\",\n" +
"            \"demodHighPass\": \"0\",\n" +
"            \"demodLowPass\": \"0\",\n" +
"            \"lowFaultActive\": \"true\",\n" +
"            \"lowAlertActive\": \"true\",\n" +
"            \"highAlertActive\": \"true\",\n" +
"            \"highFaultActive\": \"true\",\n" +
"            \"demod\": \"false\",\n" +
"            \"paramType\": \"Total Spectral Energy\",\n" +
"            \"frequencyUnits\": \"Hz\",\n" +
"            \"paramUnits\": \"Gs\",\n" +
"            \"paramAmpFactor\": \"Peak\",\n" +
"            \"sensorType\": \"Acceleration\"\n" +
"        }\n" +
"    ]\n" +
"}";
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of createHwUnitToPoint method, of class HwUnitController.
     */
    @Test
    public void test10_CreateHwUnitToPoint() {
        System.out.println("createHwUnitToPoint");
        String expResult = "{\"outcome\":\"New HwUnitToPoint created successfully.\"}";
        String result = instance.createHwUnitToPoint(content);
        System.out.println("createHwUnitToPoint json - " + content);
        assertEquals(expResult, result);
    }
    /**
     * Test of createHwUnitToPoint method, of class HwUnitController.
     */
    @Test
    public void test101_CreateHwUnitToPoint() {
        System.out.println("createHwUnitToPoint");
        String expResult = "{\"outcome\":\"New HwUnitToPoint created successfully.\"}";
        String result = instance.createHwUnitToPoint(contentDC);
        System.out.println("createHwUnitToPoint json - " + contentDC);
        assertEquals(expResult, result);
    }

    /**
     * Test of createHwUnitToPoint method, of class HwUnitController.
     * Provided Json without deviceId
     */
    @Test
    public void test12_CreateHwUnitToPoint() {
        System.out.println("createHwUnitToPoint");
        content = "{\n"   
               // + "\"deviceId\": \"BB003003\",     \n"
                + "\"channelType\": \"DC\",     \n"
                + "\"channelNum\": 1,     \n"
                + "\"customerAccount\": \"77777\",     \n"
                + "\"siteId\": \"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",            \n"
                + "\"areaId\": \"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",            \n"
                + "\"assetId\": \"4d288160-04cf-4e84-8f9f-4dc0f22442c0\",               \n"
                + "\"pointLocationId\": \"4e74c569-530d-46ce-ac0b-5d4a94f811c2\",  \n"
                + "\"pointName\": \"Test Point A11\",\n"
                + "\"apSetId\": \"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\n"
                + "\"alSetId\": \"668e4093-b882-4c6a-b9f5-a4af8b8fa47f\",\n"
                + "\"paramName\": \"TEMP\",\n"
                + "\"alarmEnabled\": true,\n"
                + "\"autoAcknowledge\": true,\n"
                + "\"isDisabled\": false,\n"
                + "\"pointType\": \"DC\",\n"
                + "\"sampleInterval\": 1191,\n"
                + "\"sensorChannelNum\": 1191,\n"
                + "\"sensorOffset\": 0.1191,\n"
                + "\"sensorSensitivity\": 1.1191,\n"
                + "\"sensorType\": \"Sensor\",\n"
                + "\"sensorUnits\": \"Celcius\",\n"
                + "\"useCustomLimits\": true,\n"
                + "\"demodEnabled\": true,\n"
                + "\"demodHighPass\": 1.1191,\n"
                + "\"dwell\": 11,\n"
                + "\"demodLowPass\": 0.1191,\n"
                + "\"fMax\": 100,\n"
                + "\"freqUnits\": \"Celcius\",\n"
                + "\"highAlert\": 1.234,\n"
                + "\"highAlertActive\": true,\n"
                + "\"highFault\": 1.134,\n"
                + "\"highFaultActive\": true,\n"
                + "\"customized\": false,\n"
                + "\"lowAlert\": 0.234,\n"
                + "\"lowAlertActive\": true,\n"
                + "\"lowFault\": 0.134,\n"
                + "\"lowFaultActive\": true,\n"
                + "\"maxFreq\": 0.1,\n"
                + "\"minFreq\": 0.5,\n"
                + "\"paramAmpFactor\": \"TEMP-Celcius\",\n"
                + "\"paramType\": \"TEMP\",\n"
                + "\"paramUnits\": \"Celcius\",\n"
                + "\"period\": 0.6,\n"
                + "\"resolution\": 1600,\n"
                + "\"sampleRate\": 0.7\n"
                + "}\n";
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createHwUnitToPoint(content);
        System.out.println("createHwUnitToPoint json - " + content);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getLookupPointConfigByHardware method, of class HwUnitController.
     * @throws java.lang.Exception
     */
    @Test
    public void test13_GetLookupPointConfigByHardware() throws Exception {
        System.out.println("getLookupPointConfigByHardware");
        String deviceId = "BB003003";
        byte chnNum  = 1;
        String result = instance.getLookupPointConfigByHardware(deviceId, "AC", chnNum);
        pointId = result.substring(result.indexOf("\"pointId\":")+11, result.indexOf("\"pointId\":")+47);
        String expResult = "{\"customerId\":\"77777\",\"deviceId\":\"BB003003\",\"channelType\":\"AC\",\"channelNumber\":1,\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"siteName\":\"null\",\"areaId\":\"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\"areaName\":\"null\",\"assetId\":\"4d288160-04cf-4e84-8f9f-4dc0f22442c0\",\"assetName\":\"null\",\"pointLocationId\":\"4e74c569-530d-46ce-ac0b-5d4a94f811c2\",\"pointLocationName\":\"null\",\"speedRatio\":0.0,\"rollDiameter\":0.0,\"rollDiameterUnits\":\"null\",\"pointId\":\"" + pointId                      + "\",\"pointName\":\"Test Point A11\",\"tachId\":null,\"baseStationPort\":0,\"sensorType\":\"Acceleration\",\"sensitivity\":1.1191,\"sensitivityUnits\":\"Celcius\",\"offset\":0.1191,\"alarmenabled\":true,\"apSet\":[{\"apSetId\":\"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\"fmax\":977,\"resolution\":1600,\"period\":1.638,\"sampleRate\":2500.0,\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"isRTD\":null,\"params\":[{\"paramName\":\"Overall\",\"paramType\":\"Total Spectral Energy\",\"freqUnits\":\"Hz\",\"paramUnits\":\"Gs\",\"paramAmpFactor\":\"Peak\",\"minFreq\":0.0,\"maxFreq\":1.0,\"alSet\":[{\"alSetId\":\"668e4093-b882-4c6a-b9f5-a4af8b8fa47f\",\"dwell\":3,\"lowFaultActive\":true,\"lowAlertActive\":true,\"highAlertActive\":true,\"highFaultActive\":true,\"lowFault\":0.0,\"lowAlert\":1.0,\"highAlert\":2.0,\"highFault\":3.0}]},{\"paramName\":\"Total Non Sync\",\"paramType\":\"Total Non Sync Energy\",\"freqUnits\":\"RPM\",\"paramUnits\":\"mils\",\"paramAmpFactor\":\"RMS\",\"minFreq\":0.0,\"maxFreq\":1.0,\"alSet\":[{\"alSetId\":\"668e4093-b882-4c6a-b9f5-a4af8b8fa47f\",\"dwell\":3,\"lowFaultActive\":true,\"lowAlertActive\":true,\"highAlertActive\":true,\"highFaultActive\":true,\"lowFault\":0.0,\"lowAlert\":1.0,\"highAlert\":2.0,\"highFault\":3.0}]},{\"paramName\":\"Total Sub Sync\",\"paramType\":\"Total Sub Sync Energy\",\"freqUnits\":\"RPM\",\"paramUnits\":\"Pascals\",\"paramAmpFactor\":\"RMS\",\"minFreq\":0.0,\"maxFreq\":1.0,\"alSet\":[{\"alSetId\":\"668e4093-b882-4c6a-b9f5-a4af8b8fa47f\",\"dwell\":3,\"lowFaultActive\":true,\"lowAlertActive\":true,\"highAlertActive\":true,\"highFaultActive\":true,\"lowFault\":0.0,\"lowAlert\":1.0,\"highAlert\":2.0,\"highFault\":3.0}]}] }]}";
        System.out.println("result 13 - " + result);
        assertEquals(expResult, result);
    }
    
    
    /**
     * Test of getLookupPointConfigByHardware method, of class HwUnitController.;
     * @throws java.lang.Exception
     */
    @Test
    public void test14_GetLookupPointConfigByHardware() throws Exception {
        System.out.println("getLookupPointConfigByHardware");
        String deviceId = "BB003004";
        byte chnNum  = 1;
        String result = instance.getLookupPointConfigByHardware(deviceId, "DC", chnNum);
        pointId = result.substring(result.indexOf("\"pointId\":")+11, result.indexOf("\"pointId\":")+47);
        String expResult = "{\"customerId\":\"77777\",\"deviceId\":\"BB003004\",\"channelType\":\"DC\",\"channelNumber\":1,\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"siteName\":\"null\",\"areaId\":\"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\"areaName\":\"null\",\"assetId\":\"4d288160-04cf-4e84-8f9f-4dc0f22442c0\",\"assetName\":\"null\",\"pointLocationId\":\"4e74c569-530d-46ce-ac0b-5d4a94f811c2\",\"pointLocationName\":\"null\",\"speedRatio\":0.0,\"rollDiameter\":0.0,\"rollDiameterUnits\":\"null\",\"pointId\":\"" + pointId +                      "\",\"pointName\":\"Test Point A11\",\"tachId\":null,\"baseStationPort\":0,\"sensorType\":\"Acceleration\",\"sensitivity\":1.1191,\"sensitivityUnits\":\"Celcius\",\"offset\":0.1191,\"apSetId\":\"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\",\"alSetId\":\"668e4093-b882-4c6a-b9f5-a4af8b8fa47f\",\"dwell\":3,\"lowFaultActive\":true,\"lowAlertActive\":true,\"highAlertActive\":true,\"highFaultActive\":true,\"lowFault\":0.0,\"lowAlert\":1.0,\"highAlert\":2.0,\"highFault\":3.0,\"alarmenabled\":true}";
        System.out.println("result 14 - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getHwUnitToPointByPK method, of class HwUnitController.
     * @throws java.lang.Exception
     */
    @Test
    public void test20_GetHwUnitToPointByDevice() throws Exception {
        System.out.println("getHwUnitToPointByDevice");
        String deviceId = "BB003003";
        String result = instance.getHwUnitToPointByDevice(deviceId);
        pointId = result.substring(result.indexOf("\"pointId\":")+11, result.indexOf("\"pointId\":")+47);
        String expResult = "{\"deviceId\":\"BB003003\",\"HwUnitToPoints\":[{\"deviceId\":\"BB003003\",\"channelType\":\"AC\",\"channelNum\":1,\"customerAcct\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"areaId\":\"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\"assetId\":\"4d288160-04cf-4e84-8f9f-4dc0f22442c0\",\"pointLocationId\":\"4e74c569-530d-46ce-ac0b-5d4a94f811c2\",\"pointId\":\"" + pointId + "\",\"apSetId\":\"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\"}],\"outcome\":\"GET worked successfully.\"}";
        System.out.println("result 20 - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getHwUnitToPointByPK method, of class HwUnitController.Provided deviceId that does not exist
     * @throws java.lang.Exception
     */
    @Test
    public void test21_GetHwUnitToPointByDevice() throws Exception {
        System.out.println("getHwUnitToPointByDevice");
        String deviceId = "BB003007";
        String result = instance.getHwUnitToPointByDevice(deviceId);
        String expResult = "{\"outcome\":\"Item/Items not found.\"}";
        System.out.println("result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getHwUnitToPointByPK method, of class HwUnitController.Provided null deviceId
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test22_GetHwUnitToPointByDevice() throws Exception {
        System.out.println("getHwUnitToPointByDevice");
        fail(instance.getHwUnitToPointByDevice(""));
    }
    
    /**
     * Test of getHwUnitToPointByPK method, of class HwUnitController.
     * @throws java.lang.Exception
     */
    @Test
    public void test23_GetHwUnitToPointByPK() throws Exception {
        System.out.println("getHwUnitToPointByPK");
        String deviceId = "BB003003";
        String channelType = "AC";
        byte channelNum = 1;
        String result = instance.getHwUnitToPointByPK(deviceId, channelType, channelNum);
        pointId = result.substring(result.indexOf("\"pointId\":")+11, result.indexOf("\"pointId\":")+47);
        String expResult = "{\"deviceId\":\"BB003003\",\"channelType\":\"AC\",\"channelNum\":\"1\",\"HwUnitToPoints\":[{\"deviceId\":\"BB003003\",\"channelType\":\"AC\",\"channelNum\":1,\"customerAcct\":\"77777\",\"siteId\":\"dee5158a-30b0-4c12-b6b9-40cb91d1206d\",\"areaId\":\"98d2cfb9-069e-4dd7-8ac0-f2835351e5b0\",\"assetId\":\"4d288160-04cf-4e84-8f9f-4dc0f22442c0\",\"pointLocationId\":\"4e74c569-530d-46ce-ac0b-5d4a94f811c2\",\"pointId\":\"" + pointId + "\",\"apSetId\":\"2d2a3fc1-f82a-4710-ab83-8dfc935f9726\"}],\"outcome\":\"GET worked successfully.\"}";
        System.out.println("result 23 - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getHwUnitToPointByPK method, of class HwUnitController.Provided deviceId that does not exist
     * @throws java.lang.Exception
     */
    @Test
    public void test24_GetHwUnitToPointByPK() throws Exception {
        System.out.println("getHwUnitToPointByPK");
        String deviceId = "BB003007";
        String channelType = "DC";
        byte channelNum = 1;
        String result = instance.getHwUnitToPointByPK(deviceId, channelType, channelNum);
        String expResult = "{\"outcome\":\"Item/Items not found.\"}";
        System.out.println("result 24 - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getHwUnitToPointByPK method, of class HwUnitController.Provided null deviceId
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test25_GetHwUnitToPointByPK() throws Exception {
        System.out.println("getHwUnitToPointByPK");
        String deviceId = null;
        String channelType = "DC";
        byte channelNum = 1;
        deviceId = deviceId == null? "":deviceId;
        fail(instance.getHwUnitToPointByPK(deviceId, channelType, channelNum));
    }

}
