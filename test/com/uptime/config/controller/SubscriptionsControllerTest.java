/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.config.controller;

import com.uptime.config.vo.SubscriptionsVO;
import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SubscriptionsControllerTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit  =
            new CassandraCQLUnit(new ClassPathCQLDataSet("resources//config.cql", true, true,"worldview_dev1"));
    private static SubscriptionsController instance;
    private static final SubscriptionsVO subscriptionsVO = new SubscriptionsVO();
    
    public SubscriptionsControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = new SubscriptionsController();
        subscriptionsVO.setCustomerAccount("FEDEX EXPRESS");
        subscriptionsVO.setSiteId(UUID.fromString("f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
        subscriptionsVO.setSubscriptionType("ALARMS");
        subscriptionsVO.setMqProtocol("ARTEMIS");
        subscriptionsVO.setMqConnectString("tcp://mqdev.corp.uptime-solutions.us:61616");
        subscriptionsVO.setMqUser("Test User");
        subscriptionsVO.setMqPwd("Test pwd");
        subscriptionsVO.setMqQueueName("Test Queue");
        subscriptionsVO.setMqClientId("Test Mq Client");
        subscriptionsVO.setInternal(true);
        subscriptionsVO.setWebhookUrl("Test Web hook");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of createSubscriptions method, of class SubscriptionsController.
     */
    @Test
    public void test10_CreateSubscriptions() {
        System.out.println("createSubscriptions");
        String content = "{\n" +
"    \"customerAccount\": \"FEDEX EXPRESS\",\n" +
"    \"siteId\": \"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\n" +
"    \"subscriptionType\": \"ALARMS\",\n" +
"    \"mqProtocol\": \"MQTT\",\n" +
"    \"mqConnectString\": \"tcp://mqdev.corp.uptime-solutions.us:61616\",\n" +
"    \"mqUser\": \"testUser\",\n" +
"    \"mqPwd\": \"testPwd\",\n" +
"    \"mqQueueName\": \"Test Queue\",\n" +
"    \"mqClientId\": \"Test Client\",\n" +
"    \"webhookUrl\": \"Test URL\",\n" +
"    \"internal\": true\n" +
"}";
        String expResult = "{\"outcome\":\"New Subscriptions created successfully.\"}";
        String result = instance.createSubscriptions(content);
        System.out.println("test10_CreateSubscriptions result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of createSubscriptions method, of class SubscriptionsController.
     */
    @Test
    public void test11_CreateSubscriptions() {
        System.out.println("createSubscriptions");
        String content = "{\n" +
"    \"customerAccount\": \"FEDEX EXPRESS\",\n" +
//"    \"siteId\": \"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\n" +
"    \"subscriptionType\": \"ALARMS\",\n" +
"    \"mqProtocol\": \"MQTT\",\n" +
"    \"mqConnectString\": \"tcp://mqdev.corp.uptime-solutions.us:61616\",\n" +
"    \"mqUser\": \"testUser\",\n" +
"    \"mqPwd\": \"testPwd\",\n" +
"    \"mqQueueName\": \"Test Queue\",\n" +
"    \"mqClientId\": \"Test Client\",\n" +
"    \"webhookUrl\": \"Test URL\",\n" +
"    \"internal\": true\n" +
"}";
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createSubscriptions(content);
        System.out.println("test11_CreateSubscriptions result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of createSubscriptions method, of class SubscriptionsController.
     */
    @Test
    public void test12_CreateSubscriptions() {
        System.out.println("createSubscriptions");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createSubscriptions(null);
        System.out.println("test12_CreateSubscriptions result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getSubscriptionsByCustomerSite method, of class SubscriptionsController.
     */
    @Test
    public void test20_GetSubscriptionsByCustomerSite() throws Exception {
        System.out.println("getSubscriptionsByCustomerSite");
        String expResult = "{\"customer\":\"FEDEX EXPRESS\",\"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\"Subscriptions\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\"subscriptionType\":\"ALARMS\",\"mqProtocol\":\"MQTT\",\"mqConnectString\":\"tcp://mqdev.corp.uptime-solutions.us:61616\",\"mqUser\":\"testUser\",\"mqPwd\":\"testPwd\",\"mqQueueName\":\"Test Queue\",\"mqClientId\":\"Test Client\",\"internal\":true,\"webhookUrl\":\"Test URL\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getSubscriptionsByCustomerSite(subscriptionsVO.getCustomerAccount(), subscriptionsVO.getSiteId().toString());
        System.out.println("test20_GetSubscriptionsByCustomerSite result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getSubscriptionsByCustomerSite method, of class SubscriptionsController.
     */
    @Test
    public void test21_GetSubscriptionsByCustomerSite() throws Exception {
        System.out.println("getSubscriptionsByCustomerSite");
        String expResult = "{\"outcome\":\"Error: Subscriptions not found in database.\"}";
        String result = instance.getSubscriptionsByCustomerSite("5555", subscriptionsVO.getSiteId().toString());
        System.out.println("test21_GetSubscriptionsByCustomerSite result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getSubscriptionsByCustomerSite method, of class SubscriptionsController.
     */
    @Test(expected = IllegalArgumentException.class)
    public void test22_GetSubscriptionsByCustomerSite() throws Exception {
        System.out.println("getSubscriptionsByCustomerSite");
        instance.getSubscriptionsByCustomerSite(subscriptionsVO.getCustomerAccount(), "5tyuuuuuuuuu");
    }

    /**
     * Test of getSubscriptionsByPK method, of class SubscriptionsController.
     */
    @Test
    public void test30_GetSubscriptionsByPK() throws Exception {
        System.out.println("getSubscriptionsByPK");
        String expResult = "{\"customer\":\"FEDEX EXPRESS\",\"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\"subscriptionType\":\"ALARMS\",\"Subscriptions\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\"subscriptionType\":\"ALARMS\",\"mqProtocol\":\"MQTT\",\"mqConnectString\":\"tcp://mqdev.corp.uptime-solutions.us:61616\",\"mqUser\":\"testUser\",\"mqPwd\":\"testPwd\",\"mqQueueName\":\"Test Queue\",\"mqClientId\":\"Test Client\",\"internal\":true,\"webhookUrl\":\"Test URL\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getSubscriptionsByPK(subscriptionsVO.getCustomerAccount(), subscriptionsVO.getSiteId().toString(), subscriptionsVO.getSubscriptionType());
        System.out.println("getSubscriptionsByPK result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSubscriptionsByPK method, of class SubscriptionsController.
     */
    @Test
    public void test31_GetSubscriptionsByPK() throws Exception {
        System.out.println("getSubscriptionsByPK");
        String expResult = "{\"outcome\":\"Error: Subscriptions not found in database.\"}";
        String result = instance.getSubscriptionsByPK("55555", subscriptionsVO.getSiteId().toString(), subscriptionsVO.getSubscriptionType());
        System.out.println("test31_GetSubscriptionsByPK result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSubscriptionsByPK method, of class SubscriptionsController.
     */
    @Test(expected = IllegalArgumentException.class)
    public void test32_GetSubscriptionsByPK() throws Exception {
        System.out.println("getSubscriptionsByPK");
        instance.getSubscriptionsByPK(subscriptionsVO.getCustomerAccount(), "54345667", subscriptionsVO.getSubscriptionType());
    }

    /**
     * Test of updateSubscriptions method, of class SubscriptionsController.
     */
    @Test
    public void test40_UpdateSubscriptions() {
        System.out.println("updateSubscriptions");
        String content = "{\n" +
"    \"customerAccount\": \"FEDEX EXPRESS\",\n" +
"    \"siteId\": \"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\n" +
"    \"subscriptionType\": \"ALARMS\",\n" +
"    \"mqProtocol\": \"MQTT\",\n" +
"    \"mqConnectString\": \"tcp://mqdev.corp.uptime-solutions.us:61616\",\n" +
"    \"mqUser\": \"testUser\",\n" +
"    \"mqPwd\": \"testPwd222\",\n" +
"    \"mqQueueName\": \"Test Queue\",\n" +
"    \"mqClientId\": \"Test Client\",\n" +
"    \"webhookUrl\": \"Test URL\",\n" +
"    \"internal\": true\n" +
"}";
        String expResult = "{\"outcome\":\"Updated Subscriptions successfully.\"}";
        String result = instance.updateSubscriptions(content);
        System.out.println("test40_UpdateSubscriptions result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSubscriptions method, of class SubscriptionsController.
     */
    @Test
    public void test41_UpdateSubscriptions() {
        System.out.println("updateSubscriptions");
        String content = "{\n" +
"    \"customerAccount\": \"FEDEX EXPRESS\",\n" +
//"    \"siteId\": \"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\n" +
"    \"subscriptionType\": \"ALARMS\",\n" +
"    \"mqProtocol\": \"MQTT\",\n" +
"    \"mqConnectString\": \"tcp://mqdev.corp.uptime-solutions.us:61616\",\n" +
"    \"mqUser\": \"testUser\",\n" +
"    \"mqPwd\": \"testPwd222\",\n" +
"    \"mqQueueName\": \"Test Queue\",\n" +
"    \"mqClientId\": \"Test Client\",\n" +
"    \"webhookUrl\": \"Test URL\",\n" +
"    \"internal\": true\n" +
"}";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateSubscriptions(content);
        System.out.println("test41_UpdateSubscriptions result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateSubscriptions method, of class SubscriptionsController.
     */
    @Test
    public void test42_UpdateSubscriptions() {
        System.out.println("updateSubscriptions");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updateSubscriptions(null);
        System.out.println("test42_UpdateSubscriptions result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteSubscriptions method, of class SubscriptionsController.
     */
    @Test
    public void test50_DeleteSubscriptions() {
        System.out.println("deleteSubscriptions");
        String content = "{\n" +
"    \"customerAccount\": \"FEDEX EXPRESS\",\n" +
"    \"siteId\": \"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\n" +
"    \"subscriptionType\": \"ALARMS\",\n" +
"    \"mqProtocol\": \"MQTT\",\n" +
"    \"mqConnectString\": \"tcp://mqdev.corp.uptime-solutions.us:61616\",\n" +
"    \"mqUser\": \"testUser\",\n" +
"    \"mqPwd\": \"testPwd222\",\n" +
"    \"mqQueueName\": \"Test Queue\",\n" +
"    \"mqClientId\": \"Test Client\",\n" +
"    \"webhookUrl\": \"Test URL\",\n" +
"    \"requestType\": \"Delete\",\n" +
"    \"internal\": true\n" +
"}";
        String expResult = "{\"outcome\":\"Deleted Subscriptions successfully.\"}";
        String result = instance.deleteSubscriptions(content);
        System.out.println("test50_DeleteSubscriptions result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteSubscriptions method, of class SubscriptionsController.
     */
    @Test
    public void test51_DeleteSubscriptions() {
        System.out.println("deleteSubscriptions");
        String content = "{\n" +
"    \"customerAccount\": \"FEDEX EXPRESS\",\n" +
//"    \"siteId\": \"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\n" +
"    \"subscriptionType\": \"ALARMS\",\n" +
"    \"mqProtocol\": \"MQTT\",\n" +
"    \"mqConnectString\": \"tcp://mqdev.corp.uptime-solutions.us:61616\",\n" +
"    \"mqUser\": \"testUser\",\n" +
"    \"mqPwd\": \"testPwd222\",\n" +
"    \"mqQueueName\": \"Test Queue\",\n" +
"    \"mqClientId\": \"Test Client\",\n" +
"    \"webhookUrl\": \"Test URL\",\n" +
"    \"requestType\": \"Delete\",\n" +
"    \"internal\": true\n" +
"}";
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteSubscriptions(content);
        System.out.println("test51_DeleteSubscriptions result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteSubscriptions method, of class SubscriptionsController.
     */
    @Test
    public void test52_DeleteSubscriptions() {
        System.out.println("deleteSubscriptions");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.deleteSubscriptions(null);
        System.out.println("test52_DeleteSubscriptions result - " + result);
        assertEquals(expResult, result);
    }
    
}
