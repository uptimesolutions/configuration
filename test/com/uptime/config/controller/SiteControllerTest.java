/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.controller;

import com.uptime.config.vo.SiteContactVO;
import com.uptime.config.vo.SiteVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.json.Json;
import javax.json.stream.JsonParser;
import static javax.json.stream.JsonParser.Event.START_ARRAY;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiteControllerTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit  =
            new CassandraCQLUnit(new ClassPathCQLDataSet("resources//config.cql", true, true,"worldview_dev1"));
    private static SiteController instance;
    private static final SiteVO siteVO = new SiteVO();
    public SiteControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = new SiteController();
        siteVO.setBillingAddress1("4820 Executive Park Ct");
        siteVO.setBillingAddress2("");
        siteVO.setBillingCity("Jacksonville");
        siteVO.setBillingPostalCode("32216");
        siteVO.setBillingStateProvince("FL");
        siteVO.setCountry("UNITED STATES");
        siteVO.setCustomerAccount("77777");
        siteVO.setRegion("NORTH AMERICA");
        siteVO.setLatitude("30.252254");
        siteVO.setLongitude("-81.602805");
        siteVO.setTimezone("Pacific/Niue (UTC -11:00)");
        siteVO.setIndustry("Production");
        siteVO.setSiteName("UPTIME SITE 1");
        siteVO.setSiteId(UUID.fromString("fe6d75c8-2f96-450b-ada9-2c6b329e565f"));
        siteVO.setPhysicalAddress1("4820 Executive Park Ct");
        siteVO.setPhysicalAddress2("Jacksonville");
        siteVO.setPhysicalCity("Jacksonville");
        siteVO.setPhysicalPostalCode("32216");
        siteVO.setPhysicalStateProvince("FL");
        siteVO.setShipAddress1("4820 Executive Park Ct");
        siteVO.setShipAddress2("Jacksonville");
        siteVO.setShipCity("Jacksonville");
        siteVO.setShipPostalCode("32216");
        siteVO.setShipStateProvince("FL");
        List<SiteContactVO> siteContactList = new ArrayList<>();
        SiteContactVO scvo = new SiteContactVO();
        scvo.setContactEmail("test1@test.com");
        scvo.setContactName("TEST NAME 1");
        scvo.setContactPhone("5555544441");
        scvo.setContactTitle("Mr.");
        scvo.setCustomerAccount("77777");
        scvo.setRole("IT");
        scvo.setSiteName("UPTIME SITE 1");
        siteContactList.add(scvo);
        
        scvo = new SiteContactVO();
        scvo.setContactEmail("test2@test.com");
        scvo.setContactName("TEST NAME 2");
        scvo.setContactPhone("5555544442");
        scvo.setContactTitle("Mr.");
        scvo.setCustomerAccount("77777");
        scvo.setRole("REPORTS");
        scvo.setSiteName("UPTIME SITE 1");
        
        siteContactList.add(scvo);
        siteVO.setSiteContactList(siteContactList);
                
                
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Test of createSite method, of class SiteController.
     * @throws java.lang.Exception
     */
    @Test
    public void test10_CreateSite() throws Exception {
        System.out.println("createSite");
        String content = "{\"customerAccount\":\"77777\",\"siteName\":\"UPTIME SITE 1\","
                + "\"region\":\"NORTH AMERICA\",\"country\":\"UNITED STATES\","
                + "\"physicalAddress1\":\"4820 Executive Park Ct\",\"physicalAddress2\":\"\","
                + "\"physicalCity\":\"Jacksonville\",\"physicalStateProvince\":\"FL\","
                + "\"physicalPostalCode\":\"32216\",\"latitude\":\"30.252254\","
                + "\"longitude\":\"-81.602805\",\"timezone\":\"Pacific/Niue (UTC -11:00)\","
                + "\"industry\":\"Production\",\"shipAddress1\":\"4820 Executive Park Ct\","
                + "\"shipAddress2\":\"\",\"shipCity\":\"Jacksonville\",\"shipStateProvince\":\"FL\","
                + "\"shipPostalCode\":\"32216\",\"billingAddress1\":\"4820 Executive Park Ct\","
                + "\"billingAddress2\":\"\",\"billingCity\":\"Jacksonville\",\"billingStateProvince\":\"FL\","
                + "\"billingPostalCode\":\"32216\",\"siteContactList\":[{\"customerAccount\":\"77777\","
                + "\"siteName\":\"UPTIME SITE 1\",\"role\":\"IT\",\"contactName\":\"TEST NAME 1\","
                + "\"contactTitle\":\"Mr.\",\"contactPhone\":\"5555544441\",\"contactEmail\":\"test1@test.com\"},"
                + "{\"customerAccount\":\"77777\",\"siteName\":\"UPTIME SITE 1\","
                + "\"role\":\"REPORTS\",\"contactName\":\"TEST NAME 2\",\"contactTitle\":\"Mr.\","
                + "\"contactPhone\":\"5555544442\",\"contactEmail\":\"test2@test.com\"}]"
                + "}\n";
        String expResult = "{\"outcome\":\"New Site created successfully.\"}";
        String result = instance.createSite(content);
        assertEquals(expResult, result);
        String result1 = instance.getSiteCustomerByCustomer(siteVO.getCustomerAccount());
        List<SiteVO> list = new ArrayList();
        JsonParser parser;
        JsonParser.Event event;
        String keyName;
        SiteVO sVO;
        boolean inArray;
        try {
            parser = Json.createParser(new StringReader(result1));
            keyName = null;
            sVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        sVO = null;
                        break;
                    case START_OBJECT:
                        sVO = inArray ? new SiteVO() : null;
                        break;
                    case END_OBJECT:
                        if(sVO != null && inArray)
                            list.add(sVO);
                        sVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "siteId":
                                    keyName = "siteId";
                                    break;
                                case "siteName":
                                    keyName = "siteName";
                                    break;
                                case "region":
                                    keyName = "region";
                                    break;
                                case "country":
                                    keyName = "country";
                                    break;
                                case "physicalAddress1":
                                    keyName = "physicalAddress1";
                                    break;
                                case "physicalAddress2":
                                    keyName = "physicalAddress2";
                                    break;
                                case "physicalCity":
                                    keyName = "physicalCity";
                                    break;
                                case "physicalStateProvince":
                                    keyName = "physicalStateProvince";
                                    break;
                                case "physicalPostalCode":
                                    keyName = "physicalPostalCode";
                                    break;
                                case "latitude":
                                    keyName = "latitude";
                                    break;
                                case "longitude":
                                    keyName = "longitude";
                                    break;
                                case "timezone":
                                    keyName = "timezone";
                                    break;
                                case "shipAddress1":
                                    keyName = "shipAddress1";
                                    break;
                                case "shipAddress2":
                                    keyName = "shipAddress2";
                                    break;
                                case "shipCity":
                                    keyName = "shipCity";
                                    break;
                                case "shipStateProvince":
                                    keyName = "shipStateProvince";
                                    break;
                                case "shipPostalCode":
                                    keyName = "shipPostalCode";
                                    break;
                                case "billingAddress1":
                                    keyName = "billingAddress1";
                                    break;
                                case "billingAddress2":
                                    keyName = "billingAddress2";
                                    break;
                                case "billingCity":
                                    keyName = "billingCity";
                                    break;
                                case "billingStateProvince":
                                    keyName = "billingStateProvince";
                                    break;
                                case "billingPostalCode":
                                    keyName = "billingPostalCode";
                                    break;
                                case "industry":
                                    keyName = "industry";
                                    break;
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(sVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    sVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteId":
                                    sVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                                case "siteName":
                                    sVO.setSiteName(parser.getString());
                                    break;
                                case "region":
                                    sVO.setRegion(parser.getString());
                                    break;
                                case "country":
                                    sVO.setCountry(parser.getString());
                                    break;
                                case "physicalAddress1":
                                    sVO.setPhysicalAddress1(parser.getString());
                                    break;
                                case "physicalAddress2":
                                    sVO.setPhysicalAddress2(parser.getString());
                                    break;
                                case "physicalCity":
                                    sVO.setPhysicalCity(parser.getString());
                                    break;
                                case "physicalStateProvince":
                                    sVO.setPhysicalStateProvince(parser.getString());
                                    break;
                                case "physicalPostalCode":
                                    sVO.setPhysicalPostalCode(parser.getString());
                                    break;
                                case "latitude":
                                    sVO.setLatitude(parser.getString());
                                    break;
                                case "longitude":
                                    sVO.setLongitude(parser.getString());
                                    break;
                                case "timezone":
                                    sVO.setTimezone(parser.getString());
                                    break;
                                case "shipAddress1":
                                    sVO.setShipAddress1(parser.getString());
                                    break;
                                case "shipAddress2":
                                    sVO.setShipAddress2(parser.getString());
                                    break;
                                case "shipCity":
                                    sVO.setShipCity(parser.getString());
                                    break;
                                case "shipStateProvince":
                                    sVO.setShipStateProvince(parser.getString());
                                    break;
                                case "shipPostalCode":
                                    sVO.setShipPostalCode(parser.getString());
                                    break;
                                case "billingAddress1":
                                    sVO.setBillingAddress1(parser.getString());
                                    break;
                                case "billingAddress2":
                                    sVO.setBillingAddress2(parser.getString());
                                    break;
                                case "billingCity":
                                    sVO.setBillingCity(parser.getString());
                                    break;
                                case "billingStateProvince":
                                    sVO.setBillingStateProvince(parser.getString());
                                    break;
                                case "billingPostalCode":
                                    sVO.setBillingPostalCode(parser.getString());
                                    break;
                                case "industry":
                                    sVO.setIndustry(parser.getString());
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            System.out.println("Exception - " + e);
            list = new ArrayList();
        }
        siteVO.setSiteId(list.get(0).getSiteId());
        System.out.println("created SiteId - " + siteVO.getSiteId());
    }
    
    /**
     * Test of createSite method, of class SiteController.
     * @throws java.lang.Exception
     */
    @Test
    public void test11_CreateSite() throws Exception {
        System.out.println("createSite");
        String content = "{"
                + "\"customerAccount\" : \"TEST ACCNT 444\", "
                + "\"siteName\" : \"Test Site 444\", "
                + "\"role\" : \"Test Role 444\", "
                + "\"region\" : \"Test region22\", "
                + "\"country\" : \"Test country22\", "
                + "\"physicalAddress1\" : \"Test physicalAddress1\", "
                + "\"physicalAddress2\" : \"Test physicalAddress2\", "
                + "\"physicalCity\" : \"Test physicalCity\", "
                + "\"physicalStateProvince\" : \"Test physicalStateProvince\", "
                + "\"physicalPostalCode\" : \"Test 324567\", "
                + "\"latitude\" : \"Test 11122\", "
                + "\"longitude\" : \"Test 222111\", "
                + "\"timezone\" : \"Test EST\", "
                + "\"industry\" : \"Test industry\", "
                + "\"shipAddress1\" : \"Test shipAddress1\", "
                + "\"shipAddress2\" : \"Test shipAddress2\", "
                + "\"shipCity\" : \"Test shipCity\", "
                + "\"shipStateProvince\" : \"Test shipStateProvince\", "
                + "\"shipPostalCode\" : \"Test 345678\", "
                + "\"billingAddress1\" : \"Test billingAddress1\", "
                + "\"billingAddress2\" : \"Test billingAddress2\", "
                + "\"billingCity\" : \"Test billingCity\", "
                + "\"billingStateProvince\" : \"Test billingStateProvince\", "
                + "\"billingPostalCode\" : \"327890\""
                + "}\n";
        String expResult = "{\"outcome\":\"New Site created successfully.\"}";
        String result = instance.createSite(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of createSite method, of class SiteController.Site Name is not provided
     * @throws java.lang.Exception
     */
    @Test
    public void test12_CreateSite() throws Exception {
        System.out.println("createSite");
        String content = "{\"customerAccount\":\"77777\","
                + "\"region\":\"NORTH AMERICA\",\"country\":\"UNITED STATES\","
                + "\"physicalAddress1\":\"4820 Executive Park Ct\",\"physicalAddress2\":\"\","
                + "\"physicalCity\":\"Jacksonville\",\"physicalStateProvince\":\"FL\","
                + "\"physicalPostalCode\":\"32216\",\"latitude\":\"30.252254\","
                + "\"longitude\":\"-81.602805\",\"timezone\":\"Pacific/Niue (UTC -11:00)\","
                + "\"industry\":\"Production\",\"shipAddress1\":\"4820 Executive Park Ct\","
                + "\"shipAddress2\":\"\",\"shipCity\":\"Jacksonville\",\"shipStateProvince\":\"FL\","
                + "\"shipPostalCode\":\"32216\",\"billingAddress1\":\"4820 Executive Park Ct\","
                + "\"billingAddress2\":\"\",\"billingCity\":\"Jacksonville\",\"billingStateProvince\":\"FL\","
                + "\"billingPostalCode\":\"32216\",\"siteContactList\":[{\"customerAccount\":\"77777\","
                + "\"siteName\":\"UPTIME SITE 1\",\"role\":\"IT\",\"contactName\":\"TEST NAME 1\","
                + "\"contactTitle\":\"Mr.\",\"contactPhone\":\"5555544441\",\"contactEmail\":\"test1@test.com\"},"
                + "{\"customerAccount\":\"77777\",\"siteName\":\"UPTIME SITE 1\","
                + "\"role\":\"REPORTS\",\"contactName\":\"TEST NAME 2\",\"contactTitle\":\"Mr.\","
                + "\"contactPhone\":\"5555544442\",\"contactEmail\":\"test2@test.com\"}]"
                + "}\n";
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createSite(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of createSite method, of class SiteController.Role is not provided in Site Contact
     * @throws java.lang.Exception
     */
    @Test
    public void test13_CreateSite() throws Exception {
        System.out.println("createSite");
        String content = "{\"customerAccount\":\"77777\",\"siteName\":\"UPTIME SITE 1\","
                + "\"region\":\"NORTH AMERICA\",\"country\":\"UNITED STATES\","
                + "\"physicalAddress1\":\"4820 Executive Park Ct\",\"physicalAddress2\":\"\","
                + "\"physicalCity\":\"Jacksonville\",\"physicalStateProvince\":\"FL\","
                + "\"physicalPostalCode\":\"32216\",\"latitude\":\"30.252254\","
                + "\"longitude\":\"-81.602805\",\"timezone\":\"Pacific/Niue (UTC -11:00)\","
                + "\"industry\":\"Production\",\"shipAddress1\":\"4820 Executive Park Ct\","
                + "\"shipAddress2\":\"\",\"shipCity\":\"Jacksonville\",\"shipStateProvince\":\"FL\","
                + "\"shipPostalCode\":\"32216\",\"billingAddress1\":\"4820 Executive Park Ct\","
                + "\"billingAddress2\":\"\",\"billingCity\":\"Jacksonville\",\"billingStateProvince\":\"FL\","
                + "\"billingPostalCode\":\"32216\",\"siteContactList\":[{\"customerAccount\":\"77777\","
                + "\"siteName\":\"UPTIME SITE 1\",\"role\":\"IT\",\"contactName\":\"TEST NAME 1\","
                + "\"contactTitle\":\"Mr.\",\"contactPhone\":\"5555544441\",\"contactEmail\":\"test1@test.com\"},"
                + "{\"customerAccount\":\"77777\",\"siteName\":\"UPTIME SITE 1\","
                + "\"role\":\"\",\"contactName\":\"TEST NAME 2\",\"contactTitle\":\"Mr.\","
                + "\"contactPhone\":\"5555544442\",\"contactEmail\":\"test2@test.com\"}]"
                + "}\n";
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createSite(content);
        assertEquals(expResult, result);
    }
            
    /**
     * Test of getSiteCustomerByCustomer method, of class SiteController.
     * @throws java.lang.Exception
     */
    @Test
    public void test121_GetSiteCustomerByCustomer() throws Exception {
        System.out.println("getSiteCustomerByCustomer");
        String expResult = "{\"customer\":\"" + siteVO.getCustomerAccount() + "\",\"SiteCustomers\":[{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"siteName\":\"" + siteVO.getSiteName()+ "\",\"region\":\"" + siteVO.getRegion()+ "\",\"country\":\"" + siteVO.getCountry()+ "\",\"physicalAddress1\":\"" + siteVO.getPhysicalAddress1() + "\",\"physicalAddress2\":\"\",\"physicalCity\":\"" + siteVO.getPhysicalCity() + "\",\"physicalStateProvince\":\"" + siteVO.getPhysicalStateProvince() + "\",\"physicalPostalCode\":\"" + siteVO.getPhysicalPostalCode() + "\",\"latitude\":\"" + siteVO.getLatitude() + "\",\"longitude\":\"" + siteVO.getLongitude() + "\",\"timezone\":\"" + siteVO.getTimezone() + "\",\"industry\":\"" + siteVO.getIndustry() + "\",\"shipAddress1\":\"" + siteVO.getShipAddress1() + "\",\"shipAddress2\":\"\",\"shipCity\":\"" + siteVO.getShipCity() + "\",\"shipStateProvince\":\"" + siteVO.getShipStateProvince() + "\",\"shipPostalCode\":\"" + siteVO.getShipPostalCode() + "\",\"billingAddress1\":\"" + siteVO.getBillingAddress1() + "\",\"billingAddress2\":\"\",\"billingCity\":\"" + siteVO.getBillingCity() + "\",\"billingStateProvince\":\"" + siteVO.getBillingStateProvince() + "\",\"billingPostalCode\":\"" + siteVO.getBillingPostalCode() + "\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getSiteCustomerByCustomer(siteVO.getCustomerAccount());
        System.out.println("test12 expResult - " + expResult);
        System.out.println("test12    result - " + result);
        assertEquals(expResult, result);
    }
        
    /**
     * Test of getSiteCustomerByCustomer method, of class SiteController.Provided a customer account that does not exist
     * @throws java.lang.Exception
     */
    @Test
    public void test122_GetSiteCustomerByCustomer() throws Exception {
        System.out.println("getSiteCustomerByCustomer");
        String expResult = "{\"outcome\":\"Error: SiteCustomer Data not found in database.\"}";
        String result = instance.getSiteCustomerByCustomer("77778");
        System.out.println("test122 expResult - " + expResult);
        System.out.println("test122    result - " + result);
        assertEquals(expResult, result);
    }
        
    /**
     * Test of getSiteCustomerByCustomer method, of class SiteController.Provided a customer account that does not exist
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test123_GetSiteCustomerByCustomer() throws Exception {
        System.out.println("getSiteCustomerByCustomer");
        fail(instance.getSiteCustomerByCustomer(""));
    }
        
    /**
     * Test of getSiteCustomerByPK method, of class SiteController.
     * @throws java.lang.Exception
     */
    @Test
    public void test131_GetSiteCustomerByPK() throws Exception {
        System.out.println("getSiteCustomerByPK");
        String expResult = "{\"customer\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"SiteCustomers\":[{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"siteName\":\"" + siteVO.getSiteName()+ "\",\"region\":\"" + siteVO.getRegion()+ "\",\"country\":\"" + siteVO.getCountry()+ "\",\"physicalAddress1\":\"" + siteVO.getPhysicalAddress1() + "\",\"physicalAddress2\":\"\",\"physicalCity\":\"" + siteVO.getPhysicalCity() + "\",\"physicalStateProvince\":\"" + siteVO.getPhysicalStateProvince() + "\",\"physicalPostalCode\":\"" + siteVO.getPhysicalPostalCode() + "\",\"latitude\":\"" + siteVO.getLatitude() + "\",\"longitude\":\"" + siteVO.getLongitude() + "\",\"timezone\":\"" + siteVO.getTimezone() + "\",\"industry\":\"" + siteVO.getIndustry() + "\",\"shipAddress1\":\"" + siteVO.getShipAddress1() + "\",\"shipAddress2\":\"\",\"shipCity\":\"" + siteVO.getShipCity() + "\",\"shipStateProvince\":\"" + siteVO.getShipStateProvince() + "\",\"shipPostalCode\":\"" + siteVO.getShipPostalCode() + "\",\"billingAddress1\":\"" + siteVO.getBillingAddress1() + "\",\"billingAddress2\":\"\",\"billingCity\":\"" + siteVO.getBillingCity() + "\",\"billingStateProvince\":\"" + siteVO.getBillingStateProvince() + "\",\"billingPostalCode\":\"" + siteVO.getBillingPostalCode() + "\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getSiteCustomerByPK(siteVO.getCustomerAccount(), siteVO.getSiteId().toString());
        System.out.println("test131 expResult - " + expResult);
        System.out.println("test131    result - " + result);
        assertEquals(expResult, result);
    }
        
    /**
     * Test of getSiteCustomerByPK method, of class SiteController.
     * @throws java.lang.Exception
     */
    @Test
    public void test132_GetSiteCustomerByPK() throws Exception {
        System.out.println("getSiteCustomerByPK");
        String expResult = "{\"outcome\":\"Error: SiteCustomer Data not found in database.\"}";
        String result = instance.getSiteCustomerByPK("88888", siteVO.getSiteId().toString());
        System.out.println("test132 expResult - " + expResult);
        System.out.println("test132    result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getSiteCustomerByPK method, of class SiteController.
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test133_GetSiteCustomerByPK() throws Exception {
        System.out.println("getSiteCustomerByPK");
        fail(instance.getSiteCustomerByPK(siteVO.getCustomerAccount(), null));
    }
    
    /**
     * Test of getSiteContactsByCustomerSite method, of class SiteController.
     * @throws java.lang.Exception
     */
    @Test
    public void test141_GetSiteContactsByCustomerSite() throws Exception {
        System.out.println("getSiteContactsByCustomerSite");
        String expResult = "{\"customer\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"SiteContacts\":[{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"role\":\"" + siteVO.getSiteContactList().get(0).getRole() + "\",\"siteName\":\"" + siteVO.getSiteContactList().get(0).getSiteName() + "\",\"contactName\":\"" + siteVO.getSiteContactList().get(0).getContactName() + "\",\"contactTitle\":\"" + siteVO.getSiteContactList().get(0).getContactTitle() + "\",\"contactPhone\":\"" + siteVO.getSiteContactList().get(0).getContactPhone() + "\",\"contactEmail\":\"" + siteVO.getSiteContactList().get(0).getContactEmail() + "\"},{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"role\":\"" + siteVO.getSiteContactList().get(1).getRole() + "\",\"siteName\":\"" + siteVO.getSiteContactList().get(1).getSiteName() + "\",\"contactName\":\"" + siteVO.getSiteContactList().get(1).getContactName() + "\",\"contactTitle\":\"" + siteVO.getSiteContactList().get(1).getContactTitle() + "\",\"contactPhone\":\"" + siteVO.getSiteContactList().get(1).getContactPhone() + "\",\"contactEmail\":\"" + siteVO.getSiteContactList().get(1).getContactEmail() + "\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getSiteContactsByCustomerSite(siteVO.getCustomerAccount(), siteVO.getSiteId().toString());
        System.out.println("test141 expResult - " + expResult);
        System.out.println("test141    result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getSiteContactsByCustomerSite method, of class SiteController.Provided a Customer Account that does not exist
     * @throws java.lang.Exception
     */
    @Test
    public void test142_GetSiteContactsByCustomerSite() throws Exception {
        System.out.println("getSiteContactsByCustomerSite");
        String expResult = "{\"outcome\":\"Error: SiteContacts Data not found in database.\"}";
        String result = instance.getSiteContactsByCustomerSite("88888", siteVO.getSiteId().toString());
        System.out.println("test142 expResult - " + expResult);
        System.out.println("test142    result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getSiteContactsByCustomerSite method, of class SiteController.Provided null siteId
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test143_GetSiteContactsByCustomerSite() throws Exception {
        System.out.println("getSiteContactsByCustomerSite");
        fail(instance.getSiteContactsByCustomerSite("88888", null));
    }
    
    /**
     * Test of getSiteContactsByCustomerSite method, of class SiteController.Provided invalid siteId
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test144_GetSiteContactsByCustomerSite() throws Exception {
        System.out.println("getSiteContactsByCustomerSite");
        fail(instance.getSiteContactsByCustomerSite("88888", "xfcgvh"));
    }
    
    /**
     * Test of getAllByCustomerSiteRegionCountry method, of class SiteController.
     * @throws java.lang.Exception
     */
    @Test
    public void test151_GetAllByCustomerSiteRegionCountry() throws Exception {
        System.out.println("getAllByCustomerSiteRegionCountry");
        String expResult = "{\"customer\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"region\":\"" + siteVO.getRegion() + "\",\"country\":\"" + siteVO.getCountry() + "\",\"SiteContacts\":[{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"role\":\"" + siteVO.getSiteContactList().get(0).getRole() + "\",\"siteName\":\"" + siteVO.getSiteContactList().get(0).getSiteName() + "\",\"contactName\":\"" + siteVO.getSiteContactList().get(0).getContactName() + "\",\"contactTitle\":\"" + siteVO.getSiteContactList().get(0).getContactTitle() + "\",\"contactPhone\":\"" + siteVO.getSiteContactList().get(0).getContactPhone() + "\",\"contactEmail\":\"" + siteVO.getSiteContactList().get(0).getContactEmail() + "\"},{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"role\":\"" + siteVO.getSiteContactList().get(1).getRole() + "\",\"siteName\":\"" + siteVO.getSiteContactList().get(1).getSiteName() + "\",\"contactName\":\"" + siteVO.getSiteContactList().get(1).getContactName() + "\",\"contactTitle\":\"" + siteVO.getSiteContactList().get(1).getContactTitle() + "\",\"contactPhone\":\"" + siteVO.getSiteContactList().get(1).getContactPhone() + "\",\"contactEmail\":\"" + siteVO.getSiteContactList().get(1).getContactEmail() + "\"}],\"SiteCustomers\":[{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"siteName\":\"" + siteVO.getSiteName() + "\",\"region\":\"" + siteVO.getRegion() + "\",\"country\":\"" + siteVO.getCountry() + "\",\"physicalAddress1\":\"" + siteVO.getPhysicalAddress1() + "\",\"physicalAddress2\":\"\",\"physicalCity\":\"" + siteVO.getPhysicalCity() + "\",\"physicalStateProvince\":\"" + siteVO.getPhysicalStateProvince() + "\",\"physicalPostalCode\":\"" + siteVO.getPhysicalPostalCode() + "\",\"latitude\":\"" + siteVO.getLatitude() + "\",\"longitude\":\"" + siteVO.getLongitude() + "\",\"timezone\":\"" + siteVO.getTimezone() + "\",\"industry\":\"" + siteVO.getIndustry() + "\",\"shipAddress1\":\"" + siteVO.getShipAddress1()+ "\",\"shipAddress2\":\"\",\"shipCity\":\"" + siteVO.getShipCity() + "\",\"shipStateProvince\":\"" + siteVO.getShipStateProvince() + "\",\"shipPostalCode\":\"" + siteVO.getShipPostalCode() + "\",\"billingAddress1\":\"" + siteVO.getBillingAddress1() + "\",\"billingAddress2\":\"\",\"billingCity\":\"" + siteVO.getBillingCity() + "\",\"billingStateProvince\":\"" + siteVO.getBillingStateProvince() + "\",\"billingPostalCode\":\"" + siteVO.getBillingPostalCode() + "\"}],\"SiteRegionCountry\":[{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"region\":\"" + siteVO.getRegion() + "\",\"country\":\"" + siteVO.getCountry() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"siteName\":\"" + siteVO.getSiteName() + "\",\"physicalAddress1\":\"" + siteVO.getPhysicalAddress1() + "\",\"physicalAddress2\":\"\",\"physicalCity\":\"" + siteVO.getPhysicalCity()+ "\",\"physicalStateProvince\":\"" + siteVO.getPhysicalStateProvince() + "\",\"physicalPostalCode\":\"" + siteVO.getPhysicalPostalCode() + "\",\"latitude\":\"" + siteVO.getLatitude() + "\",\"longitude\":\"" + siteVO.getLongitude() + "\",\"timezone\":\"" + siteVO.getTimezone() + "\",\"industry\":\"" + siteVO.getIndustry() + "\",\"shipAddress1\":\"" + siteVO.getShipAddress1() + "\",\"shipAddress2\":\"\",\"shipCity\":\"" + siteVO.getShipCity() + "\",\"shipStateProvince\":\"" + siteVO.getShipStateProvince() + "\",\"shipPostalCode\":\"" + siteVO.getShipPostalCode() + "\",\"billingAddress1\":\"" + siteVO.getBillingAddress1() + "\",\"billingAddress2\":\"\",\"billingCity\":\"" + siteVO.getBillingCity() + "\",\"billingStateProvince\":\"" + siteVO.getBillingStateProvince() + "\",\"billingPostalCode\":\"" + siteVO.getBillingPostalCode() + "\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAllByCustomerSiteRegionCountry(siteVO.getCustomerAccount(), siteVO.getRegion(), siteVO.getCountry(), siteVO.getSiteId().toString(), true, true, true);
        System.out.println("test151 expResult - " + expResult);
        System.out.println("test151    result - " + result);    
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getAllByCustomerSiteRegionCountry method, of class SiteController.Provided a Customer Account that does not exist
     * @throws java.lang.Exception
     */
    @Test
    public void test152_GetAllByCustomerSiteRegionCountry() throws Exception {
        System.out.println("getAllByCustomerSiteRegionCountry");
        String expResult = "{\"outcome\":\"Error: SiteCustomer Data not found in database.\"}";
        String result = instance.getAllByCustomerSiteRegionCountry("55555", siteVO.getRegion(), siteVO.getCountry(), siteVO.getSiteId().toString(), true, true, true);
        System.out.println("test152 expResult - " + expResult);
        System.out.println("test152    result - " + result);    
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getAllByCustomerSiteRegionCountry method, of class SiteController.Provided null Customer Account
     * @throws java.lang.Exception 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test153_GetAllByCustomerSiteRegionCountry() throws Exception {
        System.out.println("getAllByCustomerSiteRegionCountry");
        fail(instance.getAllByCustomerSiteRegionCountry("", siteVO.getRegion(), siteVO.getCountry(), siteVO.getSiteId().toString(), true, true, true));
    }
    
    /**
     * Test of getAllByCustomerSiteRegionCountry method, of class SiteController.Provided null siteId
     * @throws java.lang.Exception 
     */
    @Test(expected = NullPointerException.class)
    public void test154_GetAllByCustomerSiteRegionCountry() throws Exception {
        System.out.println("getAllByCustomerSiteRegionCountry");
        fail(instance.getAllByCustomerSiteRegionCountry(siteVO.getCustomerAccount(), siteVO.getRegion(), siteVO.getCountry(), null, true, true, true));
    }
    
    /**
     * Test of getAllByCustomerSiteRegionCountry method, of class SiteController.
     * @throws java.lang.Exception
     */
    @Test
    public void test160_GetAllByCustomerSiteRegionCountry() throws Exception {
        System.out.println("getAllByCustomerSiteRegionCountry");
        String expResult = "{\"customer\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"region\":\"" + siteVO.getRegion() + "\",\"country\":\"" + siteVO.getCountry() + "\",\"SiteContacts\":[{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"role\":\"" + siteVO.getSiteContactList().get(0).getRole() + "\",\"siteName\":\"" + siteVO.getSiteContactList().get(0).getSiteName() + "\",\"contactName\":\"" + siteVO.getSiteContactList().get(0).getContactName() + "\",\"contactTitle\":\"" + siteVO.getSiteContactList().get(0).getContactTitle() + "\",\"contactPhone\":\"" + siteVO.getSiteContactList().get(0).getContactPhone() + "\",\"contactEmail\":\"" + siteVO.getSiteContactList().get(0).getContactEmail() + "\"},{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"role\":\"" + siteVO.getSiteContactList().get(1).getRole() + "\",\"siteName\":\"" + siteVO.getSiteContactList().get(1).getSiteName() + "\",\"contactName\":\"" + siteVO.getSiteContactList().get(1).getContactName() + "\",\"contactTitle\":\"" + siteVO.getSiteContactList().get(1).getContactTitle() + "\",\"contactPhone\":\"" + siteVO.getSiteContactList().get(1).getContactPhone() + "\",\"contactEmail\":\"" + siteVO.getSiteContactList().get(1).getContactEmail() + "\"}],\"SiteCustomers\":[{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"siteName\":\"" + siteVO.getSiteName() + "\",\"region\":\"" + siteVO.getRegion() + "\",\"country\":\"" + siteVO.getCountry() + "\",\"physicalAddress1\":\"" + siteVO.getPhysicalAddress1() + "\",\"physicalAddress2\":\"\",\"physicalCity\":\"" + siteVO.getPhysicalCity() + "\",\"physicalStateProvince\":\"" + siteVO.getPhysicalStateProvince() + "\",\"physicalPostalCode\":\"" + siteVO.getPhysicalPostalCode() + "\",\"latitude\":\"" + siteVO.getLatitude() + "\",\"longitude\":\"" + siteVO.getLongitude() + "\",\"timezone\":\"" + siteVO.getTimezone() + "\",\"industry\":\"" + siteVO.getIndustry() + "\",\"shipAddress1\":\"" + siteVO.getShipAddress1()+ "\",\"shipAddress2\":\"\",\"shipCity\":\"" + siteVO.getShipCity() + "\",\"shipStateProvince\":\"" + siteVO.getShipStateProvince() + "\",\"shipPostalCode\":\"" + siteVO.getShipPostalCode() + "\",\"billingAddress1\":\"" + siteVO.getBillingAddress1() + "\",\"billingAddress2\":\"\",\"billingCity\":\"" + siteVO.getBillingCity() + "\",\"billingStateProvince\":\"" + siteVO.getBillingStateProvince() + "\",\"billingPostalCode\":\"" + siteVO.getBillingPostalCode() + "\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAllByCustomerSiteRegionCountry(siteVO.getCustomerAccount(), siteVO.getRegion(), siteVO.getCountry(), siteVO.getSiteId().toString(), true, true, false);
        System.out.println("test160 expResult - " + expResult);
        System.out.println("test160    result - " + result);    
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getAllByCustomerSiteRegionCountry method, of class SiteController.Provided a Customer Account that does not exist
     * @throws java.lang.Exception
     */
    @Test
    public void test161_GetAllByCustomerSiteRegionCountry() throws Exception {
        System.out.println("getAllByCustomerSiteRegionCountry");
        String expResult = "{\"outcome\":\"Error: SiteCustomer Data not found in database.\"}";
        String result = instance.getAllByCustomerSiteRegionCountry("55555", siteVO.getRegion(), siteVO.getCountry(), siteVO.getSiteId().toString(), true, true, false);
        System.out.println("test161 expResult - " + expResult);
        System.out.println("test161    result - " + result);    
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getAllByCustomerSiteRegionCountry method, of class SiteController.Provided null Customer Account
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test162_GetAllByCustomerSiteRegionCountry() throws Exception {
        System.out.println("getAllByCustomerSiteRegionCountry");
        fail(instance.getAllByCustomerSiteRegionCountry("", siteVO.getRegion(), siteVO.getCountry(), siteVO.getSiteId().toString(), true, true, false));
    }
    /**
     * Test of getAllByCustomerSiteRegionCountry method, of class SiteController.Provided null siteId
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test163_GetAllByCustomerSiteRegionCountry() throws Exception {
        System.out.println("getAllByCustomerSiteRegionCountry");
        fail(instance.getAllByCustomerSiteRegionCountry(siteVO.getCustomerAccount(), siteVO.getRegion(), siteVO.getCountry(), null, true, true, false));
    }
    
    /**
     * Test of getAllByCustomerSiteRegionCountry method, of class SiteController.
     * @throws java.lang.Exception
     */
    @Test
    public void test170_GetAllByCustomerSiteRegionCountry() throws Exception {
        System.out.println("getAllByCustomerSiteRegionCountry");
        String expResult = "{\"customer\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"region\":\"" + siteVO.getRegion() + "\",\"country\":\"" + siteVO.getCountry() + "\",\"SiteContacts\":[{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"role\":\"" + siteVO.getSiteContactList().get(0).getRole() + "\",\"siteName\":\"" + siteVO.getSiteContactList().get(0).getSiteName() + "\",\"contactName\":\"" + siteVO.getSiteContactList().get(0).getContactName() + "\",\"contactTitle\":\"" + siteVO.getSiteContactList().get(0).getContactTitle() + "\",\"contactPhone\":\"" + siteVO.getSiteContactList().get(0).getContactPhone() + "\",\"contactEmail\":\"" + siteVO.getSiteContactList().get(0).getContactEmail() + "\"},{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"role\":\"" + siteVO.getSiteContactList().get(1).getRole() + "\",\"siteName\":\"" + siteVO.getSiteContactList().get(1).getSiteName() + "\",\"contactName\":\"" + siteVO.getSiteContactList().get(1).getContactName() + "\",\"contactTitle\":\"" + siteVO.getSiteContactList().get(1).getContactTitle() + "\",\"contactPhone\":\"" + siteVO.getSiteContactList().get(1).getContactPhone() + "\",\"contactEmail\":\"" + siteVO.getSiteContactList().get(1).getContactEmail() + "\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAllByCustomerSiteRegionCountry(siteVO.getCustomerAccount(), siteVO.getRegion(), siteVO.getCountry(), siteVO.getSiteId().toString(), true, false, false);
        System.out.println("test170 expResult - " + expResult);
        System.out.println("test170    result - " + result);    
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getAllByCustomerSiteRegionCountry method, of class SiteController.Provided a Customer Account that does not exist
     * @throws java.lang.Exception
     */
    @Test
    public void test171_GetAllByCustomerSiteRegionCountry() throws Exception {
        System.out.println("getAllByCustomerSiteRegionCountry");
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAllByCustomerSiteRegionCountry("55555", siteVO.getRegion(), siteVO.getCountry(), siteVO.getSiteId().toString(), true, false, false);
        System.out.println("test171 expResult - " + expResult);
        System.out.println("test171    result - " + result);    
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getAllByCustomerSiteRegionCountry method, of class SiteController.Provided null Customer Account
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test172_GetAllByCustomerSiteRegionCountry() throws Exception {
        System.out.println("getAllByCustomerSiteRegionCountry");
        fail(instance.getAllByCustomerSiteRegionCountry("", siteVO.getRegion(), siteVO.getCountry(), siteVO.getSiteId().toString(), true, false, false));
    }
    
    /**
     * Test of getAllByCustomerSiteRegionCountry method, of class SiteController.Provided null siteId
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test173_GetAllByCustomerSiteRegionCountry() throws Exception {
        System.out.println("getAllByCustomerSiteRegionCountry");
        fail(instance.getAllByCustomerSiteRegionCountry(siteVO.getCustomerAccount(), siteVO.getRegion(), siteVO.getCountry(), null, true, false, false));
    }
    
    /**
     * Test of getAllByCustomerSiteRegionCountry method, of class SiteController.Provided invalid siteId
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test174_GetAllByCustomerSiteRegionCountry() throws Exception {
        System.out.println("getAllByCustomerSiteRegionCountry");
        fail(instance.getAllByCustomerSiteRegionCountry("", siteVO.getRegion(), siteVO.getCountry(), "lllll", true, false, false));
    }
    
    /**
     * Test of getAllByCustomerSiteRegionCountry method, of class SiteController.
     * @throws java.lang.Exception
     */
    @Test
    public void test18_GetAllByCustomerSiteRegionCountry() throws Exception {
        System.out.println("getAllByCustomerSiteRegionCountry");
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAllByCustomerSiteRegionCountry(siteVO.getCustomerAccount(), siteVO.getRegion(), siteVO.getCountry(), siteVO.getSiteId().toString(), false, false, false);
        System.out.println("test18 expResult - " + expResult);
        System.out.println("test18    result - " + result);    
        assertEquals(expResult, result);
    }
   
    /**
     * Test of getAllByCustomerSiteRegionCountry method, of class SiteController.
     * @throws java.lang.Exception
     */
    @Test
    public void test19_GetAllByCustomerSiteRegionCountry() throws Exception {
        System.out.println("getAllByCustomerSiteRegionCountry");
        String expResult = "{\"customer\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"region\":\"" + siteVO.getRegion() + "\",\"country\":\"" + siteVO.getCountry() + "\",\"SiteCustomers\":[{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"siteName\":\"" + siteVO.getSiteName() + "\",\"region\":\"" + siteVO.getRegion() + "\",\"country\":\"" + siteVO.getCountry() + "\",\"physicalAddress1\":\"" + siteVO.getPhysicalAddress1() + "\",\"physicalAddress2\":\"\",\"physicalCity\":\"" + siteVO.getPhysicalCity() + "\",\"physicalStateProvince\":\"" + siteVO.getPhysicalStateProvince() + "\",\"physicalPostalCode\":\"" + siteVO.getPhysicalPostalCode() + "\",\"latitude\":\"" + siteVO.getLatitude() + "\",\"longitude\":\"" + siteVO.getLongitude() + "\",\"timezone\":\"" + siteVO.getTimezone() + "\",\"industry\":\"" + siteVO.getIndustry() + "\",\"shipAddress1\":\"" + siteVO.getShipAddress1()+ "\",\"shipAddress2\":\"\",\"shipCity\":\"" + siteVO.getShipCity() + "\",\"shipStateProvince\":\"" + siteVO.getShipStateProvince() + "\",\"shipPostalCode\":\"" + siteVO.getShipPostalCode() + "\",\"billingAddress1\":\"" + siteVO.getBillingAddress1() + "\",\"billingAddress2\":\"\",\"billingCity\":\"" + siteVO.getBillingCity() + "\",\"billingStateProvince\":\"" + siteVO.getBillingStateProvince() + "\",\"billingPostalCode\":\"" + siteVO.getBillingPostalCode() + "\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAllByCustomerSiteRegionCountry(siteVO.getCustomerAccount(), siteVO.getRegion(), siteVO.getCountry(), siteVO.getSiteId().toString(), false, true, false);
        System.out.println("test19 expResult - " + expResult);
        System.out.println("test19    result - " + result);    
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getAllByCustomerSiteRegionCountry method, of class SiteController.
     * @throws java.lang.Exception
     */
    @Test
    public void test20_GetAllByCustomerSiteRegionCountry() throws Exception {
        System.out.println("getAllByCustomerSiteRegionCountry");
        String expResult = "{\"customer\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"region\":\"" + siteVO.getRegion() + "\",\"country\":\"" + siteVO.getCountry() + "\",\"SiteRegionCountry\":[{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"region\":\"" + siteVO.getRegion() + "\",\"country\":\"" + siteVO.getCountry() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"siteName\":\"" + siteVO.getSiteName() + "\",\"physicalAddress1\":\"" + siteVO.getPhysicalAddress1() + "\",\"physicalAddress2\":\"\",\"physicalCity\":\"" + siteVO.getPhysicalCity()+ "\",\"physicalStateProvince\":\"" + siteVO.getPhysicalStateProvince() + "\",\"physicalPostalCode\":\"" + siteVO.getPhysicalPostalCode() + "\",\"latitude\":\"" + siteVO.getLatitude() + "\",\"longitude\":\"" + siteVO.getLongitude() + "\",\"timezone\":\"" + siteVO.getTimezone() + "\",\"industry\":\"" + siteVO.getIndustry() + "\",\"shipAddress1\":\"" + siteVO.getShipAddress1() + "\",\"shipAddress2\":\"\",\"shipCity\":\"" + siteVO.getShipCity() + "\",\"shipStateProvince\":\"" + siteVO.getShipStateProvince() + "\",\"shipPostalCode\":\"" + siteVO.getShipPostalCode() + "\",\"billingAddress1\":\"" + siteVO.getBillingAddress1() + "\",\"billingAddress2\":\"\",\"billingCity\":\"" + siteVO.getBillingCity() + "\",\"billingStateProvince\":\"" + siteVO.getBillingStateProvince() + "\",\"billingPostalCode\":\"" + siteVO.getBillingPostalCode() + "\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAllByCustomerSiteRegionCountry(siteVO.getCustomerAccount(), siteVO.getRegion(), siteVO.getCountry(), siteVO.getSiteId().toString(), false, false, true);
        System.out.println("test20 expResult - " + expResult);
        System.out.println("test20    result - " + result);    
        assertEquals(expResult, result);
    }

    /**
     * Test of getSiteContactsOrSiteCustomerByCustomerSite method, of class SiteController.
     * @throws java.lang.Exception
     */
    @Test
    public void test21_GetSiteContactsOrSiteCustomerByCustomerSite() throws Exception {
        System.out.println("getSiteContactsOrSiteCustomerByCustomerSite");
        String expResult = "{\"customer\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"SiteContacts\":[{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"role\":\"" + siteVO.getSiteContactList().get(0).getRole() + "\",\"siteName\":\"" + siteVO.getSiteContactList().get(0).getSiteName() + "\",\"contactName\":\"" + siteVO.getSiteContactList().get(0).getContactName() + "\",\"contactTitle\":\"" + siteVO.getSiteContactList().get(0).getContactTitle() + "\",\"contactPhone\":\"" + siteVO.getSiteContactList().get(0).getContactPhone() + "\",\"contactEmail\":\"" + siteVO.getSiteContactList().get(0).getContactEmail() + "\"},{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"role\":\"" + siteVO.getSiteContactList().get(1).getRole() + "\",\"siteName\":\"" + siteVO.getSiteContactList().get(1).getSiteName() + "\",\"contactName\":\"" + siteVO.getSiteContactList().get(1).getContactName() + "\",\"contactTitle\":\"" + siteVO.getSiteContactList().get(1).getContactTitle() + "\",\"contactPhone\":\"" + siteVO.getSiteContactList().get(1).getContactPhone() + "\",\"contactEmail\":\"" + siteVO.getSiteContactList().get(1).getContactEmail() + "\"}],\"SiteCustomers\":[{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"siteName\":\"" + siteVO.getSiteName() + "\",\"region\":\"" + siteVO.getRegion() + "\",\"country\":\"" + siteVO.getCountry() + "\",\"physicalAddress1\":\"" + siteVO.getPhysicalAddress1() + "\",\"physicalAddress2\":\"\",\"physicalCity\":\"" + siteVO.getPhysicalCity() + "\",\"physicalStateProvince\":\"" + siteVO.getPhysicalStateProvince() + "\",\"physicalPostalCode\":\"" + siteVO.getPhysicalPostalCode() + "\",\"latitude\":\"" + siteVO.getLatitude() + "\",\"longitude\":\"" + siteVO.getLongitude() + "\",\"timezone\":\"" + siteVO.getTimezone() + "\",\"industry\":\"" + siteVO.getIndustry() + "\",\"shipAddress1\":\"" + siteVO.getShipAddress1() + "\",\"shipAddress2\":\"\",\"shipCity\":\"" + siteVO.getShipCity() + "\",\"shipStateProvince\":\"" + siteVO.getShipStateProvince() + "\",\"shipPostalCode\":\"" + siteVO.getShipPostalCode() + "\",\"billingAddress1\":\"" + siteVO.getBillingAddress1() + "\",\"billingAddress2\":\"\",\"billingCity\":\"" + siteVO.getBillingCity() + "\",\"billingStateProvince\":\"" + siteVO.getBillingStateProvince() + "\",\"billingPostalCode\":\"" + siteVO.getBillingPostalCode() + "\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getSiteContactsOrSiteCustomerByCustomerSite(siteVO.getCustomerAccount(), siteVO.getSiteId().toString(), true, true);
        System.out.println("test21 expResult - " + expResult);
        System.out.println("test21    result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getSiteContactsOrSiteCustomerByCustomerSite method, of class SiteController.
     * @throws java.lang.Exception
     */
    @Test
    public void test22_GetSiteContactsOrSiteCustomerByCustomerSite() throws Exception {
        System.out.println("getSiteContactsOrSiteCustomerByCustomerSite");
        String expResult = "{\"customer\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"SiteContacts\":[{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"role\":\"" + siteVO.getSiteContactList().get(0).getRole() + "\",\"siteName\":\"" + siteVO.getSiteContactList().get(0).getSiteName() + "\",\"contactName\":\"" + siteVO.getSiteContactList().get(0).getContactName() + "\",\"contactTitle\":\"" + siteVO.getSiteContactList().get(0).getContactTitle() + "\",\"contactPhone\":\"" + siteVO.getSiteContactList().get(0).getContactPhone() + "\",\"contactEmail\":\"" + siteVO.getSiteContactList().get(0).getContactEmail() + "\"},{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"role\":\"" + siteVO.getSiteContactList().get(1).getRole() + "\",\"siteName\":\"" + siteVO.getSiteContactList().get(1).getSiteName() + "\",\"contactName\":\"" + siteVO.getSiteContactList().get(1).getContactName() + "\",\"contactTitle\":\"" + siteVO.getSiteContactList().get(1).getContactTitle() + "\",\"contactPhone\":\"" + siteVO.getSiteContactList().get(1).getContactPhone() + "\",\"contactEmail\":\"" + siteVO.getSiteContactList().get(1).getContactEmail() + "\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getSiteContactsOrSiteCustomerByCustomerSite(siteVO.getCustomerAccount(), siteVO.getSiteId().toString(), true, false);
        System.out.println("test22 expResult - " + expResult);
        System.out.println("test22    result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getSiteContactsOrSiteCustomerByCustomerSite method, of class SiteController.
     * @throws java.lang.Exception
     */
    @Test
    public void test23_GetSiteContactsOrSiteCustomerByCustomerSite() throws Exception {
        System.out.println("getSiteContactsOrSiteCustomerByCustomerSite");
        String expResult = "{\"customer\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"SiteCustomers\":[{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"siteName\":\"" + siteVO.getSiteName() + "\",\"region\":\"" + siteVO.getRegion() + "\",\"country\":\"" + siteVO.getCountry() + "\",\"physicalAddress1\":\"" + siteVO.getPhysicalAddress1() + "\",\"physicalAddress2\":\"\",\"physicalCity\":\"" + siteVO.getPhysicalCity() + "\",\"physicalStateProvince\":\"" + siteVO.getPhysicalStateProvince() + "\",\"physicalPostalCode\":\"" + siteVO.getPhysicalPostalCode() + "\",\"latitude\":\"" + siteVO.getLatitude() + "\",\"longitude\":\"" + siteVO.getLongitude() + "\",\"timezone\":\"" + siteVO.getTimezone() + "\",\"industry\":\"" + siteVO.getIndustry() + "\",\"shipAddress1\":\"" + siteVO.getShipAddress1() + "\",\"shipAddress2\":\"\",\"shipCity\":\"" + siteVO.getShipCity() + "\",\"shipStateProvince\":\"" + siteVO.getShipStateProvince() + "\",\"shipPostalCode\":\"" + siteVO.getShipPostalCode() + "\",\"billingAddress1\":\"" + siteVO.getBillingAddress1() + "\",\"billingAddress2\":\"\",\"billingCity\":\"" + siteVO.getBillingCity() + "\",\"billingStateProvince\":\"" + siteVO.getBillingStateProvince() + "\",\"billingPostalCode\":\"" + siteVO.getBillingPostalCode() + "\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getSiteContactsOrSiteCustomerByCustomerSite(siteVO.getCustomerAccount(), siteVO.getSiteId().toString(), false, true);
        System.out.println("test23 expResult - " + expResult);
        System.out.println("test23    result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getSiteContactsOrSiteCustomerByCustomerSite method, of class SiteController.set both the return boolean values to false
     * @throws java.lang.Exception
     */
    @Test
    public void test231_GetSiteContactsOrSiteCustomerByCustomerSite() throws Exception {
        System.out.println("getSiteContactsOrSiteCustomerByCustomerSite");
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getSiteContactsOrSiteCustomerByCustomerSite(siteVO.getCustomerAccount(), siteVO.getSiteId().toString(), false, false);
        System.out.println("test231 expResult - " + expResult);
        System.out.println("test231    result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getSiteContactsOrSiteCustomerByCustomerSite method, of class SiteController.Provided a Customer Account that does not exist
     * @throws java.lang.Exception
     */
    @Test
    public void test232_GetSiteContactsOrSiteCustomerByCustomerSite() throws Exception {
        System.out.println("getSiteContactsOrSiteCustomerByCustomerSite");
        String expResult = "{\"outcome\":\"Error: SiteCustomer Data not found in database.\"}";
        String result = instance.getSiteContactsOrSiteCustomerByCustomerSite("55555", siteVO.getSiteId().toString(), false, true);
        System.out.println("test232 expResult - " + expResult);
        System.out.println("test232    result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getSiteContactsOrSiteCustomerByCustomerSite method, of class SiteController.Provided null Customer Account
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test233_GetSiteContactsOrSiteCustomerByCustomerSite() throws Exception {
        System.out.println("getSiteContactsOrSiteCustomerByCustomerSite");
        fail(instance.getSiteContactsOrSiteCustomerByCustomerSite("", siteVO.getSiteId().toString(), false, true));
    }
    
    /**
     * Test of getSiteContactsOrSiteCustomerByCustomerSite method, of class SiteController.Provided null siteId
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test234_GetSiteContactsOrSiteCustomerByCustomerSite() throws Exception {
        System.out.println("getSiteContactsOrSiteCustomerByCustomerSite");
        fail(instance.getSiteContactsOrSiteCustomerByCustomerSite("55555", null, false, true));
    }
    
    /**
     * Test of getSiteContactsOrSiteCustomerByCustomerSite method, of class SiteController.Provided invalid siteId
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test235_GetSiteContactsOrSiteCustomerByCustomerSite() throws Exception {
        System.out.println("getSiteContactsOrSiteCustomerByCustomerSite");
        fail(instance.getSiteContactsOrSiteCustomerByCustomerSite("55555", "ttttt", false, true));
    }
    
    /**
     * Test of getSiteContactsByPK method, of class SiteController.Provided a Customer Account that does not exist
     * @throws java.lang.Exception
     */
    @Test
    public void test24_GetSiteContactsByPK() throws Exception {
        System.out.println("getSiteContactsByPK");
        String expResult = "{\"customer\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"role\":\"" + siteVO.getSiteContactList().get(0).getRole() + "\",\"SiteContacts\":[{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"role\":\"" + siteVO.getSiteContactList().get(0).getRole() + "\",\"siteName\":\"" + siteVO.getSiteContactList().get(0).getSiteName() + "\",\"contactName\":\"" + siteVO.getSiteContactList().get(0).getContactName() + "\",\"contactTitle\":\"" + siteVO.getSiteContactList().get(0).getContactTitle() + "\",\"contactPhone\":\"" + siteVO.getSiteContactList().get(0).getContactPhone() + "\",\"contactEmail\":\"" + siteVO.getSiteContactList().get(0).getContactEmail() + "\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getSiteContactsByPK(siteVO.getCustomerAccount(), siteVO.getSiteId().toString(), siteVO.getSiteContactList().get(0).getRole());
        System.out.println("test24 expResult - " + expResult);
        System.out.println("test24    result - " + result);        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getSiteContactsByPK method, of class SiteController.Provided a Customer Account that does not exist
     * @throws java.lang.Exception
     */
    @Test
    public void test25_GetSiteContactsByPK() throws Exception {
        System.out.println("getSiteContactsByPK");
        String expResult = "{\"outcome\":\"Error: SiteContacts Data not found in database.\"}";
        String result = instance.getSiteContactsByPK("TEST ACCNT 444", siteVO.getSiteId().toString(), siteVO.getSiteContactList().get(0).getRole());
        System.out.println("test25 expResult - " + expResult);
        System.out.println("test25    result - " + result);        
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getSiteContactsByPK method, of class SiteController.Provided null Customer Account
     * @throws java.lang.Exception 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test251_GetSiteContactsByPK() throws Exception {
        System.out.println("getSiteContactsByPK");
        fail(instance.getSiteContactsByPK("", siteVO.getSiteId().toString(), siteVO.getSiteContactList().get(0).getRole()));
    }
    
    /**
     * Test of getSiteContactsByPK method, of class SiteController.Provided null siteId
     * @throws java.lang.Exception 
     */
    @Test(expected = NullPointerException.class)
    public void test252_GetSiteContactsByPK() throws Exception {
        System.out.println("getSiteContactsByPK");
        fail(instance.getSiteContactsByPK(siteVO.getCustomerAccount(), null, siteVO.getSiteContactList().get(0).getRole()));
    }
    
    /**
     * Test of getSiteContactsByPK method, of class SiteController.Provided invalid siteId
     * @throws java.lang.Exception 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test253_GetSiteContactsByPK() throws Exception {
        System.out.println("getSiteContactsByPK");
        fail(instance.getSiteContactsByPK(siteVO.getCustomerAccount(), "kkkkkk", siteVO.getSiteContactList().get(0).getRole()));
    }
    
    /**
     * Test of getSiteContactsByPK method, of class SiteController.Provided null role
     * @throws java.lang.Exception 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test254_GetSiteContactsByPK() throws Exception {
        System.out.println("getSiteContactsByPK");
        fail(instance.getSiteContactsByPK(siteVO.getCustomerAccount(), siteVO.getSiteId().toString(), ""));
    }

    /**
     * Test of getSiteRegionCountryByPK method, of class SiteController.
     * @throws java.lang.Exception
     */
    @Test
    public void test26_GetSiteRegionCountryByPK() throws Exception {
        System.out.println("getSiteRegionCountryByPK");
        String expResult = "{\"customer\":\"" + siteVO.getCustomerAccount() + "\",\"region\":\"" + siteVO.getRegion() + "\",\"country\":\"" + siteVO.getCountry() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"SiteRegionCountry\":[{\"customerAccount\":\"" + siteVO.getCustomerAccount() + "\",\"region\":\"" + siteVO.getRegion() + "\",\"country\":\"" + siteVO.getCountry() + "\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"siteName\":\"" + siteVO.getSiteName() + "\",\"physicalAddress1\":\"" + siteVO.getPhysicalAddress1() + "\",\"physicalAddress2\":\"\",\"physicalCity\":\"" + siteVO.getPhysicalCity() + "\",\"physicalStateProvince\":\"" + siteVO.getPhysicalStateProvince() + "\",\"physicalPostalCode\":\"" + siteVO.getPhysicalPostalCode() + "\",\"latitude\":\"" + siteVO.getLatitude() + "\",\"longitude\":\"" + siteVO.getLongitude() + "\",\"timezone\":\"" + siteVO.getTimezone() + "\",\"industry\":\"" + siteVO.getIndustry() + "\",\"shipAddress1\":\"" + siteVO.getShipAddress1() + "\",\"shipAddress2\":\"\",\"shipCity\":\"" + siteVO.getShipCity() + "\",\"shipStateProvince\":\"" + siteVO.getShipStateProvince() + "\",\"shipPostalCode\":\"" + siteVO.getShipPostalCode() + "\",\"billingAddress1\":\"" + siteVO.getBillingAddress1() + "\",\"billingAddress2\":\"\",\"billingCity\":\"" + siteVO.getBillingCity() + "\",\"billingStateProvince\":\"" + siteVO.getBillingStateProvince() + "\",\"billingPostalCode\":\"" + siteVO.getBillingPostalCode() + "\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getSiteRegionCountryByPK(siteVO.getCustomerAccount(), siteVO.getRegion(), siteVO.getCountry(), siteVO.getSiteId().toString());
        System.out.println("test26 expResult - " + expResult);
        System.out.println("test26    result - " + result);    
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getSiteRegionCountryByPK method, of class SiteController.Provided a Customer Account that does not exist
     * @throws java.lang.Exception
     */
    @Test
    public void test27_GetSiteRegionCountryByPK() throws Exception {
        System.out.println("getSiteRegionCountryByPK");
        String expResult = "{\"outcome\":\"Error: SiteRegionCountry Data not found in database.\"}";
        String result = instance.getSiteRegionCountryByPK("55555", siteVO.getRegion(), siteVO.getCountry(), siteVO.getSiteId().toString());
        System.out.println("test27 expResult - " + expResult);
        System.out.println("test27    result - " + result);    
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getSiteRegionCountryByPK method, of class SiteController.Provided null Customer Account
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test271_GetSiteRegionCountryByPK() throws Exception {
        System.out.println("getSiteRegionCountryByPK");
        fail(instance.getSiteRegionCountryByPK("", siteVO.getRegion(), siteVO.getCountry(), siteVO.getSiteId().toString()));
    }
 
    /**
     * Test of getSiteRegionCountryByPK method, of class SiteController.Provided null siteId
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test272_GetSiteRegionCountryByPK() throws Exception {
        System.out.println("getSiteRegionCountryByPK");
        fail(instance.getSiteRegionCountryByPK(siteVO.getCustomerAccount(), siteVO.getRegion(), siteVO.getCountry(), null));
    }
 
    /**
     * Test of getSiteRegionCountryByPK method, of class SiteController.Provided invalid siteId
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test273_GetSiteRegionCountryByPK() throws Exception {
        System.out.println("getSiteRegionCountryByPK");
        fail(instance.getSiteRegionCountryByPK(siteVO.getCustomerAccount(), siteVO.getRegion(), siteVO.getCountry(), "kkkkkk"));
    }
 
    /**
     * Test of getSiteRegionCountryByCustomerRegionCountry method, of class SiteController.
     * @throws java.lang.Exception
     */
    @Test
    public void test28_GetSiteRegionCountryByCustomerRegionCountry() throws Exception {
        System.out.println("getSiteRegionCountryByCustomerRegionCountry");
        String expResult = "{\"customer\":\"" + siteVO.getCustomerAccount() + "\",\"region\":\"NORTH AMERICA\",\"country\":\"UNITED STATES\",\"SiteRegionCountry\":[{\"customerAccount\":\"77777\",\"region\":\"NORTH AMERICA\",\"country\":\"UNITED STATES\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"siteName\":\"UPTIME SITE 1\",\"physicalAddress1\":\"4820 Executive Park Ct\",\"physicalAddress2\":\"\",\"physicalCity\":\"Jacksonville\",\"physicalStateProvince\":\"FL\",\"physicalPostalCode\":\"32216\",\"latitude\":\"30.252254\",\"longitude\":\"-81.602805\",\"timezone\":\"Pacific/Niue (UTC -11:00)\",\"industry\":\"Production\",\"shipAddress1\":\"4820 Executive Park Ct\",\"shipAddress2\":\"\",\"shipCity\":\"Jacksonville\",\"shipStateProvince\":\"FL\",\"shipPostalCode\":\"32216\",\"billingAddress1\":\"4820 Executive Park Ct\",\"billingAddress2\":\"\",\"billingCity\":\"Jacksonville\",\"billingStateProvince\":\"FL\",\"billingPostalCode\":\"32216\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getSiteRegionCountryByCustomerRegionCountry(siteVO.getCustomerAccount(), siteVO.getRegion(), siteVO.getCountry());
        System.out.println("test28 expResult - " + expResult);
        System.out.println("test28    result - " + result); 
        assertEquals(expResult, result);
    }
 
    /**
     * Test of getSiteRegionCountryByCustomerRegionCountry method, of class SiteController.Provided a Customer Account that does not exist
     * @throws java.lang.Exception
     */
    @Test
    public void test29_GetSiteRegionCountryByCustomerRegionCountry() throws Exception {
        System.out.println("getSiteRegionCountryByCustomerRegionCountry");
        String expResult = "{\"outcome\":\"Error: SiteRegionCountry Data not found in database.\"}";
        String result = instance.getSiteRegionCountryByCustomerRegionCountry("88888", siteVO.getRegion(), siteVO.getCountry());
        System.out.println("test29 expResult - " + expResult);
        System.out.println("test29    result - " + result); 
        assertEquals(expResult, result);
    }
 
    /**
     * Test of getSiteRegionCountryByCustomerRegionCountry method, of class SiteController.Provided null Customer Account
     * @throws java.lang.Exception 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test291_GetSiteRegionCountryByCustomerRegionCountry() throws Exception {
        System.out.println("getSiteRegionCountryByCustomerRegionCountry");
        fail(instance.getSiteRegionCountryByCustomerRegionCountry("", siteVO.getRegion(), siteVO.getCountry()));
    }

    /**
     * Test of getSiteRegionCountryByCustomer method, of class SiteController.
     * @throws java.lang.Exception
     */
    @Test
    public void test30_GetSiteRegionCountryByCustomer() throws Exception {
        System.out.println("getSiteRegionCountryByCustomer");
        String expResult = "{\"customer\":\"77777\",\"SiteRegionCountry\":[{\"customerAccount\":\"77777\",\"region\":\"NORTH AMERICA\",\"country\":\"UNITED STATES\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"siteName\":\"UPTIME SITE 1\",\"physicalAddress1\":\"4820 Executive Park Ct\",\"physicalAddress2\":\"\",\"physicalCity\":\"Jacksonville\",\"physicalStateProvince\":\"FL\",\"physicalPostalCode\":\"32216\",\"latitude\":\"30.252254\",\"longitude\":\"-81.602805\",\"timezone\":\"Pacific/Niue (UTC -11:00)\",\"industry\":\"Production\",\"shipAddress1\":\"4820 Executive Park Ct\",\"shipAddress2\":\"\",\"shipCity\":\"Jacksonville\",\"shipStateProvince\":\"FL\",\"shipPostalCode\":\"32216\",\"billingAddress1\":\"4820 Executive Park Ct\",\"billingAddress2\":\"\",\"billingCity\":\"Jacksonville\",\"billingStateProvince\":\"FL\",\"billingPostalCode\":\"32216\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getSiteRegionCountryByCustomer(siteVO.getCustomerAccount());
        System.out.println("test30 expResult - " + expResult);
        System.out.println("test30    result - " + result); 
        assertEquals(expResult, result);
    }

    /**
     * Test of getSiteRegionCountryByCustomer method, of class SiteController.Provided a Customer Account that does not exist
     * @throws java.lang.Exception
     */
    @Test
    public void test31_GetSiteRegionCountryByCustomer() throws Exception {
        System.out.println("getSiteRegionCountryByCustomer");
        String expResult = "{\"outcome\":\"Error: SiteRegionCountry Data not found in database.\"}";
        String result = instance.getSiteRegionCountryByCustomer("55555");
        System.out.println("test31 expResult - " + expResult);
        System.out.println("test31    result - " + result); 
        assertEquals(expResult, result);
    }

    /**
     * Test of getSiteRegionCountryByCustomer method, of class SiteController.Provided null Customer Account
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test32_GetSiteRegionCountryByCustomer() throws Exception {
        System.out.println("getSiteRegionCountryByCustomer");
        fail(instance.getSiteRegionCountryByCustomer(""));
    }

    /**
     * Test of updateSite method, of class SiteController.
     */
    @Test
    public void test310_UpdateSite() {
        System.out.println("updateSite");
        String content = "{\"customerAccount\":\"77777\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"siteName\":\"UPTIME SITE 111\","
                + "\"region\":\"NORTH AMERICA\",\"country\":\"UNITED STATES\","
                + "\"physicalAddress1\":\"4820 Executive Park Ct\",\"physicalAddress2\":\"\","
                + "\"physicalCity\":\"Jacksonville\",\"physicalStateProvince\":\"FL\","
                + "\"physicalPostalCode\":\"32216\",\"latitude\":\"30.252254\","
                + "\"longitude\":\"-81.602805\",\"timezone\":\"Pacific/Niue (UTC -11:00)\","
                + "\"industry\":\"Production\",\"shipAddress1\":\"4820 Executive Park Ct\","
                + "\"shipAddress2\":\"\",\"shipCity\":\"Jacksonville\",\"shipStateProvince\":\"FL\","
                + "\"shipPostalCode\":\"32216\",\"billingAddress1\":\"4820 Executive Park Ct\","
                + "\"billingAddress2\":\"\",\"billingCity\":\"Jacksonville\",\"billingStateProvince\":\"FL\","
                + "\"billingPostalCode\":\"32216\",\"siteContactList\":[{\"customerAccount\":\"77777\","
                + "\"siteName\":\"UPTIME SITE 1\",\"role\":\"IT\",\"contactName\":\"TEST NAME 1\","
                + "\"contactTitle\":\"Mr.\",\"contactPhone\":\"5555544441\",\"contactEmail\":\"test1@test.com\"},"
                + "{\"customerAccount\":\"77777\",\"siteName\":\"UPTIME SITE 1\","
                + "\"role\":\"REPORTS\",\"contactName\":\"TEST NAME 2\",\"contactTitle\":\"Mr.\","
                + "\"contactPhone\":\"5555544442\",\"contactEmail\":\"test2@test.com\"}]"
                + "}\n";
        String expResult = "{\"outcome\":\"Updated Sites items successfully.\"}";
        String result = instance.updateSite(content);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of updateSite method, of class SiteController.
     * customer Account is not provided
     */
    @Test
    public void test312_UpdateSite() {
        System.out.println("updateSite");
        String content = "{\"customerAccount\":\"\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"siteName\":\"UPTIME SITE 111\","
                + "\"region\":\"NORTH AMERICA\",\"country\":\"UNITED STATES\","
                + "\"physicalAddress1\":\"4820 Executive Park Ct\",\"physicalAddress2\":\"\","
                + "\"physicalCity\":\"Jacksonville\",\"physicalStateProvince\":\"FL\","
                + "\"physicalPostalCode\":\"32216\",\"latitude\":\"30.252254\","
                + "\"longitude\":\"-81.602805\",\"timezone\":\"Pacific/Niue (UTC -11:00)\","
                + "\"industry\":\"Production\",\"shipAddress1\":\"4820 Executive Park Ct\","
                + "\"shipAddress2\":\"\",\"shipCity\":\"Jacksonville\",\"shipStateProvince\":\"FL\","
                + "\"shipPostalCode\":\"32216\",\"billingAddress1\":\"4820 Executive Park Ct\","
                + "\"billingAddress2\":\"\",\"billingCity\":\"Jacksonville\",\"billingStateProvince\":\"FL\","
                + "\"billingPostalCode\":\"32216\",\"siteContactList\":[{\"customerAccount\":\"77777\","
                + "\"siteName\":\"UPTIME SITE 1\",\"role\":\"IT\",\"contactName\":\"TEST NAME 1\","
                + "\"contactTitle\":\"Mr.\",\"contactPhone\":\"5555544441\",\"contactEmail\":\"test1@test.com\"},"
                + "{\"customerAccount\":\"77777\",\"siteName\":\"UPTIME SITE 1\","
                + "\"role\":\"REPORTS\",\"contactName\":\"TEST NAME 2\",\"contactTitle\":\"Mr.\","
                + "\"contactPhone\":\"5555544442\",\"contactEmail\":\"test2@test.com\"}]"
                + "}\n";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateSite(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteSite method, of class SiteController.
     */
    @Test
    public void test320_DeleteSite() {
        System.out.println("deleteSite");
        String content = "{\"customerAccount\":\"77777\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"siteName\":\"UPTIME SITE 111\","
                + "\"region\":\"NORTH AMERICA\",\"country\":\"UNITED STATES\","
                + "\"physicalAddress1\":\"4820 Executive Park Ct\",\"physicalAddress2\":\"\","
                + "\"physicalCity\":\"Jacksonville\",\"physicalStateProvince\":\"FL\","
                + "\"physicalPostalCode\":\"32216\",\"latitude\":\"30.252254\","
                + "\"longitude\":\"-81.602805\",\"timezone\":\"Pacific/Niue (UTC -11:00)\","
                + "\"industry\":\"Production\",\"shipAddress1\":\"4820 Executive Park Ct\","
                + "\"shipAddress2\":\"\",\"shipCity\":\"Jacksonville\",\"shipStateProvince\":\"FL\","
                + "\"shipPostalCode\":\"32216\",\"billingAddress1\":\"4820 Executive Park Ct\","
                + "\"billingAddress2\":\"\",\"billingCity\":\"Jacksonville\",\"billingStateProvince\":\"FL\","
                + "\"billingPostalCode\":\"32216\",\"siteContactList\":[{\"customerAccount\":\"77777\","
                + "\"siteName\":\"UPTIME SITE 1\",\"role\":\"IT\",\"contactName\":\"TEST NAME 1\","
                + "\"contactTitle\":\"Mr.\",\"contactPhone\":\"5555544441\",\"contactEmail\":\"test1@test.com\"},"
                + "{\"customerAccount\":\"77777\",\"siteName\":\"UPTIME SITE 1\","
                + "\"role\":\"REPORTS\",\"contactName\":\"TEST NAME 2\",\"contactTitle\":\"Mr.\","
                + "\"contactPhone\":\"5555544442\",\"contactEmail\":\"test2@test.com\"}]"
                + "}\n";
        String expResult = "{\"outcome\":\"Deleted Site items successfully.\"}";
        String result = instance.deleteSite(content);
        assertEquals(expResult, result);
    }
    

    /**
     * Test of deleteSite method, of class SiteController.
     * customer Account is not provided
     */
    @Test
    public void test321_DeleteSite() {
        System.out.println("deleteSite");
        String content = "{\"customerAccount\":\"\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"siteName\":\"UPTIME SITE 111\","
                + "\"region\":\"NORTH AMERICA\",\"country\":\"UNITED STATES\","
                + "\"physicalAddress1\":\"4820 Executive Park Ct\",\"physicalAddress2\":\"\","
                + "\"physicalCity\":\"Jacksonville\",\"physicalStateProvince\":\"FL\","
                + "\"physicalPostalCode\":\"32216\",\"latitude\":\"30.252254\","
                + "\"longitude\":\"-81.602805\",\"timezone\":\"Pacific/Niue (UTC -11:00)\","
                + "\"industry\":\"Production\",\"shipAddress1\":\"4820 Executive Park Ct\","
                + "\"shipAddress2\":\"\",\"shipCity\":\"Jacksonville\",\"shipStateProvince\":\"FL\","
                + "\"shipPostalCode\":\"32216\",\"billingAddress1\":\"4820 Executive Park Ct\","
                + "\"billingAddress2\":\"\",\"billingCity\":\"Jacksonville\",\"billingStateProvince\":\"FL\","
                + "\"billingPostalCode\":\"32216\",\"siteContactList\":[{\"customerAccount\":\"77777\","
                + "\"siteName\":\"UPTIME SITE 1\",\"role\":\"IT\",\"contactName\":\"TEST NAME 1\","
                + "\"contactTitle\":\"Mr.\",\"contactPhone\":\"5555544441\",\"contactEmail\":\"test1@test.com\"},"
                + "{\"customerAccount\":\"77777\",\"siteName\":\"UPTIME SITE 1\","
                + "\"role\":\"REPORTS\",\"contactName\":\"TEST NAME 2\",\"contactTitle\":\"Mr.\","
                + "\"contactPhone\":\"5555544442\",\"contactEmail\":\"test2@test.com\"}]"
                + "}\n";
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteSite(content);
        assertEquals(expResult, result);
    }
    

    /**
     * Test of deleteSite method, of class SiteController.
     * Site does not exist
     */
    @Test
    public void test322_DeleteSite() {
        System.out.println("deleteSite");
        String content = "{\"customerAccount\":\"77777\",\"siteId\":\"" + siteVO.getSiteId() + "\",\"siteName\":\"UPTIME SITE 111\","
                + "\"region\":\"NORTH AMERICA\",\"country\":\"UNITED STATES\","
                + "\"physicalAddress1\":\"4820 Executive Park Ct\",\"physicalAddress2\":\"\","
                + "\"physicalCity\":\"Jacksonville\",\"physicalStateProvince\":\"FL\","
                + "\"physicalPostalCode\":\"32216\",\"latitude\":\"30.252254\","
                + "\"longitude\":\"-81.602805\",\"timezone\":\"Pacific/Niue (UTC -11:00)\","
                + "\"industry\":\"Production\",\"shipAddress1\":\"4820 Executive Park Ct\","
                + "\"shipAddress2\":\"\",\"shipCity\":\"Jacksonville\",\"shipStateProvince\":\"FL\","
                + "\"shipPostalCode\":\"32216\",\"billingAddress1\":\"4820 Executive Park Ct\","
                + "\"billingAddress2\":\"\",\"billingCity\":\"Jacksonville\",\"billingStateProvince\":\"FL\","
                + "\"billingPostalCode\":\"32216\",\"siteContactList\":[{\"customerAccount\":\"77777\","
                + "\"siteName\":\"UPTIME SITE 1\",\"role\":\"IT\",\"contactName\":\"TEST NAME 1\","
                + "\"contactTitle\":\"Mr.\",\"contactPhone\":\"5555544441\",\"contactEmail\":\"test1@test.com\"},"
                + "{\"customerAccount\":\"77777\",\"siteName\":\"UPTIME SITE 1\","
                + "\"role\":\"REPORTS\",\"contactName\":\"TEST NAME 2\",\"contactTitle\":\"Mr.\","
                + "\"contactPhone\":\"5555544442\",\"contactEmail\":\"test2@test.com\"}]"
                + "}\n";
        String expResult = "{\"outcome\":\"No Site items found to delete.\"}";
        String result = instance.deleteSite(content);
        assertEquals(expResult, result);
    }
    
}
