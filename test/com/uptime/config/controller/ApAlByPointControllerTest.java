/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.config.controller;

import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ApAlByPointControllerTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit  =
            new CassandraCQLUnit(new ClassPathCQLDataSet("resources//config.cql", true, true,"worldview_dev1"));

    static ApAlByPointController instance;
    
    public ApAlByPointControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = new ApAlByPointController();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
    /**
     * Test of createApAlByPoints method, of class ApAlByPointController.
     */
    @Test
    public void test10_CreateApAlByPoints() throws Exception{
        System.out.println("createApAlByPoints");
        String content = "{\n" +
"    \"customerAccount\": \"FEDEX EXPRESS\",\n" +
"    \"siteId\": \"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\n" +
"    \"areaId\": \"ab10c195-d6e1-40b3-8ebc-9a46dfbbf192\",\n" +
"    \"assetId\": \"f694cbd8-87de-47a8-ae21-3c98fb45763c\",\n" +
"    \"pointLocationId\": \"dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f\",\n" +
"    \"pointId\": \"1838e70a-da74-47c1-8a24-89d08ab792cf\",\n" +
"    \"apSetId\": \"0ddef2a9-0071-4a8c-9866-b887cfa8b10f\",\n" +
"    \"alSetId\": \"55ff3661-9d32-4f9c-970b-696bafc14327\",    \n" +
"    \"autoAcknowledge\": false,\n" +
"    \"disabled\": false,\n" +
"    \"pointType\": \"AC\",\n" +
"    \"sampleInterval\": 20,\n" +
"    \"sensorChannelNum\": 0,\n" +
"    \"sensorOffset\": 0.0,\n" +
"    \"sensorSensitivity\": 30.0,\n" +
"    \"sensorType\": \"Velocity\",\n" +
"    \"sensorUnits\": \"miles per hr\",\n" +
"    \"alarmEnabled\": true,\n" +
"    \"customized\": false,\n" +
"    \"apAlSetVOs\": [\n" +
"        {\n" +
"            \"customerAccount\": \"FEDEX EXPRESS\",\n" +
"            \"apSetName\": \"Accel Set 10-26\",\n" +
"            \"apSetId\": \"82d1ad8f-1fcb-435a-907b-83aec1952a66\",\n" +
"            \"alSetName\": \"Accel Set 10-26\",\n" +
"            \"alSetId\": \"4238e15e-3e8f-495c-8228-846cf45cdf4a\",\n" +
"            \"paramName\": \"Wave RMS 10-26\",\n" +
"            \"sensorType\": \"Acceleration\",\n" +
"            \"fmax\": 977,\n" +
"            \"resolution\": 1600,\n" +
"            \"period\": 1.6384,\n" +
"            \"sampleRate\": 2500.0,\n" +
"            \"demod\": false,\n" +
"            \"demodHighPass\": 0.0,\n" +
"            \"demodLowPass\": 0.0,\n" +
"            \"paramType\": \"Waveform RMS\",\n" +
"            \"paramUnits\": \"Gs\",\n" +
"            \"paramAmpFactor\": \"RMS\",\n" +
"            \"minFrequency\": 0.0,\n" +
"            \"maxFrequency\": 0.0,\n" +
"            \"dwell\": 1,\n" +
"            \"lowFaultActive\": false,\n" +
"            \"lowAlertActive\": false,\n" +
"            \"highAlertActive\": true,\n" +
"            \"highFaultActive\": false,\n" +
"            \"lowFault\": 0.0,\n" +
"            \"lowAlert\": 25.0,\n" +
"            \"highAlert\": 40.0,\n" +
"            \"highFault\": 0.0\n" +
"        }\n" +
"    ]\n" +
"}";
        String expResult = "{\"outcome\":\"New ApAlByPoints created successfully.\"}";
        String result = instance.createApAlByPoints(content);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of createApAlByPoints method, of class ApAlByPointController.
     */
    @Test
    public void test11_CreateApAlByPoints() throws Exception {
        System.out.println("createApAlByPoints");
        String content = "{\n" +
"    \"customerAccount\": \"FEDEX EXPRESS\",\n" +
"    \"siteId\": \"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\n" +
"    \"areaId\": \"ab10c195-d6e1-40b3-8ebc-9a46dfbbf192\",\n" +
//"    \"assetId\": \"f694cbd8-87de-47a8-ae21-3c98fb45763c\",\n" +
"    \"pointLocationId\": \"dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f\",\n" +
"    \"pointId\": \"1838e70a-da74-47c1-8a24-89d08ab792cf\",\n" +
"    \"apSetId\": \"0ddef2a9-0071-4a8c-9866-b887cfa8b10f\",\n" +
"    \"alSetId\": \"55ff3661-9d32-4f9c-970b-696bafc14327\",    \n" +
"    \"autoAcknowledge\": false,\n" +
"    \"disabled\": false,\n" +
"    \"pointType\": \"AC\",\n" +
"    \"sampleInterval\": 20,\n" +
"    \"sensorChannelNum\": 0,\n" +
"    \"sensorOffset\": 0.0,\n" +
"    \"sensorSensitivity\": 30.0,\n" +
"    \"sensorType\": \"Velocity\",\n" +
"    \"sensorUnits\": \"miles per hr\",\n" +
"    \"alarmEnabled\": true,\n" +
"    \"customized\": false,\n" +
"    \"apAlSetVOs\": [\n" +
"        {\n" +
"            \"customerAccount\": \"FEDEX EXPRESS\",\n" +
"            \"apSetName\": \"Accel Set 10-26\",\n" +
"            \"apSetId\": \"82d1ad8f-1fcb-435a-907b-83aec1952a66\",\n" +
"            \"alSetName\": \"Accel Set 10-26\",\n" +
"            \"alSetId\": \"4238e15e-3e8f-495c-8228-846cf45cdf4a\",\n" +
"            \"paramName\": \"Wave RMS 10-26\",\n" +
"            \"sensorType\": \"Acceleration\",\n" +
"            \"fmax\": 977,\n" +
"            \"resolution\": 1600,\n" +
"            \"period\": 1.6384,\n" +
"            \"sampleRate\": 2500.0,\n" +
"            \"demod\": false,\n" +
"            \"demodHighPass\": 0.0,\n" +
"            \"demodLowPass\": 0.0,\n" +
"            \"paramType\": \"Waveform RMS\",\n" +
"            \"paramUnits\": \"Gs\",\n" +
"            \"paramAmpFactor\": \"RMS\",\n" +
"            \"minFrequency\": 0.0,\n" +
"            \"maxFrequency\": 0.0,\n" +
"            \"dwell\": 1,\n" +
"            \"lowFaultActive\": false,\n" +
"            \"lowAlertActive\": false,\n" +
"            \"highAlertActive\": true,\n" +
"            \"highFaultActive\": false,\n" +
"            \"lowFault\": 0.0,\n" +
"            \"lowAlert\": 25.0,\n" +
"            \"highAlert\": 40.0,\n" +
"            \"highFault\": 0.0\n" +
"        }\n" +
"    ]\n" +
"}";
        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = instance.createApAlByPoints(content);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of createApAlByPoints method, of class ApAlByPointController.
     */
    @Test
    public void test12_CreateApAlByPoints()  throws Exception {
        System.out.println("createApAlByPoints");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createApAlByPoints(null);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSet method, of class ApAlByPointController.
     */
    @Test
    public void test20_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSet() throws Exception {
        System.out.println("getApAlByPointByCustomerSiteAreaAssetLocationPointApSet");
        String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "ab10c195-d6e1-40b3-8ebc-9a46dfbbf192";
        String assetId = "f694cbd8-87de-47a8-ae21-3c98fb45763c";
        String pointLocationId = "dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f";
        String pointId = "1838e70a-da74-47c1-8a24-89d08ab792cf";
        String apSetId = "0ddef2a9-0071-4a8c-9866-b887cfa8b10f";
        String expResult = "{\"customer\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"ab10c195-d6e1-40b3-8ebc-9a46dfbbf192\",\"assetId\":\"f694cbd8-87de-47a8-ae21-3c98fb45763c\",\"pointLocationId\":\"dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f\",\"pointId\":\"1838e70a-da74-47c1-8a24-89d08ab792cf\",\"apSetId\":\"0ddef2a9-0071-4a8c-9866-b887cfa8b10f\",\"ApAlByPoints\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"ab10c195-d6e1-40b3-8ebc-9a46dfbbf192\",\"assetId\":\"f694cbd8-87de-47a8-ae21-3c98fb45763c\",\"pointLocationId\":\"dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f\",\"pointId\":\"1838e70a-da74-47c1-8a24-89d08ab792cf\",\"apSetId\":\"0ddef2a9-0071-4a8c-9866-b887cfa8b10f\",\"alSetId\":\"55ff3661-9d32-4f9c-970b-696bafc14327\",\"paramName\":\"Wave RMS 10-26\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":1,\"fMax\":977,\"highAlert\":40.0,\"highAlertActive\":true,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":false,\"lowAlert\":25.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"paramAmpFactor\":\"RMS\",\"paramType\":\"Waveform RMS\",\"paramUnits\":\"Gs\",\"period\":1.6384,\"resolution\":1600,\"sampleRate\":2500.0,\"sensorType\":\"Velocity\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId);
        System.out.println("test2_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSet resule - " + result);
        assertEquals(expResult, result);
        
    }
    
    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSet method, of class ApAlByPointController.
     */
    @Test
    public void test21_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSet() throws Exception {
        System.out.println("getApAlByPointByCustomerSiteAreaAssetLocationPointApSet");
        String customer = "FEDEX EXPRESS";
        String siteId = UUID.randomUUID().toString();
        String areaId = "ab10c195-d6e1-40b3-8ebc-9a46dfbbf192";
        String assetId = "f694cbd8-87de-47a8-ae21-3c98fb45763c";
        String pointLocationId = "dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f";
        String pointId = "1838e70a-da74-47c1-8a24-89d08ab792cf";
        String apSetId = "0ddef2a9-0071-4a8c-9866-b887cfa8b10f";
        String expResult = "{\"outcome\":\"Item/Items not found.\"}";
        String result = instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId);
        System.out.println("test2_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSet resule - " + result);
        assertEquals(expResult, result);
        
    }
    
    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSet method, of class ApAlByPointController.
     */
    @Test(expected = NullPointerException.class)
    public void test22_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSet() throws Exception {
        System.out.println("getApAlByPointByCustomerSiteAreaAssetLocationPointApSet");
        String customer = "FEDEX EXPRESS";
        String siteId = null;
        String areaId = "ab10c195-d6e1-40b3-8ebc-9a46dfbbf192";
        String assetId = "f694cbd8-87de-47a8-ae21-3c98fb45763c";
        String pointLocationId = "dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f";
        String pointId = "1838e70a-da74-47c1-8a24-89d08ab792cf";
        String apSetId = "0ddef2a9-0071-4a8c-9866-b887cfa8b10f";
        instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId);
    }
    
    
    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSet method, of class ApAlByPointController.
     */
    @Test(expected = IllegalArgumentException.class)
    public void test23_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSet() throws Exception {
        System.out.println("getApAlByPointByCustomerSiteAreaAssetLocationPointApSet");
        String customer = "FEDEX EXPRESS";
        String siteId = "";
        String areaId = "ab10c195-d6e1-40b3-8ebc-9a46dfbbf192";
        String assetId = "f694cbd8-87de-47a8-ae21-3c98fb45763c";
        String pointLocationId = "dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f";
        String pointId = "1838e70a-da74-47c1-8a24-89d08ab792cf";
        String apSetId = "0ddef2a9-0071-4a8c-9866-b887cfa8b10f";
        instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId);
    }

    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet method, of class ApAlByPointController.
     */
    @Test
    public void test30_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet() throws Exception {
        System.out.println("getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet");
        String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "ab10c195-d6e1-40b3-8ebc-9a46dfbbf192";
        String assetId = "f694cbd8-87de-47a8-ae21-3c98fb45763c";
        String pointLocationId = "dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f";
        String pointId = "1838e70a-da74-47c1-8a24-89d08ab792cf";
        String apSetId = "0ddef2a9-0071-4a8c-9866-b887cfa8b10f";
        String alSetId = "55ff3661-9d32-4f9c-970b-696bafc14327";
        String expResult = "{\"customer\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"ab10c195-d6e1-40b3-8ebc-9a46dfbbf192\",\"assetId\":\"f694cbd8-87de-47a8-ae21-3c98fb45763c\",\"pointLocationId\":\"dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f\",\"pointId\":\"1838e70a-da74-47c1-8a24-89d08ab792cf\",\"apSetId\":\"0ddef2a9-0071-4a8c-9866-b887cfa8b10f\",\"alSetId\":\"55ff3661-9d32-4f9c-970b-696bafc14327\",\"ApAlByPoints\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"ab10c195-d6e1-40b3-8ebc-9a46dfbbf192\",\"assetId\":\"f694cbd8-87de-47a8-ae21-3c98fb45763c\",\"pointLocationId\":\"dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f\",\"pointId\":\"1838e70a-da74-47c1-8a24-89d08ab792cf\",\"apSetId\":\"0ddef2a9-0071-4a8c-9866-b887cfa8b10f\",\"alSetId\":\"55ff3661-9d32-4f9c-970b-696bafc14327\",\"paramName\":\"Wave RMS 10-26\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":1,\"fMax\":977,\"highAlert\":40.0,\"highAlertActive\":true,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":false,\"lowAlert\":25.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"paramAmpFactor\":\"RMS\",\"paramType\":\"Waveform RMS\",\"paramUnits\":\"Gs\",\"period\":1.6384,\"resolution\":1600,\"sampleRate\":2500.0,\"sensorType\":\"Velocity\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId);
        System.out.println("test30_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet method, of class ApAlByPointController.
     */
    @Test
    public void test31_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet() throws Exception {
        System.out.println("getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet");
        String customer = "FEDEX EXPRESS";
        String siteId = UUID.randomUUID().toString();
        String areaId = "ab10c195-d6e1-40b3-8ebc-9a46dfbbf192";
        String assetId = "f694cbd8-87de-47a8-ae21-3c98fb45763c";
        String pointLocationId = "dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f";
        String pointId = "1838e70a-da74-47c1-8a24-89d08ab792cf";
        String apSetId = "0ddef2a9-0071-4a8c-9866-b887cfa8b10f";
        String alSetId = "55ff3661-9d32-4f9c-970b-696bafc14327";
        String expResult = "{\"outcome\":\"Item/Items not found.\"}";
        String result = instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId);
        System.out.println("test30_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet method, of class ApAlByPointController.
     */
    @Test(expected = NullPointerException.class)
    public void test32_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet() throws Exception {
        System.out.println("getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet");
        String customer = "FEDEX EXPRESS";
        String siteId = null;
        String areaId = "ab10c195-d6e1-40b3-8ebc-9a46dfbbf192";
        String assetId = "f694cbd8-87de-47a8-ae21-3c98fb45763c";
        String pointLocationId = "dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f";
        String pointId = "1838e70a-da74-47c1-8a24-89d08ab792cf";
        String apSetId = "0ddef2a9-0071-4a8c-9866-b887cfa8b10f";
        String alSetId = "55ff3661-9d32-4f9c-970b-696bafc14327";
        instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId);
    }

    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet method, of class ApAlByPointController.
     */
    @Test(expected = IllegalArgumentException.class)
    public void test33_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet() throws Exception {
        System.out.println("getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet");
        String customer = "FEDEX EXPRESS";
        String siteId = "";
        String areaId = "ab10c195-d6e1-40b3-8ebc-9a46dfbbf192";
        String assetId = "f694cbd8-87de-47a8-ae21-3c98fb45763c";
        String pointLocationId = "dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f";
        String pointId = "1838e70a-da74-47c1-8a24-89d08ab792cf";
        String apSetId = "0ddef2a9-0071-4a8c-9866-b887cfa8b10f";
        String alSetId = "55ff3661-9d32-4f9c-970b-696bafc14327";
        String expResult = "{\"customer\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"ab10c195-d6e1-40b3-8ebc-9a46dfbbf192\",\"assetId\":\"f694cbd8-87de-47a8-ae21-3c98fb45763c\",\"pointLocationId\":\"dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f\",\"pointId\":\"1838e70a-da74-47c1-8a24-89d08ab792cf\",\"apSetId\":\"0ddef2a9-0071-4a8c-9866-b887cfa8b10f\",\"alSetId\":\"55ff3661-9d32-4f9c-970b-696bafc14327\",\"ApAlByPoints\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"ab10c195-d6e1-40b3-8ebc-9a46dfbbf192\",\"assetId\":\"f694cbd8-87de-47a8-ae21-3c98fb45763c\",\"pointLocationId\":\"dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f\",\"pointId\":\"1838e70a-da74-47c1-8a24-89d08ab792cf\",\"apSetId\":\"0ddef2a9-0071-4a8c-9866-b887cfa8b10f\",\"alSetId\":\"55ff3661-9d32-4f9c-970b-696bafc14327\",\"paramName\":\"Wave RMS 10-26\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":1,\"fMax\":977,\"highAlert\":40.0,\"highAlertActive\":true,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":false,\"lowAlert\":25.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"paramAmpFactor\":\"RMS\",\"paramType\":\"Waveform RMS\",\"paramUnits\":\"Gs\",\"period\":1.6384,\"resolution\":1600,\"sampleRate\":2500.0,\"sensorType\":\"Velocity\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId);
        System.out.println("test30_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getApAlByPointByPK method, of class ApAlByPointController.
     */
    @Test
    public void test40_GetApAlByPointByPK() throws Exception {
        System.out.println("getApAlByPointByPK");
        String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "ab10c195-d6e1-40b3-8ebc-9a46dfbbf192";
        String assetId = "f694cbd8-87de-47a8-ae21-3c98fb45763c";
        String pointLocationId = "dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f";
        String pointId = "1838e70a-da74-47c1-8a24-89d08ab792cf";
        String apSetId = "0ddef2a9-0071-4a8c-9866-b887cfa8b10f";
        String alSetId = "55ff3661-9d32-4f9c-970b-696bafc14327";
        String paramName = "Wave RMS 10-26";
        String expResult = "{\"customer\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"ab10c195-d6e1-40b3-8ebc-9a46dfbbf192\",\"assetId\":\"f694cbd8-87de-47a8-ae21-3c98fb45763c\",\"pointLocationId\":\"dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f\",\"pointId\":\"1838e70a-da74-47c1-8a24-89d08ab792cf\",\"apSetId\":\"0ddef2a9-0071-4a8c-9866-b887cfa8b10f\",\"alSetId\":\"55ff3661-9d32-4f9c-970b-696bafc14327\",\"paramName\":\"Wave RMS 10-26\",\"ApAlByPoints\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"ab10c195-d6e1-40b3-8ebc-9a46dfbbf192\",\"assetId\":\"f694cbd8-87de-47a8-ae21-3c98fb45763c\",\"pointLocationId\":\"dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f\",\"pointId\":\"1838e70a-da74-47c1-8a24-89d08ab792cf\",\"apSetId\":\"0ddef2a9-0071-4a8c-9866-b887cfa8b10f\",\"alSetId\":\"55ff3661-9d32-4f9c-970b-696bafc14327\",\"paramName\":\"Wave RMS 10-26\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":1,\"fMax\":977,\"highAlert\":40.0,\"highAlertActive\":true,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":false,\"lowAlert\":25.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"paramAmpFactor\":\"RMS\",\"paramType\":\"Waveform RMS\",\"paramUnits\":\"Gs\",\"period\":1.6384,\"resolution\":1600,\"sampleRate\":2500.0,\"sensorType\":\"Velocity\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getApAlByPointByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId, paramName);
        System.out.println("test40_GetApAlByPointByPK result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getApAlByPointByPK method, of class ApAlByPointController.
     */
    @Test
    public void test41_GetApAlByPointByPK() throws Exception {
        System.out.println("getApAlByPointByPK");
        String customer = "FEDEX EXPRESS";
        String siteId = UUID.randomUUID().toString();
        String areaId = "ab10c195-d6e1-40b3-8ebc-9a46dfbbf192";
        String assetId = "f694cbd8-87de-47a8-ae21-3c98fb45763c";
        String pointLocationId = "dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f";
        String pointId = "1838e70a-da74-47c1-8a24-89d08ab792cf";
        String apSetId = "0ddef2a9-0071-4a8c-9866-b887cfa8b10f";
        String alSetId = "55ff3661-9d32-4f9c-970b-696bafc14327";
        String paramName = "Wave RMS 10-26";
        String expResult = "{\"outcome\":\"Item/Items not found.\"}";
        String result = instance.getApAlByPointByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId, paramName);
        System.out.println("test40_GetApAlByPointByPK result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getApAlByPointByPK method, of class ApAlByPointController.
     */
    @Test(expected = NullPointerException.class)
    public void test42_GetApAlByPointByPK() throws Exception {
        System.out.println("getApAlByPointByPK");
        String customer = "FEDEX EXPRESS";
        String siteId = null;
        String areaId = "ab10c195-d6e1-40b3-8ebc-9a46dfbbf192";
        String assetId = "f694cbd8-87de-47a8-ae21-3c98fb45763c";
        String pointLocationId = "dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f";
        String pointId = "1838e70a-da74-47c1-8a24-89d08ab792cf";
        String apSetId = "0ddef2a9-0071-4a8c-9866-b887cfa8b10f";
        String alSetId = "55ff3661-9d32-4f9c-970b-696bafc14327";
        String paramName = "Wave RMS 10-26";
        instance.getApAlByPointByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId, paramName);
    }

    /**
     * Test of getApAlByPointByPK method, of class ApAlByPointController.
     */
    @Test(expected = IllegalArgumentException.class)
    public void test43_GetApAlByPointByPK() throws Exception {
        System.out.println("getApAlByPointByPK");
        String customer = "FEDEX EXPRESS";
        String siteId = "";
        String areaId = "ab10c195-d6e1-40b3-8ebc-9a46dfbbf192";
        String assetId = "f694cbd8-87de-47a8-ae21-3c98fb45763c";
        String pointLocationId = "dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f";
        String pointId = "1838e70a-da74-47c1-8a24-89d08ab792cf";
        String apSetId = "0ddef2a9-0071-4a8c-9866-b887cfa8b10f";
        String alSetId = "55ff3661-9d32-4f9c-970b-696bafc14327";
        String paramName = "Wave RMS 10-26";
        instance.getApAlByPointByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId, paramName);
    }

    /**
     * Test of updateApAlByPoints method, of class ApAlByPointController.
     */
    @Test
    public void test50_UpdateApAlByPoints()  throws Exception{
        System.out.println("updateApAlByPoints");
        String content = "{\n" +
"    \"customerAccount\": \"FEDEX EXPRESS\",\n" +
"    \"siteId\": \"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\n" +
"    \"areaId\": \"ab10c195-d6e1-40b3-8ebc-9a46dfbbf192\",\n" +
"    \"assetId\": \"f694cbd8-87de-47a8-ae21-3c98fb45763c\",\n" +
"    \"pointLocationId\": \"dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f\",\n" +
"    \"pointId\": \"1838e70a-da74-47c1-8a24-89d08ab792cf\",\n" +
"    \"apSetId\": \"0ddef2a9-0071-4a8c-9866-b887cfa8b10f\",\n" +
"    \"alSetId\": \"55ff3661-9d32-4f9c-970b-696bafc14327\",    \n" +
"    \"autoAcknowledge\": false,\n" +
"    \"disabled\": false,\n" +
"    \"pointType\": \"AC\",\n" +
"    \"sampleInterval\": 20,\n" +
"    \"sensorChannelNum\": 0,\n" +
"    \"sensorOffset\": 0.0,\n" +
"    \"sensorSensitivity\": 30.0,\n" +
"    \"sensorType\": \"Velocity\",\n" +
"    \"sensorUnits\": \"miles per hr\",\n" +
"    \"alarmEnabled\": true,\n" +
"    \"customized\": false,\n" +
"    \"apAlSetVOs\": [\n" +
"        {\n" +
"            \"customerAccount\": \"FEDEX EXPRESS\",\n" +
"            \"apSetName\": \"Accel Set 10-26\",\n" +
"            \"apSetId\": \"82d1ad8f-1fcb-435a-907b-83aec1952a66\",\n" +
"            \"alSetName\": \"Accel Set 10-26\",\n" +
"            \"alSetId\": \"4238e15e-3e8f-495c-8228-846cf45cdf4a\",\n" +
"            \"paramName\": \"Wave RMS 10-26\",\n" +
"            \"sensorType\": \"Acceleration\",\n" +
"            \"fmax\": 977,\n" +
"            \"resolution\": 1600,\n" +
"            \"period\": 1.6384,\n" +
"            \"sampleRate\": 2500.0,\n" +
"            \"demod\": false,\n" +
"            \"demodHighPass\": 0.0,\n" +
"            \"demodLowPass\": 0.0,\n" +
"            \"paramType\": \"Waveform RMS\",\n" +
"            \"paramUnits\": \"Gs\",\n" +
"            \"paramAmpFactor\": \"RMS\",\n" +
"            \"minFrequency\": 10.0,\n" +
"            \"maxFrequency\": 20.0,\n" +
"            \"dwell\": 1,\n" +
"            \"lowFaultActive\": false,\n" +
"            \"lowAlertActive\": false,\n" +
"            \"highAlertActive\": true,\n" +
"            \"highFaultActive\": false,\n" +
"            \"lowFault\": 0.0,\n" +
"            \"lowAlert\": 25.0,\n" +
"            \"highAlert\": 40.0,\n" +
"            \"highFault\": 0.0\n" +
"        }\n" +
"    ]\n" +
"}\n" +
"";
        String expResult = "{\"outcome\":\"Updated ApAlByPoints successfully.\"}";
        String result = instance.updateApAlByPoints(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateApAlByPoints method, of class ApAlByPointController.
     */
    @Test
    public void test52_UpdateApAlByPoints()  throws Exception{
        System.out.println("updateApAlByPoints");
        String content = "{\n" +
"    \"customerAccount\": \"FEDEX EXPRESS\",\n" +
"    \"siteId\": \"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\n" +
//"    \"areaId\": \"ab10c195-d6e1-40b3-8ebc-9a46dfbbf192\",\n" +
"    \"assetId\": \"f694cbd8-87de-47a8-ae21-3c98fb45763c\",\n" +
"    \"pointLocationId\": \"dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f\",\n" +
"    \"pointId\": \"1838e70a-da74-47c1-8a24-89d08ab792cf\",\n" +
"    \"apSetId\": \"0ddef2a9-0071-4a8c-9866-b887cfa8b10f\",\n" +
"    \"alSetId\": \"55ff3661-9d32-4f9c-970b-696bafc14327\",    \n" +
"    \"autoAcknowledge\": false,\n" +
"    \"disabled\": false,\n" +
"    \"pointType\": \"AC\",\n" +
"    \"sampleInterval\": 20,\n" +
"    \"sensorChannelNum\": 0,\n" +
"    \"sensorOffset\": 0.0,\n" +
"    \"sensorSensitivity\": 30.0,\n" +
"    \"sensorType\": \"Velocity\",\n" +
"    \"sensorUnits\": \"miles per hr\",\n" +
"    \"alarmEnabled\": true,\n" +
"    \"customized\": false,\n" +
"    \"apAlSetVOs\": [\n" +
"        {\n" +
"            \"customerAccount\": \"FEDEX EXPRESS\",\n" +
"            \"apSetName\": \"Accel Set 10-26\",\n" +
"            \"apSetId\": \"82d1ad8f-1fcb-435a-907b-83aec1952a66\",\n" +
"            \"alSetName\": \"Accel Set 10-26\",\n" +
"            \"alSetId\": \"4238e15e-3e8f-495c-8228-846cf45cdf4a\",\n" +
"            \"paramName\": \"Wave RMS 10-26\",\n" +
"            \"sensorType\": \"Acceleration\",\n" +
"            \"fmax\": 977,\n" +
"            \"resolution\": 1600,\n" +
"            \"period\": 1.6384,\n" +
"            \"sampleRate\": 2500.0,\n" +
"            \"demod\": false,\n" +
"            \"demodHighPass\": 0.0,\n" +
"            \"demodLowPass\": 0.0,\n" +
"            \"paramType\": \"Waveform RMS\",\n" +
"            \"paramUnits\": \"Gs\",\n" +
"            \"paramAmpFactor\": \"RMS\",\n" +
"            \"minFrequency\": 10.0,\n" +
"            \"maxFrequency\": 20.0,\n" +
"            \"dwell\": 1,\n" +
"            \"lowFaultActive\": false,\n" +
"            \"lowAlertActive\": false,\n" +
"            \"highAlertActive\": true,\n" +
"            \"highFaultActive\": false,\n" +
"            \"lowFault\": 0.0,\n" +
"            \"lowAlert\": 25.0,\n" +
"            \"highAlert\": 40.0,\n" +
"            \"highFault\": 0.0\n" +
"        }\n" +
"    ]\n" +
"}\n" +
"";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateApAlByPoints(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateApAlByPoints method, of class ApAlByPointController.
     */
    @Test
    public void test53_UpdateApAlByPoints()  throws Exception {
        System.out.println("updateApAlByPoints");
        String content = "{\n" +
"    \"customerAccount\": \"FEDEX EXPRESS\",\n" +
"    \"siteId\": \"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\n" +
"    \"areaId\": \"ab10c195-d6e1-40b3-8ebc-\",\n" + //invalid area id
"    \"assetId\": \"f694cbd8-87de-47a8-ae21-3c98fb45763c\",\n" +
"    \"pointLocationId\": \"dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f\",\n" +
"    \"pointId\": \"1838e70a-da74-47c1-8a24-89d08ab792cf\",\n" +
"    \"apSetId\": \"0ddef2a9-0071-4a8c-9866-b887cfa8b10f\",\n" +
"    \"alSetId\": \"55ff3661-9d32-4f9c-970b-696bafc14327\",    \n" +
"    \"autoAcknowledge\": false,\n" +
"    \"disabled\": false,\n" +
"    \"pointType\": \"AC\",\n" +
"    \"sampleInterval\": 20,\n" +
"    \"sensorChannelNum\": 0,\n" +
"    \"sensorOffset\": 0.0,\n" +
"    \"sensorSensitivity\": 30.0,\n" +
"    \"sensorType\": \"Velocity\",\n" +
"    \"sensorUnits\": \"miles per hr\",\n" +
"    \"alarmEnabled\": true,\n" +
"    \"customized\": false,\n" +
"    \"apAlSetVOs\": [\n" +
"        {\n" +
"            \"customerAccount\": \"FEDEX EXPRESS\",\n" +
"            \"apSetName\": \"Accel Set 10-26\",\n" +
"            \"apSetId\": \"82d1ad8f-1fcb-435a-907b-83aec1952a66\",\n" +
"            \"alSetName\": \"Accel Set 10-26\",\n" +
"            \"alSetId\": \"4238e15e-3e8f-495c-8228-846cf45cdf4a\",\n" +
"            \"paramName\": \"Wave RMS 10-26\",\n" +
"            \"sensorType\": \"Acceleration\",\n" +
"            \"fmax\": 977,\n" +
"            \"resolution\": 1600,\n" +
"            \"period\": 1.6384,\n" +
"            \"sampleRate\": 2500.0,\n" +
"            \"demod\": false,\n" +
"            \"demodHighPass\": 0.0,\n" +
"            \"demodLowPass\": 0.0,\n" +
"            \"paramType\": \"Waveform RMS\",\n" +
"            \"paramUnits\": \"Gs\",\n" +
"            \"paramAmpFactor\": \"RMS\",\n" +
"            \"minFrequency\": 10.0,\n" +
"            \"maxFrequency\": 20.0,\n" +
"            \"dwell\": 1,\n" +
"            \"lowFaultActive\": false,\n" +
"            \"lowAlertActive\": false,\n" +
"            \"highAlertActive\": true,\n" +
"            \"highFaultActive\": false,\n" +
"            \"lowFault\": 0.0,\n" +
"            \"lowAlert\": 25.0,\n" +
"            \"highAlert\": 40.0,\n" +
"            \"highFault\": 0.0\n" +
"        }\n" +
"    ]\n" +
"}\n" +
"";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updateApAlByPoints(content);
        System.out.println("test53_UpdateApAlByPointsresult - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of updateApAlByPoints method, of class ApAlByPointController.
     */
    @Test
    public void test54_UpdateApAlByPoints()  throws Exception{
        System.out.println("updateApAlByPoints");
        String content = "";
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updateApAlByPoints(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteApAlByPoints method, of class ApAlByPointController.
     */
    @Test
    public void test60_DeleteApAlByPoints() {
        System.out.println("deleteApAlByPoints");
        String content = "{\n" +
"    \"customerAccount\": \"FEDEX EXPRESS 1\",\n" +
"    \"siteId\": \"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\n" +
"    \"areaId\": \"ab10c195-d6e1-40b3-8ebc-9a46dfbbf192\",\n" +
"    \"assetId\": \"f694cbd8-87de-47a8-ae21-3c98fb45763c\",\n" +
"    \"pointLocationId\": \"dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f\",\n" +
"    \"pointId\": \"1838e70a-da74-47c1-8a24-89d08ab792cf\",\n" +
"    \"apSetId\": \"0ddef2a9-0071-4a8c-9866-b887cfa8b10f\",\n" +
"    \"alSetId\": \"55ff3661-9d32-4f9c-970b-696bafc14327\",    \n" +
"    \"autoAcknowledge\": false,\n" +
"    \"disabled\": false,\n" +
"    \"pointType\": \"AC\",\n" +
"    \"sampleInterval\": 20,\n" +
"    \"sensorChannelNum\": 0,\n" +
"    \"sensorOffset\": 0.0,\n" +
"    \"sensorSensitivity\": 30.0,\n" +
"    \"sensorType\": \"Velocity\",\n" +
"    \"sensorUnits\": \"miles per hr\",\n" +
"    \"alarmEnabled\": true,\n" +
"    \"customized\": false,\n" +
"    \"apAlSetVOs\": [\n" +
"        {\n" +
"            \"customerAccount\": \"FEDEX EXPRESS\",\n" +
"            \"apSetName\": \"Accel Set 10-26\",\n" +
"            \"apSetId\": \"82d1ad8f-1fcb-435a-907b-83aec1952a66\",\n" +
"            \"alSetName\": \"Accel Set 10-26\",\n" +
"            \"alSetId\": \"4238e15e-3e8f-495c-8228-846cf45cdf4a\",\n" +
"            \"paramName\": \"Wave RMS 10-26\",\n" +
"            \"sensorType\": \"Acceleration\",\n" +
"            \"fmax\": 977,\n" +
"            \"resolution\": 1600,\n" +
"            \"period\": 1.6384,\n" +
"            \"sampleRate\": 2500.0,\n" +
"            \"demod\": false,\n" +
"            \"demodHighPass\": 0.0,\n" +
"            \"demodLowPass\": 0.0,\n" +
"            \"paramType\": \"Waveform RMS\",\n" +
"            \"paramUnits\": \"Gs\",\n" +
"            \"paramAmpFactor\": \"RMS\",\n" +
"            \"minFrequency\": 10.0,\n" +
"            \"maxFrequency\": 20.0,\n" +
"            \"dwell\": 1,\n" +
"            \"lowFaultActive\": false,\n" +
"            \"lowAlertActive\": false,\n" +
"            \"highAlertActive\": true,\n" +
"            \"highFaultActive\": false,\n" +
"            \"lowFault\": 0.0,\n" +
"            \"lowAlert\": 25.0,\n" +
"            \"highAlert\": 40.0,\n" +
"            \"highFault\": 0.0\n" +
"        }\n" +
"    ]\n" +
"}\n" +
"";
        String expResult = "{\"outcome\":\"No ApAlByPoints found to delete.\"}";
        String result = instance.deleteApAlByPoints(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteApAlByPoints method, of class ApAlByPointController.
     */
    @Test
    public void test61_DeleteApAlByPoints() {
        System.out.println("deleteApAlByPoints");
        String content = "{\n" +
"    \"customerAccount\": \"FEDEX EXPRESS\",\n" +
"    \"siteId\": \"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\n" +
"    \"areaId\": \"ab10c195-d6e1-40b3-8ebc-9a46dfbbf192\",\n" +
"    \"assetId\": \"f694cbd8-87de-47a8-ae21-3c98fb45763c\",\n" +
"    \"pointLocationId\": \"dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f\",\n" +
"    \"pointId\": \"1838e70a-da74-47c1-8a24-89d08ab792cf\",\n" +
"    \"apSetId\": \"0ddef2a9-0071-4a8c-9866-b887cfa8b10f\",\n" +
"    \"alSetId\": \"55ff3661-9d32-4f9c-970b-696bafc14327\",    \n" +
"    \"autoAcknowledge\": false,\n" +
"    \"disabled\": false,\n" +
"    \"pointType\": \"AC\",\n" +
"    \"sampleInterval\": 20,\n" +
"    \"sensorChannelNum\": 0,\n" +
"    \"sensorOffset\": 0.0,\n" +
"    \"sensorSensitivity\": 30.0,\n" +
"    \"sensorType\": \"Velocity\",\n" +
"    \"sensorUnits\": \"miles per hr\",\n" +
"    \"alarmEnabled\": true,\n" +
"    \"customized\": false,\n" +
"    \"apAlSetVOs\": [\n" +
"        {\n" +
"            \"customerAccount\": \"FEDEX EXPRESS\",\n" +
"            \"apSetName\": \"Accel Set 10-26\",\n" +
"            \"apSetId\": \"82d1ad8f-1fcb-435a-907b-83aec1952a66\",\n" +
"            \"alSetName\": \"Accel Set 10-26\",\n" +
"            \"alSetId\": \"4238e15e-3e8f-495c-8228-846cf45cdf4a\",\n" +
"            \"paramName\": \"Wave RMS 10-26\",\n" +
"            \"sensorType\": \"Acceleration\",\n" +
"            \"fmax\": 977,\n" +
"            \"resolution\": 1600,\n" +
"            \"period\": 1.6384,\n" +
"            \"sampleRate\": 2500.0,\n" +
"            \"demod\": false,\n" +
"            \"demodHighPass\": 0.0,\n" +
"            \"demodLowPass\": 0.0,\n" +
"            \"paramType\": \"Waveform RMS\",\n" +
"            \"paramUnits\": \"Gs\",\n" +
"            \"paramAmpFactor\": \"RMS\",\n" +
"            \"minFrequency\": 10.0,\n" +
"            \"maxFrequency\": 20.0,\n" +
"            \"dwell\": 1,\n" +
"            \"lowFaultActive\": false,\n" +
"            \"lowAlertActive\": false,\n" +
"            \"highAlertActive\": true,\n" +
"            \"highFaultActive\": false,\n" +
"            \"lowFault\": 0.0,\n" +
"            \"lowAlert\": 25.0,\n" +
"            \"highAlert\": 40.0,\n" +
"            \"highFault\": 0.0\n" +
"        }\n" +
"    ]\n" +
"}\n" +
"";
        String expResult = "{\"outcome\":\"Deleted ApAlByPoints successfully.\"}";
        String result = instance.deleteApAlByPoints(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteApAlByPoints method, of class ApAlByPointController.
     */
    @Test
    public void test62_DeleteApAlByPoints() {
        System.out.println("deleteApAlByPoints");
        String content = "{\n" +
"    \"customerAccount\": \"FEDEX EXPRESS\",\n" +
"    \"siteId\": \"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\n" +
"    \"areaId\": \"ab10c195-d6e1-40b3-8ebc-9a46dfbbf192\",\n" +
"    \"assetId\": \"f694cbd8-87de-47a8-ae21-3c98fb45763c\",\n" +
"    \"pointLocationId\": \"dc10fad1-bb37-4ee8-af3f-4c9b7f1f075f\",\n" +
//"    \"pointId\": \"1838e70a-da74-47c1-8a24-89d08ab792cf\",\n" +
"    \"apSetId\": \"0ddef2a9-0071-4a8c-9866-b887cfa8b10f\",\n" +
"    \"alSetId\": \"55ff3661-9d32-4f9c-970b-696bafc14327\",    \n" +
"    \"autoAcknowledge\": false,\n" +
"    \"disabled\": false,\n" +
"    \"pointType\": \"AC\",\n" +
"    \"sampleInterval\": 20,\n" +
"    \"sensorChannelNum\": 0,\n" +
"    \"sensorOffset\": 0.0,\n" +
"    \"sensorSensitivity\": 30.0,\n" +
"    \"sensorType\": \"Velocity\",\n" +
"    \"sensorUnits\": \"miles per hr\",\n" +
"    \"alarmEnabled\": true,\n" +
"    \"customized\": false,\n" +
"    \"apAlSetVOs\": [\n" +
"        {\n" +
"            \"customerAccount\": \"FEDEX EXPRESS\",\n" +
"            \"apSetName\": \"Accel Set 10-26\",\n" +
"            \"apSetId\": \"82d1ad8f-1fcb-435a-907b-83aec1952a66\",\n" +
"            \"alSetName\": \"Accel Set 10-26\",\n" +
"            \"alSetId\": \"4238e15e-3e8f-495c-8228-846cf45cdf4a\",\n" +
"            \"paramName\": \"Wave RMS 10-26\",\n" +
"            \"sensorType\": \"Acceleration\",\n" +
"            \"fmax\": 977,\n" +
"            \"resolution\": 1600,\n" +
"            \"period\": 1.6384,\n" +
"            \"sampleRate\": 2500.0,\n" +
"            \"demod\": false,\n" +
"            \"demodHighPass\": 0.0,\n" +
"            \"demodLowPass\": 0.0,\n" +
"            \"paramType\": \"Waveform RMS\",\n" +
"            \"paramUnits\": \"Gs\",\n" +
"            \"paramAmpFactor\": \"RMS\",\n" +
"            \"minFrequency\": 10.0,\n" +
"            \"maxFrequency\": 20.0,\n" +
"            \"dwell\": 1,\n" +
"            \"lowFaultActive\": false,\n" +
"            \"lowAlertActive\": false,\n" +
"            \"highAlertActive\": true,\n" +
"            \"highFaultActive\": false,\n" +
"            \"lowFault\": 0.0,\n" +
"            \"lowAlert\": 25.0,\n" +
"            \"highAlert\": 40.0,\n" +
"            \"highFault\": 0.0\n" +
"        }\n" +
"    ]\n" +
"}\n" +
"";
        String expResult = "{\"outcome\":\"Insufficient data given in json\"}";
        String result = instance.deleteApAlByPoints(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteApAlByPoints method, of class ApAlByPointController.
     */
    @Test
    public void test63_DeleteApAlByPoints() {
        System.out.println("deleteApAlByPoints");
        String content = "";
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.deleteApAlByPoints(content);
        assertEquals(expResult, result);
    }
    
}
