/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.controller;

import com.uptime.cassandra.workorder.dao.OpenWorkOrdersDAO;
import com.uptime.cassandra.workorder.dao.WorkOrdersMapperImpl;
import com.uptime.cassandra.workorder.entity.OpenWorkOrders;
import com.uptime.config.vo.AreaConfigScheduleVO;
import com.uptime.config.vo.AreaVO;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import java.util.UUID;
import javax.json.Json;
import javax.json.stream.JsonParser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author gsingh
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AreaControllerTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit  =
            new CassandraCQLUnit(new ClassPathCQLDataSet("resources//assets.cql", true, true,"worldview_dev1"));
    
    static AreaController instance;
    static AreaVO areaVO;
    transient final String DATE_TIME_OFFSET_FORMAT;
    transient JsonParser parser;
    transient JsonParser.Event event;
    static OpenWorkOrdersDAO instance1;
    static OpenWorkOrders entity;
    private static final String DATE_TIME_OFFSET_STRING = "yyyy-MM-dd HH:mm:ssZ";

    public AreaControllerTest() {
        DATE_TIME_OFFSET_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
    }

    @BeforeClass
    public static void setUpClass() {
        instance = new AreaController();
        areaVO = new AreaVO();
        areaVO.setCustomerAccount("77777");
        areaVO.setClimateControlled(false);
        areaVO.setSiteId(UUID.fromString("ddb973d4-c9ef-11ec-9d64-0242ac120002"));
        areaVO.setAreaName("AREA TEST 9999");
        areaVO.setDescription("DESC 9999");
        areaVO.setEnvironment("PROD");
        List<AreaConfigScheduleVO> scheduleVOList = new ArrayList<>();
        AreaConfigScheduleVO acsVO = new AreaConfigScheduleVO();
        acsVO.setAreaName("AREA TEST 9999");
        acsVO.setContinuous(false);
        acsVO.setCustomerAccount("77777");
        acsVO.setDayOfWeek((byte) 2);
        acsVO.setHoursOfDay("00:00-01:00,01:00-2:00");
        acsVO.setSchDescription("DESC1");
        acsVO.setScheduleName("SCHEDULE TEST 999 1");
        acsVO.setSchDescription("SCHEDULE TEST DESC 999 1");
        acsVO.setType("New");

        scheduleVOList.add(acsVO);
                 
        areaVO.setScheduleVOList(scheduleVOList);
        
        instance1 = WorkOrdersMapperImpl.getInstance().openWorkOrdersDAO();
        entity = new OpenWorkOrders();
        entity.setAction("");
        entity.setAreaName("Test Area 1");
        entity.setAssetComponent("XXX Pump");
        entity.setAssetId(UUID.fromString("88c4a233-5e07-4d89-947b-7fead5950e84"));
        entity.setAssetName("Test Asset 1");
        entity.setCreatedDate(LocalDateTime.parse("2022-10-19 01:00:00+0000", DateTimeFormatter.ofPattern(DATE_TIME_OFFSET_STRING)).atZone(ZoneOffset.UTC).toInstant());
        entity.setCustomerAccount("77777");
        entity.setDescription("Client Asset Desc 1");
        entity.setIssue("Test issue");
        entity.setNotes("Test Note");
        entity.setPriority((byte)2);
        entity.setSiteId(UUID.fromString("ddb973d4-c9ef-11ec-9d64-0242ac120002"));
        entity.setUser("Uptime");
        
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createArea method, of class AreaController.
     * @throws java.lang.Exception
     */
    @Test
    public void test10_CreateArea() throws Exception {
        String content = "{\"customerAccount\":\"77777\",\n"
                + "\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                + "\"areaName\":\"AREA TEST 9999 1\",\n"
                + "\"climateControlled\":false,\n"
                + "\"description\":\"DESC 9999\",\n"
                + "\"environment\":\"false,false\",\n"
                + "\"scheduleVOList\":[\n"
                + "{\n"
                + "\"customerAccount\":\"77777\",\n"
                + "\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                + "\"areaName\":\"AREA TEST 999 1\",\n"
                + "\"scheduleName\":\"SCHEDULE TEST 999 1\",\n"
                + "\"dayOfWeek\":2,\n"
                + "\"schDescription\":\"SCHEDULE TEST DESC 999 1\",\n"
                + "\"hoursOfDay\":\"00:00-01:00,01:00-2:00\",\n"
                + "\"continuous\":false,\n"
                + "\"type\":\"New\"\n"
                + "}\n"
                + "]\n"
                + "}";

        String expResult = "{\"outcome\":\"New Area created successfully.\"}";
        String result = instance.createArea(content);
        System.out.println("CreateArea result - " + result);
        assertEquals(expResult, result);
        String result1 = instance.getAreaSiteByCustomerSite(areaVO.getCustomerAccount() , areaVO.getSiteId().toString());
        System.out.println("result1 - " + result1);
        List<AreaVO> areaList = populateAreaSiteFromJson(result1);
        areaVO.setAreaId(areaList.get(0).getAreaId());
        System.out.println("set AreaId - " + areaVO.getAreaId());
        String result2 = instance.getAreaConfigScheduleByCustomerSiteArea(areaVO.getCustomerAccount() , areaVO.getSiteId().toString(), areaVO.getAreaId().toString());
        System.out.println("result2 - " + result2);
        List<AreaConfigScheduleVO> acvoList = populateAreaConfigScheduleFromJson(result2);
        areaVO.getScheduleVOList().get(0).setScheduleId(acvoList.get(0).getScheduleId());
        System.out.println("set ScheduleId - " + areaVO.getScheduleVOList().get(0).getScheduleId());
    }

    /**
     * Test of createArea method, of class AreaController.Provided a Json without Area Name
     * @throws java.lang.Exception
     */
    @Test
    public void test12_CreateArea() throws Exception {
        String content = "{\"customerAccount\":\"77777\",\n"
                + "\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                //+ "\"areaName\":\"AREA TEST 9999 1\",\n"
                + "\"climateControlled\":false,\n"
                + "\"description\":\"DESC 9999\",\n"
                + "\"environment\":\"false,false\",\n"
                + "\"scheduleVOList\":[\n"
                + "{\n"
                + "\"customerAccount\":\"77777\",\n"
                + "\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                + "\"areaName\":\"SCHEDULE TEST 999 1\",\n"
                + "\"scheduleName\":\"SCHEDULE TEST 999 1\",\n"
                + "\"dayOfWeek\":2,\n"
                + "\"schDescription\":\"SCHEDULE TEST DESC 999 1\",\n"
                + "\"hoursOfDay\":\"00:00-01:00,01:00-2:00\",\n"
                + "\"continuous\":false,\n"
                + "\"type\":\"New\"\n"
                + "}\n"
                + "]\n"
                + "}";

        String expResult = "{\"outcome\":\"Insufficient and/or invalid data given in json\"}";
        String result = instance.createArea(content);
        System.out.println("CreateArea result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of createArea method, of class AreaController.Provided null Json
     * @throws java.lang.Exception 
     */
    @Test
    public void test13_CreateArea() throws Exception {
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createArea(null);
        System.out.println("CreateArea result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAreaSiteByCustomerSite method, of class AreaController.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test31_GetAreaSiteByCustomerSite() throws Exception {
        String customer = areaVO.getCustomerAccount();
        UUID siteId = areaVO.getSiteId();
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\"areaSites\":[{\"customerAccount\":\"77777\",\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\"areaId\":\"" + areaVO.getAreaId() + "\",\"areaName\":\"AREA TEST 9999 1\",\"description\":\"DESC 9999\",\"environment\":\"false,false\",\"climateControlled\":false}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAreaSiteByCustomerSite(customer, siteId.toString());
        System.out.println("getAreaSiteByCustomerSite result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAreaSiteByCustomerSite method, of class AreaController.
     * Provided a siteId that does not exist
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test32_GetAreaSiteByCustomerSite() throws Exception {
        String customer = areaVO.getCustomerAccount();
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAreaSiteByCustomerSite(customer, UUID.randomUUID().toString());
        System.out.println("getAreaSiteByCustomerSite result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAreaSiteByCustomerSite method, of class AreaController.
     * Provided invalid siteId
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test33_GetAreaSiteByCustomerSite() throws Exception {
        String customer = areaVO.getCustomerAccount();
        fail(instance.getAreaSiteByCustomerSite(customer, "ddb973d4-c9ef-11ec-"));
    }

    /**
     * Test of getAreaSiteByCustomerSite method, of class AreaController.
     * Provided invalid siteId
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test34_GetAreaSiteByCustomerSite() throws Exception {
        String customer = areaVO.getCustomerAccount();
        fail(instance.getAreaSiteByCustomerSite(customer, null));
    }

    /**
     * Test of getAreaSiteByPK method, of class AreaController.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test35_GetAreaSiteByPK() throws Exception {
        String customer = "77777";
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\"areaId\":\"" + areaVO.getAreaId() + "\",\"areaSites\":[{\"customerAccount\":\"77777\",\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\"areaId\":\"" + areaVO.getAreaId() + "\",\"areaName\":\"AREA TEST 9999 1\",\"description\":\"DESC 9999\",\"environment\":\"false,false\",\"climateControlled\":false}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAreaSiteByPK(customer, "ddb973d4-c9ef-11ec-9d64-0242ac120002", areaVO.getAreaId().toString());
        System.out.println("getAreaSiteByPK result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAreaSiteByPK method, of class AreaController.
     * Provided a siteId that does not exist
     * @throws java.lang.Exception
     */
    @Test
    public void test36_GetAreaSiteByPK() throws Exception {
        String customer = "77777";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAreaSiteByPK(customer, "ddb973d4-c9ef-11ec-9d64-0242ac120007", areaVO.getAreaId().toString());
        System.out.println("getAreaSiteByPK result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAreaSiteByPK method, of class AreaController.
     * Provided an invalid siteId 
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test37_GetAreaSiteByPK() throws Exception {
        String customer = "77777";
        fail(instance.getAreaSiteByPK(customer, "ddb973d4-c9ef", areaVO.getAreaId().toString()));
    }

    /**
     * Test of getAreaSiteByPK method, of class AreaController.
     * Provided null siteId 
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test38_GetAreaSiteByPK() throws Exception {
        String customer = "77777";
        fail(instance.getAreaSiteByPK(customer, null, areaVO.getAreaId().toString()));
    }

    /**
     * Test of getAreaConfigScheduleByCustomerSiteArea method, of class
     * AreaController.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test39_GetAreaConfigScheduleByCustomerSiteArea() throws Exception {
        String customer = "77777";
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\"areaId\":\"" + areaVO.getAreaId() + "\",\"areaConfigSchedules\":[{\"customerAccount\":\"77777\",\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\"areaId\":\"" + areaVO.getAreaId() + "\",\"scheduleId\":\"" + areaVO.getScheduleVOList().get(0).getScheduleId() + "\",\"dayOfWeek\":2,\"areaName\":\"AREA TEST 9999 1\",\"scheduleName\":\"SCHEDULE TEST 999 1\",\"description\":\"SCHEDULE TEST DESC 999 1\",\"continuous\":false,\"hoursOfDay\":\"00:00-01:00,01:00-2:00\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAreaConfigScheduleByCustomerSiteArea(customer, "ddb973d4-c9ef-11ec-9d64-0242ac120002", areaVO.getAreaId().toString());
        System.out.println("getAreaConfigScheduleByCustomerSiteArea result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAreaConfigScheduleByCustomerSiteArea method, of class
     * AreaController.
     * Provided a siteId that does not exist
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test40_GetAreaConfigScheduleByCustomerSiteArea() throws Exception {
        String customer = "77777";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAreaConfigScheduleByCustomerSiteArea(customer, "ddb973d4-c9ef-11ec-9d64-0242ac120008", areaVO.getAreaId().toString());
        System.out.println("getAreaConfigScheduleByCustomerSiteArea result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAreaConfigScheduleByCustomerSiteArea method, of class
     * AreaController.
     * Provided null siteId
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test41_GetAreaConfigScheduleByCustomerSiteArea() throws Exception {
        String customer = "77777";
        fail(instance.getAreaConfigScheduleByCustomerSiteArea(customer, null, areaVO.getAreaId().toString()));
    }

    /**
     * Test of getAreaConfigScheduleByCustomerSiteArea method, of class
     * AreaController.
     * Provided Invalid siteId
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test42_GetAreaConfigScheduleByCustomerSiteArea() throws Exception {
        String customer = "77777";
        fail(instance.getAreaConfigScheduleByCustomerSiteArea(customer, "dgfhjkl", areaVO.getAreaId().toString()));
    }

    /**
     * Test of getAreaConfigScheduleByCustomerSiteAreaSchedule method, of class
     * AreaController.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test43_GetAreaConfigScheduleByCustomerSiteAreaSchedule() throws Exception {
        String customer = "77777";
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\"areaId\":\"" + areaVO.getAreaId() + "\",\"scheduleId\":\"" + areaVO.getScheduleVOList().get(0).getScheduleId() + "\",\"areaConfigSchedules\":[{\"customerAccount\":\"77777\",\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\"areaId\":\"" + areaVO.getAreaId() + "\",\"scheduleId\":\"" + areaVO.getScheduleVOList().get(0).getScheduleId() + "\",\"dayOfWeek\":2,\"areaName\":\"AREA TEST 9999 1\",\"scheduleName\":\"SCHEDULE TEST 999 1\",\"description\":\"SCHEDULE TEST DESC 999 1\",\"continuous\":false,\"hoursOfDay\":\"00:00-01:00,01:00-2:00\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAreaConfigScheduleByCustomerSiteAreaSchedule(customer, "ddb973d4-c9ef-11ec-9d64-0242ac120002", areaVO.getAreaId().toString(), areaVO.getScheduleVOList().get(0).getScheduleId().toString());
        System.out.println("getAreaConfigScheduleByCustomerSiteAreaSchedule result 43 - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getAreaConfigScheduleByCustomerSiteAreaSchedule method, of class
     * AreaController.
     * Provided a siteId that does not exist
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test44_GetAreaConfigScheduleByCustomerSiteAreaSchedule() throws Exception {
        String customer = "77777";
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAreaConfigScheduleByCustomerSiteAreaSchedule(customer, "adb973d4-c9ef-11ec-9d64-0242ac120002", areaVO.getAreaId().toString(), areaVO.getScheduleVOList().get(0).getScheduleId().toString());
        System.out.println("getAreaConfigScheduleByCustomerSiteAreaSchedule result - " + result);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getAreaConfigScheduleByCustomerSiteAreaSchedule method, of class
     * AreaController.
     * Provided an invalid siteId
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test45_GetAreaConfigScheduleByCustomerSiteAreaSchedule() throws Exception {
        String customer = "77777";
        fail(instance.getAreaConfigScheduleByCustomerSiteAreaSchedule(customer, "0242ac120002", areaVO.getAreaId().toString(), areaVO.getScheduleVOList().get(0).getScheduleId().toString()));
    }
    
    /**
     * Test of getAreaConfigScheduleByCustomerSiteAreaSchedule method, of class
     * AreaController.
     * Provided null siteId
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test46_GetAreaConfigScheduleByCustomerSiteAreaSchedule() throws Exception {
        String customer = "77777";
        fail(instance.getAreaConfigScheduleByCustomerSiteAreaSchedule(customer, null, areaVO.getAreaId().toString(), areaVO.getScheduleVOList().get(0).getScheduleId().toString()));
    }

    /**
     * Test of getAreaConfigScheduleByPK method, of class AreaController.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test47_GetAreaConfigScheduleByPK() throws Exception {
        String customer = "77777";
        byte dayOfWeek = 2;
        String expResult = "{\"customerAccount\":\"77777\",\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\"areaId\":\"" + areaVO.getAreaId() + "\",\"scheduleId\":\"" + areaVO.getScheduleVOList().get(0).getScheduleId() + "\",\"dayOfWeek\":\"2\",\"areaConfigSchedules\":[{\"customerAccount\":\"77777\",\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\"areaId\":\"" + areaVO.getAreaId() + "\",\"scheduleId\":\"" + areaVO.getScheduleVOList().get(0).getScheduleId() + "\",\"dayOfWeek\":2,\"areaName\":\"AREA TEST 9999 1\",\"scheduleName\":\"SCHEDULE TEST 999 1\",\"description\":\"SCHEDULE TEST DESC 999 1\",\"continuous\":false,\"hoursOfDay\":\"00:00-01:00,01:00-2:00\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getAreaConfigScheduleByPK(customer, "ddb973d4-c9ef-11ec-9d64-0242ac120002", areaVO.getAreaId().toString(), areaVO.getScheduleVOList().get(0).getScheduleId().toString(), dayOfWeek);
        System.out.println("getAreaConfigScheduleByPK result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAreaConfigScheduleByPK method, of class AreaController.
     * Provided a siteId that does not exist
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test48_GetAreaConfigScheduleByPK() throws Exception {
        String customer = "77777";
        byte dayOfWeek = 2;
        String expResult = "{\"outcome\":\"Error: Null value received from database.\"}";
        String result = instance.getAreaConfigScheduleByPK(customer, "ddb973d4-c9ef-11ec-9d64-0242ac120009", areaVO.getAreaId().toString(), areaVO.getScheduleVOList().get(0).getScheduleId().toString(), dayOfWeek);
        System.out.println("getAreaConfigScheduleByPK result - " + result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAreaConfigScheduleByPK method, of class AreaController.
     * Provided an invalid siteId
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test49_GetAreaConfigScheduleByPK() throws Exception {
        String customer = "77777";
        byte dayOfWeek = 2;
        fail(instance.getAreaConfigScheduleByPK(customer, "9d64-0242ac120002", areaVO.getAreaId().toString(), areaVO.getScheduleVOList().get(0).getScheduleId().toString(), dayOfWeek));
    }

    /**
     * Test of getAreaConfigScheduleByPK method, of class AreaController.
     * Provided null siteId
     *
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test50_GetAreaConfigScheduleByPK() throws Exception {
        String customer = "77777";
        byte dayOfWeek = 2;
        fail(instance.getAreaConfigScheduleByPK(customer, null, areaVO.getAreaId().toString(), areaVO.getScheduleVOList().get(0).getScheduleId().toString(), dayOfWeek));
    }

    /**
     * Test of updateArea method, of class AreaController.
     */
    @Test
    public void test51_UpdateArea() {
        String content = "{\n"
                + "\"customerAccount\":\"77777\",\n"
                + "\"siteName\":\"TEST SITE 001\",\n"
                + "\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                + "\"areaId\":\"" + areaVO.getAreaId() + "\",\n"
                + "\"areaName\":\"AREA TEST 8888\",\n"
                + "\"climateControlled\":false,\n"
                + "\"description\":\"DESC 99991\",\n"
                + "\"environment\":\"false,false\",\n"
                + "\"scheduleVOList\":[\n"
                + "{\n"
                + "\"customerAccount\":\"77777\",\n"
                + "\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                + "\"areaId\":\"" + areaVO.getAreaId() + "\",\n"
                + "\"scheduleId\":\"" + areaVO.getScheduleVOList().get(0).getScheduleId() + "\",\n"
                + "\"siteName\":\"TEST SITE 001\",\n"
                + "\"areaName\":\"AREA TEST 9999\",\n"
                + "\"scheduleName\":\"SCHEDULE 99\",\n"
                + "\"dayOfWeek\":2,\n"
                + "\"schDescription\":\"DESC1\",\n"
                + "\"hoursOfDay\":\"00:00-01:00,01:00-2:00\",\n"
                + "\"continuous\":false,\n"
                + "\"type\":\"Update\"\n"
                + "}\n"
                + "]\n"
                + "}";

        String expResult = "{\"outcome\":\"Updated Area items successfully.\"}";
        entity.setAreaId(areaVO.getAreaId());
        instance1.create(entity);
        String result = instance.updateArea(content);
        List<OpenWorkOrders> resultWorkOrders =  instance1.findByCustomerSiteAreaAsset(entity.getCustomerAccount(), entity.getSiteId(), 
                entity.getAreaId(), entity.getAssetId());
        assertEquals(1, resultWorkOrders.size());
        assertEquals(resultWorkOrders.get(0).getAreaName(), "AREA TEST 8888");
        assertEquals(expResult, result);
    }

    /**
     * Test of updateArea method, of class AreaController.
     * Provided an Area Json without the siteId
     */
    @Test
    public void test53_UpdateArea() {
        String content = "{\n"
                + "\"customerAccount\":\"77777\",\n"
                + "\"siteName\":\"TEST SITE 001\",\n"
                //+ "\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120009\",\n"
                + "\"areaId\":\"" + areaVO.getAreaId() + "\",\n"
                + "\"areaName\":\"AREA TEST 9999\",\n"
                + "\"climateControlled\":false,\n"
                + "\"description\":\"DESC 99991\",\n"
                + "\"environment\":\"false,false\",\n"
                + "\"scheduleVOList\":[\n"
                + "{\n"
                + "\"customerAccount\":\"77777\",\n"
                + "\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                + "\"areaId\":\"" + areaVO.getAreaId() + "\",\n"
                + "\"scheduleId\":\"" + areaVO.getScheduleVOList().get(0).getScheduleId() + "\",\n"
                + "\"siteName\":\"TEST SITE 001\",\n"
                + "\"areaName\":\"AREA TEST 9999\",\n"
                + "\"scheduleName\":\"SCHEDULE 99\",\n"
                + "\"dayOfWeek\":2,\n"
                + "\"schDescription\":\"DESC1\",\n"
                + "\"hoursOfDay\":\"00:00-01:00,01:00-2:00\",\n"
                + "\"continuous\":false,\n"
                + "\"type\":\"Update\"\n"
                + "}\n"
                + "]\n"
                + "}";

        String expResult = "{\"outcome\":\"Insufficient and/or invalid data given in json\"}";
        String result = instance.updateArea(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateArea method, of class AreaController.
     * Provided null Json 
     */
    @Test
    public void test54_UpdateArea() {
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updateArea(null);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteArea method, of class AreaController.
     */
    @Test
    public void test55_DeleteArea() {
        String content = "{\n"
                + "\"customerAccount\":\"77777\",\n"
                + "\"siteName\":\"TEST SITE 001\",\n"
                + "\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                + "\"areaId\":\"" + areaVO.getAreaId() + "\",\n"
                + "\"areaName\":\"AREA TEST 9999\",\n"
                + "\"climateControlled\":false,\n"
                + "\"description\":\"DESC 99991\",\n"
                + "\"environment\":\"false,false\",\n"
                + "\"scheduleVOList\":[\n"
                + "{\n"
                + "\"customerAccount\":\"77777\",\n"
                + "\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                + "\"areaId\":\"" + areaVO.getAreaId() + "\",\n"
                + "\"scheduleId\":\"" + areaVO.getScheduleVOList().get(0).getScheduleId() + "\",\n"
                + "\"siteName\":\"TEST SITE 001\",\n"
                + "\"areaName\":\"AREA TEST 9999\",\n"
                + "\"scheduleName\":\"SCHEDULE 99\",\n"
                + "\"dayOfWeek\":2,\n"
                + "\"schDescription\":\"DESC1\",\n"
                + "\"hoursOfDay\":\"00:00-01:00,01:00-2:00\",\n"
                + "\"continuous\":false,\n"
                + "\"type\":\"New\"\n"
                + "}\n"
                + "]\n"
                + "}";

        String expResult = "{\"outcome\":\"Deleted Area items successfully.\"}";
        String result = instance.deleteArea(content);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of deleteArea method, of class AreaController.
     * Provided a Json for an Area that does not exist
     */
    @Test
    public void test56_DeleteArea() {
        String content = "{\n"
                + "\"customerAccount\":\"77777\",\n"
                + "\"siteName\":\"TEST SITE 001\",\n"
                + "\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120008\",\n"
                + "\"areaId\":\"" + areaVO.getAreaId() + "\",\n"
                + "\"areaName\":\"AREA TEST 9999\",\n"
                + "\"climateControlled\":false,\n"
                + "\"description\":\"DESC 99991\",\n"
                + "\"environment\":\"false,false\",\n"
                + "\"scheduleVOList\":[\n"
                + "{\n"
                + "\"customerAccount\":\"77777\",\n"
                + "\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                + "\"areaId\":\"" + areaVO.getAreaId() + "\",\n"
                + "\"scheduleId\":\"" + areaVO.getScheduleVOList().get(0).getScheduleId() + "\",\n"
                + "\"siteName\":\"TEST SITE 001\",\n"
                + "\"areaName\":\"AREA TEST 9999\",\n"
                + "\"scheduleName\":\"SCHEDULE 99\",\n"
                + "\"dayOfWeek\":2,\n"
                + "\"schDescription\":\"DESC1\",\n"
                + "\"hoursOfDay\":\"00:00-01:00,01:00-2:00\",\n"
                + "\"continuous\":false,\n"
                + "\"type\":\"New\"\n"
                + "}\n"
                + "]\n"
                + "}";

        String expResult = "{\"outcome\":\"No Area items found to delete.\"}";
        String result = instance.deleteArea(content);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of deleteArea method, of class AreaController.
     * Provided a Json for without SiteId
     */
    @Test
    public void test57_DeleteArea() {
        String content = "{\n"
                + "\"customerAccount\":\"77777\",\n"
                + "\"siteName\":\"TEST SITE 001\",\n"
                //+ "\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120008\",\n"
                + "\"areaId\":\"" + areaVO.getAreaId() + "\",\n"
                + "\"areaName\":\"AREA TEST 9999\",\n"
                + "\"climateControlled\":false,\n"
                + "\"description\":\"DESC 99991\",\n"
                + "\"environment\":\"false,false\",\n"
                + "\"scheduleVOList\":[\n"
                + "{\n"
                + "\"customerAccount\":\"77777\",\n"
                + "\"siteId\":\"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                + "\"areaId\":\"" + areaVO.getAreaId() + "\",\n"
                + "\"scheduleId\":\"" + areaVO.getScheduleVOList().get(0).getScheduleId() + "\",\n"
                + "\"siteName\":\"TEST SITE 001\",\n"
                + "\"areaName\":\"AREA TEST 9999\",\n"
                + "\"scheduleName\":\"SCHEDULE 99\",\n"
                + "\"dayOfWeek\":2,\n"
                + "\"schDescription\":\"DESC1\",\n"
                + "\"hoursOfDay\":\"00:00-01:00,01:00-2:00\",\n"
                + "\"continuous\":false,\n"
                + "\"type\":\"New\"\n"
                + "}\n"
                + "]\n"
                + "}";

        String expResult = "{\"outcome\":\"Insufficient and/or invalid data given in json\"}";
        String result = instance.deleteArea(content);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of deleteArea method, of class AreaController.
     * Provided a Json for without SiteId
     */
    @Test
    public void test58_DeleteArea() {
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.deleteArea(null);
        assertEquals(expResult, result);
    }
    
    /**
     * Converts a json to a List Object of AreaVO Objects
     * @param content, String Object, json 
     * @return List Object of AreaVO Objects
     */
    private List<AreaVO> populateAreaSiteFromJson(String content) {
        List<AreaVO> list = new ArrayList();
        String keyName;
        AreaVO areaVO1;
        boolean inArray;
         
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            areaVO1 = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        areaVO1 = null;
                        break;
                    case START_OBJECT:
                        areaVO1 = inArray ? new AreaVO() : null;
                        break;
                    case END_OBJECT:
                        if(areaVO1 != null && inArray)
                            list.add(areaVO1);
                        areaVO1 = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "siteId":
                                    keyName = "siteId";
                                    break;
                                case "areaId":
                                    keyName = "areaId";
                                    break;
                                case "siteName":
                                    keyName = "siteName";
                                    break;
                                case "areaName":
                                    keyName = "areaName";
                                    break;
                                case "description":
                                    keyName = "description";
                                    break;
                                case "environment":
                                    keyName = "environment";
                                    break;
                                case "climateControlled":
                                    keyName = "climateControlled";
                                    break;
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(areaVO1 != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    areaVO1.setCustomerAccount(parser.getString());
                                    break;
                                case "siteId":
                                    areaVO1.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                               case "areaId":
                                    areaVO1.setAreaId(UUID.fromString(parser.getString()));
                                    break;
                               case "areaName":
                                    areaVO1.setAreaName(parser.getString());
                                    break;
                                case "description":
                                    areaVO1.setDescription(parser.getString());
                                    break;
                                case "environment":
                                    areaVO1.setEnvironment(parser.getString());
                                    break;
                            }
                        }
                        break;
                    
                    case VALUE_TRUE:
                        if(areaVO1 != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "climateControlled":
                                    areaVO1.setClimateControlled(true);
                                    break;
                            }
                        }
                        break;
                    case VALUE_FALSE:
                        if(areaVO1 != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "climateControlled":
                                    areaVO1.setClimateControlled(false);
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            System.out.println("Exception -" + e);
            list = new ArrayList();
        }
        return list;
    }
    
    /**
     * Converts a json to a List Object of AreaConfigScheduleVO Objects
     * @param content, String Object, json 
     * @return List Object of AreaConfigScheduleVO Objects
     */
    private List<AreaConfigScheduleVO> populateAreaConfigScheduleFromJson(String content) {
        List<AreaConfigScheduleVO> list = new ArrayList();
        String keyName;
        AreaConfigScheduleVO acsVO;
        boolean inArray;
        try {
            parser = Json.createParser(new StringReader(content));
            keyName = null;
            acsVO = null;
            inArray = false;
            
            while (parser.hasNext()) {
                switch((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        acsVO = null;
                        break;
                    case START_OBJECT:
                        acsVO = inArray ? new AreaConfigScheduleVO() : null;
                        break;
                    case END_OBJECT:
                        if(acsVO != null && inArray)
                            list.add(acsVO);
                        acsVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                            switch(parser.getString()) {
                                case "customerAccount":
                                    keyName = "customerAccount";
                                    break;
                                case "siteId":
                                    keyName = "siteId";
                                    break;
                                case "areaId":
                                    keyName = "areaId";
                                     break;
                                case "scheduleId":
                                    keyName = "scheduleId";
                                     break;
                                case "siteName":
                                    keyName = "siteName";
                                    break;
                                case "areaName":
                                    keyName = "areaName";
                                    break;
                                case "scheduleName":
                                    keyName = "scheduleName";
                                    break;
                                case "dayOfWeek":
                                    keyName = "dayOfWeek";
                                    break;
                                case "description":
                                    keyName = "description";
                                    break;
                                case "continuous":
                                    keyName = "continuous";
                                    break;
                                case "hoursOfDay":
                                    keyName = "hoursOfDay";
                                    break;
                                default:
                                    keyName = null;
                                    break;      
                            }
                        break;
                    case VALUE_STRING:
                        if(acsVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "customerAccount":
                                    acsVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteId":
                                    acsVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                                case "areaId":
                                     acsVO.setAreaId(UUID.fromString(parser.getString()));
                                     break;
                                case "scheduleId":
                                     acsVO.setScheduleId(UUID.fromString(parser.getString()));
                                     break;
                                case "areaName":
                                    acsVO.setAreaName(parser.getString());
                                    break;
                                case "description":
                                    acsVO.setSchDescription(parser.getString());
                                    break;
                                case "hoursOfDay":
                                    acsVO.setHoursOfDay(parser.getString());
                                    break;
                                case "scheduleName":
                                    acsVO.setScheduleName(parser.getString());
                                    break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if(acsVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "dayOfWeek":
                                    acsVO.setDayOfWeek((byte)parser.getInt());
                                    break;
                            }
                        }
                        break;
                    case VALUE_TRUE:
                        if(acsVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "continuous":
                                    acsVO.setContinuous(true);
                                    break;
                            }
                        }
                        break;
                    case VALUE_FALSE:
                        if(acsVO != null && inArray && keyName != null) {
                            switch(keyName) {
                                case "continuous":
                                    acsVO.setContinuous(false);
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            System.out.println("Exception - " + e.toString());
            list = new ArrayList();
        }
        return list;
    }

    
}
