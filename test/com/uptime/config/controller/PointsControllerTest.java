/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.controller;

import com.uptime.config.vo.PointVO;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.json.Json;
import javax.json.stream.JsonParser;
import static javax.json.stream.JsonParser.Event.END_ARRAY;
import static javax.json.stream.JsonParser.Event.END_OBJECT;
import static javax.json.stream.JsonParser.Event.KEY_NAME;
import static javax.json.stream.JsonParser.Event.START_ARRAY;
import static javax.json.stream.JsonParser.Event.START_OBJECT;
import static javax.json.stream.JsonParser.Event.VALUE_FALSE;
import static javax.json.stream.JsonParser.Event.VALUE_NUMBER;
import static javax.json.stream.JsonParser.Event.VALUE_STRING;
import static javax.json.stream.JsonParser.Event.VALUE_TRUE;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PointsControllerTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//config.cql", true, true, "worldview_dev1"));
    private static final PointVO pVO = new PointVO();
    static PointsController instance;

    private static String content = "";

    public PointsControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        UUID pointId = UUID.randomUUID();
        pVO.setPointId(pointId);
        instance = new PointsController();
        content = "{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"895cb899-3e3e-4ee4-a81c-c077fd732f61\",\"assetId\":\"5c44985b-fbbc-41c1-877b-2b20fb0b66a7\",\"pointLocationId\":\"49db6ff7-638a-4a28-917e-3cb8845c8c33\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"alSetId\":\"c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a\",\"pointName\":\"Test\",\"autoAcknowledge\":false,\"disabled\":false,\"pointType\":\"AC\",\"sampleInterval\":12,\"sensorChannelNum\":2,\"sensorOffset\":1.0,\"sensorSensitivity\":1.0,\"sensorType\":\"Acceleration\",\"sensorUnits\":\"G/s\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":0,\"fMax\":0,\"siteName\":\"ADSFDG\",\"areaName\":\"10\",\"assetName\":\"Asset 620\",\"pointLocationName\":\"1750\",\"demodInterval\":0,\"alarmEnabled\":false,\"highAlert\":0.0,\"highAlertActive\":false,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":true,\"lowAlert\":0.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"period\":0.0,\"resolution\":0,\"sampleRate\":0.0,\"apAlSetVOs\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"apSetName\":\"AUTO_ACCELERATION\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"alSetName\":\"1143 ASSET 13_VIB\",\"alSetId\":\"c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a\",\"paramName\":\"Total Non Sync\",\"sensorType\":\"Acceleration\",\"fmax\":977,\"resolution\":1600,\"period\":1.6384,\"sampleRate\":2500.0,\"demod\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"paramType\":\"Total Non Sync Energy\",\"frequencyUnits\":\"RPM\",\"paramUnits\":\"mils\",\"paramAmpFactor\":\"RMS\",\"minFrequency\":0.0,\"maxFrequency\":1.0,\"dwell\":3,\"lowFaultActive\":true,\"lowAlertActive\":true,\"highAlertActive\":true,\"highFaultActive\":true,\"lowFault\":0.0,\"lowAlert\":1.0,\"highAlert\":2.0,\"highFault\":3.0}]}";
    }

    /**
     * Test of createPoints method, of class PointsController.
     *
     * @throws java.lang.Exception
     * 
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test11_CreatePoints() throws Exception {
        System.out.println("test11_CreatePoints");
        String expResult = "{\"outcome\":\"New Points created / updated successfully.\"}";
        String result = instance.createPoints(content);
        System.out.println("test11_CreatePoints result - " + result);
        assertEquals(expResult, result);

        JsonParser parser;
        JsonParser.Event event;
        List<PointVO> list = new ArrayList();
        String keyName;
        PointVO pointVO;
        boolean inArray;

        String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String result1 = instance.getPointsByCustomerSiteAreaAssetLocation(customer, siteId, areaId, assetId, pointLocationId);

        try {
            parser = Json.createParser(new StringReader(result1));
            keyName = null;
            pointVO = null;
            inArray = false;

            while (parser.hasNext()) {
                switch ((event = parser.next())) {
                    case START_ARRAY:
                        inArray = true;
                        break;
                    case END_ARRAY:
                        inArray = false;
                        keyName = null;
                        pointVO = null;
                        break;
                    case START_OBJECT:
                        pointVO = inArray ? new PointVO() : null;
                        break;
                    case END_OBJECT:
                        if (pointVO != null && inArray) {
                            list.add(pointVO);
                        }
                        pointVO = null;
                        keyName = null;
                        break;
                    case KEY_NAME:
                        switch (parser.getString()) {
                            case "customerAccount":
                                keyName = "customerAccount";
                                break;
                            case "siteId":
                                keyName = "siteId";
                                break;
                            case "areaId":
                                keyName = "areaId";
                                break;
                            case "assetId":
                                keyName = "assetId";
                                break;
                            case "pointLocationId":
                                keyName = "pointLocationId";
                                break;
                            case "pointId":
                                keyName = "pointId";
                                break;
                            case "apSetId":
                                keyName = "apSetId";
                                break;
                            case "alSetId":
                                keyName = "alSetId";
                                break;
                            case "pointName":
                                keyName = "pointName";
                                break;
                            case "autoAcknowledge":
                                keyName = "autoAcknowledge";
                                break;
                            case "demodEnabled":
                                keyName = "demodEnabled";
                                break;
                            case "demodHighPass":
                                keyName = "demodHighPass";
                                break;
                            case "demodInterval":
                                keyName = "demodInterval";
                                break;
                            case "demodLowPass":
                                keyName = "demodLowPass";
                                break;
                            case "disabled":
                                keyName = "disabled";
                                break;
                            case "pointType":
                                keyName = "pointType";
                                break;
                            case "sampleInterval":
                                keyName = "sampleInterval";
                                break;
                            case "sensorChannelNum":
                                keyName = "sensorChannelNum";
                                break;
                            case "sensorOffset":
                                keyName = "sensorOffset";
                                break;
                            case "sensorSensitivity":
                                keyName = "sensorSensitivity";
                                break;
                            case "sensorType":
                                keyName = "sensorType";
                                break;
                            case "sensorUnits":
                                keyName = "sensorUnits";
                                break;
                            default:
                                keyName = null;
                                break;
                        }
                        break;
                    case VALUE_STRING:
                        if (pointVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "customerAccount":
                                    pointVO.setCustomerAccount(parser.getString());
                                    break;
                                case "siteId":
                                    pointVO.setSiteId(UUID.fromString(parser.getString()));
                                    break;
                                case "areaId":
                                    pointVO.setAreaId(UUID.fromString(parser.getString()));
                                    break;
                                case "assetId":
                                    pointVO.setAssetId(UUID.fromString(parser.getString()));
                                    break;
                                case "pointLocationId":
                                    pointVO.setPointLocationId(UUID.fromString(parser.getString()));
                                    break;
                                case "pointId":
                                    pointVO.setPointId(UUID.fromString(parser.getString()));
                                    break;
                                case "apSetId":
                                    pointVO.setApSetId(UUID.fromString(parser.getString()));
                                    break;
                                case "alSetId":
                                    pointVO.setAlSetId(UUID.fromString(parser.getString()));
                                    break;
                                case "pointName":
                                    pointVO.setPointName(parser.getString());
                                    break;
                                case "pointType":
                                    pointVO.setPointType(parser.getString());
                                    break;
                                case "sensorType":
                                    pointVO.setSensorType(parser.getString());
                                    break;
                                case "sensorUnits":
                                    pointVO.setSensorUnits(parser.getString());
                                    break;
                            }
                        }
                        break;
                    case VALUE_NUMBER:
                        if (pointVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "sensorOffset":
                                    pointVO.setSensorOffset(parser.getBigDecimal().floatValue());
                                    break;
                                case "sensorSensitivity":
                                    pointVO.setSensorSensitivity(parser.getBigDecimal().floatValue());
                                    break;
//                                case "sampleInterval":
//                                    pointVO.setSampleInterval(parser.getInt());
//                                    break;
                                case "sensorChannelNum":
                                    pointVO.setSensorChannelNum(parser.getInt());
                                    break;
                            }
                        }
                        break;
                    case VALUE_TRUE:
                        if (pointVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "autoAcknowledge":
                                    pointVO.setAutoAcknowledge(true);
                                    break;
                                case "disabled":
                                    pointVO.setDisabled(true);
                                    break;
                            }
                        }
                        break;
                    case VALUE_FALSE:
                        if (pointVO != null && inArray && keyName != null) {
                            switch (keyName) {
                                case "resolved":
                                case "autoAcknowledge":
                                    pointVO.setAutoAcknowledge(false);
                                    break;
                                case "disabled":
                                    pointVO.setDisabled(false);
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            System.out.println("Exception - " + e);
            list = new ArrayList();
        }
        pVO.setPointId(list.get(0).getPointId());
        System.out.println("created PointId - " + pVO.getPointId());

    }

    /**
     * Test of createPoints method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Missing required field siteId in input json.
     */
    @Test
    public void test12_CreatePoints() throws Exception {
        System.out.println("test12_CreatePoints");
        String contentJson = "{\n"
                + "\"customerAccount\": \"FEDEX\",     \n"
                + "\"areaId\": \"9e3c16da-1a28-4a31-81ff-8ef46aa23cfa\",            \n"
                + "\"assetId\": \"9e3c16da-1a28-4a31-81ff-8ef46aa23cfa\",               \n"
                + "\"pointLocationId\": \"9e3c16da-1a28-4a31-81ff-8ef46aa23cfa\",  \n"
                + "\"pointId\": \"" + pVO.getPointId() + "\",  \n"
                + "\"pointName\": \"Test\",\n"
                + "\"apSetId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                + "\"alSetId\": \"ddb973d4-c9ef-11ec-9d64-0242ac120002\",\n"
                + "\"paramName\": \"Param\",\n"
                + "\"alSetName\": \"TestAlSetName1191\",\n"
                + "\"autoAcknowledge\": true,\n"
                + "\"demodEnabled\": true,\n"
                + "\"demodHighPass\": 1.1191,\n"
                + "\"demodInterval\": 1191,\n"
                + "\"demodLowPass\": 0.1191,\n"
                + "\"isDisabled\": false,\n"
                + "\"pointType\": \"Type\",\n"
                + "\"sampleInterval\": 1191,\n"
                + "\"sensorChannelNum\": 1191,\n"
                + "\"sensorOffset\": 0.1191,\n"
                + "\"sensorSensitivity\": 1.1191,\n"
                + "\"sensorType\": \"Sensor\",\n"
                + "\"sensorUnits\": \"Unit\",\n"
                + "\"useCustomLimits\": true\n}";

        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.createPoints(contentJson);
        assertEquals(expResult, result);
    }

    /**
     * Test of createPoints method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Empty input json.
     */
    @Test
    public void test13_CreatePoints() throws Exception {
        System.out.println("test13_CreatePoints");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createPoints("");
        assertEquals(expResult, result);
    }

    /**
     * Test of createPoints method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: null input json.
     */
    @Test
    public void test14_CreatePoints() throws Exception {
        System.out.println("test14_CreatePoints");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.createPoints(null);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPointsByCustomerSiteAreaAsset method, of class
     * PointsController.
     *
     * @throws java.lang.Exception
     * Happy path: Passing input correctly and achieving desired output. 
     */
    //@Test
    public void test21_GetPointsByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test21_GetPointsByCustomerSiteAreaAsset");

        String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "7b696a33-9678-4cde-b7fd-4a37324aef7d";
        String assetId = "7e5bb981-ee96-4406-9c66-84056e47ad87";

        String expResult = "{\"customer\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"7b696a33-9678-4cde-b7fd-4a37324aef7d\",\"assetId\":\"7e5bb981-ee96-4406-9c66-84056e47ad87\",\"Points\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"7b696a33-9678-4cde-b7fd-4a37324aef7d\",\"assetId\":\"7e5bb981-ee96-4406-9c66-84056e47ad87\",\"pointLocationId\":\"6f92a94b-dfd6-4253-87d0-876f1958e460\",\"pointId\":\"0fef30b5-218e-4581-a09b-6ceee29d29e9\",\"apSetId\":\"575efc62-1578-4c24-ac0d-eae5802ef361\",\"alSetId\":\"c6abc370-c36c-4960-b124-055ef6e5d00c\",\"autoAcknowledge\":false,\"disabled\":false,\"pointName\":\"Battery\",\"pointType\":\"DC\",\"sensorChannelNum\":1,\"sensorOffset\":0.0,\"sensorSensitivity\":1000.0,\"sensorType\":\"Voltage\",\"sensorUnits\":\"V\",\"alarmEnabled\":false},{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"7b696a33-9678-4cde-b7fd-4a37324aef7d\",\"assetId\":\"7e5bb981-ee96-4406-9c66-84056e47ad87\",\"pointLocationId\":\"6f92a94b-dfd6-4253-87d0-876f1958e460\",\"pointId\":\"1604b903-9a77-4922-9136-10f901dba318\",\"apSetId\":\"8e8904a9-403a-440a-ad5f-ef411044e951\",\"alSetId\":\"0f8a2aae-1926-4206-9f0b-4b1e273a23c5\",\"autoAcknowledge\":false,\"disabled\":false,\"pointName\":\"Acceleration\",\"pointType\":\"AC\",\"sensorChannelNum\":1,\"sensorOffset\":0.0,\"sensorSensitivity\":50.0,\"sensorType\":\"Acceleration\",\"sensorUnits\":\"Gs\",\"alarmEnabled\":false},{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"7b696a33-9678-4cde-b7fd-4a37324aef7d\",\"assetId\":\"7e5bb981-ee96-4406-9c66-84056e47ad87\",\"pointLocationId\":\"6f92a94b-dfd6-4253-87d0-876f1958e460\",\"pointId\":\"170729d8-d9fc-41ad-88e0-c759762f5d39\",\"apSetId\":\"34435ffd-d82e-47bd-96fb-ceeed7e85006\",\"alSetId\":\"856b4185-08ca-4e56-9179-83e33639c244\",\"autoAcknowledge\":false,\"disabled\":false,\"pointName\":\"Temp\",\"pointType\":\"DC\",\"sensorChannelNum\":0,\"sensorOffset\":0.0,\"sensorSensitivity\":1000.0,\"sensorType\":\"Temperature\",\"sensorUnits\":\"F\",\"alarmEnabled\":false},{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"7b696a33-9678-4cde-b7fd-4a37324aef7d\",\"assetId\":\"7e5bb981-ee96-4406-9c66-84056e47ad87\",\"pointLocationId\":\"6f92a94b-dfd6-4253-87d0-876f1958e460\",\"pointId\":\"a7f1929e-b7b3-4b4f-ba1b-11c0287028a2\",\"apSetId\":\"23162a10-393f-4bdd-80b2-e7080848874b\",\"alSetId\":\"235a9e5a-017c-471a-8167-9dd6ca0be0b7\",\"autoAcknowledge\":false,\"disabled\":false,\"pointName\":\"Ultrasonic\",\"pointType\":\"AC\",\"sensorChannelNum\":0,\"sensorOffset\":0.0,\"sensorSensitivity\":50.0,\"sensorType\":\"Ultrasonic\",\"sensorUnits\":\"Pascals\",\"alarmEnabled\":false},{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"7b696a33-9678-4cde-b7fd-4a37324aef7d\",\"assetId\":\"7e5bb981-ee96-4406-9c66-84056e47ad87\",\"pointLocationId\":\"7e8f270c-88d2-4293-85bd-0aae2637a7fb\",\"pointId\":\"4a291d53-c02a-4be8-991f-a9d35e8a14e2\",\"apSetId\":\"23162a10-393f-4bdd-80b2-e7080848874b\",\"alSetId\":\"235a9e5a-017c-471a-8167-9dd6ca0be0b7\",\"autoAcknowledge\":false,\"disabled\":false,\"pointName\":\"Ultrasonic\",\"pointType\":\"AC\",\"sensorChannelNum\":0,\"sensorOffset\":0.0,\"sensorSensitivity\":50.0,\"sensorType\":\"Ultrasonic\",\"sensorUnits\":\"Pascals\",\"alarmEnabled\":false},{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"7b696a33-9678-4cde-b7fd-4a37324aef7d\",\"assetId\":\"7e5bb981-ee96-4406-9c66-84056e47ad87\",\"pointLocationId\":\"7e8f270c-88d2-4293-85bd-0aae2637a7fb\",\"pointId\":\"87890cf5-ccc6-41cc-8e18-34293386627b\",\"apSetId\":\"34435ffd-d82e-47bd-96fb-ceeed7e85006\",\"alSetId\":\"856b4185-08ca-4e56-9179-83e33639c244\",\"autoAcknowledge\":false,\"disabled\":false,\"pointName\":\"Temp\",\"pointType\":\"DC\",\"sensorChannelNum\":0,\"sensorOffset\":0.0,\"sensorSensitivity\":1000.0,\"sensorType\":\"Temperature\",\"sensorUnits\":\"F\",\"alarmEnabled\":false},{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"7b696a33-9678-4cde-b7fd-4a37324aef7d\",\"assetId\":\"7e5bb981-ee96-4406-9c66-84056e47ad87\",\"pointLocationId\":\"7e8f270c-88d2-4293-85bd-0aae2637a7fb\",\"pointId\":\"bf71dca7-9c71-44a6-b51e-a7b910290705\",\"apSetId\":\"8e8904a9-403a-440a-ad5f-ef411044e951\",\"alSetId\":\"0f8a2aae-1926-4206-9f0b-4b1e273a23c5\",\"autoAcknowledge\":false,\"disabled\":false,\"pointName\":\"Acceleration\",\"pointType\":\"AC\",\"sensorChannelNum\":1,\"sensorOffset\":0.0,\"sensorSensitivity\":50.0,\"sensorType\":\"Acceleration\",\"sensorUnits\":\"Gs\",\"alarmEnabled\":false},{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"7b696a33-9678-4cde-b7fd-4a37324aef7d\",\"assetId\":\"7e5bb981-ee96-4406-9c66-84056e47ad87\",\"pointLocationId\":\"7e8f270c-88d2-4293-85bd-0aae2637a7fb\",\"pointId\":\"e8440a11-ee02-42ee-8992-8bf6e0a7b2bf\",\"apSetId\":\"575efc62-1578-4c24-ac0d-eae5802ef361\",\"alSetId\":\"c6abc370-c36c-4960-b124-055ef6e5d00c\",\"autoAcknowledge\":false,\"disabled\":false,\"pointName\":\"Battery\",\"pointType\":\"DC\",\"sensorChannelNum\":1,\"sensorOffset\":0.0,\"sensorSensitivity\":1000.0,\"sensorType\":\"Voltage\",\"sensorUnits\":\"V\",\"alarmEnabled\":false},{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"7b696a33-9678-4cde-b7fd-4a37324aef7d\",\"assetId\":\"7e5bb981-ee96-4406-9c66-84056e47ad87\",\"pointLocationId\":\"c26931b5-bd02-4876-bf6a-7f5b6a77735e\",\"pointId\":\"12a6e2f9-7994-4eed-88d2-e8e3f01f1c07\",\"apSetId\":\"23162a10-393f-4bdd-80b2-e7080848874b\",\"alSetId\":\"235a9e5a-017c-471a-8167-9dd6ca0be0b7\",\"autoAcknowledge\":false,\"disabled\":false,\"pointName\":\"Ultrasonic\",\"pointType\":\"AC\",\"sensorChannelNum\":0,\"sensorOffset\":0.0,\"sensorSensitivity\":50.0,\"sensorType\":\"Ultrasonic\",\"sensorUnits\":\"Pascals\",\"alarmEnabled\":false},{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"7b696a33-9678-4cde-b7fd-4a37324aef7d\",\"assetId\":\"7e5bb981-ee96-4406-9c66-84056e47ad87\",\"pointLocationId\":\"c26931b5-bd02-4876-bf6a-7f5b6a77735e\",\"pointId\":\"2aaa8c4d-5af9-457a-98ca-d7d43be23c25\",\"apSetId\":\"575efc62-1578-4c24-ac0d-eae5802ef361\",\"alSetId\":\"c6abc370-c36c-4960-b124-055ef6e5d00c\",\"autoAcknowledge\":false,\"disabled\":false,\"pointName\":\"Battery\",\"pointType\":\"DC\",\"sensorChannelNum\":1,\"sensorOffset\":0.0,\"sensorSensitivity\":1000.0,\"sensorType\":\"Voltage\",\"sensorUnits\":\"V\",\"alarmEnabled\":false},{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"7b696a33-9678-4cde-b7fd-4a37324aef7d\",\"assetId\":\"7e5bb981-ee96-4406-9c66-84056e47ad87\",\"pointLocationId\":\"c26931b5-bd02-4876-bf6a-7f5b6a77735e\",\"pointId\":\"5840a1bb-9fd2-4891-85a4-fdc02e8ad00b\",\"apSetId\":\"8e8904a9-403a-440a-ad5f-ef411044e951\",\"alSetId\":\"0f8a2aae-1926-4206-9f0b-4b1e273a23c5\",\"autoAcknowledge\":false,\"disabled\":false,\"pointName\":\"Acceleration\",\"pointType\":\"AC\",\"sensorChannelNum\":1,\"sensorOffset\":0.0,\"sensorSensitivity\":50.0,\"sensorType\":\"Acceleration\",\"sensorUnits\":\"Gs\",\"alarmEnabled\":false},{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"7b696a33-9678-4cde-b7fd-4a37324aef7d\",\"assetId\":\"7e5bb981-ee96-4406-9c66-84056e47ad87\",\"pointLocationId\":\"c26931b5-bd02-4876-bf6a-7f5b6a77735e\",\"pointId\":\"7e5849eb-3c3d-48e9-9fef-c8af5244c83a\",\"apSetId\":\"34435ffd-d82e-47bd-96fb-ceeed7e85006\",\"alSetId\":\"856b4185-08ca-4e56-9179-83e33639c244\",\"autoAcknowledge\":false,\"disabled\":false,\"pointName\":\"Temp\",\"pointType\":\"DC\",\"sensorChannelNum\":0,\"sensorOffset\":0.0,\"sensorSensitivity\":1000.0,\"sensorType\":\"Temperature\",\"sensorUnits\":\"F\",\"alarmEnabled\":false}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getPointsByCustomerSiteAreaAsset(customer, siteId, areaId, assetId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPointsByCustomerSiteAreaAsset method, of class
     * PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: DAO throws IllegalArgumentException when passed empty customer Id. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test22_GetPointsByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test22_GetPointsByCustomerSiteAreaAsset");
        String customer = "";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        fail(instance.getPointsByCustomerSiteAreaAsset(customer, siteId, areaId, assetId));
    }

    /**
     * Test of getPointsByCustomerSiteAreaAsset method, of class
     * PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when customer is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test23_GetPointsByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test23_GetPointsByCustomerSiteAreaAsset");
        String customer = null;
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        fail(instance.getPointsByCustomerSiteAreaAsset(customer, siteId, areaId, assetId));
    }

    /**
     * Test of getPointsByCustomerSiteAreaAsset method, of class
     * PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws IllegalArgumentException when passing invalid siteId. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test24_GetPointsByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test24_GetPointsByCustomerSiteAreaAsset");
        String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        fail(instance.getPointsByCustomerSiteAreaAsset(customer, siteId, areaId, assetId));
    }

    /**
     * Test of getPointsByCustomerSiteAreaAsset method, of class
     * PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when siteId is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test25_GetPointsByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test25_GetPointsByCustomerSiteAreaAsset");
        String customer = "FEDEX EXPRESS";
        String siteId = null;
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        fail(instance.getPointsByCustomerSiteAreaAsset(customer, siteId, areaId, assetId));
    }

    /**
     * Test of getPointsByCustomerSiteAreaAsset method, of class
     * PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Item not found if passing invalid siteId. 
     */
    @Test
    public void test26_GetPointsByCustomerSiteAreaAsset() throws Exception {
        System.out.println("test26_GetPointsByCustomerSiteAreaAsset");
       String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3c";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "6c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String expResult = "{\"outcome\":\"Item/Items not found.\"}";
        String result = instance.getPointsByCustomerSiteAreaAsset(customer, siteId, areaId, assetId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPointsByCustomerSiteAreaAssetLocation method, of class
     * PointsController.
     *
     * @throws java.lang.Exception
     * Happy path: Passing input correctly and achieving desired output. 
     */
    //@Test
    public void test31_GetPointsByCustomerSiteAreaAssetLocation() throws Exception {
        System.out.println("test31_GetPointsByCustomerSiteAreaAssetLocation");
        String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String expResult = "{\"customer\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"895cb899-3e3e-4ee4-a81c-c077fd732f61\",\"assetId\":\"5c44985b-fbbc-41c1-877b-2b20fb0b66a7\",\"pointLocationId\":\"49db6ff7-638a-4a28-917e-3cb8845c8c33\",\"Points\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"895cb899-3e3e-4ee4-a81c-c077fd732f61\",\"assetId\":\"5c44985b-fbbc-41c1-877b-2b20fb0b66a7\",\"pointLocationId\":\"49db6ff7-638a-4a28-917e-3cb8845c8c33\",\"pointId\":\""+pVO.getPointId()+"\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"alSetId\":\"c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a\",\"autoAcknowledge\":false,\"disabled\":false,\"pointName\":\"Test\",\"pointType\":\"AC\",\"sensorChannelNum\":2,\"sensorOffset\":1.0,\"sensorSensitivity\":1.0,\"sensorType\":\"Acceleration\",\"sensorUnits\":\"G/s\",\"alarmEnabled\":false}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getPointsByCustomerSiteAreaAssetLocation(customer, siteId, areaId, assetId, pointLocationId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPointsByCustomerSiteAreaAssetLocation method, of class
     * PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: DAO throws IllegalArgumentException when passed empty customer Id. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test32_GetPointsByCustomerSiteAreaAssetLocation() throws Exception {
        System.out.println("test32_GetPointsByCustomerSiteAreaAssetLocation");
        String customer = "";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        fail(instance.getPointsByCustomerSiteAreaAssetLocation(customer, siteId, areaId, assetId, pointLocationId));
    }

    /**
     * Test of getPointsByCustomerSiteAreaAssetLocation method, of class
     * PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when customer is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test33_GetPointsByCustomerSiteAreaAssetLocation() throws Exception {
        System.out.println("test33_GetPointsByCustomerSiteAreaAssetLocation");
        String customer = null;
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        fail(instance.getPointsByCustomerSiteAreaAssetLocation(customer, siteId, areaId, assetId, pointLocationId));
    }

    /**
     * Test of getPointsByCustomerSiteAreaAssetLocation method, of class
     * PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws IllegalArgumentException when passing invalid siteId. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test34_GetPointsByCustomerSiteAreaAssetLocation() throws Exception {
        System.out.println("test34_GetPointsByCustomerSiteAreaAssetLocation");
        String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        fail(instance.getPointsByCustomerSiteAreaAssetLocation(customer, siteId, areaId, assetId, pointLocationId));
    }

    /**
     * Test of getPointsByCustomerSiteAreaAssetLocation method, of class
     * PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when siteId is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test35_GetPointsByCustomerSiteAreaAssetLocation() throws Exception {
        System.out.println("test35_GetPointsByCustomerSiteAreaAssetLocation");
       String customer = "FEDEX EXPRESS";
        String siteId = null;
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        fail(instance.getPointsByCustomerSiteAreaAssetLocation(customer, siteId, areaId, assetId, pointLocationId));
    }

    /**
     * Test of getPointsByCustomerSiteAreaAssetLocation method, of class
     * PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Item not found if passing invalid siteId. 
     */
    @Test
    public void test36_GetPointsByCustomerSiteAreaAssetLocation() throws Exception {
        System.out.println("test36_GetPointsByCustomerSiteAreaAssetLocation");
        String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db4ff7-638a-4a28-917e-3cb8845c8c33";
        String expResult = "{\"outcome\":\"Item/Items not found.\"}";
        String result = instance.getPointsByCustomerSiteAreaAssetLocation(customer, siteId, areaId, assetId, pointLocationId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPointsByCustomerSiteAreaAssetLocationPoint method, of class
     * PointsController.
     *
     * @throws java.lang.Exception
     * Happy path: Passing input correctly and achieving desired output. 
     */
  //  @Test
    public void test41_GetPointsByCustomerSiteAreaAssetLocationPoint() throws Exception {
        System.out.println("test41_GetPointsByCustomerSiteAreaAssetLocationPoint");
       String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String expResult = "{\"customer\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"895cb899-3e3e-4ee4-a81c-c077fd732f61\",\"assetId\":\"5c44985b-fbbc-41c1-877b-2b20fb0b66a7\",\"pointLocationId\":\"49db6ff7-638a-4a28-917e-3cb8845c8c33\",\"pointId\":\""+pVO.getPointId()+"\",\"Points\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"895cb899-3e3e-4ee4-a81c-c077fd732f61\",\"assetId\":\"5c44985b-fbbc-41c1-877b-2b20fb0b66a7\",\"pointLocationId\":\"49db6ff7-638a-4a28-917e-3cb8845c8c33\",\"pointId\":\""+pVO.getPointId()+"\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"alSetId\":\"c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a\",\"autoAcknowledge\":false,\"disabled\":false,\"pointName\":\"Test\",\"pointType\":\"AC\",\"sensorChannelNum\":2,\"sensorOffset\":1.0,\"sensorSensitivity\":1.0,\"sensorType\":\"Acceleration\",\"sensorUnits\":\"G/s\",\"alarmEnabled\":false}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getPointsByCustomerSiteAreaAssetLocationPoint(customer, siteId, areaId, assetId, pointLocationId, pointId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPointsByCustomerSiteAreaAssetLocationPoint method, of class
     * PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: DAO throws IllegalArgumentException when passed empty customer Id. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test42_GetPointsByCustomerSiteAreaAssetLocationPoint() throws Exception {
        System.out.println("test42_GetPointsByCustomerSiteAreaAssetLocationPoint");
        String customer = "";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        fail(instance.getPointsByCustomerSiteAreaAssetLocationPoint(customer, siteId, areaId, assetId, pointLocationId, pointId));
    }

    /**
     * Test of getPointsByCustomerSiteAreaAssetLocationPoint method, of class
     * PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when customer is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test43_GetPointsByCustomerSiteAreaAssetLocationPoint() throws Exception {
        System.out.println("test43_GetPointsByCustomerSiteAreaAssetLocationPoint");
        String customer = null;
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        fail(instance.getPointsByCustomerSiteAreaAssetLocationPoint(customer, siteId, areaId, assetId, pointLocationId, pointId));
    }

    /**
     * Test of getPointsByCustomerSiteAreaAssetLocationPoint method, of class
     * PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws IllegalArgumentException when passing invalid siteId. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test44_GetPointsByCustomerSiteAreaAssetLocationPoint() throws Exception {
        System.out.println("test44_GetPointsByCustomerSiteAreaAssetLocationPoint");
        String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        fail(instance.getPointsByCustomerSiteAreaAssetLocationPoint(customer, siteId, areaId, assetId, pointLocationId, pointId));
    }

    /**
     * Test of getPointsByCustomerSiteAreaAssetLocationPoint method, of class
     * PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when siteId is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test45_GetPointsByCustomerSiteAreaAssetLocationPoint() throws Exception {
        System.out.println("test45_GetPointsByCustomerSiteAreaAssetLocationPoint");
       String customer = "FEDEX EXPRESS";
        String siteId = null;
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        fail(instance.getPointsByCustomerSiteAreaAssetLocationPoint(customer, siteId, areaId, assetId, pointLocationId, pointId));
    }

    /**
     * Test of getPointsByCustomerSiteAreaAssetLocationPoint method, of class
     * PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Item not found if passing invalid siteId. 
     */
    @Test
    public void test46_GetPointsByCustomerSiteAreaAssetLocationPoint() throws Exception {
        System.out.println("test46_GetPointsByCustomerSiteAreaAssetLocationPoint");
       String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db4ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = "23162a10-393f-4bdd-80b2-e7080848874b";
        String expResult = "{\"outcome\":\"Item/Items not found.\"}";
        String result = instance.getPointsByCustomerSiteAreaAssetLocationPoint(customer, siteId, areaId, assetId, pointLocationId, pointId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPointsByPK method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Happy path: Passing input correctly and achieving desired output. 
     */
    //@Test
    public void test51_GetPointsByPK() throws Exception {
        System.out.println("test51_GetPointsByPK");
        String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "7b696a33-9678-4cde-b7fd-4a37324aef7d";
        String assetId = "7e5bb981-ee96-4406-9c66-84056e47ad87";
        String pointLocationId = "6f92a94b-dfd6-4253-87d0-876f1958e460";
        String pointId = "7f1929e-b7b3-4b4f-ba1b-11c0287028a2";
        String apSetId = "23162a10-393f-4bdd-80b2-e7080848874b";
        String expResult = "{\"customer\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"895cb899-3e3e-4ee4-a81c-c077fd732f61\",\"assetId\":\"5c44985b-fbbc-41c1-877b-2b20fb0b66a7\",\"pointLocationId\":\"49db6ff7-638a-4a28-917e-3cb8845c8c33\",\"pointId\":\""+pVO.getPointId()+"\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"Points\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"895cb899-3e3e-4ee4-a81c-c077fd732f61\",\"assetId\":\"5c44985b-fbbc-41c1-877b-2b20fb0b66a7\",\"pointLocationId\":\"49db6ff7-638a-4a28-917e-3cb8845c8c33\",\"pointId\":\""+pVO.getPointId()+"\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"alSetId\":\"c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a\",\"autoAcknowledge\":false,\"disabled\":false,\"pointName\":\"Test\",\"pointType\":\"AC\",\"sensorChannelNum\":2,\"sensorOffset\":1.0,\"sensorSensitivity\":1.0,\"sensorType\":\"Acceleration\",\"sensorUnits\":\"G/s\",\"alarmEnabled\":false}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getPointsByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPointsByPK method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: DAO throws IllegalArgumentException when passed empty customer Id. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test52_GetPointsByPK() throws Exception {
        System.out.println("test52_GetPointsByPK");
        String customer = "";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "700cc412-b311-4295-8faa-295d2ef5c354";
        fail(instance.getPointsByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId));
    }

    /**
     * Test of getPointsByPK method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when customer is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test53_GetPointsByPK() throws Exception {
        System.out.println("test53_GetPointsByPK");
        String customer = null;
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "700cc412-b311-4295-8faa-295d2ef5c354";
        fail(instance.getPointsByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId));
    }

    /**
     * Test of getPointsByPK method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws IllegalArgumentException when passing invalid siteId. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test54_GetPointsByPK() throws Exception {
        System.out.println("test54_GetPointsByPK");
         String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "700cc412-b311-4295-8faa-295d2ef5c354";
        fail(instance.getPointsByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId));
    }

    /**
     * Test of getPointsByPK method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when siteId is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test55_GetPointsByPK() throws Exception {
        System.out.println("test55_GetPointsByPK");
         String customer = "FEDEX EXPRESS";
        String siteId = null;
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "700cc412-b311-4295-8faa-295d2ef5c354";
        fail(instance.getPointsByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId));
    }

    /**
     * Test of getPointsByPK method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Item not found if passing invalid siteId. 
     */
    @Test
    public void test56_GetPointsByPK() throws Exception {
        System.out.println("test56_GetPointsByPK");
         String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "600cc412-b311-4295-8faa-295d2ef5c354";
        String expResult = "{\"outcome\":\"Item/Items not found.\"}";
        String result = instance.getPointsByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSet method,
     * of class PointsController.
     *
     * @throws java.lang.Exception
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test61_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSet() throws Exception {
        System.out.println("test61_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSet");
         String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "700cc412-b311-4295-8faa-295d2ef5c354";
        String expResult = "{\"customer\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"895cb899-3e3e-4ee4-a81c-c077fd732f61\",\"assetId\":\"5c44985b-fbbc-41c1-877b-2b20fb0b66a7\",\"pointLocationId\":\"49db6ff7-638a-4a28-917e-3cb8845c8c33\",\"pointId\":\""+pVO.getPointId()+"\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"ApAlByPoints\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"895cb899-3e3e-4ee4-a81c-c077fd732f61\",\"assetId\":\"5c44985b-fbbc-41c1-877b-2b20fb0b66a7\",\"pointLocationId\":\"49db6ff7-638a-4a28-917e-3cb8845c8c33\",\"pointId\":\""+pVO.getPointId()+"\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"alSetId\":\"c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a\",\"paramName\":\"Total Non Sync\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":3,\"fMax\":977,\"freqUnits\":\"RPM\",\"highAlert\":2.0,\"highAlertActive\":true,\"highFault\":3.0,\"highFaultActive\":true,\"customized\":true,\"lowAlert\":1.0,\"lowAlertActive\":true,\"lowFault\":0.0,\"lowFaultActive\":true,\"maxFreq\":1.0,\"minFreq\":0.0,\"paramAmpFactor\":\"RMS\",\"paramType\":\"Total Non Sync Energy\",\"paramUnits\":\"mils\",\"period\":1.6384,\"resolution\":1600,\"sampleRate\":2500.0,\"sensorType\":\"Acceleration\"}],\"outcome\":\"GET worked successfully.\"}";
        System.out.println("expResult-->"+expResult);
        String result = instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId);
        System.out.println("result-->"+result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSet method,
     * of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: DAO throws IllegalArgumentException when passed empty customer Id. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test62_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSet() throws Exception {
        System.out.println("test62_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSet");
        String customer = ""; 
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "700cc412-b311-4295-8faa-295d2ef5c354";
        fail(instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId));
    }

    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSet method,
     * of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when customer is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test63_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSet() throws Exception {
        System.out.println("test63_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSet");
         String customer = null;
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "700cc412-b311-4295-8faa-295d2ef5c354";
        fail(instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId));
    }

    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSet method,
     * of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws IllegalArgumentException when passing invalid siteId. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test64_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSet() throws Exception {
        System.out.println("test64_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSet");
         String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "700cc412-b311-4295-8faa-295d2ef5c354";
        fail(instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId));
    }

    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSet method,
     * of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when siteId is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test65_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSet() throws Exception {
        System.out.println("test65_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSet");
         String customer = "FEDEX EXPRESS";
        String siteId = null;
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "700cc412-b311-4295-8faa-295d2ef5c354";
        fail(instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId));
    }

    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSet method,
     * of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Item not found if passing invalid siteId. 
     */
    @Test
    public void test66_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSet() throws Exception {
        System.out.println("test66_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSet");
         String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "600cc412-b311-4295-8faa-295d2ef5c354";
        String expResult = "{\"outcome\":\"Item/Items not found.\"}";
        String result = instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet
     * method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test71_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet() throws Exception {
        System.out.println("test71_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet");
         String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "700cc412-b311-4295-8faa-295d2ef5c354";
        String alSetId = "c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a";
        String expResult = "{\"customer\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"895cb899-3e3e-4ee4-a81c-c077fd732f61\",\"assetId\":\"5c44985b-fbbc-41c1-877b-2b20fb0b66a7\",\"pointLocationId\":\"49db6ff7-638a-4a28-917e-3cb8845c8c33\",\"pointId\":\""+pVO.getPointId()+"\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"alSetId\":\"c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a\",\"ApAlByPoints\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"895cb899-3e3e-4ee4-a81c-c077fd732f61\",\"assetId\":\"5c44985b-fbbc-41c1-877b-2b20fb0b66a7\",\"pointLocationId\":\"49db6ff7-638a-4a28-917e-3cb8845c8c33\",\"pointId\":\""+pVO.getPointId()+"\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"alSetId\":\"c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a\",\"paramName\":\"Total Non Sync\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":3,\"fMax\":977,\"freqUnits\":\"RPM\",\"highAlert\":2.0,\"highAlertActive\":true,\"highFault\":3.0,\"highFaultActive\":true,\"customized\":true,\"lowAlert\":1.0,\"lowAlertActive\":true,\"lowFault\":0.0,\"lowFaultActive\":true,\"maxFreq\":1.0,\"minFreq\":0.0,\"paramAmpFactor\":\"RMS\",\"paramType\":\"Total Non Sync Energy\",\"paramUnits\":\"mils\",\"period\":1.6384,\"resolution\":1600,\"sampleRate\":2500.0,\"sensorType\":\"Acceleration\"}],\"outcome\":\"GET worked successfully.\"}";
        System.out.println(expResult);
        String result = instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId);
        System.out.println(result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet
     * method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: DAO throws IllegalArgumentException when passed empty customer Id. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test72_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet() throws Exception {
        System.out.println("test72_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet");
        String customer = "";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "700cc412-b311-4295-8faa-295d2ef5c354";
        String alSetId = "c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a";
        fail(instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId));
    }

    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet
     * method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when customer is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test73_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet() throws Exception {
        System.out.println("test73_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet");
        String customer = null;
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "700cc412-b311-4295-8faa-295d2ef5c354";
        String alSetId = "c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a";
        fail(instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId));
    }

    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet
     * method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws IllegalArgumentException when passing invalid siteId. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test74_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet() throws Exception {
        System.out.println("test74_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet");
        String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "700cc412-b311-4295-8faa-295d2ef5c354";
        String alSetId = "c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a";
        fail(instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId));
    }

    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet
     * method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when siteId is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test75_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet() throws Exception {
        System.out.println("test75_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet");
        String customer = "FEDEX EXPRESS";
        String siteId = null;
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "700cc412-b311-4295-8faa-295d2ef5c354";
        String alSetId = "c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a";
        fail(instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId));
    }

    /**
     * Test of getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet
     * method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Item not found if passing invalid siteId. 
     */
    @Test
    public void test76_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet() throws Exception {
        System.out.println("test76_GetApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet");
        String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "600cc412-b311-4295-8faa-295d2ef5c354";
        String alSetId = "c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a";
        String expResult = "{\"outcome\":\"Item/Items not found.\"}";
        String result = instance.getApAlByPointByCustomerSiteAreaAssetLocationPointApSetAlSet(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId);
        assertEquals(expResult, result);
    }

    /**
     * Test of getApAlByPointByPK method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test81_GetApAlByPointByPK() throws Exception {
        System.out.println("test81_GetApAlByPointByPK");
        String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "700cc412-b311-4295-8faa-295d2ef5c354";
        String alSetId = "c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a";
        String paramName = "Total Non Sync";
        String expResult = "{\"customer\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"895cb899-3e3e-4ee4-a81c-c077fd732f61\",\"assetId\":\"5c44985b-fbbc-41c1-877b-2b20fb0b66a7\",\"pointLocationId\":\"49db6ff7-638a-4a28-917e-3cb8845c8c33\",\"pointId\":\""+pVO.getPointId()+"\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"alSetId\":\"c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a\",\"paramName\":\"Total Non Sync\",\"ApAlByPoints\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"895cb899-3e3e-4ee4-a81c-c077fd732f61\",\"assetId\":\"5c44985b-fbbc-41c1-877b-2b20fb0b66a7\",\"pointLocationId\":\"49db6ff7-638a-4a28-917e-3cb8845c8c33\",\"pointId\":\""+pVO.getPointId()+"\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"alSetId\":\"c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a\",\"paramName\":\"Total Non Sync\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":3,\"fMax\":977,\"freqUnits\":\"RPM\",\"highAlert\":2.0,\"highAlertActive\":true,\"highFault\":3.0,\"highFaultActive\":true,\"customized\":true,\"lowAlert\":1.0,\"lowAlertActive\":true,\"lowFault\":0.0,\"lowFaultActive\":true,\"maxFreq\":1.0,\"minFreq\":0.0,\"paramAmpFactor\":\"RMS\",\"paramType\":\"Total Non Sync Energy\",\"paramUnits\":\"mils\",\"period\":1.6384,\"resolution\":1600,\"sampleRate\":2500.0,\"sensorType\":\"Acceleration\"}],\"outcome\":\"GET worked successfully.\"}";
        String result = instance.getApAlByPointByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId, paramName);
        assertEquals(expResult, result);
    }

    /**
     * Test of getApAlByPointByPK method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: DAO throws IllegalArgumentException when passed empty customer Id. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test82_GetApAlByPointByPK() throws Exception {
        System.out.println("test82_GetApAlByPointByPK");
        String customer = "";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "700cc412-b311-4295-8faa-295d2ef5c354";
        String alSetId = "c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a";
        String paramName = "Total Non Sync";
        fail(instance.getApAlByPointByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId, paramName));
    }

    /**
     * Test of getApAlByPointByPK method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when customer is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test83_GetApAlByPointByPK() throws Exception {
        System.out.println("test83_GetApAlByPointByPK");
        String customer = null;
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "700cc412-b311-4295-8faa-295d2ef5c354";
        String alSetId = "c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a";
        String paramName = "Total Non Sync";
        fail(instance.getApAlByPointByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId, paramName));
    }

    /**
     * Test of getApAlByPointByPK method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws IllegalArgumentException when passing invalid siteId. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test84_GetApAlByPointByPK() throws Exception {
        System.out.println("test84_GetApAlByPointByPK");
        String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "700cc412-b311-4295-8faa-295d2ef5c354";
        String alSetId = "c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a";
        String paramName = "Total Non Sync";
        fail(instance.getApAlByPointByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId, paramName));
    }

    /**
     * Test of getApAlByPointByPK method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when siteId is passed as null. 
     */
    @Test(expected = NullPointerException.class)
    public void test85_GetApAlByPointByPK() throws Exception {
        System.out.println("test85_GetApAlByPointByPK");
        String customer = "FEDEX EXPRESS";
        String siteId = null;
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "700cc412-b311-4295-8faa-295d2ef5c354";
        String alSetId = "c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a";
        String paramName = "Total Non Sync";
        fail(instance.getApAlByPointByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId, paramName));
    }

    /**
     * Test of getApAlByPointByPK method, of class PointsController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Item not found if passing invalid siteId. 
     */
    @Test
    public void test86_GetApAlByPointByPK() throws Exception {
        System.out.println("test86_GetApAlByPointByPK");
        String customer = "FEDEX EXPRESS";
        String siteId = "8b086e15-371f-4a8d-bedb-c1b67b49be3e";
        String areaId = "895cb899-3e3e-4ee4-a81c-c077fd732f61";
        String assetId = "5c44985b-fbbc-41c1-877b-2b20fb0b66a7";
        String pointLocationId = "49db6ff7-638a-4a28-917e-3cb8845c8c33";
        String pointId = pVO.getPointId().toString();
        String apSetId = "600cc412-b311-4295-8faa-295d2ef5c354";
        String alSetId = "c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a";
        String paramName = "Total Non Sync";
        String expResult = "{\"outcome\":\"Item/Items not found.\"}";
        String result = instance.getApAlByPointByPK(customer, siteId, areaId, assetId, pointLocationId, pointId, apSetId, alSetId, paramName);
        assertEquals(expResult, result);
    }

    /**
     * Test of updatePoints method, of class PointsController.
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test91_UpdatePoints() {
        System.out.println("test91_UpdatePoints");
        content = "{\"customerAccount\":\"FEDEX EXPRESS\",\"pointId\":\""+pVO.getPointId()+"\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"895cb899-3e3e-4ee4-a81c-c077fd732f61\",\"assetId\":\"5c44985b-fbbc-41c1-877b-2b20fb0b66a7\",\"pointLocationId\":\"49db6ff7-638a-4a28-917e-3cb8845c8c33\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"alSetId\":\"c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a\",\"pointName\":\"Test\",\"autoAcknowledge\":false,\"disabled\":false,\"pointType\":\"AC\",\"sampleInterval\":12,\"sensorChannelNum\":2,\"sensorOffset\":1.0,\"sensorSensitivity\":1.0,\"sensorType\":\"Acceleration\",\"sensorUnits\":\"G/s\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":0,\"fMax\":0,\"siteName\":\"ADSFDG\",\"areaName\":\"10\",\"assetName\":\"Asset 620\",\"pointLocationName\":\"1750\",\"demodInterval\":0,\"alarmEnabled\":false,\"highAlert\":0.0,\"highAlertActive\":false,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":true,\"lowAlert\":0.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"period\":0.0,\"resolution\":0,\"sampleRate\":0.0,\"apAlSetVOs\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"apSetName\":\"AUTO_ACCELERATION\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"alSetName\":\"1143 ASSET 13_VIB\",\"alSetId\":\"c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a\",\"paramName\":\"Total Non Sync\",\"sensorType\":\"Acceleration\",\"fmax\":977,\"resolution\":1600,\"period\":1.6384,\"sampleRate\":2500.0,\"demod\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"paramType\":\"Total Non Sync Energy\",\"frequencyUnits\":\"RPM\",\"paramUnits\":\"mils\",\"paramAmpFactor\":\"RMS\",\"minFrequency\":0.0,\"maxFrequency\":1.0,\"dwell\":3,\"lowFaultActive\":true,\"lowAlertActive\":true,\"highAlertActive\":true,\"highFaultActive\":true,\"lowFault\":0.0,\"lowAlert\":1.0,\"highAlert\":2.0,\"highFault\":3.0}]}";

        String expResult = "{\"outcome\":\"Updated Points successfully.\"}";
        String result = instance.updatePoints(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updatePoints method, of class PointsController.
     * Unhappy path: Missing required field pointId in input json.
     */
    @Test
    public void test92_UpdatePoints() {
        System.out.println("test92_UpdatePoints");
        content = "{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"895cb899-3e3e-4ee4-a81c-c077fd732f61\",\"assetId\":\"5c44985b-fbbc-41c1-877b-2b20fb0b66a7\",\"pointLocationId\":\"49db6ff7-638a-4a28-917e-3cb8845c8c33\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"alSetId\":\"c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a\",\"pointName\":\"Test\",\"autoAcknowledge\":false,\"disabled\":false,\"pointType\":\"AC\",\"sampleInterval\":12,\"sensorChannelNum\":2,\"sensorOffset\":1.0,\"sensorSensitivity\":1.0,\"sensorType\":\"Acceleration\",\"sensorUnits\":\"G/s\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":0,\"fMax\":0,\"siteName\":\"ADSFDG\",\"areaName\":\"10\",\"assetName\":\"Asset 620\",\"pointLocationName\":\"1750\",\"demodInterval\":0,\"alarmEnabled\":false,\"highAlert\":0.0,\"highAlertActive\":false,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":true,\"lowAlert\":0.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"period\":0.0,\"resolution\":0,\"sampleRate\":0.0,\"apAlSetVOs\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"apSetName\":\"AUTO_ACCELERATION\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"alSetName\":\"1143 ASSET 13_VIB\",\"alSetId\":\"c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a\",\"paramName\":\"Total Non Sync\",\"sensorType\":\"Acceleration\",\"fmax\":977,\"resolution\":1600,\"period\":1.6384,\"sampleRate\":2500.0,\"demod\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"paramType\":\"Total Non Sync Energy\",\"frequencyUnits\":\"RPM\",\"paramUnits\":\"mils\",\"paramAmpFactor\":\"RMS\",\"minFrequency\":0.0,\"maxFrequency\":1.0,\"dwell\":3,\"lowFaultActive\":true,\"lowAlertActive\":true,\"highAlertActive\":true,\"highFaultActive\":true,\"lowFault\":0.0,\"lowAlert\":1.0,\"highAlert\":2.0,\"highFault\":3.0}]}";

        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.updatePoints(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updatePoints method, of class PointsController.
     * Unhappy path: Empty input json.
     */
    @Test
    public void test93_UpdatePoints() {
        System.out.println("test93_UpdatePoints");
        content = "";
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updatePoints(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of updatePoints method, of class PointsController.
     * Unhappy path: null input json.
     */
    @Test
    public void test94_UpdatePoints() {
        System.out.println("test94_UpdatePoints");
        content = null;
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.updatePoints(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deletePoints method, of class PointsController.
     * Happy path: Passing input correctly and achieving desired output. 
     */
   @Test
    public void test95_DeletePoints() {
        System.out.println("test95_DeletePoints");
        content = "{\"customerAccount\":\"FEDEX EXPRESS\",\"pointId\":\""+pVO.getPointId()+"\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"895cb899-3e3e-4ee4-a81c-c077fd732f61\",\"assetId\":\"5c44985b-fbbc-41c1-877b-2b20fb0b66a7\",\"pointLocationId\":\"49db6ff7-638a-4a28-917e-3cb8845c8c33\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"alSetId\":\"c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a\",\"pointName\":\"Test\",\"autoAcknowledge\":false,\"disabled\":false,\"pointType\":\"AC\",\"sampleInterval\":12,\"sensorChannelNum\":2,\"sensorOffset\":1.0,\"sensorSensitivity\":1.0,\"sensorType\":\"Acceleration\",\"sensorUnits\":\"G/s\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":0,\"fMax\":0,\"siteName\":\"ADSFDG\",\"areaName\":\"10\",\"assetName\":\"Asset 620\",\"pointLocationName\":\"1750\",\"demodInterval\":0,\"alarmEnabled\":false,\"highAlert\":0.0,\"highAlertActive\":false,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":true,\"lowAlert\":0.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"period\":0.0,\"resolution\":0,\"sampleRate\":0.0,\"apAlSetVOs\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"apSetName\":\"AUTO_ACCELERATION\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"alSetName\":\"1143 ASSET 13_VIB\",\"alSetId\":\"c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a\",\"paramName\":\"Total Non Sync\",\"sensorType\":\"Acceleration\",\"fmax\":977,\"resolution\":1600,\"period\":1.6384,\"sampleRate\":2500.0,\"demod\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"paramType\":\"Total Non Sync Energy\",\"frequencyUnits\":\"RPM\",\"paramUnits\":\"mils\",\"paramAmpFactor\":\"RMS\",\"minFrequency\":0.0,\"maxFrequency\":1.0,\"dwell\":3,\"lowFaultActive\":true,\"lowAlertActive\":true,\"highAlertActive\":true,\"highFaultActive\":true,\"lowFault\":0.0,\"lowAlert\":1.0,\"highAlert\":2.0,\"highFault\":3.0}]}";
        String expResult = "{\"outcome\":\"Deleted Points successfully.\"}";
        String result = instance.deletePoints(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deletePoints method, of class PointsController.
     * Unhappy path: Missing required field pointId in input json.
     */
    @Test
    public void test96_DeletePoints() {
        System.out.println("test96_DeletePoints");
        content = "{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"areaId\":\"895cb899-3e3e-4ee4-a81c-c077fd732f61\",\"assetId\":\"5c44985b-fbbc-41c1-877b-2b20fb0b66a7\",\"pointLocationId\":\"49db6ff7-638a-4a28-917e-3cb8845c8c33\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"alSetId\":\"c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a\",\"pointName\":\"Test\",\"autoAcknowledge\":false,\"disabled\":false,\"pointType\":\"AC\",\"sampleInterval\":12,\"sensorChannelNum\":2,\"sensorOffset\":1.0,\"sensorSensitivity\":1.0,\"sensorType\":\"Acceleration\",\"sensorUnits\":\"G/s\",\"demodEnabled\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"dwell\":0,\"fMax\":0,\"siteName\":\"ADSFDG\",\"areaName\":\"10\",\"assetName\":\"Asset 620\",\"pointLocationName\":\"1750\",\"demodInterval\":0,\"alarmEnabled\":false,\"highAlert\":0.0,\"highAlertActive\":false,\"highFault\":0.0,\"highFaultActive\":false,\"customized\":true,\"lowAlert\":0.0,\"lowAlertActive\":false,\"lowFault\":0.0,\"lowFaultActive\":false,\"maxFreq\":0.0,\"minFreq\":0.0,\"period\":0.0,\"resolution\":0,\"sampleRate\":0.0,\"apAlSetVOs\":[{\"customerAccount\":\"FEDEX EXPRESS\",\"siteId\":\"8b086e15-371f-4a8d-bedb-c1b67b49be3e\",\"apSetName\":\"AUTO_ACCELERATION\",\"apSetId\":\"700cc412-b311-4295-8faa-295d2ef5c354\",\"alSetName\":\"1143 ASSET 13_VIB\",\"alSetId\":\"c9d9f154-bb9d-4d0a-98e8-292d4b75ac2a\",\"paramName\":\"Total Non Sync\",\"sensorType\":\"Acceleration\",\"fmax\":977,\"resolution\":1600,\"period\":1.6384,\"sampleRate\":2500.0,\"demod\":false,\"demodHighPass\":0.0,\"demodLowPass\":0.0,\"paramType\":\"Total Non Sync Energy\",\"frequencyUnits\":\"RPM\",\"paramUnits\":\"mils\",\"paramAmpFactor\":\"RMS\",\"minFrequency\":0.0,\"maxFrequency\":1.0,\"dwell\":3,\"lowFaultActive\":true,\"lowAlertActive\":true,\"highAlertActive\":true,\"highFaultActive\":true,\"lowFault\":0.0,\"lowAlert\":1.0,\"highAlert\":2.0,\"highFault\":3.0}]}";
        String expResult = "{\"outcome\":\"insufficient data given in json\"}";
        String result = instance.deletePoints(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deletePoints method, of class PointsController.
     * Unhappy path: Empty input json.
     */
    @Test
    public void test97_DeletePoints() {
        System.out.println("test97_DeletePoints");
        content = "";
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.deletePoints(content);
        assertEquals(expResult, result);
    }

    /**
     * Test of deletePoints method, of class PointsController.
     * Unhappy path: null input json.
     */
    @Test
    public void test98_DeletePoints() {
        System.out.println("test98_DeletePoints");
        content = null;
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = instance.deletePoints(content);
        assertEquals(expResult, result);
    }
}
