/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.config.controller;

import com.uptime.cassandra.config.entity.Points;
import com.uptime.cassandra.presets.entity.SiteApAlSets;
import com.uptime.config.dao.DeviceStatusDAO;
import com.uptime.config.delegate.device.CreateDeviceDelegate;
import com.uptime.config.delegate.pointlocations.CreatePointLocationsDelegate;
import com.uptime.config.vo.DeviceStatusVO;
import com.uptime.config.vo.PointLocationVO;
import com.uptime.config.vo.DeviceVO;
import com.uptime.services.util.JsonConverterUtil;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.stream.JsonParser;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 *
 * @author kpati
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ManageDeviceControllerTest {

    
    @Mock
    DeviceStatusDAO deviceStatusDAO;
    static ManageDeviceController instance;

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//config.cql", true, true, "worldview_dev1"));

    transient JsonParser parser;
    transient JsonParser.Event event;
    private static List<Points> pointList;
    private static PointLocationVO pointLocationVO;
    private static SiteApAlSets siteApAlSets;
    private static String customerAccount;

    private static DeviceVO deviceVO;

    public ManageDeviceControllerTest() {
        pointList = new ArrayList();
        pointLocationVO = new PointLocationVO();
        deviceVO = new DeviceVO();
        customerAccount = "77777";
        deviceStatusDAO = PowerMockito.mock(DeviceStatusDAO.class);

        pointLocationVO.setCustomerAccount(customerAccount);
        pointLocationVO.setSiteId(UUID.fromString("2d2a3fc1-f82a-4710-ab83-8dfc935f9726"));
        pointLocationVO.setAreaId(UUID.fromString("2d2a3fc1-f82a-4710-ab83-8dfc935f9726"));
        pointLocationVO.setAssetId(UUID.fromString("2d2a3fc1-f82a-4710-ab83-8dfc935f9726"));
        pointLocationVO.setPointLocationId(UUID.fromString("2d2a3fc1-f82a-4710-ab83-8dfc935f9726"));
        pointLocationVO.setPointLocationName("Test PL Name");
        pointLocationVO.setBasestationPortNum((short) 10);

        siteApAlSets = new SiteApAlSets();
        siteApAlSets.setAlSetId(UUID.fromString("2d2a3fc1-f82a-4710-ab83-8dfc935f9726"));
        siteApAlSets.setApSetId(UUID.fromString("2d2a3fc1-f82a-4710-ab83-8dfc935f9726"));
        siteApAlSets.setCustomerAccount(customerAccount);
        siteApAlSets.setSiteId(pointLocationVO.getSiteId());
        siteApAlSets.setParamName("Test Param Name");
        siteApAlSets.setSensorType("Acceleration");
        siteApAlSets.setFmax(0);
        siteApAlSets.setResolution(0);
        siteApAlSets.setPeriod(0);
        siteApAlSets.setSampleRate(0);
        siteApAlSets.setDemod(false);
        siteApAlSets.setDemodHighPass(0);
        siteApAlSets.setDemodLowPass(0);
        siteApAlSets.setMinFrequency(0);
        siteApAlSets.setMaxFrequency(0);
        siteApAlSets.setDwell(0);
        siteApAlSets.setLowFaultActive(false);
        siteApAlSets.setLowAlertActive(false);
        siteApAlSets.setHighAlertActive(false);
        siteApAlSets.setHighFaultActive(false);
        siteApAlSets.setLowFault(0);
        siteApAlSets.setLowAlert(0);
        siteApAlSets.setHighAlert(0);
        siteApAlSets.setHighFault(0);
        

        CreatePointLocationsDelegate createPointLocationsDelegate = new CreatePointLocationsDelegate();
        CreateDeviceDelegate createDeviceDelegate = new CreateDeviceDelegate();
        // Insert the entities into Cassandra
        try {
            //create a point location for testing purpose
            createPointLocationsDelegate.createPointLocations(pointLocationVO);
            //create a siteApAlSets for testing purpose
            createDeviceDelegate.createSiteApAlSet(siteApAlSets);
        } catch (Exception e) {
            System.out.println(" exp--------------------------> " + e.getMessage());
        }

        instance = new ManageDeviceController();

        deviceVO.setCustomerAccount(customerAccount);
        deviceVO.setSiteId(pointLocationVO.getSiteId());
        deviceVO.setAreaId(pointLocationVO.getAreaId());
        deviceVO.setAssetId(pointLocationVO.getAssetId());
        deviceVO.setPointLocationId(pointLocationVO.getPointLocationId());
        deviceVO.setApSetId(siteApAlSets.getApSetId());
        deviceVO.setAlSetId(siteApAlSets.getAlSetId());
        deviceVO.setSiteName("Test Site Name");
        deviceVO.setAreaName("Test Area Name");
        deviceVO.setAssetName("Test Asset Name");
        deviceVO.setPointLocationName(pointLocationVO.getPointLocationName());
        deviceVO.setBasestationPortNum(pointLocationVO.getBasestationPortNum());
        deviceVO.setNewDeviceId("BB009010");
        deviceVO.setExistingDeviceId("BB009010");
        deviceVO.setChannelType("Acceleration");

        Points points = new Points();
        points.setPointId(UUID.fromString("2d2a3fc1-f82a-4710-ab83-8dfc935f9726"));
        points.setApSetId(deviceVO.getApSetId());
        points.setAlSetId(deviceVO.getAlSetId());
        points.setSensorType("Acceleration");
        points.setPointType("AC");
        points.setPointName("997 ULTRA");
        points.setSensorChannelNum(0);
        points.setSensorOffset(0);
        points.setSensorSensitivity(50);
        points.setSensorType("Ultrasonic");
        points.setSensorUnits("Pa");
        points.setAlarmEnabled(false);
        pointList.add(points);
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
        
    }
    
    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of replaceDevice method, of class ManageDeviceController.
     */
    @Test
    public void test010_InstallNewDevice() {
        try {
            System.out.println("InstallNewDevice");
            
            String install_device_json_content = "{\n"
                    + "  \"customerAccount\": \"" + deviceVO.getCustomerAccount() + "\",\n"
                    + "  \"siteId\": \"" + deviceVO.getSiteId() + "\",\n"
                    + "  \"areaId\": \"" + deviceVO.getAreaId() + "\",\n"
                    + "  \"assetId\": \"" + deviceVO.getAssetId() + "\",\n"
                    + "  \"pointLocationId\": \"" + deviceVO.getPointLocationId() + "\",\n"
                    + "  \"channelNum\": 0,\n"
                    + "  \"pointList\": [\n"
                    + "    {\n"
                    + "      \"customerAccount\": \"" + deviceVO.getCustomerAccount() + "\",\n"
                    + "      \"siteId\": \"" + deviceVO.getSiteId() + "\",\n"
                    + "      \"areaId\": \"" + deviceVO.getAreaId() + "\",\n"
                    + "      \"assetId\": \"" + deviceVO.getAssetId() + "\",\n"
                    + "      \"pointLocationId\": \"" + deviceVO.getPointLocationId() + "\",\n"
                    + "      \"pointId\": \"" + pointList.get(0).getPointId() + "\",\n"
                    + "      \"apSetId\": \"" + deviceVO.getApSetId() + "\",\n"
                    + "      \"alSetId\": \"" + deviceVO.getAlSetId() + "\",\n"
                    + "      \"pointName\": \"" + pointList.get(0).getPointName() + "\",\n"
                    + "      \"autoAcknowledge\": true,\n"
                    + "      \"disabled\": false,\n"
                    + "      \"pointType\": \"" + pointList.get(0).getPointType() + "\",\n"
                    + "      \"sampleInterval\": 0,\n"
                    + "      \"sensorChannelNum\": \"" + pointList.get(0).getSensorChannelNum() + "\",\n"
                    + "      \"sensorOffset\": \"" + pointList.get(0).getSensorOffset() + "\",\n"
                    + "      \"sensorSensitivity\": \"" + pointList.get(0).getSensorSensitivity() + "\",\n"
                    + "      \"sensorType\": \"" + pointList.get(0).getSensorType() + "\",\n"
                    + "      \"sensorUnits\": \"" + pointList.get(0).getSensorUnits() + "\",\n"
                    + "      \"demodEnabled\": false,\n"
                    + "      \"demodHighPass\": \"" + siteApAlSets.getDemodHighPass()+ "\",\n"
                    + "      \"demodLowPass\": \"" + siteApAlSets.getDemodLowPass()+ "\",\n"
                    + "      \"dwell\": \"" + siteApAlSets.getDwell()+ "\",\n"
                    + "      \"fMax\": 0,\n"
                    + "      \"demodInterval\": 0,\n"
                    + "      \"alarmEnabled\": \"" + pointList.get(0).isAlarmEnabled() + "\",\n"
                    + "      \"c\": 0,\n"
                    + "      \"highAlertActive\": false,\n"
                    + "      \"highFault\": 0,\n"
                    + "      \"highFaultActive\": false,\n"
                    + "      \"customized\": false,\n"
                    + "      \"lowAlert\": 0,\n"
                    + "      \"lowAlertActive\": false,\n"
                    + "      \"lowFault\": 0,\n"
                    + "      \"lowFaultActive\": false,\n"
                    + "      \"maxFreq\": 0,\n"
                    + "      \"minFreq\": 0,\n"
                    + "      \"period\": \"" + siteApAlSets.getPeriod()+ "\",\n"
                    + "      \"resolution\":\"" + siteApAlSets.getResolution() + "\",\n"
                    + "      \"sampleRate\": \"" + siteApAlSets.getSampleRate()+ "\",\n"
                    + "      \"apAlSetVOs\": [\n"
                    + "        {\n"
                    + "          \"customerAccount\": \"" + deviceVO.getCustomerAccount() + "\",\n"
                    + "          \"siteId\": \"" + deviceVO.getSiteId() + "\",\n"
                    + "          \"apSetName\": \"AUTO_ULTRASONIC\",\n"
                    + "          \"apSetId\": \"" + deviceVO.getApSetId() + "\",\n"
                    + "          \"alSetName\": \"2560 ASSET 317_ULTRA\",\n"
                    + "          \"alSetId\": \"" + deviceVO.getAlSetId() + "\",\n"
                    + "          \"sensorType\": \"" + siteApAlSets.getSensorType() + "\",\n"
                    + "          \"fmax\": \"" + siteApAlSets.getFmax() + "\",\n"
                    + "          \"resolution\": \"" + siteApAlSets.getResolution() + "\",\n"
                    + "          \"period\": \"" + siteApAlSets.getPeriod()+ "\",\n"
                    + "          \"sampleRate\": \"" + siteApAlSets.getSampleRate()+ "\",\n"
                    + "          \"demod\": \"" + siteApAlSets.isDemod() + "\",\n"
                    + "          \"demodHighPass\": \"" + siteApAlSets.getDemodHighPass() + "\",\n"
                    + "          \"demodLowPass\": \"" + siteApAlSets.getDemodLowPass() + "\",\n"
                    + "          \"minFrequency\": \"" + siteApAlSets.getMinFrequency() + "\",\n"
                    + "          \"maxFrequency\": \"" + siteApAlSets.getMaxFrequency() + "\",\n"
                    + "          \"dwell\": \"" + siteApAlSets.getDwell()+ "\",\n"
                    + "          \"lowFaultActive\": \"" + siteApAlSets.isLowFaultActive() + "\",\n"
                    + "          \"lowAlertActive\": \"" + siteApAlSets.isLowAlertActive() + "\",\n"
                    + "          \"highAlertActive\": \"" + siteApAlSets.isHighAlertActive() + "\",\n"
                    + "          \"highFaultActive\": \"" + siteApAlSets.isHighFaultActive() + "\",\n"
                    + "          \"lowFault\": \"" + siteApAlSets.getLowFault() + "\",\n"
                    + "          \"lowAlert\": \"" + siteApAlSets.getLowAlert() + "\",\n"
                    + "          \"highAlert\": \"" + siteApAlSets.getHighAlert() + "\",\n"
                    + "          \"highFault\": \"" + siteApAlSets.getHighFault() + "\"\n"
                    + "        }\n"
                    + "      ]\n"
                    + "    }\n"
                    + "  ],\n"
                    + "  \"newDeviceId\": \"" + deviceVO.getNewDeviceId() + "\",\n"
                    + "  \"siteName\": \"" + deviceVO.getSiteName() + "\",\n"
                    + "  \"areaName\": \"" + deviceVO.getAreaName() + "\",\n"
                    + "  \"assetName\": \"" + deviceVO.getAssetName() + "\",\n"
                    + "  \"pointLocationName\": \"" + deviceVO.getPointLocationName() + "\",\n"
                    + "  \"dbpointListExists\": false,\n"
                    + "  \"basestationPortNum\": \"" + deviceVO.getBasestationPortNum() + "\"\n"
                    + "}";
//        deviceStatusDAO.upsert(deviceStatusVO);
        when(deviceStatusDAO.upsert(new DeviceStatusVO())).thenReturn(1);
        assertEquals("{\"outcome\":\"Device installed successfully.\"}", instance.installNewDevice(install_device_json_content));
        } catch (SQLException ex) {
            Logger.getLogger(ManageDeviceControllerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of updateDevice method, of class ManageDeviceController.
     */
    @Test
    public void test011_UpdateDevice() {
        System.out.println("updateDevice");
        StringBuilder sb;
        try {
            when(deviceStatusDAO.upsert(new DeviceStatusVO())).thenReturn(1);
            when(deviceStatusDAO.clearDevice("BB009010")).thenReturn(1);
            deviceVO.setExistingDeviceId("BB009010");

            sb = new StringBuilder();
            sb
                    .append("{\"request\":\"").append("enableDevice")
                    .append("\",\"attributes\":").append(JsonConverterUtil.toJSON(deviceVO)).append("}");
            assertEquals("{\"outcome\":\"Device enabled successfully.\"}", instance.updateDevice(sb.toString()));
        } catch (Exception ex) {
            fail("The method test011_UpdateDevice Failed.");
        }
    }

    /**
     * Test of updateDevice method, of class ManageDeviceController.
     */
    @Test
    public void test012_UpdateDevice() {
        System.out.println("updateDevice");
        StringBuilder sb;

        try {
            when(deviceStatusDAO.upsert(new DeviceStatusVO())).thenReturn(1);
            when(deviceStatusDAO.clearDevice("BB009010")).thenReturn(1);
            deviceVO.setExistingDeviceId("BB009010");

            sb = new StringBuilder();
            sb
                    .append("{\"request\":\"").append("disableDevice")
                    .append("\",\"attributes\":").append(JsonConverterUtil.toJSON(deviceVO)).append("}");
            assertEquals("{\"outcome\":\"Device disabled successfully.\"}", instance.updateDevice(sb.toString()));
        } catch (Exception ex) {
            fail("The method test012_UpdateDevice Failed.");
        }
    }
    
    /**
     * Test of updateDevice method, of class ManageDeviceController.
     */
    @Test
    public void test013_NonUpdateDevice() {
        System.out.println("nonupdateDevice");
        StringBuilder sb;

        try {
            deviceVO.setExistingDeviceId("BB009010");
            when(deviceStatusDAO.upsert(new DeviceStatusVO())).thenReturn(1);
            when(deviceStatusDAO.clearDevice("BB009010")).thenReturn(1);
            sb = new StringBuilder();
            sb
                    .append("{\"request\":\"").append("nondisableDevice")
                    .append("\",\"attributes\":").append(JsonConverterUtil.toJSON(deviceVO)).append("}");
            assertEquals("{\"outcome\":\"Invalid Request type in json\"}", instance.updateDevice(sb.toString()));
        } catch (Exception ex) {
            fail("The method test012_UpdateDevice Failed.");
        }
    }

    /**
     * Test of updateDevice method, of class ManageDeviceController.
     */
//    @Test
//    public void test013_UpdateDevice() {
//        System.out.println("updateDevice");
//        List<PointVO> pointVOList;
//        StringBuilder sb;
//        PointVO points;
//        
//        try {
//
//            points = new PointVO();
//            points.setCustomerAccount(customerAccount);
//            points.setSiteId(UUID.fromString("2d2a3fc1-f82a-4710-ab83-8dfc935f9726"));
//            points.setAreaId(UUID.fromString("2d2a3fc1-f82a-4710-ab83-8dfc935f9726"));
//            points.setAssetId(UUID.fromString("2d2a3fc1-f82a-4710-ab83-8dfc935f9726"));
//            points.setPointLocationId(UUID.fromString("2d2a3fc1-f82a-4710-ab83-8dfc935f9726"));
//            points.setPointId(UUID.fromString("2d2a3fc1-f82a-4710-ab83-8dfc935f9726"));
//            points.setApSetId(deviceVO.getApSetId());
//            points.setAlSetId(deviceVO.getAlSetId());
//            points.setSensorType("Acceleration");
//
//            pointVOList = new ArrayList();
//            pointVOList.add(points);
//
//            deviceVO.setPointList(pointVOList);
//            deviceVO.setExistingDeviceId("BB009010");
//            deviceVO.setNewDeviceId("BB009006");
//            
//            sb = new StringBuilder();
//            sb
//                    .append("{\"request\":\"").append("replaceDevice")
//                    .append("\",\"attributes\":").append(JsonConverterUtil.toJSON(deviceVO)).append("}");
//            
//            assertEquals("{\"outcome\":\"insufficient data found.\"}", instance.updateDevice(sb.toString()));
//        } catch (Exception ex) {
//            fail("The method test013_UpdateDevice Failed.");
//        }
//    }
    /**
     * Test of getPointToHwUnitByCustomerSiteAreaAssetLocation method, of class
     * ManageDeviceController.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test03_GetPointToHwUnitByCustomerSiteAreaAssetLocation() throws Exception {
        String expResponJson = "{\"customer\":\"" + deviceVO.getCustomerAccount() + "\",\"siteId\":\"" + deviceVO.getSiteId() + "\",\"areaId\":\"" + deviceVO.getAreaId() + "\",\"assetId\":\"" + deviceVO.getAssetId() + "\",\"pointLocationId\":\"" + deviceVO.getPointLocationId() + "\",\"PointToHwUnit\":[{\"pointId\":\"" + pointList.get(0).getPointId() + "\",\"channelType\":\"" + pointList.get(0).getPointType() + "\",\"channelNum\":" + pointList.get(0).getSensorChannelNum() + ",\"deviceId\":\"" + deviceVO.getExistingDeviceId() + "\"}],\"outcome\":\"GET worked successfully.\"}";
        String responseJson = instance.getPointToHwUnitByCustomerSiteAreaAssetLocation(deviceVO.getCustomerAccount(), UUID.fromString(deviceVO.getSiteId().toString()), UUID.fromString(deviceVO.getAreaId().toString()), UUID.fromString(deviceVO.getAssetId().toString()), UUID.fromString(deviceVO.getPointLocationId().toString()));
        assertEquals(expResponJson, responseJson);
        System.out.println("getPointToHwUnitByCustomerSiteAreaAssetLocation Done");
    }
    
    /**
     * Test of getPointToHwUnitByCustomerSiteAreaAssetLocation method, of class
     * ManageDeviceController.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test07_GetPointToHwUnitByCustomerSiteAreaAssetLocation() throws Exception {
        String expResponJson = "";
        String responseJson = instance.getPointToHwUnitByCustomerSiteAreaAssetLocation(deviceVO.getCustomerAccount(), UUID.fromString(deviceVO.getSiteId().toString()), UUID.fromString(deviceVO.getAreaId().toString()), UUID.fromString(deviceVO.getAssetId().toString()), UUID.fromString(deviceVO.getPointLocationId().toString()));
        assertNotEquals(expResponJson, responseJson);
        System.out.println("getPointToHwUnitByCustomerSiteAreaAssetLocation Done");
    }

     /**
     * Test of getDeviceManagementRowsByCustomerSiteIdDevice method, of class
     * ManageDeviceController.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test04_GetDeviceManagementRowsByCustomerSiteIdDevice() throws Exception {
        String expRespJson = "{\"customer\":\"" + deviceVO.getCustomerAccount() + "\",\"siteId\":\"" + deviceVO.getSiteId() + "\",\"deviceId\":\"" + deviceVO.getExistingDeviceId() + "\",\"disabled\":true,\"deviceManagementRows\":[{\"deviceId\":\"" + deviceVO.getExistingDeviceId() + "\",\"customerAcct\":\"" + deviceVO.getCustomerAccount() + "\",\"siteId\":\"" + deviceVO.getSiteId() + "\",\"areaId\":\"" + deviceVO.getAreaId() + "\",\"assetId\":\"" + deviceVO.getAssetId() + "\",\"pointLocationId\":\"" + pointLocationVO.getPointLocationId() + "\",\"pointLocationName\":\"" + pointLocationVO.getPointLocationName() + "\",\"pointId\":\"" + pointList.get(0).getPointId() + "\",\"pointName\":\"" + pointList.get(0).getPointName() + "\",\"apSetId\":\"" + deviceVO.getApSetId() + "\",\"alarmEnabled\":false,\"channelType\":\"" + pointList.get(0).getPointType() + "\",\"channelNum\":" + pointList.get(0).getSensorChannelNum() + ",\"sensorType\":\"" + siteApAlSets.getSensorType() + "\",\"fmax\":"+siteApAlSets.getFmax()+",\"resolution\":"+siteApAlSets.getResolution()+",\"basestationPortNum\":"+deviceVO.getBasestationPortNum()+"}],\"outcome\":\"GET worked successfully.\"}";
        String responseJson = instance.getDeviceManagementRowsByCustomerSiteIdDevice(deviceVO.getCustomerAccount(), UUID.fromString(deviceVO.getSiteId().toString()), deviceVO.getExistingDeviceId());
        assertEquals(expRespJson, responseJson);
        System.out.println("getDeviceManagementRowsByCustomerSiteIdDevice Done");
    }
    
    /**
     * Test of getDeviceManagementRowsByCustomerSiteIdDevice method, of class
     * ManageDeviceController.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void test09_GetDeviceManagementRowsByCustomerSiteIdDevice() throws Exception {
        String expRespJson = "";
        String responseJson = instance.getDeviceManagementRowsByCustomerSiteIdDevice(deviceVO.getCustomerAccount(), UUID.fromString(deviceVO.getSiteId().toString()), deviceVO.getExistingDeviceId());
        assertNotEquals(expRespJson, responseJson);
        System.out.println("getDeviceManagementRowsByCustomerSiteIdDevice Done");
    }

    /**
     * Test of getVerboseDeviceManagementRowsByCustomerSiteIdDevice method, of class ManageDeviceController.
     */
    @Test
    public void test05_GetVerboseDeviceManagementRowsByCustomerSiteIdDevice() throws Exception {
        String expResponJson = "{\"customer\":\"" + deviceVO.getCustomerAccount() + "\",\"siteId\":\"" + deviceVO.getSiteId() + "\",\"deviceId\":\"" + deviceVO.getExistingDeviceId() + "\",\"disabled\":true,\"channelVOs\":[{\"customerAccount\":\"" + deviceVO.getCustomerAccount() + "\",\"deviceId\":\"" + deviceVO.getExistingDeviceId() + "\",\"siteId\":\"" + deviceVO.getSiteId() + "\",\"areaId\":\"" + deviceVO.getAreaId() + "\",\"assetId\":\"" + deviceVO.getAssetId() + "\",\"pointLocationId\":\"" + pointLocationVO.getPointLocationId() + "\",\"pointLocationName\":\"" + pointLocationVO.getPointLocationName() + "\",\"pointId\":\"" + pointList.get(0).getPointId() + "\",\"pointName\":\"" + pointList.get(0).getPointName() + "\",\"apSetId\":\"" + deviceVO.getApSetId() + "\",\"alSetId\":\"" + deviceVO.getAlSetId() + "\",\"pointType\":\""+ pointList.get(0).getPointType()+"\",\"basestationPortNum\":" + deviceVO.getBasestationPortNum() + ",\"sensorChannelNum\":"+ pointList.get(0).getSensorChannelNum()+",\"sensorOffset\":" + pointList.get(0).getSensorOffset() + ",\"sensorSensitivity\":" + (float)pointList.get(0).getSensorSensitivity() + ",\"sensorType\":\"" +pointList.get(0).getSensorType()+ "\",\"sensorUnits\":\"" + pointList.get(0).getSensorUnits() + "\",\"rollDiameter\":0.0,\"speedRatio\":0.0,\"fMax\":0,\"resolution\":" + siteApAlSets.getResolution() + ",\"period\":" + (float)siteApAlSets.getPeriod()+ ",\"sampleRate\":" + (float)siteApAlSets.getSampleRate()+ ",\"sampleInterval\":0,\"alarmEnabled\":false,\"apAlSetVOs\":[{\"customerAccount\":\"" + deviceVO.getCustomerAccount() + "\",\"siteId\":\"" + deviceVO.getSiteId() + "\",\"apSetId\":\"" + deviceVO.getApSetId() + "\",\"alSetId\":\"" + deviceVO.getAlSetId() + "\",\"paramName\":\"" + siteApAlSets.getParamName() + "\",\"sensorType\":\"" + siteApAlSets.getSensorType() + "\",\"fmax\":"+siteApAlSets.getFmax()+",\"resolution\":"+siteApAlSets.getResolution()+",\"period\":" + (float)siteApAlSets.getPeriod()+ ",\"sampleRate\":" + (float)siteApAlSets.getSampleRate()+ ",\"demod\":" + siteApAlSets.isDemod() + ",\"demodHighPass\":" + (float)siteApAlSets.getDemodHighPass() + ",\"demodLowPass\":" + (float)siteApAlSets.getDemodLowPass() + ",\"minFrequency\":" + (float)siteApAlSets.getMinFrequency() + ",\"maxFrequency\":" + (float)siteApAlSets.getMaxFrequency() + ",\"dwell\":" + siteApAlSets.getDwell()+ ",\"lowFaultActive\":" + siteApAlSets.isLowFaultActive() + ",\"lowAlertActive\":" + siteApAlSets.isLowAlertActive() + ",\"highAlertActive\":" + siteApAlSets.isHighAlertActive() + ",\"highFaultActive\":" + siteApAlSets.isHighFaultActive() + ",\"lowFault\":" + (float)siteApAlSets.getLowFault()+ ",\"lowAlert\":" + (float)siteApAlSets.getLowAlert()+ ",\"highAlert\":" + (float)siteApAlSets.getHighAlert()+ ",\"highFault\":" + (float)siteApAlSets.getHighFault()+ "}]}],\"outcome\":\"GET worked successfully.\"}";
        String responseJson = instance.getVerboseDeviceManagementRowsByCustomerSiteIdDevice(deviceVO.getCustomerAccount(), deviceVO.getSiteId(), deviceVO.getExistingDeviceId());
        assertEquals(expResponJson, responseJson);
        System.out.println("getVerboseDeviceManagementRowsByCustomerSiteIdDevice Done");
    }
    
    /**
     * Test of getVerboseDeviceManagementRowsByCustomerSiteIdDevice method, of class ManageDeviceController.
     */
    @Test
    public void test14_GetVerboseDeviceManagementRowsByCustomerSiteIdDevice() throws Exception {
        String expResponJson = "";
        String responseJson = instance.getVerboseDeviceManagementRowsByCustomerSiteIdDevice(deviceVO.getCustomerAccount(), deviceVO.getSiteId(), deviceVO.getExistingDeviceId());
        assertNotEquals(expResponJson, responseJson);
        System.out.println("getVerboseDeviceManagementRowsByCustomerSiteIdDevice Done");
    }
    /**
     * Test of installOrModifyTS1Device method, of class ManageDeviceController.
     */
    @Test
    public void test06_installOrModifyTS1Device() {
        System.out.println("installOrModifyTS1Device");
        
        try {
            assertEquals("{\"outcome\":\"insufficient and/or invalid data given in json\"}", instance.installOrModifyTS1Device("", false, false));
        } catch (Exception ex) {
            fail("The method installOrModifyTS1Device Failed.");
        }
    }
    
    /**
     * Test of deleteDevice method, of class ManageDeviceController.
     */
    @Test
    public void test50_deleteDevice() {
        System.out.println("deleteDevice");
        
        try {
            deviceVO.setExistingDeviceId("BB009010");
            deviceVO.setNewDeviceId(null);
            assertEquals("{\"outcome\":\"Device deleted successfully.\"}", instance.deleteDevice(deviceVO));
        } catch (Exception ex) {
            fail("The method installOrModifyTS1Device Failed.");
        }
    }
}
